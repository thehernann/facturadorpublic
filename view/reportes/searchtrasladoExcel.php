<?php 


$objPHPExcel = new PHPExcel();


        


// Set document properties
$objPHPExcel->getProperties()->setCreator("vtechnology")
               ->setLastModifiedBy("HERNAN VILCHEZ")
               ->setTitle("Office 2007 XLSX Test Document")
               ->setSubject("Office 2007 XLSX Test Document")
               ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
               ->setKeywords("office 2007 openxml php")
               ->setCategory("Test result file");

$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
                                          ->setSize(10);            

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'N°')
            ->setCellValue('B1', 'Fecha')
            ->setCellValue('C1', 'Tipo de Doc')
            ->setCellValue('D1', 'Serie')
            ->setCellValue('E1', 'Numero')
            ->setCellValue('F1', 'Punto de Partida')
            ->setCellValue('G1', 'Destinatario');
        
//$informe = getAllListsAndVideos();
//$i = 2;
//while($row = $informe->fetch_array(MYSQLI_ASSOC))
//{

    $i = 2;
    
   foreach ($documentos as $documento){
       
            
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A".$i, $i-1)
            ->setCellValue("B".$i, $documento->getFechaemision())
            ->setCellValue("C".$i, $documento->getTipo())
            ->setCellValue("D".$i, $documento->getSerie())
            ->setCellValue("E".$i, $documento->getNumero())
            ->setCellValue("F".$i, $documento->getRuc()." - ".$documento->getRazonsocial())
            ->setCellValue("G".$i, $documento->getRucdestinatario().' - '.$documento->getRazonsialdestinatario()); 
           
           
//            ->setCellValue("B".$i, "hol")
//            ->setCellValue("C".$i, "hola")
//            ->setCellValue("D".$i, "hola")
//            ->setCellValue("E".$i, "hola")
//            ->setCellValue("F".$i, "hola")
//            ->setCellValue("G".$i, "hola") 
//            ->setCellValue("H".$i, "hola")
//            ->setCellValue("I".$i, "hola");
           
         
        $i++;
        

}


//$i++;
//}

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);




$objPHPExcel->getActiveSheet()->setTitle('Reporte');

$objPHPExcel->setActiveSheetIndex(0);

getHeaders();

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
 /* Limpiamos el búfer */
ob_end_clean();
$objWriter->save('php://output');
exit;