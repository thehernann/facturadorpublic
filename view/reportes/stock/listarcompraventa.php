<?php 

  /** Actual month last day **/
  function _data_last_month_day() { 
      $month = date('m');
      $year = date('Y');
      $day = date("d", mktime(0,0,0, $month+1, 0, $year));
 
      return date('d/m/Y', mktime(0,0,0, $month, $day, $year));
  }
 
  /** Actual month first day **/
  function _data_first_month_day() {
      $month = date('m');
      $year = date('Y');
      return date('d/m/Y', mktime(0,0,0, $month, 1, $year));
  }



?>

<section class="content">
    <div class="container-fluid">
        
       
        
        <div class="block-header">
            <!--    <br>-->
            <div class="RespuestaAjax"></div>
        </div>
        <!-- Basic Examples -->

        <!-- #END# Basic Examples -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           Stock Compra - Venta
                            

                        </h2>
                       
                    
                    </div>
                    <div class="body">
                        <input type="hidden" value="<?=base_url ?>" id="url" name="url">  
                        
                     <form action="<?= base_url?>producto/searchstockcompraventa" method="POST"  id="FormularioAjaxBuscar" data-form="insert" enctype="multipart/form-data" autocomplete="off" >   
                        
                        <div class="row">  
                        <?php if(permisos::rol('rol-compraventareportesucur')){?>
                           <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                            
                                <label>Almacén</label>
                                <select class="form-control show-tick" id="cbsucursalstockgeneral" name="cbsucursalstockgeneral">
                                    
                                    <?php 
                                   
                                                                        
                                        foreach ($sucursales as $sucursal){
                                            if($_SESSION['idalmacen'] == $sucursal->getId()){
                                                echo '<option value="'.$sucursal->getId().'" selected="selected" >'.$sucursal->getNombre().' - '.$sucursal->getDireccion().'</option>';
                                                
                                            }else{
                                                echo '<option value="'.$sucursal->getId().'" >'.$sucursal->getNombre().' - '.$sucursal->getDireccion().'</option>';
                                                
                                            }
                                            


                                    }

                                    ?>

                            </select>
                            </div>
                        </div>
                        <?php } ?>    
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                                <label>Marca</label>
                                <select  class="form-control show-tick" id="cbmarcastockgeneral" name="cbmarcastockgeneral">
                                    <option value="">- Seleccione -</option>
                                    <?php 
                                  
                                    
                                    foreach ($marcas as $marca){

                                            echo '<option value="'.$marca->getId().'" >'.$marca->getDescripcion().'</option>';


                                    }

                                    ?>

                            </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                                <label>Linea</label>
                                <select  class="form-control show-tick" id="cblineastockgeneral" name="cblineastockgeneral">
                                    <option value="">- Seleccione -</option>
                                    <?php 
                                  
                                    
                                    foreach ($lineas as $linea){

                                            echo '<option value="'.$linea->getId().'" >'.$linea->getDescripcion().'</option>';


                                    }

                                    ?>

                            </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                                <label>Categoria</label>
                                <select  class="form-control show-tick" id="cbcategoriastockgeneral" name="cbcategoriastockgeneral">
                                    <option value="">- Seleccione -</option>
                                    <?php 
                                  
                                    
                                    foreach ($categorias as $categoria){

                                            echo '<option value="'.$categoria->getId().'" >'.$categoria->getDescripcion().'</option>';


                                    }

                                    ?>

                            </select>
                            </div>
                        </div>
                            
                       
                        
                        </div>
                        <div class="row">
                            
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group form-float">
                                <label class="form-label">(Cod., Cod. barra, Descripción) </label>
                                <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" id="txtbuscarstockgeneral" name="txtbuscarstockgeneral">
                                    </div>
                                </div>

                            </div>
                        </div>

                         
                
                        
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <label>Acciones</label>
                            <div class="form-group form-float">
                                
                                <button class="btn btn-sm btn-primary waves-effect" type="submit"><span class="glyphicon glyphicon-search "></span> Buscar</button>
                                
                         
                                
                                <button class="btn btn-sm btn-success waves-effect" id="excelstockgeneral" url="<?= base_url?>producto/stockcompraventaexcel"><span class="glyphicon glyphicon-file"></span> Excel</button>
                                
                            </div>
                        </div>
                        </div>
                            
                            
                            
                            
                        
                     </form>
                        
                        
                        
                        
                       
                        <div class="table-responsive" id="respuestaAjax">
                            <table class="table  table-hover table-bordered" id="tabladocumento">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Categoria</th>
                                        <th>Linea</th>                          
                                        <th>Marca</th>
                                        <th>Descripción</th>    
                                        <th>Compra</th>  
                                        <th>Venta</th>
                                        <th>Saldo</th>
                                        <!--<th>Sucursal</th>-->
                                    </tr>
                                </thead>
<!--                                <tfoot>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Tipo</th>
                                        <th>Serie</th>
                                        <th>Número</th>
                                        <th>RUC/ DNI</th>
                                        <th>Nombre / Rz. Social</th>
                                        <th>Total</th>
                                        <th>Est. Local</th>
                                        <th>Est. Sunat</th>
                                        <th>Imprimir</th>
                                        <th>Acciones</th>
                                    </tr>
                                </tfoot>-->
                                <tbody >
                                    <?php 
                                  
                                    foreach ($cabecera as $detalle){
                                          
                                            echo '<tr>';
                                            
                                            echo '<td>'.$detalle['codigo'].'</td>';
                                       
                                            echo '<td>'.$detalle['categoria'].'</td>';
                                            echo '<td>'.$detalle['linea'].'</td>';
                                           
                                            echo '<td>'.$detalle['marca'].'</td>';
                                            echo '<td>'.$detalle['descripcion'].'</td>';
                                            echo '<td>'.$detalle['compra'].'</td>';
                                            echo '<td>'.$detalle['venta'].'</td>';
                                            echo '<td>'.$detalle['stock_actual'].'</td>';
                                          
                                            
                                            echo '</tr>';
                                        
                                      
                                        
                                    } 
                                    
                                    
                                    
                                    ?>



                                </tbody>
                                
                               
                            </table>
                            <div class="pagination">
                                <nav>
                                    <ul class="pagination"></ul>
                                    
                                </nav>
                                
                            </div>
                            
                        </div>
                     
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table --> 
    </div>
</section>

<script>

   var table = '#tabladocumento'
   $(document).ready( function (){   
     
       $('.pagination').html('')
       var trnum =0
       var maxRows = 20
       var totalRows = $(table+' tbody tr').length
       $(table+' tr:gt(0)').each( function (){
           trnum++
           if(trnum > maxRows){
               $(this).hide()
           }
           if(trnum <= maxRows){
               $(this).show()
               
           }
           
           
           
       })
       if(totalRows > maxRows){
           var pagenum = Math.ceil(totalRows/maxRows)
           for(var i=1; i<= pagenum;){
               $('.pagination').append('<li data-page="'+i+'">\<span>'+ i++ +'<span class="sr-only">(current)</span>\n\
                </span>\</li>').show()
               
           }
           
       }
       $('.pagination li:first-child').addClass('active')
       $('.pagination li').on('click', function (){
       
        var pageNum = $(this).attr('data-page')
        var trIndex = 0;
        $('.pagination li').removeClass('active')
        $(this).addClass('active')
        $(table+' tr:gt(0)').each(function (){
            trIndex++
            if(trIndex > (maxRows*pageNum) || trIndex <= ((maxRows*pageNum)- maxRows)){
                $(this).hide()
            }else {
                $(this).show()
            }
                
            })
            
        });
       
       
       });
       
//       $(function (){
//        $('table tr:eq(0)').prepend('<th>ID</th>')
//        var id=0;
//        $('table tr:gt(0)').each(function (){
//            id++
//            $(this).prepend('<td>'+id+'</td>')
//            
//        })
//       
//       
//       
//       
//       })
       
       
       
       

    
    
  
</script>    




















