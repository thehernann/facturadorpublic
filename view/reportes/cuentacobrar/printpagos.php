<?php 
//   set_include_path(get_include_path() . PATH_SEPARATOR . "/path/to/dompdf");
   
require 'vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
//use Spipu\Html2Pdf\Exception\Html2PdfException;
//use Spipu\Html2Pdf\Exception\ExceptionFormatter;
//header("Content-type: image/jpg"); 
// $path =  base_url.'images/user-lg.jpg';
// $type = pathinfo($path, PATHINFO_EXTENSION);
// $data = file_get_contents($path);
// $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
ob_start();


$codhtml = '
<!DOCTYPE html>
<html>
 
<head>
    <style type="text/css">
    
        body {
          position: relative;
          width: 750px;
          max-width: 750px;
         
          margin: 0 auto; 
        }
        table#detalle{ 
        
        
        
        
        border: 1px solid black;
        border-collapse: separate;
        
        border-radius: 7px;
        border-spacing: 0px;
        width: 750px;
        max-width: 750px;
        }
        thead#detalle {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
            border-collapse: separate;
        }
        tr#detalle {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }
        th, td#detalle {
            
            text-align: left;
            vertical-align: top;
           

             
        }
        td#detalle {
            border-top: 1px solid black;
        }
       
        thead#detalle:first-child tr#detalle:first-child th:first-child, tbody#detalle:first-child tr#detalle:first-child td#detalle:first-child {
            border-radius: 7px 0 0 0;
        }
        thead#detalle:last-child tr#detalle:last-child th#detalle:first-child, tbody#detalle:last-child tr#detalle:last-child td#detalle:first-child {
            border-radius: 0 0 0 7px;
        }
        td.producto,
        th.producto {
          width: 170px;
          max-width: 170px;
          word-break: break-all;
        }

        td.cantidad,
        th.cantidad {
          width: 80px;
          max-width: 80px;
          text-align: center;
          word-break: break-all;
        }

        td.precio,
        th.precio {
          width: 100px;
          max-width: 100px;
          text-align: center;
          word-break: break-all;
        }

        th {
          font-size: 12px;
          font-family: Arial;  

        }


        .centrado {
          text-align: center;
          align-content: center;
        }
        .justificado {
          text-align: justify;

        } 

        

        img {
        /*  max-width: inherit;
          width: inherit;*/
        display:block;


        }

        /*----------- CAJA ----------- */
        #cajaruc { 
        font-family: Arial; 
        
        font-size: 16px; 
        text-align: center;
        line-height : 20px;
        padding: 10px;
        border-radius: 15px 15px 15px 15px;  
        border: 1px solid black;
        width: 250px;
        height: 70px;
        max-width: 250px;
      /*  float: right !important;
        margin-left: 500px;*/
position:absolute;
        left: 490;
        top: 20;
       
        }
        .cajacliente { 
        font-family: Arial;
        font-size: 12px; 
        line-height: 18px;
        text-align: left; 
        overflow: auto; 
        width:400px;
        height: 65px;
        max-width: 400px;
       /* float: left !important;*/

        position: absolute;
        top: 130;
        left:5;
        
        
        }
        .cajafecha { 
        font-family: Arial;  
        font-size: 12px; 
        line-height: 18px;
        text-align: left;  
        
        width: 200px;
        height: 77px;
        max-width:200px;
       /* float: right !important;
        margin-left: 200px;*/
        position: absolute;
        right: 180;
        top: 130;
       
        }
        .importeletra{
            font-family: Arial; 
            font-size: 12px;
        }
        
        .divlogo{
            
            padding: 0px; 

            width: 200px;
            height: 220px;
            max-width: 200px;
           /* position:absolute;
            left: 30;
            top: 0; */


        }

        .divempresa { 
        font-family: Arial;
        color: black; 
        font-size: 12px; 
        
        text-align: center; 
        line-height: 5px;
        padding: 0px; 
        width: 300px;
        height: 220px;
        max-width: 400px;
        position:absolute;
        left: 180;
        top: 20; 
        
        }
        table#resultados{
           
            
            width: 300px;
           max-width: 300px;
           font-size: 12px;
           padding: 10px;
        }
        .rletra{
            text-align: left;
            width: 140px;
            max-width: 140px;
            
        }
        .rnum{
             text-align: right;
             width: 80px;
            max-width: 80px;
        }
        .qr{
            font-family: Arial; 
        
        font-size: 16px; 
        text-align: center;
        line-height : 20px;
        padding: 10px;
        border-radius: 15px 15px 15px 15px;  
        border: 1px solid black;
        width: 730px;
        height: 70px;
        max-width: 730px;
  

        }
        .rqr{
            text-align: center;
            width: 470px;
            max-width: 470px;
        
        }
        
        
     
       
    </style>
 
 
</head>'; 
    $dir= "temp/";
 
    $moneda = '';
    $simbolo = '';
    
    if(!file_exists($dir)){
        mkdir($dir);
        
    }
   $filename = $dir.'test.png';
    $opc = '';  
    
 $dirimg= "temp/img/";
    if(!file_exists($dirimg)){
        mkdir($dirimg);
        
    }
    file_put_contents($dirimg."logo.jpg", base64_decode($sucur->getImgtoplogo()));
    
    $logo = 'temp/img/logo.jpg';

$codhtml.='

 
<body>
    
   
  <div class="A4">
    <div class="divlogo">
      <img src="'.$logo.'" width="100" height="100" alt="Logo" />'. 
        
    '</div><div class="divempresa"><h4>'
     
    .$sucur->getEmpresa().'</h4><hr>'.$sucur->getDireccion().'<br>'.$sucur->getDpto().' - '.$sucur->getProvincia().' - '.$sucur->getDistrito().'<br>'.$sucur->getTelf().'<br>'.
    '</div><div class="cajacliente">'
       
        . '<strong>Nombre / Razón social: </strong>'.
        $document->getRazonsocial().'<br>'.
        $document->getRuc().'<br>'.
        'DOCUMENTO: '.$document->gettipo().'<br>'
         .'<strong>Serie / Nro:  </strong>'.$document->getSerie().' - '.$document->getNumero().'<br>'.
         '<strong>Fecha Emisión:  </strong>'.$document->getFechaemision().'<br>'.
        
    '</div>
  
    <div class="cajafecha">'.
    '<strong>Fecha Vencimiento:  </strong>'.$document->getFechavencimiento().'<br>
     <strong>Total:  </strong>'.number_format($document->getTotal(),2).' '.$document->getMoneda().'<br>
     <strong>Amortizado:  </strong>'.number_format($amortizado,2).' '.$document->getMoneda().'<br>
     <strong>Deuda:  </strong>'.number_format($document->getTotal()-$amortizado,2).' '.$document->getMoneda().'<br>'.
    '</div>
        <label> Detalle de abonos: </label>
    <table id="detalle">
      <thead>
        <tr>
          <th class="cantidad">TIPO PAGO</th>
          <th class="producto">ENTIDAD</th>
          <th class="precio">NRO. OP / CHEQUE</th>
          <th class="precio">MONTO</th>
          <th class="precio">FECHA</th>
          <th class="producto">OBSERVACION</th>
        </tr>
      </thead>
      <tbody>';
          
          foreach ($pagos as $pago){
             
$codhtml.='
        <tr>
            <td class="cantidad">'.$pago->getTipopago().'</td>
            <td class="producto">'.$pago->getEntidad().'</td>
            <td class="precio">'.$pago->getNop().'</td>
            <td class="precio">'.$pago->getMonto().'</td>
            <td class="precio">'.$pago->getFecha().'</td>
            <td class="observacion">'.$pago->getObservacion().'</td>
        </tr>';

          
        
        
          } 
          
  
$codhtml.='
      </tbody>
      
      </table>

    ';
    
$codhtml.='
    
        
    
  </div>
</body>
//</html>';
//ob_get_clean();  
$html2pdf = new Html2Pdf();

//$html2pdf->pdf->SetDisplayMode('fullpage');
$html2pdf->writeHTML($codhtml);
$html2pdf->output('documento.pdf');
    
    
    
