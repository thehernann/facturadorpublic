
<section class="content">
    <div class="container-fluid">

        <div class="block-header">
            <!--<h2>FORM EXAMPLES</h2>-->
            <!--    <div class="RespuestaAjax"></div>-->
        </div>
        <!-- Inline Layout | With Floating Label -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           Amortizar deuda
                            <!--<small>Edición</small>-->
                        </h2>
                        
                        


                    </div>
                    <div class="body">
                        <div class="row">
                        <div class="card">
                            <div class="header">
                                Resumen
                            </div>
                            <div class="body">
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    Nombre / Razón social: <?= $documento->getRazonsocial();?>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    Documento: <?= $documento->getTipo();?>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    Serie / Nro: <?= $documento->getNumero().' - '.$documento->getSerie();?>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    Fecha emisión: <?= $documento->getFechaemision();?>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    Fecha vencimiento: <?= $documento->getFechavencimiento();?>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    Total: <?= number_format($documento->getTotal(),2).' '.$documento->getMoneda(); ?>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    Amortizado: <?= number_format($amortizado,2).' '.$documento->getMoneda(); ?>
                                </div>
                                
                                </div>
                                <div class="row" >
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="text-danger h4">Deuda de la <?=$documento->getTipo()?>: <?= number_format($documento->getTotal()-$amortizado,2).' '.$documento->getMoneda(); ?></div>
                                </div>
                                    <a href="<?= base_url ?>documento/cuentacobrar"  class="btn btn-danger btn-lg m-l-15 waves-effect" id="btncancelar">REGRESAR</a>
                                </div>
                                
                            </div>
                            
                        </div>
                        </div>
                        <?php if($documento->getTotal() - $amortizado > 0){ ?>
                        <div class="row">
                        <div class="card">
                            <div class="header">
                                Nuevo pago
                            </div>
                            <div class="body">
                        <form action="<?= base_url?>documentopago/pagar" method="POST"  id="FormularioAjax" data-form="insert" enctype="multipart/form-data" autocomplete="off" >
                            <div class="row clearfix">
                                <!--<form >-->
                                <div class="row">
                                    <input type="hidden" value="<?= $id ?>" id="id" name="id">

                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Tipo Pago (*)</label>
                                            
                                           <select class="form-control show-tick" id="cbtipopago" name="cbtipopago">
                                    
                                                    <?php 
                                                    $op = array('Efectivo', 'Cheque','Deposito','Transferencia');

                                                        for ($i=0; $i< count($op); $i++){

                                                                echo '<option value="'.$op[$i].'" >'.$op[$i].'</option>';



                                                    }

                                                    ?>

                                            </select>
                                          
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Entidad </label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtentidad" name="txtentidad" value="" >

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Nro. OP / Nro. Cheque</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtnroop" name="txtnroop" value="" >

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Monto en <?=$documento->getMoneda();?> (*)</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtmonto" name="txtmonto" value="" required="" >

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Fecha (*) </label>
                                            <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                            <div class="form-group">
                                                <div class="form-line" id="bs_datepicker_container">
                                                    <input type="text" class="form-control" placeholder="Fecha" id="dpfecha" name="dpfecha" value="" required="">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Observación: </label>
                                            <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                            <div class="form-group">
                                                <div class="form-line" id="bs_datepicker_container">
                                                    <textarea class="form-control" name="txtobservacion" id="txtobservacion"></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </div>




                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
            <!--                            <input type="checkbox" id="remember_me_5" class="filled-in">
                                        <label for="remember_me_5">Remember Me</label>-->
                                        <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">GUARDAR</button>
                                        
                                    </div>
                                </div>
                                <!--</form>-->
                            </div>
                        </form>
                            
                        <div id="respuestaAjax"></div>
                            </div>
                        </div>
                        </div>
                        <?php } ?>
                        
                        <div class="row">
                            <div class="card">
                                <div class="header bg-warning">
                                    Pagos <a  href="<?= base_url.'documentopago/imprimir&id='.$id?>" target="_blank" data-toggle="tooltip" data-placement="top" title="PRINT PDF" > <i class="material-icons" style="border:none;background: none;">local_printshop</i></a>
                                    
                                </div>
                                <div class="body">
                                <div class="table-responsive">    
                                <table class="table  table-hover table-bordered" id="tabladocumento">
                                    <thead>
                                        <tr>
                                            <th>Tipo Pago</th>

                                            <th>Entidad</th>
                                            <th>Nro. Op/Nro. Cheque</th>

                                            <th>Monto</th>
                                            <th>Fecha</th>
                                            <th>Observación</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            
                                                foreach($pagos as $pago){
                                                    echo '<tr>';
                                                    echo '<td>'.$pago->getTipopago();'</td>';
                                                    echo '<td>'.$pago->getEntidad();'</td>';
                                                    echo '<td>'.$pago->getNop();'</td>';
                                                    echo '<td>'.$pago->getMonto();'</td>';
                                                    echo '<td>'.$pago->getFecha();'</td>';
                                                    echo '<td>'.$pago->getObservacion();'</td>';
                                                    
                                                    echo '</tr>';
                                                 

                                                }
                                        ?>
                                        
                                        
                                    </tbody>
                                </table>
                                    
                                </div>
                                    
                                    
                                </div>
                                
                                
                                
                            </div>
                            
                            
                            
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Inline Layout | With Floating Label -->

    </div>
</section>
