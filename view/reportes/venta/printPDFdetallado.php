<?php 
//   set_include_path(get_include_path() . PATH_SEPARATOR . "/path/to/dompdf");
   
require 'vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
//use Spipu\Html2Pdf\Exception\Html2PdfException;
//use Spipu\Html2Pdf\Exception\ExceptionFormatter;
//header("Content-type: image/jpg"); 
// $path =  base_url.'images/user-lg.jpg';
// $type = pathinfo($path, PATHINFO_EXTENSION);
// $data = file_get_contents($path);
// $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
ob_start();


$codhtml = '
<!DOCTYPE html>
<html>
 
<head>
    <style type="text/css">
    
        body {
          position: relative;
          width: 950px;
          max-width: 950px;
         
          margin: 0 auto; 
        }
        table#detalle{ 
        
        
        
        
        border: 1px solid black;
        border-collapse: separate;
        
        border-radius: 7px;
        border-spacing: 0px;
        width: 750px;
        max-width: 750px;
        }
        thead#detalle {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
            border-collapse: separate;
        }
        tr#detalle {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }
        th, td#detalle {
            
            text-align: left;
            vertical-align: top;
           

             
        }
        td#detalle {
            border-top: 1px solid black;
        }
       
        thead#detalle:first-child tr#detalle:first-child th:first-child, tbody#detalle:first-child tr#detalle:first-child td#detalle:first-child {
            border-radius: 7px 0 0 0;
        }
        thead#detalle:last-child tr#detalle:last-child th#detalle:first-child, tbody#detalle:last-child tr#detalle:last-child td#detalle:first-child {
            border-radius: 0 0 0 7px;
        }
        td.producto,
        th.producto {
          width: 250px;
          max-width: 250px;
          word-break: break-all;
        }

        td.cantidad,
        th.cantidad {
          width: 80px;
          max-width: 80px;
          text-align: center;
          word-break: break-all;
        }

        td.precio,
        th.precio {
          width: 80px;
          max-width: 80px;
          text-align: center;
          word-break: break-all;
        }
        td.ajustado,
        th.ajustado {
          width: 47px;
          max-width: 47px;
          text-align: center;
          word-break: break-all;
        }

        th {
          font-size: 12px;
          font-family: Arial;  

        }


        .centrado {
          text-align: center;
          align-content: center;
        }
        .justificado {
          text-align: justify;

        }
        
        td.razonsocial{
          width: 150px;
          max-width: 150px;
          word-break: break-all;
          line-height: 14px;
      
        }

        table#resultados{
           
            
            width: 300px;
           max-width: 300px;
           font-size: 12px;
           padding: 10px;
        }
        .rletra{
            text-align: left;
            width: 140px;
            max-width: 140px;
            
        }
        .rnum{
             text-align: right;
             width: 80px;
            max-width: 80px;
        }
   

    </style>
 
 
</head>'; 

    

 
    if($moneda == 'Soles'){
        $moneda = 'PEN';
        $simbolo = 'S/ ';
        
    }
    if($moneda == 'Dolares'){
        $moneda= 'USD';
        $simbolo = '$ ';
    }
   

$codhtml.='

 
<body>
    
   
  <div class="A4">
    <div class="divlogo">'.  
    '</div><div class="divempresa"><h4> VENTA POR DETALLE </h4>'.'<br> DESDE: '.$desde.' HASTA: '.$hasta.
     
    
    '</div><table id="razonsocialtable">'
        . '<tbody>'
        . '<tr>'
        . ' <td class="razonsocial">'. //<div class="cajacliente">
       '</td>'.
        

    '</tr>
        </tbody>
        </table>
    <div id="cajaruc">'
  
    .'</div>
    '.  
    '<table id="detalle">
      <thead>
        <tr>
          <th class="ajustado">DOC.</th>
          <th class="precio">SERIE/NUM</th>
          <th class="precio">FECHA</th>
          <th class="razonsocial">NOMBRE / RZ</th>
          <th class="precio">VENDEDOR</th>
          <th class="precio">TIPO PAGO</th>
          <th class="producto">DESCRIPCIÓN</th>
          <th class="precio">TOTAL</th>
          <th class="ajustado">INC. IGV</th>
          <th class="precio">TIPO IGV</th>
          <th class="ajustado">ESTADO</th>
        </tr>
      </thead>
      <tbody>';
            $total=0;
            $estado = '';
            foreach ($detallado as $temp){
               if($temp['estadolocal'] != 'Anulado'){
                   $total += $temp['total'];
                   $estado = '';
               }else {
                   $estado = 'Anulado';
               }
              
$codhtml.='
        <tr>
            <td class="ajustado">'.$temp['tipo'].'</td>
            <td class="precio">'.$temp['serien'].'</td>
            <td class="precio">'.$temp['fechaemision'].'</td>
            <td class="razonsocial">'.$temp['razonsocial'].'</td>
            <td class="precio">'.$temp['vendedor'].'</td>
            <td class="precio">'.$temp['tipo_pago'].'</td>
            <td class="producto">'.$temp['descripcionprod'].'</td>
            <td class="precio">'.number_format($temp['total'],2).'</td>
            <td class="ajustado">'.$temp['incigv'].'</td>
            <td class="precio">'.$temp['impuesto'].'</td>
            <td class="ajustado">'.$estado.'</td>
        </tr>';

          } 
          
          
$codhtml.='
      </tbody>
      
      </table>
    
    
      <table id="resultados">
        <tbody>
      
          <tr>
        
           
          <td class="rletra"><strong><h4>TOTAL: </h4></strong></td>
         
          <td class="rnum"><strong><h4>'.$simbolo.' '.number_format($total,2).'</h4></strong></td>
          </tr>
      </tbody>
    </table>
    
    ';
    
    
$codhtml.='

  </div>
</body>
</html>';
//ob_get_clean();  
$html2pdf = new Html2Pdf('L', 'A4', 'es');

//$html2pdf->pdf->SetDisplayMode('fullpage');
$html2pdf->writeHTML($codhtml);
$html2pdf->output('ventadetalladoPDF.pdf');
    
    
    
