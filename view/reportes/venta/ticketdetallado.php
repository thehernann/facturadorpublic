
<!DOCTYPE html>
<html>
 
<head>

  <link rel="stylesheet" href="<?= base_url?>css/cssticket.css">
<!--  <script src="css/cssticket.css"></script>-->
 
</head>

<?php 

     if($moneda == 'Soles'){
        $moneda = 'PEN';
        $simbolo = 'S/ ';

    }
    if($moneda == 'Dolares'){
        $moneda= 'USD';
        $simbolo = '$ ';
    }
 
    
    
   

?>

 
<body>
    
   
  <div class="ticket">
      
      
      <div class="titulo">
          
      </div>
      <div class="encabezado">
          <p class="centrado"><strong>SUCURSAL - <?= strtoupper($_SESSION['sucursal']) ?></strong></p>
          <p class="centrado">VENTA POR DETALLE</p>
      </div>
    <table>
        <thead class="headdetalle">
        <tr>
          <th class="cantidad">S/NUM</th>
          <th class="producto">DESCRIPCIÓN</th>
          <th class="precio">TOTAL</th>
           <th class="precio">EST.</th>
        </tr>
      </thead>
      <tbody class="tbodydetalle">
          <?php
//          $temp = new Temp(); 
          $total=0;
          $estado = '';
          foreach ($detallado as $detalle){
              if($detalle['estadolocal'] != 'Anulado'){
                  $total += $detalle['total'];
                  $estado = '';
          }else {
              
              $estado = 'Anulado';
          }
            
              ?>
        <tr>
          <td class="precio"><?php echo $detalle['serien']; ?></td>
            <td class="producto"><?php echo $detalle['descripcionprod'] ?></td>
            <td class="precio">[<?php echo number_format($detalle['total'] ,2)?>]</td>
            <td class="precio"><?php echo $estado ?></td>
            
            
        </tr>
       
        
        
        <?php 
          }
//            $total+=$importe;
//            $gratuita += $temp->getMontobasegratuito();
//            $exonerada += $temp->getMontobaseexonarado();
//            $inafecta += $temp->getMontobaseinafecto();
//            $ivap += $temp->getMontobaseivap();
//            $expo += $temp->getMontobaseexpo();
//
//          } 
//          $total = $total - ($gratuita + $ivap);
//          
//          $gravada = ($total -($expo + $exonerada + $inafecta))/1.18;
//          $igv =($total -($expo + $exonerada + $inafecta)) - $gravada;
//          
//          if($document->getIncigv() == 0){
//              $gravada = $total;
//              $total += $igv;
//              $igv = $total - $gravada;
//              
//          }
//          
//          $totalf = (float)number_format($total,2);
          ?>
     
         </tbody>
      </table>
    <table class="resultados">
        <tbody >
 
          <tr>
          <td ></td>
          <td></td>
           
          <td class="rletra">TOTAL: </td>
         
          <td class="rnum"><?php echo $simbolo.'  '.number_format($total,2) ?></td>
          </tr>
      </tbody>
    </table>
    <hr>
    <div class="footerup">
        
        
    </div>
       
        
    <div class="footerdown">

       
    </div>
        
        
        
        
        
    </div>
     
 
    <hr>
 
 <script>
window.print();

</script>   
    
</body>


  
</html>


