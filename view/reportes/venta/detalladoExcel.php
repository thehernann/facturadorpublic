<?php 


//require_once ($_SERVER['DOCUMENT_ROOT']."/aquavita/vendor/PHPExcel/Classes/PHPExcel.php");


$objPHPExcel = new PHPExcel();

//$desde = $_POST['desde'];
//$hasta = $_POST['hasta'];
//
//
//require_once ("../../conexion/datasource.php");
////require_once ($_SERVER['DOCUMENT_ROOT']."/aquavita/conexion/datasource.php");
//
//$data_source = new DataSource();
//$data_tabla = $data_source->ejecutarconsulta("select doc.fecha_vencimiento,tc.op,doc.serie,doc.numero,p.rucdni,concat(p.nombre,' ',p.apellidos) as nombre,SUM(d.cantidad)as cant,doc.total/1.18 as base
//,(doc.total)-doc.total/1.18 as igv,doc.total,doc.op as tipo from documento as doc inner JOIN tipo_comprobante as tc
//on tc.id_comprobante = doc.id_comprobante INNER JOIN persona as p on p.id_persona = doc.id_persona INNER JOIN
//detalle as d on d.id_documento = doc.id_documento where doc.fecha_vencimiento BETWEEN ? and ?  and tc.comprobante='FACTURA'
//GROUP BY doc.id_documento ORDER BY doc.op, doc.fecha_vencimiento ASC;",array($desde,$hasta));

        


// Set document properties
$objPHPExcel->getProperties()->setCreator("vtechnology")
               ->setLastModifiedBy("HERNAN VILCHEZ")
               ->setTitle("Office 2007 XLSX Test Document")
               ->setSubject("Office 2007 XLSX Test Document")
               ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
               ->setKeywords("office 2007 openxml php")
               ->setCategory("Test result file");

$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
                                          ->setSize(10);            

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Documento')
            ->setCellValue('B1', 'Serie/Num')
            ->setCellValue('C1', 'Fecha Emisión')
            ->setCellValue('D1', 'N° Documento')
            ->setCellValue('E1', 'Nombre / Razón social')
            ->setCellValue('F1', 'Vendedor')
            ->setCellValue('G1', 'Tipo de pago')
            ->setCellValue('H1', 'Descripción')
            ->setCellValue('I1', 'Unidad med.')
            ->setCellValue('J1', 'Precio unit.')
            ->setCellValue('K1', 'Cantidad')
            ->setCellValue('L1', 'Total')
            ->setCellValue('M1', 'Moneda')
            ->setCellValue('N1', 'Inc. Igv')
            ->setCellValue('O1', 'Tipo Igv')
            ->setCellValue('P1', 'Estado Local')
            ->setCellValue('Q1', 'Estado Sunat');



            

//$informe = getAllListsAndVideos();
//$i = 2;
//while($row = $informe->fetch_array(MYSQLI_ASSOC))
//{

    $i = 2;
    $total = 0;
    
   foreach ($detallado as $detalle){
       if($detalle['estadolocal'] != 'Anulado'){
           $total += $detalle['total'];
           
       }
            
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A".$i, $detalle['tipo'])
            ->setCellValue("B".$i, $detalle['serien'])
            ->setCellValue("C".$i, $detalle['fechaemision'])
            ->setCellValue("D".$i, $detalle['ruc'])
            ->setCellValue("E".$i, $detalle['razonsocial'])
            ->setCellValue("F".$i, $detalle['vendedor'])
            ->setCellValue("G".$i, $detalle['tipo_pago'])
            ->setCellValue("H".$i, $detalle['descripcionprod']) 
            ->setCellValue("I".$i, $detalle['unidad']) 
            ->setCellValue("J".$i, number_format($detalle['precio'],2))
            ->setCellValue("K".$i, $detalle['cantidad']) 
            ->setCellValue("L".$i, number_format($detalle['total'],2))
            ->setCellValue("M".$i, $detalle['moneda'])
            ->setCellValue("N".$i, $detalle['incigv'])
            ->setCellValue("O".$i, $detalle['impuesto'])
            ->setCellValue("P".$i, $detalle['estadolocal'])
            ->setCellValue("Q".$i, $detalle['estadosunat']);
           
//            ->setCellValue("B".$i, "hol")
//            ->setCellValue("C".$i, "hola")
//            ->setCellValue("D".$i, "hola")
//            ->setCellValue("E".$i, "hola")
//            ->setCellValue("F".$i, "hola")
//            ->setCellValue("G".$i, "hola") 
//            ->setCellValue("H".$i, "hola")
//            ->setCellValue("I".$i, "hola");
           
         
        $i++;
        

}
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$i,'Total: ' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$i,  number_format($total,2));

//$i++;
//}

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);




$objPHPExcel->getActiveSheet()->setTitle('Reporte');

$objPHPExcel->setActiveSheetIndex(0);

getHeaders();

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
 /* Limpiamos el búfer */
ob_end_clean();
$objWriter->save('php://output');
exit;