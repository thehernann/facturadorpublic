<!--<style>
    .ui-autocomplete {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        float: left;
        display: none;
        min-width: 160px;   
        padding: 4px 0;
        margin: 0 0 10px 25px;
        list-style: none;
        background-color: #ffffff;
        border-color: #ccc;
        border-color: rgba(0, 0, 0, 0.2);
        border-style: solid;
        border-width: 1px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        *border-right-width: 2px;
        *border-bottom-width: 2px;
        max-height: 200px;
        max-width:  auto;
        overflow-y: auto;
        overflow-x: auto;

        /* add padding to account for vertical scrollbar */
        padding-right: 20px;

    }

    .ui-menu-item > a.ui-corner-all {
        display: block;
        padding: 3px 15px;
        clear: both;
        font-weight: normal;
        line-height: 18px;
        color: #555555;
        white-space: nowrap;
        text-decoration: none;

    }

    .ui-state-hover, .ui-state-active {
        color: #ffffff;
        text-decoration: none;
        background-color: #0088cc;
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        background-image: none;
        cursor: pointer;
    }

    .ui-helper-hidden-accessible { display:none; }

    /* //////////// quitar padding de los div   */
    /* no-gutters Class Rules */
    .row .no-gutters {
        margin-right: 0;
        margin-left: 0;
    }
    .row .no-gutters > [class^="col-"],
    .row .no-gutters > [class*=" col-"] {
        padding-right: 0;
        padding-left: 0;
    }
</style>-->
<?php 
 
  /** Actual month last day **/
  function _data_last_month_day() { 
      $month = date('m');
      $year = date('Y');
      $day = date("d", mktime(0,0,0, $month+1, 0, $year));
 
      return date('d/m/Y', mktime(0,0,0, $month, $day, $year));
  }
 
  /** Actual month first day **/
  function _data_first_month_day() {
      $month = date('m');
      $year = date('Y');
      return date('d/m/Y', mktime(0,0,0, $month, 1, $year));
  }



?>
<section class="content">
    <!--<div class="container-fluid">-->
        
       
        
        <div class="block-header">
            <!--    <br>-->
            <div class="RespuestaAjax"></div>
        </div>
        <!-- Basic Examples -->

        <!-- #END# Basic Examples -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2 id="titulo">
                           Kardex 
                            

                        </h2>
                       
                    
                    </div>
                    <div class="body">
                        <input type="hidden" value="<?=base_url ?>" id="url" name="url">  
                        
                     <form action="<?= base_url?>producto/searchkardexfisico" method="POST"  id="FormularioAjaxBuscar" data-form="insert" enctype="multipart/form-data" autocomplete="off" >   
                        
                        <div class="row">  
                       <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                                <label class="form-label">Desde </label>
                                <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                <div class="form-group">
                                    <div class="form-line" id="bs_datepicker_container">
                                        <input type="text" class="form-control" placeholder="Desde" id="dpdesde" name="dpdesde" value="<?= _data_first_month_day()?>" required="">
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                                <label class="form-label">Hasta </label>
                                <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                <div class="form-group">
                                    <div class="form-line" id="bs_datepicker_container">
                                        <input type="text" class="form-control" placeholder="Hasta" id="dphasta" name="dphasta" value="<?= _data_last_month_day()?>" required="">
                                    </div>
                                </div>

                            </div>
                        </div>
                            
<!--                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <input id="id" name="id" type="hidden">
                            <div class="form-group form-float">
                                <label class="form-label">Descripción del articulo </label>
                                                                <h2 class="card-inside-title">Aniversario</h2>

                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" id="txtbuscar" name="txtbuscar">
                                    </div>
                                </div>

                            </div>
                        </div>-->
                        
<!--                        </div>
                        <div class="row">-->
                            
                            
                            
                            
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                            
                                <label>Almacen</label>
                                <select class="form-control show-tick" id="cbsucursal" name="cbsucursal">
                                    
                                    <?php 
                                   
                                                                        
                                        foreach ($sucursales as $sucursal){
                                            if($_SESSION['idalmacen'] == $sucursal->getId()){
                                                echo '<option value="'.$sucursal->getId().'" selected="selected" >'.$sucursal->getNombre().' - '.$sucursal->getDireccion().'</option>';
                                                
                                            }else{
                                                echo '<option value="'.$sucursal->getId().'" >'.$sucursal->getNombre().' - '.$sucursal->getDireccion().'</option>';
                                                
                                            }
                                            


                                    }

                                    ?>

                            </select>
                            </div>
                        </div>
                            
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                            
                                <label>Kardex</label>
                                <select class="form-control show-tick" id="cbkardex" name="cbkardex">
                                    
                                    <?php 
                                    
                                    $op = array('Físico','Valorizado');
                                    $value = array('fisico','valorizado');
                                                                        
                                        for ($i =0 ; $i < count($value); $i++){
                                            
                                            echo '<option value="'.$value[$i].'" >'.$op[$i].'</option>';
         
                                    }

                                    ?>

                            </select>
                            </div>
                        </div>
                      
                        
                        </div>
                         <div class="row">
                         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <label>Acciones</label>
                            <div class="form-group form-float">
                                
                                <button class="btn btn-sm btn-primary waves-effect" type="submit"><span class="glyphicon glyphicon-search "></span> Buscar</button>
                                
                         
                                
                                <button class="btn btn-sm btn-success waves-effect" id="excelkardex" url="<?= base_url?>producto/kardexfisicoexcel"><span class="glyphicon glyphicon-file"></span> Excel</button>
                                
                            </div>
                        </div>
                             
                         </div>
                            
                            
                            
                            
                        
                     </form>
                        
                        
                        
                        
                        <div id="respuestaExcel"></div>
                        <div class="table-responsive" id="respuestaAjax">
                            <table class="table  table-hover table-bordered" id="tabladocumento">
                                <thead>
                                    <tr>
                                         <th>Operación</th>
                                     
                                        <th>Fecha</th>

                                        <th>Observación</th>
                                        <th>Descripción</th>
                                        <th>Cantidad</th>
                                        <th>Stock anterior</th>
                                        <th>Saldo</th>
                                       
                                    </tr>
                                </thead>
<!--                                <tfoot>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Tipo</th>
                                        <th>Serie</th>
                                        <th>Número</th>
                                        <th>RUC/ DNI</th>
                                        <th>Nombre / Rz. Social</th>
                                        <th>Total</th>
                                        <th>Est. Local</th>
                                        <th>Est. Sunat</th>
                                        <th>Imprimir</th>
                                        <th>Acciones</th>
                                    </tr>
                                </tfoot>-->
                                <tbody >
                                    <?php 
//                                    $se = 0;
//                         
                                    foreach ($fisico as $detalle){
                                        echo '<tr>';
                                        echo '<td>'.$detalle['concepto'].'</td>';
                                        echo '<td>'.$detalle['created_at'].'</td>';
                                        echo '<td>'.$detalle['observacion'].'</td>';
                                        echo '<td>'.$detalle['descripcion'].'</td>';
                                        echo '<td>'.$detalle['cantidad'].'</td>';
                                        echo '<td>'.$detalle['stock_anterior'].'</td>';
                                        echo '<td>'.$detalle['stock_actual'].'</td>';
                                        
                                        echo '</tr>';
                                        
                                        
//                                        if($detalle['operacion'] == 'Entrada'){
//                                            $se += $detalle['cantidad'];
//                                            
//                                        }else {
//                                            $se -= $detalle['cantidad'];
//                                            
//                                        }
//                                            echo '<tr>';
//                                            
//                                            echo '<td>'.$detalle['operacion'].'</td>';
//                                            echo '<td>'.$detalle['fechaemision'].'</td>';
//                                            echo '<td>'.$detalle['tipo'].'</td>';
//                                            echo '<td>'.$detalle['documento'].'</td>';
//                                           
//                                            echo '<td>'.$detalle['cantidad'].'</td>';
//                                            echo '<td>'.$se.'</td>';
//                                            
//                                            echo '</tr>';
//                                        
//                                       
//                                        
                                    } 
  
                                    ?>



                                </tbody>
                                
                               
                            </table>
                            <div class="pagination">
                                <nav>
                                    <ul class="pagination"></ul>
                                    
                                </nav>
                                
                            </div>
                            
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table --> 
    </div>
</section>

<script>

   var table = '#tabladocumento'
   $(document).ready( function (){   
     
       $('.pagination').html('')
       var trnum =0
       var maxRows = 20
       var totalRows = $(table+' tbody tr').length
       $(table+' tr:gt(0)').each( function (){
           trnum++
           if(trnum > maxRows){
               $(this).hide()
           }
           if(trnum <= maxRows){
               $(this).show()
               
           }
           
           
           
       })
       if(totalRows > maxRows){
           var pagenum = Math.ceil(totalRows/maxRows)
           for(var i=1; i<= pagenum;){
               $('.pagination').append('<li data-page="'+i+'">\<span>'+ i++ +'<span class="sr-only">(current)</span>\n\
                </span>\</li>').show()
               
           }
           
       }
       $('.pagination li:first-child').addClass('active')
       $('.pagination li').on('click', function (){
       
        var pageNum = $(this).attr('data-page')
        var trIndex = 0;
        $('.pagination li').removeClass('active')
        $(this).addClass('active')
        $(table+' tr:gt(0)').each(function (){
            trIndex++
            if(trIndex > (maxRows*pageNum) || trIndex <= ((maxRows*pageNum)- maxRows)){
                $(this).hide()
            }else {
                $(this).show()
            }
                
            })
            
        });
       
       
       });
       
//       $(function (){
//        $('table tr:eq(0)').prepend('<th>ID</th>')
//        var id=0;
//        $('table tr:gt(0)').each(function (){
//            id++
//            $(this).prepend('<td>'+id+'</td>')
//            
//        })
//       
//       
//       
//       
//       })
       
       
//    $(document).on("keydown.autocompleteselect", "#txtbuscar", function () {
//                $(this).autocomplete({
//                    minLength: 2,
//                    source:
//                            function (request, response) {
//                                $.ajax({
//                                    type: 'POST',
//                                    url: '<?= base_url . 'producto/search' ?>',
//                                    data: {query: request.term},
//                                    success: response,
//                                    error: function (jqXHR, textStatus, errorThrown) {
//                                        alert("error handler!");
//                                        alert(errorThrown);
//                                    },
//                                    dataType: 'json',
//                                    delay: 100
//                                });
// 
//                            },
//
//                    select: function (event, ui) {
//                       
//                        $('#id').val(ui.item.id);
//                        $('#buscar').val(ui.item.descripcion);
//                        $('#titulo').html(' [ '+ui.item.descripcion+' - '+ui.item.codigo+' ]');
//
//                       
////                        event.preventDefault();
//
//                    }
//                });
//
//    //            event.preventDefault();
//            });
//       
        
    
    
  
</script>    




















