<?php 


//require_once ($_SERVER['DOCUMENT_ROOT']."/aquavita/vendor/PHPExcel/Classes/PHPExcel.php");


$objPHPExcel = new PHPExcel();

//$desde = $_POST['desde'];
//$hasta = $_POST['hasta'];
//
//
//require_once ("../../conexion/datasource.php");
////require_once ($_SERVER['DOCUMENT_ROOT']."/aquavita/conexion/datasource.php");
//
//$data_source = new DataSource();
//$data_tabla = $data_source->ejecutarconsulta("select doc.fecha_vencimiento,tc.op,doc.serie,doc.numero,p.rucdni,concat(p.nombre,' ',p.apellidos) as nombre,SUM(d.cantidad)as cant,doc.total/1.18 as base
//,(doc.total)-doc.total/1.18 as igv,doc.total,doc.op as tipo from documento as doc inner JOIN tipo_comprobante as tc
//on tc.id_comprobante = doc.id_comprobante INNER JOIN persona as p on p.id_persona = doc.id_persona INNER JOIN
//detalle as d on d.id_documento = doc.id_documento where doc.fecha_vencimiento BETWEEN ? and ?  and tc.comprobante='FACTURA'
//GROUP BY doc.id_documento ORDER BY doc.op, doc.fecha_vencimiento ASC;",array($desde,$hasta));

        


// Set document properties
$objPHPExcel->getProperties()->setCreator("vtechnology")
               ->setLastModifiedBy("HERNAN VILCHEZ")
               ->setTitle("Office 2007 XLSX Test Document")
               ->setSubject("Office 2007 XLSX Test Document")
               ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
               ->setKeywords("office 2007 openxml php")
               ->setCategory("Test result file");

$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
                                          ->setSize(10);  

if($kardex =='fisico'){
    $objPHPExcel->setActiveSheetIndex(0)
            
            ->setCellValue('A1', 'Operación')
            ->setCellValue('B1', 'Fecha')
            ->setCellValue('C1', 'Observación')
            ->setCellValue('D1', 'Descripción')
            ->setCellValue('E1', 'Cantidad')
            ->setCellValue('F1', 'Stock anterior')
            ->setCellValue('G1', 'Saldo');
                
}else {
    $objPHPExcel->setActiveSheetIndex(0)
            
            ->setCellValue('A1', 'Operación')
            ->setCellValue('B1', 'Fecha')
            ->setCellValue('C1', 'Observación')
            ->setCellValue('D1', 'Descripción')
            ->setCellValue('E1', 'Cantidad')
            ->setCellValue('F1', 'Precio Unit')
          
            ->setCellValue('G1', 'Moneda')
            ->setCellValue('H1', 'Stock anterior')
            ->setCellValue('I1', 'Saldo')
            ->setCellValue('J1', '#');
    
}

   

//$informe = getAllListsAndVideos();
//$i = 2;
//while($row = $informe->fetch_array(MYSQLI_ASSOC))
//{

    $i = 2;
   
   foreach ($fisico as $detalle){
    
       
     if($kardex =='fisico'){
         $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A".$i, $detalle['concepto'])
            ->setCellValue("B".$i, $detalle['created_at'])
            ->setCellValue("C".$i, $detalle['observacion'])
            ->setCellValue("D".$i, $detalle['descripcion'])
            ->setCellValue("E".$i, $detalle['cantidad'])
            ->setCellValue("F".$i, $detalle['stock_anterior'])
            ->setCellValue("G".$i, $detalle['stock_actual']);
         
         
         
         

     }else {
       
         $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A".$i, $detalle['concepto'])
            ->setCellValue("B".$i, $detalle['created_at'])
            ->setCellValue("C".$i, $detalle['observacion'])
            ->setCellValue("D".$i, $detalle['descripcion'])
            ->setCellValue("E".$i, $detalle['cantidad'])
            ->setCellValue("F".$i, $detalle['precio'])
            ->setCellValue("G".$i, $detalle['moneda '])
            ->setCellValue("H".$i, $detalle['stock_anterior'])
            ->setCellValue("I".$i, $detalle['stock_actual'])
            ->setCellValue("J".$i, number_format($detalle['cantidad'] * $detalle['precio'],2));
      
     }
    
    
    
       
        $i++;
        

}


//$i++;
//}

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);


$objPHPExcel->getActiveSheet()->setTitle('Reporte');

$objPHPExcel->setActiveSheetIndex(0);

getHeaders();

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
 /* Limpiamos el búfer */
ob_end_clean();
$objWriter->save('php://output');
exit;