

<div class="menu navbar-collapse"  > <!--  -->
    <ul class="list" >
        <li class="header">Menu</li>
        <li class="active">
            <a href="<?= base_url ?>dashboard/index">
                <i class="material-icons">home</i>
                <span>Inicio</span>
            </a>
        </li>
<!--        <li>
            <a href="pages/typography.html">
                <i class="material-icons">text_fields</i>
                <span>Typography</span>
            </a>
        </li>-->
<!--        <li>
            <a href="pages/helper-classes.html">
                <i class="material-icons">layers</i>
                <span>Helper Classes</span>
            </a>
        </li>-->
        <?php if(permisos::rol('rol-panel')){ ?>
        <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">widgets</i>
                <span>Panel </span>
                
            </a>
            <ul class="ml-menu">
                <li >
                    <a href="<?=base_url?>empresa/select" >Empresas</a>
                </li>
                
            </ul>
        </li>
        <?php } ?>
            
        <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">widgets</i>
                <span>Administración</span>
                
            </a>
            <ul class="ml-menu">
                
                <?php if( permisos::rol('rol-bancolistar')){?>
                <li >
                    <a href="<?=base_url?>bancos/select" >Bancos</a>
                </li>
                <?php }?>
                <?php if( permisos::rol('rol-cuentabancalistar')){?>
                <li >
                    <a href="<?=base_url?>cuentabancaria/select" >Cuentas Bancarias</a>
                </li>
                <?php }?>
                
                <?php if( permisos::rol('rol-clientelistar')){?>
                <li >
                    <a href="<?=base_url?>persona/selectclient" >Cliente</a>
                </li>
                <?php }?>
                
                <?php if( permisos::rol('rol-proveedorlistar')){?>
                <li>
                    <a href="<?=base_url?>persona/selectprovee">Proveedor</a>
                </li>
                <?php }?>
                
                <?php if( permisos::rol('rol-marcalistar')){?>
                <li>
                    <a href="<?=base_url?>marca/select">Marca</a>
                </li>
                
                <?php }?>
                
                <?php if( permisos::rol('rol-unidlistar')){?>
                <li>
                    <a href="<?=base_url?>medida/select">Unid. Medida</a>
                </li>
                
                <?php }?>
                
                <?php if( permisos::rol('rol-categorialistar')){?>
                <li>
                    <a href="<?=base_url?>categoria/select">Categoria</a>
                </li>
                <?php }?>
                
                <?php if( permisos::rol('rol-linealistar')){?>
                <li>
                    <a href="<?=base_url?>linea/select">Linea</a>
                </li>
                <?php }?>
                
                 <?php if( permisos::rol('rol-articulolistar')){?>
                <li>
                    <a href="<?=base_url?>producto/selectprod">Articulos</a>
                </li>
                <?php }?>
                
                 <?php if( permisos::rol('rol-serviciolistar')){?>
                <li>
                    <a href="<?=base_url?>producto/selectserv">Servicios</a>
                </li>
                <?php }?>
                
                <?php if( permisos::rol('rol-usuariolistar')){?>
                <li>
                    <a href="<?=base_url?>usuario/select">Usuarios</a>
                </li>
                
                <?php }?>
                <?php if( permisos::rol('rol-permisos')){?>
                <li>
                    <a href="<?=base_url?>nivel/select"><div class="demo-google-material-icon"> <i class="material-icons">lock_open</i> <span class="icon-name">Nivel</span> </div></a>
                </li>
                <?php }?>
                 <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <div class="demo-google-material-icon"> <i class="material-icons">business</i> <span class="icon-name">Gestión</span> </div>
                    </a>
                    <ul class="ml-menu">
                        <?php // if( permisos::rol('rol-sucursalprincipalmostrar')){?>
<!--                        <li>
                            <a href="<?=base_url?>sucursal/main">Principal</a>
                        </li>-->
                        <?php // }?>
                        
                        
                        <?php if( permisos::rol('rol-sucursallistar')){?>
                        <li>
                            <a href="<?=base_url?>sucursal/select">Sucursales</a>
                        </li>
                        <?php }?>
                        <?php if( permisos::rol('rol-sucursallistar')){?>
                        <li>
                            <a href="<?=base_url?>almacen/select">Almacenes</a>
                        </li>
                        <?php }?>
                        <?php if( permisos::rol('rol-documentolistar')){?>
                         <li>
                            <a href="<?=base_url?>sucursaldocumento/select">Documentos</a>
                        </li>
                         <?php }?>
                        
                    </ul>
                 </li>
                

            </ul>
        </li>
          <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">widgets</i>
                <span>Ventas</span>
                
            </a>
            <ul class="ml-menu">
                <?php if( permisos::rol('rol-vercomprobanteventa') || permisos::rol('rol-vercomprobanteventaporsucur')){?>
                <li >
                    <a href="<?=base_url?>documento/selectdocument" >Ver Comprobantes</a>
                </li>
                 <?php }?>
                
                <?php if( permisos::rol('rol-boletafacturaventa')){?>
                <li >
                    <a href="<?=base_url?>documento/sale" >Emitir Boleta / Factura</a>
                </li>
                <?php }?>
                <?php if( permisos::rol('rol-notacreditoventa')){?>
                <li >
                    <a href="<?=base_url?>documento/notecredit" >Emitir Nota de Crédito</a>
                </li>
                <?php }?>
                
                <?php if( permisos::rol('rol-notadebitoventa')){?>
                <li >
                    <a href="<?=base_url?>documento/notedebit" >Emitir Nota de Débito</a>
                </li>
                <?php }?>
                <?php if( permisos::rol('rol-vercotizacionesventa') || permisos::rol('rol-vercotizacionesventasucur')){?>
                 <li >
                    <a href="<?=base_url?>documento/selectcotizacion" >Cotización</a>
                </li>
                <?php }?>

            </ul>
        </li>
        
            <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">widgets</i>
                <span>Compras</span>
                
            </a>
            <ul class="ml-menu">
<!--                <li >
                    <a href="<?=base_url?>documento/ordencompra" >Ver comprobantes</a>
                </li>-->
                <?php if( permisos::rol('rol-vercomprobantecompra') || permisos::rol('rol-vercomprobantecomprasucur') ){?>
                <li >
                    <a href="<?=base_url?>documento/selectcompra" >Ver comprobantes</a>
                </li>
                <?php }?>
                <?php if( permisos::rol('rol-compra')){?>
                <li >
                    <a href="<?=base_url?>documento/compra" >Compra</a>
                </li>
                <?php }?>
                <?php if( permisos::rol('rol-vercomprobanteordencompra') ||  permisos::rol('rol-vercomprobanteordencomprasucur')){?>
                <li >
                    <a href="<?=base_url?>documento/selectordencompra" >Orden de compra</a>
                </li>
                <?php }?>
            </ul>
        </li>
            <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">widgets</i>
                <span>Reportes</span>
                
            </a>
            <ul class="ml-menu">
<!--                <li >
                    <a href="<?=base_url?>documento/ordencompra" >Ver comprobantes</a>
                </li>-->
                <?php if( permisos::rol('rol-ventadetallereporte') ||  permisos::rol('rol-ventadetallereportesucur')){?>
                <li >
                    <a href="<?=base_url?>documento/selectdetallado" >Venta por detalle</a>
                </li>
                 <?php }?>
                
                <?php if( permisos::rol('rol-compradetallereporte') ||  permisos::rol('rol-compradetallereportesucur')){?>
                <li >
                    <a href="<?=base_url?>documento/selectdetalladocompra" >Compra por detalle</a>
                </li>
                <?php }?>
                 <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <div class="demo-google-material-icon"> <i class="material-icons">style</i> <span class="icon-name">Stock</span> </div>
                    </a>
                    <ul class="ml-menu">
                        <?php if( permisos::rol('rol-generalreporte') ||  permisos::rol('rol-generalreportesucur')){?>
                        <li>
                            <a href="<?=base_url?>producto/selectstockgeneral">General</a>
                        </li>
                        
                        <?php }?>
                        
                        <?php if( permisos::rol('rol-valorizadoreporte') ||  permisos::rol('rol-valorizadoreportesucur')){?>
                        <li>
                            <a href="<?=base_url?>producto/selectstockvalorizado">Valorizado</a>
                        </li>
                        <?php }?>
                        <?php if( permisos::rol('rol-compraventareporte') ||  permisos::rol('rol-compraventareportesucur')){?>
                         <li>
                            <a href="<?=base_url?>producto/selectstockcompraventa">Compra - Venta</a>
                        </li>
                        <?php }?>
                        
                    </ul>
                 </li>
                 <?php if( permisos::rol('rol-kardexreporte') ||  permisos::rol('rol-kardexreportesucur')){?>
                 <li>
                        <a href="<?=base_url?>producto/kardex" >Kardex </a>
                 </li>
                 <?php }?>
                 
                 <?php if( permisos::rol('rol-cuentascobrarreporte') ||  permisos::rol('rol-cuentascobrarreportesucur')){?>
                 <li>
                        <a href="<?=base_url?>documento/cuentacobrar" >Cuentas por cobrar </a>
                 </li>
                 <?php }?>
                 <?php if( permisos::rol('rol-cuentaspagarreporte') ||  permisos::rol('rol-cuentaspagarreportesucur')){?>
                 <li>
                        <a href="<?=base_url?>documento/cuentapagar" >Cuentas por pagar </a>
                 </li>
                 <?php }?>
              
                 
                
                

            </ul>
        </li>
            <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">widgets</i>
                <span>Operaciones</span>
                
            </a>
            <ul class="ml-menu">
<!--                <li >
                    <a href="<?=base_url?>documento/ordencompra" >Ver comprobantes</a>
                </li>-->
                <?php // if( permisos::rol('rol-gastosoperaciones') ||  permisos::rol('rol-gastosoperacionessucur')){?>
<!--                <li >
                    <a href="//<?=base_url?>gastos/select" >Gastos</a>
                </li>-->
                <?php // }?>
                
                <?php if( permisos::rol('rol-movimientocajaoperaciones')){?>
                <li >
                    <a href="<?=base_url?>caja/report" >Reporte de Caja</a>
                </li>  
                <li >
                    <a href="<?=base_url?>caja/select" >Movimiento de Caja</a>
                </li>  
                <?php }?>
                
                <?php if( permisos::rol('rol-trasladooperaciones')){?>
                <li >
                    <a href="<?=base_url?>documento/selectguiatraslado" >Traslado de Articulos</a>
                </li>   
                <?php }?>
               
         
            </ul>
        </li>
        
       
        

    </ul>
</div>