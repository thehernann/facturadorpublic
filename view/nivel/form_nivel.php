
<style>
    .titulocheck{
        font-size: 16px !important;
        color: #2196F3;
       
        
        font-weight: bold !important;
    }
    ul{
        margin-left: 2% !important;
    }
</style>

<section class="content">
    <!--<div class="container-fluid">-->
        
<div class="block-header">
    <!--<h2>FORM EXAMPLES</h2>-->
    <!--    <div class="RespuestaAjax"></div>-->
</div>
<!-- Inline Layout | With Floating Label -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                Nuevo Nivel / Permisos
                    <!--<small>Edición</small>-->
                </h2>


            </div>
            <div class="body">
                <form action="<?= base_url ?>nivel/insert" method="POST"  id="FormularioAjax" data-form=" " enctype="multipart/form-data" autocomplete="off" >
                    <div class="row clearfix">
                        <!--<form >-->
                        <div class="row">
                            <input type="hidden" value="" id="id" name="id">
                            
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">Descripción (*)</label>
                                    <div class="form-line ">
                                        <input type="text" class="form-control" id="txtdescripcion" name="txtdescripcion" value="" required="">
                                        
                                    </div>
                                </div>
                            </div>
                            </div>
                        <div class="row " >
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                
                                
                                <div class="clearfix m-b-20">
                                <div class="">
                                    <ul class="checktree">
                                        <li >
                                            <input type="checkbox" id="rol-privilegiosall" name="rol-privilegiosall" /> <!-- checked -->
                                            <label for="rol-privilegiosall" class="titulocheck" > Nivel</label>
                                            <ul class="dd-list">
                                                <li class="dd-item">
                                                    <input type="checkbox" id="rol-permisos" name="rol-permisos" /> <!-- checked -->
                                                    <label for="rol-permisos"> Listar / Mostrar</label>
                                                </li>
                                           
                                            </ul>
                                        </li>
                                        <li >
                                             <input type="checkbox" id="rol-bancoall" name="rol-bancoall" /> <!-- checked -->
                                                    <label for="rol-bancoall" class="titulocheck"> Bancos</label>
                                            <ul class="dd-list">
                                                <li class="dd-item" data-id="3">
                                                    <input type="checkbox" id="rol-bancoeditar" name="rol-bancoeditar" /> <!-- checked -->
                                                    <label for="rol-bancoeditar"> Editar</label>
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <input type="checkbox" id="rol-bancoeliminar" name="rol-bancoeliminar" /> <!-- checked -->
                                                    <label for="rol-bancoeliminar">Eliminar</label>
                                                </li>
                                                
                                                <li class="dd-item" data-id="9">
                                                    <input type="checkbox" id="rol-bancolistar" name="rol-bancolistar" /> <!-- checked -->
                                                    <label for="rol-bancolistar">Listar / Mostrar</label>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <input type="checkbox" id="rol-banconuevo" name="rol-banconuevo" /> <!-- checked -->
                                                <label for="rol-banconuevo">Nuevo</label>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                           <input type="checkbox" id="rol-cuentabancaall" name="rol-cuentabancaall" /> <!-- checked -->
                                            <label for="rol-cuentabancaall" class="titulocheck"> Cuenta Bancaria</label>
                                             <ul class="dd-list">
                                                <li class="dd-item" data-id="3">
                                                    <div class=""><input type="checkbox" id="rol-cuentabancaeditar" name="rol-cuentabancaeditar" /> <!-- checked -->
                                                    <label for="rol-cuentabancaeditar">Editar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <div class=""><input type="checkbox" id="rol-cuentabancaeliminar" name="rol-cuentabancaeliminar" /> <!-- checked -->
                                                <label for="rol-cuentabancaeliminar">Eliminar</label></div>
                                                </li>
                                                
                                                <li class="dd-item" data-id="9">
                                                    <div class=""><input type="checkbox" id="rol-cuentabancalistar" name="rol-cuentabancalistar" /> <!-- checked -->
                                                <label for="rol-cuentabancalistar">Listar / Mostrar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <div class=""><input type="checkbox" id="rol-cuentabancanuevo" name="rol-cuentabancanuevo" /> <!-- checked -->
                                                <label for="rol-cuentabancanuevo">Nuevo</label></div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li >
                                            <input type="checkbox" id="rol-clienteall" name="rol-clienteall" /> <!-- checked -->
                                                    <label for="rol-clienteall" class="titulocheck">Cliente</label>
                                            <ul class="dd-list">
                                                <li class="dd-item" data-id="3">
                                                    <div class=""><input type="checkbox" id="rol-clienteeditar" name="rol-clienteeditar" /> <!-- checked -->
                                                    <label for="rol-clienteeditar">Editar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <div class=""><input type="checkbox" id="rol-clienteeliminar" name="rol-clienteeliminar" /> <!-- checked -->
                                                <label for="rol-clienteeliminar">Eliminar</label></div>
                                                </li>
                                                
                                                <li class="dd-item" data-id="9">
                                                    <div class=""><input type="checkbox" id="rol-clientelistar" name="rol-clientelistar" /> <!-- checked -->
                                                <label for="rol-clientelistar">Listar / Mostrar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <div class=""><input type="checkbox" id="rol-clientenuevo" name="rol-clientenuevo" /> <!-- checked -->
                                                <label for="rol-clientenuevo">Nuevo</label></div>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <div class=""><input type="checkbox" id="rol-clientedefecto" name="rol-clientedefecto" /> <!-- checked -->
                                                <label for="rol-clientedefecto">Por defecto</label></div>
                                                </li>
                                            </ul>
                                        </li>
                                             <li >
                                             <input type="checkbox" id="rol-proveedorall" name="rol-proveedorall" /> <!-- checked -->
                                        <label for="rol-proveedorall" class="titulocheck"> Proveedor</label> 
                                            <ul class="dd-list">
                                                <li class="dd-item" data-id="3">
                                                    <div class=""><input type="checkbox" id="rol-proveedoreditar" name="rol-proveedoreditar" /> <!-- checked -->
                                                    <label for="rol-proveedoreditar">Editar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <div class=""><input type="checkbox" id="rol-proveedoreliminar" name="rol-proveedoreliminar" /> <!-- checked -->
                                                <label for="rol-proveedoreliminar">Eliminar</label></div>
                                                </li>
                                                
                                                <li class="dd-item" data-id="9">
                                                    <div class=""><input type="checkbox" id="rol-proveedorlistar" name="rol-proveedorlistar" /> <!-- checked -->
                                                <label for="rol-proveedorlistar">Listar / Mostrar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <div class=""><input type="checkbox" id="rol-proveedornuevo" name="rol-proveedornuevo" /> <!-- checked -->
                                                <label for="rol-proveedornuevo">Nuevo</label></div>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <div class=""><input type="checkbox" id="rol-proveedordefecto" name="rol-proveedordefecto" /> <!-- checked -->
                                                <label for="rol-proveedordefecto">Por defecto</label></div>
                                                </li>
                                            </ul>
                                        </li>
                                             <li >
                                            <input type="checkbox" id="rol-marcaall" name="rol-marcaall" /> <!-- checked -->
                                                        <label for="rol-marcaall"class="titulocheck">Marca</label>
                                                <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-marcaeditar" name="rol-marcaeditar" /> <!-- checked -->
                                                        <label for="rol-marcaeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-marcaeliminar" name="rol-marcaeliminar" /> <!-- checked -->
                                                    <label for="rol-marcaeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-marcalistar" name="rol-marcalistar" /> <!-- checked -->
                                                    <label for="rol-marcalistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-marcanuevo" name="rol-marcanuevo" /> <!-- checked -->
                                                    <label for="rol-marcanuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-unidall" name="rol-unidall" /> <!-- checked -->
                                                        <label for="rol-unidall" class="titulocheck">Unid. Medida</label>
                                           <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-unideditar" name="rol-unideditar" /> <!-- checked -->
                                                        <label for="rol-unideditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-unideliminar" name="rol-unideliminar" /> <!-- checked -->
                                                    <label for="rol-unideliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-unidlistar" name="rol-unidlistar" /> <!-- checked -->
                                                    <label for="rol-unidlistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-unidnuevo" name="rol-unidnuevo" /> <!-- checked -->
                                                    <label for="rol-unidnuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                        
                                        <li >
                                            <input type="checkbox" id="rol-categoriaall" name="rol-categoriaall" /> <!-- checked -->
                                                        <label for="rol-categoriaall"class="titulocheck">Categoria</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-categoriaeditar" name="rol-categoriaeditar" /> <!-- checked -->
                                                        <label for="rol-categoriaeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-categoriaeliminar" name="rol-categoriaeliminar" /> <!-- checked -->
                                                    <label for="rol-categoriaeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-categorialistar" name="rol-categorialistar" /> <!-- checked -->
                                                    <label for="rol-categorialistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-categorianuevo" name="rol-categorianuevo" /> <!-- checked -->
                                                    <label for="rol-categorianuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li>
                                        <input type="checkbox" id="rol-lineaall" name="rol-lineaall" /> <!-- checked -->
                                                        <label for="rol-lineaall" class="titulocheck">Linea</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-lineaeditar" name="rol-lineaeditar" /> <!-- checked -->
                                                        <label for="rol-lineaeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-lineaeliminar" name="rol-lineaeliminar" /> <!-- checked -->
                                                    <label for="rol-lineaeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-linealistar" name="rol-linealistar" /> <!-- checked -->
                                                    <label for="rol-linealistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-lineanuevo" name="rol-lineanuevo" /> <!-- checked -->
                                                    <label for="rol-lineanuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-articuloall" name="rol-articuloall" /> <!-- checked -->
                                                        <label for="rol-articuloall" class="titulocheck">Articulos</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-articuloeditar" name="rol-articuloeditar" /> <!-- checked -->
                                                        <label for="rol-articuloeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-articuloeliminar" name="rol-articuloeliminar" /> <!-- checked -->
                                                    <label for="rol-articuloeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-articulolistar" name="rol-articulolistar" /> <!-- checked -->
                                                    <label for="rol-articulolistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-articulonuevo" name="rol-articulonuevo" /> <!-- checked -->
                                                    <label for="rol-articulonuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-servicioall" name="rol-servicioall" /> <!-- checked -->
                                                        <label for="rol-servicioall" class="titulocheck">Servicios</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-servicioeditar" name="rol-servicioeditar" /> <!-- checked -->
                                                        <label for="rol-servicioeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-servicioeliminar" name="rol-servicioeliminar" /> <!-- checked -->
                                                    <label for="rol-servicioeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-serviciolistar" name="rol-serviciolistar" /> <!-- checked -->
                                                    <label for="rol-serviciolistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-servicionuevo" name="rol-servicionuevo" /> <!-- checked -->
                                                    <label for="rol-servicionuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-usuarioall" name="rol-usuarioall" /> <!-- checked -->
                                                        <label for="rol-usuarioall" class="titulocheck">Usuarios</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-usuarioeditar" name="rol-usuarioeditar" /> <!-- checked -->
                                                        <label for="rol-usuarioeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-usuarioeliminar" name="rol-usuarioeliminar" /> <!-- checked -->
                                                    <label for="rol-usuarioeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-usuariolistar" name="rol-usuariolistar" /> <!-- checked -->
                                                    <label for="rol-usuariolistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-usuarionuevo" name="rol-usuarionuevo" /> <!-- checked -->
                                                    <label for="rol-usuarionuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
<!--                                         <li >
                                        <input type="checkbox" id="rol-sucursalpall" name="rol-sucursalpall" />  checked 
                                                        <label for="rol-sucursalpall" class="titulocheck">Sucursal Principal</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-sucursalprincipaleditar" name="rol-sucursalprincipaleditar" />  checked 
                                                        <label for="rol-sucursalprincipaleditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-sucursalprincipalmostrar" name="rol-sucursalprincipalmostrar" />  checked 
                                                    <label for="rol-sucursalprincipalmostrar">Mostrar</label></div>
                                                    </li>

                                                </ul>
                                        </li>-->
                                         <li >
                                        <input type="checkbox" id="rol-sucursalall" name="rol-sucursalall" /> <!-- checked -->
                                                        <label for="rol-sucursalall" class="titulocheck">Sucursales</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-sucursaleditar" name="rol-sucursaleditar" /> <!-- checked -->
                                                        <label for="rol-sucursaleditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-sucursaleliminar" name="rol-sucursaleliminar" /> <!-- checked -->
                                                    <label for="rol-sucursaleliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-sucursallistar" name="rol-sucursallistar" /> <!-- checked -->
                                                    <label for="rol-sucursallistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-sucursalnuevo" name="rol-sucursalnuevo" /> <!-- checked -->
                                                    <label for="rol-sucursalnuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-documentoall" name="rol-documentoall" /> <!-- checked -->
                                                        <label for="rol-documentoall" class="titulocheck">Documentos</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-documentoeditar" name="rol-documentoeditar" /> <!-- checked -->
                                                        <label for="rol-documentoeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-documentoeliminar" name="rol-documentoeliminar" /> <!-- checked -->
                                                    <label for="rol-documentoeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-documentolistar" name="rol-documentolistar" /> <!-- checked -->
                                                    <label for="rol-documentolistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-documentonuevo" name="rol-documentonuevo" /> <!-- checked -->
                                                    <label for="rol-documentonuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-ventaall" name="rol-ventaall" /> <!-- checked -->
                                                        <label for="rol-ventaall" class="titulocheck"> Ventas</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobanteventa" name="rol-vercomprobanteventa" /> <!-- checked -->
                                                        <label for="rol-vercomprobanteventa">Ver comprobantes</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobanteventaporsucur" name="rol-vercomprobanteventaporsucur" /> <!-- checked -->
                                                        <label for="rol-vercomprobanteventaporsucur">Ver comprobantes por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-boletafacturaventa" name="rol-boletafacturaventa" /> <!-- checked -->
                                                    <label for="rol-boletafacturaventa">Emitir boleta/factura</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-notacreditoventa" name="rol-notacreditoventa" /> <!-- checked -->
                                                    <label for="rol-notacreditoventa">Emitir nota de crédito</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-notadebitoventa" name="rol-notadebitoventa" /> <!-- checked -->
                                                    <label for="rol-notadebitoventa">Emitir nota de débito</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-cotizacionventa" name="rol-cotizacionventa" /> <!-- checked -->
                                                    <label for="rol-cotizacionventa">Cotización</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-vercotizacionesventa" name="rol-vercotizacionesventa" /> <!-- checked -->
                                                    <label for="rol-vercotizacionesventa">Ver cotizaciones</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-vercotizacionesventasucur" name="rol-vercotizacionesventasucur" /> <!-- checked -->
                                                    <label for="rol-vercotizacionesventasucur">Ver cotizaciones por sucursal</label></div>
                                                    </li>
                                                    
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-editarprecio" name="rol-editarprecio" /> <!-- checked -->
                                                    <label for="rol-editarprecio">Editar precios</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                            <input type="checkbox" id="rol-compraall" name="rol-compraall" /> <!-- checked -->
                                                        <label for="rol-compraall" class="titulocheck">Compras</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobantecompra" name="rol-vercomprobantecompra" /> <!-- checked -->
                                                        <label for="rol-vercomprobantecompra">Ver comprobantes</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobantecomprasucur" name="rol-vercomprobantecomprasucur" /> <!-- checked -->
                                                        <label for="rol-vercomprobantecomprasucur">Ver comprobantes por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-compra" name="rol-compra" /> <!-- checked -->
                                                    <label for="rol-compra">Compra</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobanteordencompra" name="rol-vercomprobanteordencompra" /> <!-- checked -->
                                                    <label for="rol-vercomprobanteordencompra">Ver comprobantes orden compra</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobanteordencomprasucur" name="rol-vercomprobanteordencomprasucur" /> <!-- checked -->
                                                    <label for="rol-vercomprobanteordencomprasucur">Ver comprobantes orden compra por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-ordencompra" name="rol-ordencompra" /> <!-- checked -->
                                                    <label for="rol-ordencompra">Emitir orden compra</label></div>
                                                    </li>
                                                    
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-reportesall" name="rol-reportesall" /> <!-- checked -->
                                                        <label for="rol-reportesall" class="titulocheck">Reportes</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-ventadetallereporte" name="rol-ventadetallereporte" /> <!-- checked -->
                                                        <label for="rol-ventadetallereporte">Venta por detalle</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-ventadetallereportesucur" name="rol-ventadetallereportesucur" /> <!-- checked -->
                                                        <label for="rol-ventadetallereportesucur">Venta por detalle por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-compradetallereporte" name="rol-compradetallereporte" /> <!-- checked -->
                                                    <label for="rol-compradetallereporte">Compra por detalle</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-compradetallereportesucur" name="rol-compradetallereportesucur" /> <!-- checked -->
                                                    <label for="rol-compradetallereportesucur">Compra por detalle por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="2">
                                                    <div class=""><input type="checkbox" id="rol-stockreporte" name="rol-stockreporte" /> <!-- checked -->
                                                        <label for="rol-stockreporte">Stock</label></div>
                                                        
                                                    
                                                        <ul class="dd-list">
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-generalreporte" name="rol-generalreporte" /> <!-- checked -->
                                                            <label for="rol-generalreporte">General</label></div>
                                                            </li>
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-generalreportesucur" name="rol-generalreportesucur" /> <!-- checked -->
                                                            <label for="rol-generalreportesucur">General por sucursal</label></div>
                                                            </li>
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-valorizadoreporte" name="rol-valorizadoreporte" /> <!-- checked -->
                                                            <label for="rol-valorizadoreporte">Valorizado</label></div>
                                                            </li>
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-valorizadoreportesucur" name="rol-valorizadoreportesucur" /> <!-- checked -->
                                                            <label for="rol-valorizadoreportesucur">Valorizado por sucursal</label></div>
                                                            </li>
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-compraventareporte" name="rol-compraventareporte" /> <!-- checked -->
                                                            <label for="rol-compraventareporte">Compra - venta </label></div>
                                                            </li>
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-compraventareportesucur" name="rol-compraventareportesucur" /> <!-- checked -->
                                                            <label for="rol-compraventareportesucur">Compra - venta por sucursal </label></div>
                                                            </li>
                                                            
                                                        </ul>
                                                        
                                                    </li>
                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-kardexreporte" name="rol-kardexreporte" /> <!-- checked -->
                                                    <label for="rol-kardexreporte">Kardex</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-kardexreportesucur" name="rol-kardexreportesucur" /> <!-- checked -->
                                                    <label for="rol-kardexreportesucur">Kardex por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-cuentascobrarreporte" name="rol-cuentascobrarreporte" /> <!-- checked -->
                                                    <label for="rol-cuentascobrarreporte">Cuentas por cobrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-cuentascobrarreportesucur" name="rol-cuentascobrarreportesucur" /> <!-- checked -->
                                                    <label for="rol-cuentascobrarreportesucur">Cuentas por cobrar por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-cuentaspagarreporte" name="rol-cuentaspagarreporte" /> <!-- checked -->
                                                    <label for="rol-cuentaspagarreporte">Cuentas por pagar </label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-cuentaspagarreportesucur" name="rol-cuentaspagarreportesucur" /> <!-- checked -->
                                                    <label for="rol-cuentaspagarreportesucur">Cuentas por pagar por sucursal</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-operacionesall" name="rol-operacionesall" /> <!-- checked -->
                                                        <label for="rol-operacionesall" class="titulocheck">Operaciones</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-gastosoperaciones" name="rol-gastosoperaciones" /> <!-- checked -->
                                                        <label for="rol-gastosoperaciones">Gastos</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-gastosoperacionessucur" name="rol-gastosoperacionessucur" /> <!-- checked -->
                                                        <label for="rol-gastosoperacionessucur">Gastos por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-movimientocajaoperaciones" name="rol-movimientocajaoperaciones" /> <!-- checked -->
                                                    <label for="rol-movimientocajaoperaciones">Movimiento de caja</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-trasladooperaciones" name="rol-trasladooperaciones" /> <!-- checked -->
                                                    <label for="rol-trasladooperaciones">Traslado de articulos</label></div>
                                                    </li>
                                                   
                                                  
                                                </ul>
                                        </li>
                                        
                                     
                                        
                                        
                                    </ul>
                                </div>
                            </div>
                                
                           
                            </div>
                     
                        
                    
    
                        </div> 


                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
    <!--                            <input type="checkbox" id="remember_me_5" class="filled-in">
                                <label for="remember_me_5">Remember Me</label>-->
                                <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">GUARDAR</button>
                                <a href="<?= base_url ?>nivel/select"  class="btn btn-danger btn-lg m-l-15 waves-effect" id="btncancelar">CANCELAR</a>
                            </div>
                        </div>
                        <!--</form>-->
                    </div>
                </form>
                <div id="respuestaAjax"></div>



            </div>
        </div>
    </div>
</div>
<!-- #END# Inline Layout | With Floating Label -->

   </div>
</section>
<script>
	$(function(){
		$("ul.checktree").checktree();
	});

</script>

