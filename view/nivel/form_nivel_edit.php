
<style>
    .titulocheck{
        font-size: 16px !important;
        color: #2196F3;
       
        
        font-weight: bold !important;
    }
    ul{
        margin-left: 2% !important;
    }
</style>

<section class="content">
    <!--<div class="container-fluid">-->
        
<div class="block-header">
    <!--<h2>FORM EXAMPLES</h2>-->
    <!--    <div class="RespuestaAjax"></div>-->
</div>
<!-- Inline Layout | With Floating Label -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                Editar Nivel / Permisos
                    <!--<small>Edición</small>-->
                </h2>


            </div>
            <div class="body">
                <form action="<?= base_url ?>nivel/update" method="POST"  id="FormularioAjax" data-form=" " enctype="multipart/form-data" autocomplete="off" >
                    <div class="row clearfix">
                        <!--<form >-->
                        <div class="row">
                            <input type="hidden" value="<?= $ni->getId(); ?>" id="id" name="id">
                            
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">Descripción (*)</label>
                                    <div class="form-line ">
                                        <input type="text" class="form-control" id="txtdescripcion" name="txtdescripcion" value="<?= $ni->getDescripcion(); ?>" required="">
                                        
                                    </div>
                                </div>
                            </div>
                            </div>
                        <div class="row " >
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                
                                
                                <div class="clearfix m-b-20">
                                <div class="">
                                    <ul class="checktree">
                                        <li >
                                            <input type="checkbox" id="rol-privilegiosall" name="rol-privilegiosall" <?= $niveltodo ?>/> <!-- checked -->
                                            <label for="rol-privilegiosall" class="titulocheck" > Nivel</label>
                                            <ul class="dd-list">
                                                <li class="dd-item">
                                                    <input type="checkbox" id="rol-permisos" name="rol-permisos" <?= $nivel ?>/> <!-- checked -->
                                                    <label for="rol-permisos"> Listar / Mostrar</label>
                                                </li>
                                           
                                            </ul>
                                        </li>
                                        <li >
                                             <input type="checkbox" id="rol-bancoall" name="rol-bancoall" <?= $bancostodo ?>/> <!-- checked -->
                                                    <label for="rol-bancoall" class="titulocheck"> Bancos</label>
                                            <ul class="dd-list">
                                                <li class="dd-item" data-id="3">
                                                    <input type="checkbox" id="rol-bancoeditar" name="rol-bancoeditar" <?= $bancoeditar ?>/> <!-- checked -->
                                                    <label for="rol-bancoeditar"> Editar</label>
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <input type="checkbox" id="rol-bancoeliminar" name="rol-bancoeliminar" <?= $bancoeliminar ?>/> <!-- checked -->
                                                    <label for="rol-bancoeliminar">Eliminar</label>
                                                </li>
                                                
                                                <li class="dd-item" data-id="9">
                                                    <input type="checkbox" id="rol-bancolistar" name="rol-bancolistar" <?= $bancolistar ?>/> <!-- checked -->
                                                    <label for="rol-bancolistar">Listar / Mostrar</label>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <input type="checkbox" id="rol-banconuevo" name="rol-banconuevo" <?= $banconuevo ?>/> <!-- checked -->
                                                <label for="rol-banconuevo">Nuevo</label>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                           <input type="checkbox" id="rol-cuentabancaall" name="rol-cuentabancaall" <?= $cuentabanctodo ?>/> <!-- checked -->
                                            <label for="rol-cuentabancaall" class="titulocheck"> Cuenta Bancaria</label>
                                             <ul class="dd-list">
                                                <li class="dd-item" data-id="3">
                                                    <div class=""><input type="checkbox" id="rol-cuentabancaeditar" name="rol-cuentabancaeditar" <?= $cuentabancaeditar ?>/> <!-- checked -->
                                                    <label for="rol-cuentabancaeditar">Editar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <div class=""><input type="checkbox" id="rol-cuentabancaeliminar" name="rol-cuentabancaeliminar" <?= $cuentabancaeliminar ?>/> <!-- checked -->
                                                <label for="rol-cuentabancaeliminar">Eliminar</label></div>
                                                </li>
                                                
                                                <li class="dd-item" data-id="9">
                                                    <div class=""><input type="checkbox" id="rol-cuentabancalistar" name="rol-cuentabancalistar" <?= $cuentabancalistar ?>/> <!-- checked -->
                                                <label for="rol-cuentabancalistar">Listar / Mostrar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <div class=""><input type="checkbox" id="rol-cuentabancanuevo" name="rol-cuentabancanuevo" <?= $cuentabancanuevo ?>/> <!-- checked -->
                                                <label for="rol-cuentabancanuevo">Nuevo</label></div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li >
                                            <input type="checkbox" id="rol-clienteall" name="rol-clienteall" <?= $clientetodo ?>/> <!-- checked -->
                                                    <label for="rol-clienteall" class="titulocheck">Cliente</label>
                                            <ul class="dd-list">
                                                <li class="dd-item" data-id="3">
                                                    <div class=""><input type="checkbox" id="rol-clienteeditar" name="rol-clienteeditar" <?= $clienteeditar ?>/> <!-- checked -->
                                                    <label for="rol-clienteeditar">Editar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <div class=""><input type="checkbox" id="rol-clienteeliminar" name="rol-clienteeliminar" <?= $clienteeliminar ?>/> <!-- checked -->
                                                <label for="rol-clienteeliminar">Eliminar</label></div>
                                                </li>
                                                
                                                <li class="dd-item" data-id="9">
                                                    <div class=""><input type="checkbox" id="rol-clientelistar" name="rol-clientelistar" <?= $clientelistar ?>/> <!-- checked -->
                                                <label for="rol-clientelistar">Listar / Mostrar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <div class=""><input type="checkbox" id="rol-clientenuevo" name="rol-clientenuevo" <?= $clientenuevo ?>/> <!-- checked -->
                                                <label for="rol-clientenuevo">Nuevo</label></div>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <div class=""><input type="checkbox" id="rol-clientedefecto" name="rol-clientedefecto" <?= $clientedefecto ?>/> <!-- checked -->
                                                <label for="rol-clientedefecto">Por defecto</label></div>
                                                </li>
                                            </ul>
                                        </li>
                                             <li >
                                             <input type="checkbox" id="rol-proveedorall" name="rol-proveedorall" <?= $proveedortodo ?>/> <!-- checked -->
                                        <label for="rol-proveedorall" class="titulocheck"> Proveedor</label> 
                                            <ul class="dd-list">
                                                <li class="dd-item" data-id="3">
                                                    <div class=""><input type="checkbox" id="rol-proveedoreditar" name="rol-proveedoreditar" <?= $proveedoreditar ?>/> <!-- checked -->
                                                    <label for="rol-proveedoreditar">Editar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <div class=""><input type="checkbox" id="rol-proveedoreliminar" name="rol-proveedoreliminar" <?= $proveedoreliminar ?>/> <!-- checked -->
                                                <label for="rol-proveedoreliminar">Eliminar</label></div>
                                                </li>
                                                
                                                <li class="dd-item" data-id="9">
                                                    <div class=""><input type="checkbox" id="rol-proveedorlistar" name="rol-proveedorlistar" <?= $proveedorlistar ?>/> <!-- checked -->
                                                <label for="rol-proveedorlistar">Listar / Mostrar</label></div>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <div class=""><input type="checkbox" id="rol-proveedornuevo" name="rol-proveedornuevo" <?= $proveedornuevo ?>/> <!-- checked -->
                                                <label for="rol-proveedornuevo">Nuevo</label></div>
                                                </li>
                                                <li class="dd-item" data-id="10">
                                                    <div class=""><input type="checkbox" id="rol-proveedordefecto" name="rol-proveedordefecto" <?= $proveedordefecto ?>/> <!-- checked -->
                                                <label for="rol-proveedordefecto">Por defecto</label></div>
                                                </li>
                                            </ul>
                                        </li>
                                      
                                             <li >
                                            <input type="checkbox" id="rol-marcaall" name="rol-marcaall"  <?= $marcatodo ?>/> <!-- checked -->
                                                        <label for="rol-marcaall"class="titulocheck">Marca</label>
                                                <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-marcaeditar" name="rol-marcaeditar" <?= $marcaeditar ?>/> <!-- checked -->
                                                        <label for="rol-marcaeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-marcaeliminar" name="rol-marcaeliminar" <?= $marcaeliminar ?>/> <!-- checked -->
                                                    <label for="rol-marcaeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-marcalistar" name="rol-marcalistar" <?= $marcalistar ?>/> <!-- checked -->
                                                    <label for="rol-marcalistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-marcanuevo" name="rol-marcanuevo" <?= $marcanuevo ?>/> <!-- checked -->
                                                    <label for="rol-marcanuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-unidall" name="rol-unidall"  <?= $unidmedidatodo ?>/> <!-- checked -->
                                                        <label for="rol-unidall" class="titulocheck">Unid. Medida</label>
                                           <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-unideditar" name="rol-unideditar" <?= $unideditar ?>/> <!-- checked -->
                                                        <label for="rol-unideditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-unideliminar" name="rol-unideliminar" <?= $unideliminar ?>/> <!-- checked -->
                                                    <label for="rol-unideliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-unidlistar" name="rol-unidlistar" <?= $unidlistar ?>/> <!-- checked -->
                                                    <label for="rol-unidlistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-unidnuevo" name="rol-unidnuevo" <?= $unidnuevo ?>/> <!-- checked -->
                                                    <label for="rol-unidnuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                        
                                        <li >
                                            <input type="checkbox" id="rol-categoriaall" name="rol-categoriaall" <?= $categoriatodo ?>/> <!-- checked -->
                                                        <label for="rol-categoriaall"class="titulocheck">Categoria</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-categoriaeditar" name="rol-categoriaeditar" <?= $categoriaeditar ?>/> <!-- checked -->
                                                        <label for="rol-categoriaeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-categoriaeliminar" name="rol-categoriaeliminar" <?= $categoriaeliminar ?>/> <!-- checked -->
                                                    <label for="rol-categoriaeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-categorialistar" name="rol-categorialistar" <?= $categorialistar ?>/> <!-- checked -->
                                                    <label for="rol-categorialistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-categorianuevo" name="rol-categorianuevo" <?= $categorianuevo ?>/> <!-- checked -->
                                                    <label for="rol-categorianuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li>
                                        <input type="checkbox" id="rol-lineaall" name="rol-lineaall" <?= $lineatodo ?>/> <!-- checked -->
                                                        <label for="rol-lineaall" class="titulocheck">Linea</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-lineaeditar" name="rol-lineaeditar" <?= $lineaeditar ?>/> <!-- checked -->
                                                        <label for="rol-lineaeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-lineaeliminar" name="rol-lineaeliminar" <?= $lineaeliminar ?>/> <!-- checked -->
                                                    <label for="rol-lineaeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-linealistar" name="rol-linealistar" <?= $linealistar ?>/> <!-- checked -->
                                                    <label for="rol-linealistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-lineanuevo" name="rol-lineanuevo" <?= $lineanuevo ?>/> <!-- checked -->
                                                    <label for="rol-lineanuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-articuloall" name="rol-articuloall" <?= $articulotodo ?>/> <!-- checked -->
                                                        <label for="rol-articuloall" class="titulocheck">Articulos</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-articuloeditar" name="rol-articuloeditar" <?= $articuloeditar ?>/> <!-- checked -->
                                                        <label for="rol-articuloeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-articuloeliminar" name="rol-articuloeliminar" <?= $articuloeliminar ?>/> <!-- checked -->
                                                    <label for="rol-articuloeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-articulolistar" name="rol-articulolistar" <?= $articulolistar ?>/> <!-- checked -->
                                                    <label for="rol-articulolistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-articulonuevo" name="rol-articulonuevo" <?= $articulonuevo ?>/> <!-- checked -->
                                                    <label for="rol-articulonuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-servicioall" name="rol-servicioall" <?= $serviciotodo ?>/> <!-- checked -->
                                                        <label for="rol-servicioall" class="titulocheck">Servicios</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-servicioeditar" name="rol-servicioeditar" <?= $servicioeditar ?>/> <!-- checked -->
                                                        <label for="rol-servicioeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-servicioeliminar" name="rol-servicioeliminar" <?= $servicioeliminar ?>/> <!-- checked -->
                                                    <label for="rol-servicioeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-serviciolistar" name="rol-serviciolistar" <?= $serviciolistar ?>/> <!-- checked -->
                                                    <label for="rol-serviciolistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-servicionuevo" name="rol-servicionuevo" <?= $servicionuevo ?>/> <!-- checked -->
                                                    <label for="rol-servicionuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-usuarioall" name="rol-usuarioall" <?= $usuariotodo ?>/> <!-- checked -->
                                                        <label for="rol-usuarioall" class="titulocheck">Usuarios</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-usuarioeditar" name="rol-usuarioeditar" <?= $usuarioeditar ?>/> <!-- checked -->
                                                        <label for="rol-usuarioeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-usuarioeliminar" name="rol-usuarioeliminar" <?= $usuarioeliminar ?>/> <!-- checked -->
                                                    <label for="rol-usuarioeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-usuariolistar" name="rol-usuariolistar" <?= $usuariolistar ?>/> <!-- checked -->
                                                    <label for="rol-usuariolistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-usuarionuevo" name="rol-usuarionuevo" <?= $usuarionuevo ?>/> <!-- checked -->
                                                    <label for="rol-usuarionuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
<!--                                         <li >
                                        <input type="checkbox" id="rol-sucursalpall" name="rol-sucursalpall" />  checked 
                                                        <label for="rol-sucursalpall" class="titulocheck">Sucursal Principal</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-sucursalprincipaleditar" name="rol-sucursalprincipaleditar" />  checked 
                                                        <label for="rol-sucursalprincipaleditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-sucursalprincipalmostrar" name="rol-sucursalprincipalmostrar" <?= $sucursalprincipalmostrar ?>/>  checked 
                                                    <label for="rol-sucursalprincipalmostrar">Mostrar</label></div>
                                                    </li>

                                                </ul>
                                        </li>-->
                                         <li >
                                        <input type="checkbox" id="rol-sucursalall" name="rol-sucursalall" <?= $sucursalestodo ?>/> <!-- checked -->
                                                        <label for="rol-sucursalall" class="titulocheck">Sucursales</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-sucursaleditar" name="rol-sucursaleditar" <?= $sucursaleditar ?>/> <!-- checked -->
                                                        <label for="rol-sucursaleditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-sucursaleliminar" name="rol-sucursaleliminar" <?= $sucursaleliminar ?>/> <!-- checked -->
                                                    <label for="rol-sucursaleliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-sucursallistar" name="rol-sucursallistar" <?= $sucursallistar ?>/> <!-- checked -->
                                                    <label for="rol-sucursallistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-sucursalnuevo" name="rol-sucursalnuevo" <?= $sucursalnuevo ?>/> <!-- checked -->
                                                    <label for="rol-sucursalnuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-documentoall" name="rol-documentoall" <?= $documentotodo ?>/> <!-- checked -->
                                                        <label for="rol-documentoall" class="titulocheck">Documentos</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-documentoeditar" name="rol-documentoeditar" <?= $documentoeditar ?>/> <!-- checked -->
                                                        <label for="rol-documentoeditar">Editar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-documentoeliminar" name="rol-documentoeliminar" <?= $documentoeliminar ?>/> <!-- checked -->
                                                    <label for="rol-documentoeliminar">Eliminar</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-documentolistar" name="rol-documentolistar" <?= $documentolistar ?>/> <!-- checked -->
                                                    <label for="rol-documentolistar">Listar / Mostrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-documentonuevo" name="rol-documentonuevo" <?= $documentonuevo ?>/> <!-- checked -->
                                                    <label for="rol-documentonuevo">Nuevo</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-ventaall" name="rol-ventaall" <?= $ventatodo ?>/> <!-- checked -->
                                                        <label for="rol-ventaall" class="titulocheck"> Ventas</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobanteventa" name="rol-vercomprobanteventa" <?= $vercomprobanteventa ?>/> <!-- checked -->
                                                        <label for="rol-vercomprobanteventa">Ver comprobantes</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobanteventaporsucur" name="rol-vercomprobanteventaporsucur" <?= $vercomprobanteventaporsucur ?>/> <!-- checked -->
                                                        <label for="rol-vercomprobanteventaporsucur">Ver comprobantes por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-boletafacturaventa" name="rol-boletafacturaventa" <?= $boletafacturaventa ?>/> <!-- checked -->
                                                    <label for="rol-boletafacturaventa">Emitir boleta/factura</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-notacreditoventa" name="rol-notacreditoventa" <?=  $notacreditoventa ?>/> <!-- checked -->
                                                    <label for="rol-notacreditoventa">Emitir nota de crédito</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-notadebitoventa" name="rol-notadebitoventa" <?= $notadebitoventa ?>/> <!-- checked -->
                                                    <label for="rol-notadebitoventa">Emitir nota de débito</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-cotizacionventa" name="rol-cotizacionventa" <?= $cotizacionventa ?>/> <!-- checked -->
                                                    <label for="rol-cotizacionventa">Cotización</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-vercotizacionesventa" name="rol-vercotizacionesventa" <?= $vercotizacionesventa ?>/> <!-- checked -->
                                                    <label for="rol-vercotizacionesventa">Ver cotizaciones</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-vercotizacionesventasucur" name="rol-vercotizacionesventasucur" <?= $vercotizacionesventasucur ?>/> <!-- checked -->
                                                    <label for="rol-vercotizacionesventasucur">Ver cotizaciones por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-editarprecio" name="rol-editarprecio"  <?= $editarpreciodocumento ?>/> <!-- checked -->
                                                    <label for="rol-editarprecio">Editar precios</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                            <input type="checkbox" id="rol-compraall" name="rol-compraall" <?= $compratodo ?>/> <!-- checked -->
                                                        <label for="rol-compraall" class="titulocheck">Compras</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobantecompra" name="rol-vercomprobantecompra" <?= $vercomprobantecompra ?>/> <!-- checked -->
                                                        <label for="rol-vercomprobantecompra">Ver comprobantes</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobantecomprasucur" name="rol-vercomprobantecomprasucur" <?= $vercomprobantecomprasucur ?>/> <!-- checked -->
                                                        <label for="rol-vercomprobantecomprasucur">Ver comprobantes por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-compra" name="rol-compra" <?= $compra ?>/> <!-- checked -->
                                                    <label for="rol-compra">Compra</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobanteordencompra" name="rol-vercomprobanteordencompra" <?= $vercomprobanteordencompra ?>/> <!-- checked -->
                                                    <label for="rol-vercomprobanteordencompra">Ver comprobantes orden compra</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-vercomprobanteordencomprasucur" name="rol-vercomprobanteordencomprasucur" <?= $vercomprobanteordencomprasucur ?>/> <!-- checked -->
                                                    <label for="rol-vercomprobanteordencomprasucur">Ver comprobantes orden compra por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-ordencompra" name="rol-ordencompra" <?= $ordencompra ?>/> <!-- checked -->
                                                    <label for="rol-ordencompra">Emitir orden compra</label></div>
                                                    </li>
                                                    
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-reportesall" name="rol-reportesall" <?= $reportetodo ?>/> <!-- checked -->
                                                        <label for="rol-reportesall" class="titulocheck">Reportes</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-ventadetallereporte" name="rol-ventadetallereporte" <?= $ventadetallereporte ?>/> <!-- checked -->
                                                        <label for="rol-ventadetallereporte">Venta por detalle</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-ventadetallereportesucur" name="rol-ventadetallereportesucur" <?= $ventadetallereportesucur ?>/> <!-- checked -->
                                                        <label for="rol-ventadetallereportesucur">Venta por detalle por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-compradetallereporte" name="rol-compradetallereporte" <?= $compradetallereporte ?>/> <!-- checked -->
                                                    <label for="rol-compradetallereporte">Compra por detalle</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-compradetallereportesucur" name="rol-compradetallereportesucur" <?= $compradetallereportesucur ?>/> <!-- checked -->
                                                    <label for="rol-compradetallereportesucur">Compra por detalle por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="2">
                                                    <div class=""><input type="checkbox" id="rol-stockreporte" name="rol-stockreporte" <?= $stocktodo ?> /> <!-- checked -->
                                                        <label for="rol-stockreporte">Stock</label></div>
                                                        
                                                    
                                                        <ul class="dd-list">
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-generalreporte" name="rol-generalreporte" <?= $generalreporte ?>/> <!-- checked -->
                                                            <label for="rol-generalreporte">General</label></div>
                                                            </li>
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-generalreportesucur" name="rol-generalreportesucur" <?= $generalreportesucur ?>/> <!-- checked -->
                                                            <label for="rol-generalreportesucur">General por sucursal</label></div>
                                                            </li>
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-valorizadoreporte" name="rol-valorizadoreporte" <?= $valorizadoreporte ?>/> <!-- checked -->
                                                            <label for="rol-valorizadoreporte">Valorizado</label></div>
                                                            </li>
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-valorizadoreportesucur" name="rol-valorizadoreportesucur" <?= $valorizadoreportesucur ?>/> <!-- checked -->
                                                            <label for="rol-valorizadoreportesucur">Valorizado por sucursal</label></div>
                                                            </li>
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-compraventareporte" name="rol-compraventareporte" <?= $compraventareporte ?>/> <!-- checked -->
                                                            <label for="rol-compraventareporte">Compra - venta </label></div>
                                                            </li>
                                                            <li class="dd-item" data-id="9">
                                                                <div class=""><input type="checkbox" id="rol-compraventareportesucur" name="rol-compraventareportesucur" <?= $compraventareportesucur ?>/> <!-- checked -->
                                                            <label for="rol-compraventareportesucur">Compra - venta por sucursal </label></div>
                                                            </li>
                                                            
                                                        </ul>
                                                        
                                                    </li>
                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-kardexreporte" name="rol-kardexreporte" <?= $kardexreporte ?>/> <!-- checked -->
                                                    <label for="rol-kardexreporte">Kardex</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-kardexreportesucur" name="rol-kardexreportesucur" <?= $kardexreportesucur ?>/> <!-- checked -->
                                                    <label for="rol-kardexreportesucur">Kardex por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-cuentascobrarreporte" name="rol-cuentascobrarreporte" <?= $cuentascobrarreporte ?>/> <!-- checked -->
                                                    <label for="rol-cuentascobrarreporte">Cuentas por cobrar</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-cuentascobrarreportesucur" name="rol-cuentascobrarreportesucur" <?= $cuentascobrarreportesucur ?>/> <!-- checked -->
                                                    <label for="rol-cuentascobrarreportesucur">Cuentas por cobrar por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-cuentaspagarreporte" name="rol-cuentaspagarreporte" <?= $cuentaspagarreporte ?>/> <!-- checked -->
                                                    <label for="rol-cuentaspagarreporte">Cuentas por pagar </label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="10">
                                                        <div class=""><input type="checkbox" id="rol-cuentaspagarreportesucur" name="rol-cuentaspagarreportesucur" <?= $cuentaspagarreportesucur ?>/> <!-- checked -->
                                                    <label for="rol-cuentaspagarreportesucur">Cuentas por pagar por sucursal</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                         <li >
                                        <input type="checkbox" id="rol-operacionesall" name="rol-operacionesall" <?= $operaciontodo ?>/> <!-- checked -->
                                                        <label for="rol-operacionesall" class="titulocheck">Operaciones</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-gastosoperaciones" name="rol-gastosoperaciones" <?= $gastosoperaciones ?>/> <!-- checked -->
                                                        <label for="rol-gastosoperaciones">Gastos</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-gastosoperacionessucur" name="rol-gastosoperacionessucur" <?= $gastosoperacionessucur ?>/> <!-- checked -->
                                                        <label for="rol-gastosoperacionessucur">Gastos por sucursal</label></div>
                                                    </li>
                                                    <li class="dd-item" data-id="4">
                                                        <div class=""><input type="checkbox" id="rol-movimientocajaoperaciones" name="rol-movimientocajaoperaciones" <?= $movimientocajaoperaciones ?>/> <!-- checked -->
                                                    <label for="rol-movimientocajaoperaciones">Movimiento de caja</label></div>
                                                    </li>

                                                    <li class="dd-item" data-id="9">
                                                        <div class=""><input type="checkbox" id="rol-trasladooperaciones" name="rol-trasladooperaciones" <?= $trasladooperaciones ?>/> <!-- checked -->
                                                    <label for="rol-trasladooperaciones">Traslado de articulos</label></div>
                                                    </li>
                                                   
                                                  
                                                </ul>
                                        </li>
                                        
                                        <?php if(permisos::rol('rol-panel')){ ?>
                                        <li >
                                             <input type="checkbox" id="rol-panelall" name="rol-panelall" <?= $panel ?>/> <!-- checked -->
                                                        <label for="rol-panelall" class="titulocheck" >Panel</label>
                                            <ul class="dd-list">
                                                    <li class="dd-item" data-id="3">
                                                        <div class=""><input type="checkbox" id="rol-panel" name="rol-panel" <?= $panel ?>/> <!-- checked -->
                                                            <label for="rol-panel">Empresas</label></div>
                                                    </li>
                                                  
                                                </ul>
                                        </li>
                                        <?php }?>
                                     
                                        
                                        
                                    </ul>
                                </div>
                            </div>
                                
                           
                            </div>
                     
                        
                    
    
                        </div> 


                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
    <!--                            <input type="checkbox" id="remember_me_5" class="filled-in">
                                <label for="remember_me_5">Remember Me</label>-->
                                <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">GUARDAR</button>
                                <a   class="btn btn-danger btn-lg m-l-15 waves-effect" onclick="eliminar(<?= $ni->getId()?>,'<?=base_url.'nivel/delete'?>','delete')">ELIMINAR</a>
                                <a href="#"  onclick="window.history.back(-1)" class="btn btn-warning btn-lg m-l-15 waves-effect" id="btncancelar">CANCELAR</a>
                            </div>
                        </div>
                        <!--</form>-->
                    </div>
                </form>
                <div id="respuestaAjax" class="RespuestaAjax"></div>



            </div>
        </div>
    </div>
</div>
<!-- #END# Inline Layout | With Floating Label -->

   </div>
</section>
<script>
	$(function(){
		$("ul.checktree").checktree();
	});

</script>

