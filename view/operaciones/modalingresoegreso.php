<div class="modal fade modalingresoegreso" id="mdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content "> <!-- modal-col-deep-purple-->
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel"><label  id="titulo"></label></h4>
            </div>
            <div class="modal-body">
                <form id="FormularioAjax" action="<?= base_url ?>caja/insertgasto" method="POST" autocomplete="off">
                    <input type="hidden" id="op" name="op"/>
                    <input type="hidden" id="moneda" name="moneda"/>
                    <input type="hidden" id="idcaja" name="idcaja"/>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group form-float">
                                <label class="form-label">Descripción (*)</label>
                                <div class="form-line">
                                    <input type="text" class="form-control" id="txtdescripcion" name="txtdescripcion"  required="">

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group form-float">
                                <label class="form-label">Detalle (*)</label>
                                <div class="form-line">
                                    <input type="text" class="form-control" id="txtdetalle" name="txtdetalle"  required="">

                                </div>
                            </div>
                        </div>
                         </div>
                    <div class="row">

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group form-float">
                                <label class="form-label">Forma de pago</label>
                                <select class="form-control show-tick" id="cbformapago" name="cbformapago">
                                    <option value="efectivo">Efectivo</option>
                                    <option value="tarjeta">Tarjeta</option>
                                    <option value="transferencia">Transferencia</option>
                                
                                    

                                </select>
                            </div>
                        </div>
                        <div id="pago">
                         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group form-float">
                                <label class="form-label">Monto (*)</label>
                                <div class="form-line">
                                    <input type="text" class="form-control" id="txtmonto" name="txtmonto"  required="">

                                </div>
                            </div>
                        </div>
                        
                         
                        
                        
                        </div>
                        
                    </div>
               
                    <button type="submit" class="btn btn-primary waves-effect">GUARDAR</button>
                    </form>
                    </div>
                    
                
                <div id="respuestagastoAjax"></div>
            
            <div class="modal-footer">

                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
            </div>
            </div>
        </div>
    </div>

<script>
$(document).on('change','#cbformapago', function (){
    
    var div = document.getElementById('pago');
   
   $('#pago').empty();
   if($(this).val()=='efectivo'){
       
       var monto =  document.createElement('div');
       monto.setAttribute('class','col-lg-3 col-md-3 col-sm-6 col-xs-6');
       monto.innerHTML= '<div class="form-group form-float"><label class="form-label">Monto (*)</label><div class="form-line"><input type="text" class="form-control" id="txtmonto" name="txtmonto"  required=""></div></div>';
       div.appendChild(monto);
       
       
       
   }
     if($(this).val()=='transferencia'){
       
       var monto =  document.createElement('div');
       monto.setAttribute('class','col-lg-3 col-md-3 col-sm-6 col-xs-6');
       monto.innerHTML= '<div class="form-group form-float"><label class="form-label">Monto (*)</label><div class="form-line"><input type="text" class="form-control" id="txtmonto" name="txtmonto"  required=""></div></div>';
       div.appendChild(monto);
       
       var dive = document.createElement('div');
       dive.setAttribute('class','col-lg-3 col-md-3 col-sm-6 col-xs-6');
       dive.innerHTML= '<div class="form-group form-float"><label class="form-label">N. Ope. (*)</label><div class="form-line"><input type="text" class="form-control" id="txtnop" name="txtnop"  required=""></div></div>';
       
       div.appendChild(dive);
       
   }
   
   
   
   if($(this).val()=='tarjeta'){
       var tipot = document.createElement('div');
       tipot.setAttribute('class','col-lg-3 col-md-3 col-sm-6 col-xs-6');
       var inp = '<div class="form-group form-float">';
       inp+='<label class="form-label">Forma de pago</label>';
       inp+='<select class="form-control show-tick" id="cbtipopago" name="cbtipopago">';
       inp+='<option value="visa">Visa</option>';
       inp+='<option value="mastercard">Mastercard</option>';
       inp+='<option value="americanexpress">American Express</option>';
       inp+='<option value="diners">Diners</option>';
       inp+='</select>';
       inp+='</div>';
       
       tipot.innerHTML= inp;
       div.appendChild(tipot);
       
        var monto =  document.createElement('div');
       monto.setAttribute('class','col-lg-3 col-md-3 col-sm-6 col-xs-6');
       monto.innerHTML= '<div class="form-group form-float"><label class="form-label">Monto (*)</label><div class="form-line"><input type="text" class="form-control" id="txtmonto" name="txtmonto"  required=""></div></div>';
       div.appendChild(monto);
       
       var dive = document.createElement('div');
       dive.setAttribute('class','col-lg-3 col-md-3 col-sm-6 col-xs-6');
       dive.innerHTML= '<div class="form-group form-float"><label class="form-label">N. Ope. (*)</label><div class="form-line"><input type="text" class="form-control" id="txtnop" name="txtnop"  required=""></div></div>';
       
       div.appendChild(dive);
       
   }
   
   
    
});






</script>
