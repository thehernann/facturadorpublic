<div class="modal fade modaldetallecaja " id="mdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content "> <!-- modal-col-deep-purple-->
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Resumen</h4>
            </div>
            <div class="modal-body">
               
                   
                              
                <div id="respuestaAjax" class="row"></div>
                <div  class="row">
                   
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">               
                            
                               <div class="card">
                            <div class="header">
                                
                                Totales
                            </div>
                            
                            <div class="body">
                                
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center">Ingresos</th>
                                        </tr> 
                                        <tr>
                                            <th>Concepto</th>
                                            <th>Monto</th>
                                            
                                        </tr>
                                   
                                        
                                    </thead>
                                    
                                   
                                    
                                    <tfoot>
                                    <th colspan="4" class="text-center ">
                                        <h5 class=""><label id="ingresos">[ Total Ingresos S/. ]
                                                - [ Total Ingresos $  ]</label></h5> </th>
                                    </tfoot>    
                                    <tbody>
                                    <tr>    
                                    <td>Ventas Soles</td>
                                    <td><label id="ventasoles"> </label></td>
                                    </tr>
                                    
                                    <tr>                                    
                                    <td>Ventas Dólares</td>
                                    <td><label id="ventadolar"> </label></td>
                                    </tr>    
                                    <tr >                                    
                                        <td >Otros Ingresos Soles</td>
                                    <td><label id="ingresosoles">S/ </label></td>
                                    </tr>    
                                    <tr  >                                    
                                    <td>Otros Ingresos Dólares</td>
                                    <td><label id="ingresodolares">$ </label></td>
                                    </tr>    
                                        
                                        
                                    </tbody>
                                    
                                    
                                    
                                </table>
                                
                                
                                 <!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">-->               
                            
                               <!--<div class="card">-->
                         
                            
                            <!--<div class="body ">-->
                                <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center">Egresos</th>
                                        </tr>
                                        <tr>
                                            <th>Concepto</th>
                                            <th>Monto</th>
                                        </tr>
                                    
                                        
                                    </thead>
                                    
                                   
                                    
                                    <tfoot>
                                    <th colspan="2" class="text-center ">
                                        <label id="egresos">[ Total Egresos S/.  ]
                                                - [ Total Egresos $  ]</label> </th>
                                    </tfoot>    
                                    <tbody>
                                    <tr>    
                                    <td>Compras Soles</td>
                                    <td><label id="comprasoles">S/.</label></td>
                                    </tr>
                                    
                                    <tr>                                    
                                    <td>Compras Dólares</td>
                                    <td><label id="compradolar">$ </label></td>
                                    </tr>  
                                    
                                    <tr >                                    
                                    <td>Otros Egresos Soles</td>
                                    <td><label id="egresosoles">S/. </label></td>
                                    </tr>    
                                    <tr >                                    
                                    <td>Otros Egresos Dólares</td>
                                    <td><label id="egresodolares">$ </label></td>
                                    </tr>    
                                        
                                        
                                    </tbody>
                                    
                                    
                                    
                                </table>
                                
                                </div>
                            
                                <!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">-->
                                <br>
                        <div class="card">
                            <div class="header text-center">
                                <strong> Ventas</strong>
                                
                            </div>
                            
                            <div class="body">
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Efectivo Soles</h4>
                                    <label id="efectivosoles">S/. </label>
                                </div>
                                
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Efectivo Dolares</h4>
                                    <label id="efectivodolar">$ </label>
                                </div>
                                </div>
                                
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Tarjeta Soles</h4>
                                    <label id="tarjetasoles">S/. </label>
                                </div>
                                
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Tarjeta Dolares</h4>
                                    <label id="tarjetadolar">$ </label>
                                </div>
                                </div>
                                    
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Transferencia Soles</h4>
                                    <label id="transferenciasoles">S/ </label>
                                </div>
                                
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Transferencia Dolares</h4>
                                    <label id="transferenciadollar">$ </label>
                                    
                                </div>
                                </div>
                                
                                
                                
                                
                                
                            </div>
                            
                            
                            
                        </div>

                            
                        </div>
                                

                            </div>
                            
                        </div>

                    
                       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    Caja
                                    
                                </div>
                                <div class="body">
                                    
                                   
                                    
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <div class="card">
                                                <div class="body">
                                                    <h4 class="text-info">CAJA SOLES</h4>
                                                    <label id="cajasoles"> S/. </label>

                                                </div>

                                            </div>
                                        </div> 
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <div class="card">
                                                <div class="body">
                                                    <h4 class="text-info">CAJA DÓLARES</h4>
                                                    <label id="cajadolar">$ </label>

                                                </div>

                                            </div>
                                        </div> 
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <strong>Estado: </strong><label id="estado"></label>
                                            
                                        </div>
                                        
                                        
                                    </div>
<!--                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
                                        <div class="card">
                                            <div class="header bg-green">
                                                Apertura
                                                
                                            </div>
                                            <div class="body">
                                                
                                           
                                             
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <div class="form-group form-float">
                                                            <label>Monto real soles: </label><br>
                                                            <label id="aperturasoles">0.00 </label>
                                                        
                                                        
                                                        </div>
                                                    </div>
                                                
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <div class="form-group form-float">
                                                            <label>Monto real dolares: </label><br>
                                                            <label id="apertudadolar">0.00 </label>
                                                            
                                                            
                                                            
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                     
                                                    
                                                </div>
                                           
                                                
                                                
                                            </div>
                                            

                                        </div>
                                            <div class="card">
                                                <div class="header bg-warning">
                                                    
                                                    Cierre
                                                </div>                                                
                                                
                                                <div class="body">
                                               
                                                    
                                                 
                                                    <div class="row">
                                                      
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <div class="form-group form-float">
                                                            <label>Monto real soles </label><br>
                                                            <label id="cierresoles">0.00</label>
                                                           
                                                        </div> 
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <div class="form-group form-float">
                                                            <label>Monto real dólares</label><br>
                                                            <label id="cierredolar">0.00</label>
                                                            
                                                        </div>
                                                    
                                                    </div>
                                                   
                                                    
                                                    </div>
                                                  
                                                    
                                                </div>
                                            </div>

                                    

                                </div> <!-- -->
                 
                                       
                    
                </div> <!-- respuesta jax --> 
                  
                    </div>
                    
                
                <div id="respuestagastoAjax"></div>
            
            
            </div>
        </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
            </div>
    </div>

<script>

//$(document).ready( function (){
//    
//    $.ajax({
//        type: 'POST',
//        url: "<?= base_url ?>caja/selectbyid",
//        dataType: 'JSON',
//        data: {id:id},
//        success: function (data) {
//            console.log(data);
//        }
//        
//        
//        
//    });
//    
//    
//});





</script>
