<style>
    
    .card {
        margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 10px; /* Added */
        width: 320px;
        max-width: 320px;
}
</style>

<section class="content">
   
        
        <div class="card">
            
            <div class="header">
                <div class="h4 text-center">APERTURA DE CAJA</div>
            </div>
            <div class="body">
                <form action="<?= base_url.'caja/insert' ?>" id="FormularioAjax" method="POST" autocomplete="off">
                    
                    <div class="form-group">
                        <label>Monto en soles (*)</label> 
                        <div class="form-line">
                            <input type="text" class="form-control" name="montosoles" id="montosoles" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                       <label>Monto en dólares (*)</label> 
                        <div class="form-line">
                            <input type="text" class="form-control" name="montodolar" id="montodolar" required>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-xs-8">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">APERTURAR CAJA</button>
                        </div>
                    </div>
                 
                </form>
            </div>
            
            <div id="respuestaAjax"></div>
        </div>
    
    
</section>