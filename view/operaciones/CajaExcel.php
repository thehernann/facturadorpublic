<?php 


$objPHPExcel = new PHPExcel();


        


// Set document properties
$objPHPExcel->getProperties()->setCreator("vtechnology")
               ->setLastModifiedBy("HERNAN VILCHEZ")
               ->setTitle("Office 2007 XLSX Test Document")
               ->setSubject("Office 2007 XLSX Test Document")
               ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
               ->setKeywords("office 2007 openxml php")
               ->setCategory("Test result file");

$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
                                          ->setSize(10);            

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B2', 'Razon Social: '.$resumen['sucursal'])
            ->setCellValue('B3', 'RESPONSABLE: '.$resumen['usuario'])
            ->setCellValue('C4', 'ARQUEO DE CAJA')
            ->setCellValue('C5', $resumen['fecha_apertura'])
            ->setCellValue('D6', 'SOLES ')
            ->setCellValue('E6', 'DOLARES ')
            ->setCellValue('B7', 'APERTURA DE CAJA')
            ->setCellValue('D7', number_format($resumen['apertura_soles'],2))
            ->setCellValue('E7', number_format($resumen['apertura_dolares'],2))
            ->setCellValue('B9', 'INGRESOS')
            ->setCellValue('C10', 'Ventas')
            ->setCellValue('D10', number_format($resumen['montosoles'],2))
            ->setCellValue('E10', number_format($resumen['montodolares'],2))
            ->setCellValue('C11', 'Otros Ingresos')
            ->setCellValue('D11', number_format($resumen['ingresosoles'],2))
            ->setCellValue('E11', number_format($resumen['ingresodolares'],2))
            
            ->setCellValue('B12', 'TOTAL DE INGRESOS')
            ->setCellValue('D12', number_format(($resumen['montosoles'] + $resumen['ingresosoles']),2))
            ->setCellValue('E12', number_format(($resumen['montodolares'] + $resumen['ingresodolares']),2))
        
            ->setCellValue('B14', 'EGRESOS')
            ->setCellValue('C15', 'Otros Egresos')
            ->setCellValue('D15', number_format($resumen['egresosoles'],2))
            ->setCellValue('E15', number_format($resumen['egresodolares'],2))
           
            ->setCellValue('B17', 'LIQUIDACIÓN')
            ->setCellValue('D17', number_format((($resumen['montosoles'] + $resumen['ingresosoles'] + $resumen['apertura_soles'])- $resumen['egresosoles']),2))
            ->setCellValue('E17', number_format((($resumen['montodolares'] + $resumen['ingresodolares'] + $resumen['apertura_dolares']) - $resumen['egresodolares']),2))
            ->setCellValue('B19', '** REGISTRO DE OTROS EGRESOS **')
            ->setCellValue('B21', 'FECHA')
            ->setCellValue('C21', 'DESCRIPCIÓN')
            ->setCellValue('D21', 'DETALLE')
            ->setCellValue('E21', 'FORMA PAGO')
            ->setCellValue('F21', 'MONEDA')
            ->setCellValue('G21', 'MONTO');
            
        
//$informe = getAllListsAndVideos();
//$i = 2;
//while($row = $informe->fetch_array(MYSQLI_ASSOC))
//{

    $i = 22;
    
   foreach ($egresosoles as $egresosol){
       
            
    $objPHPExcel->setActiveSheetIndex(0)
           
            ->setCellValue("B".$i, $egresosol->getFecha())
            ->setCellValue("C".$i, $egresosol->getConcepto())
            ->setCellValue("D".$i, $egresosol->getDetalle())
            ->setCellValue("E".$i, $egresosol->getFormapago())
            ->setCellValue("F".$i, $egresosol->getMoneda())
            ->setCellValue("G".$i, $egresosol->getMonto()); 
                   
        $i++;

}

   foreach ($egresodolares as $egresodolar){
       
            
    $objPHPExcel->setActiveSheetIndex(0)
           
            ->setCellValue("B".$i, $egresodolar->getFecha())
            ->setCellValue("C".$i, $egresodolar->getConcepto())
            ->setCellValue("D".$i, $egresodolar->getDetalle())
            ->setCellValue("E".$i, $egresodolar->getFormapago())
            ->setCellValue("F".$i, $egresodolar->getMoneda())
            ->setCellValue("G".$i, $egresodolar->getMonto()); 
                   
        $i++;

}
       
$i++;     
     $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('B'.$i, '** REGISTRO DE OTROS INGRESOS **');
$i++;
$i++;
    $objPHPExcel->setActiveSheetIndex(0)
   
    ->setCellValue('B'.$i, 'FECHA')
    ->setCellValue('C'.$i, 'DESCRIPCIÓN')
    ->setCellValue('D'.$i, 'DETALLE')
    ->setCellValue('E'.$i, 'FORMA PAGO')
    ->setCellValue('F'.$i, 'MONEDA')
    ->setCellValue('G'.$i, 'MONTO');
$i++;    
    foreach ($ingresosoles as $ingresosol){
       
            
    $objPHPExcel->setActiveSheetIndex(0)
           
            ->setCellValue("B".$i, $ingresosol->getFecha())
            ->setCellValue("C".$i, $ingresosol->getConcepto())
            ->setCellValue("D".$i, $ingresosol->getDetalle())
            ->setCellValue("E".$i, $ingresosol->getFormapago())
            ->setCellValue("F".$i, $ingresosol->getMoneda())
            ->setCellValue("G".$i, $ingresosol->getMonto()); 
                   
        $i++;

}
    foreach ($ingresodolares as $ingresodolar){
       
            
    $objPHPExcel->setActiveSheetIndex(0)
           
            ->setCellValue("B".$i, $ingresodolar->getFecha())
            ->setCellValue("C".$i, $ingresodolar->getConcepto())
            ->setCellValue("D".$i, $ingresodolar->getDetalle())
            ->setCellValue("E".$i, $ingresodolar->getFormapago())
            ->setCellValue("F".$i, $ingresodolar->getMoneda())
            ->setCellValue("G".$i, $ingresodolar->getMonto()); 
                   
        $i++;

}


//$i++;
//}

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);




$objPHPExcel->getActiveSheet()->setTitle('Reporte');

$objPHPExcel->setActiveSheetIndex(0);

getHeaders();

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
 /* Limpiamos el búfer */
ob_end_clean();
$objWriter->save('php://output');
exit;