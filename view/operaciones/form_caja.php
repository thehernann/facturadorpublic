
<style>
/*    .select{
       
        
    }*/
    
    .select :hover{
         background-color:#2196F3;
         color: #ffffff;
    }
    
    .select{
        
        cursor: pointer;
    }
    
    
       
    
    
</style>

<section class="content">
    <!--<div class="container-fluid">-->
        
<div class="block-header">
    <!--<h2>FORM EXAMPLES</h2>-->
    <!--    <div class="RespuestaAjax"></div>-->
</div>
<!-- Inline Layout | With Floating Label -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2 >
                    <label id="titulo"> Caja - <?= $fecha ?> </label>
                    <!--<small>Edición</small>-->
                </h2>


            </div>
            <div class="body">
               
                <div id="respuestaAjax" class="row"></div>
                <div  class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">               
                            
                               <div class="card">
                            <div class="header">
                                
                                Totales
                            </div>
                            
                            <div class="body">
                                
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center">Ingresos</th>
                                        </tr> 
                                        <tr>
                                            <th>Concepto</th>
                                            <th>Monto</th>
                                            
                                        </tr>
                                   
                                        
                                    </thead>
                                    
                                   
                                    
                                    <tfoot>
                                    <th colspan="4" class="text-center ">
                                        <h5 class=""><label id="ingresos">
                                              </label></h5> </th>
                                    </tfoot>    
                                    <tbody>
                                    <tr>    
                                    <td>Ventas Soles</td>
                                    <td><label id="ventasoles"></label></td>
                                    </tr>
                                    
                                    <tr>                                    
                                    <td>Ventas Dólares</td>
                                    <td><label id="ventadolar"></label></td>
                                    </tr>    
                                    <tr onclick="mostrarop('Otros Ingresos Soles','Soles','ingreso')" class="select">                                    
                                        <td >Otros Ingresos Soles</td>
                                    <td><label id="ingresosoles"></label></td>
                                    </tr>    
                                    <tr class="select" onclick="mostrarop('Otros Ingresos Dólares','Dolares','ingreso')" >                                    
                                    <td>Otros Ingresos Dólares</td>
                                    <td><label id="ingresodolares"></label></td>
                                    </tr>    
                                        
                                        
                                    </tbody>
                                    
                                    
                                    
                                </table>
                                
                                
                                 <!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">-->               
                            
                               <!--<div class="card">-->
                         
                            
                            <!--<div class="body ">-->
                                <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center">Egresos</th>
                                        </tr>
                                        <tr>
                                            <th>Concepto</th>
                                            <th>Monto</th>
                                        </tr>
                                    
                                        
                                    </thead>
                                    
                                   
                                    
                                    <tfoot>
                                    <th colspan="2" class="text-center ">
                                        <label id="egresos"></label> </th>
                                    </tfoot>    
                                    <tbody>
                                    <tr>    
                                    <td>Compras Soles</td>
                                    <td><label id="comprasoles"></label></td>
                                    </tr>
                                    
                                    <tr>                                    
                                    <td>Compras Dólares</td>
                                    <td><label id="compradolar"></label></td>
                                    </tr>  
                                    
                                    <tr class="select" onclick="mostrarop('Otros Egresos Soles','Soles','egreso')">                                    
                                    <td>Otros Egresos Soles</td>
                                    <td><label id="egresosoles"></label></td>
                                    </tr>    
                                    <tr class="select" onclick="mostrarop('Otros Egresos Dólares','Dolares','egreso')">                                    
                                    <td>Otros Egresos Dólares</td>
                                    <td><label id="egresodolares"></label></td>
                                    </tr>    
                                        
                                        
                                    </tbody>
                                    
                                    
                                    
                                </table>
                                
                                </div>
                            
                                <!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">-->
                                <br>
                        <div class="card">
                            <div class="header text-center">
                                <strong> Ventas</strong>
                                
                            </div>
                            
                            <div class="body">
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Efectivo Soles</h4>
                                    <label id="efectivosoles"></label>
                                </div>
                                
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Efectivo Dolares</h4>
                                    <label id="efectivodolar"></label>
                                </div>
                                </div>
                                
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Tarjeta Soles</h4>
                                    <label id="tarjetasoles"></label>
                                </div>
                                
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Tarjeta Dolares</h4>
                                    <label id="tarjetadolar"></label>
                                </div>
                                </div>
                                    
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Transferencia Soles</h4>
                                    <label id="transferenciasoles"></label>
                                </div>
                                
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Transferencia Dolares</h4>
                                    <label id="transferenciadollar"></label>
                                    
                                </div>
                                </div>
                                
                                
                                
                                
                                
                            </div>
                            
                            
                            
                        </div>

                            
                        </div>
                                

                            </div>
                            
                        </div>

                    
                       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    Caja
                                    
                                </div>
                                <div class="body">
                                    
                                      <form action="<?= base_url ?>caja/searchcaja" method="POST"  id="buscarcaja" data-form="" enctype="multipart/form-data" autocomplete="off" >
                                        <div class="row clearfix">
                                            <!--<form >-->
                                            <div class="row">

                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                             <div class="form-group form-float">
                                                                 <label class="form-label">Fecha </label>
                                                                 <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                                                 <div class="form-group">
                                                                     <div class="form-line" id="bs_datepicker_container">
                                                                         <input type="text" class="form-control" placeholder="Fecha" id="dpfecha" name="dpfecha" value="<?= $fecha ?>" required="">
                                                                     </div>
                                                                 </div>

                                                             </div>
                                                </div>
                                                
                                                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">
                                                   
                                                        <label class="form-label">Responsable </label>
                                                        <div id="opresponsable">
                                                            <select class="form-control" id="responsable" name="responsable">
                                                                <!--<option value="">-- Todo --</option>-->
                                                                
                                                                <?php
                                                                    if(count($responsables) == 0){
                                                                        echo '<option value="">-- Todo --</option>';
                                                                    }
                                                                    foreach ($responsables as $responsable){  
                                                                      echo '<option value="'.$responsable['id'].'">'.$responsable['nombre'].' - '.$responsable['create'].'</option>';
                                                                    }
                                                                ?>



                                                            </select>
                                                        </div>
                                                 
                                                </div>

                                            </div>
                                            <!--</form>-->
                                            </div>
                                        </form>
                                    
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <div class="card">
                                                <div class="body">
                                                    <h4 class="text-info">CAJA SOLES</h4>
                                                    <label id="cajasoles"> </label>

                                                </div>

                                            </div>
                                        </div> 
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <div class="card">
                                                <div class="body">
                                                    <h4 class="text-info">CAJA DÓLARES</h4>
                                                    <label id="cajadolar"></label>

                                                </div>

                                            </div>
                                        </div> 
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <strong>Estado: </strong><label id="estado"></label>
                                            
                                        </div>
                                        
                                        
                                    </div>
<!--                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
                                        <div class="card">
                                            <div class="header bg-green">
                                                Apertura
                                                
                                            </div>
                                            <div class="body">
                                                
                                            <form action="<?= base_url."caja/updateapertura" ?>" method="POST"  id="FormularioAjax" data-form="" enctype="multipart/form-data" autocomplete="off" > 
                                                <input type="hidden" id="id" name="id" value="" />
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <div class="form-group form-float">
                                                        <label>Monto real soles: </label>
                                                        
                                                        <div class="form-line ">
                                                            <input id="aperturasoles" class="form-control" name="aperturasoles" value=""/> 
                                                        </div>
                                                        </div>
                                                    </div>
                                                
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <div class="form-group form-float">
                                                            <label>Monto real dolares: </label> 
                                                            <div class="form-line ">
                                                                
                                                                <input id="apertudadolar" class="form-control" name="apertudadolar" value="" /> 
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <p>Acción</p>
                                                        <button class="btn btn-warning btn-lg waves-effect" type="submit" >Editar</button>
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </form>
                                                
                                                
                                            </div>
                                            

                                        </div>
                                            <div class="card">
                                                <div class="header bg-warning">
                                                    
                                                    Cierre
                                                </div>                                                
                                                
                                                <div class="body">
                                                    <?php 
                                                    
//                                                        if(is_null($resumen['idcaja'])):
//                                                            $action = base_url."caja/insert";
//                                                        else: 
//                                                            $action = base_url."caja/update";
//                                                        endif;
                                                    ?>
                                                    
                                                   <form action="<?= base_url."caja/update" ?>" method="POST"  id="FormularioAjax" data-form="" enctype="multipart/form-data" autocomplete="off" >
                                                    <div class="row">
                                                        <input type="hidden" id="idcaja" name="idcaja" value="" />
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <div class="form-group form-float">
                                                            <label>Monto real soles (*)</label>
                                                            <div class="form-line ">
                                                                <input class="form-control" id="montosoles" name="montosoles" value="" required="">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <div class="form-group form-float">
                                                            <label>Monto real dólares (*)</label>
                                                            <div class="form-line ">
                                                                <input class="form-control" id="montodolar" name="montodolar" value="" required="">
                                                            </div>
                                                        </div>
                                                    
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                        <p>Acción</p>
                                                        <button class="btn btn-primary btn-lg waves-effect" type="submit" id="btnapertura">Cerrar caja</button>
                                                        
                                                    </div>
                                                    
                                                    </div>
                                                   </form>
                                                    
                                                </div>
                                            </div>
                                            
<!--                                        </div>
                                        
                                    </div>-->
                                    
                                    <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">-->
                                <div class="card">
                                    <div class="header bg-info">
                                        Cuadre de caja
                                    </div>
                                    <div class="body">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="card">
                                                    <div class="header">
                                                        
                                                        Ingresos 
                                                    </div>
                                                    <div class="body">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <button class="btn btn-primary btn-sm waves-effect" data-toggle="modal" data-target=".modalingresoegreso" data-op="ingreso" data-moneda="Soles" data-titulo="Registrar Otros ingresos soles" >Agregar Ingresos Soles</button>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <button class="btn btn-primary waves-effect" data-toggle="modal" data-target=".modalingresoegreso" data-op="ingreso" data-moneda="Dolares" data-titulo="Registrar Otros ingresos dolares" >Agregar Ingresos Dolares</button>
                                                            </div> 

                                                            </div>
                                                       
                                                        
                                                    </div>
                                                    
                                                </div>
                                                
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="card">
                                                    <div class="header">
                                                        
                                                        Egresos 
                                                    </div>
                                                    <div class="body">
                                                         <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <button class="btn btn-primary btn-sm waves-effect" data-toggle="modal" data-target=".modalingresoegreso" data-op="egreso" data-moneda="Soles" data-titulo="Registrar Otros egresos soles" >Agregar Egresos Soles</button>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <button class="btn btn-primary waves-effect" data-toggle="modal" data-target=".modalingresoegreso" data-op="egreso" data-moneda="Dolares" data-titulo="Registrar Otros egresos dolares" >Agregar Egresos Dolares</button>
                                                            </div> 

                                                            </div>
                                                       
                                                        
                                                    </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                                
                                            </div>
                                            
                                            
                                        </div>
                                        


                                    </div>
                                    
                                    



                                <!--</div>-->
                                    

                                </div> <!-->
                                
                            </div>
                            
                            
                        </div>
                       <!--<div class="row">-->
                                        
                            


                            <!--</div>-->
                    
                    <!--<div class="row">-->
                        
<!--                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    Detalle de concepto
                                </div>
                                <div class="body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover dataTable">
                                            <tr>
                                                <th colspan="5" class="text-center">Seleccione un concepto para ver el detalle</th>
                                            </tr>
                                            <tr>
                                                
                                                <th>Descripción</th>
                                                <th>Estado</th>
                                                <th>Fecha</th>
                                                <th>Forma pago</th>
                                                <th>Monto</th>
                                            </tr>
                                            
                                            
                                        </table>
                                        
                                        
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                            
                        </div>-->
                    <!--</div>-->


                        <!--</div>-->
                    
                    <!--<div class="row">-->
                       
                
                    <!--</div>-->
                    <!--<div class="row">-->
                    
                        
                    <!--</div>-->
                    
                </div> <!-- respuesta jax --> 



            </div> <!--body --> 
        </div> <!--card --> 
    </div> <!--col-->
</div><!-- -->
<!-- #END# Inline Layout | With Floating Label -->


</section>

<script>
    
$(document).ready( function (){
    $('#buscarcaja').submit();
});
    
    
    $(document).on('show.bs.modal','.modalingresoegreso', function (event){
        var caja = $('#id').val();
       var button = $(event.relatedTarget); // Button that triggered the modal
        var op = button.data('op');
        var moneda = button.data('moneda');
        var titulo = button.data('titulo');
        console.log(titulo);
        
       var modal = $(this);
       modal.find('.modal-header #titulo').html(titulo);
       modal.find('.modal-body #op').val(op);
       modal.find('.modal-body #moneda').val(moneda);
       modal.find('.modal-body #idcaja').val(caja);
       
        
    });
    
    $(document).on('submit','#buscarcaja',function (event){
        event.preventDefault();
        
        var accion = $(this).attr('action');
        var form = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: accion,
            data: form,
            dataType: 'json',
            beforeSend: function (xhr) {
                        bloquearpantalla("");
                    },
            success: function (data) {
//                console.log(data);
                var estado = '';
                console.log('click');
               
//                if(data['estado'] == 1 ){
//                    estado = 'APERTURADA';
//                   
//                }else if(data['estado'] == 0){
//                    estado = 'CERRADA';
//                   
//                }
                console.log(data['idcaja']);
                
                $('#id').val(data['idcaja']);
                $('#idcaja').val(data['idcaja']);
                
                $('#ingresos').html( '[Total Ingresos S/. '+(parseFloat(data["ventasoles"])+parseFloat(data["ingresosoles"])).toFixed(2)+'] - [Total Ingresos $ '+(parseFloat(data["ventadolares"])+parseFloat(data["ingresodolares"])).toFixed(2)+']');
                $('#ventasoles').html('S/. '+parseFloat(data['ventasoles']).toFixed(2));
                $('#ventadolar').html('$ '+parseFloat(data['ventadolares']).toFixed(2));
                $('#egresos').html('[ Total Egresos S/. '+(parseFloat(data['comprasoles'])+parseFloat(data['egresosoles'])).toFixed(2)+'] - [ Total Egresos $'+(parseFloat(data['compradolares'])+parseFloat(data['egresodolares'])).toFixed(2)+' ]');
                $('#comprasoles').html('S/. '+parseFloat(data['comprasoles']).toFixed(2));
                $('#compradolar').html('$ '+parseFloat(data['compradolares']).toFixed(2));
                $('#gastosoles').html('S/. '+parseFloat(data['cajasoles']).toFixed(2));
                $('#gastodolar').html('$ '+parseFloat(data['cajadolar']).toFixed(2));
                $('#efectivosoles').html('S/. '+parseFloat(data['efectivosoles']).toFixed(2));
                $('#efectivodolar').html('$ '+parseFloat(data['efectivodolares']).toFixed(2));
                $('#tarjetasoles').html('S/. '+parseFloat(data['tarjetasoles']).toFixed(2));
                $('#tarjetadolar').html('$ '+parseFloat(data['tarjetadollar']).toFixed(2));
                $('#transferenciadollar').html('$ '+parseFloat(data['transdollar']).toFixed(2));
                $('#transferenciasoles').html('S/. '+parseFloat(data['transsoles']).toFixed(2));
                $('#cajasoles').html('S/. '+parseFloat(data['cajasoles']).toFixed(2));
                $('#cajadolar').html('$ '+parseFloat(data['cajadolar']).toFixed(2));
                $('#estado').html(data['estado']);
                
                $('#ingresosoles').html('S/ '+parseFloat(data['ingresosoles']).toFixed(2));
                $('#ingresodolares').html('$ '+parseFloat(data['ingresodolares']).toFixed(2));
                $('#egresosoles').html('S/ '+parseFloat(data['egresosoles']).toFixed(2));
                $('#egresodolares').html('$ '+parseFloat(data['egresodolares']).toFixed(2));
                
                
//                $(this).setAttribute('action',url);
                $('#montosoles').val(parseFloat(data['cierresoles']).toFixed(2));
                $('#montodolar').val(parseFloat(data['cierredolares']).toFixed(2));
                
                $('#aperturasoles').val(parseFloat(data['aperturasoles']).toFixed(2));
                $('#apertudadolar').val(parseFloat(data['aperturadolares']).toFixed(2));
                
               
                $.unblockUI();
          
               
               
            }, 
            

        
        
    });
        
        
    });
    
    
    $(document).on('change','#responsable',function (){
        $('#buscarcaja').submit();
    
    
    });
    
        $(document).on('change','#dpfecha', function (e){

        e.preventDefault();
        $('#titulo').html('Movimiento Caja - '+$(this).val()); 
        
        $.ajax({
            type: 'POST',
            url:'<?= base_url.'caja/responsable'?>',
            data: {dpfecha:$(this).val()},
            success: function (data) {
//                console.log(data);
                $('#opresponsable').html(data);
                
                $('#buscarcaja').submit();
                
               
        
                       
            }
            
            
        });
        
        
    });
    
    
   function mostrarop(title,moneda,tipo){
//   'Otros Ingresos Soles','Soles','ingreso',$resumen['idcaja']
   
    
    
    $('.modalmostraringresoegreso').modal().show();
    $('.modal-title').html(title);
    var idcaja = $('#id').val();
    $('#idcajaop').val(idcaja);
    $('#tipoop').val(tipo);
    $('#monedaop').val(moneda);
    
    $.ajax({
        type: 'POST',
        url: "<?= base_url ?>gastos/selectbycaja",
        data: {moneda:moneda,tipo:tipo,idcaja:idcaja},
        success: function (data) {
            
            $('#divop').html(data);
                        
        }
        
        
        
        
    });
   
   
   }
    
    
    
    
    
    
        
        
        
    
    
    
</script>    
