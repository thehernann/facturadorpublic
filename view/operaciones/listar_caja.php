<?php 
 
  /** Actual month last day **/
  function _data_last_month_day() { 
      $month = date('m');
      $year = date('Y');
      $day = date("d", mktime(0,0,0, $month+1, 0, $year));
 
      return date('d/m/Y', mktime(0,0,0, $month, $day, $year));
  }
 
  /** Actual month first day **/
  function _data_first_month_day() {
      $month = date('m');
      $year = date('Y');
      return date('d/m/Y', mktime(0,0,0, $month, 1, $year));
  }



?>

<section class="content">
    <!--<div class="container-fluid">-->
        
       
        
        <div class="block-header">
            <!--    <br>-->
            <div class="RespuestaAjax"></div>
        </div>
        <!-- Basic Examples -->

        <!-- #END# Basic Examples -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           Reporte de cajas
                            

                        </h2>
                       
                    
                    </div>
                    <div class="body">
                       
                        
                     <form  method="POST"  id="Formcaja" data-form="insert" enctype="multipart/form-data" autocomplete="off" >    <!-- <?= base_url?>documento/searchventa -->
                         <input type="hidden" value="Venta" id="tipodoc" name="tipodoc">
                        <div class="row">  
                       <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                                <label class="form-label">Desde </label>
                                <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                <div class="form-group">
                                    <div class="form-line" id="bs_datepicker_container">
                                        <input type="text" class="form-control" placeholder="Desde" id="dpdesde" name="dpdesde" value="<?= _data_first_month_day()?>" required="">
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                                <label class="form-label">Hasta </label>
                                <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                <div class="form-group">
                                    <div class="form-line" id="bs_datepicker_container">
                                        <input type="text" class="form-control" placeholder="Hasta" id="dphasta" name="dphasta" value="<?= _data_last_month_day()?>" required="">
                                    </div>
                                </div>

                            </div>
                        </div>
                            
                          
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                            
                                <label>Sucursal</label>
                                <select class="form-control show-tick" id="cbsucursal" name="cbsucursal">
                                    
                                    <?php 
                                   
                                                                        
                                        foreach ($sucursales as $sucursal){
                                            if($_SESSION['idsucursal'] == $sucursal->getId()){
                                                echo '<option value="'.$sucursal->getId().'" selected="selected" >'.$sucursal->getNombre().'</option>';
                                                
                                            }else{
                                                echo '<option value="'.$sucursal->getId().'" >'.$sucursal->getNombre().'</option>';
                                                
                                            }
                                            


                                    }

                                    ?>

                            </select>
                            </div>
                        </div>
                          <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                            
                                <label>Responsable</label>
                                <select class="form-control show-tick" id="cbusuario" name="cbusuario">
                                    <option value="">--Seleccione--</option>
                                    <?php                               
                                        foreach ($usuarios as $user){
                                           
                                           
                                                echo '<option value="'.$user->getId().'" >'.$user->getNombre().' '.$user->getApellidoP().'</option>';
   
                                    }

                                    ?>

                                </select>
                            </div>
                        </div>
                        
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
                            <label>Acciones</label>
                            <div class="form-group form-float">
                                
                                <button class="btn btn-sm btn-primary waves-effect" type="submit"><span class="glyphicon glyphicon-search "></span> Buscar</button>
                                
                         
                                
                                <!--<button class="btn btn-sm btn-success waves-effect" id="exceldocument" url="<?= base_url?>documento/imprimirexcel"><span class="glyphicon glyphicon-file"></span> Excel</button>-->
                                
                            </div>
                        </div>
                            
                            
                            
                        </div>
                     </form>
                        
                        
                        
                        
                        <div id="respuestaExcel"></div>
                        <div class="table-responsive" id="respuestaAjax">
                            <table class="table  table-hover table-bordered" id="tabladocumento">
                                <thead>
                                    <tr>
                                        <th>Sucursal</th>
                                        <th>Fecha</th>
                                        <th>Responsable</th>
                                        <th>Estado</th>
                                        <th>Monto caja soles</th>
                                        <th>Monto caja dólares</th>
                                        <th>Monto real soles</th>
                                        <th>Monto real dólares</th>
                                        <th>Condición soles</th>
                                        <th>Condición dólares</th>
                                        <!--<th>Est. Sunat</th>-->
                                        <th>Imprimir</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
<!--                                <tfoot>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Tipo</th>
                                        <th>Serie</th>
                                        <th>Número</th>
                                        <th>RUC/ DNI</th>
                                        <th>Nombre / Rz. Social</th>
                                        <th>Total</th>
                                        <th>Est. Local</th>
                                        <th>Est. Sunat</th>
                                        <th>Imprimir</th>
                                        <th>Acciones</th>
                                    </tr>
                                </tfoot>-->
                               
                               
                            </table>
                            <div class="pagination">
                                <nav>
                                    <ul class="pagination"></ul>
                                    
                                </nav>
                                
                            </div>
                            
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table --> 
    
</section>

<script>
   $(document).ready(function (){
     
      
         $('#tabladocumento').DataTable({
//             "responsive": true,
//            "processing": true,
//            "serverSide": true,
            "searching": false,
            
            
            'ajax':{
                "type":"POST",
                "url":'<?= base_url.'caja/searchreport'?>',
                "dataType": 'JSON',
               
                "data" :  {
                dpdesde: $('#dpdesde').val(),
                dphasta: $('#dphasta').val(),
                
                
                cbsucursal: $('#cbsucursal').val(),
                cbusuario: 0
            
              }
             
//              "dataSrc": ""
                
            },
            "columns":[
                {"data" : "sucursal"},
                {"data" : "fecha_apertura"},
                {"data" : "usuario"},
                {"data" : "estado"},
                {"data" : "montosoles"},
                {"data" : "montodolares"},
                 {"data" : "cierre_soles"},
                {"data" : "cierre_dolares"},
                {"data" : "estadosol"},
                {"data" : "estadodolar"},
                {"data" : "imprimir"},
                {"data" : "acciones"}
                
            ],
//            "ordering": false
        });
        

     $(document).on('submit','#Formcaja', function (e){
        e.preventDefault();
        console.log($('#filtro').val());
       
        $("#tabladocumento").dataTable().fnDestroy();
        
         $('#tabladocumento').DataTable({
//             "responsive": true,
//            "processing": true,
//            "serverSide": true,
            "searching": false,
            
            'ajax':{
                "type":"POST",
                "url":'<?= base_url.'caja/searchreport'?>',
                "data" :  {
                    dpdesde: $('#dpdesde').val(),
                    dphasta: $('#dphasta').val(),
                    cbsucursal: $('#cbsucursal').val(),
                    cbusuario: $('#cbusuario').val()
              },
//              "dataSrc": ""
                
            },
            "columns":[
                 {"data" : "sucursal"},
                {"data" : "fecha_apertura"},
                {"data" : "usuario"},
                {"data" : "estado"},
                {"data" : "montosoles"},
                {"data" : "montodolares"},
                 {"data" : "cierre_soles"},
                {"data" : "cierre_dolares"},
                {"data" : "estadosol"},
                {"data" : "estadodolar"},
                {"data" : "imprimir"},
                {"data" : "acciones"}
                
            ],
//            "ordering": false
        });
    });
    
       $(document).on('change','#cbsucursal', function (e){
        e.preventDefault();
        $('#Formcaja').submit();
    
    
    
    });
    
       $(document).on('change','#cbusuario', function (e){
        e.preventDefault();
        $('#Formcaja').submit();
    
    
    
    });
    
       $(document).on('change','#dpdesde', function (e){
        e.preventDefault();
        $('#Formcaja').submit();
    
    
    
    });
       $(document).on('change','#dphasta', function (e){
        e.preventDefault();
        $('#Formcaja').submit();
    
    
    
    });
    
    $(document).on('show.bs.modal','.modaldetallecaja', function (e){
        var button = $(e.relatedTarget); // Button that triggered the modal
        var id = button.data('id');
        
//        console.log(id);
        
//        
//        var modal = $(this);
//        modal.find(".modal-body #id").val(id);
        
        $.ajax({
            type: 'POST',
            url: '<?= base_url."caja/cajabyid" ?>',
            dataType: 'JSON',
            data: {id:id},
            success: function (data) {
                console.log(data);
              
                $('#ventasoles').text("S/ "+parseFloat(data['montosoles']).toFixed(2));
                $('#ventadolar').text("$ "+parseFloat(data['montodolar']).toFixed(2));
                $('#ingresosoles').text("S/ "+parseFloat(data['ingresosoles']).toFixed(2));
                $('#ingresodolares').text("$ "+parseFloat(data['ingresodolares']).toFixed(2));
                $('#ingresos').text("[ Total Ingresos S/ "+(parseFloat(data['ingresosoles']) + parseFloat(data['montosoles'])).toFixed(2)+" ]"+
                        " - [ Total Ingresos $ "+(parseFloat(data['ingresodolares'])+parseFloat(data['montodolar'])).toFixed(2) + " ]");
                
                $('#comprasoles').text("S/ "+parseFloat(data['montosolescompra']).toFixed(2));
                $('#compradolar').text("$ "+parseFloat(data['montodolarescompra']).toFixed(2));
                $('#egresosoles').text("S/ "+parseFloat(data['egresosoles']).toFixed(2));
                $('#egresodolares').text("$ "+parseFloat(data['egresodolares']).toFixed(2));
                
                 $('#egresos').text("[ Total Egresos S/ "+(parseFloat(data['egresosoles']) + parseFloat(data['montosolescompra'])).toFixed(2)+" ]"+
                        " - [ Total Egresos $ "+(parseFloat(data['egresodolares'])+parseFloat(data['montodolarescompra'])).toFixed(2) + " ]");
                
                $('#efectivosoles').text("S/ "+parseFloat(data['efectivosoles']).toFixed(2));
                $('#efectivodolar').text("$ "+parseFloat(data['efectivodolar']).toFixed(2));
                
                $('#tarjetasoles').text("S/ "+parseFloat(data['montosolestarjeta']).toFixed(2));
                $('#tarjetadolar').text("$ "+parseFloat(data['montodolarestarjeta']).toFixed(2));
                
                $('#transferenciasoles').text("S/ "+parseFloat(data['montosolestransferencia']).toFixed(2));
                $('#transferenciadollar').text("$ "+parseFloat(data['monrodolarestransferencia']).toFixed(2));
                
                $('#cajasoles').text("S/ "+((parseFloat(data['montosoles']) + parseFloat(data['ingresosoles']) + parseFloat(data['apertura_soles'])) - parseFloat(data['egresosoles'])).toFixed(2));
                $('#cajadolar').text("$ "+((parseFloat(data['montodolar']) + parseFloat(data['ingresodolares']) + parseFloat(data['apertura_dolares'])) - parseFloat(data['egresodolares'])).toFixed(2));
                
                $('#estado').text(data['estado']);
                $('#aperturasoles').text("S/ "+parseFloat(data['apertura_soles']).toFixed(2));
                $('#apertudadolar').text("$ "+parseFloat(data['apertura_dolares']).toFixed(2));
                
                $('#cierresoles').text("S/ "+parseFloat(data['cierresoles']).toFixed(2));
                $('#cierredolar').text("$ "+parseFloat(data['cierredolares']).toFixed(2));
                   
            }
        
        
        
        });

        
        
    });
    
    
    
    
    });
  
</script>    




















