
<!DOCTYPE html>
<html>
 
<head>

  <link rel="stylesheet" href="<?= base_url?>css/cssticket.css">
<!--  <script src="css/cssticket.css"></script>-->
 
</head>


 
<body>
    
   
  <div class="ticket">
      
    
      <div class="titulo">
      <p class="centrado"><strong><?= $resumen['sucursal']; ?></strong><br>Responsable: <?= $resumen['usuario'] ?> <br><br><strong>   ARQUEO DE CAJA </strong> </p>
      </div>
      <div class="encabezado">
    <p> <br>
     <?= $resumen['fecha_apertura'] ?> </p>
    <p><strong>APERTURA DE CAJA </strong> <br>
        
    Soles: <?= number_format($resumen['apertura_soles'],2) ?><br>
    Dólares: <?= number_format($resumen['apertura_dolares'],2) ?><br></p>
    <p> <strong>INGRESOS: </strong><br>
    
    Ventas Soles:  <?= number_format($resumen['montosoles'],2) ?><br>
    Ventas Dólares: <?= number_format($resumen['montodolares'],2) ?><br>
    Otros Ingresos Soles:  <?= number_format($resumen['ingresosoles'],2) ?><br>
   Otros Ingresos Dólares:  <?= number_format($resumen['ingresodolares'],2) ?><br>
   TOTAL INGRESOS SOLES:  <?= number_format(($resumen['montosoles'] + $resumen['ingresosoles']),2) ?><br>
    TOTAL INGRESOS DÓLARES:  <?= number_format(($resumen['montodolares'] + $resumen['ingresodolares']),2) ?><br>
    </p>
    <p><strong> EGRESOS: </strong><br>
   
    Otros Egresos Soles:  <?= number_format($resumen['egresosoles'],2) ?><br>
    Otros Egresos Dólares: <?= number_format($resumen['egresodolares'],2) ?><br>      
    </p>
    <p><strong>LIQUIDACIÓN </strong><br>
     <strong>Soles:  </strong><?= number_format((($resumen['montosoles'] + $resumen['ingresosoles'] + $resumen['apertura_soles'])- $resumen['egresosoles']),2) ?><br>   
     <strong>Dólares:  </strong><?= number_format((($resumen['montodolares'] + $resumen['ingresodolares'] + $resumen['apertura_dolares']) - $resumen['egresodolares']),2) ?><br>    
    </p><strong> REGISTRO DE OTROS EGRESOS </strong></div>
     
    
      
    <table>
        <thead class="headdetalle">
        <tr>
          <th class="cantidad">Fecha</th>
          <th class="producto">Descripción</th>
          <th class="precio">Pago</th>
          <th class="precio">#</th>
          <th class="precio">Monto</th>
        </tr>
      </thead>
      <tbody class="tbodydetalle">
          <?php

          foreach ($egresosoles as $egresosol){
             
              ?>
        <tr>
            <td class="cantidad"><?php echo $egresosol->getFecha() ?></td>
            <td class="producto"><?php echo $egresosol->getConcepto() ?></td>
            <td class="precio"><?php echo $egresosol->getFormapago() ?></td>
            <td class="precio"><?php echo $egresosol->getMoneda() ?></td>
            <td class="precio"><?php echo number_format($egresosol->getMonto(),2) ?></td>
        </tr>
       
       <?php } 
                foreach ($egresodolares as $egresodolar){
             
              ?>
        <tr>
            <td class="cantidad"><?php echo $egresodolar->getFecha() ?></td>
            <td class="producto"><?php echo $egresodolar->getConcepto() ?></td>
            <td class="precio"><?php echo $egresodolar->getFormapago() ?></td>
            <td class="precio"><?php echo $egresodolar->getMoneda() ?></td>
            <td class="precio"><?php echo number_format($egresodolar->getMonto(),2) ?></td>
        </tr>
        <?php } ?>
     
         </tbody>
      </table>
      <div class="encabezado"><br> <strong> REGISTRO DE OTROS INGRESOS </strong></div>
    
    
    <table>
        <thead class="headdetalle">
        <tr>
          <th class="cantidad">Fecha</th>
          <th class="producto">Descripción</th>
          <th class="precio">Pago</th>
          <th class="precio">#</th>
          <th class="precio">Monto</th>
        </tr>
      </thead>
      <tbody class="tbodydetalle">
          <?php

          foreach ($ingresosoles as $ingresosol){
             
              ?>
        <tr>
            <td class="cantidad"><?php echo $ingresosol->getFecha() ?></td>
            <td class="producto"><?php echo $ingresosol->getConcepto() ?></td>
            <td class="precio"><?php echo $ingresosol->getFormapago() ?></td>
            <td class="precio"><?php echo $ingresosol->getMoneda() ?></td>
            <td class="precio"><?php echo number_format($ingresosol->getMonto(),2) ?></td>
        </tr>
        <?php } 
         foreach ($ingresodolares as $ingresodolar){
             
              ?>
        <tr>
            <td class="cantidad"><?php echo $ingresodolar->getFecha() ?></td>
            <td class="producto"><?php echo $ingresodolar->getConcepto() ?></td>
            <td class="precio"><?php echo $ingresodolar->getFormapago() ?></td>
            <td class="precio"><?php echo $ingresodolar->getMoneda() ?></td>
            <td class="precio"><?php echo number_format($ingresodolar->getMonto(),2) ?></td>
        </tr>
        <?php } ?>
     
     
         </tbody>
      </table>
      
      
 
    <hr>
  
    
    
       
        
    <div class="footerdown">
        
        <p> Fecha de imopresión <?= date('d-m-Y H:m'); ?></p>
    </div>
        
        
        
        
        
    </div>
     
 
    <hr>
 
 <script>
window.print();

</script>   
    
</body>


  
</html>


