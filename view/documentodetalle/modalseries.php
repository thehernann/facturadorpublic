<div class="modal fade modalseries" id="mdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content "> <!-- modal-col-deep-purple-->
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Series del producto</h4>
            </div>
            <div class="modal-body">
                <input id="ident" type="hidden" />
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Series Registradas</label>
                    
                    <input class="form-control" type="text" id="buscarseriemodal" name="buscarseriemodal">
                    
                    <table class="table table-hover" id="tablaseriesregistradas">
                        <thead>
                            <tr>
                                  <th scope="col">#</th>
                                  <th scope="col">N/S</th>
                                  <th scope="col">Agregar</th>
                                  
                                </tr>

                            
                        </thead>
                        <tbody>
                            
                            
                        </tbody>
                        
                        
                    </table>
                    
                </div>
                
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Series agregadas</label>
                    <input id="serieagregadamodal" name="serieagregadamodal" type="text" class="form-control"  />
                    
                    <table class="table table-hover" id="tablaseriesagregadas">
                        <thead>
                            <tr>
                                <th scope="col">Devolver</th>
                                  
                                 <th scope="col">N/S</th>
                                 <th scope="col">#</th>
                                  
                                </tr>

                            
                        </thead>
                        <tbody>
                            
                            
                        </tbody>
                        
                        
                    </table>
                    
                </div>
                
               
           
            </div>
            <div class="modal-footer">
                 
                <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal"> Aceptar</button>
            </div>
    </div>
</div>
</div>

<script>
    
    
    
</script>


