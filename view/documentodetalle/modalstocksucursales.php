<div class="modal fade modalstock" id="mdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content "> <!-- modal-col-deep-purple-->
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Consulta de Stock</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive RespuestaAjaxmodal">
                    

             </div>
           
        </div>
             <div class="modal-footer">

                <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal"> Cerrar</button>
            </div>
    </div>
</div>
</div>

<script>
    
    function searchstock(id){
        $.ajax({
            type: 'POST',
            url: "<?= base_url?>producto/searchstock",
            data: {idprod:id},
            beforeSend: function (xhr) {
                 $('.RespuestaAjaxmodal').html('Cargando ...');        
                    },
            success: function (data) {
                $('.RespuestaAjaxmodal').html(data);
                        
            }
            
        });
        
    };
    
    
</script>


