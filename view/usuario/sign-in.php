
<body class="login-page"> 
    <div class="login-box">
<!--          <div class="logo" >
                    <a href="javascript:void(0);">FACTURADOR<b> ELECTRONICO</b></a>
                    <small>INICIO DE SESION</small>
                    
                </div>-->
        
        
        <div class="card" style="border-radius: 3%; padding: 5%;" >
            <div class="body">
                <div  class="logo center-block">
                    <img id="profile-img"  class="profile-img-card" src="<?=base_url?>images/CBET-LOGOTIPO-FINAL.png" />

                </div>
                <form action="<?= base_url ?>usuario/login" id="formlogin" method="POST"  autocomplete="off" >
                    <!--<div class="msg">Sign in to start your session</div>-->
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons" >person</i>
                        </span>
                        <div class="form-line">
                            <input style="font-size: 20px; padding: 8%;" type="text" class="form-control" name="username" placeholder="Usuario" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" style="font-size: 20px; padding: 8%;" class="form-control" name="password" placeholder="Contraseña" required>
                        </div>
                    </div>
                    <div class="row">
                        <div  id="respuestaAjax"></div>
<!--                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Recuérdame</label>
                        </div>-->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-block bg-indigo  waves-purple btnsession" style="height: 50px; font-size: 16px;"  type="submit" >INICIAR SESION</button>
                        </div>
                    </div>
                    <!--                    <div class="row m-t-15 m-b--20">
                                            <div class="col-xs-6">
                                                <a href="sign-up.html">Register Now!</a>
                                            </div>
                                            <div class="col-xs-6 align-right">
                                                <a href="forgot-password.html">Forgot Password?</a>
                                            </div>
                                        </div>-->
                </form>
               
                
            </div>
        </div>
    </div>
    

