

<section class="content">
    <div class="container-fluid">

        <div class="block-header">
            <!--<h2>FORM EXAMPLES</h2>-->
            <!--    <div class="RespuestaAjax"></div>-->
        </div>
        <!-- Inline Layout | With Floating Label -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <?= $titulo ?>
                            <!--<small>Edición</small>-->
                        </h2>


                    </div>
                    <div class="body">
                        <form action="<?= base_url.$url ?>" method="POST"  id="FormularioAjax" data-form="<?= $data ?>" enctype="multipart/form-data" autocomplete="off" >
                            <div class="row clearfix">
                                <!--<form >-->
                                <input type="hidden" value="<?= $gasto->getId() ?>" id="id" name="id">
                                <input type="hidden" value="planilla" id="tipo" name="tipo">
                                
                                <div class="row">
                                    
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                         <div class="form-group form-float">
                                             <label class="form-label">Fecha </label>
                                             <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                             <div class="form-group">
                                                 <div class="form-line" id="bs_datepicker_container">
                                                     <input type="text" class="form-control" placeholder="Fecha" id="dpfecha" name="dpfecha" value="<?= $fecha ?>" required="">
                                                 </div>
                                             </div>

                                         </div>
                                     </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Entregar a (*)</label>
                                             <select class="form-control show-tick" id="txtentrega" name="txtentrega" required>
                                                 <option value="">-- Seleccione -- </option>
                                                <?php
//                                                var_dump($usuarios);
                                               
                                                foreach ($usuarios as $usuario) {
//                                                    var_dump($usuario->getNombre());
                                                    if($usuario->getNombre() == $gasto->getEntrega()){
                                                        
                                                        echo '<option value="' . $usuario->getNombre() . '" selected>' . $usuario->getNombre() . '</option>';
                                                    }else{
                                                        echo '<option value="' . $usuario->getNombre() . '">' . $usuario->getNombre() . '</option>';
                                                    }
                                                   
                                                        
                                                        
                                                }
                                                ?>

                                            </select>
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Moneda (*)</label>
                                            <select class="form-control show-tick" id="cbmoneda" name="cbmoneda" required>

                                                <?php
                                                $money = array('Soles', 'Dolares');

                                                for ($i = 0; $i < count($money); $i++) {
                                                    if($gasto->getMoneda == $money[$i]){
                                                        
                                                        echo '<option value="' . $money[$i] . '" selected>' . $money[$i] . '</option>';
                                                    }else{
                                                        echo '<option value="' . $money[$i] . '">' . $money[$i] . '</option>';
                                                    }
                                                   
                                                        
                                                        
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="row">
                                    
                                     
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Tipo Pago (*)</label>
                                            
                                           <select class="form-control show-tick" id="cbtipopago" name="cbtipopago">
                                    
                                                    <?php 
                                                    $op = array('Efectivo', 'Cheque','Deposito','Transferencia');

                                                        for ($i=0; $i< count($op); $i++){
                                                            if($gasto->getTipopago() == $op[$i]){
                                                                
                                                                echo '<option value="'.$op[$i].'" selected>'.$op[$i].'</option>';
                                                            }else {
                                                                echo '<option value="'.$op[$i].'" >'.$op[$i].'</option>';
                                                            }

                                                                



                                                    }

                                                    ?>

                                            </select>
                                          
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Entidad </label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtentidad" name="txtentidad" value="<?= $gasto->getEntidad() ?>" >

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Nro. OP / Nro. Cheque</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtnroop" name="txtnroop" value="<?= $gasto->getNop() ?>" >

                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                <div class="row">
                                    
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Concepto (*)</label>
                                            <div class="form-line ">
                                                <textarea class="form-control" id="txtconcepto" name="txtconcepto" required=""><?= $gasto->getConcepto()?></textarea>

                                            </div>
                                        </div>
                                    </div>
                                    
                                




                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
            <!--                            <input type="checkbox" id="remember_me_5" class="filled-in">
                                        <label for="remember_me_5">Remember Me</label>-->
                                        <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">GUARDAR</button>
                                        <a href="<?= base_url ?>gastos/select"  class="btn btn-danger btn-lg m-l-15 waves-effect" id="btncancelar">CANCELAR</a>
                                    </div>
                                </div>
                                <!--</form>-->
                            </div>
                        </form>
                        <div id="respuestaAjax"></div>



                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Inline Layout | With Floating Label -->

    </div>
</section>
