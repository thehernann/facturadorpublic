<div class="modal fade modalajustestock" id="mdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content "> <!-- modal-col-deep-purple-->
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Ajuste de Stock</h4>
            </div>
            <div class="modal-body">
              
                
                  <div class="card">
                    <div class="header ">
                        <label><strong> <?= $producto->getDescripcion().'   - Stock ('.$ultimokardex->getStockactual().')' ?></strong></label>
                        
                    </div>
                    <form action="<?= base_url ?>kardex/ajustestock" method="POST" id="FormularioAjax" autocomplete="off">
                    <div class="body">
 
                            <input type="hidden" value="<?= $_GET['id']; ?>" id="idproducto" name="idproducto">
                            <input type="hidden" value="<?= $producto->getDescripcion(); ?>" id="descripcion" name="descripcion">
                            
                                
                        <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="divstock">
                            <?php $read = '';
                            if($producto->getIncluir()== 'Si'){
                                    $read = 'readonly';
                                
                            } ?>
                                <div class="form-group form-float">
                                    <label class="form-label">Cantidad (*)</label>
                                    <div class="form-line">
                                        <input type="text" class="form-control" id="txtstock" name="txtstock" value="<?= $producto->getStock(); ?>" required="" <?=$read ?>>

                                    </div>
                                </div>

                           
                        </div>
                        
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <!--<div class="form-group form-float">-->
                                <label class="form-label">Tipo Ajuste  </label>
                                <select class="" id="cbajuste" name="cbajuste">
                                  
                                    <?php
                                    $ajuste = array('Positivo', 'Negativo');
                                   
                                    for ($i = 0; $i < count($ajuste); $i++) {
                                       
                                            echo '<option value="' . $ajuste[$i] . '" >' . $ajuste[$i] . '</option>';
                                        
                                    }
                                    ?>

                                </select>

                            <!--</div>-->
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center">
                            <?php
                            $check = '';
                            if($producto->getIncluir()== 'Si'){
                                    $check = 'checked';
                                
                            } ?>

                            <label class="form-label">Series </label>
                            <div class="demo-switch">
                                <div class="switch">
                                    <label>NO<input type="checkbox" <?= $check ?> id="cbincluir" name="cbincluir"><span class="lever"></span>SI</label>
                                </div>
                                
                            </div>
                        </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group form-float">
                                <label class="form-label">Observación (*)</label>
                                <div class="form-line">
                                    <textarea id="txtobservacion" name="txtobservacion" class="form-control" required> </textarea>
                                </div>
                            </div>
                        </div>
                            
                        </div>
                        
                        <div class="row divbtnaceptar">
                        <?php if ($producto->getIncluir() == 'Si') {?>
                        
                            
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
                                
                            <button type="button" class="btn bg-deep-purple waves-effect" id="addserie"><span class="glyphicon glyphicon-plus"></span> Agregar serie</button>
                           </div>
                        
                        
                        <?php } ?>
                        </div>  
                            
<!--                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                
                                 <table class="table table-hover" id="tablaseriesregistradas">
                                <thead>
                                    <tr>
                                          <th scope="col">#</th>
                                          <th scope="col">N/S</th>
                                          <th scope="col">Agregar</th>

                                        </tr>


                                </thead>
                                <tbody>


                                </tbody>


                            </table>
                                
                            </div> 
                           
                            
                        </div>-->
                        
                        
                        <div id="divseries" class="row">
                            
                            <?php 
//                                foreach ($series as $serie):
//                                    echo '<div class="row">';
//                                   echo '<div class = "col-lg-5 col-md-5 col-sm-5 col-xs-6">';
//                                   echo '<input class = "form-control" value="'.$serie->getSerie().'" id="serie" name="serie[]"></input>';                 
//                                   echo '</div>';  
//                                   
////                                    echo '<div class = "eliminar col-lg-2 col-md-2 col-sm-2 col-xs-6">';
////                                   echo '<button type="button" class="btn btn-danger waves-effect"> <span class="glyphicon glyphicon-remove"></span> </button>';                 
////                                   echo '</div>';   
//                                   echo '</div>';
//                                   
//                                   
//                                endforeach;
                            
                            
                            ?>
                            
                        </div>
                            
                    
                    </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary  waves-effect"  >GUARDAR</button>
                        </div>
                        
                        
                      </form>
                </div>
                
                
               
                
                
                
                
                
                
                
            </div>
            <div class="modal-footer ">

                <button type="button" class="btn btn-link  waves-effect"  data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>


<script>

  
     
    $(function () {
        
        var serie = []; 
        
        var id = '<?= $ultimokardex->getId() ?>';
        
        $.ajax({
            type: 'POST',
            url: '<?=base_url ?>serieProducto/select',
            data: { id :id },
            dataType: 'JSON',
            success: function (data) {
//                var datos = eval(data);
//                console.log(datos);
                serie = data;
//               datos.forEach(function (data, index) {
//                   console.log(data.serie);
//                   
//               });
              
            }
            
            
        });
        
       
//   $(document).on('click','#btnaceptarserie', function (){
//      
//        $("input[name='serie[]']").each(function (indice, elemento) {
//                // console.log('El elemento con el índice '+indice+' contiene '+$(elemento).val());
//               var s = {
//                   id:0,
//                   serie: $(elemento).val()
//                   
//                };
//                
//                
//                console.log(s)
//               serie.push(s);     
//             
//         });
//         
//         console.log(serie);
//   });
  
   $(document).on('change','#cbincluir', function (event){
       
              $.ajax({
            type: 'POST',
            url: '<?=base_url ?>serieProducto/select',
            data: { id :id },
            dataType: 'JSON',
            success: function (data) {
//                var datos = eval(data);
//                console.log(datos);
                serie = data;
//               datos.forEach(function (data, index) {
//                   console.log(data.serie);
//                   
//               });
              
            }
            
            
        });
        
          if(!$(this).is(':checked') && serie.length > 0){
            swal('Imposible realizar la operacion, el producto tiene series asignadas','','info');
            $(this).prop( "checked",true );
            return false;
        }else {
            var stocka = '<?= $ultimokardex->getStockactual() ?>';
            if(stocka > 0 ){
                swal('Imposible realizar la operacion, el producto cuenta con stock','','info');
                $(this).prop( "checked",false );
                return false;
            }
            
            
            
        } 
        
        if($(this).is(':checked')){
//            console.log('si');
            $('.divbtnaceptar').empty();
            $('#txtstock').attr('readonly',true);
            
              var divcol = document.createElement('div');
            divcol.setAttribute('class','col-lg-2 col-md-2 col-sm-4 col-xs-4');
            
            var button = document.createElement('button');
            button.setAttribute('class','btn bg-deep-purple waves-effect');
            button.setAttribute('id','addserie');
            button.innerHTML='<span class="glyphicon glyphicon-plus" ></span>Agregar Serie';
            
            divcol.appendChild(button);
            
            $('.divbtnaceptar').append(divcol);
            $('#txtstock').val(0);
            
        }else {
//            console.log('no');
             $('.divbtnaceptar').empty();
             $('#divseries').empty();
            $('#txtstock').removeAttr('readonly',true);
        }

      
   });
   
   $(document).on('change','#cbajuste', function (){
       
         var id = '<?= $ultimokardex->getId() ?>';
        $.ajax({
            type: 'POST',
            url: '<?=base_url ?>serieProducto/select',
            data: { id :id },
            dataType: 'JSON',
            success: function (data) {
//                var datos = eval(data);
//                console.log(datos);
                serie = data;
//               datos.forEach(function (data, index) {
//                   console.log(data.serie);
//                   
//               });
              
            }
            
            
        });
//        console.log($(this).val());
        if($(this).val() == 'Negativo' && $('#cbincluir').is(':checked')){
           
            $('.divbtnaceptar').empty();
            $('#divseries').empty();
            
            
            serie.forEach(function (data, index){
            var divcol = document.createElement('div');
            divcol.setAttribute('class','col-lg-5 col-md-5 col-sm-5 col-xs-6');
            var diveliminar = document.createElement('div');
            diveliminar.setAttribute('class','eliminar col-lg-2 col-md-2 col-sm-2 col-xs-6');
            diveliminar.setAttribute('index',index);
            var input = document.createElement('input');
//           divcol.createTextNode(input);
           input.setAttribute('type','text');
           input.setAttribute('class','form-control');
           input.setAttribute('id','serie[]');
           input.setAttribute('name','serie[]');
       
           input.setAttribute('value',data.serie);
           input.setAttribute('readonly','readonly');
           input.setAttribute('placeholder','Serie');
           
           divcol.appendChild(input);    
           
             var span = document.createElement('span');
            span.setAttribute('class','glyphicon glyphicon-remove');


            ///btn eliminar //////

            var btn = document.createElement('button');
            btn.setAttribute('type','button');
            btn.setAttribute('class','btn btn-danger waves-effect');
 //           btn.setAttribute('id','btneliminar');


            btn.appendChild(span);
            diveliminar.appendChild(btn);


            var div = document.createElement('div');
            div.setAttribute('class','row');
            div.appendChild(divcol); 
 //           div.appendChild(divcol2);
 //      
 //          
            div.appendChild(diveliminar);
           
           
            
            $('#divseries').append(div);
                
            });
            $('#txtstock').val(0);
            
        }
        if($(this).val() == 'Positivo' && $('#cbincluir').is(':checked')){
            $('#divseries').empty();
            
            var divcol = document.createElement('div');
            divcol.setAttribute('class','col-lg-2 col-md-2 col-sm-4 col-xs-4');
            
            var button = document.createElement('button');
            button.setAttribute('class','btn bg-deep-purple waves-effect');
            button.setAttribute('id','addserie');
            button.innerHTML='<span class="glyphicon glyphicon-plus" ></span>Agregar Serie';
            
            divcol.appendChild(button);
            
            $('.divbtnaceptar').append(divcol);
            $('#txtstock').val(0);
        }
       
   });
 
  //////////////////////////// ////////////////////////////////////////////////
  ///////////////// Precios //////////////////
          
        $(document).on('click',"#addserie", function (e) {
//              var newDiv = document.createElement("div"); 
//              var newContent = document.createTextNode("Hola!¿Qué tal?"); 
//              newDiv.appendChild(newContent); //añade texto al div creado. 
//
//              // añade el elemento creado y su contenido al DOM 
//              var currentDiv = document.getElementById("div1"); 
//              document.getElementById('#tabla').innerHTML (newDiv, currentDiv);
           
           
           ////////////////////////////// div /////////////
           
           e.preventDefault();
           var divcol = document.createElement('div');
           divcol.setAttribute('class','col-lg-5 col-md-5 col-sm-5 col-xs-6');
           
           
           
                    
           
           var diveliminar = document.createElement('div');
           diveliminar.setAttribute('class','eliminar col-lg-2 col-md-2 col-sm-2 col-xs-6');
           
           //////////////////////////////////////////////////////////////////
           /////////// elemento nombre ///
           
                
              ////// input ///////////
           var input = document.createElement('input');
//           divcol.createTextNode(input);
           input.setAttribute('type','text');
           input.setAttribute('class','form-control');
           input.setAttribute('id','serie[]');
           input.setAttribute('name','serie[]');
        
           input.setAttribute('placeholder','Serie');
           
           divcol.appendChild(input);
           //////////////////////////////////////////////
           //
                /////////// elemento descripcion ///
           
                
              ////// input ///////////
//           var input = document.createElement('input');
////           divcol.createTextNode(input);
//           input.setAttribute('type','text');
//           input.setAttribute('class','form-control');
//           input.setAttribute('id','preciomultiple[]');
//           input.setAttribute('name','preciomultiple[]');
//           input.setAttribute('form','FormularioProducto');
//           input.setAttribute('placeholder','Precio');
//           
//           divcol2.appendChild(input);
           

           //////////// elemento acciones ////////////
           ///////// span //////
           
           var span = document.createElement('span');
           span.setAttribute('class','glyphicon glyphicon-remove');
           
           
           ///btn eliminar //////
           
           var btn = document.createElement('button');
           btn.setAttribute('type','button');
           btn.setAttribute('class','btn btn-danger waves-effect');
//           btn.setAttribute('id','btneliminar');
           
          
           btn.appendChild(span);
           diveliminar.appendChild(btn);
           
           
           var div = document.createElement('div');
           div.setAttribute('class','row');
           div.appendChild(divcol); 
//           div.appendChild(divcol2);
//      
//          
           div.appendChild(diveliminar);
           
           
            
            $('#divseries').append(div);
            
//           var fila = $("#tabla  div:eq(0)").clone();
//           fila.appendTo("#tabla");
//            
//            
//            /////////////////////////////////////////////
//            var op = $('#unidad').clone(true,true);
//            
//            var selectedValue = $("#unidad option:selected").val();
//            op.find("option[value = '" + selectedValue + "']").attr("selected", "selected");
//            op.attr('class','form-control show-tick');
////            $('#tabla').append(op);
//            op.appendTo('#select');


//            var serieact = '<?= count($series) ?>';
//            console.log(serieact);
//            
            var serieing=0;
            $("input[name='serie[]']").each(function (indice, elemento) {
                // console.log('El elemento con el índice '+indice+' contiene '+$(elemento).val());
               
               serieing ++;
            });
            
//            console.log(serieing);
//            
//             var ajuste = serieing - serieact;
            $('#txtstock').val(serieing);
        });
        
        
  
  //////////////////////////////////////////////////////////////
  

        // Evento que selecciona la fila y la edivmina 
        $(document).on("click", ".eliminar", function () {
           
            var parent = $(this).parents().get(0);
//            var parent1 = $(this).parents().get(1);
            $(parent).remove();
//            $(parent1).remove();

            
            
            var index = $(this).attr('index');
            
            var serieing=0;
            var ajuste =0;
            $("input[name='serie[]']").each(function (indice, elemento) {
                // console.log('El elemento con el índice '+indice+' contiene '+$(elemento).val());
               
               serieing ++;
            });
            
            if($('#cbajuste').val() == 'Negativo'){
                var serieact = serie.length;
//                console.log('---serie actual--');
//                console.log(serieact);
                ajuste = (serieing - serieact) * -1;
                
                
//                serie.splice(index,1);
                
                
            }else{
                
                ajuste = serieing;
            }
            
            
         
            
            
             
            $('#txtstock').val(ajuste);



        });
       
        
//        $(document).on("click", ".btneliminar", function () {
//            alert('hola');
//        });
    });






</script>
