
<?php 
$url= '';
$urlup='';
$urldel='';
if($tipo=='producto'){
    $urlcrear = 'producto/crearprod';
    $urlup = 'producto/cargarprod';
    $urldel = base_url.'producto/deleteprod';
}
if($tipo=='servicio') {
    $urlcrear = 'producto/crearserv';
    $urlup = 'producto/cargarserv';
    $urldel = base_url.'producto/deleteserv';
}

?>
<section class="content">
    <!--<div class="container-fluid">-->


<div class="block-header">
<!--    <br>-->
    <div class="RespuestaAjax"></div>
</div>
<!-- Basic Examples -->

<!-- #END# Basic Examples -->
<!-- Exportable Table -->
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <?=  $tipo=='producto' ?  'LISTA DE ARTICULOS' : 'LISTA DE SERVICIOS' ?> (Busqueda avanzada)
                                <a href="<?= base_url.$urlcrear?>"  class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> NUEVO</a>
                               
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
<!--                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>-->
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            
                            <div class="card">
                                
                                <div class="body">
                                    <form id="FormProductos" method="post"  autocomplete="off"> <!--action="<?= base_url.'producto/searchadvanced'?>" -->
                                    <div class="row">
                               
                                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Código/Descripción </label>
                                            <div class="form-line ">
                                                <input type="text" class="form-control" id="txtnombre" name="txtnombre">

                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Código alt./Descripción Ext.</label>
                                            <div class="form-line ">
                                                <input type="text" class="form-control" id="txtnombrealt" name="txtnombrealt">

                                            </div>
                                        </div>
                                    </div>
                                        
                                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Detalle</label>
                                            <div class="form-line ">
                                                <input type="text" class="form-control" id="txtdetalle" name="txtdetalle">

                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Detalle Dos</label>
                                            <div class="form-line ">
                                                <input type="text" class="form-control" id="txtdetalledos" name="txtdetalledos">

                                            </div>
                                        </div>
                                    </div>
                                        
                                     <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Categoria </label>
                                             <select class="form-control show-tick" id="cbcategoria" name="cbcategoria" >
                                                    <option value="">- Categoria -</option>
                                                    <?php
                                                    foreach ($categorias as $categoria) {
                                                        
                                                            echo '<option value="' . $categoria->getId() . '">' . $categoria->getDescripcion() . '</option>';
                                                      
                                                    }
                                                    ?>

                                                </select>
                                        </div>
                                    </div>
                                     <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Linea </label>
                                            <select class="form-control show-tick" id="cblinea" name="cblinea" >
                                                    <option value="" >- Linea - </option>
                                                    <?php
                                                    foreach ($lineas as $linea) {
                                                       
                                                            echo '<option value="' . $linea->getId() . '">' . $linea->getDescripcion() . '</option>';
                                                        
                                                    }
                                                    ?>

                                                </select>
                                        </div>
                                    </div>
                                     <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Marca </label>
                                              <select class="form-control show-tick" id="cbmarca" name="cbmarca" >
                                                    <option value="">- Marca -</option>
                                                    <?php
                                                    foreach ($marcas as $marca) {
                                                       
                                                            echo '<option value="' . $marca->getId() . '">' . $marca->getDescripcion() . '</option>';
                                                        
                                                    }
                                                    ?>

                                                </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><span class="glyphicon glyphicon-tag"></span> Etiquetas </label>
                                                <div class="form-line ">

                                                <input type="text" name="filtro" id="filtro" class="form-control"/>

                                        </div>
                                    </div>
                                </div>
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
                                        <label class="">Acciones</label>
                                        <div class="form-group form-float">
                                            
                                            <button type="submit" class="btn btn-primary waves-effect" >Buscar</button>
                                        </div>
                                        
                                </div>
                                    </div>
                                    </form>     
                                    <div id="respuestaAjax" ></div>    
                                    
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable" id="tablapersona">
                                    <thead>
                                        <tr>
                                            <th>Nro°</th>
                                            <th>Codigo</th>
                                            <th>Marca</th>
                                             <th>Descripción</th>
                                             <th>Codigo Alter.</th>
                                            <th>Detalle</th>
                                            <th>Detalle dos</th>
                                           <th>Stock</th>
                                            <th>Precio</th>
                                            <th>Linea</th>
                                            <th>Categoria</th>
                                            <th>Etiquetas</th>

                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
<!--                                    <tfoot>
                                        <tr>
                                            <th>Nro°</th>
                                            <th>Codigo</th>
                                            <th>Descripción</th>
                                            <th>Stock</th>
                                            <th>P.C</th>
                                            <th>P.V</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </tfoot>-->
                                    <!--<tbody>-->
                                        
                                        <?php 
//                                        $i=1;
//                                        foreach ($productos as $producto){ 
//                                            echo '<tr>';
//                                            echo '<td>'.$i.'</td>';
//                                            echo '<td>'.$producto->getCodigo().'</td>';
//                                            echo '<td>'.$producto->getDescripcion().'</td>';
//                                            echo '<td>'.$producto->getStock().'</td>';
//                                            echo '<td>'.$producto->getPrecioc().'</td>';
//                                            echo '<td>'.$producto->getPreciov().'</td>';
//                                           
//                                            echo '<td><a href="'.base_url.$urlup.'&id='.$producto->getId().'" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>'
//                                            .' <button  type="button" class="btn btn-danger" onclick=eliminar(' . $producto->getId() . ',"' . $urldel . '","delete")><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>';
//                                            echo '</tr>';
//                                            $i++;
//                                        }
                                        ?>
                                      
                                     
                                    <!--</tbody>-->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- #END# Exportable Table --> 
   </div>
</section>
<script>
$(document).ready(function (){
  
         var example_table = $('#tablapersona').DataTable({
//             "responsive": true,
//            "processing": true,
//            "serverSide": true,
            "searching": false,
            
            
            'ajax':{
                "type":"POST",
                "url":'<?= base_url.'producto/searchadvanced'?>',
                "data" :  {
                txtnombre: $('#txtnombre').val(),
                txtnombrealt: $('#txtnombrealt').val(),
                txtdetalle: $('#txtdetalle').val(),
                txtdetalledos: $('#txtdetalledos').val(),
                
                cbcategoria: $('#cbcategoria').val(),
                cbmarca: $('#cbmarca').val(),
                cblinea: $('#cblinea').val(),
                filtro: $('#filtro').val(),
                urlup: '<?= $urlup?>',
                urldel: '<?= $urldel?>'
              },
//              "dataSrc": ""
                
            },
            "columns":[
                {"data" : "item"},
                {"data" : "codigo"},
                {"data" : "marca"},
                {"data" : "descripcion"},
                {"data" : "codigoalt"},
                {"data" : "observacion"},
                {"data" : "detalledos"},
                
                {"data" : "stock"},
                {"data" : "preciov"},
                {"data" : "linea"},
                {"data" : "categoria"},
                {"data" : "caracteristica"},
                {"data" : "acciones"}
                
            ],
//            "ordering": false
        });
        example_table.ajax.reload();
    
    
   
//    
    $('#filtro').tokenfield({
        
        autocomplete:{
            minLength: 1,
            source:  function (request, response) {
                                $.ajax({
                                    type: 'POST',
                                    url: '<?= base_url . 'caracteristica/search' ?>',
                                    data: {query: request.term},
                                    success: response,
//                                    error: function (jqXHR, textStatus, errorThrown) {
//                                        alert("error handler!");
//                                        alert(errorThrown);
//                                    },
                                    dataType: 'json',
                                    delay: 100
                                });
    //                          .done(function (data){
    //                      console.log('done'+data);
    ////                      console.log('select dano'+ui);
    //                      
    //                     
    //                  });
                            },
                            select: function (event, ui) {
                            event.preventDefault();
                             $('#FormProductos').submit();
                            
                            },
                            
            showAutocompleteOnFocus: true,
        }
        
        
    });
//    $('#filtro').tokenfield({
//  autocomplete: {
//    source: ['red','blue','green','yellow','violet','brown','purple','black','white'],
//    delay: 100
//  },
//  showAutocompleteOnFocus: true
//});
    $(document).on('submit','#FormProductos', function (e){
        e.preventDefault();
        console.log($('#filtro').val());
       
        $("#tablapersona").dataTable().fnDestroy();
        
          var example_table = $('#tablapersona').DataTable({
//             "responsive": true,
//            "processing": true,
//            "serverSide": true,
            "searching": false,
            
            'ajax':{
                "type":"POST",
                "url":'<?= base_url.'producto/searchadvanced'?>',
                "data" :  {
                txtnombre: $('#txtnombre').val(),
                txtnombrealt: $('#txtnombrealt').val(),
                txtdetalle: $('#txtdetalle').val(),
                txtdetalledos: $('#txtdetalledos').val(),
                

                cbcategoria: $('#cbcategoria').val(),
                cbmarca: $('#cbmarca').val(),
                cblinea: $('#cblinea').val(),
                filtro: $('#filtro').val(),
                urlup: '<?= $urlup?>',
                urldel: '<?= $urldel?>'
              },
//              "dataSrc": ""
                
            },
            "columns":[
                {"data" : "item"},
                {"data" : "codigo"},
                {"data" : "marca"},
                {"data" : "descripcion"},
                {"data" : "codigoalt"},
                {"data" : "observacion"},
                {"data" : "detalledos"},
                
                {"data" : "stock"},
                {"data" : "preciov"},
                {"data" : "linea"},
                {"data" : "categoria"},
                {"data" : "caracteristica"},
                {"data" : "acciones"}
                
            ],
//            "ordering": false
        });
    });
    
    $(document).on('change','#cbcategoria', function (e){
        e.preventDefault();
        $('#FormProductos').submit();
    
    
    
    });
    $(document).on('change','#cbmarca', function (e){
        e.preventDefault();
        $('#FormProductos').submit();
    
    
    
    });
    $(document).on('change','#cblinea', function (e){
        e.preventDefault();
        $('#FormProductos').submit();
    
    
    
    });
 
    
    $(document).on('tokenfield:removedtoken' ,'#filtro', function (e) {
         e.preventDefault();
        $('#FormProductos').submit();
    });
//    $(document).on('tokenfield:createdtoken' ,'#filtro', function (e) {
////         e.preventDefault();
//        $('#FormProductos').submit();
//        alert('sdf');
//    });
    
});

</script>

