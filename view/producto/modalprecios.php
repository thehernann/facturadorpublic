<div class="modal fade modalprecios" id="mdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content "> <!-- modal-col-deep-purple-->
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Agregar precios</h4>
            </div>
            <div class="modal-body">
              
                
                  <div class="card">
                    <div class="header ">
                        <label><strong>Precios de venta</strong></label>
                        
                    </div>
                    
                    <div class="body">
                        <div id="divprecios">
                            
                        </div>
                        <button type="button" class="btn bg-deep-purple waves-effect" id="addprecios"><span class="glyphicon glyphicon-plus"></span></button>
                    </div>
                </div>
                
                
               
                
                
                
                
                
                
                
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal"><span class="glyphicon glyphicon-check"></span> Aceptar</button>
            </div>
        </div>
    </div>
</div>


<script>

    
     <?php foreach($preciosmultiples as $precios){ ?>
             ////////////////////////////// div /////////////
           
           var divcol = document.createElement('div');
           divcol.setAttribute('class','col-lg-5 col-md-5 col-sm-5 col-xs-6');
           
           var divcol2 = document.createElement('div');
           divcol2.setAttribute('class','col-lg-5 col-md-5 col-sm-5 col-xs-6');
           
                    
           
           var diveliminar = document.createElement('div');
           diveliminar.setAttribute('class','eliminar col-lg-2 col-md-2 col-sm-2 col-xs-6');
           
           //////////////////////////////////////////////////////////////////
           /////////// elemento nombre ///
           
                
              ////// input ///////////
           var input = document.createElement('input');
//           divcol.createTextNode(input);
           input.setAttribute('type','text');
           input.setAttribute('class','form-control');
           input.setAttribute('id','cantidadpreciomultiple[]');
           input.setAttribute('name','cantidadpreciomultiple[]');
           input.setAttribute('form','FormularioProducto');
           input.setAttribute('value','<?= $precios->getCantidad(); ?>');
           
           input.setAttribute('placeholder','Cantidad');
           
           divcol.appendChild(input);
           //////////////////////////////////////////////
           //
                /////////// elemento descripcion ///
           
                
              ////// input ///////////
           var input = document.createElement('input');
//           divcol.createTextNode(input);
           input.setAttribute('type','text');
           input.setAttribute('class','form-control');
           input.setAttribute('id','preciomultiple[]');
           input.setAttribute('name','preciomultiple[]');
           input.setAttribute('form','FormularioProducto');
           input.setAttribute('value','<?= $precios->getPrecio(); ?>');
           input.setAttribute('placeholder','Precio');
           
           divcol2.appendChild(input);
           

           //////////// elemento acciones ////////////
           ///////// span //////
           
           var span = document.createElement('span');
           span.setAttribute('class','glyphicon glyphicon-remove');
           
           
           ///btn eliminar //////
           
           var btn = document.createElement('button');
           btn.setAttribute('type','button');
           btn.setAttribute('class','btn btn-danger waves-effect');
//           btn.setAttribute('id','btneliminar');
           
          
           btn.appendChild(span);
           diveliminar.appendChild(btn);
           
           
           var div = document.createElement('div');
           div.setAttribute('class','row');
           div.appendChild(divcol);
           div.appendChild(divcol2);
      
           div.appendChild(diveliminar);
           
           
            
            $('#divprecios').append(div);
    
             
     <?php } ?>   

    $(function () {
        
    
        
 
  //////////////////////////// ////////////////////////////////////////////////
  ///////////////// Precios //////////////////
          
        $(document).on('click',"#addprecios", function () {
//              var newDiv = document.createElement("div"); 
//              var newContent = document.createTextNode("Hola!¿Qué tal?"); 
//              newDiv.appendChild(newContent); //añade texto al div creado. 
//
//              // añade el elemento creado y su contenido al DOM 
//              var currentDiv = document.getElementById("div1"); 
//              document.getElementById('#tabla').innerHTML (newDiv, currentDiv);
           
           
           ////////////////////////////// div /////////////
           
           var divcol = document.createElement('div');
           divcol.setAttribute('class','col-lg-5 col-md-5 col-sm-5 col-xs-6');
           
           var divcol2 = document.createElement('div');
           divcol2.setAttribute('class','col-lg-5 col-md-5 col-sm-5 col-xs-6');
           
                    
           
           var diveliminar = document.createElement('div');
           diveliminar.setAttribute('class','eliminar col-lg-2 col-md-2 col-sm-2 col-xs-6');
           
           //////////////////////////////////////////////////////////////////
           /////////// elemento nombre ///
           
                
              ////// input ///////////
           var input = document.createElement('input');
//           divcol.createTextNode(input);
           input.setAttribute('type','text');
           input.setAttribute('class','form-control');
           input.setAttribute('id','cantidadpreciomultiple[]');
           input.setAttribute('name','cantidadpreciomultiple[]');
           input.setAttribute('form','FormularioProducto');
           input.setAttribute('placeholder','Cantidad');
           
           divcol.appendChild(input);
           //////////////////////////////////////////////
           //
                /////////// elemento descripcion ///
           
                
              ////// input ///////////
           var input = document.createElement('input');
//           divcol.createTextNode(input);
           input.setAttribute('type','text');
           input.setAttribute('class','form-control');
           input.setAttribute('id','preciomultiple[]');
           input.setAttribute('name','preciomultiple[]');
           input.setAttribute('form','FormularioProducto');
           input.setAttribute('placeholder','Precio');
           
           divcol2.appendChild(input);
           

           //////////// elemento acciones ////////////
           ///////// span //////
           
           var span = document.createElement('span');
           span.setAttribute('class','glyphicon glyphicon-remove');
           
           
           ///btn eliminar //////
           
           var btn = document.createElement('button');
           btn.setAttribute('type','button');
           btn.setAttribute('class','btn btn-danger waves-effect');
//           btn.setAttribute('id','btneliminar');
           
          
           btn.appendChild(span);
           diveliminar.appendChild(btn);
           
           
           var div = document.createElement('div');
           div.setAttribute('class','row');
           div.appendChild(divcol);
           div.appendChild(divcol2);
      
           div.appendChild(diveliminar);
           
           
            
            $('#divprecios').append(div);
            
//           var fila = $("#tabla  div:eq(0)").clone();
//           fila.appendTo("#tabla");
//            
//            
//            /////////////////////////////////////////////
//            var op = $('#unidad').clone(true,true);
//            
//            var selectedValue = $("#unidad option:selected").val();
//            op.find("option[value = '" + selectedValue + "']").attr("selected", "selected");
//            op.attr('class','form-control show-tick');
////            $('#tabla').append(op);
//            op.appendTo('#select');
        });
  
  //////////////////////////////////////////////////////////////
  

        // Evento que selecciona la fila y la edivmina 
        $(document).on("click", ".eliminar", function () {
           
            var parent = $(this).parents().get(0);
//            var parent1 = $(this).parents().get(1);
            $(parent).remove();
//            $(parent1).remove();
        });
        
        
        
//        $(document).on("click", ".btneliminar", function () {
//            alert('hola');
//        });
    });






</script>
