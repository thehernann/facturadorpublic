<style>
    .header {
        border-radius: 5px;
       
    }
    .well{
        border: none;
        background-color: #ffffff;
    }
    
</style>


<section class="content">
    <!--<div class="container-fluid" >-->

        <div class="block-header">
            <!--<h2>FORM EXAMPLES</h2>-->
                <div id="RespuestaAjax"></div>
        </div>
        <!-- Inline Layout | With Floating Label -->
        <div class="row clearfix ">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header ">
                        <!--<h4 class="text-center">-->
                        <div class="row">
                            
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                                
                                <h5 class="text-primary"><span class="glyphicon glyphicon-shopping-cart"></span> EMITIR COMPROBANTE</h5>
                            </div>
                            
   
                        </div> 
   
                        <input id="print" type="hidden" value="<?= base_url ?>documento/printticket" >
                        <input id="tipo" type="hidden" value="<?= $tipodoc ?>" >
                        

                    </div>
                    <div class="body">
                        
                        <a class="btn  waves-effect m-b-15" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false"
                           aria-controls="collapseExample" title="Mostrar opciones">
                               <div class="demo-google-material-icon"> <i class="material-icons">menu</i> <span class="icon-name"></span> </div>
                        </a>
                       
<!--                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 "> -->
<a  class="btn bg-teal waves-effect m-b-15" data-toggle="modal" data-target=".modaldocserie" title="Serie Documento"><div class="demo-google-material-icon"> <i class="material-icons">attachment</i> <span class="icon-name"></span> </div></a>
                            <!--</div>--> 
                            <!--<div class="col-lg-1 col-md-1 col-sm-1 col-xs-3">-->
                                    <!--                                <div class="form-group form-float">-->
                                    <?php $check = 'checked';
                                     if($documento->getIncigv()== '0' ){
                                        $check ='';
                                    } ?>
                                    <input type="checkbox" id="incigv" form="FormularioAjaxDocumento" name="incigv" <?= $check ?> />
                                    <label for="incigv">Inc. IGV</label>
                                    <!--</div>-->
                            <!--</div>-->
                        
                        
                        <form action="<?= base_url ?>documento/insertsale" method="POST"  id="FormularioAjaxDocumento" data-form="insert" enctype="multipart/form-data" autocomplete="off" >
                            
                            <div class="collapse" id="collapseExample">
                            <div class="well">   
                            <div class="row clearfix">
                                <!--<form >-->
                                <div class="row">
                                   
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float">
                                            <label >Tipo Doc. (*)</label>
                                            <select class="form-control" id="tipodoc" name="tipodoc" required="" onchange="cargarserie('<?= base_url.'sucursaldocumento/cargarserie' ?>','<?= base_url.'documento/selectmaxnro' ?>');">
                                                <!--<option value="" class="">- Documento  -</option>-->
                                                <?php 

                                                foreach ($tipodocventa as $tipodoc){
                                                    $print = '';
                                                    switch ($tipodoc->getTipodoc()) {
                                                        case 'Factura':
                                                            $print= 'Factura';


                                                            break;
                                                        case 'Boleta':
                                                            $print= 'Boleta';


                                                            break;
                                                        case 'nota_venta':
                                                            $print= 'Nota de Venta';


                                                            break;

                                                        default:
                                                            break;
                                                    }

                                                        echo '<option value="'.$tipodoc->getTipodoc().'" >'.$print.'</option>';


                                                }

                                                ?>

                                        </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float" id="divserie">
                                            <label >Serie (*)</label>
                                            <div id="divcargarserie">
                                                <select class="form-control show-tick" id="cbserie" name="cbserie" required="">
                                                    <!--<option value="" class="">- Doc-Serie  -</option>-->
                                                    <?php
                                                    foreach ($seriesdoc as $docsucur) {
    
                                                        echo '<option value="' . $docsucur->getSerie() . '">' . $docsucur->getSerie() . '</option>';
                                                    }
                                                    ?>

                                                </select>
                                                </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Nro: (*)</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtnro" name="txtnro" value="<?= $numero ?>" required="" >
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Moneda (*)</label>
                                            <select class="form-control show-tick" id="cbmoneda" name="cbmoneda" required="">

                                                <?php
                                                $pred = array('Soles', 'Dolares');
                                                    
                                                for ($i = 0; $i < count($pred); $i++) {
                                                    if($documento->getMoneda() == $pred[$i]){
                                                        echo '<option value="' . $pred[$i] . '" selected >' . $pred[$i] . '</option>';
                                                    }else{
                                                        echo '<option value="' . $pred[$i] . '"  >' . $pred[$i] . '</option>';
                                                    }
                                                    
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                        <div class="form-group form-float">
                                            <!--                                <h2 class="card-inside-title">Aniversario</h2>-->
                                            <label for="dpfechaemision">Fecha Emisión (*)</label>
                                           
                                                <div class="form-line" id="bs_datepicker_container">
                                                    <input type="text" class="form-control" placeholder="Fecha Emisión" id="dpfechaemision" name="dpfechaemision" value="<?= date('d/m/Y') ?>" required="">
                                                </div>
                                            

                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                        <div class="form-group form-float">
                                            <!--                                <h2 class="card-inside-title">Aniversario</h2>-->
                                            <label for="dpfechavencimiento">Fecha Venc. (*)</label>
                                            
                                                <div class="form-line" id="bs_datepicker_container">
                                                    <input type="text" class="form-control" placeholder="Fecha Vencimiento" id="dpfechavencimiento" name="dpfechavencimiento" value="<?= date('d/m/Y') ?>" required="">
                                                </div>
                                            

                                        </div>
                                    </div>




                                </div>
                                <div class="row">

                                    

                                   

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float">
                                            <label >Tipo OP (*)</label>
                                            <select class="form-control show-tick" data-live-search="true" id="cbtipoop" name="cbtipoop" required="">

                                                <?php
                                                foreach ($transactions as $transaction) {

                                                    echo '<option value="' . $transaction->getId() . '">' . $transaction->getDescripcion() . '</option>';
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float">
                                            <label>Vendedor (*) </label>
                                            <select class="form-control show-tick" id="cbvendedor" name="cbvendedor" required="">
                                                <option value="">- Vendedor (*) -</option>
                                                <?php
                                                foreach ($usuarios as $usuario) {
                                                    
                                                    if($documento->getIdusuario() != 0){
                                                        
                                                  
                                                    if($documento->getIdusuario() == $usuario->getId()){
                                                         echo '<option value="' . $usuario->getId() . '" selected>' . $usuario->getNombre() . ' ' . $usuario->getApellidop() . '</option>';
                                                    } else {
                                                        echo '<option value="' . $usuario->getId() . '" >' . $usuario->getNombre() . '</option>';
                                                    }
                                                        
                                                     }else{
                                                         
                                                         if ($usuario->getId() == $_SESSION['id']) {

                                                            echo '<option value="' . $usuario->getId() . '" selected>' . $usuario->getNombre() . ' ' . $usuario->getApellidop() . '</option>';
                                                        } else {
                                                            echo '<option value="' . $usuario->getId() . '" >' . $usuario->getNombre() . '</option>';
                                                        }
                                                     }
                                                    
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float">
                                            <label >Tipo Venta (*)</label>
                                            <select class="form-control show-tick" id="cbtipoventa" name="cbtipoventa" required="">

                                                <?php
                                                $pred = array('Contado', 'Credito');
                                                $predo = array('Contado', 'Crédito');
                                                
                                                for ($i = 0; $i < count($pred); $i++) {

                                                    echo '<option value="' . $pred[$i] . '" >' . $predo[$i] . '</option>';
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Sujeto a</label>
                                            <select class="form-control show-tick" id="cbsujetoa" name="cbsujetoa">
                                                <option value="">- Sujeto a  -</option>
                                                <?php
                                                $pred = array('Detracción');

                                                for ($i = 0; $i < count($pred); $i++) {

                                                    echo '<option value="' . $pred[$i] . '" >' . $pred[$i] . '</option>';
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div id="cargartipopago">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float">
                                            <label>Tipo de Pago (*)</label>
                                            <select class="form-control show-tick" id="cbtipopago" name="cbtipopago" required="">

                                                <?php
                                                $pred = array('Efectivo', 'Tarjeta', 'Transferencia','Mixto');

                                                for ($i = 0; $i < count($pred); $i++) {

                                                    echo '<option value="' . $pred[$i] . '" >' . $pred[$i] . '</option>';
                                                }
                                                ?>

                                            </select>
                                        </div>
                                            
                                            <div id ="msjalert"></div>
                                        </div>
                                    </div>


                                    
                                     <div id="cargarpago"></div>
                                     
                                </div>
                                <div class="row">

                                    

                                    
                                   
                                   

<!--                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtnroop" name="txtnroop" value="" readonly="" >
                                                <label class="form-label">Nro OP</label>
                                            </div>
                                        </div>
                                    </div>-->

                                


                                </div>
                              
                                <!--                        <div class="h3 text-danger">DATOS DEL CLIENTE</div>
                                                        <HR>-->
                                <div class="row">
                                     <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 ">
                                        <div class="form-group form-float">
                                            <label class="form-label ">Tipo Doc (*)</label>
                                            <select class="form-control" id="cbtipodocpersona" name="cbtipodocpersona" onchange="consultarucDoc('<?= base_url . 'persona/buscar' ?>');" >

                                                <?php

                                                if(isset($personabydefault)){
                                                   $iddoc = $personabydefault->getTipodocumento();

                                                } else {
                                                   $iddoc = $documento->getPersonatipodoc();
                                                }
                                                foreach ($docs as $doc) {
                                                    if ($iddoc == $doc->getAbreviatura()) {

                                                        echo '<option value="' . $doc->getAbreviatura() ."-".$doc->getId(). '" selected="selected">' . $doc->getAbreviatura() . '</option>';
                                                    } else {
                                                        echo '<option value="' . $doc->getAbreviatura() ."-".$doc->getId(). '">' . $doc->getAbreviatura() . '</option>';
                                                    }
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <input type="hidden" name="idcliente" id="idcliente" value="<?= ( isset($personabydefault)) ? $personabydefault->getId()  : $documento->getIdpersona() ?>">
                                        <div class="form-group form-float">
                                            <label class="form-label">N° documento (*)</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtrucbuscar" name="txtrucbuscar" value="<?= (isset($personabydefault)) ? $personabydefault->getRuc()  : $documento->getRuc()  ?>" required="" onkeyup="consultarucDoc('<?= base_url . 'persona/buscar' ?>');">

                                            </div>
                                        </div>
                                        <div class="text-danger" id="respuestabusqueda"></div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Razón Social / Nombre (*)</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtcliente" name="txtcliente" value="<?= (isset($personabydefault)) ? $personabydefault->getNombre()  : $documento->getRazonsocial()  ?>" required="">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Dirección </label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtdireccion" name="txtdireccion" value="<?= (isset($personabydefault)) ? $personabydefault->getDireccion()  : $documento->getDireccion()   ?>" >

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Email</label>
                                            <div class="form-line">
                                                <input type="email" class="form-control" id="txtemail" name="txtemail" value="<?= (isset($personabydefault)) ? $personabydefault->getEmail()  : $documento->getEmail()   ?>">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Teléfono</label>
                                            <div class="form-line">
                                                <input type="tel" class="form-control" id="txttelefono" name="txttelefono" value="<?= (isset($personabydefault)) ? $personabydefault->getTelfijo()  : $documento->getTelfijo()   ?>">

                                            </div>
                                        </div>
                                         
                                    </div>
                                    
                                   

                                </div>


                                <!--</form>-->
                            </div>
                            
                    </div>
                    </div>
                        <!--</form>-->

                        <!--<label> Productos / Servicios </label>-->
                        <?php require_once 'view/documentodetalle/detalleventa.php'; ?>


                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                <div class="card">


                                    <div class="body">
                                       
                                        <div class="row align-center" >
                                            <button type="button"  class="btn btn-primary m-l-15 waves-effect " data-toggle="modal" data-target=".modalguia">Guías, Condiciones y Otros datos</button>
                                           

                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="form-group">
                                                <label for="textordenc">Orden de compra</label>
                                                <div class="form-line">
                                                    <input type="text" class="form-control" id="txtordenc" name="txtordenc"/>
                                                </div>


                                            </div>
                                            <div class="form-group">
                                                <label for="textobservacion">Observación</label>
                                                <div class="form-line">
                                                    <input type="text" class="form-control" id="txtobservacion" name="txtobservacion"/>
                                                </div>
                                            </div>


                                        </div>






                                    </div>

                                </div>

                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                <div class="card">


                                    <div class="body">
                                        
                                        
                                        <div class="row" id="montos">
<!--                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center exportacion">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                            
                                            </div> 
                                        </div>
                                       
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center gratuita">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                            
                                            </div> 
                                        </div>
                                       
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center exonerado">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                            
                                            </div> 
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center inafecto">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                            
                                            </div> 
                                        </div>-->
                                            
                                            <input type="hidden" id="gratuita" name="gratuita">
                                            <input type="hidden" id="exonerado" name="exonerado">
                                            <input type="hidden" id="inafecto" name="inafecto">
                                            <input type="hidden" id="exportacion" name="exportacion">
                                            <input type="hidden" id="gravada" name="gravada">
                                            <input type="hidden" id="igv" name="igv">
                                            <input type="hidden" id="totaldoc" name="totaldoc">
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center">
                                                <label class="text-danger" id="lblgravada"><strong>GRAVADA: </strong>  S/ 0.00</label>

                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center">

                                                <label class="text-danger" id="lbligv"><strong>IGV 18%: </strong>  S/ 0.00</label>
                                            </div>
                                        
                                       
                                        <div class="row text-center">
                                            <!--<div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">-->
                                            <label class="text-danger " id="lbltotal" ><strong>TOTAL: </strong>    S/ 0.00</label>
                                            <!--</div>--> 
                                        </div>
                                        </div> 
                                    



                                    </div>



                                </div>
                                <button type="submit" class="btn btn-block btn-lg btn-success waves-effect" id="guardardocumento" name="guardardocumento">REGISTRAR COMPROBANTE</button>


                            </div>


                        </div>



                        <!-- modal Guia ----------->
                        <?php require_once 'view/documentocabecera/modalguias.php'; ?>
                        <?php require_once 'view/documentocabecera/modalpagomixto.php'; ?>

                        

                        <!------------------------------>
                    </form>
                    </div>
                </div>
                

            </div>
            <!-- #END# Inline Layout | With Floating Label -->

        </div>
</section>

<script>
    $(document).on("change","#cbtipopago",function (e){
        $("#cargarpago").empty();
        $("#msjalert").empty();
        
        var divcontenedorpagos = document.getElementById("cargarpago");
        
        if($(this).val() == 'Tarjeta'){
  
            var divpagos = document.createElement("div");
            divpagos.setAttribute("class","col-lg-3 col-md-3 col-sm-6 col-xs-6");


            var divtarjeta = document.createElement("div");
            divtarjeta.setAttribute("class","col-lg-6 col-md-6 col-sm-6 col-xs-12");
            divtarjeta.innerHTML = '<label >Tarjeta (*) </label>';

            var divop = document.createElement("div");
            divop.setAttribute("class","col-lg-6 col-md-6 col-sm-6 col-xs-12");
            
            divop.innerHTML = '<label >N. Ope. (*) </label>';

            var cbtarjeta = document.createElement('select');
            cbtarjeta.setAttribute("class","form-control  show-tick");
            cbtarjeta.setAttribute("id","cbtarjeta");
            cbtarjeta.setAttribute("name","cbtarjeta");
            cbtarjeta.setAttribute("required","required");
            cbtarjeta.innerHTML = ' <option value""> Visa </option> <option value""> MasterCard </option> <option value""> American Express </option> <option value""> Diners </option>';


            var nop = document.createElement("input");
            nop.setAttribute("name","txtnroop");
            nop.setAttribute("id","txtnroop");
            nop.setAttribute("required","required");
            nop.setAttribute("class","form-control");
            nop.setAttribute("placeholder","Operación");

            divtarjeta.appendChild(cbtarjeta);
            divop.appendChild(nop);

            divpagos.appendChild(divtarjeta);
            divpagos.appendChild(divop);


            divcontenedorpagos.appendChild(divpagos);
        }
        
        if($(this).val() == 'Transferencia'){
            var divpagos = document.createElement("div");
            divpagos.setAttribute("class","col-lg-2 col-md-3 col-sm-3 col-xs-6");
            
            var divfloat = document.createElement("div");
            divfloat.setAttribute("class","form-group form-float");
            
            var divline = document.createElement("div");
            divline.setAttribute("class","form-line");
            
            
            
             var divop = document.createElement("div");
            divop.setAttribute("class","col-lg-12 col-md-12 col-sm-12 col-xs-12");
            divop.innerHTML = '<label >N. Ope. (*) </label>';
            
            var nop = document.createElement("input");
            nop.setAttribute("class","form-control");
            nop.setAttribute("name","txtnroop");
            nop.setAttribute("id","txtnroop");
            nop.setAttribute("required","required");
            nop.setAttribute("placeholder","Operación");
            
            divline.appendChild(nop);
            divfloat.appendChild(divline);
            
            divop.appendChild(divfloat);
            divpagos.appendChild(divop);
            divcontenedorpagos.appendChild(divpagos);
            
        }
        if($(this).val() == 'Mixto'){
            
            var divgroup = document.createElement("div");
            divgroup.setAttribute("class","form-group");
            
            
            
            var divmixto = document.createElement("div");
            divmixto.setAttribute("class","col-lg-2 col-md-2 col-sm-3 col-xs-6 ");
            divmixto.innerHTML = '<label > Pagos (*)</label>';
            
//            var label = document.createElement("label");
//            label.innerHTML="Pagos (*)";
            
            var button = document.createElement("button");
            button.setAttribute("class","btn btn-primary btn-sm ");
            button.setAttribute("data-toggle","modal");
            button.setAttribute("data-target",".modalpagomixto");
            button.innerHTML = "<span class='glyphicon glyphicon-credit-card waves-effect'></span>";
//            divgroup.appendChild(label);
            divgroup.appendChild(button);
            
            var inputmixto = document.createElement("input");
           inputmixto.setAttribute('type','hidden');
           inputmixto.setAttribute('id','totalpagomixto');
           divgroup.appendChild(inputmixto);
           
           
//            var inputmixtocont = document.createElement("input");
//           inputmixtocont.setAttribute('type','text');
//           inputmixtocont.setAttribute('id','contpagomixto');
//           divgroup.appendChild(inputmixtocont);
            
            divmixto.appendChild(divgroup);
            
            divcontenedorpagos.appendChild(divmixto);
            
            $('#msjalert').html('<div class="alert alert-danger" role="alert"> Registre los pagos mixtos. </div>');
            $('#tabla tbody > tr').remove();
            detalles=[];
            $('.totalpagomixto').html('Total: 0.00');
        }
   
    });
    
    $(document).on("change","#cbtipoventa",function (e){
        var divcontenedortipopagos = document.getElementById("cargartipopago");
        $("#cargartipopago").empty();
        $("#cargarpago").empty();
        if($(this).val()=="Contado"){
            var divtipopagos = document.createElement("div");
            divtipopagos.setAttribute("class","col-lg-3 col-md-3 col-sm-3 col-xs-6");
            
            var divfloat = document.createElement("div");
            divfloat.setAttribute("class","form-group form-float");
            
            
            divtipopagos.innerHTML = '<label >Tipo de Pago (*) </label>';

            var cbtarjeta = document.createElement('select');
                cbtarjeta.setAttribute("class","form-control show-tick");
                cbtarjeta.setAttribute("id","cbtipopago");
                cbtarjeta.setAttribute("name","cbtipopago");
                cbtarjeta.setAttribute("required","required");
                cbtarjeta.innerHTML = ' <option value="Efectivo"> Efectivo </option> <option value="Tarjeta"> Tarjeta </option> <option value="Transferencia"> Transferencia </option>';

            divfloat.appendChild(cbtarjeta);
            divtipopagos.appendChild(divfloat);
            divcontenedortipopagos.appendChild(divtipopagos);
            
        }
 
    });
    
    $(document).ready( function (e){
        
        var tipo = $('#tipodoc').val();
        var tipodocP = $('#cbtipodocpersona').val();
        
        if(tipo == 'Factura'){
            if(tipodocP != 'RUC-1'){
                $('#cbtipodocpersona :nth(0)').prop('selected', 'selected').change();
            }

        }else {
            if(tipodocP == 'RUC-1'){
            $('#cbtipodocpersona :nth(1)').prop('selected', 'selected').change();
            }

        } 
        
    });
    
    
    //////////////////////////////////////////////////////////////////////////////////////////
    ///// Modal pago mixto 
    //////////////////////////////////////////////////////////////////////////////////////////
       $(document).on("change","#cbtipopagomixto",function (e){
        $("#cargarpagomixto").empty();
        
        var divcontenedorpagos = document.getElementById("cargarpagomixto");
        
        if($(this).val() == 'Tarjeta'){
  
//            var divpagos = document.createElement("div");
//            divpagos.setAttribute("class","col-lg-3 col-md-3 col-sm-3 col-xs-6");


            var divtarjeta = document.createElement("div");
            divtarjeta.setAttribute("class","col-lg-6 col-md-4 col-sm-12 col-xs-12");
            divtarjeta.innerHTML = '<label >Tarjeta (*) </label>';

            var divop = document.createElement("div");
            divop.setAttribute("class","col-lg-6 col-md-4 col-sm-12 col-xs-12");
            divop.innerHTML = '<label >N. Ope. (*) </label>';

            var cbtarjeta = document.createElement('select');
            cbtarjeta.setAttribute("class","form-control  show-tick");
            cbtarjeta.setAttribute("id","cbtarjetamixto");
            cbtarjeta.setAttribute("name","cbtarjetamixto");
//            cbtarjeta.setAttribute("required","required");
            cbtarjeta.innerHTML = ' <option value""> Visa </option> <option value""> MasterCard </option> <option value""> American Express </option> <option value""> Diners </option>';


            var nop = document.createElement("input");
            nop.setAttribute("name","txtnroopmixto");
            nop.setAttribute("id","txtnroopmixto");
//            nop.setAttribute("required","required");
            nop.setAttribute("class","form-control");
            nop.setAttribute("placeholder","Operación");

            divtarjeta.appendChild(cbtarjeta);
            divop.appendChild(nop);

//            divpagos.appendChild(divtarjeta);
//            divpagos.appendChild(divop);


            divcontenedorpagos.appendChild(divtarjeta);
            divcontenedorpagos.appendChild(divop);
        }
        
        if($(this).val() == 'Transferencia'){
//            var divpagos = document.createElement("div");
//            divpagos.setAttribute("class","col-lg-2 col-md-3 col-sm-3 col-xs-6");
            
//            var divfloat = document.createElement("div");
//            divfloat.setAttribute("class","form-group form-float");
//            
//            var divline = document.createElement("div");
//            divline.setAttribute("class","form-line");
//            
            
            
             var divop = document.createElement("div");
            divop.setAttribute("class","col-lg-6 col-md-6 col-sm-12 col-xs-12");
            divop.innerHTML = '<label >N. Ope. (*) </label>';
            
            var nop = document.createElement("input");
            nop.setAttribute("class","form-control");
            nop.setAttribute("name","txtnroopmixto");
            nop.setAttribute("id","txtnroopmixto");
//            nop.setAttribute("required","required");
            nop.setAttribute("placeholder","Operación");
            
            divop.appendChild(nop);
//            divfloat.appendChild(divline);
            
//            divop.appendChild(divfloat);
//            divpagos.appendChild(divop);
            divcontenedorpagos.appendChild(divop);
            
        }
      
   
    });
    
    
    
    
    
    
    var detalles=[];
    
    $(document).on("click","#agregar",function (e){
        e.preventDefault();
        
        
        if(isNaN($("#txtmontomixto").val()) || $("#txtmontomixto").val() == ''){
            swal('Alert!','Ingrese un monto valido','info');
            
            return false;
        }
        
        
        var tipodepago = $("#cbtipopagomixto").val();
        var monto = $("#txtmontomixto").val();
        var tarjeta = "-";
        var nop= "-";
        
        if($("#cbtarjetamixto").length >0){
            tarjeta=$("#cbtarjetamixto").val();
            
        }
        
        if($("#txtnroopmixto").length >0){
            nop=$("#txtnroopmixto").val();
            
        }
        var detalle={
            index: detalles.length ,
            tipodepago: tipodepago,
            monto: monto,
            tarjeta:tarjeta,
            nop: nop
            
        };
        
          $('.table').append('<tr><td>' + tipodepago + '</td><td>' + parseFloat(monto).toFixed(2) + '</td><td>' + tarjeta + '</td><td>'+nop+'</td><td><button id="eliminaritempago" onclick="eliminaritem('+ (detalles.length ) +')" class="btn btn-danger btn-sm waves-effect"><span class="glyphicon glyphicon-remove"></span></button></td></tr>');
       
        
        detalles.push(detalle);
        
        console.log(detalles);
        
        $("#txtmontomixto").val("");
        
        
        $("#txtnroopmixto").val("");
        calculartotalpagomixto();
        
    });
    
 function calculartotalpagomixto(){
     var totalglobal = $('#totaldoc').val();
     
     console.log(totalglobal);
     
     var total=0;
     var j = 0;
     for(var i = 0 ; i < detalles.length ; i++){
          var item = detalles[i];
        total +=  parseFloat(item.monto);
        j ++;
     }
     
     $('.totalpagomixto').html('Total: '+total.toFixed(2) );
     $('#totalpagomixto').val(total);
     
     if(j == 1){
         $('#msjalert').html('<div class="alert alert-danger" role="alert"> Ingrese 2 pagos como minimo, para el modo mixto. </div>');
         return false;
     }else {
         $('#msjalert').empty();
     }
     
     if(total < totalglobal || total > totalglobal){         
          $('#msjalert').html('<div class="alert alert-danger" role="alert"> La sumatoria de pagos mixtos debe coincidir con el total de la venta. </div>');
          return false;
     }else {
         $('#msjalert').empty();
     }
     
     
     
     return total;
 }   
    
    
    
    
 function eliminaritem(Indice){
     
      
      detalles.splice(Indice,1);
        console.log(detalles);
      $('#tabla tbody > tr').remove();
    for(var i = 0 ; i < detalles.length ; i++){
        var item = detalles[i];
        
        console.log(item.tipodepago);
         $('.table').append('<tr><td>' + item.tipodepago + '</td><td>' + parseFloat(item.monto).toFixed(2) + '</td><td>' + item.tarjeta + '</td><td>'+item.nop+'</td><td><button id="eliminaritempago" onclick="eliminaritem('+ (detalles.length -1) +')" class="btn btn-danger btn-sm waves-effect"><span class="glyphicon glyphicon-remove"></span></button></td></tr>');   

        
    }
        
      calculartotalpagomixto();  
      
 }
 
 //////////////////////////////////////////////////////////
    
</script>


