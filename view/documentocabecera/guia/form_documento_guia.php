


<section class="content">
    <!--<div class="container-fluid">-->

        <div class="block-header">
            <!--<h2>FORM EXAMPLES</h2>-->
                <div id="respuestaAjax"></div>
        </div>
        <!-- Inline Layout | With Floating Label -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header ">
                        <h5 class="text-primary">
                            <span class="glyphicon glyphicon-export"></span> Emitir Guia Remisión Traslado 
                            <!--<small>Edición</small>-->

                        </h5>

                    </div>
                    <div class="body">
                        <form action="<?= base_url ?>documento/insertguia" method="POST"  id="FormularioAjax" data-form="insert" enctype="multipart/form-data" autocomplete="off" >
                            <div class="row clearfix">
                                <!--<form >-->
                                <div class="row">
 
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float" id="divserie">
                                            <label >Serie (*)</label>
                                            <div id="divcargarserie">
                                                <select class="form-control show-tick" id="cbserie" name="cbserie" required="">
                                                    <option value="">-- Seleccione --</option>  
                                                    <?php 
                                                        foreach ($sucurseries as $serie){
                                                          echo '<option value="'.$serie->getSerie().'">'.$serie->getSerie().'</option>';
                                                        }
                                                    ?>
                                                    
                                                    
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Nro: (*)</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtnro" name="txtnro" value="" required="" readonly="">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <!--                                <h2 class="card-inside-title">Aniversario</h2>-->
                                            <label for="dpfechaemision">Fecha Entrega (*)</label>
                                            <div class="form-group ">
                                                <div class="form-line" id="bs_datepicker_container">
                                                    <input type="text" class="form-control" placeholder="Fecha Emisión" id="dpfechaentrega" name="dpfechaentrega" value="<?= date('d/m/Y') ?>" required="">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <!--                                <h2 class="card-inside-title">Aniversario</h2>-->
                                            <label for="dpfechavencimiento">Fecha traslado (*)</label>
                                            <div class="form-group">
                                                <div class="form-line" id="bs_datepicker_container">
                                                    <input type="text" class="form-control" placeholder="Fecha Vencimiento" id="dpfechatraslado" name="dpfechatraslado" value="<?= date('d/m/Y') ?>" required="">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Moneda (*)</label>
                                            <select class="form-control show-tick" id="cbmoneda" name="cbmoneda" required="">

                                                <?php
                                                $pred = array('Soles', 'Dolares');

                                                for ($i = 0; $i < count($pred); $i++) {

                                                    echo '<option value="' . $pred[$i] . '" >' . $pred[$i] . '</option>';
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>

                                </div>
                              
                                <div class="row">

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Punto de partida (*)</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="txtemisordireccion" name="txtemisordireccion" value="<?= $origen->getDireccion() ?>" required="" >
                                                
                                            </div>
                                        </div>
                                    </div>

                                
                            

                                </div>
                                <div class="row">
                                    
                                    
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        
                                        <div class="card">
                                            <div class="header">    
                                                Datos del Destinatario
                                            </div>
                                            
                                            <div class="body">
                                                <div class="row">
                                                
                                                
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    
                                                    <div class="card">
                                                        
                                                        
                                                        <div class="body">
                                                            
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                    <div class="form-group form-float">

                                                                        <label>Almacén Destino (*)</label>
                                                                        
                                                                        <select class="form-control show-tick" id="cbdestinatario" name="cbdestinatario" required="">
                                                                            <option value="">-- Seleccione --</option>
                                                                            <?php 



                                                                                foreach ($sucursales as $sucursal){
                              
                                                                                    if($_SESSION['idalmacen'] != $sucursal->getId()){                                                      
                                                                                        echo '<option value="'.$sucursal->getId().'" >'.$sucursal->getNombre().'</option>';

                                                                                 }



                                                                            }

                                                                            ?>

                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                                <div class="form-group form-float">
                                                                    <label class="form-label">Nombre  (*)</label>
                                                                    <div class="form-line">
                                                                        <input type="text" class="form-control" id="txtrazonsocialdestino" name="txtrazonsocialdestino" value="" readonly="" required="">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                                
<!--                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                <div class="form-group form-float">
                                                                    <label class="form-label">RUC (*)</label>
                                                                    <div class="form-line">
                                                                        <input type="text" class="form-control" id="txtrucdestino" name="txtrucdestino" value="" readonly="" required="">

                                                                    </div>
                                                                </div>
                                                            </div>-->
                                                            
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group form-float">
                                                                    <label class="form-label">Dirección (*)</label>
                                                                    <div class="form-line">
                                                                        <input type="text" class="form-control" id="txtdirecciondestino" name="txtdirecciondestino" value="" required="">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                                
                                            </div>
                                                
                                            </div>
                                            
                                            
                                        </div>
                                        
                                    </div> 
                                   
                                </div>
                                <!--                        <div class="h3 text-danger">DATOS DEL CLIENTE</div>
                                                        <HR>-->
                             


                                <!--</form>-->
                            </div>
                        <!--</form>-->

                        <!--<label> Productos / Servicios </label>-->
                        <?php require_once 'view/documentodetalle/detalleventa.php'; ?>
                        
                           <div class="row">
                                    
                                    
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        
                                        <div class="card">
                                            <div class="header">    
                                                Datos del transportista
                                            </div>
                                            
                                            <div class="body">
                                                <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="card">
                                                        <div class="header">
                                                            Vehiculo
                                                        </div>

                                                        <div class="body">
                                                            <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                                             
                                                                <div class="form-group form-float">
                                                                    <label class="form-label">Marca / Placa </label>
                                                                    <div class="form-line">
                                                                        <input type="text" class="form-control" id="txtmarcaplaca" name="txtmarcaplaca" value="">

                                                                    </div>
                                                                </div>
                                                                <div id="respuestabusqueda"></div>
                                                            </div>
                                                            
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                                                <div class="form-group form-float">
                                                                    <label class="form-label">Licencia </label>
                                                                    <div class="form-line">
                                                                        <input type="text" class="form-control" id="txtlicencia" name="txtlicencia" value="" >

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                                                <div class="form-group form-float">
                                                                    <label class="form-label">Certificado </label>
                                                                    <div class="form-line">
                                                                        <input type="text" class="form-control" id="txtcertificado" name="txtcertificado" value="" >

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            

                                                        </div>


                                                    </div>


                                                </div>
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    
                                                    <div class="card">
                                                        
                                                        
                                                        <div class="body">
                                                            
                                                            <div class="row">
                                                                
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group form-float">
                                                                    <label class="form-label">Nombre / Razón Social</label>
                                                                    <div class="form-line">
                                                                        <input type="text" class="form-control" id="txtrazonsocialtransp" name="txtrazonsocialtransp" value="">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                                
                                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-6">
                                                                <div class="form-group form-float">
                                                                    <label class="form-label">RUC</label>
                                                                    <div class="form-line">
                                                                        <input type="text" class="form-control" id="txtructransp" name="txtructransp" value="">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                                <div class="form-group form-float">
                                                                    <label class="form-label">Flete</label>
                                                                    <div class="form-line">
                                                                        <input type="text" class="form-control" id="txtflete" name="txtflete" value="">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                                
                                            </div>
                                                
                                            </div>
                                            
                                            
                                        </div>
                                        
                                    </div> 
                                   
                                </div>


                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                <div class="card">


                                    <div class="body">
                                        
                                       

                                        <div class="row">
                                         
                                            <div class="form-group">
                                                <label for="textobservacion">Motivo</label>
                                                <div class="form-line">
                                                    <textarea class="form-control" id="txtmotivo" name="txtmotivo"></textarea>
                                                </div>
                                            </div>


                                        </div>






                                    </div>

                                </div>

                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                <div class="card" style="display:none;">


                                    <div class="body">
                                        
                                        
                                        <div class="row" id="montos">
<!--                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center exportacion">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                            
                                            </div> 
                                        </div>
                                       
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center gratuita">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                            
                                            </div> 
                                        </div>
                                       
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center exonerado">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                            
                                            </div> 
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center inafecto">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                            
                                            </div> 
                                        </div>-->
                                            
                                            <input type="hidden" id="gratuita" name="gratuita">
                                            <input type="hidden" id="exonerado" name="exonerado">
                                            <input type="hidden" id="inafecto" name="inafecto">
                                            <input type="hidden" id="exportacion" name="exportacion">
                                            <input type="hidden" id="gravada" name="gravada">
                                            <input type="hidden" id="igv" name="igv">
                                            <input type="hidden" id="totaldoc" name="totaldoc">
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center">
                                                <label class="text-danger" id="lblgravada"><strong>GRAVADA: </strong>  S/ 0.00</label>

                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center">

                                                <label class="text-danger" id="lbligv"><strong>IGV 18%: </strong>  S/ 0.00</label>
                                            </div>
                                        
                                       
                                        <div class="row text-center">
                                            <!--<div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">-->
                                            <label class="text-danger " id="lbltotal" ><strong>TOTAL: </strong>    S/ 0.00</label>
                                            <!--</div>--> 
                                        </div>
                                        </div> 
                                    



                                    </div>



                                </div>
                                <button type="submit" class="btn btn-lg btn-success waves-effect" id="guardardocumento" name="guardardocumento">REGISTRAR COMPROBANTE</button>


                            </div>


                        </div>



                        <!-- modal Guia ----------->
                        <?php require_once 'view/documentocabecera/modalguias.php'; ?>



                        <!------------------------------>
                    </form>
                    </div>
                </div>
                

            </div>
            <!-- #END# Inline Layout | With Floating Label -->

        </div>
</section>

<script>
    
    $(document).on('change','#cbserie', function (e){
        
        $.ajax({
            type: 'POST',
            url: '<?=base_url.'documento/selectmaxnro' ?>',
            data: {serie : $(this).val(),tipod:'guia_remision',tipo:'guia_remision'},
            beforeSend: function (xhr) {
                        $('#txtnro').val('Cargando ...');
                    },
            success: function (data) {
                    $('#txtnro').val(data);
                        
            }
            
            
            
        });
        
        
    });
    
    $(document).on('change','#cbdestinatario', function (e){
    
    var id = $(this).val();
    
        $.ajax({
            type: 'POST',
            url: '<?=base_url.'almacen/selectbyid' ?>',
            data: {id:id},
            dataType: 'JSON',
            beforeSend: function (xhr) {
                       bloquearpantalla("");
            },
            success: function (data) {
                
                $('#txtrazonsocialdestino').val(data['razonsocial']);
//                $('#txtrucdestino').val(data['ruc']);
                $('#txtdirecciondestino').val(data['direccion']);

//                   console.log(data);
                   $.unblockUI();
                        
            }
                       
        });

    });
    
    
    
</script>

<!--<script>
$('.bs_datepicker_container').datepicker({
    language: 'es'
});
</script>-->


