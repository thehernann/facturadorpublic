<?php 
 
  /** Actual month last day **/
  function _data_last_month_day() { 
      $month = date('m');
      $year = date('Y');
      $day = date("d", mktime(0,0,0, $month+1, 0, $year));
 
      return date('d/m/Y', mktime(0,0,0, $month, $day, $year));
  }
 
  /** Actual month first day **/
  function _data_first_month_day() {
      $month = date('m');
      $year = date('Y');
      return date('d/m/Y', mktime(0,0,0, $month, 1, $year));
  }



?>

<section class="content">
    <div class="container-fluid">
        
       
        
        <div class="block-header">
            <!--    <br>-->
            <div class="RespuestaAjax"></div>
        </div>
        <!-- Basic Examples -->

        <!-- #END# Basic Examples -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           Comprobantes Traslado
                            

                        </h2>
                        <hr>
                        <a  class="btn btn-primary" href="<?= base_url ?>documento/crearguia" ><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
                    
                    </div>
                    <div class="body">
                        <div class="RespuestaAjax"></div>                    
                     <form action="<?= base_url?>documento/searchguiatras" method="POST"  id="FormularioAjaxBuscar" data-form="insert" enctype="multipart/form-data" autocomplete="off" >   
                        
                        <div class="row">  
                       <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                            <div class="form-group form-float">
                                <label class="form-label" >Desde </label>
                                <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                <div class="form-group">
                                    <div class="form-line" id="bs_datepicker_container">
                                        <input type="text" class="form-control" placeholder="Desde" id="dpdesde" name="dpdesde" value="<?= _data_first_month_day()?>" required="">
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                            <div class="form-group form-float">
                                <label class="form-label">Hasta </label>
                                <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                <div class="form-group">
                                    <div class="form-line" id="bs_datepicker_container">
                                        <input type="text" class="form-control" placeholder="Hasta" id="dphasta" name="dphasta" value="<?= _data_last_month_day()?>" required="">
                                    </div>
                                </div>

                            </div>
                        </div>
                            
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                                <label>Tipo Comprobante</label>
                                <select  class="form-control show-tick" id="cbtipocomprobante" name="cbtipocomprobante">
<!--                                    <option value="">- Tipo comprobante -</option>-->
                                    <?php 
                                    $pred= array('Guia de remisión');
                                    $value= array('guia_remision');
                                    
                                    for($i=0;$i < count($pred);$i++){

                                            echo '<option value="'.$value[$i].'" >'.$pred[$i].'</option>';


                                    }

                                    ?>

                            </select>
                            </div>
                        </div>
                            
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                            <div class="form-group form-float">
                                <label class="form-label">Serie </label>
                                <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" id="txtserie" name="txtserie">
                                    </div>
                                </div>

                            </div>
                        </div>
                            
                       <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                            <div class="form-group form-float">
                                <label class="form-label">Número </label>
                                <!--                                <h2 class="card-inside-title">Aniversario</h2>-->

                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" id="txtnumero" name="txtnumero">
                                    </div>
                                </div>

                            </div>
                        </div>
                            
                            
                            
                                           
                        </div>
                        <div class="row">
  
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <div class="form-group form-float">
                            
                                    <label>Punto de partida</label>
                                    <select class="form-control show-tick" id="cbpuntopartida" name="cbpuntopartida">
                                        <option value="">-- Seleccione --</option>
                                        <?php 


                                            foreach ($sucursales as $sucursal){
//                                                if($_SESSION['idsucursal'] == $sucursal->getId()){
//                                                    echo '<option value="'.$sucursal->getRuc().'" selected="selected" >'.$sucursal->getNombre().'</option>';
//
//                                                }else{
                                                    echo '<option value="'.$sucursal->getId().'" >'.$sucursal->getNombre().'</option>';

//                                                }



                                        }

                                        ?>

                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group form-float">
                            
                                <label>Destinatario</label>
                                <select class="form-control show-tick" id="cbdestinatario" name="cbdestinatario">
                                    <option value="">-- Seleccione --</option>
                                    <?php 
                                   
                                                                        
                                        foreach ($sucursales as $sucursal){
                                           
                                                echo '<option value="'.$sucursal->getId().'" >'.$sucursal->getNombre().'</option>';
                                                
                                           
                                            


                                    }

                                    ?>

                            </select>
                            </div>
                        </div>
                       
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <label>Acciones</label>
                            <div class="form-group form-float">
                                
                                <button class="btn btn-sm btn-primary waves-effect" type="submit"><span class="glyphicon glyphicon-search "></span> Buscar</button>
                                
                         
                                
                                <button class="btn btn-sm btn-success waves-effect" id="exceldocumenttras" url="<?= base_url?>documento/imprimirexceltraslado"><span class="glyphicon glyphicon-file"></span> Excel</button>
                                
                            </div>
                        </div>
                            
                            
                            
                            
                        </div>
                     </form>
                        
                        
                        
                        
                        <div id="respuestaExcel"></div>
                        <div class="table-responsive" id="respuestaAjax">
                            <table class="table  table-hover table-bordered" id="tabladocumento">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Tipo</th>
                                        <th>Serie</th>
                                        <th>Número</th>
                                      
                                        <th>Punto de partida</th>
                                        <th>Destinatario</th>
                                        <th>Estado</th>
                                        <th>Imprimir</th>
                                        
                                    </tr>
                                </thead>
<!--                                <tfoot>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Tipo</th>
                                        <th>Serie</th>
                                        <th>Número</th>
                                        <th>RUC/ DNI</th>
                                        <th>Nombre / Rz. Social</th>
                                        <th>Total</th>
                                        <th>Est. Local</th>
                                        <th>Est. Sunat</th>
                                        <th>Imprimir</th>
                                        <th>Acciones</th>
                                    </tr>
                                </tfoot>-->
                                <tbody >
                                    
                                    <?php foreach ($documentos as $documento){
                                    $estado = '';  
                                   
                                        if($documento->getEstadolocal() == 'sin_confirmar'){
                                            $estado = '<button onclick="confirmar('."'".$documento->getId()."'".','."'".$documento->getIdalmacendestino()."'".','."'".$documento->getIdalmacenemisor()."'".')" class="btn btn-danger" title="Confirme el traslado"> Sin confirmar</button>';
                                        }
                                        
                                       if($documento->getEstadolocal() == 'confirmado'){
                                            $estado = '<span class="label bg-green"> Confirmado </span>';
                                        }

                                            echo '<tr>';
                                            echo '<td>'.$documento->getFechaemision().'</td>';
                                            echo '<td>'.$documento->getTipo().'</td>';
                                            echo '<td>'.$documento->getSerie().'</td>';
                                            echo '<td>'.$documento->getNumero().'</td>';
                                            echo '<td>'.$documento->getRazonsocial().'</td>';
                                            echo '<td>'.$documento->getRazonsialdestinatario().'</td>';
                                            echo '<td>'.$estado.'</td>';
                                            echo '<td><a  href="'.base_url.'documento/imprimirguia&id='.$documento->getId().'" target="_blank" data-toggle="tooltip" data-placement="top" title="PDF" style="background: none;"> <i class="material-icons">picture_as_pdf</i></a><button type ="text" style="border:none;background: none;" data-toggle="tooltip" data-placement="top" title="TICKET" onclick ="VentanaCentrada('."'".base_url.'documento/printticketguia&id='.$documento->getId()."'".','."'".'Ticket'."'".','."''".','."''".','."''".','."'false'".');">  <i class="material-icons">confirmation_number</i> </button> </td>';

//                                            echo '<td><a  href="'.base_url.'documento/imprimir&id='.$documento->getId().'" target="_blank" data-toggle="tooltip" data-placement="top" title="PDF" style="background: none;"> '
//                                                    . '<i class="material-icons">picture_as_pdf</i></a><button type ="text" style="border:none;background: none;" data-toggle="tooltip" data-placement="top" title="TICKET" '
//                                                    . 'onclick ="VentanaCentrada('."'".base_url.'documento/printticket&id='.$documento->getId()."'".','."'".'Ticket'."'".','."''".','."''".','."''".','."'false'".');">  '
//                                                    . '<i class="material-icons">confirmation_number</i> </button> </td>';
                                            echo '</tr>';
                                        
                                    } ?>



                                </tbody>
                               
                            </table>
                            <div class="pagination">
                                <nav>
                                    <ul class="pagination"></ul>
                                    
                                </nav>
                                
                            </div>
                            
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table --> 
    </div>
</section>

<script>

   var table = '#tabladocumento'
   $(document).ready( function (){   
     
       $('.pagination').html('')
       var trnum =0
       var maxRows = 20
       var totalRows = $(table+' tbody tr').length
       $(table+' tr:gt(0)').each( function (){
           trnum++
           if(trnum > maxRows){
               $(this).hide()
           }
           if(trnum <= maxRows){
               $(this).show()
               
           }
           
           
           
       })
       if(totalRows > maxRows){
           var pagenum = Math.ceil(totalRows/maxRows)
           for(var i=1; i<= pagenum;){
               $('.pagination').append('<li data-page="'+i+'">\<span>'+ i++ +'<span class="sr-only">(current)</span>\n\
                </span>\</li>').show()
               
           }
           
       }
       $('.pagination li:first-child').addClass('active')
       $('.pagination li').on('click', function (){
       
        var pageNum = $(this).attr('data-page')
        var trIndex = 0;
        $('.pagination li').removeClass('active')
        $(this).addClass('active')
        $(table+' tr:gt(0)').each(function (){
            trIndex++
            if(trIndex > (maxRows*pageNum) || trIndex <= ((maxRows*pageNum)- maxRows)){
                $(this).hide()
            }else {
                $(this).show()
            }
                
            })
            
        });
       
       
       });
       
       
       $(document).on('change','#cbpuntopartida', function (){
      
        $('#FormularioAjaxBuscar').submit();
    });
   
       $(document).on('change','#cbdestinatario', function (){
        $('#FormularioAjaxBuscar').submit();
    });
       
//       $(function (){
//        $('table tr:eq(0)').prepend('<th>ID</th>')
//        var id=0;
//        $('table tr:gt(0)').each(function (){
//            id++
//            $(this).prepend('<td>'+id+'</td>')
//            
//        })
//       
//       
//       
//       
//       })

    function confirmar(id,iddestino,idemisor){
        
  
        swal({
            title: "¿Estás seguro?",
            text: "Quieres realizar la operación solicitada",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Aceptar",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: 'POST',
                url: '<?=base_url.'documento/confirmartraslado' ?>',
               
                data: {id: id, iddestino:iddestino, idemisor:idemisor},
    //            data: formdata,

                beforeSend: function (data) {
                    $('.RespuestaAjax').html('<p class="text-center">Procesando... </p>');
                    bloquearpantalla("No recargue la ventana");
                },
                success: function (data) {

                    $('.RespuestaAjax').html(data);
                    $.unblockUI();
                },
                error: function () {
                    $('.RespuestaAjax').html(msjError);
                }
            });
            return false;
        });
        
        
        
    }
       
       
       
       

    
    
  
</script>    




















