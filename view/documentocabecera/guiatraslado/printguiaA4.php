<?php 
//   set_include_path(get_include_path() . PATH_SEPARATOR . "/path/to/dompdf");
   
require 'vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
//use Spipu\Html2Pdf\Exception\Html2PdfException;
//use Spipu\Html2Pdf\Exception\ExceptionFormatter;
//header("Content-type: image/jpg"); 
// $path =  base_url.'images/user-lg.jpg';
// $type = pathinfo($path, PATHINFO_EXTENSION);
// $data = file_get_contents($path);
// $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
ob_start();


$codhtml = '
<!DOCTYPE html>
<html>
 
<head>
    <style type="text/css">
      
        body {
          position: relative;
          width: 750px;
          max-width: 750px;
         
          margin: 0 auto; 
        }
        table#detalle{ 
        border: 1px solid black;
        border-collapse: separate;
        
        border-radius: 7px;
      
        width: 750px;
        max-width: 750px;
        }
        thead#detalle {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
            border-collapse: separate;
        }
        tr#detalle {
           display: table-row;
            vertical-align: inherit;
            border-color: inherit;
   
           
           
        }
        th, td#detalle {
            
            text-align: left;
            vertical-align: top;
           

             
        }
        td#detalle {
            border-top: 1px solid black;
        }
       
        thead#detalle:first-child tr#detalle:first-child th:first-child, tbody#detalle:first-child tr#detalle:first-child td#detalle:first-child {
            border-radius: 7px 0 0 0;
        }
        thead#detalle:last-child tr#detalle:last-child th#detalle:first-child, tbody#detalle:last-child tr#detalle:last-child td#detalle:first-child {
            border-radius: 0 0 0 7px;
        }
        td.producto,
        th.producto {
          width: 500px;
          max-width: 500px;
          word-break: break-all;
        }

        td.cantidad,
        th.cantidad {
          width: 80px;
          max-width: 80px;
          text-align: center;
          word-break: break-all;
        }

        td.precio,
        th.precio {
          width: 150px;
          max-width: 150px;
          text-align: center;
          word-break: break-all;
        }

        th {
          font-size: 12px;
          font-family: Arial;  

        }
        
       


        .centrado {
          text-align: center;
          align-content: center;
        }
        .justificado {
          text-align: justify;

        }
        
        td.razonsocial{
          width: 500px;
          max-width: 500px;
          word-break: break-all;
          line-height: 18px;
      
        }
        
        td.razonsocialII{
          width: 250px;
          max-width: 250px;
          word-break: break-all;
          line-height: 18px;
          
        }
        
        #razonsocialtable {
            margin-bottom : 2%;
        }

        
       
        

        img {
        /*  max-width: inherit;
          width: inherit;*/
        display:block;


        }

        /*----------- CAJA ----------- */
        #cajaruc { 
        font-family: Arial; 
        
        font-size: 16px; 
        text-align: center;
        line-height : 20px;
        padding: 10px;
        border-radius: 15px 15px 15px 15px;  
        border: 1px solid black;
        width: 250px;
        height: 70px;
        max-width: 250px;
      /*  float: right !important;
        margin-left: 500px;*/
        position:absolute;
        left: 490;
        top: 20;
       
        }
        .cajacliente { 
        font-family: Arial;
        font-size: 11px; 
        line-height: 18px;
        text-align: left; 
        overflow: auto; 
        width:400px;
        height: 65px;
        max-width: 400px;
       /* float: left !important;*/

        position: absolute;
        top: 130;
        left:5;
        
        
        }
        .cajafecha { 
        font-family: Arial;  
        font-size: 11px; 
        line-height: 18px;
        text-align: left;  
        
        width: 200px;
        height: 77px;
        max-width:200px;
       /* float: right !important;
        margin-left: 200px;*/
        position: absolute;
        right: 120;
        top: 130;
       
        }
        .importeletra{
            font-family: Arial; 
            font-size: 12px;
        }
        
        .divlogo{
            
            padding: 0px; 

            width: 300px;
            height: 150px;
            max-width: 300px;
           /* position:absolute;
            left: 30;
            top: 0; */


        }

        .divempresa { 
        font-family: Arial;
       
        font-size: 12px; 
        
        text-align: center; 
        
       
        width: 300px;
        height: 200px;
        line-height: 14px;
        max-width: 400px;
        position:absolute;
        left: 180;
        top: 20; 
        
        }
        table#resultados{
           
            
            width: 300px;
           max-width: 300px;
           font-size: 12px;
           padding: 10px;
        }
        .rletra{
            text-align: left;
            width: 140px;
            max-width: 140px;
            
        }
        .rnum{
             text-align: right;
             width: 80px;
            max-width: 80px;
        }
        .qr{
            font-family: Arial; 
        
        font-size: 16px; 
        text-align: center;
        line-height : 20px;
        padding: 10px;
        border-radius: 15px 15px 15px 15px;  
        border: 1px solid black;
        width: 730px;
        height: 70px;
        max-width: 730px;
  

        }
        .rqr{
            text-align: center;
            width: 470px;
            max-width: 470px;
        
        }
        
        
     
       
    </style>
 
 
</head>'; 
    $dir= "temp/";
 
    $moneda = '';
    $simbolo = '';
    
    if(!file_exists($dir)){
        mkdir($dir);
        
    }
   $filename = $dir.'test.png';
    $opc = '';  
    
 $dirimg= "temp/img/";
    if(!file_exists($dirimg)){
        mkdir($dirimg);
        
    }
    file_put_contents($dirimg."logo.jpg", base64_decode($sucur->getImgtoplogo()));
    
    $logo = 'temp/img/logo.jpg';
   
    if($document->getTipo() =="Factura" and $document->getTipodoc()=="Venta"){
        $comprobante="FACTURA ELECTRÓNICA"; 
        $opc = "01";
    }elseif($document->getTipo() =="Boleta" and $document->getTipodoc()=="Venta") {
        $comprobante="BOLETA DE VENTA ELECTRÓNICA";
        $opc = "02";
    }elseif($document->getTipo() =="nota_venta" and $document->getTipodoc()=="Venta") {
        $comprobante="NOTA DE VENTA";
        $opc = "05";
    }elseif($document->getTipo() =="nota_debito"){
        $comprobante="NOTA DE DEBITO ELECTRÓNICA";
        $opc = "03";
    }elseif($document->getTipo() =="nota_credito" ){
        $comprobante="NOTA DE CREDITO ELECTRÓNICA";
        $opc = "04";
    }elseif($document->getTipo() =="guia_remision" ){
        $comprobante="GUÍA DE TRASLADO";
        $opc = "05";
    }elseif($document->getTipo() =="Factura" and $document->getTipodoc()=="Compra"){
        $comprobante="COMPRA";
    }else{
        $comprobante="ORDEN DE COMPRA";
    }
    if($document->getMoneda()== 'Soles'){
        $moneda = 'PEN';
        $simbolo = 'S/ ';
        
    }
    if($document->getMoneda()== 'Dolares'){
        $moneda= 'USD';
        $simbolo = '$ ';
    }
    $estado = "";
    if($document->getEstadolocal() == "confirmado"){
        
        $estado = '<label style="color:#1F91F3">Confirmado</label>';
        
    }else {
        $estado = '<label style="color:#FF2100">Sin Confirmar</label>';
        
    }
   

$codhtml.='

 
<body>
    
   
  <div class="A4">
    <div class="divlogo">
      <img src="'.$logo.'" width="190" height="130" alt="Logo" />'. 
        
    '</div><div class="divempresa"><h4>'
     
    .$sucur->getEmpresa().'</h4>'.$sucur->getDireccion().'<br>'.$sucur->getDpto().' - '.$sucur->getProvincia().' - '.$sucur->getDistrito().'<br>'.$sucur->getTelf().'<br>'.
    '</div><table id="razonsocialtable">'
        . '<tbody>'
        . '<tr>'
        . '<td class="razonsocial">' //<div class="cajacliente">
        . '<strong>DATOS DEL TRASLADO </strong><br>'
        . 'FECHA EMISIÓN:  '.$document->getFechaemision().'<br>'
        . 'MOTIVO DE TRASLADO: '.$document->getMotivoguia().'<br><br>'
        . '<strong>DATOS DEL DESTINATARIO </strong><br>'.
        'SEÑOR(ES):'.$document->getRazonsialdestinatario().'<br>'.
        'DOCUMENTO DE IDENTIDAD/RUC: '.$document->getRucdestinatario().'<br><br>'.
        '<strong>DATOS DEL PUNTO DE PARTIDA Y PUNTO DE LLEGADA </strong><br>'.
        'DIRECCIÓN DEL PUNTO DE PARTIDA: '.$document->getDireccion().'<br>'.
        'DIRECCIÓN DEL PUNTO DE LLEGADA: '.$document->getDirecciondestinatario().'<br><br>'.
        '<strong>DATOS DEL TRANSPORTE </strong><br>'.
        'TRANSPORTISTA: '.$document->getRuctransportista().' '.$document->getRazonsocialtransportista().'<br>'.
        
        'PLACA:'.$document->getMarcaplaca().'<br><br>'.
        '<strong>ESTADO: '.$estado.'</strong><br><br>'.
        '<strong>DATOS DE LOS BIENES</strong>'.
        '</td>
            
        </tr>
        </tbody>
        </table>
    <div id="cajaruc">
    RUC.: '.$sucur->getRuc().'<br>'.$comprobante.'<br>'.
        $document->getSerie().'-'.str_pad($document->getNumero(), 6, "0", STR_PAD_LEFT)
    .'</div>
    '.  
    '<table id="detalle" class="det">
      <thead>
        <tr>
          <th class="cantidad">[CANT.]</th>
          <th class="producto">DESCRIPCIÓN</th>
          <th class="precio">UNID. MED.</th>
          
        </tr>
      </thead>
      <tbody>';
            $total=0;
            $gravada = 0;
            $igv = 0;
            $gratuita = 0;
            $exonerada = 0;
            $inafecta = 0;
            $ivap = 0;
            $expo = 0;
            $descuento = 0;
          foreach ($detalles as $temp){
              $importe = $temp->getCantidad() * $temp->getPrecio();
              $serie = "";
              if(!empty($temp->getSeries())){
                  $serie = ' S/N:'.$temp->getSeries();
              }
              
           
            
                      
$codhtml.='
        <tr class="items">
            <td class="cantidad">'.number_format($temp->getCantidad() ,2).'</td>
            <td class="producto">'.$temp->getDescripcionprod().$serie.'</td>
            <td class="precio">'.$temp->getUnidmed().'</td>
            
        </tr>';

            $total+=$importe;
            $gratuita += $temp->getMontobasegratuito();
            $exonerada += $temp->getMontobaseexonarado();
            $inafecta += $temp->getMontobaseinafecto();
            $ivap += $temp->getMontobaseivap();
            $expo += $temp->getMontobaseexpo();
        
        
          } 
          
          $total = $total - ($gratuita + $ivap) - $descuento;
          
          $gravada = ($total -($expo + $exonerada + $inafecta))/1.18;
          $igv =($total -($expo + $exonerada + $inafecta)) - $gravada;
          
          if($document->getIncigv() == false){
              $gravada = $total;
              $total += $igv;
              $igv = $total - $gravada;
              
          }
          
          $totalf = (float)number_format($total,2);
          $qr = $sucur->getRuc().' | '.$opc.' | '.$document->getSerie().' | '.str_pad($document->getNumero(), 6, "0", STR_PAD_LEFT).' | '.$igv.' | '.$total.' | '.$document->getFechaemision().' | '.$document->getRuc().' || ';
          QRcode::png($qr,$filename,'M',20,15);
$codhtml.='
      </tbody>
      
      </table>
      
     
    
      
    
    ';


$codhtml.='
    
        
    
  </div>
</body>
//</html>';
//ob_get_clean();  
$html2pdf = new Html2Pdf();

//$html2pdf->pdf->SetDisplayMode('fullpage');
$html2pdf->writeHTML($codhtml);
$html2pdf->output('documento.pdf');
    
    
    
