
<!DOCTYPE html>
<html>
 
<head>

  <link rel="stylesheet" href="<?= base_url?>css/cssticket.css">
<!--  <script src="css/cssticket.css"></script>-->
 
</head>

<?php 
    require_once 'libs/phpqrcode/qrlib.php';

    $dir= "temp/";
    if(!file_exists($dir)){
        mkdir($dir);
        
    }
    
    
    $filename = base_url.'temp/test.png';
    $moneda = '';
    $simbolo = '';


    if($document->getTipo() =='Factura' and $document->getTipodoc()=='Venta'){
        $comprobante='FACTURA ELECTRÓNICA'; 
        $opc = '01';
    }elseif($document->getTipo() =='Boleta' and $document->getTipodoc()=='Venta') {
        $comprobante='BOLETA DE VENTA ELECTRÓNICA';
        $opc = '02';
    }elseif($document->getTipo() =='nota_debito'){
        $comprobante='NOTA DE DEBITO ELECTRÓNICA';
        $opc = '03';
    }elseif($document->getTipo() =='nota_credito' ){
        $comprobante='NOTA DE CREDITO ELECTRÓNICA';
        $opc = '04';
    }elseif($document->getTipo() =="guia_remision" ){
        $comprobante="GUÍA DE TRASLADO";
        $opc = "05";
    }elseif($document->getTipo() =='Factura' and $document->getTipodoc()=='Compra'){
        $comprobante='COMPRA';
        $opc = '05';
    }elseif($document->getTipo()== 'nota_venta' and $document->getTipodoc() == 'Venta'){
        $comprobante='NOTA DE VENTA';
        $opc = '06';
    }elseif($document->getTipo()== 'Cotizacion' and $document->getTipodoc() == 'Cotizacion'){
        $comprobante='';
        $opc = '07';
    }else {
        $comprobante='ORDEN DE COMPRA';
        $opc = '07';
    }
    
    if($document->getMoneda()== 'Soles'){
        $moneda = 'PEN';
        $simbolo = 'S/ ';
        
    }
    if($document->getMoneda()== 'Dolares'){
        $moneda= 'USD';
        $simbolo = '$ ';
    }
    
     $estado = "";
    if($document->getEstadolocal() == "confirmado"){
        
        $estado = '<label >Confirmado</label>';
        
    }else {
        $estado = '<label >Sin Confirmar</label>';
        
    }
   

?>

 
<body>
    
   
  <div class="ticket">
      
      <img src="data:image/jpg;base64 ,<?=  $sucur->getImgtoplogo(); ?>" width="100" height="100" alt="Logo" /> 
      <div class="titulo">
      <p class="centrado"><strong><?= $sucur->getEmpresa(); ?></strong><br><?= $sucur->getDireccion(); ?> <br> <?= $sucur->getDpto().' - '.$sucur->getProvincia().' - '.$sucur->getDistrito() ?><br> <?= $sucur->getTelf(); ?> <br><strong> <?= 'RUC.: '.$sucur->getRuc(); ?> <br><?=  $comprobante;?><br>
      <?= $document->getSerie().'-'.str_pad($document->getNumero(), 6, "0", STR_PAD_LEFT)?></strong></p>
      </div>
      <div class="encabezado">
    <p> <strong>   DATOS DEL TRASLADO </strong><br>
    FECHA EMISIÓN:  <?= $document->getFechaemision()?><br>
    MOTIVO DE TRASLADO: <?= $document->getMotivoguia()?><br><br>
    
    <strong>DATOS DEL DESTINATARIO: </strong><br>
    SEÑOR(ES): <?= $document->getRazonsialdestinatario()?><br>
    DOCUMENTO DE IDENTIDAD/RUC: <?= $document->getRucdestinatario()?><br><br>
    <strong>DATOS DEL PUNTO DE PARTIDA Y PUNTO DE LLEGADA</strong><br>
    DIRECCIÓN DEL PUNTO DE PARTIDA <?= $document->getDireccion()?><br>
    DIRECCIÓN DEL PUNTO DE LLEGADA: <?= $document->getDirecciondestinatario()?><br><BR>
    <strong>DATOS DEL TRANSPORTE</strong><br>
    TRANSPORTISTA: <?= $document->getRuctransportista().' '.$document->getRazonsocialtransportista()?><br>
    PLACA: <?= $document->getMarcaplaca() ?><br><br>
    <strong>ESTADO: <?= $estado ?></strong></p><br>
    <strong>DATOS DE LOS BIENES</strong>
      </div><br>
    <table>
        <thead class="headdetalle">
        <tr>
          <th class="cantidad">[CANT.]</th>
          <th class="producto">DESCRIPCIÓN</th>
          <th class="precio">UNID.</th>
          
        </tr>
      </thead>
      <tbody class="tbodydetalle">
          <?php
//          $temp = new Temp(); 
          
          foreach ($detalles as $temp){
              $importe = $temp->getCantidad() * $temp->getPrecio();
               $serie = "";
              if(!empty($temp->getSeries())){
                  $serie = ' S/N:'.$temp->getSeries();
              }
              
              ?>
        <tr>
            <td class="cantidad">[<?php echo number_format($temp->getCantidad() ,2)?>]</td>
            <td class="producto"><?php echo $temp->getDescripcionprod().$serie ?></td>
            <td class="precio"><?php echo $temp->getUnidmed() ?></td>
          
        </tr>
       
        
        
        <?php 
          

          } 
         
              
         ?>
     
         </tbody>
      </table>
  
    <hr>    
        
    <div class="footerdown">
        
        <h3>ESTE COMPROBANTE NO TIENE VALIDEZ TRIBUTARIO </h3>
        
        
    </div>

        
    </div>
     
 
    <hr>
 
 <script>
window.print();

</script>   
    
</body>


  
</html>


