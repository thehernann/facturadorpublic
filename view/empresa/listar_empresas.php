
<section class="content">
    <!--<div class="container-fluid">-->
        
       
        
        <div class="block-header">
            <!--    <br>-->
            <div class="RespuestaAjax"></div>
        </div>
        <!-- Basic Examples -->

        <!-- #END# Basic Examples -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           Empresas
                            <!--<a href="<?= base_url ?>bancos/crear"  class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> NUEVO</a>-->

                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <!--                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                                        <i class="material-icons">more_vert</i>
                                                                    </a>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li><a href="javascript:void(0);">Action</a></li>
                                                                        <li><a href="javascript:void(0);">Another action</a></li>
                                                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                                                    </ul>-->
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="tablaempresas">
                                <thead>
                                    <tr>
                                        <th>Nro°</th>
                                        <th>RUC</th>
                                        <th>Razón Social</th>
                                        <th>Nombre comercial</th>
                                        <th>Email</th>
                                        <th>Teléfono</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Nro°</th>
                                        <th>RUC</th>
                                        <th>Razón Social</th>
                                        <th>Nombre comercial</th>
                                        <th>Email</th>
                                        <th>Teléfono</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </tfoot>
                                <tbody>

                                    <?php
                                    $i = 1;
                                    $estado = '';
                                    foreach ($empresas as $empresa){
                                        if($empresa->getActivo() == 1 ){
                                            $estado = 'Activo';
                                            
                                        }else {
                                            $estado = 'Inactivo';
                                        }
                                        
                                        
                                        echo '<tr>';
                                        echo '<td>' . $i . '</td>';
                                        echo '<td>' . $empresa->getRuc() . '</td>';
                                        echo '<td>' . $empresa->getRazonsocial() . '</td>';
                                        echo '<td>' . $empresa->getNombrecomercial() . '</td>';
                                        echo '<td>' . $empresa->getEmail() . '</td>';
                                        echo '<td>' . $empresa->getTelefono() . '</td>';
                                        echo '<td>' . $estado . '</td>';
                                 
                                        echo '<td><a href="' . base_url . 'empresa/cargar&id=' . $empresa->getId() . '" type="button" class="btn btn-default" title="Editar"><div class="demo-google-material-icon"> <i class="material-icons">edit</i> <span class="icon-name"></span> </div></span></a>'
                                                . ' <a href="' . base_url . 'sucursal/empresa&id=' . $empresa->getId() . '" type="button" class="btn btn-default" title="Sucursales" ><div class="demo-google-material-icon"> <i class="material-icons">home</i> <span class="icon-name"></span> </div></a>'
                                                . ' <a href="' . base_url . 'nivel/empresa&id=' . $empresa->getId() . '" type="button" class="btn btn-default" title="Niveles" ><div class="demo-google-material-icon"> <i class="material-icons">lock_open</i> <span class="icon-name"></span> </div></a> </td>';
                                        echo '</tr>';
                                        $i++;
                                    }
                                    ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table --> 
    </div>
</section>
