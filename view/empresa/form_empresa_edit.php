


<section class="content">
    <div class="container-fluid">
        
<div class="block-header">
    <!--<h2>FORM EXAMPLES</h2>-->
    <!--    <div class="RespuestaAjax"></div>-->
</div>
<!-- Inline Layout | With Floating Label -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
        <div class="card">
            <div class="header">
                <h2>
                Editar Empresa
                    <!--<small>Edición</small>-->
                </h2>


            </div>
            <div class="body">
                <form action="<?= base_url . 'empresa/update' ?>" method="POST"  id="FormularioAjax" data-form="update" enctype="multipart/form-data" autocomplete="off" >
                    <div class="row clearfix">
                        <!--<form >-->
                        <div class="row">
                            <input type="hidden" value="<?= $empresa->getId(); ?>" id="id" name="id">

                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                <div class="form-group form-float">
                                    <label class="form-label">RUC (*)</label>
                                    <div class="form-line ">
                                        <input type="text" class="form-control" id="txtruc" name="txtruc" value="<?= $empresa->getRuc(); ?>" required="">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">Razón Social (*)</label>
                                    <div class="form-line ">
                                        <input type="text" class="form-control" id="txtrazonsocial" name="txtrazonsocial" value="<?= $empresa->getRazonsocial(); ?>" required="">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">Nombre Comercial(*)</label>
                                    <div class="form-line ">
                                        <input type="text" class="form-control" id="txtnombre" name="txtnombre" value="<?= $empresa->getNombrecomercial(); ?>" required="">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">Dirección(*)</label>
                                    <div class="form-line ">
                                        <input type="text" class="form-control" id="txtdireccion" name="txtdireccion" value="<?= $empresa->getDireccion(); ?>" required="">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">Email (*)</label>
                                    <div class="form-line ">
                                        <input type="email" class="form-control" id="txtemail" name="txtemail" value="<?= $empresa->getEmail(); ?>" required="">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">Teléfono</label>
                                    <div class="form-line ">
                                        <input type="tel" class="form-control" id="txttelefono" name="txttelefono" value="<?= $empresa->getTelefono(); ?>" >
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">Representante (*) </label>
                                    <div class="form-line ">
                                        <input type="text" class="form-control" id="txtrepresentante" name="txtrepresentante" value="<?= $empresa->getDuenio(); ?>" required="">
                                        
                                    </div>
                                </div>
                            </div>
                            


             
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
    <!--                            <input type="checkbox" id="remember_me_5" class="filled-in">
                                <label for="remember_me_5">Remember Me</label>-->
                                <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">GUARDAR</button>
                                <a href="<?= base_url ?>empresa/select"  class="btn btn-danger btn-lg m-l-15 waves-effect" id="btncancelar">CANCELAR</a>
                            </div>
                        </div>
                        <!--</form>-->
                    </div>
                </form>
                <div id="respuestaAjax"></div>



            </div>
        </div>
    </div>
</div>
<!-- #END# Inline Layout | With Floating Label -->

   </div>
</section>
