<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of kardexController
 *
 * @author HERNAN
 */

require_once 'model/kardex.php';
require_once 'model/serieProducto.php';
require_once 'model/producto.php';

class kardexController {
    //put your code here
    
    function ajustestock(){
//        var_dump($_POST);
        if( isset($_POST['cbajuste'])  && !empty($_POST['cbajuste'])
                && isset($_POST['txtobservacion'])  && !empty($_POST['txtobservacion'])
                         && isset($_POST['idproducto'])  && !empty($_POST['idproducto'])
                && isset($_POST['descripcion'])  && !empty($_POST['descripcion']) 
                 
                && isset($_POST['txtstock'])  && !empty($_POST['txtstock'])){
            
            
            
            if(!is_numeric($_POST['txtstock']) || $_POST['txtstock'] < 0){
                
                  ?> 
                    <script>

                         swal('No se realizo registro', 'Ingrese una cantidad valida', 'info');
                    </script>  <?php
                    die();
            }
            
            $kardexm = new kardex();
            $seriep = new serieProducto();
            $prodm = new producto();
            
            
            
            
            $idprod = $_POST['idproducto'];
          
            
            
            $incluir = '';
            if(isset($_POST['cbincluir'])){
                echo 'incluir si ';
                $incluir = 'Si';
            }else {
                $incluir = 'No';
                 echo 'incluir no ';
            }
            
             
            $ultimok = $kardexm->ultimomovimientoproducto($idprod);
            
            $cantidad = $_POST['txtstock'];
//            
//            
//            if(isset($_POST['txtstock']) && !empty($_POST['txtstock'])){
//                $cantidad = $_POST['txtstock'];
//                
//            }
            
//            $serie = array();
            if($incluir == 'Si'){
                
                $seriek = $seriep->select($ultimok->getId());
//                $serie = $_POST['serie'];
                
//                var_dump($seriek);
                $cadenaserie = '';
                $cont = 0;
                foreach ($seriek as $s){
                    if($cont > 0){
                        $cadenaserie .= ' , ';
                        
                    }
                    
                    $cadenaserie .= $s->getSerie();
                    $cont ++;
                }
                $kardexm->setStockanteriorserie($cadenaserie);
//                $cantidad = count($serie);
                
                
            }
             
            $ajuste = $_POST['cbajuste'];
            
            $stockactual = 0;
            
            if($ajuste == 'Negativo'){
                if($incluir == 'Si'){
                    if(isset($_POST['serie'])){
                        $se = $_POST['serie'];
                        $stockactual = count($se);
                    }else {
                        $stockactual = 0;
                    }
                    
                }else {
                   $stockactual = $ultimok->getStockactual() - $_POST['txtstock'];
                    
                }
                
                
            }else {
                if($incluir == 'Si'){
                    if(isset($_POST['serie'])){
                         $se = $_POST['serie'];
                         $stockactual = $ultimok->getStockactual() + count($se);
                    }else {
                        $stockactual = $ultimok->getStockactual();
                    }
                    
                }else {
                    
                    $stockactual = $ultimok->getStockactual() + $_POST['txtstock'];
                }
            }
            
            $observacion = $_POST['txtobservacion'];
            
            $descripcion = $_POST['descripcion'];
           
            $kardexm->setStockanterior($ultimok->getStockactual());
            $kardexm->setStockactual($stockactual);
            $kardexm->setIdproducto($idprod);
            $kardexm->setIdalmacen($_SESSION['idalmacen']);
            $kardexm->setIdempresa($_SESSION['idempresa']);
            $kardexm->setTipodeajuste($ajuste);
            $kardexm->setObservacion($observacion);
            $kardexm->setDescripcion($descripcion);
            $kardexm->setConcepto('Ajuste');
            $kardexm->setCantidad($cantidad);
            $kardexm->setIddetalle(null);
            
            $idkardex = $kardexm->insertOne($kardexm);
            
            
            $prodm->updateincluir($idprod, $incluir);
            
            if(isset($_POST['serie'])){
                $serie = $_POST['serie'];
                $seriepinsert = array();
                for($i = 0; $i < count($serie); $i++){
                    $seriea = array(
                        strtoupper($serie[$i]),
                        $idkardex,
                        $_SESSION['idempresa']

                    );
                    array_push($seriepinsert, $seriea);

                }

                $seriep->insert($seriepinsert);
            }
            
            if($idkardex > 0){
                
                 ?> 
                <script>
                
                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                      document.location.reload();
                </script>  <?php
                
                
            }else {
                
                 ?> 
                <script>

                     swal('Error', 'No se realizarón cambios.', 'error');
                   
                </script>  <?php
            }
            
            
            
            
   
        }else {
            
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios (*)', 'info');
            </script>  <?php
        }
        
        
        
    }
    
    function series(){
        
        if(isset($_POST['id']) && !empty($_POST['id'])){
            
            $idprod = $_POST['id'];
            
            
            $kardexm = new kardex();
            $serieprod = new serieProducto();
            
            $ultimokardex = $kardexm->ultimomovimientoproducto($idprod);
            
            $series = $serieprod->select($ultimokardex->getId());
            
            $seriesa = array();
            
            foreach ($series as $serie){
                $d = array(
                    'id' => $serie->getId(),
                    'serie' => $serie->getSerie()
                    
                    
                );
                array_push($seriesa, $d);
                
            }
            
            
            echo json_encode($seriesa);
            
            
            
        }
        
        
        
    }
    
    
    
}
