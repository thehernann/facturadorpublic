<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of empresaController
 *
 * @author HERNAN
 */
require_once 'model/empresa.php';
class empresaController {
    //put your code here
    function  select(){
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-panel')){
            
            $empresa = new Empresa();
        
        $empresas = $empresa->selectAll();
        
        require_once 'view/empresa/listar_empresas.php';
            
        }else {
            
            require_once 'view/sinpermiso.php';
        }
        
        
        
        
        
        require_once 'view/layout/footer.php';
        
        
        
    }
    
    function cargar(){
        require_once 'view/layout/header.php';
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $id = $_GET['id'];
            
            $empresam = new Empresa();
            
            $empresa = $empresam->selectOne($id);
            require_once 'view/empresa/form_empresa_edit.php';
            
            
            
        }else {
            require_once 'view/error.php';
            
        }
        require_once 'view/layout/footer.php';
        
        
    }
    
    function update(){
        
        if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['txtruc']) && !empty($_POST['txtruc']) 
                && isset($_POST['txtrazonsocial']) && !empty($_POST['txtrazonsocial']) && isset($_POST['txtnombre']) && !empty($_POST['txtnombre'])
                && isset($_POST['txtdireccion']) && !empty($_POST['txtdireccion']) && isset($_POST['txtemail']) && !empty($_POST['txtemail'])
                && isset($_POST['txtrepresentante']) && !empty($_POST['txtrepresentante']) && isset($_POST['txttelefono']) ){
            
            $id = $_POST['id'];
            $ruc = $_POST['txtruc'];
            $razons = $_POST['txtrazonsocial'];
            $nombre = $_POST['txtnombre'];
            $direccion = $_POST['txtdireccion'];
            $email = $_POST['txtemail'];
            $representa = $_POST['txtrepresentante'];
            $telefono = $_POST['txttelefono'];
            
            if(permisos::rol('rol-panel') == false){
                 ?> 
                  <script>

                    swal("Acceso denegado","Necesita permisos para realizar esta acción", "error");
                      
                  </script>  <?php
                                   die();
                
            }
            
            
            $empresa = new Empresa();
            
            $empresa->setId($id);
            $empresa->setRuc($ruc);
            $empresa->setRazonsocial($razons);
            $empresa->setNombrecomercial($nombre);
            $empresa->setDireccion($direccion);
            $empresa->setEmail($email);
            $empresa->setDuenio($representa);
            $empresa->setTelefono($telefono);
            
            $fila = $empresa->update($empresa);
            
            if($fila > 0){
                 ?> 
                  <script>

                    swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                      
                  </script>  <?php
                
            }
            
            
        }else {
            
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
        }
        
        
        
        
        
    }
    
    
    
    
}
