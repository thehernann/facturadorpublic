<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gastosController
 *
 * @author HERNAN
 */
require_once 'model/gastos.php';
require_once 'model/sucursal.php';
require_once 'model/usuario.php';
class gastosController {
    //put your code here
    private $gasto;
    
    function __construct() {
        $this->gasto = new gastos();
        
    }
    
    function select(){
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-gastosoperaciones') || permisos::rol('rol-gastosoperacionessucur')){
            $month = date('m');
            $year = date('Y');
            $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
            $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
            $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));


            $sucursal = new sucursal();

            $sucursales = $sucursal->selectAll();

            $gastos = $this->gasto->search($desde, $hasta, '', $_SESSION['idsucursal']);

            require_once 'view/gastos/listargastos.php';
            
        }else {
            $url='';
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
        
        
    }
    function selectbycaja(){
       
        
//        var_dump($_POST);
        if(isset($_POST['moneda']) && isset($_POST['tipo']) && isset($_POST['idcaja'])){
            
            
            $id = $_POST['idcaja'];
            $moneda = $_POST['moneda'];
            $tipo = $_POST['tipo'];
            $gasto = new gastos();
            
            $gastos = $gasto->selectbycaja($id,$moneda,$tipo);
            
//            var_dump($gastos);
            if(count($gastos) == 0){
                
                echo '<p class="text-danger">No se encontrarón registros</p>';
            }
            
            $itemsforma = array("Efectivo","Tarjeta","Transferencia");
            $itemsformav = array("efectivo","tarjeta","transferencia");
            
            $itemstipo = array("Visa","MasterCard","American Express","Diners");
            $itemstipov = array("visa","mastercard","americanexpress","diners");
            
            $j= 0;
            foreach ($gastos as $g){
             
                echo '<form method="POST" id="FormularioAjax" autocomplete="off" action="'.base_url.'gastos/updatebycaja">';
                echo '<input type="hidden" id="id" name="id" value="'.$g->getId().'"/>';
                echo '<div class="row">';
                
                echo '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">';
                echo '<label class="form-label">Descripción (*)</label>';
                echo '<input type="text" class="form-control" id="descripcionop" name="descripcionop" value="'.$g->getConcepto().'" />';
                echo '</div>';
                
                echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">';
                 echo '<label class="form-label">Detalle (*)</label>';
                echo '<input type="text" class="form-control" id="detalleop" name="detalleop" value="'.$g->getDetalle().'" />';
                echo '</div>';
                
                echo '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">';
                 echo '<label class="form-label">Forma Pago(*)</label>';
                echo '<select id="formapago" id="formapagoop" ident="'.$j.'"  name="formapagoop" class="form-control">';
                
                
                
                for ($i = 0 ; count($itemsformav) > $i ; $i++ ){
                    
                   
                    if($itemsformav[$i] == $g->getFormapago()){
                        
                        echo '<option value='.$itemsformav[$i].' selected="selected">'.$itemsforma[$i].'</option>';
                    }else {
                        
                        echo '<option value='.$itemsformav[$i].' >'.$itemsforma[$i].'</option>';
                    }
                    
                }
                echo '</select>';
                echo '</div>';
                echo '<div id="pagoop'.$j.'">';
               
                if($g->getFormapago() == 'tarjeta'){
                    
                    echo '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">';
                    echo '<label class="form-label">Tipo de Pago </label>';
                    echo '<select id="tipopagoop" name="tipopagoop" class="form-control">';
                    for ($i = 0 ; count($itemstipov) > $i ; $i++ ){
                        if($itemstipov[$i] == $g->getTipopago()){

                            echo '<option value='.$itemstipov[$i].' selected="selected">'.$itemstipo[$i].'</option>';
                        }else {

                            echo '<option value='.$itemstipov[$i].' >'.$itemstipo[$i].'</option>';
                        }

                    }
                    echo '</select>';
                    echo '</div>';
                    
                    echo '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">';
                    echo '<label class="form-label">N. Ope. (*)</label>';
                    echo '<input type="text" class="form-control" id="numeroop" name="numeroop" value="'.$g->getNop().'" />';
                    echo '</div>';
                    
                }
                
                if($g->getFormapago() == 'transferencia'){
                    
                     echo '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">';
                    echo '<label class="form-label">N. Ope. (*)</label>';
                    echo '<input type="text" class="form-control" id="numeroop" name="numeroop" value="'.$g->getMonto().'" />';
                    echo '</div>';
                    
                    
                }
                 
                echo '</div>';
                
                
                echo '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">';
                echo '<label class="form-label">Monto (*)</label>';
                echo '<input type="text" class="form-control" id="montoop" name="montoop" value="'.number_format($g->getMonto(),2).'" />';
                echo '</div>';
                
                echo '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-1 eliminar">';
               echo '<label class="form-label">--Acciones--</label>';
              echo '<button type="button" class="btn btn-danger" onclick="eliminar('.$g->getId().','."'".base_url.'gastos/eliminar'."'".','."'".'delete'."'".')" ><span class="glyphicon glyphicon-remove"></span> </button>';
                echo '<button type="submit" class="btn btn-primary waves-effect"  ><span class="glyphicon glyphicon-ok"></span></button>';
                echo '</div>';
                
                echo '</div>';
                
                echo '</form>';
               echo '<hr>';
               
                $j ++;
            }
            
             
            
            
        }else {
            
             echo '<p class="text-danger">No se encontrarón registros</p>';
        }

  
        
    }
    
    function planilla(){
        require_once 'view/layout/header.php';
        require_once 'view/gastos/form_gastos.php';
        require_once 'view/layout/footer.php';
        
        
    }
    
    function crear(){
         require_once 'view/layout/header.php';
         
         $titulo = 'Nuevo gasto';
         $gasto = new gastos();
         $fecha = '';
         $url = 'gastos/insert';
        
        require_once 'view/gastos/form_gastos.php';
        require_once 'view/layout/footer.php';
        
    }
    function crearplanilla(){
         require_once 'view/layout/header.php';
         
         $titulo = 'Nuevo gasto';
         $gasto = new gastos();
         $fecha = '';
         $url = 'gastos/insert';
         
         $usuariom = new usuario();
         $usuarios = $usuariom->selectAll();
         
//         var_dump($usuarios);
        require_once 'view/gastos/form_gastos_planilla.php';
        require_once 'view/layout/footer.php';
        
    }
    
    function insert(){
//        var_dump($_POST);
        if(isset($_POST['dpfecha']) && isset($_POST['txtentrega']) && isset($_POST['cbmoneda']) && isset($_POST['cbtipopago']) 
                && isset($_POST['txtentidad']) && isset($_POST['txtnroop']) && isset($_POST['txtconcepto']) && 
                !empty($_POST['dpfecha']) && !empty($_POST['txtentrega']) && !empty($_POST['cbmoneda']) && !empty($_POST['cbtipopago']) && 
                !empty($_POST['txtconcepto']) && isset($_POST['tipo'])  && !empty($_POST['tipo']) && isset($_POST['txtmonto']) && is_numeric($_POST['txtmonto'])
                && isset($_POST['idcaja'])){
            
            $emision = trim($_POST['dpfecha']);

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');
            $gasto = new gastos();
            
            $gasto->setFecha($emisionf);
            $gasto->setEntrega($_POST['txtentrega']);
            $gasto->setMoneda($_POST['cbmoneda']);
            $gasto->setTipopago($_POST['cbtipopago']);
            $gasto->setEntidad($_POST['txtentidad']);
            $gasto->setNop($_POST['txtnroop']);
            $gasto->setConcepto($_POST['txtconcepto']);
            $gasto->setIdsucursal($_SESSION['idsucursal']);
            $gasto->setTipo($_POST['tipo']);
            $gasto->setMonto($_POST['txtmonto']);
            $gasto->setIdcaja($_POST['idcaja']);
            $fila = $this->gasto->insert($gasto);
            
            if($fila > 0){
                ?>
            
            <script>
                
                 swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                
                $('#FormularioAjax').trigger("reset");
               
            </script>
            <?php
                
                
            }else {
                  ?> 
            <script>
               
                 swal('No se realizarón cambios', 'Ocurrio un error inesperado', 'error');
            </script>  <?php
                
                
            }
            
        }else {
              ?> 
            <script>
               
                 swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'error');
            </script>  <?php
                        
        }

    }
    
    function eliminar(){
        
        if(isset($_POST['id'])){
            
            $id = $_POST['id'];
            $gasto = new gastos();
            
            $fila = $gasto->eliminar($id);
            
            
            if($fila > 0 ){
               ?>
                <script>
                    swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                    $('#buscarcaja').submit();
                    $('.modalmostraringresoegreso').modal('hide');
                    
                </script>
            

                <?php  
               
                
                
            }
            
            
            
        }
        
        
        
    }
            
    function cargar(){
         require_once 'view/layout/header.php';
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $id = $_GET['id'];
            $titulo = 'Gasto (modo edición)';
            $gasto = $this->gasto->selectOne($id);
//            var_dump($gasto);
            
            $fech= trim($gasto->getFecha());
            $fechaa= DateTime::createFromFormat('Y-m-d', $fech);
            $fecha = $fechaa->format('d/m/Y');
            
            $usuariom = new usuario();
            $usuarios = $usuariom->selectAll();
            $url = 'gastos/update';
            if($gasto->getTipo() == 'planilla'){
            require_once 'view/gastos/form_gastos_planilla.php';
                
            }else{
                require_once 'view/gastos/form_gastos.php';
            }
            
            
            
            
        }else {
            
            require_once 'view/error.php';
        }
        
        require_once 'view/layout/footer.php';
        
    }
    
    
function update(){
    if(isset($_POST['dpfecha']) && isset($_POST['txtentrega']) && isset($_POST['cbmoneda']) && isset($_POST['cbtipopago']) 
                && isset($_POST['txtentidad']) && isset($_POST['txtnroop']) && isset($_POST['txtconcepto']) && 
                !empty($_POST['dpfecha']) && !empty($_POST['txtentrega']) && !empty($_POST['cbmoneda']) && !empty($_POST['cbtipopago']) && 
                !empty($_POST['txtconcepto']) && isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['txtmonto']) && is_numeric($_POST['txtmonto'])){
            $emision = trim($_POST['dpfecha']);

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');
            $gasto = new gastos();
            $id = $_POST['id'];
            
            $gasto->setId($id);
            $gasto->setFecha($emisionf);
            $gasto->setEntrega($_POST['txtentrega']);
            $gasto->setMoneda($_POST['cbmoneda']);
            $gasto->setTipopago($_POST['cbtipopago']);
            $gasto->setEntidad($_POST['txtentidad']);
            $gasto->setNop($_POST['txtnroop']);
            $gasto->setConcepto($_POST['txtconcepto']);
            $gasto->setIdsucursal($_SESSION['idsucursal']);
            $gasto->setMonto($_POST['txtmonto']);
            $fila = $this->gasto->update($gasto);
            
            if($fila > 0){
                ?>
            
            <script>
                
                 swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                
//                $('#FormularioAjax').trigger("reset");
               
            </script>
            <?php
                
                
            }else {
                  ?> 
            <script>
               
                 swal('No se realizarón cambios', 'Ocurrio un error inesperado', 'error');
            </script>  <?php
                
                
            }
            
        }else {
              ?> 
            <script>
               
                 swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'error');
            </script>  <?php
                        
        }
        
        
        
    }
    
function updatebycaja(){
    
    if(isset($_POST['descripcionop'])  && isset($_POST['detalleop'])  && isset($_POST['id'])  
               && isset($_POST['montoop']) && isset($_POST['formapagoop']) && !empty($_POST['descripcionop'])  && !empty($_POST['detalleop'])  && !empty($_POST['id'])  
               && !empty($_POST['montoop']) && !empty($_POST['formapagoop'])){
        
        $numeroop='';
        $tipodepago='';
        
          if(isset($_POST['tipopagoop']) ){
              if(empty($_POST['tipopagoop'])){
                   ?> 
                <script>

                     swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'info');
                </script>  <?php
                  die();
              }else{
                  $tipodepago = $_POST['tipopagoop'];
              }
            
            
        }
      
        if(isset($_POST['numeroop'])){
            
            if(empty($_POST['numeroop'])){
                   ?> 
                <script>

                     swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'info');
                </script>  <?php
                  die();
              }else{
                   $numeroop = $_POST['numeroop'];
              }
            
           
        }
        
        
        if(!is_numeric($_POST['montoop'])){
            ?>
            <script>
                
                swal("Alert!", "Ingrese un monto valido.", "info");
            </script>
            

            <?php
            die();
        }else {
            if($_POST['montoop'] <= 0){
             ?>
            <script>
                
                swal("Alert!", "Ingrese un monto valido.", "info");
            </script>
            

            <?php
             die();
            }
            
        }
        
      
        
             $id = $_POST['id'];
          
             
             $descripcion = $_POST['descripcionop'];
             $detalle = $_POST['detalleop'];
             
             $monto = $_POST['montoop'];
             
             $formapago = $_POST['formapagoop'];
             
             
             
            $gasto = new gastos();
            
            
            $gasto->setId($id);
           
            $gasto->setConcepto($descripcion);
            $gasto->setDetalle($detalle);
            $gasto->setFormapago($formapago);
            $gasto->setTipopago($tipodepago);
            $gasto->setNop($numeroop);
            $gasto->setMonto($monto);
         
            
         
            
            
            
            
            
            
            
           
            $fila = $this->gasto->update($gasto);
            
            if($fila > 0){
                ?>
            
            <script>
                
                 swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                 $('#buscarcaja').submit();
//                $('#FormularioAjax').trigger("reset");
               
            </script>
            <?php
                
                
            }else {
                  ?> 
            <script>
               
                 swal('No se realizarón cambios', 'Ocurrio un error inesperado', 'error');
            </script>  <?php
                
                
            }
            
        }else {
              ?> 
            <script>
               
                 swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'info');
            </script>  <?php
                        
        }
        
        
        
    }
    
    
    function search(){
        
        if(isset($_POST['dpdesde']) && isset($_POST['dphasta']) && isset($_POST['txtbuscar'])){
            $desde = $_POST['dpdesde'];
            $dated = DateTime::createFromFormat('d/m/Y', $desde);
            $datedf = $dated->format('Y-m-d');

            $hasta = $_POST['dphasta'];
            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
            $datehf = $dateh->format('Y-m-d');
            
            $cadena = $_POST['txtbuscar'];
            
            if(permisos::rol('rol-gastosoperacionessucur') && isset($_POST['cbsucursal'])){
                $sucursal = $_POST['cbsucursal'];
            }elseif(permisos::rol('rol-gastosoperaciones')){
                $sucursal= $_SESSION['idsucursal'];
                
            }
        
        $gastos = $this->gasto->search($datedf, $datehf, $cadena,$sucursal );
        ?>
        
        <table class="table  table-hover table-bordered" id="tabladocumento">
            <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Entregado a</th>
                    <th>Concepto</th>
                    <th>Moneda</th>
                    <th>Monto</th>
                    <th>Tipo Entrega</th>
                    <th>Nro. op / Cheque</th>
                    <th>Acciones</th>

                </tr>
            </thead>
<!--                                <tfoot>
                <tr>
                    <th>Fecha</th>
                    <th>Tipo</th>
                    <th>Serie</th>
                    <th>Número</th>
                    <th>RUC/ DNI</th>
                    <th>Nombre / Rz. Social</th>
                    <th>Total</th>
                    <th>Est. Local</th>
                    <th>Est. Sunat</th>
                    <th>Imprimir</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>-->
            <tbody >
                <?php 
//                                    $se = 0;
//                         
                foreach ($gastos as $detalle){

                        echo '<tr>';

                        echo '<td>'.$detalle['fecha'].'</td>';
                        echo '<td>'.$detalle['entrega'].'</td>';
                        echo '<td>'.$detalle['concepto'].'</td>';
                        echo '<td>'.$detalle['moneda'].'</td>';

                        echo '<td>'.number_format($detalle['monto'],2).'</td>';
                        echo '<td>'.$detalle['tipopago'].'</td>';
                        echo '<td>'.$detalle['nop'].'</td>';

                        echo '<td><a href="'.base_url.'gastos/cargar&id='.$detalle['id'].'" data-toggle="tooltip" data-placement="top" title="EDITAR"><i class="material-icons" style="border:none;background: none;">create</i></a></td>';

                        echo '</tr>';



                } 

                ?>



            </tbody>


        </table>
        <div class="pagination">
            <nav>
                <ul class="pagination"></ul>

            </nav>

        </div>
            <?php
        }
    }

    


    
}

