<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of nivelController
 *
 * @author HERNAN
 */
require_once 'model/nivel.php';
require_once 'model/detalleNivel.php';
require_once 'model/empresa.php';

class nivelController {
    //put your code here
//    private $nivel;
    function __construct() {
//        $this->nivel = new nivel();
        
    }
    
    function permiso(){
        
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-permisos')){
            
            if(permisos::rol('rol-panel') && isset($_GET['empresa']) && !empty($_GET['empresa'])){
                
                $idempresa= $_GET['empresa'];
                require_once 'view/nivel/form_nivel_panel.php';
            }else {
                require_once 'view/nivel/form_nivel.php';
            }
            
            
        }else{
            $url='';
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
        
        
        
    }
    function select(){
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-permisos')){
            $nivel = new nivel();
            $permisos = $nivel->selectpermiso();
            require_once 'view/nivel/listar_nivel.php';
            
        }else {
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
    }
    
    function delete(){
//        var_dump($_POST);
        if(isset($_POST['id']) && !empty($_POST['id'])){
            
            $id= $_POST['id'];
            
            $nivel = new nivel();
            
            $fila = $nivel->delete($id);
                  
            if($fila > 0){
                  ?>
                <script>
                    swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                    
                    document.location.href= '<?= base_url.'nivel/select' ?>';
                </script>
                <?php
                
                
            }else {
                 ?>
                <script>
                    swal("Error", "No se realizo ninguna operación", "success");
                    
                    document.location.href= '<?= base_url.'nivel/select' ?>';
                </script>
                <?php
                
            }
            
        }else {
            ?>
            <script>
                swal('Error','Algo sucedio mal :(','error');
            </script>
            <?php
            
        }
        
    }
            
    function insert(){
        if($_POST){
            $nivel = new nivel();
            $nivel->setDescripcion($_POST['txtdescripcion']);
            $idempresa = $_SESSION['idempresa'];
        if(isset($_POST['idempresa']) && !empty($_POST['idempresa'])){
            $idempresa = $_POST['idempresa'];
            
        }
            $nivel->setIdempresa($idempresa);
            $niveldet = new detalleNivel();
            
            $id = $nivel->insert($nivel);
            $privilegios = array();
            
            if(isset($_POST['rol-editarprecio'])){
                $editprecio = array(
                    $id,
                    'Editar precio documento',
                    'rol-editarprecio'
                );
                array_push($privilegios, $editprecio);
            }
            
            if(isset($_POST['rol-permisos'])){
                $permisos = array(
                    $id,
                    'Listar nivel',
                    'rol-permisos'
                );
                array_push($privilegios, $permisos);
            }
            if(isset($_POST['rol-stockreporte'])){
                $stocktodo = array(
                    $id,
                    'Stock todos',
                    'rol-stockreporte'
                );
                array_push($privilegios, $stocktodo);
            }
            if(isset($_POST['rol-privilegiosall'])){
                $permisostodos = array(
                    $id,
                    'Nivel todos',
                    'rol-privilegiosall'
                );
                array_push($privilegios, $permisostodos);
            }
            
            
            if(isset($_POST['rol-bancoeditar'])){
                $bancoedit = array(
                    $id,
                    'Editar bancos',
                    'rol-bancoeditar'
                );
                array_push($privilegios, $bancoedit);
            }
            if(isset($_POST['rol-bancoeliminar'])){
                $bancoelimin = array(
                    $id,
                    'Eliminar bancos',
                    'rol-bancoeliminar'
                );
                array_push($privilegios, $bancoelimin);
            }
            if(isset($_POST['rol-bancolistar'])){
                $bancolist = array(
                    $id,
                    'Listar bancos',
                    'rol-bancolistar'
                );
                array_push($privilegios, $bancolist);
            }
            if(isset($_POST['rol-banconuevo'])){
                $banconuevo = array(
                    $id,
                    'Nuevo banco',
                    'rol-banconuevo'
                );
                array_push($privilegios, $banconuevo);
            }
            if(isset($_POST['rol-bancoall'])){
                $bancotodo = array(
                    $id,
                    'Banco todos',
                    'rol-bancoall'
                );
                array_push($privilegios, $bancotodo);
            }
            if(isset($_POST['rol-cuentabancaeditar'])){
                $cuentabaneditar = array(
                    $id,
                    'Cuenta bancaria editar',
                    'rol-cuentabancaeditar'
                );
                array_push($privilegios, $cuentabaneditar);
            }
            if(isset($_POST['rol-cuentabancaeliminar'])){
                $cuentabanelimin = array(
                    $id,
                    'Cuenta bancaria eliminar',
                    'rol-cuentabancaeliminar'
                );
                array_push($privilegios, $cuentabanelimin);
            }
            if(isset($_POST['rol-cuentabancalistar'])){
                $cuentabancarialist = array(
                    $id,
                    'Cuenta bancaria listar',
                    'rol-cuentabancalistar'
                );
                array_push($privilegios, $cuentabancarialist);
            }
            if(isset($_POST['rol-cuentabancanuevo'])){
                $cuentabancanuevo = array(
                    $id,
                    'Cuenta bancaria nuevo',
                    'rol-cuentabancanuevo'
                );
                array_push($privilegios, $cuentabancanuevo);
            }
            
            if(isset($_POST['rol-cuentabancaall'])){
                $cuentabancatodo = array(
                    $id,
                    'Cuenta bancaria todos',
                    'rol-cuentabancaall'
                );
                array_push($privilegios, $cuentabancatodo);
            }
            
            
            if(isset($_POST['rol-clienteeliminar'])){
                $clienteelimin = array(
                    $id,
                    'Cliente eliminar',
                    'rol-clienteeliminar'
                );
                array_push($privilegios, $clienteelimin);
            }
            if(isset($_POST['rol-clientelistar'])){
                $clientelistar = array(
                    $id,
                    'Cliente listar',
                    'rol-clientelistar'
                );
                array_push($privilegios, $clientelistar);
            }
              if(isset($_POST['rol-clienteeditar'])){
                $clienteedit = array(
                    $id,
                    'Cliente editar',
                    'rol-clienteeditar'
                );
                array_push($privilegios, $clienteedit);
            }
            if(isset($_POST['rol-clientenuevo'])){
                $clientenuevo = array(
                    $id,
                    'Cliente nuevo',
                    'rol-clientenuevo'
                );
                array_push($privilegios, $clientenuevo);
            }
            if(isset($_POST['rol-clientedefecto'])){
                $clientedefecto = array(
                    $id,
                    'Cliente por defecto',
                    'rol-clientedefecto'
                );
                array_push($privilegios, $clientedefecto);
            }
            if(isset($_POST['rol-clienteall'])){
                $clientetodo = array(
                    $id,
                    'Cliente todos',
                    'rol-clienteall'
                );
                array_push($privilegios, $clientetodo);
            }
            
            
            if(isset($_POST['rol-proveedoreditar'])){
                $proveedoredit = array(
                    $id,
                    'Proveedor editar',
                    'rol-proveedoreditar'
                );
                array_push($privilegios, $proveedoredit);
            }
            if(isset($_POST['rol-proveedoreliminar'])){
                $proveedorelimin = array(
                    $id,
                    'Proveedor eliminar',
                    'rol-proveedoreliminar'
                );
                array_push($privilegios, $proveedorelimin);
            }
            if(isset($_POST['rol-proveedorlistar'])){
                $proveedorlist = array(
                    $id,
                    'Proveedor listar',
                    'rol-proveedorlistar'
                );
                array_push($privilegios, $proveedorlist);
            }
            if(isset($_POST['rol-proveedornuevo'])){
                $proveedornuevo = array(
                    $id,
                    'Proveedor nuevo',
                    'rol-proveedornuevo'
                );
                array_push($privilegios, $proveedornuevo);
            }
            if(isset($_POST['rol-proveedordefecto'])){
                $proveedordefecto = array(
                    $id,
                    'Proveedor por defecto',
                    'rol-proveedordefecto'
                );
                array_push($privilegios, $proveedordefecto);
            }
            if(isset($_POST['rol-proveedorall'])){
                $proveedortodo = array(
                    $id,
                    'Proveedor todos',
                    'rol-proveedorall'
                );
                array_push($privilegios, $proveedortodo);
            }
            if(isset($_POST['rol-marcaeditar'])){
                $marcaedit = array(
                    $id,
                    'Marca editar',
                    'rol-marcaeditar'
                );
                array_push($privilegios, $marcaedit);
            }
            if(isset($_POST['rol-marcaeliminar'])){
                $marcaelim = array(
                    $id,
                    'Marca eliminar',
                    'rol-marcaeliminar'
                );
                array_push($privilegios, $marcaelim);
            }
            if(isset($_POST['rol-marcalistar'])){
                $marcalist = array(
                    $id,
                    'Marca listar',
                    'rol-marcalistar'
                );
                array_push($privilegios, $marcalist);
            }
            if(isset($_POST['rol-marcanuevo'])){
                $marcanuevo = array(
                    $id,
                    'Marca nuevo',
                    'rol-marcanuevo'
                );
                array_push($privilegios, $marcanuevo);
            }
            if(isset($_POST['rol-marcaall'])){
                $marcatodo= array(
                    $id,
                    'Marca todos',
                    'rol-marcaall'
                );
                array_push($privilegios, $marcatodo);
            }
            if(isset($_POST['rol-unideditar'])){
                $unideditar = array(
                    $id,
                    'Unid. medida editar',
                    'rol-unideditar'
                );
                array_push($privilegios, $unideditar);
            }
            if(isset($_POST['rol-unideliminar'])){
                $unideliminar = array(
                    $id,
                    'Unid. medida eliminar',
                    'rol-unideliminar'
                );
                array_push($privilegios, $unideliminar);
            }
            if(isset($_POST['rol-unidlistar'])){
                $unidlistar = array(
                    $id,
                    'Unid. medida listar',
                    'rol-unidlistar'
                );
                array_push($privilegios, $unidlistar);
            }
            if(isset($_POST['rol-unidnuevo'])){
                $unidnuevo = array(
                    $id,
                    'Unid. medida nuevo',
                    'rol-unidnuevo'
                );
                array_push($privilegios, $unidnuevo);
            }
            if(isset($_POST['rol-unidall'])){
                $unidtodo = array(
                    $id,
                    'Unid. medida todos',
                    'rol-unidall'
                );
                array_push($privilegios, $unidtodo);
            }
            if(isset($_POST['rol-categoriaeditar'])){
                $categoriaedit = array(
                    $id,
                    'Categoria editar',
                    'rol-categoriaeditar'
                );
                array_push($privilegios, $categoriaedit);
            }
            if(isset($_POST['rol-categoriaeliminar'])){
                $categoriaelim = array(
                    $id,
                    'Categoria eliminar',
                    'rol-categoriaeliminar'
                );
                array_push($privilegios, $categoriaelim);
            }
            if(isset($_POST['rol-categorialistar'])){
                $categorialist = array(
                    $id,
                    'Categoria listar',
                    'rol-categorialistar'
                );
                array_push($privilegios, $categorialist);
            }
            if(isset($_POST['rol-categorianuevo'])){
                $categorianuevo = array(
                    $id,
                    'Categoria nuevo',
                    'rol-categorianuevo'
                );
                array_push($privilegios, $categorianuevo);
            }
            if(isset($_POST['rol-categoriaall'])){
                $categoriatodos = array(
                    $id,
                    'Categoria todos',
                    'rol-categoriaall'
                );
                array_push($privilegios, $categoriatodos);
            }
            if(isset($_POST['rol-lineaeditar'])){
                $lineaedit = array(
                    $id,
                    'Linea editar',
                    'rol-lineaeditar'
                );
                array_push($privilegios, $lineaedit);
            }
            if(isset($_POST['rol-lineaeliminar'])){
                $lineaelim = array(
                    $id,
                    'Linea eliminar',
                    'rol-lineaeliminar'
                );
                array_push($privilegios, $lineaelim);
            }
            if(isset($_POST['rol-linealistar'])){
                $linealist = array(
                    $id,
                    'Linea listar',
                    'rol-linealistar'
                );
                array_push($privilegios, $linealist);
            }
            if(isset($_POST['rol-lineanuevo'])){
                $lineanuevo = array(
                    $id,
                    'Linea nuevo',
                    'rol-lineanuevo'
                );
                array_push($privilegios, $lineanuevo);
            }
            if(isset($_POST['rol-lineaall'])){
                $lineatodo = array(
                    $id,
                    'Linea todo',
                    'rol-lineaall'
                );
                array_push($privilegios, $lineatodo);
            }
            if(isset($_POST['rol-articuloeditar'])){
                $articuloeditar = array(
                    $id,
                    'Articulo editar',
                    'rol-articuloeditar'
                );
                array_push($privilegios, $articuloeditar);
            }
            if(isset($_POST['rol-articuloeliminar'])){
                $articuloelim = array(
                    $id,
                    'Articulo eliminar',
                    'rol-articuloeliminar'
                );
                array_push($privilegios, $articuloelim);
            }
            if(isset($_POST['rol-articulolistar'])){
                $articulolist = array(
                    $id,
                    'Articulo listar',
                    'rol-articulolistar'
                );
                array_push($privilegios, $articulolist);
            }
            if(isset($_POST['rol-articulonuevo'])){
                $articulonuev = array(
                    $id,
                    'Articulo nuevo',
                    'rol-articulonuevo'
                );
                array_push($privilegios, $articulonuev);
            }
            if(isset($_POST['rol-articuloall'])){
                $articulotodo = array(
                    $id,
                    'Articulo todos',
                    'rol-articuloall'
                );
                array_push($privilegios, $articulotodo);
            }
            if(isset($_POST['rol-servicioeditar'])){
                $servicioedit = array(
                    $id,
                    'Servicio editar',
                    'rol-servicioeditar'
                );
                array_push($privilegios, $servicioedit);
            }
            if(isset($_POST['rol-servicioeliminar'])){
                $servicioeliminar = array(
                    $id,
                    'Servicio eliminar',
                    'rol-servicioeliminar'
                );
                array_push($privilegios, $servicioeliminar);
            }
            if(isset($_POST['rol-serviciolistar'])){
                $serviciolist = array(
                    $id,
                    'Servicio listar',
                    'rol-serviciolistar'
                );
                array_push($privilegios, $serviciolist);
            }
            if(isset($_POST['rol-servicionuevo'])){
                $servicionuev = array(
                    $id,
                    'Servicio nuevo',
                    'rol-servicionuevo'
                );
                array_push($privilegios, $servicionuev);
            }
            if(isset($_POST['rol-servicioall'])){
                $serviciotodo = array(
                    $id,
                    'Servicio todos',
                    'rol-servicioall'
                );
                array_push($privilegios, $serviciotodo);
            }
            if(isset($_POST['rol-usuarioeditar'])){
                $usuarioedit = array(
                    $id,
                    'Usuario editar',
                    'rol-usuarioeditar'
                );
                array_push($privilegios, $usuarioedit);
            }
            if(isset($_POST['rol-usuarioeliminar'])){
                $usuarioelim = array(
                    $id,
                    'Usuario eliminar',
                    'rol-usuarioeliminar'
                );
                array_push($privilegios, $usuarioelim);
            }
            if(isset($_POST['rol-usuariolistar'])){
                $usuariolist = array(
                    $id,
                    'Usuario listar',
                    'rol-usuariolistar'
                );
                array_push($privilegios, $usuariolist);
            }
            if(isset($_POST['rol-usuarionuevo'])){
                $usuarionuevo = array(
                    $id,
                    'Usuario nuevo',
                    'rol-usuarionuevo'
                );
                array_push($privilegios, $usuarionuevo);
            }
            if(isset($_POST['rol-usuarioall'])){
                $usuariotodos = array(
                    $id,
                    'Usuario todos',
                    'rol-usuarioall'
                );
                array_push($privilegios, $usuariotodos);
            }
//            if(isset($_POST['rol-sucursalprincipaleditar'])){
//                $sucurpedit = array(
//                    $id,
//                    'Sucursal principal editar',
//                    'rol-sucursalprincipaleditar'
//                );
//                array_push($privilegios, $sucurpedit);
//            }
//            if(isset($_POST['rol-sucursalprincipalmostrar'])){
//                $sucurpmostrar = array(
//                    $id,
//                    'Sucursal principal mostrar',
//                    'rol-sucursalprincipalmostrar'
//                );
//                array_push($privilegios, $sucurpmostrar);
//            }
            if(isset($_POST['rol-sucursaleditar'])){
                $sucursaledit = array(
                    $id,
                    'Sucursal editar',
                    'rol-sucursaleditar'
                );
                array_push($privilegios, $sucursaledit);
            }
            if(isset($_POST['rol-sucursaleliminar'])){
                $sucursalelim = array(
                    $id,
                    'Sucursal eliminar',
                    'rol-sucursaleliminar'
                );
                array_push($privilegios, $sucursalelim);
            }
            if(isset($_POST['rol-sucursallistar'])){
                $sucursallist = array(
                    $id,
                    'Sucursal listar',
                    'rol-sucursallistar'
                );
                array_push($privilegios, $sucursallist);
            }
            if(isset($_POST['rol-sucursalnuevo'])){
                $sucursalnuevo = array(
                    $id,
                    'Sucursal nuevo',
                    'rol-sucursalnuevo'
                );
                array_push($privilegios, $sucursalnuevo);
            }
            if(isset($_POST['rol-sucursalall'])){
                $sucursaltodo = array(
                    $id,
                    'Sucursal todos',
                    'rol-sucursalall'
                );
                array_push($privilegios, $sucursaltodo);
            }
            if(isset($_POST['rol-documentoeditar'])){
                $doceditar = array(
                    $id,
                    'Documento editar',
                    'rol-documentoeditar'
                );
                array_push($privilegios, $doceditar);
            }
            if(isset($_POST['rol-documentoeliminar'])){
                $doceliminar = array(
                    $id,
                    'Documento eliminar',
                    'rol-documentoeliminar'
                );
                array_push($privilegios, $doceliminar);
            }
            if(isset($_POST['rol-documentolistar'])){
                $documentolist = array(
                    $id,
                    'Documento listar',
                    'rol-documentolistar'
                );
                array_push($privilegios, $documentolist);
            }
            if(isset($_POST['rol-documentonuevo'])){
                $docnuevo = array(
                    $id,
                    'Documento nuevo',
                    'rol-documentonuevo'
                );
                array_push($privilegios, $docnuevo);
            }
            if(isset($_POST['rol-documentoall'])){
                $doctodo = array(
                    $id,
                    'Documento todos',
                    'rol-documentoall'
                );
                array_push($privilegios, $doctodo);
            }
            if(isset($_POST['rol-ventaall'])){
                $ventatodo = array(
                    $id,
                    'Ventas todos',
                    'rol-ventaall'
                );
                array_push($privilegios, $ventatodo);
            }
            if(isset($_POST['rol-vercomprobanteventa'])){
                $vercomprobanvent = array(
                    $id,
                    'Ver comprobantes ventas',
                    'rol-vercomprobanteventa'
                );
                array_push($privilegios, $vercomprobanvent);
            }
            if(isset($_POST['rol-vercomprobanteventaporsucur'])){
                $comprobanteventsucur = array(
                    $id,
                    'Ver comprobantes por sucursal [ventas]',
                    'rol-vercomprobanteventaporsucur'
                );
                array_push($privilegios, $comprobanteventsucur);
            }
            if(isset($_POST['rol-boletafacturaventa'])){
                $boletafactventa = array(
                    $id,
                    'Boleta / Factura [venta] ',
                    'rol-boletafacturaventa'
                );
                array_push($privilegios, $boletafactventa);
            }
            if(isset($_POST['rol-notacreditoventa'])){
                $notacred = array(
                    $id,
                    'Nota crédito',
                    'rol-notacreditoventa'
                );
                array_push($privilegios, $notacred);
            }
            if(isset($_POST['rol-notadebitoventa'])){
                $notadeb = array(
                    $id,
                    'Nota débito',
                    'rol-notadebitoventa'
                );
                array_push($privilegios, $notadeb);
            }
            if(isset($_POST['rol-cotizacionventa'])){
                $cotiza = array(
                    $id,
                    'Cotización',
                    'rol-cotizacionventa'
                );
                array_push($privilegios, $cotiza);
            }
            if(isset($_POST['rol-vercotizacionesventa'])){
                $vercotiza = array(
                    $id,
                    'Ver cotizaciones',
                    'rol-vercotizacionesventa'
                );
                array_push($privilegios, $vercotiza);
            }
            if(isset($_POST['rol-vercotizacionesventasucur'])){
                $vercotizasucur = array(
                    $id,
                    'Ver cotizaciones por sucursal',
                    'rol-vercotizacionesventasucur'
                );
                array_push($privilegios, $vercotizasucur);
            }
            if(isset($_POST['rol-compraall'])){
                $compratodo = array(
                    $id,
                    'Compras todos',
                    'rol-compraall'
                );
                array_push($privilegios, $compratodo);
            }
            if(isset($_POST['rol-vercomprobantecompra'])){
                $vercomprocompra = array(
                    $id,
                    'Ver comprobates compras',
                    'rol-vercomprobantecompra'
                );
                array_push($privilegios, $vercomprocompra);
            }
            if(isset($_POST['rol-vercomprobantecomprasucur'])){
                $vercomprobancompra = array(
                    $id,
                    'Ver comprobantes por sucursal [compras]',
                    'rol-vercomprobantecomprasucur'
                );
                array_push($privilegios, $vercomprobancompra);
            }
            if(isset($_POST['rol-compra'])){
                $compra = array(
                    $id,
                    'Compra',
                    'rol-compra'
                );
                array_push($privilegios, $compra);
            }
            if(isset($_POST['rol-ordencompra'])){
                $ordencompra = array(
                    $id,
                    'Orden de compra',
                    'rol-ordencompra'
                );
                array_push($privilegios, $ordencompra);
            }
            if(isset($_POST['rol-vercomprobanteordencompra'])){
                $verordencompra = array(
                    $id,
                    'Ver comprobantes orden de compra',
                    'rol-vercomprobanteordencompra'
                );
                array_push($privilegios, $verordencompra);
            }
            if(isset($_POST['rol-vercomprobanteordencomprasucur'])){
                $verordencomprasucur = array(
                    $id,
                    'Ver orden de compra por sucursal',
                    'rol-vercomprobanteordencomprasucur'
                );
                array_push($privilegios, $verordencomprasucur);
            }
            if(isset($_POST['rol-reportesall'])){
                $reportetodo = array(
                    $id,
                    'Reporte todos',
                    'rol-reportesall'
                );
                array_push($privilegios, $reportetodo);
            }
            if(isset($_POST['rol-ventadetallereporte'])){
                $ventadet = array(
                    $id,
                    'Venta por detalle',
                    'rol-ventadetallereporte'
                );
                array_push($privilegios, $ventadet);
            }
            if(isset($_POST['rol-ventadetallereportesucur'])){
                $ventadetsucur = array(
                    $id,
                    'Venta detalle por sucursal',
                    'rol-ventadetallereportesucur'
                );
                array_push($privilegios, $ventadetsucur);
            }
            if(isset($_POST['rol-compradetallereporte'])){
                $compradet = array(
                    $id,
                    'Compra por detalle',
                    'rol-compradetallereporte'
                );
                array_push($privilegios, $compradet);
            }
            if(isset($_POST['rol-compradetallereportesucur'])){
                $compradetsucur = array(
                    $id,
                    'Compra detalle por sucursal',
                    'rol-compradetallereportesucur'
                );
                array_push($privilegios, $compradetsucur);
            }
            if(isset($_POST['rol-generalreporte'])){
                $stockgene = array(
                    $id,
                    'Stock general',
                    'rol-generalreporte'
                );
                array_push($privilegios, $stockgene);
            }
            if(isset($_POST['rol-generalreportesucur'])){
                $stockgenesucur = array(
                    $id,
                    'Stock general por sucursal',
                    'rol-generalreportesucur'
                );
                array_push($privilegios, $stockgenesucur);
            }
            if(isset($_POST['rol-valorizadoreporte'])){
                $valorizadorep = array(
                    $id,
                    'Stock valorizado',
                    'rol-valorizadoreporte'
                );
                array_push($privilegios, $valorizadorep);
            }
            if(isset($_POST['rol-valorizadoreportesucur'])){
                $valorizadorepsucur = array(
                    $id,
                    'Stock valorizado por sucursal',
                    'rol-valorizadoreportesucur'
                );
                array_push($privilegios, $valorizadorepsucur);
            }
            if(isset($_POST['rol-compraventareporte'])){
                $compraventarep = array(
                    $id,
                    'Stock compra - venta ',
                    'rol-compraventareporte'
                );
                array_push($privilegios, $compraventarep);
            }
            if(isset($_POST['rol-compraventareportesucur'])){
                $compraventasucur = array(
                    $id,
                    'Stock compra - venta por sucursal',
                    'rol-compraventareportesucur'
                );
                array_push($privilegios, $compraventasucur);
            }
            if(isset($_POST['rol-kardexreporte'])){
                $kardex = array(
                    $id,
                    'Kardex',
                    'rol-kardexreporte'
                );
                array_push($privilegios, $kardex);
            }
            if(isset($_POST['rol-kardexreportesucur'])){
                $kardexsucur = array(
                    $id,
                    'Kardex por sucursal',
                    'rol-kardexreportesucur'
                );
                array_push($privilegios, $kardexsucur);
            }
            if(isset($_POST['rol-cuentascobrarreporte'])){
                $cuentacobrar = array(
                    $id,
                    'Cuentas por cobrar',
                    'rol-cuentascobrarreporte'
                );
                array_push($privilegios, $cuentacobrar);
            }
            if(isset($_POST['rol-cuentascobrarreportesucur'])){
                $cuentacobrarsucur = array(
                    $id,
                    'Cuenta por cobrar por sucursal',
                    'rol-cuentascobrarreportesucur'
                );
                array_push($privilegios, $cuentacobrarsucur);
            }
            if(isset($_POST['rol-cuentaspagarreporte'])){
                $cuentapagar = array(
                    $id,
                    'Cuentas por pagar',
                    'rol-cuentaspagarreporte'
                );
                array_push($privilegios, $cuentapagar);
            }
            if(isset($_POST['rol-cuentaspagarreportesucur'])){
                $cuentapagarsucur = array(
                    $id,
                    'Cuentas por pagar sucursal',
                    'rol-cuentaspagarreportesucur'
                );
                array_push($privilegios, $cuentapagarsucur);
            }
            if(isset($_POST['rol-operacionesall'])){
                $optodos = array(
                    $id,
                    'Operaciones todos',
                    'rol-operacionesall'
                );
                array_push($privilegios, $optodos);
            }
            if(isset($_POST['rol-gastosoperaciones'])){
                $gastos = array(
                    $id,
                    'Gastos',
                    'rol-gastosoperaciones'
                );
                array_push($privilegios, $gastos);
            }
            if(isset($_POST['rol-gastosoperacionessucur'])){
                $gastosucur = array(
                    $id,
                    'Gastos por sucursal',
                    'rol-gastosoperacionessucur'
                );
                array_push($privilegios, $gastosucur);
            }
            if(isset($_POST['rol-movimientocajaoperaciones'])){
                $movcaja = array(
                    $id,
                    'Movimiento de caja',
                    'rol-movimientocajaoperaciones'
                );
                array_push($privilegios, $movcaja);
            }
            if(isset($_POST['rol-trasladooperaciones'])){
                $traslado = array(
                    $id,
                    'Traslado de articulos',
                    'rol-trasladooperaciones'
                );
                array_push($privilegios, $traslado);
            }
            if(isset($_POST['rol-panel'])){
                $panel = array(
                    $id,
                    'Panel administrativo',
                    'rol-panel'
                );
                array_push($privilegios, $panel);
            }
            
//            var_dump($privilegios);
            if(count($privilegios) >0){
                $det =$niveldet->insert($privilegios);
                if($id>0 && count($det)>0){
                    ?> 
                  <script>

                       swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                       $('#FormularioAjax').trigger("reset");

                  </script>  <?php
                }else{
                     ?> 
            <script>
               
                 swal('No se realizo registro', 'Algo sucedio mal :(', 'error');
            </script>  <?php
                    
                }
                
            }           
                else {
            
             ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese Permisos', 'error');
            </script>  <?php
        }
            }
            

    }
    
    
     function update(){
//         var_dump($_POST);
        if($_POST){
            $id = (int)$_POST['id'];
            
            $nivel = new nivel();
            $nivel->setDescripcion($_POST['txtdescripcion']);
            $nivel->setId($id);
            $niveldet = new detalleNivel();
            
            $nivel->update($nivel);
            $privilegios = array();
            
              if(isset($_POST['rol-editarprecio'])){
                $editprecio = array(
                    $id,
                    'Editar precio documento',
                    'rol-editarprecio'
                );
                array_push($privilegios, $editprecio);
            }
            
              if(isset($_POST['rol-stockreporte'])){
                $stocktodo = array(
                    $id,
                    'Stock todos',
                    'rol-stockreporte'
                );
                array_push($privilegios, $stocktodo);
            }
            
             if(isset($_POST['rol-privilegiosall'])){
                $permisostodos = array(
                    $id,
                    'Nivel todos',
                    'rol-privilegiosall'
                );
                array_push($privilegios, $permisostodos);
            }
            if(isset($_POST['rol-bancoall'])){
                $bancotodo = array(
                    $id,
                    'Banco todos',
                    'rol-bancoall'
                );
                array_push($privilegios, $bancotodo);
            }
             if(isset($_POST['rol-cuentabancaall'])){
                $cuentabancatodo = array(
                    $id,
                    'Cuenta bancaria todos',
                    'rol-cuentabancaall'
                );
                array_push($privilegios, $cuentabancatodo);
            }
              if(isset($_POST['rol-clienteall'])){
                $clientetodo = array(
                    $id,
                    'Cliente todos',
                    'rol-clienteall'
                );
                array_push($privilegios, $clientetodo);
            }
              if(isset($_POST['rol-proveedorall'])){
                $proveedortodo = array(
                    $id,
                    'Proveedor todos',
                    'rol-proveedorall'
                );
                array_push($privilegios, $proveedortodo);
            }
             if(isset($_POST['rol-marcaall'])){
                $marcatodo= array(
                    $id,
                    'Marca todos',
                    'rol-marcaall'
                );
                array_push($privilegios, $marcatodo);
            }
              if(isset($_POST['rol-unidall'])){
                $unidtodo = array(
                    $id,
                    'Unid. medida todos',
                    'rol-unidall'
                );
                array_push($privilegios, $unidtodo);
            }
             if(isset($_POST['rol-categoriaall'])){
                $categoriatodos = array(
                    $id,
                    'Categoria todos',
                    'rol-categoriaall'
                );
                array_push($privilegios, $categoriatodos);
            }
              if(isset($_POST['rol-lineaall'])){
                $lineatodo = array(
                    $id,
                    'Linea todo',
                    'rol-lineaall'
                );
                array_push($privilegios, $lineatodo);
            }
              if(isset($_POST['rol-articuloall'])){
                $articulotodo = array(
                    $id,
                    'Articulo todos',
                    'rol-articuloall'
                );
                array_push($privilegios, $articulotodo);
            }
             if(isset($_POST['rol-servicioall'])){
                $serviciotodo = array(
                    $id,
                    'Servicio todos',
                    'rol-servicioall'
                );
                array_push($privilegios, $serviciotodo);
            }
              if(isset($_POST['rol-usuarioall'])){
                $usuariotodos = array(
                    $id,
                    'Usuario todos',
                    'rol-usuarioall'
                );
                array_push($privilegios, $usuariotodos);
            }
                if(isset($_POST['rol-sucursalall'])){
                $sucursaltodo = array(
                    $id,
                    'Sucursal todos',
                    'rol-sucursalall'
                );
                array_push($privilegios, $sucursaltodo);
            }
                if(isset($_POST['rol-documentoall'])){
                $doctodo = array(
                    $id,
                    'Documento todos',
                    'rol-documentoall'
                );
                array_push($privilegios, $doctodo);
            }
              if(isset($_POST['rol-ventaall'])){
                $ventatodo = array(
                    $id,
                    'Ventas todos',
                    'rol-ventaall'
                );
                array_push($privilegios, $ventatodo);
            }
               if(isset($_POST['rol-compraall'])){
                $compratodo = array(
                    $id,
                    'Compras todos',
                    'rol-compraall'
                );
                array_push($privilegios, $compratodo);
            }
                if(isset($_POST['rol-reportesall'])){
                $reportetodo = array(
                    $id,
                    'Reporte todos',
                    'rol-reportesall'
                );
                array_push($privilegios, $reportetodo);
            }
               if(isset($_POST['rol-operacionesall'])){
                $optodos = array(
                    $id,
                    'Operaciones todos',
                    'rol-operacionesall'
                );
                array_push($privilegios, $optodos);
            }
                if(isset($_POST['rol-panel'])){
                $panel = array(
                    $id,
                    'Panel administrativo',
                    'rol-panel'
                );
                array_push($privilegios,$panel );
            }
            
            
            
            if(isset($_POST['rol-permisos'])){
                $permisos = array(
                    $id,
                    'Listar nivel',
                    'rol-permisos'
                );
                array_push($privilegios, $permisos);
            }
            if(isset($_POST['rol-bancoeditar'])){
                $bancoedit = array(
                    $id,
                    'Editar bancos',
                    'rol-bancoeditar'
                );
                array_push($privilegios, $bancoedit);
            }
            if(isset($_POST['rol-bancoeliminar'])){
                $bancoelimin = array(
                    $id,
                    'Eliminar bancos',
                    'rol-bancoeliminar'
                );
                array_push($privilegios, $bancoelimin);
            }
            if(isset($_POST['rol-bancolistar'])){
                $bancolist = array(
                    $id,
                    'Listar bancos',
                    'rol-bancolistar'
                );
                array_push($privilegios, $bancolist);
            }
            if(isset($_POST['rol-banconuevo'])){
                $banconuevo = array(
                    $id,
                    'Nuevo banco',
                    'rol-banconuevo'
                );
                array_push($privilegios, $banconuevo);
            }
            if(isset($_POST['rol-cuentabancaeditar'])){
                $cuentabaneditar = array(
                    $id,
                    'Cuenta bancaria editar',
                    'rol-cuentabancaeditar'
                );
                array_push($privilegios, $cuentabaneditar);
            }
            if(isset($_POST['rol-cuentabancaeliminar'])){
                $cuentabanelimin = array(
                    $id,
                    'Cuenta bancaria eliminar',
                    'rol-cuentabancaeliminar'
                );
                array_push($privilegios, $cuentabanelimin);
            }
            if(isset($_POST['rol-cuentabancalistar'])){
                $cuentabancarialist = array(
                    $id,
                    'Cuenta bancaria listar',
                    'rol-cuentabancalistar'
                );
                array_push($privilegios, $cuentabancarialist);
            }
            if(isset($_POST['rol-cuentabancanuevo'])){
                $cuentabancanuevo = array(
                    $id,
                    'Cuenta bancaria nuevo',
                    'rol-cuentabancanuevo'
                );
                array_push($privilegios, $cuentabancanuevo);
            }
            if(isset($_POST['rol-clienteeliminar'])){
                $clienteelimin = array(
                    $id,
                    'Cliente eliminar',
                    'rol-clienteeliminar'
                );
                array_push($privilegios, $clienteelimin);
            }
            if(isset($_POST['rol-clientelistar'])){
                $clientelistar = array(
                    $id,
                    'Cliente listar',
                    'rol-clientelistar'
                );
                array_push($privilegios, $clientelistar);
            }
            if(isset($_POST['rol-clienteeditar'])){
                $clienteedit = array(
                    $id,
                    'Cliente editar',
                    'rol-clienteeditar'
                );
                array_push($privilegios, $clienteedit);
            }
            if(isset($_POST['rol-clientenuevo'])){
                $clientenuevo = array(
                    $id,
                    'Cliente nuevo',
                    'rol-clientenuevo'
                );
                array_push($privilegios, $clientenuevo);
            }
            if(isset($_POST['rol-clientedefecto'])){
                $clientedefecto = array(
                    $id,
                    'Cliente por defecto',
                    'rol-clientedefecto'
                );
                array_push($privilegios, $clientedefecto);
            }
            if(isset($_POST['rol-proveedoreditar'])){
                $proveedoredit = array(
                    $id,
                    'Proveedor editar',
                    'rol-proveedoreditar'
                );
                array_push($privilegios, $proveedoredit);
            }
            if(isset($_POST['rol-proveedoreliminar'])){
                $proveedorelimin = array(
                    $id,
                    'Proveedor eliminar',
                    'rol-proveedoreliminar'
                );
                array_push($privilegios, $proveedorelimin);
            }
            if(isset($_POST['rol-proveedorlistar'])){
                $proveedorlist = array(
                    $id,
                    'Proveedor listar',
                    'rol-proveedorlistar'
                );
                array_push($privilegios, $proveedorlist);
            }
            if(isset($_POST['rol-proveedornuevo'])){
                $proveedornuevo = array(
                    $id,
                    'Proveedor nuevo',
                    'rol-proveedornuevo'
                );
                array_push($privilegios, $proveedornuevo);
            }
            if(isset($_POST['rol-proveedordefecto'])){
                $proveedordefecto = array(
                    $id,
                    'Proveedor por defecto',
                    'rol-proveedordefecto'
                );
                array_push($privilegios, $proveedordefecto);
            }
            if(isset($_POST['rol-marcaeditar'])){
                $marcaedit = array(
                    $id,
                    'Marca editar',
                    'rol-marcaeditar'
                );
                array_push($privilegios, $marcaedit);
            }
            if(isset($_POST['rol-marcaeliminar'])){
                $marcaelim = array(
                    $id,
                    'Marca eliminar',
                    'rol-marcaeliminar'
                );
                array_push($privilegios, $marcaelim);
            }
            if(isset($_POST['rol-marcalistar'])){
                $marcalist = array(
                    $id,
                    'Marca listar',
                    'rol-marcalistar'
                );
                array_push($privilegios, $marcalist);
            }
            if(isset($_POST['rol-marcanuevo'])){
                $marcanuevo = array(
                    $id,
                    'Marca nuevo',
                    'rol-marcanuevo'
                );
                array_push($privilegios, $marcanuevo);
            }
            if(isset($_POST['rol-unideditar'])){
                $unideditar = array(
                    $id,
                    'Unid. medida editar',
                    'rol-unideditar'
                );
                array_push($privilegios, $unideditar);
            }
            if(isset($_POST['rol-unideliminar'])){
                $unideliminar = array(
                    $id,
                    'Unid. medida eliminar',
                    'rol-unideliminar'
                );
                array_push($privilegios, $unideliminar);
            }
            if(isset($_POST['rol-unidlistar'])){
                $unidlistar = array(
                    $id,
                    'Unid. medida listar',
                    'rol-unidlistar'
                );
                array_push($privilegios, $unidlistar);
            }
            if(isset($_POST['rol-unidnuevo'])){
                $unidnuevo = array(
                    $id,
                    'Unid. medida nuevo',
                    'rol-unidnuevo'
                );
                array_push($privilegios, $unidnuevo);
            }
            if(isset($_POST['rol-categoriaeditar'])){
                $categoriaedit = array(
                    $id,
                    'Categoria editar',
                    'rol-categoriaeditar'
                );
                array_push($privilegios, $categoriaedit);
            }
            if(isset($_POST['rol-categoriaeliminar'])){
                $categoriaelim = array(
                    $id,
                    'Categoria eliminar',
                    'rol-categoriaeliminar'
                );
                array_push($privilegios, $categoriaelim);
            }
            if(isset($_POST['rol-categorialistar'])){
                $categorialist = array(
                    $id,
                    'Categoria listar',
                    'rol-categorialistar'
                );
                array_push($privilegios, $categorialist);
            }
            if(isset($_POST['rol-categorianuevo'])){
                $categorianuevo = array(
                    $id,
                    'Categoria nuevo',
                    'rol-categorianuevo'
                );
                array_push($privilegios, $categorianuevo);
            }
            if(isset($_POST['rol-lineaeditar'])){
                $lineaedit = array(
                    $id,
                    'Linea editar',
                    'rol-lineaeditar'
                );
                array_push($privilegios, $lineaedit);
            }
            if(isset($_POST['rol-lineaeliminar'])){
                $lineaelim = array(
                    $id,
                    'Linea eliminar',
                    'rol-lineaeliminar'
                );
                array_push($privilegios, $lineaelim);
            }
            if(isset($_POST['rol-linealistar'])){
                $linealist = array(
                    $id,
                    'Linea listar',
                    'rol-linealistar'
                );
                array_push($privilegios, $linealist);
            }
            if(isset($_POST['rol-lineanuevo'])){
                $lineanuevo = array(
                    $id,
                    'Linea nuevo',
                    'rol-lineanuevo'
                );
                array_push($privilegios, $lineanuevo);
            }
            if(isset($_POST['rol-articuloeditar'])){
                $articuloeditar = array(
                    $id,
                    'Articulo editar',
                    'rol-articuloeditar'
                );
                array_push($privilegios, $articuloeditar);
            }
            if(isset($_POST['rol-articuloeliminar'])){
                $articuloelim = array(
                    $id,
                    'Articulo eliminar',
                    'rol-articuloeliminar'
                );
                array_push($privilegios, $articuloelim);
            }
            if(isset($_POST['rol-articulolistar'])){
                $articulolist = array(
                    $id,
                    'Articulo listar',
                    'rol-articulolistar'
                );
                array_push($privilegios, $articulolist);
            }
            if(isset($_POST['rol-articulonuevo'])){
                $articulonuev = array(
                    $id,
                    'Articulo nuevo',
                    'rol-articulonuevo'
                );
                array_push($privilegios, $articulonuev);
            }
            if(isset($_POST['rol-servicioeditar'])){
                $servicioedit = array(
                    $id,
                    'Servicio editar',
                    'rol-servicioeditar'
                );
                array_push($privilegios, $servicioedit);
            }
            if(isset($_POST['rol-servicioeliminar'])){
                $servicioeliminar = array(
                    $id,
                    'Servicio eliminar',
                    'rol-servicioeliminar'
                );
                array_push($privilegios, $servicioeliminar);
            }
            if(isset($_POST['rol-serviciolistar'])){
                $serviciolist = array(
                    $id,
                    'Servicio listar',
                    'rol-serviciolistar'
                );
                array_push($privilegios, $serviciolist);
            }
            if(isset($_POST['rol-servicionuevo'])){
                $servicionuev = array(
                    $id,
                    'Servicio nuevo',
                    'rol-servicionuevo'
                );
                array_push($privilegios, $servicionuev);
            }
            if(isset($_POST['rol-usuarioeditar'])){
                $usuarioedit = array(
                    $id,
                    'Usuario editar',
                    'rol-usuarioeditar'
                );
                array_push($privilegios, $usuarioedit);
            }
            if(isset($_POST['rol-usuarioeliminar'])){
                $usuarioelim = array(
                    $id,
                    'Usuario eliminar',
                    'rol-usuarioeliminar'
                );
                array_push($privilegios, $usuarioelim);
            }
            if(isset($_POST['rol-usuariolistar'])){
                $usuariolist = array(
                    $id,
                    'Usuario listar',
                    'rol-usuariolistar'
                );
                array_push($privilegios, $usuariolist);
            }
            if(isset($_POST['rol-usuarionuevo'])){
                $usuarionuevo = array(
                    $id,
                    'Usuario nuevo',
                    'rol-usuarionuevo'
                );
                array_push($privilegios, $usuarionuevo);
            }
            if(isset($_POST['rol-sucursalprincipaleditar'])){
                $sucurpedit = array(
                    $id,
                    'Sucursal principal editar',
                    'rol-sucursalprincipaleditar'
                );
                array_push($privilegios, $sucurpedit);
            }
            if(isset($_POST['rol-sucursalprincipalmostrar'])){
                $sucurpmostrar = array(
                    $id,
                    'Sucursal principal mostrar',
                    'rol-sucursalprincipalmostrar'
                );
                array_push($privilegios, $sucurpmostrar);
            }
            if(isset($_POST['rol-sucursaleditar'])){
                $sucursaledit = array(
                    $id,
                    'Sucursal editar',
                    'rol-sucursaleditar'
                );
                array_push($privilegios, $sucursaledit);
            }
            if(isset($_POST['rol-sucursaleliminar'])){
                $sucursalelim = array(
                    $id,
                    'Sucursal eliminar',
                    'rol-sucursaleliminar'
                );
                array_push($privilegios, $sucursalelim);
            }
            if(isset($_POST['rol-sucursallistar'])){
                $sucursallist = array(
                    $id,
                    'Sucursal listar',
                    'rol-sucursallistar'
                );
                array_push($privilegios, $sucursallist);
            }
            if(isset($_POST['rol-sucursalnuevo'])){
                $sucursalnuevo = array(
                    $id,
                    'Sucursal nuevo',
                    'rol-sucursalnuevo'
                );
                array_push($privilegios, $sucursalnuevo);
            }
            if(isset($_POST['rol-documentoeditar'])){
                $doceditar = array(
                    $id,
                    'Documento editar',
                    'rol-documentoeditar'
                );
                array_push($privilegios, $doceditar);
            }
            if(isset($_POST['rol-documentoeliminar'])){
                $doceliminar = array(
                    $id,
                    'Documento eliminar',
                    'rol-documentoeliminar'
                );
                array_push($privilegios, $doceliminar);
            }
            if(isset($_POST['rol-documentolistar'])){
                $documentolist = array(
                    $id,
                    'Documento listar',
                    'rol-documentolistar'
                );
                array_push($privilegios, $documentolist);
            }
            if(isset($_POST['rol-documentonuevo'])){
                $docnuevo = array(
                    $id,
                    'Documento nuevo',
                    'rol-documentonuevo'
                );
                array_push($privilegios, $docnuevo);
            }
            if(isset($_POST['rol-vercomprobanteventa'])){
                $vercomprobanvent = array(
                    $id,
                    'Ver comprobantes ventas',
                    'rol-vercomprobanteventa'
                );
                array_push($privilegios, $vercomprobanvent);
            }
            if(isset($_POST['rol-vercomprobanteventaporsucur'])){
                $comprobanteventsucur = array(
                    $id,
                    'Ver comprobantes por sucursal [ventas]',
                    'rol-vercomprobanteventaporsucur'
                );
                array_push($privilegios, $comprobanteventsucur);
            }
            if(isset($_POST['rol-boletafacturaventa'])){
                $boletafactventa = array(
                    $id,
                    'Boleta / Factura [venta] ',
                    'rol-boletafacturaventa'
                );
                array_push($privilegios, $boletafactventa);
            }
            if(isset($_POST['rol-notacreditoventa'])){
                $notacred = array(
                    $id,
                    'Nota crédito',
                    'rol-notacreditoventa'
                );
                array_push($privilegios, $notacred);
            }
            if(isset($_POST['rol-notadebitoventa'])){
                $notadeb = array(
                    $id,
                    'Nota débito',
                    'rol-notadebitoventa'
                );
                array_push($privilegios, $notadeb);
            }
            if(isset($_POST['rol-cotizacionventa'])){
                $cotiza = array(
                    $id,
                    'Cotización',
                    'rol-cotizacionventa'
                );
                array_push($privilegios, $cotiza);
            }
            if(isset($_POST['rol-vercotizacionesventa'])){
                $vercotiza = array(
                    $id,
                    'Ver cotizaciones',
                    'rol-vercotizacionesventa'
                );
                array_push($privilegios, $vercotiza);
            }
            if(isset($_POST['rol-vercotizacionesventasucur'])){
                $vercotizasucur = array(
                    $id,
                    'Ver cotizaciones por sucursal',
                    'rol-vercotizacionesventasucur'
                );
                array_push($privilegios, $vercotizasucur);
            }
            if(isset($_POST['rol-vercomprobantecompra'])){
                $vercomprocompra = array(
                    $id,
                    'Ver comprobates compras',
                    'rol-vercomprobantecompra'
                );
                array_push($privilegios, $vercomprocompra);
            }
            if(isset($_POST['rol-vercomprobantecomprasucur'])){
                $vercomprobancompra = array(
                    $id,
                    'Ver comprobantes por sucursal [compras]',
                    'rol-vercomprobantecomprasucur'
                );
                array_push($privilegios, $vercomprobancompra);
            }
            if(isset($_POST['rol-compra'])){
                $compra = array(
                    $id,
                    'Compra',
                    'rol-compra'
                );
                array_push($privilegios, $compra);
            }
            if(isset($_POST['rol-ordencompra'])){
                $ordencompra = array(
                    $id,
                    'Orden de compra',
                    'rol-ordencompra'
                );
                array_push($privilegios, $ordencompra);
            }
            if(isset($_POST['rol-vercomprobanteordencompra'])){
                $verordencompra = array(
                    $id,
                    'Ver comprobantes orden de compra',
                    'rol-vercomprobanteordencompra'
                );
                array_push($privilegios, $verordencompra);
            }
            if(isset($_POST['rol-vercomprobanteordencomprasucur'])){
                $verordencomprasucur = array(
                    $id,
                    'Ver orden de compra por sucursal',
                    'rol-vercomprobanteordencomprasucur'
                );
                array_push($privilegios, $verordencomprasucur);
            }
            if(isset($_POST['rol-ventadetallereporte'])){
                $ventadet = array(
                    $id,
                    'Venta por detalle',
                    'rol-ventadetallereporte'
                );
                array_push($privilegios, $ventadet);
            }
            if(isset($_POST['rol-ventadetallereportesucur'])){
                $ventadetsucur = array(
                    $id,
                    'Venta detalle por sucursal',
                    'rol-ventadetallereportesucur'
                );
                array_push($privilegios, $ventadetsucur);
            }
            if(isset($_POST['rol-compradetallereporte'])){
                $compradet = array(
                    $id,
                    'Compra por detalle',
                    'rol-compradetallereporte'
                );
                array_push($privilegios, $compradet);
            }
            if(isset($_POST['rol-compradetallereportesucur'])){
                $compradetsucur = array(
                    $id,
                    'Compra detalle por sucursal',
                    'rol-compradetallereportesucur'
                );
                array_push($privilegios, $compradetsucur);
            }
            if(isset($_POST['rol-generalreporte'])){
                $stockgene = array(
                    $id,
                    'Stock general',
                    'rol-generalreporte'
                );
                array_push($privilegios, $stockgene);
            }
            if(isset($_POST['rol-generalreportesucur'])){
                $stockgenesucur = array(
                    $id,
                    'Stock general por sucursal',
                    'rol-generalreportesucur'
                );
                array_push($privilegios, $stockgenesucur);
            }
            if(isset($_POST['rol-valorizadoreporte'])){
                $valorizadorep = array(
                    $id,
                    'Stock valorizado',
                    'rol-valorizadoreporte'
                );
                array_push($privilegios, $valorizadorep);
            }
            if(isset($_POST['rol-valorizadoreportesucur'])){
                $valorizadorepsucur = array(
                    $id,
                    'Stock valorizado por sucursal',
                    'rol-valorizadoreportesucur'
                );
                array_push($privilegios, $valorizadorepsucur);
            }
            if(isset($_POST['rol-compraventareporte'])){
                $compraventarep = array(
                    $id,
                    'Stock compra - venta ',
                    'rol-compraventareporte'
                );
                array_push($privilegios, $compraventarep);
            }
            if(isset($_POST['rol-compraventareportesucur'])){
                $compraventasucur = array(
                    $id,
                    'Stock compra - venta por sucursal',
                    'rol-compraventareportesucur'
                );
                array_push($privilegios, $compraventasucur);
            }
            if(isset($_POST['rol-kardexreporte'])){
                $kardex = array(
                    $id,
                    'Kardex',
                    'rol-kardexreporte'
                );
                array_push($privilegios, $kardex);
            }
            if(isset($_POST['rol-kardexreportesucur'])){
                $kardexsucur = array(
                    $id,
                    'Kardex por sucursal',
                    'rol-kardexreportesucur'
                );
                array_push($privilegios, $kardexsucur);
            }
            if(isset($_POST['rol-cuentascobrarreporte'])){
                $cuentacobrar = array(
                    $id,
                    'Cuentas por cobrar',
                    'rol-cuentascobrarreporte'
                );
                array_push($privilegios, $cuentacobrar);
            }
            if(isset($_POST['rol-cuentascobrarreportesucur'])){
                $cuentacobrarsucur = array(
                    $id,
                    'Cuenta por cobrar por sucursal',
                    'rol-cuentascobrarreportesucur'
                );
                array_push($privilegios, $cuentacobrarsucur);
            }
            if(isset($_POST['rol-cuentaspagarreporte'])){
                $cuentapagar = array(
                    $id,
                    'Cuentas por pagar',
                    'rol-cuentaspagarreporte'
                );
                array_push($privilegios, $cuentapagar);
            }
            if(isset($_POST['rol-cuentaspagarreportesucur'])){
                $cuentapagarsucur = array(
                    $id,
                    'Cuentas por pagar sucursal',
                    'rol-cuentaspagarreportesucur'
                );
                array_push($privilegios, $cuentapagarsucur);
            }
            if(isset($_POST['rol-gastosoperaciones'])){
                $gastos = array(
                    $id,
                    'Gastos',
                    'rol-gastosoperaciones'
                );
                array_push($privilegios, $gastos);
            }
            if(isset($_POST['rol-gastosoperacionessucur'])){
                $gastosucur = array(
                    $id,
                    'Gastos por sucursal',
                    'rol-gastosoperacionessucur'
                );
                array_push($privilegios, $gastosucur);
            }
            if(isset($_POST['rol-movimientocajaoperaciones'])){
                $movcaja = array(
                    $id,
                    'Movimiento de caja',
                    'rol-movimientocajaoperaciones'
                );
                array_push($privilegios, $movcaja);
            }
            if(isset($_POST['rol-trasladooperaciones'])){
                $traslado = array(
                    $id,
                    'Traslado de articulos',
                    'rol-trasladooperaciones'
                );
                array_push($privilegios, $traslado);
            }
            
//            var_dump($privilegios);
            if(count($privilegios) >0 && isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['txtdescripcion']) && !empty($_POST['txtdescripcion'])){
                $delete = $niveldet->delete($id);
//                echo $delete.' delete';
                $det =$niveldet->insert($privilegios);
                if($id>0 && count($det)>0 ){
                    ?> 
                  <script>

                       swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                      

                  </script>  <?php
                }else{
                     ?> 
            <script>
               
                 swal('No se realizo registro', 'Algo sucedio mal :(', 'error');
            </script>  <?php
                    
                }
                
            }           
                else {
            
             ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese Permisos', 'error');
            </script>  <?php
        }
            }
            

    }
    
    
    function cargar(){
       
    if(isset($_GET['id']) && !empty($_GET['id']) && is_numeric($_GET['id'])){
        
        $nivel = new detalleNivel();
        $n = new nivel();
        $id= (int)$_GET['id'];
        $permisos = $nivel->selectAll($id);
        $nivel=''; $bancoeditar=''; $bancoeliminar = ''; $bancolistar =''; $banconuevo='';
        $cuentabancaeditar=''; $cuentabancaeliminar=''; $cuentabancalistar='';
        $cuentabancanuevo=''; $clienteeditar=''; $clienteeliminar='';
        $clientenuevo='';$clientelistar='';$clientedefecto = '';$proveedoreditar ='';
        $proveedoreliminar='';$proveedorlistar=''; $proveedornuevo='';$proveedordefecto='';$marcaeditar='';
        $marcaeliminar='';$marcalistar='';$marcanuevo='';$unideditar ='';$unideliminar='';$unidlistar='';
        $unidnuevo='';$categoriaeditar='';$categoriaeliminar='';$categorialistar='';$categorianuevo='';
        $lineaeditar='';$lineaeliminar='';$linealistar='';$lineanuevo='';$articuloeditar='';$articuloeliminar='';
        $articulolistar='';$articulonuevo ='';$servicioeditar='';$servicioeliminar='';$serviciolistar='';$servicionuevo='';
        $usuarioeditar='';$usuarioeliminar='';$usuariolistar='';$usuarionuevo='';$sucursalprincipalmostrar='';
        $sucursaleditar='';$sucursaleliminar='';$sucursallistar='';$sucursalnuevo='';$documentoeditar='';
        $documentoeliminar='';$documentolistar='';$documentonuevo='';$vercomprobanteventa='';$vercomprobanteventaporsucur='';
        $boletafacturaventa='';$notacreditoventa='';$notadebitoventa='';$cotizacionventa='';$vercotizacionesventa='';
        $vercotizacionesventasucur='';$vercomprobantecompra='';$vercomprobantecomprasucur='';$vercomprobanteordencomprasucur='';

        $compra='';$vercomprobanteordencompra='';$ordencompra='';$ventadetallereporte='';
        $ventadetallereportesucur='';$compradetallereporte='';$compradetallereportesucur='';
        $generalreporte='';$generalreportesucur='';$valorizadoreporte='';$valorizadoreportesucur='';
        $compraventareporte='';$compraventareportesucur='';$kardexreporte='';$kardexreportesucur='';$cuentascobrarreporte='';$cuentascobrarreportesucur='';
        $cuentaspagarreporte='';$cuentaspagarreportesucur='';$gastosoperaciones='';$gastosoperacionessucur='';
        $movimientocajaoperaciones='';$trasladooperaciones='';
        
        $niveltodo = '';$bancostodo=''; $cuentabanctodo = ''; $clientetodo =''; $proveedortodo ='';
        $marcatodo = ''; $unidmedidatodo = ''; $categoriatodo = ''; $lineatodo =''; $articulotodo = '';
        $serviciotodo = ''; $usuariotodo = '';$sucursalestodo = ''; $documentotodo =''; $ventatodo= '';
        $compratodo = ''; $reportetodo =''; $operaciontodo = ''; $panel = ''; $stocktodo = '';$editarpreciodocumento = '';
        
        $ni = $n->selectone($id);
        
        foreach($permisos as $permiso){
             if ($permiso->getOp()=='rol-editarprecio'){
                $editarpreciodocumento = 'checked';
            }
            
            if ($permiso->getOp()=='rol-stockreporte'){
                $stocktodo = 'checked';
            }
            
            if ($permiso->getOp()=='rol-privilegiosall'){
                $niveltodo = 'checked';
            }
            if ($permiso->getOp()=='rol-bancoall'){
                $bancostodo = 'checked';
            }
            if ($permiso->getOp()=='rol-cuentabancaall'){
                $cuentabanctodo = 'checked';
            }
            if ($permiso->getOp()=='rol-clienteall'){
                $clientetodo = 'checked';
            }
            if ($permiso->getOp()=='rol-proveedorall'){
                $proveedortodo = 'checked';
            }
            if ($permiso->getOp()=='rol-marcaall'){
                $marcatodo = 'checked';
            }
            if ($permiso->getOp()=='rol-unidall'){
                $unidmedidatodo = 'checked';
            }
            if ($permiso->getOp()=='rol-categoriaall'){
                $categoriatodo = 'checked';
            }
            if ($permiso->getOp()=='rol-lineaall'){
                $lineatodo = 'checked';
            }
            if ($permiso->getOp()=='rol-articuloall'){
                $articulotodo = 'checked';
            }
            if ($permiso->getOp()=='rol-servicioall'){
                $serviciotodo = 'checked';
            }
            if ($permiso->getOp()=='rol-usuarioall'){
                $usuariotodo = 'checked';
            }
            if ($permiso->getOp()=='rol-sucursalall'){
                $sucursalestodo = 'checked';
            }
            if ($permiso->getOp()=='rol-documentoall'){
                $documentotodo = 'checked';
            }
            if ($permiso->getOp()=='rol-ventaall'){
                $ventatodo = 'checked';
            }
            if ($permiso->getOp()=='rol-compraall'){
                $compratodo = 'checked';
            }
            if ($permiso->getOp()=='rol-reportesall'){
                $reportetodo = 'checked';
            }
            if ($permiso->getOp()=='rol-operacionesall'){
                $operaciontodo = 'checked';
            }
            if ($permiso->getOp()=='rol-panel'){
                $panel = 'checked';
            }
                  
            
            
            
            
            
            
            
            
            /////////////////////////////////////////////
            if ($permiso->getOp()=='rol-permisos'){
                $nivel = 'checked';
            }
            if ($permiso->getOp()=='rol-bancoeditar'){
                $bancoeditar = 'checked';
            }
            if ($permiso->getOp()=='rol-bancoeliminar'){
                $bancoeliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-bancolistar'){
                $bancolistar = 'checked';
            }
            if ($permiso->getOp()=='rol-banconuevo'){
                $banconuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-cuentabancaeditar'){
                $cuentabancaeditar = 'checked';
            }
            if ($permiso->getOp()=='rol-cuentabancaeliminar'){
                $cuentabancaeliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-cuentabancalistar'){
                $cuentabancalistar = 'checked';
            }
            if ($permiso->getOp()=='rol-cuentabancanuevo'){
                $cuentabancanuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-clienteeditar'){
                $clienteeditar = 'checked';
            }
            if ($permiso->getOp()=='rol-clienteeliminar'){
                $clienteeliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-clientelistar'){
                $clientelistar = 'checked';
            }
            if ($permiso->getOp()=='rol-clientenuevo'){
                $clientenuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-clientedefecto'){
                $clientedefecto = 'checked';
            }
            if ($permiso->getOp()=='rol-proveedoreditar'){
                $proveedoreditar = 'checked';
            }
            if ($permiso->getOp()=='rol-proveedoreliminar'){
                $proveedoreliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-proveedorlistar'){
                $proveedorlistar = 'checked';
            }
            if ($permiso->getOp()=='rol-proveedornuevo'){
                $proveedornuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-proveedordefecto'){
                $proveedordefecto = 'checked';
            }
            if ($permiso->getOp()=='rol-marcaeditar'){
                $marcaeditar = 'checked';
            }
            if ($permiso->getOp()=='rol-marcaeliminar'){
                $marcaeliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-marcalistar'){
                $marcalistar = 'checked';
            }
            if ($permiso->getOp()=='rol-marcanuevo'){
                $marcanuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-unideditar'){
                $unideditar = 'checked';
            }
            if ($permiso->getOp()=='rol-unideliminar'){
                $unideliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-unidlistar'){
                $unidlistar = 'checked';
            }
            if ($permiso->getOp()=='rol-unidnuevo'){
                $unidnuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-categoriaeditar'){
                $categoriaeditar = 'checked';
            }
            if ($permiso->getOp()=='rol-categoriaeliminar'){
                $categoriaeliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-categorialistar'){
                $categorialistar = 'checked';
            }
            if ($permiso->getOp()=='rol-categorianuevo'){
                $categorianuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-lineaeditar'){
                $lineaeditar = 'checked';
            }
            if ($permiso->getOp()=='rol-lineaeliminar'){
                $lineaeliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-linealistar'){
                $linealistar = 'checked';
            }
            if ($permiso->getOp()=='rol-lineanuevo'){
                $lineanuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-articuloeditar'){
                $articuloeditar = 'checked';
            }
            if ($permiso->getOp()=='rol-articuloeliminar'){
                $articuloeliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-articulolistar'){
                $articulolistar = 'checked';
            }
            if ($permiso->getOp()=='rol-articulonuevo'){
                $articulonuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-servicioeditar'){
                $servicioeditar = 'checked';
            }
            if ($permiso->getOp()=='rol-servicioeliminar'){
                $servicioeliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-serviciolistar'){
                $serviciolistar = 'checked';
            }
            if ($permiso->getOp()=='rol-serviciolistar'){
                $serviciolistar = 'checked';
            }
            if ($permiso->getOp()=='rol-servicionuevo'){
                $servicionuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-usuarioeditar'){
                $usuarioeditar = 'checked';
            }
            if ($permiso->getOp()=='rol-usuarioeliminar'){
                $usuarioeliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-usuarionuevo'){
                $usuarionuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-usuariolistar'){
                $usuariolistar = 'checked';
            }
            if ($permiso->getOp()=='rol-sucursalprincipalmostrar'){
                $sucursalprincipalmostrar = 'checked';
            }
            if ($permiso->getOp()=='rol-sucursaleditar'){
                $sucursaleditar = 'checked';
            }
            if ($permiso->getOp()=='rol-sucursaleliminar'){
                $sucursaleliminar = 'checked';
            }
            if ($permiso->getOp()=='rol-sucursallistar'){
                $sucursallistar = 'checked';
            }
            if ($permiso->getOp()=='rol-sucursalnuevo'){
                $sucursalnuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-documentoeditar'){
                $documentoeditar = 'checked';
            }
            if ($permiso->getOp()=='rol-documentoeliminar'){
                $documentoeliminar= 'checked';
            }
            if ($permiso->getOp()=='rol-documentolistar'){
                $documentolistar = 'checked';
            }
            if ($permiso->getOp()=='rol-documentonuevo'){
                $documentonuevo = 'checked';
            }
            if ($permiso->getOp()=='rol-vercomprobanteventa'){
                $vercomprobanteventa = 'checked';
            }
            if ($permiso->getOp()=='rol-vercomprobanteventaporsucur'){
                $vercomprobanteventaporsucur = 'checked';
            }
            if ($permiso->getOp()=='rol-boletafacturaventa'){
                $boletafacturaventa = 'checked';
            }
            if ($permiso->getOp()=='rol-notacreditoventa'){
                $notacreditoventa = 'checked';
            }
            if ($permiso->getOp()=='rol-notadebitoventa'){
                $notadebitoventa = 'checked';
            }
            if ($permiso->getOp()=='rol-cotizacionventa'){
                $cotizacionventa = 'checked';
            }
            if ($permiso->getOp()=='rol-vercotizacionesventa'){
                $vercotizacionesventa = 'checked';
            }
            if ($permiso->getOp()=='rol-vercotizacionesventasucur'){
                $vercotizacionesventasucur = 'checked';
            }
            if ($permiso->getOp()=='rol-vercomprobantecompra'){
                $vercomprobantecompra = 'checked';
            }
            if ($permiso->getOp()=='rol-vercomprobantecomprasucur'){
                $vercomprobantecomprasucur = 'checked';
            }
            if ($permiso->getOp()=='rol-compra'){
                $compra = 'checked';
            }
            if ($permiso->getOp()=='rol-vercomprobanteordencompra'){
                $vercomprobanteordencompra = 'checked';
            }
            if ($permiso->getOp()=='rol-vercomprobanteordencomprasucur'){
                $vercomprobanteordencomprasucur = 'checked';
            }
            if ($permiso->getOp()=='rol-ordencompra'){
                $ordencompra = 'checked';
            }
            if ($permiso->getOp()=='rol-ventadetallereporte'){
                $ventadetallereporte = 'checked';
            }
            if ($permiso->getOp()=='rol-ventadetallereportesucur'){
                $ventadetallereportesucur = 'checked';
            }
            
            
            if ($permiso->getOp()=='rol-compradetallereporte'){
                $compradetallereporte = 'checked';
            }
            if ($permiso->getOp()=='rol-compradetallereportesucur'){
                $compradetallereportesucur = 'checked';
            }
            if ($permiso->getOp()=='rol-generalreporte'){
                $generalreporte = 'checked';
            }
            if ($permiso->getOp()=='rol-generalreportesucur'){
                $generalreportesucur = 'checked';
            }
            if ($permiso->getOp()=='rol-valorizadoreporte'){
                $valorizadoreporte = 'checked';
            }
            if ($permiso->getOp()=='rol-valorizadoreportesucur'){
                $valorizadoreportesucur = 'checked';
            }
            if ($permiso->getOp()=='rol-compraventareporte'){
                $compraventareporte = 'checked';
            }
            if ($permiso->getOp()=='rol-compraventareportesucur'){
                $compraventareportesucur = 'checked';
            }
            
            
            if ($permiso->getOp()=='rol-kardexreporte'){
                $kardexreporte = 'checked';
            }
            if ($permiso->getOp()=='rol-kardexreportesucur'){
                $kardexreportesucur = 'checked';
            }
            if ($permiso->getOp()=='rol-cuentascobrarreporte'){
                $cuentascobrarreporte = 'checked';
            }
            if ($permiso->getOp()=='rol-cuentascobrarreportesucur'){
                $cuentascobrarreportesucur = 'checked';
            }
            if ($permiso->getOp()=='rol-cuentaspagarreporte'){
                $cuentaspagarreporte = 'checked';
            }
            if ($permiso->getOp()=='rol-cuentaspagarreportesucur'){
                $cuentaspagarreportesucur = 'checked';
            }
            if ($permiso->getOp()=='rol-gastosoperaciones'){
                $gastosoperaciones = 'checked';
            }
            
            if ($permiso->getOp()=='rol-gastosoperacionessucur'){
                $gastosoperacionessucur = 'checked';
            }
            if ($permiso->getOp()=='rol-movimientocajaoperaciones'){
                $movimientocajaoperaciones = 'checked';
            }
            if ($permiso->getOp()=='rol-trasladooperaciones'){
                $trasladooperaciones = 'checked';
            }
            
            
        }
        require_once 'view/layout/header.php';
        require_once 'view/nivel/form_nivel_edit.php';
        require_once 'view/layout/footer.php';
     
    }else {
           ?> 
            <script>
               
                 swal('Error', 'Algo sucedio mal :(', 'error');
            </script>  <?php
        
    }
        
        
        
        
        
        
    }
    
    function empresa(){
        require_once 'view/layout/header.php';
        if(isset($_GET['id']) && !empty($_GET['id']) && permisos::rol('rol-panel')){
            
            $id = $_GET['id'];
            $nivel = new nivel();
            $empresam = new Empresa();
            
            $empresa = $empresam->selectOne($id);
            
            $permisos = $nivel->selectpermisoporempresa($id);
            
            $titulo = 'Permisos del nivel ('.$empresa->getRazonsocial().')';
            
            require_once 'view/nivel/listar_nivel_empresa.php';
        }else {
            require_once 'view/sinpermiso.php';
            
            
        }
        
        require_once 'view/layout/footer.php';
        
        
    }
    
    function validapermiso(){
       
        if(isset($_POST['rol']) && !empty($_POST['rol'])){
            
            $rol = $_POST['rol'];
            
            if( permisos::rol($rol)){
                echo 1;
            }else {
                echo 0;
            }

        }
        
        
    }


//    function permisoempresa(){
//        
//        require_once 'view/layout/header.php';
//        
//        if(isset($_GET['id']) && !empty($_GET['id']) && permisos::rol('rol-panel')){
//            $id= $_GET['id'];
//            
//            $empresam = new Empresa();
//            $empresa = $empresam->selectOne($id);
//            
//            $titulo = 'NUEVO NIVEL/PERMISO ('.strtoupper($empresa->getRazonsocial()).")";
//            
//            
//        }else {
//            require_once 'view/sinpermiso.php';
//            
//        }
//        
//        require_once 'view/layout/footer.php';
//        
//    }

}
