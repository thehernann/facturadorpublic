<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sucursalController
 *
 * @author HERNAN
 */
require_once 'model/sucursal.php';
require_once 'model/almacen.php';
require_once 'model/empresa.php';


class sucursalController {

    //put your code here
    private $sucursal;

    function __construct() {
        $this->sucursal = new sucursal();
    }

    function select() {
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-sucursallistar') ){
            $sucursales = $this->sucursal->selectAll();
        
        require_once 'view/sucursal/listar_sucursal.php';
            
        }else {
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
    }

    function cargar() {
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-sucursaleditar')){
        $titulo = '<div class="demo-google-material-icon"> <i class="material-icons">business</i> <span class="icon-name"> EDITAR SUCURSAL </span> </div>';
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $sucursal = $this->sucursal->selectOne($id);

//            var_dump($sucursal);
            
            require_once 'view/sucursal/form_sucursal.php';
        }else{
            require_once 'view/error.php';
        }
        
        
            
        }else {
             require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
    }

    function crear() {
        
        require_once 'view/layout/header.php';
        if(permisos::rol('rol-sucursalnuevo') ){
        $empresa = '';
        
        if(isset($_GET['empresa'])){
            $empresam = new Empresa();
            
            $e = $empresam->selectOne($_GET['empresa']);
            
           $empresa=' ('. strtoupper($e->getRazonsocial()).')';
        }
        
        
        
       
    
        
        
        $titulo = '<div class="demo-google-material-icon"> <i class="material-icons">business</i> <span class="icon-name"> NUEVA SUCURSAL'.$empresa.' </span> </div>';

        $sucursal = new sucursal();

//            var_dump($sucursal);
        
        require_once 'view/sucursal/form_sucursal.php';
        
        }else {
            require_once 'view/sinpermiso.php';
        }
        require_once 'view/layout/footer.php';
    }

    function update() {
//        var_dump($_POST);
        if (isset($_POST['txtnombre']) && isset($_POST['txtempresa']) && isset($_POST['txtruc']) &&
                isset($_POST['txtcod']) && isset($_POST['txtslogan']) && isset($_POST['txtdireccion']) &&
                isset($_POST['txttelf'])  && isset($_POST['txtweb']) && isset($_POST['txtemail']) && 
                isset($_POST['txtresponsable']) && !empty($_POST['txtnombre']) && !empty($_POST['txtempresa']) && !empty($_POST['txtruc'])
                && isset($_POST['id']) && !empty($_POST['id'] && is_numeric($_POST['id']))
                ) {  //&& isset($_POST['txttelf1']) && isset($_POST['txttelf2']) && isset($_POST['txttelf3'])

            $ruc = str_replace(" ", "", trim($_POST['txtruc']));
            $id = (int)trim($_POST['id']);
            $fila = 0;
//            if($this->sucursal->duplicadoedit($ruc, $id,$_SESSION['idempresa']) == 0){
                    
            $sucursal = new sucursal();
            $sucursal->setId($_POST['id']);
            $sucursal->setCodigo($_POST['txtcod']);
            $sucursal->setNombre($_POST['txtnombre']);
            $sucursal->setEmpresa($_POST['txtempresa']);
            $sucursal->setRuc($ruc);
            $sucursal->setSlogan($_POST['txtslogan']);
//            $sucursal->setSerieimpresora($_POST['txtimpresora']);
//            $sucursal->setDpto($_POST['txtdpto']);
//            $sucursal->setProvincia($_POST['txtprovincia']);
//            $sucursal->setDistrito($_POST['txtdistrito']);
            $sucursal->setDireccion($_POST['txtdireccion']);
            $sucursal->setTelf($_POST['txttelf']);
//            $sucursal->setTelf1($_POST['txttelf1']);
//            $sucursal->setTelf2($_POST['txttelf2']);
//            $sucursal->setTelf3($_POST['txttelf3']);
            $sucursal->setWeb($_POST['txtweb']);
            $sucursal->setEmail($_POST['txtemail']);
            $sucursal->setResponsable($_POST['txtresponsable']);
            $sucursal->setIdempresa($_SESSION['idempresa']);
//            $sucursal->setMapaid($_POST['txtmapaid']);

            $imgpie = '';
            if (!empty($_FILES['imglocal']['name']) && !empty($_FILES['txtimgtop']['name']) && !empty($_FILES['imgpie']['name'])) {

                $imglocal = $_FILES['imglocal'];
                $imglocal64 = base64_encode(file_get_contents($imglocal["tmp_name"]));

                $imglogo = $_FILES['imgtop'];
                $imglogo64 = base64_encode(file_get_contents($imglogo["tmp_name"]));

                $imgpie = $_FILES['imgpie'];
                $imgpie64 = base64_encode(file_get_contents($imgpie["tmp_name"]));

                $sucursal->setImglocal($imglocal64);
                $sucursal->setImgtoplogo($imglogo64);
                $sucursal->setImgpie($imgpie64);

                $fila= $this->sucursal->update($sucursal);
            } elseif (!empty($_FILES['imglocal']['name'])) {
                $imglocal = $_FILES['imglocal'];
                $imglocal64 = base64_encode(file_get_contents($imglocal["tmp_name"]));

                $sucursal->setImglocal($imglocal64);
                $fila = $this->sucursal->updateimglocal($sucursal);
            } elseif (!empty($_FILES['imgtop']['name'])) {

                $imglogo = $_FILES['imgtop'];
                $imglogo64 = base64_encode(file_get_contents($imglogo["tmp_name"]));
                $sucursal->setImgtoplogo($imglogo64);
                $fila= $this->sucursal->updateimgtoplogo($sucursal);
            } elseif (!empty($_FILES['imgpie']['name'])) {
                $imgpie = $_FILES['imgpie'];
                $imgpie64 = base64_encode(file_get_contents($imgpie["tmp_name"]));
                $sucursal->setImgpie($imgpie64);

                $fila = $this->sucursal->updateimgpie($sucursal);
            } else {

                $fila = $this->sucursal->updatenoimg($sucursal);
            }
            
            if($fila >0){
                 ?> 
                <script>

                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                     
                </script>  <?php
                    
                
            }else {
                 ?> 
                <script>

                     swal('Error', 'No se realizarón cambios.', 'error');
                   
                </script>  <?php
            }
           

        }
    }

    function insert() {

//        var_dump($_POST);
        if (isset($_POST['txtnombre']) && isset($_POST['txtempresa']) && isset($_POST['txtruc']) &&
                isset($_POST['txtcod']) && isset($_POST['txtslogan']) && isset($_POST['txtdireccion']) &&
                isset($_POST['txttelf'])  && isset($_POST['txtweb']) && isset($_POST['txtemail']) && 
                isset($_POST['txtresponsable']) && !empty($_POST['txtnombre']) && !empty($_POST['txtempresa']) && !empty($_POST['txtruc'])) {
            //&& isset($_POST['txttelf1']) && isset($_POST['txttelf2']) && isset($_POST['txttelf3'])
            $ruc = str_replace(" ", "", trim($_POST['txtruc']));
            
//            if($this->sucursal->duplicado($ruc,$_SESSION['idempresa']) == 0){
                
                 if(isset($_POST['idempresa']) && !empty($_POST['idempresa'])){
                     
                     if(permisos::rol('rol-panel')){
                         $idempresa = $_POST['idempresa'];
                     }else {
                         ?>
                        <script>
                            swal('Acceso denegado, necesita permisos para realizar esta acción','No se realizarón cambios','error');
                            
                        </script>
                            
                         <?php
                              die();
                     }
                    

                    }else {
                        $idempresa = $_SESSION['idempresa'];

                    }
                
                $almacen = new Almacen();
                
                $almacen->setDireccion($_POST['txtdireccion']);
                $almacen->setNombre($_POST['txtnombre']);
                $almacen->setIdempresa($idempresa);
                
                $idalmacen=$almacen->insert($almacen);
                
                
   
            
                $sucursal = new sucursal();
    //            $sucursal->setId($_POST['id']);
                $sucursal->setCodigo($_POST['txtcod']);
                $sucursal->setNombre($_POST['txtnombre']);
                $sucursal->setEmpresa($_POST['txtempresa']);
                $sucursal->setRuc($_POST['txtruc']);
                $sucursal->setSlogan($_POST['txtslogan']);
    //            $sucursal->setSerieimpresora($_POST['txtimpresora']);
    //            $sucursal->setDpto($_POST['txtdpto']);
    //            $sucursal->setProvincia($_POST['txtprovincia']);
    //            $sucursal->setDistrito($_POST['txtdistrito']);
                $sucursal->setDireccion($_POST['txtdireccion']);
                $sucursal->setTelf($_POST['txttelf']);
    //            $sucursal->setTelf1($_POST['txttelf1']);
    //            $sucursal->setTelf2($_POST['txttelf2']);
    //            $sucursal->setTelf3($_POST['txttelf3']);
                $sucursal->setWeb($_POST['txtweb']);
                $sucursal->setEmail($_POST['txtemail']);
                $sucursal->setResponsable($_POST['txtresponsable']);
//            $sucursal->setMapaid($_POST['txtmapaid']);
                $sucursal->setIdalmacen($idalmacen);
                $sucursal->setIdempresa($idempresa);

            if (!empty($_FILES['imglocal']['name'])) {

                $imglocal = $_FILES['imglocal'];
                $imglocal64 = base64_encode(file_get_contents($imglocal["tmp_name"]));
            } else {
                $imglocal64 = '';
            }
            if (!empty($_FILES['imgtop']['name'])) {
                $imglogo = $_FILES['imgtop'];
                $imglogo64 = base64_encode(file_get_contents($imglogo["tmp_name"]));
            } else {

                $imglogo64 = '';
            }

            if (!empty($_FILES['imgpie']['name'])) {
                $imgpie = $_FILES['imgpie'];
                $imgpie64 = base64_encode(file_get_contents($imgpie["tmp_name"]));
            } else {

                $imgpie64 = '';
            }



            $sucursal->setImglocal($imglocal64);
            $sucursal->setImgtoplogo($imglogo64);
            $sucursal->setImgpie($imgpie64);

            $fila= $this->sucursal->insert($sucursal);
            
            if($fila >0){
                 ?> 
                <script>

                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                     $('#FormularioAjax').trigger("reset");
                </script>  <?php
                    
                
            }else {
                 ?> 
                <script>

                     swal('Error', 'No se realizarón cambios.', 'error');
                   
                </script>  <?php
            }
            
            
            
            

            
            
        }else {
            
              ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
        }
   
        
    }

//    function main() {
//        
//        require_once 'view/layout/header.php';
//        
//        if(permisos::rol('rol-sucursalprincipalmostrar')){
//           $titulo = '<div class="demo-google-material-icon"> <i class="material-icons">business</i> <span class="icon-name"> SUCURSAL PRINCIPAL</span> </div>';
//
//            $sucursal = $this->sucursal->selectMain();
//
//    //            var_dump($sucursal);
//
//            require_once 'view/sucursal/form_sucursal.php'; 
//            
//        }else {
//            require_once 'view/sinpermiso.php';
//        }
//        
//        require_once 'view/layout/footer.php';
//    }
    
    function delete() {
        
        if(permisos::rol('rol-sucursaleliminar')){
            if(isset($_POST['id']) && !empty($_POST['id'])){
//            var_dump($_POST);
            $id = $_POST['id'];
            $fila =$this->sucursal->delete($id,$_SESSION['idempresa']);
            
            
//            echo 'fila '.$fila;
                    if($fila!=0 ){
                        echo '<script>swal("Éxitosamente!", "Operación realizada correctamente.", "success");</script>';
                        echo "<META HTTP-EQUIV='Refresh' CONTENT='0; URL=".base_url."sucursal/select'>";
//                        header("Location:".base_url."producto/selectserv");
   
                    }else {
                        
                        echo '<script>swal("No se realizarón cambios!", "Algo sucedio mal :(", "error");</script>';
                    }
            
            
            }
            
            
        }else{
            echo '<script>swal("Acceso denegado", "Necesita permisos para realizar esta acción", "error");</script>';
            
        }
        
        
        
    }
    
    function  selectbyid(){
//        var_dump($_POST);
        
        if(isset($_POST['id']) && !empty($_POST['id'])){
            
            $id = $_POST['id'];
            
            
            $sucursalm = new sucursal();
            
            $sucursal = $sucursalm->selectOne($id);
            
//            var_dump($sucursal);
            
            $res = array(
                "razonsocial"=>$sucursal->getEmpresa(),
                "ruc"=>$sucursal->getRuc(),
                "direccion"=>$sucursal->getDireccion()
                
                
            );
            
            echo json_encode($res);
  
        }
        
    }
    
    
    function empresa(){
        require_once 'view/layout/header.php';
        
        
        if(isset($_GET['id']) && !empty($_GET['id'])){
            
            $id = $_GET['id'];
            
            $empresam = new Empresa();
            
            $empresa = $empresam->selectOne($id);
            
            $titulo = 'LISTA DE SUCURSALES ('.strtoupper($empresa->getRazonsocial()).')';
            
            
            $sucursal = new sucursal();
            
           $sucursales = $sucursal->selectbyempresa($id);
           
           require_once 'view/sucursal/listar_sucursal_empresa.php';
            
            
            
        }else {
            
            require_once 'view/error.php';
        }        
        
        require_once 'view/layout/footer.php';
        
    }
    
    
    
    

}
