<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of productoController
 *
 * @author HERNAN
 */
require_once 'model/producto.php';
require_once 'model/linea.php';
require_once 'model/categoriaprod.php';
require_once 'model/marcaprod.php';
require_once 'model/unidmedida.php';
require_once 'model/tipoImpuesto.php';
require_once 'model/serieProducto.php';
require_once 'model/sucursal.php';
require_once 'model/precioProducto.php';
require_once 'model/carateristicas.php';
require_once 'model/almacen.php';
require_once 'model/kardex.php';

class productoController {
    //put your code here
    private $producto;
    private $linea;
    private $categoria;
    private $marca;
    private $medida;
    private $impuesto;
    function __construct() {
        $this->producto = new producto();
        $this->linea= new linea();
        $this->categoria= new categoriaprod();
        $this->marca= new marcaprod();
        $this->medida = new unidmedida();
        $this->impuesto = new tipoImpuesto();
    }
    
    function selectprod(){
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-articulolistar')){
            $lineas = $this->linea->selectAll();
            $categorias=$this->categoria->selectAll();
            $marcas = $this->marca->selectAll();
            $tipo = 'producto';
            $productos = $this->producto->select($tipo);
            require_once 'view/producto/listar_producto.php';
        }else {
            require_once 'view/sinpermiso.php';
        }
        
   
        
        
        require_once 'view/layout/footer.php';
    }
    
    function selectserv(){
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-serviciolistar')){
            $tipo = 'servicio';
            $productos = $this->producto->select($tipo);  
            
//            var_dump($productos);
            require_once 'view/producto/listar_servicio.php';
            
        }else {
            require_once 'view/sinpemiso.php';
        }
        
        
        
        require_once 'view/layout/footer.php';
    }
    
    function cargarprod(){
        require_once 'view/layout/header.php';    
        
        if(permisos::rol('rol-articuloeditar')){
        if(isset($_GET['id']) && !empty($_GET['id']) && is_numeric($_GET['id'])){
            
            $producto = $this->producto->selectone($_GET['id']);
            $lineas = $this->linea->selectAll();
            $categorias=$this->categoria->selectAll();
            $marcas = $this->marca->selectAll();
            $medidas = $this->medida->selectAll();
            $impuestos=$this->impuesto->selectAll();
            
            $seriesprod = new serieProducto();
            $preciosm = new precioProducto();
            $caract = new carateristicas();
            $kardex = new kardex();
                    
            
            $preciosmultiples = $preciosm->selectAll($_GET['id']);
            $caractmultiples = $caract->selectAll($_GET['id']);
            
            
            
            $ultimokardex = $kardex->ultimomovimientoproductoalmacen($_GET['id'], $_SESSION['idalmacen']);
//            var_dump($caractmultiples);
                    
            $series = $seriesprod->select($ultimokardex->getId());
            
            
//            var_dump($series);
            
            require_once 'view/producto/form_producto_edit.php';  
            require_once 'view/marca/modalnewmarca.php'; 
            require_once 'view/linea/modalnewlinea.php'; 
            require_once 'view/categoria/modalnewcategoria.php'; 
            require_once 'view/unidmedida/modalnewunidmedida.php';
            
            require_once 'view/producto/modalprecios.php';
            require_once 'view/producto/modal/modalcaracteristicas.php';
            require_once 'view/producto/modal/modalajustestock.php';
            
            
        }else {
            
            require_once 'view/error.php';
           
        }
    }else {
        require_once 'view/sinpermiso.php';
        
    }
        require_once 'view/layout/footer.php';
       
    }
    function cargarserv(){
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-servicioeditar')){
             if(isset($_GET['id'])){
            
            $producto = $this->producto->selectone($_GET['id']);
            $lineas = $this->linea->selectAll();
            $categorias=$this->categoria->selectAll();
            $marcas = $this->marca->selectAll();
            $medidas = $this->medida->selectAll();
            $impuestos=$this->impuesto->selectAll();
           
            require_once 'view/producto/form_servicio.php';  
            require_once 'view/marca/modalnewmarca.php'; 
            require_once 'view/linea/modalnewlinea.php'; 
            require_once 'view/categoria/modalnewcategoria.php'; 
            require_once 'view/unidmedida/modalnewunidmedida.php';
            
        }else {
            
            require_once 'view/error.php'; 
            
        }
            
        }else{
            require_once 'view/sinpermiso.php';
        }
         
        require_once 'view/layout/footer.php';
    }
    function  insert(){
//        var_dump($_POST);
        $id = 0;
        if(isset($_POST['txtdescripcion']) && isset($_POST['txtdescripciondos']) && isset($_POST['txtcodigoalt'])  && isset($_POST['cbmoneda']) && 
                isset($_POST['txtpreciov'])  && isset($_POST['cbimpuesto'])
                && !empty($_POST['txtdescripcion']) && !empty($_POST['cbmedida']) && !empty($_POST['cbmoneda'])
                 && !empty($_POST['txtpreciov']) 
                && !empty($_POST['cbimpuesto']) && isset($_POST['txtcod']) && !empty($_POST['txtcod']) && isset($_POST['txtdetalledos'])){
            
            $cadena = trim($_POST['txtdescripcion']);
            $codigo = trim($_POST['txtcod']);
            
            if($this->producto->duplicado($cadena,$codigo,'producto',$_SESSION['idsucursal']) == 0){
                $precios = true;
                
//                $stock = 0;
//            if(  isset($_POST['txtstock'])  && !empty($_POST['txtstock'])){
//                if(is_numeric($_POST['txtstock'])){
//                    $stock = $_POST['txtstock'];
//                }else {
//                     ?> 
                    <script>

//                         swal('Error', 'Ingrese stock inicial valido', 'error');

                    </script>  
                        <?php
////                    die();
////                }
////                
////               
////            }
            
            $preciomin = NULL;
            if(  isset($_POST['txtpreciomin'])  && !empty($_POST['txtpreciomin'])){
                if(is_numeric($_POST['txtpreciomin'])){
                    $preciomin = $_POST['txtpreciomin'];
                }else {
                     ?> 
                    <script>

                         swal('Error', 'Ingrese precio venta minimo valido', 'error');

                    </script>  <?php
                    die();
                }
            }
            $preciodos = NULL;
            if(  isset($_POST['txtpreciodos'])  && !empty($_POST['txtpreciodos'])){
                if(is_numeric($_POST['txtpreciodos'])){
                    $preciodos = $_POST['txtpreciodos'];
                }else {
                     ?> 
                    <script>

                         swal('Error', 'Ingrese precio venta minimo valido', 'error');

                    </script>  <?php
                    die();
                }
            }
            $preciotres = NULL;
            if(  isset($_POST['txtpreciotres'])  && !empty($_POST['txtpreciotres'])){
                if(is_numeric($_POST['txtpreciotres'])){
                    $preciotres = $_POST['txtpreciotres'];
                }else {
                     ?> 
                    <script>

                         swal('Error', 'Ingrese precio venta minimo valido', 'error');

                    </script>  <?php
                    die();
                }
            }
            
            $preciocompra = NULL;
            if(  isset($_POST['txtprecioc'])  && !empty($_POST['txtprecioc'])){
                if(is_numeric($_POST['txtprecioc'])){
                    $preciocompra = $_POST['txtprecioc'];
                }else {
                     ?> 
                    <script>

                         swal('Error', 'Ingrese precio compra valido', 'error');

                    </script>  <?php
                    die();
                }
            }
                
                if (isset($_POST['cantidadpreciomultiple']) && isset($_POST['preciomultiple'])) {
                    $cantidadm = $_POST['cantidadpreciomultiple'];
                    $preciom = $_POST['preciomultiple'];

                    
                    for ($i = 0; $i < count($preciom); $i++) {
                        if($cantidadm[$i] == '' || $preciom[$i] == '' || !is_numeric($cantidadm[$i]) || !is_numeric($preciom[$i])){
                            $precios = false;
                             ?> 
                            <script>

                                 swal('Error', 'No puede ingresar precio y/o cantidad multiple en blanco.', 'error');
                                
                            </script>  <?php
                            die();
                        }
                }
                }
                if (isset($_POST['caracteristicas'])) {
                    $carat = $_POST['caracteristicas'];
                    for ($k = 0; $k < count($carat); $k++){
                        if($carat[$k] == ''){
                            
                              ?> 
                            <script>

                                 swal('Error', 'No puede ingresar caracteristica en blanco.', 'error');
                                
                            </script>  <?php
                            die();
                        }
                        
                    }
                }
            
            if($precios == true){
            if(empty($_POST['cblinea']) || is_nan($_POST['cblinea'])){
                $linea = 0;
            }else {
                $linea = $_POST['cblinea'];
            }
            if(empty($_POST['cbcategoria']) || is_nan($_POST['cbcategoria'])){
                $categoria = 0;
            }else {
                $categoria = $_POST['cbcategoria'];
            }
            if(empty($_POST['cbmarca']) || is_nan($_POST['cbmarca'])){
                $marca = 0;
            }else {
                $marca = $_POST['cbmarca'];
            }
            if(empty($_POST['txtpeso']) || is_nan($_POST['txtpeso'])){
                $peso = 0;
            }else {
                $peso = $_POST['txtpeso'];
            }
            
            
            
            $producto = new producto();
//            $producto->setTipo($_POST['cbtipo']);
            
            $producto->setCodigoalternativo($_POST['txtcodigoalt']);
            $producto->setDescripciondos($_POST['txtdescripciondos']);
            $producto->setCodigo($_POST['txtcod']);
            $producto->setCodbarra($_POST['txtcodbarra']);
            $producto->setMarca($_POST['cbmarca']);
            $producto->setDescripcion($_POST['txtdescripcion']);
            $producto->setUnidmed($_POST['cbmedida']);
            $producto->setMoneda($_POST['cbmoneda']);
            $producto->setPrecioc($preciocompra);
            $producto->setPreciov($_POST['txtpreciov']);
            $producto->setPreciovmin($preciomin);
//            $producto->setStock($stock);
            $producto->setPeso($peso);
            $producto->setIncluir($_POST['cbincluir']);
//            $producto->setNrocuenta($_POST['txtncuenta']);
            $producto->setObservacion($_POST['txtdetalle']);
            $producto->setIdtipoimpuesto($_POST['cbimpuesto']);
            $producto->setProdservi('producto');
            $producto->setIdcategoria($categoria);
            $producto->setIdlinea($linea);
            $producto->setIdmarca($marca);
            $producto->setIdunidmedida($_POST['cbmedida']);
            
            $producto->setPreciodos($preciodos);
            $producto->setPreciotres($preciotres);
            $producto->setObservaciondos($_POST['txtdetalledos']);
            
            $id= $this->producto->insert($producto);
            
             if (isset($_POST['cantidadpreciomultiple']) && isset($_POST['preciomultiple'])) {
//            var_dump($_POST['nombreotros']);
                    $cantidadm = $_POST['cantidadpreciomultiple'];
                    $preciom = $_POST['preciomultiple'];

                    $precios = array();
                    for ($i = 0; $i < count($preciom); $i++) {
                        $d = array(
                            $id,
                            $preciom[$i],
                            $cantidadm[$i]
                            
                        );
                        array_push($precios, $d);
                    }
                    $preciomodel = new precioProducto();
                    $preciomodel->insert($precios);
            }
            if (isset($_POST['caracteristicas'])) {
                $carat = $_POST['caracteristicas'];
                $caracts = array();
                
                for($j = 0; $j < count($carat); $j++){
                    $caract = array(
                        $id,
                        str_replace(' ', '', $caract[$j]),
                        1,
                        $_SESSION['idempresa']
                    );
                    array_push($caracts, $caract);
                    
                }
                $caractm = new carateristicas();
                $caractm->insert($caracts);
            }
            
            if($id >0){
                $cod = $producto->generarcod();
              
                 ?> 
                <script>
//                    $("#FormularioProducto")[0].reset();
                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                     document.location.reload();
//                     $('#FormularioProducto').trigger("reset");
//                    $('#divprecios').empty();
//                    console.log('<?= $cod ?>');
//                    $('.txtcod').val('<?= $cod ?>');
                 
                    
                     
                </script>  <?php
                              
                               
                
            }else {
                 ?> 
                <script>

                     swal('Error', 'No se realizarón cambios.', 'error');
//                     $('#FormularioAjax').trigger("reset");
                </script>  <?php
            }
            }else {
                /////////////
                ?> 
                <script>

                     swal('Error', 'Ingrese precios validos en "MULTIPLES PRECIOS DE VENTA".', 'error');
                     
                </script>  <?php
            }
            }else {
                 ?> 
                    <script>

                         swal('Error', 'El producto ya se encuentra registrado.', 'error');
                    </script>  <?php
                
            }
            
            
        }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            
            
            
        }
       
    }
    
       function  insertserv(){
        
        $fila = 0;
        if(isset($_POST['txtdescripcion']) && isset($_POST['txtcod']) &&  isset($_POST['cbmoneda']) && isset($_POST['txtprecioc']) && 
                isset($_POST['txtpreciov']) && isset($_POST['txtpreciomin']) && isset($_POST['cbimpuesto']) 
                && isset($_POST['txtstock']) && !empty($_POST['txtdescripcion']) && !empty($_POST['cbmoneda']) 
                && !empty($_POST['txtprecioc']) && !empty($_POST['txtpreciov']) && !empty($_POST['txtpreciomin'])
                && !empty($_POST['cbimpuesto']) && !empty($_POST['txtcod'])){
            
                $codigo = trim($_POST['txtcod']);
                $cadena = trim($_POST['txtdescripcion']);
                
             if($this->producto->duplicado($cadena, $codigo, 'servicio',$_SESSION['idsucursal']) == 0){
                $linea = 0;
                

                if(empty($_POST['cbcategoria']) || is_nan($_POST['cbcategoria'])){
                    $categoria = 0;
                }else {
                    $categoria = $_POST['cbcategoria'];
                }

                    $marca = 0;

                    $peso = 0;
                    
                    $tock = 0;
                    
                    if(!empty($_POST['txtstock'])){
                       $tock= $_POST['txtstock'];
                        
                    }




                $producto = new producto();
    //            $producto->setTipo($_POST['cbtipo']);
                $producto->setCodigo($_POST['txtcod']);
    //            $producto->setCodbarra($_POST['txtcodbarra']);
    //            $producto->setMarca($_POST['cbmarca']);
                $producto->setDescripcion($_POST['txtdescripcion']);
    //            $producto->setUnidmed($_POST['cbmedida']);
                $producto->setMoneda($_POST['cbmoneda']);
                $producto->setPrecioc($_POST['txtprecioc']);
                $producto->setPreciov($_POST['txtpreciov']);
                $producto->setPreciovmin($_POST['txtpreciomin']);
                $producto->setStock($tock);
                $producto->setPeso($peso);
    //            $producto->setIncluir($_POST['cbincluir']);
    //            $producto->setNrocuenta($_POST['txtncuenta']);
                $producto->setObservacion($_POST['txtdetalle']);
                $producto->setIdtipoimpuesto($_POST['cbimpuesto']);
                $producto->setProdservi('servicio');
                $producto->setIdcategoria($categoria);
                $producto->setIdlinea($linea);
                $producto->setIdmarca($marca);
    //            $producto->setIdunidmedida($_POST['cbmedida']);

                $fila = $this->producto->insert($producto);
    
          }else {
                
                ?> 
                    <script>

                         swal('Error', 'El servicio ya se encuentra registrada.', 'error');
                    </script>  <?php
            }
            if($fila >0){
                 ?> 
                <script>

                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                     $('#FormularioProducto').trigger("reset");
                </script>  <?php
                    
                
            }   
        }else{
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php

        }
    
    }
    
      function  update(){
//          var_dump($_POST);
        $fila = 0;
        if(isset($_POST['txtdescripcion']) && isset($_POST['txtdescripciondos']) && isset($_POST['txtcodigoalt']) && isset($_POST['cbmedida']) && isset($_POST['cbmoneda'])  && 
                isset($_POST['txtpreciov'])  
                && !empty($_POST['txtdescripcion']) && !empty($_POST['cbmedida']) && !empty($_POST['cbmoneda'])
                && !empty($_POST['txtpreciov']) 
                && !empty($_POST['cbimpuesto']) && isset($_POST['txtcod']) && isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['txtdetalledos'])){
            
            $cadena = trim($_POST['txtdescripcion']);
            $codigo = trim($_POST['txtcod']);
            $id = $_POST['id'];
            
            
            if($this->producto->duplicadoedit($cadena,$codigo,'producto',$id,$_SESSION['idsucursal']) == 0){
                
                
                
                $stock = 0;
            if(  isset($_POST['txtstock'])  && !empty($_POST['txtstock'])){
                if(is_numeric($_POST['txtstock'])){
                    $stock = $_POST['txtstock'];
                }else {
                     ?> 
                    <script>

                         swal('Error', 'Ingrese stock inicial valido', 'error');

                    </script>  <?php
                    die();
                }
                
               
            }
            
            $preciomin = NULL;
            if(  isset($_POST['txtpreciomin'])  && !empty($_POST['txtpreciomin'])){
                if(is_numeric($_POST['txtpreciomin'])){
                    $preciomin = $_POST['txtpreciomin'];
                }else {
                     ?> 
                    <script>

                         swal('Error', 'Ingrese precio venta minimo valido', 'error');

                    </script>  <?php
                    die();
                }
            }
            
             $preciodos = NULL;
            if(  isset($_POST['txtpreciodos'])  && !empty($_POST['txtpreciodos'])){
                if(is_numeric($_POST['txtpreciodos'])){
                    $preciodos = $_POST['txtpreciodos'];
                }else {
                     ?> 
                    <script>

                         swal('Error', 'Ingrese precio 2 valido', 'error');

                    </script>  <?php
                    die();
                }
            }
            $preciotres = NULL;
            if(  isset($_POST['txtpreciotres'])  && !empty($_POST['txtpreciotres'])){
                if(is_numeric($_POST['txtpreciotres'])){
                    $preciotres = $_POST['txtpreciotres'];
                }else {
                     ?> 
                    <script>

                         swal('Error', 'Ingrese precio 3  valido', 'error');

                    </script>  <?php
                    die();
                }
            }
            
            $preciocompra = NULL;
            if(  isset($_POST['txtprecioc'])  && !empty($_POST['txtprecioc'])){
                if(is_numeric($_POST['txtprecioc'])){
                    $preciocompra = $_POST['txtprecioc'];
                }else {
                     ?> 
                    <script>

                         swal('Error', 'Ingrese precio compra valido', 'error');

                    </script>  <?php
                    die();
                }
            }
                
                
                
                
                
                $precios = true;
                if (isset($_POST['cantidadpreciomultiple']) && isset($_POST['preciomultiple'])) {
                    $cantidadm = $_POST['cantidadpreciomultiple'];
                    $preciom = $_POST['preciomultiple'];

                    
                    for ($i = 0; $i < count($preciom); $i++) {
                        if($cantidadm[$i] == '' || $preciom[$i] == '' || !is_numeric($cantidadm[$i]) || !is_numeric($preciom[$i])){
                            $precios = false;
                             ?> 
                            <script>

                                 swal('Error', 'Ingrese precio y/o cantidad multiple valido.', 'error');
                                
                            </script>  <?php
                            die();
                        }
                }
                }
                
                  if (isset($_POST['caracteristicas'])) {
                    $carat = $_POST['caracteristicas'];
                    for ($k = 0; $k < count($carat); $k++){
                        if($carat[$k] == ''){
                            
                              ?> 
                            <script>

                                 swal('Error', 'No puede ingresar caracteristica en blanco.', 'error');
                                
                            </script>  <?php
                            die();
                        }
                        
                    }
                }
            if($precios == true){    
                $marca = 0;
                $linea = 0;
                $categoria = 0;
                $stock = 0;
                $peso = 0;
                if(!empty($_POST['cbmarca'])){
                    $marca = $_POST['cbmarca'];
                }
                if(!empty($_POST['cblinea'])){
                    $linea = $_POST['cblinea'];
                }
                if(!empty($_POST['cbcategoria'])){
                    $categoria = $_POST['cbcategoria'];
                }
                if(!empty($_POST['txtstock'])){
                    $stock = $_POST['txtstock'];
                }
                if(!empty($_POST['txtpeso'])){
                    $peso = $_POST['txtpeso'];
                }
                
                
            $producto = new producto();
//            $producto->setTipo($_POST['cbtipo']);
            
            $producto->setCodigoalternativo($_POST['txtcodigoalt']);
            $producto->setDescripciondos($_POST['txtdescripciondos']);
            
            $producto->setCodigo($_POST['txtcod']);
            $producto->setCodbarra($_POST['txtcodbarra']);
            
            $producto->setMarca($marca);
            $producto->setDescripcion($_POST['txtdescripcion']);
            $producto->setUnidmed($_POST['cbmedida']);
            $producto->setMoneda($_POST['cbmoneda']);
            $producto->setPrecioc($preciocompra);
            $producto->setPreciov($_POST['txtpreciov']);
            $producto->setPreciovmin($preciomin);
            $producto->setStock($stock);
            $producto->setPeso($peso);
//            $producto->setIncluir($_POST['cbincluir']);
//            $producto->setNrocuenta($_POST['txtncuenta']);
            $producto->setObservacion($_POST['txtdetalle']);
            $producto->setIdtipoimpuesto($_POST['cbimpuesto']);
//            $producto->setProdservi($_POST['txtprod']);
            $producto->setIdcategoria($categoria);
            $producto->setIdlinea($linea);
            $producto->setIdmarca($marca);
            $producto->setIdunidmedida($_POST['cbmedida']);
            $producto->setId($_POST['id']);
            $producto->setPreciodos($preciodos);
            $producto->setPreciotres($preciotres);
            $producto->setObservaciondos($_POST['txtdetalledos']);
            
                    
            $fila = $this->producto->update($producto);
            
//            var_dump($_POST['cantidadpreciomultiple']);
//            var_dump($_POST['preciomultiple']);
            $insertprecios = array();
            if (isset($_POST['cantidadpreciomultiple']) && isset($_POST['preciomultiple'])) {
//            var_dump($_POST['nombreotros']);
                $preciomodel = new precioProducto();
                $preciomodel->delete($id);
                
                $cantidadm = $_POST['cantidadpreciomultiple'];
                $preciom = $_POST['preciomultiple'];

                $precios = array();
                for ($i = 0; $i < count($preciom); $i++) {
                    $d = array(
                        $id,
                        $preciom[$i],
                        $cantidadm[$i]

                    );
                    array_push($precios, $d);
                }

                $insertprecios = $preciomodel->insert($precios);
            }
            $insertcarat = array();
            if (isset($_POST['caracteristicas'])) {
                $caractm = new carateristicas();
                $caractm->delete($id);
                $carat = $_POST['caracteristicas'];
                $caracts = array();
                
                for($j = 0; $j < count($carat); $j++){
                    $caract = array(
                        $id,
                        $carat[$j],
                        1
                    );
                    array_push($caracts, $caract);
                    
                }
                
                $insertcarat = $caractm->insert($caracts);
            }
            
           
                 ?> 
                <script>

                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                     document.location.reload();
                </script>  <?php
                    
      
            }else {
                /// Precios en blanco
                ?> 
                <script>

                     swal('Error', 'Ingrese precios validos en "MULTIPLES PRECIOS DE VENTA".', 'error');
                     
                </script>  <?php
                
            }
            }else {
                 ?> 
                    <script>

                         swal('Error', 'El producto ya se encuentra registrado.', 'error');
                    </script>  <?php
                
            }  
            
            
        }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            
            
            
        }
//        if($fila != 0){
//             echo '<script>swal("Éxitosamente!", "Operación realizada correctamente.", "success");</script>';
//            
//            
//        }else{
//            echo '<script>swal("Error!", "Algo sucedio mal :(.", "error");</script>';
//            
//            
//        }
       
    }
    
    function  updateserv(){
        
        $fila = 0;
        if(isset($_POST['txtdescripcion']) &&  isset($_POST['cbmoneda']) && isset($_POST['txtprecioc']) && 
                isset($_POST['txtpreciov']) && isset($_POST['txtpreciomin']) && isset($_POST['cbimpuesto']) 
                && isset($_POST['txtstock']) && !empty($_POST['txtdescripcion']) && !empty($_POST['cbmoneda']) 
                && !empty($_POST['txtprecioc']) && !empty($_POST['txtpreciov']) && !empty($_POST['txtpreciomin'])
                && !empty($_POST['cbimpuesto']) && isset($_POST['id'])){
            
            $codigo = trim($_POST['txtcod']);
            $cadena = trim($_POST['txtdescripcion']);
            $id = $_POST['id'];   
             if($this->producto->duplicadoedit($cadena, $codigo, 'servicio',$id,$_SESSION['idsucursal']) == 0){
            $stock = 0;
            
            
            
            if(!empty($_POST['txtstock'])){
              $stock=  $_POST['txtstock'];
            }
            $producto = new producto();
//            $producto->setTipo($_POST['cbtipo']);
            $producto->setCodigo($_POST['txtcod']);
//            $producto->setCodbarra($_POST['txtcodbarra']);
//            $producto->setMarca($_POST['cbmarca']);
            $producto->setDescripcion($_POST['txtdescripcion']);
//            $producto->setUnidmed($_POST['cbmedida']);
            $producto->setMoneda($_POST['cbmoneda']);
            $producto->setPrecioc($_POST['txtprecioc']);
            $producto->setPreciov($_POST['txtpreciov']);
            $producto->setPreciovmin($_POST['txtpreciomin']);
            $producto->setStock($stock);
//            $producto->setPeso($_POST['txtpeso']);
//            $producto->setIncluir($_POST['cbincluir']);
//            $producto->setNrocuenta($_POST['txtncuenta']);
            $producto->setObservacion($_POST['txtdetalle']);
            $producto->setIdtipoimpuesto($_POST['cbimpuesto']);
//            $producto->setProdservi($_POST['txtprod']);
            $producto->setIdcategoria($_POST['cbcategoria']);
//            $producto->setIdlinea($_POST['cblinea']);
//            $producto->setIdmarca($_POST['cbmarca']);
//            $producto->setIdunidmedida($_POST['cbmedida']);
            $producto->setId($_POST['id']);
                    
            $fila = $this->producto->update($producto);
           
             if($fila >0){
                 ?> 
                <script>

                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                     $('#FormularioAjax').trigger("reset");
                </script>  <?php
                    
                
            }else {
                 ?> 
                <script>

                     swal('Error', 'No se realizarón cambios.', 'error');
                    
                </script>  <?php
                
            }  
             }else {
                
                ?> 
                    <script>

                         swal('Error', 'El servicio ya se encuentra registrada.', 'error');
                    </script>  <?php
            }
            
            
        }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            
            
            
        }
//        if($fila != 0){
//             echo '<script>swal("Éxitosamente!", "Operación realizada correctamente.", "success");</script>';
//            
//            
//        }else{
//            echo '<script>swal("Error!", "Algo sucedio mal :(.", "error");</script>';
//            
//            
//        }
       
    }



function  search(){
    
   
    if (isset($_POST['query'])){
        $return_arr = array();
        
        foreach ($this->producto->search($_POST['query']) as $producto)  //$_GET['term']  $_POST['query']
        {
        //    $id_cliente=$persona->getId();
            $row_array['value'] = $producto->getDescripcion();
            $row_array['id']=$producto->getId();
            $row_array['codigo']=$producto->getCodigo();
            $row_array['codigobarra']=$producto->getCodbarra();
            $row_array['descripcion']=$producto->getDescripcion();
            $row_array['preciov']=$producto->getPreciov();
            $row_array['preciovmin']=$producto->getPreciovmin();
            $row_array['incluir']=$producto->getIncluir();
            $row_array['moneda'] = $producto->getMoneda();
            $row_array['unidmed'] = $producto->getUnidmed();
            


            array_push($return_arr,$row_array);
        }

        /* Toss back results as json encoded array. */
        echo json_encode($return_arr);
}   
    
}
function  searchcodserie(){
    
   
    if (isset($_POST['query'])){
        
        
        $producto= $this->producto->searchcodserie($_POST['query']); //$_GET['term']  $_POST['query']
        
        //    $id_cliente=$persona->getId();
//            $row_array['value'] = $producto->getDescripcion();
            $return_arr = array(
                0=>$producto->getId(),
                1=>$producto->getCodigo(),
                2=>$producto->getDescripcion(),
                3=>$producto->getPreciov(),
                4=>$producto->getPreciovmin(),
                5=>$producto->getIncluir(),
                6=>$producto->getMoneda(),
                7=>$producto->getUnidmed()
            );


        /* Toss back results as json encoded array. */
        echo json_encode($return_arr);
}   
    
}





  function crearprod(){
        
            require_once 'view/layout/header.php';
            
            if(permisos::rol('rol-articulonuevo')){
                $series = array();
                $producto = new producto();
                $lineas = $this->linea->selectAll();
                $categorias=$this->categoria->selectAll();
                $marcas = $this->marca->selectAll();
                $medidas = $this->medida->selectAll();
                $impuestos=$this->impuesto->selectAll();

                $preciosmultiples = array();
                $caractmultiples = array();
//                var_dump($preciosmultiples);
                $producto->setCodigo($this->producto->generarcod());
                require_once 'view/producto/form_producto.php';  
                require_once 'view/marca/modalnewmarca.php'; 
                require_once 'view/linea/modalnewlinea.php'; 
                require_once 'view/categoria/modalnewcategoria.php'; 
                require_once 'view/unidmedida/modalnewunidmedida.php';
                 
                require_once 'view/producto/modalprecios.php';
                require_once 'view/producto/modal/modalcaracteristicas.php';
                
            } else {
                require_once 'view/sinpermiso.php';
            }
            
            require_once 'view/layout/footer.php';
            
        
        
        
    }
    
     function crearserv(){
        
        
            require_once 'view/layout/header.php';
            if(permisos::rol('rol-servicionuevo')){
            $producto = new producto();
            $lineas = $this->linea->selectAll();
            $categorias=$this->categoria->selectAll();
            $marcas = $this->marca->selectAll();
            $medidas = $this->medida->selectAll();
            $impuestos=$this->impuesto->selectAll();
            $producto->setCodigo($this->producto->generarcod());
            require_once 'view/producto/form_servicio.php';  
            require_once 'view/marca/modalnewmarca.php'; 
            require_once 'view/linea/modalnewlinea.php'; 
            require_once 'view/categoria/modalnewcategoria.php'; 
            require_once 'view/unidmedida/modalnewunidmedida.php';
            
            }else {
                require_once 'view/sinpermiso.php';
            }
            
            require_once 'view/layout/footer.php';
 
        
    }
    
        function deleteserv() {
            
        if(permisos::rol('rol-servicioeliminar')){
                
        if(isset($_POST['id']) && !empty($_POST['id'])){
//            var_dump($_POST);
            $id = $_POST['id'];
            $fila =$this->producto->delete($id);
            
            
//            echo 'fila '.$fila;
                    if($fila!=0 ){
                        echo '<script>swal("Éxitosamente!", "Operación realizada correctamente.", "success");</script>';
                        echo "<META HTTP-EQUIV='Refresh' CONTENT='0; URL=".base_url."producto/selectserv'>";
//                        header("Location:".base_url."producto/selectserv");
   
                    }else {
                        
                        echo '<script>swal("No se realizarón cambios!", "Algo sucedio mal :(", "error");</script>';
                    }
            
            
        }
         }else {
                        
                        echo '<script>swal("Acceso denegado", "Necesita permisos para realizar esta acción", "error");</script>';
                    }
         
        
    }
    
        function deleteprod() {
        
       if(permisos::rol('rol-articuloeliminar')){
        if(isset($_POST['id']) && !empty($_POST['id'])){
//            var_dump($_POST);
            $id = $_POST['id'];
            $fila =$this->producto->delete($id);
            
            
//            echo 'fila '.$fila;
                    if($fila!=0 ){
                        echo '<script>swal("Éxitosamente!", "Operación realizada correctamente.", "success");</script>';
                        echo "<META HTTP-EQUIV='Refresh' CONTENT='0; URL=".base_url."producto/selectprod'>";
//                        header("Location:".base_url."producto/selectprod");
                    }else {
                        
                        echo '<script>swal("No se realizarón cambios!", "Algo sucedio mal :(", "error");</script>';
                    }
            
            
        }
       }else {
                        
                        echo '<script>swal("Acceso denegado", "Necesita permisos para realizar esta acción", "error");</script>';
                    }
        
    }
    
      function selectstockgeneral() {
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-generalreporte') || permisos::rol('rol-generalreportesucur')){
        $almacen = new Almacen();
        
        $sucursales = $almacen->selectAll();
        $marcas = $this->marca->selectAll();
        $categorias = $this->categoria->selectAll();
        $lineas = $this->linea->selectAll();
        $general = $this->producto->selectstockgeneral($_SESSION['idalmacen'], '', '', '', '', '');
//        $detallado = $this->documento->selectdetallado($desde, $hasta, 'Factura','Venta' ,'', '', '','Soles', $_SESSION['idsucursal']);
//        var_dump($detallado);
        require_once 'view/reportes/stock/listargeneral.php';
      }else {
          require_once 'view/sinpermiso.php';
      }
        require_once 'view/layout/footer.php';
    }
    
     function searchstockgeneral() {
//        var_dump($_POST);
         
        if ( isset($_POST['cbmarcastockgeneral']) && isset($_POST['cblineastockgeneral']) && isset($_POST['cbcategoriastockgeneral'])
                && isset($_POST['txtbuscarstockgeneral']) ) { //&& isset($_POST['cbmonedastockgeneral'])

            if(isset($_POST['cbsucursalstockgeneral']) && permisos::rol('rol-generalreportesucur')){
                $idsucursal = $_POST['cbsucursalstockgeneral'];
            }elseif (permisos::rol('rol-generalreporte')) {
                $idsucursal =$_SESSION['idsucursal'];
            }
            
            $marca = $_POST['cbmarcastockgeneral'];
            $linea = $_POST['cblineastockgeneral'];
            $categoria = $_POST['cbcategoriastockgeneral'];
            $buscar = $_POST['txtbuscarstockgeneral'];
//            $moneda = $_POST['cbmonedastockgeneral'];

            $general = $this->producto->selectstockgeneral($idsucursal, $marca, $linea,$categoria, $buscar,'');

            ////// tabla ///// 
            ?>
            <table class="table  table-hover table-bordered" id="tabladocumento">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Codigo barra</th>
                        <th>Categoria</th>
                        <th>Linea</th>

                        <th>Marca</th>
                        <th>Descripción</th>
                        <th>Stock</th>
                         <!--<th>Sucursal</th>-->
                    </tr>
                </thead>
            <!--                                <tfoot>
                    <tr>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Serie</th>
                        <th>Número</th>
                        <th>RUC/ DNI</th>
                        <th>Nombre / Rz. Social</th>
                        <th>Total</th>
                        <th>Est. Local</th>
                        <th>Est. Sunat</th>
                        <th>Imprimir</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>-->
                <tbody >
                    <?php 
                          foreach ($general as $detalle){
                                       
                            echo '<tr>';

                            echo '<td>'.$detalle['codigo'].'</td>';
                            echo '<td>'.$detalle['codigobarra'].'</td>';
                            echo '<td>'.$detalle['categoria'].'</td>';
                            echo '<td>'.$detalle['linea'].'</td>';

                            echo '<td>'.$detalle['marca'].'</td>';
                            echo '<td>'.$detalle['descripcion'].'</td>'; 
                            
                            echo '<td>'.$detalle['stock'].'</td>';
//                            echo '<td>'.$detalle['sucursal'].'</td>';

                            echo '</tr>';



                        } 
                                    

                        ?>
            



                </tbody>

            </table>
          
            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 20
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });

                    
                });
                
    


            </script>    


            <?php
        }
    }
    
    function  stockgeneralexcel() {
        
        if (isset($_GET['cbsucursalstockgeneral']) && isset($_GET['cbmarcastockgeneral']) && isset($_GET['cblineastockgeneral']) && isset($_GET['cbcategoriastockgeneral'])
                && isset($_GET['txtbuscarstockgeneral']) ) { //&& isset($_GET['cbmonedastockgeneral'])

            $idsucursal = $_GET['cbsucursalstockgeneral'];
            $marca = $_GET['cbmarcastockgeneral'];
            $linea = $_GET['cblineastockgeneral'];
            $categoria = $_GET['cbcategoriastockgeneral'];
            $buscar = $_GET['txtbuscarstockgeneral'];
//            $moneda = $_GET['cbmonedastockgeneral'];

            $general = $this->producto->selectstockgeneral($idsucursal, $marca, $linea,$categoria, $buscar,'');
            
            require_once 'view/reportes/functions/excel.php';
//            
//            activeErrorReporting();
//            noCli();

            require_once 'plugins/PHPExcel/Classes/PHPExcel.php';
            require_once 'view/reportes/stock/generalStockExcel.php';
            
        }
        
        
        
    }
      function selectstockvalorizado() {
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-valorizadoreporte') || permisos::rol('rol-valorizadoreportesucur')){
            $almacen = new Almacen();
        
            $sucursales = $almacen->selectAll();
            $marcas = $this->marca->selectAll();
            $categorias = $this->categoria->selectAll();
            $lineas = $this->linea->selectAll();
            $valorizado = $this->producto->selectstockvalorizado($_SESSION['idalmacen'], '', '', '', '', 'Soles');
    //        $detallado = $this->documento->selectdetallado($desde, $hasta, 'Factura','Venta' ,'', '', '','Soles', $_SESSION['idsucursal']);
    //        var_dump($detallado);
            require_once 'view/reportes/stock/listarvalorizado.php';
            
        }else {
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
    }
    
     function searchstockvalorizado() {
//        var_dump($_POST);
         
        if ( isset($_POST['cbmarcastockgeneral']) && isset($_POST['cblineastockgeneral']) && isset($_POST['cbcategoriastockgeneral'])
                && isset($_POST['txtbuscarstockgeneral']) && isset($_POST['cbmonedastockgeneral'])) {
            if(permisos::rol('rol-valorizadoreportesucur') && isset($_POST['cbsucursalstockgeneral'])){
                $idsucursal = $_POST['cbsucursalstockgeneral'];
                
            }elseif (permisos::rol('rol-valorizadoreporte')) {
                $idsucursal = $_SESSION['idsucursal'];
            }

            
            $marca = $_POST['cbmarcastockgeneral'];
            $linea = $_POST['cblineastockgeneral'];
            $categoria = $_POST['cbcategoriastockgeneral'];
            $buscar = $_POST['txtbuscarstockgeneral'];
            $moneda = $_POST['cbmonedastockgeneral'];

            $valorizado = $this->producto->selectstockvalorizado($idsucursal, $marca, $linea,$categoria, $buscar,$moneda);

            ////// tabla ///// 
            ?>
            <table class="table  table-hover table-bordered" id="tabladocumento">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Categoria</th>
                        <th>Linea</th>
                        <th>Marca</th>
                        <th>Descripción</th>
                        <th>Stock</th>
                        <th>Moneda</th>
                        <th>Precio C. C/U</th>
                        <th>Precio V. C/U</th>
                        <th>Precio V. Total</th>
                        <th>Precio C. Total</th>
                        <th>Utilidad</th>
                        <!--<th>Sucursal</th>-->
                    </tr>
                </thead>
            <!--                                <tfoot>
                    <tr>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Serie</th>
                        <th>Número</th>
                        <th>RUC/ DNI</th>
                        <th>Nombre / Rz. Social</th>
                        <th>Total</th>
                        <th>Est. Local</th>
                        <th>Est. Sunat</th>
                        <th>Imprimir</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>-->
                <tbody >
                    <?php
                    $precioc = 0;
                    $preciov = 0;
                    $utilidad = 0;
                      foreach ($valorizado as $detalle){

                                echo '<tr>';

                                echo '<td>'.$detalle['codigo'].'</td>';

                                echo '<td>'.$detalle['categoria'].'</td>';
                                echo '<td>'.$detalle['linea'].'</td>';
                                echo '<td>'.$detalle['marca'].'</td>';
                                echo '<td>'.$detalle['descripcion'].'</td>';
                                echo '<td>'.$detalle['stock'].'</td>';
                                echo '<td>'.$detalle['moneda'].'</td>';
                                echo '<td>'.$detalle['precioc'].'</td>';
                                echo '<td>'.$detalle['preciov'].'</td>';
                                echo '<td>'.$detalle['preciovtotal'].'</td>';
                                echo '<td>'.$detalle['precioctotal'].'</td>';
                                echo '<td>'.$detalle['utilidad'].'</td>';
//                                echo '<td>'.$detalle['sucursal'].'</td>';


                                echo '</tr>';
                                
                                $precioc += $detalle['precioctotal'];
                                $preciov += $detalle['preciovtotal'];
                                $utilidad += $detalle['utilidad'];



                        }        

                        ?>
            



                </tbody>

            </table>
          
            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            
             <div class="card">
                        <div class="header bg-warning">
                            <h5>Resumen</h5>
                        </div>
                        <div class="body">
                            <label>Total Compra: <?= number_format($precioc,2) ?></label> <br>
                            <label>Total Venta: <?= number_format($preciov,2) ?></label> <br>
                            <label>Total utilidad: <?= number_format($utilidad,2) ?></label>


                        </div>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 20
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });

                    
                });
                
    


            </script>    


            <?php
        }
    }
    
    function  stockvalorizadoexcel() {
        
        if (isset($_GET['cbsucursalstockgeneral']) && isset($_GET['cbmarcastockgeneral']) && isset($_GET['cblineastockgeneral']) && isset($_GET['cbcategoriastockgeneral'])
                && isset($_GET['txtbuscarstockgeneral']) && isset($_GET['cbmonedastockgeneral'])) {

            $idsucursal = $_GET['cbsucursalstockgeneral'];
            $marca = $_GET['cbmarcastockgeneral'];
            $linea = $_GET['cblineastockgeneral'];
            $categoria = $_GET['cbcategoriastockgeneral'];
            $buscar = $_GET['txtbuscarstockgeneral'];
            $moneda = $_GET['cbmonedastockgeneral'];

            $general = $this->producto->selectstockvalorizado($idsucursal, $marca, $linea,$categoria, $buscar,$moneda);
            
            require_once 'view/reportes/functions/excel.php';
//            
//            activeErrorReporting();
//            noCli();

            require_once 'plugins/PHPExcel/Classes/PHPExcel.php';
            require_once 'view/reportes/stock/valorizadoStockExcel.php';
            
        }
        
        
        
    }
      function selectstockcompraventa() {
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-compraventareporte') || permisos::rol('rol-compraventareportesucur')){
            
            $almacen = new Almacen();
        
            $sucursales = $almacen->selectAll();
            $marcas = $this->marca->selectAll();
            $categorias = $this->categoria->selectAll();
            $lineas = $this->linea->selectAll();
            $cabecera = $this->producto->selectstockcompraventa($_SESSION['idalmacen'], '', '', '', '');
    //        $detallado = $this->documento->selectdetallado($desde, $hasta, 'Factura','Venta' ,'', '', '','Soles', $_SESSION['idsucursal']);
    //        var_dump($detallado);
            require_once 'view/reportes/stock/listarcompraventa.php';
        }else{
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
    }
    
     function searchstockcompraventa() {
//        var_dump($_POST);
         
        if (isset($_POST['cbmarcastockgeneral']) && isset($_POST['cblineastockgeneral']) && isset($_POST['cbcategoriastockgeneral'])
                && isset($_POST['txtbuscarstockgeneral']) ) {
            if(permisos::rol('rol-compraventareportesucur') && isset($_POST['cbsucursalstockgeneral'])){
               $idsucursal = $_POST['cbsucursalstockgeneral']; 
            }elseif(permisos::rol('rol-compraventareporte')){
                $idsucursal = $_SESSION['idsucursal'];
            }

            
            $marca = $_POST['cbmarcastockgeneral'];
            $linea = $_POST['cblineastockgeneral'];
            $categoria = $_POST['cbcategoriastockgeneral'];
            $buscar = $_POST['txtbuscarstockgeneral'];
           

            $cabecera = $this->producto->selectstockcompraventa($idsucursal, $marca, $linea,$categoria, $buscar);

            ////// tabla ///// 
            ?>
            <table class="table  table-hover table-bordered" id="tabladocumento">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Categoria</th>
                        <th>Linea</th>                          
                        <th>Marca</th>
                        <th>Descripción</th>
                        <th>Compra</th>  
                        <th>Venta</th>
                        <th>Saldo</th>
<!--                        <th>Sucursal</th>-->
                    </tr>
                </thead>
            <!--                                <tfoot>
                    <tr>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Serie</th>
                        <th>Número</th>
                        <th>RUC/ DNI</th>
                        <th>Nombre / Rz. Social</th>
                        <th>Total</th>
                        <th>Est. Local</th>
                        <th>Est. Sunat</th>
                        <th>Imprimir</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>-->
                <tbody >
                    <?php 
                        $saldo = 0;
                        foreach ($cabecera as $detalle){
                           $saldo = $detalle['compra'] - $detalle['venta'];
                            echo '<tr>';

                            echo '<td>'.$detalle['codigo'].'</td>';

                            echo '<td>'.$detalle['categoria'].'</td>';
                            echo '<td>'.$detalle['linea'].'</td>';

                            echo '<td>'.$detalle['marca'].'</td>';
                            echo '<td>'.$detalle['descripcion'].'</td>';
                            echo '<td>'.$detalle['compra'].'</td>';
                            echo '<td>'.$detalle['venta'].'</td>';
                            echo '<td>'.$detalle['stock_actual'].'</td>';
                           

                            echo '</tr>';



                        }       

                        ?>
            



                </tbody>

            </table>
          
            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 20
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });

                    
                });
                
    


            </script>    


            <?php
        }
    }
    
    function  stockcompraventaexcel() {
        
        if (isset($_GET['cbsucursalstockgeneral']) && isset($_GET['cbmarcastockgeneral']) && isset($_GET['cblineastockgeneral']) && isset($_GET['cbcategoriastockgeneral'])
                && isset($_GET['txtbuscarstockgeneral'])) {

            $idsucursal = $_GET['cbsucursalstockgeneral'];
            $marca = $_GET['cbmarcastockgeneral'];
            $linea = $_GET['cblineastockgeneral'];
            $categoria = $_GET['cbcategoriastockgeneral'];
            $buscar = $_GET['txtbuscarstockgeneral'];
            

            $cabecera = $this->producto->selectstockcompraventa($idsucursal, $marca, $linea,$categoria, $buscar);
            
            require_once 'view/reportes/functions/excel.php';
//            
//            activeErrorReporting();
//            noCli();

            require_once 'plugins/PHPExcel/Classes/PHPExcel.php';
            require_once 'view/reportes/stock/compraventaStockExcel.php';
            
        }
        
        
        
    }
    
        function kardex() {
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-kardexreporte') || permisos::rol('rol-kardexreportesucur')){
            $month = date('m');
            $year = date('Y');
            $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
            $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
            $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
            
            $almacen = new Almacen();
        
            $sucursales = $almacen->selectAll();

            $fisico = $this->producto->kardexfisico($desde, $hasta,$_SESSION['idalmacen']); //$idprod
    //        $detallado = $this->documento->selectdetallado($desde, $hasta, 'Factura','Venta' ,'', '', '','Soles', $_SESSION['idsucursal']);
    //        var_dump($detallado);
            require_once 'view/reportes/kardex/listarkardex.php';
        }else{
            require_once 'view/sinpermiso.php';
        }
        
        
        require_once 'view/layout/footer.php';
    }
    
     function searchkardexfisico() {
//         var_dump($_POST);
         
        if (isset($_POST['dpdesde']) && isset($_POST['dphasta']) 
                && !empty($_POST['dpdesde']) && !empty($_POST['dphasta']) 
                && isset($_POST['cbkardex']) && !empty($_POST['cbkardex'])) {
            
            if(permisos::rol('rol-kardexreportesucur') && isset($_POST['cbsucursal'])){ //&& !empty($_POST['cbsucursal'])&& isset($_POST['id'])&& !empty($_POST['id'])  
                $idsucur = $_POST['cbsucursal'];
            }elseif (permisos::rol('rol-kardexreporte')) {
                $idsucur = $_SESSION['idalmacen'];
            }
            

            $emision = $_POST['dpdesde'];

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');
            $kardex = $_POST['cbkardex'];

            $vencimiento = $_POST['dphasta'];
            $dateven = DateTime::createFromFormat('d/m/Y', $vencimiento);
            $vencimientof = $dateven->format('Y-m-d');
            
//            $idprod = $_POST['id'];
            

            $fisico = $this->producto->kardexfisico($emisionf, $vencimientof,$idsucur); //$idprod

            ////// tabla ///// 
            ?>
            <table class="table  table-hover table-bordered" id="tabladocumento">
                <thead>
                    <tr>
                        <?php if($kardex == 'fisico'){ ?>
                         <th>Operación</th>
                                     
                        <th>Fecha</th>
                      
                        <th>Observación</th>
                        <th>Descripción</th>
                        <th>Cantidad</th>
                        <th>Stock anterior</th>
                        <th>Saldo</th>
                        <?php }else { ?>
                        <th>Operación</th>
                                     
                        <th>Fecha</th>
                        <th>Observación</th>
                        <th>Descripción</th>
                        <th>Cantidad</th>
                        <th>Precio Unit.</th>
                         <th>Moneda</th>
                        <th>Stock anterior</th>
                        <th>Saldo</th>
                        <th>#</th>
                        
                        <?php } ?>
    </tr>
                </thead>
            <!--                                <tfoot>
                    <tr>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Serie</th>
                        <th>Número</th>
                        <th>RUC/ DNI</th>
                        <th>Nombre / Rz. Social</th>
                        <th>Total</th>
                        <th>Est. Local</th>
                        <th>Est. Sunat</th>
                        <th>Imprimir</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>-->
                <tbody >
                     <?php 
                        $se = 0;
                        $saldov = 0;
                        foreach ($fisico as $detalle){
                            
                           echo '<tr>';
                           if($kardex == 'fisico'){
                                echo '<td>'.$detalle['concepto'].'</td>';
                                echo '<td>'.$detalle['created_at'].'</td>';
                                echo '<td>'.$detalle['observacion'].'</td>';
                                echo '<td>'.$detalle['descripcion'].'</td>';
                                echo '<td>'.$detalle['cantidad'].'</td>';
                                echo '<td>'.$detalle['stock_anterior'].'</td>';
                                echo '<td>'.$detalle['stock_actual'].'</td>';
                               
                               
                           }else {
                               $saldov=$detalle['precio'] * $se;
                               
                                echo '<td>'.$detalle['concepto'].'</td>';
                                echo '<td>'.$detalle['created_at'].'</td>';
                                echo '<td>'.$detalle['observacion'].'</td>';
                                echo '<td>'.$detalle['descripcion'].'</td>';
                                echo '<td>'.$detalle['cantidad'].'</td>';

                                echo '<td>'.$detalle['precio'].'</td>';
                                echo '<td>'.$detalle['moneda'].'</td>';
                                echo '<td>'.$detalle['stock_anterior'].'</td>';
                               
                                echo '<td>'.$detalle['stock_actual'].'</td>';
                                echo '<td>'.number_format($detalle['cantidad'] * $detalle['precio'],2).'</td>';
                              
                                
                               
                           }

                            echo '</tr>';



                        } 

                        ?>
            



                </tbody>

            </table>
          
            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 20
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });

                    
                });
                
    


            </script>    


            <?php
        }
    }
    
       function  kardexfisicoexcel() {
        
        if (isset($_GET['dpdesde']) && isset($_GET['dphasta']) && isset($_GET['id']) && isset($_GET['cbsucursal'])
                && !empty($_GET['dpdesde']) && !empty($_GET['dphasta']) && !empty($_GET['id']) && !empty($_GET['cbsucursal'])
                && isset($_GET['cbkardex']) && !empty($_GET['cbkardex'])) {
            
            $kardex = $_GET['cbkardex'];
            $emision = $_GET['dpdesde'];

//            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
//            $emisionf = $dateemis->format('Y-m-d');


            $vencimiento = $_GET['dphasta'];
//            $dateven = DateTime::createFromFormat('d/m/Y', $vencimiento);
//            $vencimientof = $dateven->format('Y-m-d');
            
            $idprod = $_GET['id'];
            $idsucur = $_GET['cbsucursal'];

            $fisico = $this->producto->kardexfisico($emision, $vencimiento,$idsucur);
            
            require_once 'view/reportes/functions/excel.php';
//            
//            activeErrorReporting();
//            noCli();

            require_once 'plugins/PHPExcel/Classes/PHPExcel.php';
            require_once 'view/reportes/kardex/kardexfisicoExcel.php';
            
        }
        
        
        
    }
    
    function searchstock(){
        if(isset($_POST['idprod']) && !empty($_POST['idprod'])){
           $idprod = trim($_POST['idprod']);
           $productos = $this->producto->selectstocksucursal($idprod);
           
//           var_dump($productos);
            echo '<table class="table table-hover">
                    <thead>
                    <th>Almacén</th>
                    <th>Descripción</th>
                    <th>Stock</th>
                    </thead>
                    <tbody>';
                  
            foreach ($productos as $prod){
                    echo '<tr>';
                    echo '<td>'.$prod['almacen'].'</td>';
                    echo '<td>'.$prod['descripcion'].'</td>';
                    echo '<td>'.$prod['stock'].'</td>';
                    echo '</tr>';
                
                
            }
           
            echo '</tbody>';
            echo '</table>';       
            
        }else {
            
            echo '<label class="text-danger">Realice busqueda del producto o servicio</label>';
        }
        
        
        
    }
    
    function searchadvanced(){
//        var_dump($_POST);
        $result = array();
        if(isset($_POST['txtnombre']) && isset($_POST['txtnombrealt']) && isset($_POST['txtdetalle']) && isset($_POST['cbcategoria']) && isset($_POST['cblinea']) && isset($_POST['cbmarca'])
              && isset($_POST['filtro']) && isset($_POST['txtdetalledos'])){
            
            $urlup='';
            $urldel ='';
            if(isset($_POST['urlup']) && isset($_POST['urldel'])){
                $urlup = $_POST['urlup'];
                $urldel = $_POST['urldel']; 
            }
            
            $cadenaalt = $_POST['txtnombrealt'];
            $detalle = $_POST['txtdetalle'];
            
            $cadena = $_POST['txtnombre'];
            $categoria = $_POST['cbcategoria'];
            $linea = $_POST['cblinea'];
            $marca = $_POST['cbmarca'];
            $filtro = str_replace(' ', '', $_POST['filtro']);
            $detalledos = $_POST['txtdetalledos'];
            $buquedas = $this->producto->searchadvanced($cadena,$cadenaalt,$detalle,$detalledos, $categoria, $linea, $marca, $filtro);
//            $buquedas = $this->producto->searchadvanced("", "", "", "", "");
            
             
             $i = 1;
        
            foreach ($buquedas as $busqueda){
                $serie = '';
                if (!empty($busqueda->getSerie())){
                    $serie = ' Series: '.$busqueda->getSerie();
                    
                }
                $r = [
                    'item'=> $i,
                    'codigo'=>$busqueda->getCodigo(),
                    'codigoalt'=>$busqueda->getCodigoalternativo(),
                    'descripcion'=>$busqueda->getDescripcion().$serie,
                    'descripciondos'=>$busqueda->getDescripciondos(),
                    'marca'=>$busqueda->getMarca(),
                    'linea'=>$busqueda->getLinea(),
                    'categoria'=>$busqueda->getCategoria(),
                    'caracteristica'=>$busqueda->getCaracteristica(),
                    'stock'=>$busqueda->getStock(),
                    'moneda'=>$busqueda->getMoneda(),
                    'precioc'=>$busqueda->getPrecioc(),
                    'preciov'=>$busqueda->getPreciov(),
                    'detalledos' => $busqueda->getObservaciondos(),
                   
                    'observacion'=>$busqueda->getObservacion(),

                    'acciones'=> '<a href="'.base_url.$urlup.'&id='.$busqueda->getId().'" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>'
                                            .' <button  type="button" class="btn btn-danger" onclick=eliminar(' . $busqueda->getId() . ',"' . $urldel . '","delete")><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>'
                ];
                $i++;
                array_push($result, $r);
            }
        
      
        
        
            
            
            
        }
        echo '{"data":'.json_encode($result).'}';  // send data as json format
       
    }
    
    function selectprecios(){
        
        if(isset($_POST['id'])){
            
            $id = $_POST['id'];
            
            $prodm  = new producto();

            $producto = $prodm->selectone($id);

        
            $precios = array(
                $producto->getPreciov(),
                $producto->getPreciodos(),
                $producto->getPreciotres()
                
            );
                
         echo json_encode($precios);   
            
        }
        
        
    }
    
    
     
    
 


}
