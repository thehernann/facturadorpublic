<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of documentopagoController
 *
 * @author HERNAN
 */
require_once 'model/pagodocumento.php';
require_once 'model/documento.php';
require_once 'model/sucursal.php';
class documentopagoController {
    //put your code here
    private $pago; 
    
    function __construct() {
        $this->pago = new pagodocumento();
        
    }
    
    function amortizar(){
         require_once 'view/layout/header.php';
         if(isset($_GET['id']) && !empty($_GET['id'])){
            $id = $_GET['id'];
            $documentom = new documento();
            $documento =$documentom->selectOne($id);
            $pagos = $this->pago->selectOne($id);
            $amortizado = 0;
            foreach($pagos as $pago){
               $amortizado += $pago->getMonto();
            }

            require_once 'view/reportes/cuentacobrar/amortizar.php';
             
         }else {
             require_once 'view/error.php';
         }
         
      
        
        require_once 'view/layout/footer.php';
        
        
    }
        function imprimir(){
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            
            $sucursal = new sucursal();
            $sucur = $sucursal->selectOne($_SESSION['idsucursal']);
            
            $documentom = new documento();
            $document =$documentom->selectOne($id);
            $pagos = $this->pago->selectOne($id);
            $amortizado = 0;
            foreach($pagos as $pago){
               $amortizado += $pago->getMonto();
            }
//            require_once "vendor/autoload.php";
//            require_once 'libs/phpqrcode/qrlib.php';
            require_once 'view/CifrasEnLetras.php'; 
//            require_once 'libs/dompdf';
            require_once 'view/reportes/cuentacobrar/printpagos.php'; 
        }
    }
    
    function pagar(){
        var_dump($_POST);
        $fila = 0;
        if(isset($_POST['id']) && isset($_POST['cbtipopago']) && isset($_POST['txtnroop']) && isset($_POST['txtmonto']) 
                && isset($_POST['dpfecha']) && !empty($_POST['id']) && !empty($_POST['cbtipopago']) && !empty($_POST['txtmonto'])
               && is_numeric($_POST['txtmonto'])  && !empty($_POST['dpfecha']) && isset($_POST['txtobservacion']) && isset($_POST['txtentidad'])){
            
            $emision = $_POST['dpfecha'];

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');
            
            $pago = new pagodocumento();
            $pago->setId_documento($_POST['id']);
            $pago->setTipopago($_POST['cbtipopago']);
            $pago->setNop($_POST['txtnroop']);
            $pago->setMonto($_POST['txtmonto']);
            $pago->setFecha($emisionf);
            $pago->setObservacion($_POST['txtobservacion']);
            $pago->setEntidad($_POST['txtentidad']);
            
            $fila = $this->pago->insert($pago);
            
            if($fila >0){
                ?> 
            <script>
               
                 swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                
            </script>  <?php 
               echo "<META HTTP-EQUIV ='Refresh' CONTENT='0; URL=".base_url."documento/cuentacobrar'>";
 
            }else {
                ?> 
                <script>

                    swal("No se realizarón cambios!", "Algo sucedio mal :(.", "error");
                </script>  <?php 
                
            }
       
        }else {
        
        ?> 
            <script>
               
                 swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'error');
            </script>  <?php 
    }
        
    }
    

}
