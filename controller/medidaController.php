<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of medidaController
 *
 * @author HERNAN
 */
require_once 'model/unidmedida.php';
class medidaController {
    //put your code here
    
    private $medida;
    
    function __construct() {
      $this->medida = new unidmedida();   
    }
    
    function insert(){
//        var_dump($_POST);
        if(permisos::rol('rol-unidnuevo')){
        $fila= 0;       
        if(isset($_POST['txtdescripcionunid'])
                && !empty($_POST['txtdescripcionunid'])){ // && isset($_POST['txtcodunid']) && isset($_POST['txtordenunid']) 
            $cadena = trim($_POST['txtdescripcionunid']);
            
            if($this->medida->duplicado($cadena) == 0){
                $medida = new unidmedida();
//                $medida->setCodigo($_POST['txtcodunid']);
                $medida->setDescripcion($_POST['txtdescripcionunid']);
//                $medida->setOrden($_POST['txtordenunid']);
                $fila=$this->medida->insert($medida); 
                
            }else {
                
                ?> 
                    <script>

                         swal('Error', 'La unidad ya se encuentra registrada.', 'error');
                    </script>  <?php
            }
            if($fila >0){
                 ?> 
                <script>

                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                     $('#FormularioAjax').trigger("reset");
                     
                     if($('.modal').length && $('#divmedida').length){
                            $('.modal').modal('hide');
                            $('#divmedida').load(location.href + " #divmedida>*", ""); 
                            
                        }
                     
                     
                     
                </script>  <?php
                    
                
            }
             
            
        }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            
            
            
        }
    }else{
         ?> 
            <script>
                 swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
    }
     
    }
    
    function update(){
       
        if(permisos::rol('rol-unideditar')){
        $fila= 0;       
        if(isset($_POST['id']) && isset($_POST['txtdescripcionunid']) 
                && !empty($_POST['txtdescripcionunid'])){ //&& isset($_POST['txtcodunid']) && isset($_POST['txtordenunid'])
            $cadena = trim($_POST['txtdescripcionunid']);
            $id = $_POST['id'];
            if($this->medida->duplicadoedit($cadena,$id) == 0){
                $medida = new unidmedida();
                $medida->setId($_POST['id']);
//                $medida->setCodigo($_POST['txtcodunid']);
                $medida->setDescripcion($_POST['txtdescripcionunid']);
//                $medida->setOrden($_POST['txtordenunid']);
                $fila= $this->medida->update($medida);
                
                
            }else {
                
                ?> 
                    <script>

                         swal('Error', 'La unidad ya se encuentra registrada.', 'error');
                    </script>  <?php
            }
             if($fila >0){
                 ?> 
                <script>

                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                </script>  <?php
                    
                
            }
            
            
            
        }else {
             ?> 
                <script>

                     swal('No se realizo registro', 'Ingrese campos obligatorios', 'success');
                </script>  <?php
            
        }
    }else{
        ?> 
                <script>

                     swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'success');
                </script>  <?php
        
    }
       
    }
    
    
    function select(){
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-unidlistar')){
           $medidas = $this->medida->selectAll();
            require_once 'view/unidmedida/listar_medida.php';  
            
        }else{
            $url='';
            require_once 'view/sinpermiso.php';
            
        }
        
      
        
        require_once 'view/layout/footer.php';
    }
    
    function crear(){
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-unidnuevo')){
            $medida= new unidmedida();
            require_once 'view/unidmedida/form_medida.php';
            
        }else {
            $url='medida/select';
            require_once 'view/sinpermiso.php';
        }
   
        require_once 'view/layout/footer.php';
        
        
        
    }
    
    function cargar(){
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-unideditar')){
            if(isset($_GET['id']) && !empty($_GET['id'])){
            
            $medida = $this->medida->selectOne($_GET['id']);
            require_once 'view/unidmedida/form_medida.php';
            }else{

                require_once 'view/error.php';
            }
            
        }else {
            $url='medida/select';
              require_once 'view/sinpermiso.php';
            
        }
        
        
        require_once 'view/layout/footer.php';
        
        
        
    }
    
}
