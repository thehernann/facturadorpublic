<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of categoriaController
 *
 * @author HERNAN
 */
require_once 'model/categoriaprod.php';
class categoriaController {
    //put your code here
         private $categoria;
    
    function __construct() {
      $this->categoria = new categoriaprod();   
    }
    
    function insert(){
//        var_dump($_POST);
        if(permisos::rol('rol-categorianuevo')){
        $fila= 0;       
        if(isset($_POST['txtdescripcioncategoria']) 
                && !empty($_POST['txtdescripcioncategoria'])){ //&& isset($_POST['txtcodcategoria']) && isset($_POST['txtordencategoria'])
                $cadena = trim($_POST['txtdescripcioncategoria']);
            if($this->categoria->duplicado($cadena) == 0){
                $categoria = new categoriaprod();
//                $categoria->setCodigo($_POST['txtcodcategoria']);
                $categoria->setDescripcion($_POST['txtdescripcioncategoria']);
//                $categoria->setOrden($_POST['txtordencategoria']);
                $fila= $this->categoria->insert($categoria);
                
            }else {
                
                ?> 
                    <script>

                         swal('Error', 'La categoria ya se encuentra registrada.', 'error');
                    </script>  <?php
            }
            if($fila >0){
                 ?> 
                <script>

                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                     $('#FormularioAjax').trigger("reset");
                     
                      if($('.modal').length && $('#divcategoria').length){
                            $('.modal').modal('hide');
                            $('#divcategoria').load(location.href + " #divcategoria>*", ""); 
                            
                        }
                     
                     
                </script>  <?php
                    
                
            }
            
            
        }
            else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            
            
            
        }
        
    }else{
        ?> 
            <script>
               
                 swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
        
    }
            
            
    }
       
    
    
    
    function update(){
       if(permisos::rol('rol-categoriaeditar')){
        $fila= 0;       
        if(isset($_POST['id']) && isset($_POST['txtdescripcioncategoria']) 
                && !empty($_POST['txtdescripcioncategoria']) && !empty($_POST['id'])){ //&& isset($_POST['txtcodcategoria']) && isset($_POST['txtordencategoria'])
             $cadena = trim($_POST['txtdescripcioncategoria']);
             $id = $_POST['id'];
           
            if($this->categoria->duplicadoedit($cadena,$id) == 0){
            $categoria = new categoriaprod();
            $categoria->setId($_POST['id']);
//            $categoria->setCodigo($_POST['txtcodcategoria']);
            $categoria->setDescripcion($_POST['txtdescripcioncategoria']);
//            $categoria->setOrden($_POST['txtordencategoria']);
            $fila= $this->categoria->update($categoria);
            if($fila >0){
                 ?> 
                <script>
                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                </script>  <?php  
            }else {
                 ?> 
                <script>
                     swal('Error', 'No se realizarón cambios.', 'error');
                </script>  <?php  
                
            }
            
            
            
        }else {
          ?> 
                    <script>
                        swal('Error', 'La categoria ya se encuentra registrada.', 'error');
                    </script>  <?php
            }
          
            
        }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            
            
            
        }
    }else{
        ?> 
            <script>
               
                 swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
        
    }
       
    }
    
    
    function select(){
        require_once 'view/layout/header.php';
        
        
        if(permisos::rol('rol-categorialistar')){
            $categorias = $this->categoria->selectAll();        
            require_once 'view/categoria/listar_categoria.php';
            
        }else {
            
            require_once 'view/sinpermiso.php';
        }
        
      
        
        require_once 'view/layout/footer.php';
    }
    
    function crear(){
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-categorianuevo')){
        $categoria= new categoriaprod();
       
        
        
        require_once 'view/categoria/form_categoria.php';
      
        }else {
            require_once 'view/sinpermiso.php';
        }
        require_once 'view/layout/footer.php';
        
        
        
    }
    
    function cargar(){
        
        require_once 'view/layout/header.php';
        if(permisos::rol('rol-categoriaeditar')){
            if(isset($_GET['id']) && !empty($_GET['id'])){
            
            $categoria = $this->categoria->selectOne($_GET['id']);
            require_once 'view/categoria/form_categoria.php';
            }else{

                require_once 'view/error.php';
            }
        }else {
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
        
        
        
    }
}
