<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of lineacontroller
 *
 * @author HERNAN
 */
require_once 'model/linea.php';
class lineacontroller {
    //put your code here
     private $linea;
    
    function __construct() {
      $this->linea = new linea();   
    }
    
    function insert(){
//        var_dump($_POST);
        if(permisos::rol('rol-lineanuevo')){
        $fila= 0;       
        if(isset($_POST['txtdescripcionlinea']) 
                && !empty($_POST['txtdescripcionlinea'])){ //&& isset($_POST['txtcodlinea']) && isset($_POST['txtordenlinea'])
             
            $cadena = trim($_POST['txtdescripcionlinea']);
            if($this->linea->duplicado($cadena) == 0){
                $linea = new linea();
//                $linea->setCodigo($_POST['txtcodlinea']);
                $linea->setDescripcion($_POST['txtdescripcionlinea']);
//                $linea->setOrden($_POST['txtordenlinea']);
                $fila= $this->linea->insert($linea); 
                
                if($fila>0 ){
                     ?> 
                    <script>

                         swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                         $('#FormularioAjax').trigger("reset");
                         
                         if($('.modal').length && $('#divlinea').length){
                            $('.modal').modal('hide');
                            $('#divlinea').load(location.href + " #divlinea>*", "");
                            
                        }
                    </script>  <?php
                    
                }else {
                 ?> 
                <script>
                     swal('Error', 'No se realizarón cambios.', 'error');
                </script>  <?php  
                
                }
            }else {
                
                ?> 
                    <script>

                         swal('Error', 'La linea ya se encuentra registrada.', 'error');
                    </script>  <?php
            }
            
        }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
   
        }
    }else {
         ?> 
            <script>
               
                 swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
        
    }
    }
    
    function update(){
        
        if(permisos::rol('rol-lineaeditar')){
       
        $fila= 0;       
        if(isset($_POST['id']) && isset($_POST['txtdescripcionlinea']) 
                && !empty($_POST['txtdescripcionlinea'])){ //&& isset($_POST['txtcodlinea']) && isset($_POST['txtordenlinea'])
            
            $cadena = trim($_POST['txtdescripcionlinea']);
            $id = $_POST['id'];
            
            if($this->linea->duplicadoedit($cadena,$id) == 0){
            $linea = new linea();
            $linea->setId($_POST['id']);
//            $linea->setCodigo($_POST['txtcodlinea']);
            $linea->setDescripcion($_POST['txtdescripcionlinea']);
//            $linea->setOrden($_POST['txtordenlinea']);
            $fila= $this->linea->update($linea);
            
            if($fila>0 ){
                     ?> 
                    <script>

                         swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                         $('#FormularioAjax').trigger("reset");
                    </script>  <?php
                    
                }else {
                 ?> 
                <script>
                     swal('Error', 'No se realizarón cambios.', 'error');
                </script>  <?php  
                
                }
            
            
        }else {
                
                ?> 
                    <script>

                         swal('Error', 'La linea ya se encuentra registrada.', 'error');
                    </script>  <?php
            }
     
    }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            
            
            
        }
    }else{
         ?> 
            <script>
               
                 swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php

    }
    }
    
    
    function select(){
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-linealistar')){
          $lineas = $this->linea->selectAll();
          require_once 'view/linea/listar_linea.php';  
            
        }else {
            require_once 'view/sinpermiso.php';
        }
        
      
        
        require_once 'view/layout/footer.php';
    }
    
    function crear(){
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-lineanuevo')){
            $linea= new linea();
            require_once 'view/linea/form_linea.php';
  
        }else {
            $url = '';
            require_once 'view/sinpermiso.php';
        }
        
      
        
        require_once 'view/layout/footer.php';
        
        
        
    }
    
    function cargar(){
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-lineaeditar')){
            if(isset($_GET['id']) && !empty($_GET['id'])){
            
            $linea = $this->linea->selectOne($_GET['id']);
            require_once 'view/linea/form_linea.php';
            }else{

                require_once 'view/error.php';
            }
        }else {
            $url='';
            require_once 'view/sinpermiso.php';
        }
        
        
        require_once 'view/layout/footer.php';
        
        
        
    }
    
    

}
