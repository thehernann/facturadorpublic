<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'model/bancos.php';

class bancosController{
    
    private $bancosm;

    
    function __construct() {
        $this->bancosm = new bancos();
       
    }
    
    function select(){
        
        
        require_once 'view/layout/header.php';

            
            if(permisos::rol('rol-bancolistar')){
                $bancos = $this->bancosm->selectAll();
                require_once 'view/bancos/listar_bancos.php';
                
            }else {
                
                require_once 'view/sinpermiso.php';
            }
 
        require_once 'view/layout/footer.php';
        
        
        
        
    }
    
    function crear(){
        require_once 'view/layout/header.php';
        
            
            if(permisos::rol('rol-banconuevo')){
                $banco = new bancos();
                require_once 'view/bancos/form_bancos.php';
           
            }else{
                $url = 'bancos/select';
                require_once 'view/sinpermiso.php';
            }
        

        require_once 'view/layout/footer.php';
        
        
    }
    
    function insert(){
       
        if(isset($_POST['txtnombre']) && !empty($_POST['txtnombre'])){
            $nombre =trim($_POST['txtnombre']);
            $fila = 0;
            if($this->bancosm->duplicado($nombre) == 0){
                $banco = new bancos();
                $banco->setNombre($nombre);
                $fila= $this->bancosm->insert($banco);
                
            }else {
                ?> 
            <script>
               
                 swal('No se realizo registro', 'Ya se encuentra registrado', 'error');
            </script>  <?php
            }
            
             if($fila > 0){
                ?> 
                  <script>

                       swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                       $('#FormularioAjax').trigger("reset");

                  </script>  <?php

           }      
        }else {
            
             ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
        }
        
    }
    
    function cargar(){
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-bancoeditar')){
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $id = $_GET['id'];
            $banco = $this->bancosm->selectOne($id);
        
            require_once 'view/bancos/form_bancos.php';
      
        
        
        }else {
            require_once 'view/error.php';
            
        }
        
        } else {
            $url = 'bancos/select';
            require_once 'view/sinpermiso.php';
            
        }
        require_once 'view/layout/footer.php';
    }
    
    function update(){
        if(permisos::rol('rol-bancoeditar')){
        if(isset($_POST['txtnombre']) && !empty($_POST['txtnombre']) && isset($_POST['id']) && !empty($_POST['id'])){
            
            $nombre = trim($_POST['txtnombre']);
            $fila = 0;
            if($this->bancosm->duplicadoedit($nombre, $_POST['id'])== 0){
                $banco = new bancos();
                $banco->setId($_POST['id']);
                $banco->setNombre($nombre);

                $fila =  $this->bancosm->update($banco);
                
                if($fila > 0){
                ?> 
                  <script>

                       swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                       

                  </script>  <?php

                } else{
                    ?> 
                  <script>

                       swal("Error", "No se realizaron cambios", "error");
                       

                  </script>  <?php
                    
                }  
                
                
                
                
            }else {
                 ?> 
            <script>
               
                 swal('No se realizo registro', 'Ya se encuentra registrado', 'error');
            </script>  <?php
                
            }
             
            
            
            
        }else{
             ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            
            
        }
        
        
        
        
        
    }else{
         ?> 
            <script>
               
                 swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
                
            }
    }

    
    
    
    
    
    
    
    
    
    
}

