<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'model/documentoSucursal.php';
require_once 'model/documento.php';
require_once 'model/sunatTransaction.php';
require_once 'model/usuario.php';
require_once 'model/tipoImpuesto.php';
require_once 'model/unidmedida.php';
require_once 'model/tiponota.php';
require_once 'model/sucursal.php';
require_once 'model/Detalle.php';
require_once 'model/tipocambio.php';
require_once 'model/Detalle.php';
require_once 'model/serieProducto.php';
require_once 'model/serieDetalle.php';
require_once 'model/documentoOtros.php';
require_once 'model/documentoGuia.php';
require_once 'model/producto.php';
require_once 'model/cuentaBancaria.php';
require_once 'model/persona.php';
require_once 'model/personaTipoDocumento.php';
require_once 'model/gastos.php';
require_once 'model/consumirFacturacion.php';
require_once 'model/anularFactura.php';
require_once 'model/caja.php';
require_once 'model/pagoMixto.php';
require_once 'model/kardex.php';
require_once 'model/almacen.php';

//require_once 'model/serieDetalle.php';


class documentoController {

    private $documento;
    private $docsucursal;
    private $sunattrans;
    private $usuario;
    private $impuesto;
    private $unidad;

    function __construct() {
        $this->docsucursal = new documentoSucursal();
        $this->documento = new documento();
        $this->sunattrans = new sunatTransaction();
        $this->usuario = new usuario();
        $this->impuesto = new tipoImpuesto();
        $this->unidad = new unidmedida();
    }

    function sale() {
        require_once 'view/layout/header.php';
        if(permisos::rol('rol-boletafacturaventa')){
            
            if(permisos::rol('rol-editarprecio')){
                $msjeditprecio='';
                $editprecio = true;
            }else{
                $msjeditprecio = '<label class="text-danger">[Necesita permisos para editar el precio]</label>';
                $editprecio = false;
            }
        $caja = new caja();
       
         if($caja->status(date('Y-m-d')) == false){
             require_once 'view/operaciones/apertura_caja.php';
             
         } else  {
            
         $sucursaldocm = new documentoSucursal();
         $tipodocventa = $sucursaldocm->selectTipoDocVenta();
         $tipocambio = new tipocambio();
         $utimotc = $tipocambio->selectMax();
         $ventatc = $utimotc->getVenta();
//         echo 'tipocambio';
//         var_dump($ventatc);
         
//         var_dump($tipodocventa[0]->getTipodoc());
         
//         $tipodocpred = array_shift($tipodocventa);
//         $tipodocpredv=$tipodocpred->getTipodoc();
//         var_dump($tipodocpredv);
         $seriesdoc = array();
         $op = "";
         if(count($tipodocventa)>0){
              $op = $tipodocventa[0]->getTipodoc();
             
         }
         $seriesdoc = $sucursaldocm->selectAll($_SESSION['idsucursal'], $op);
//         var_dump($seriesdoc);
         
         $seriepredeterminado = "";
         if(count($seriesdoc) > 0){
            $seriepredeterminado = $seriesdoc[0]->getSerie(); 
         }
//         var_dump($seriepredeterminado);
         ////////////// numero de documento predeterminado ////////////
         $documentomaxnumeromodel = $this->documento->selectMax('Venta', $op, $seriepredeterminado);
        $numeror = str_replace(" ", "", $documentomaxnumeromodel->getNumero());
        $numero = (int) $numeror + 1;
         /////////////////////////////////////////////////////////////
         
         
        $detalles = array();
//        $documentossuc = $this->docsucursal->selectAll();
        $transactions = $this->sunattrans->selectAll();
        $usuarios = $this->usuario->selectAllbysucursal();
        $impuestos = $this->impuesto->selectAll();
        $unidades = $this->unidad->selectAll();
        $seriesm = new serieProducto();
//        $series = $seriesm->select(74);
        $sucurm = new sucursal();
        $sucursales = $sucurm->selectAll();
        $tipodoc = 'Venta';
        
        $documento = new documento();
        
        $personam = new persona();
        $tipodocp = new personaTipoDocumento();
        $personabydefault = $personam->bydefault(1);
//        var_dump($personabydefault);
        $docs = $tipodocp->selectAll();
        require_once 'view/documentocabecera/form_documento.php';

        require_once 'view/documentosucursal/modalnewdocumentosucursal.php';
        }
        }else {
            
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
    }

    function notecredit() {
        require_once 'view/layout/header.php';
        if(permisos::rol('rol-notacreditoventa')){
            
            $detalles = array();
            $idsucur = $_SESSION['idsucursal'];
            $documentossuc = $this->docsucursal->selectAll($idsucur, 'nota_credito');
            $transactions = $this->sunattrans->selectAll();
            $usuarios = $this->usuario->selectAll();
            $impuestos = $this->impuesto->selectAll();
            $unidades = $this->unidad->selectAll();
            
            $tipocambio = new tipocambio();
            $utimotc = $tipocambio->selectMax();
            $ventatc = $utimotc->getCompra();
            
            $nota = new tiponota();
            $notas = $nota->select('credito');
            $tipo = 'nota_credito';
    //        $nro = $this->documento->selectMax($tipo,$tipo)->getNumero() + 1;
            $titulo = "Emitir nota de crédito electrónica";
            $sucurm = new sucursal();
            $sucursales = $sucurm->selectAll();
            ///////////// cargo un nuevo documento ////////
            $documento = new documento();
            ////////////////////////////////////////
            $tipodocp = new personaTipoDocumento();
            $docs = $tipodocp->selectAll();
            require_once 'view/documentocabecera/nota/form_documento_note.php';
            require_once 'view/documentosucursal/modalnewdocumentosucursal.php';
            
        }else{
            
            require_once 'view/sinpermiso.php';
        }
        

        require_once 'view/layout/footer.php';
    }

    function notedebit() {
        require_once 'view/layout/header.php';
        if(permisos::rol('rol-notadebitoventa')){
        $detalles = array();
        $idsucur = $_SESSION['idsucursal'];
        $documentossuc = $this->docsucursal->selectAll($idsucur, 'nota_debito');
        $transactions = $this->sunattrans->selectAll();
        $usuarios = $this->usuario->selectAll();
        $impuestos = $this->impuesto->selectAll();
        $unidades = $this->unidad->selectAll();
        
        $tipocambio = new tipocambio();
        $utimotc = $tipocambio->selectMax();
        $ventatc = $utimotc->getVenta();
        

        $nota = new tiponota();
        $notas = $nota->select('debito');

        $tipo = 'nota_debito';
//        $nro = $this->documento->selectMax($tipo,$tipo)->getNumero() + 1;
        $titulo = "Emitir nota de débito electrónica";

        ///////////// cargo un nuevo documento ////////
        $documento = new documento();
        ////////////////////////////////////////
        $sucurm = new sucursal();
        $sucursales = $sucurm->selectAll();
        $tipodocp = new personaTipoDocumento();
        $docs = $tipodocp->selectAll();
        
        require_once 'view/documentocabecera/nota/form_documento_note.php';
        require_once 'view/documentosucursal/modalnewdocumentosucursal.php';
        }else {
            require_once 'view/sinpermiso.php';
        }
        require_once 'view/layout/footer.php';
    }

    function selectdocument() {

        require_once 'view/layout/header.php';
        if(permisos::rol('rol-vercomprobanteventa') || permisos::rol('rol-vercomprobanteventaporsucur')){
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
        $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
        $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));


        $sucursal = new sucursal();

        $sucursales = $sucursal->selectAll();

        $documentos = $this->documento->select($desde, $hasta, 'Factura', 'Venta', '', '', '', $_SESSION['idsucursal']);

        require_once 'view/documentocabecera/listar.php';
        }else{
            require_once 'view/sinpermiso.php';
            
        }
        
        require_once 'view/layout/footer.php';
    }
    function selectguiatraslado() {

        require_once 'view/layout/header.php';  
        if(permisos::rol('rol-trasladooperaciones')){
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
        $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
        $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));

        $almacen = new Almacen();
        
                
//        $sucursal = new sucursal();

        $sucursales = $almacen->selectAll();
        
//        $sucursalone = $sucursal->selectOne($_SESSION['idsucursal']);

        $documentos = $this->documento->selecttraslado($desde, $hasta, 'guia_remision', 'guia_remision', '', '','', '');

        require_once 'view/documentocabecera/guiatraslado/listar.php';
        }else{
            require_once 'view/sinpermiso.php';
            
        }
        
        require_once 'view/layout/footer.php';
    }

    function selectdetallado() {

        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-ventadetallereporte') || permisos::rol('rol-ventadetallereportesucur')){
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
        $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
        $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));


        $sucursal = new sucursal();

        $sucursales = $sucursal->selectAll();

        $detallado = $this->documento->selectdetallado($desde, $hasta, 'Factura', 'Venta', '', '', '', 'Soles', $_SESSION['idsucursal']);
//        var_dump($detallado);
        require_once 'view/reportes/venta/listar.php';
        }else {
            
            require_once 'view/sinpermiso.php';
        }
        require_once 'view/layout/footer.php';
    }

    function selectdetalladocompra() {

        require_once 'view/layout/header.php';

        if(permisos::rol('rol-compradetallereporte') || permisos::rol('rol-compradetallereportesucur')){
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
        $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
        $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));


        $sucursal = new sucursal();

        $sucursales = $sucursal->selectAll();

        $detallado = $this->documento->selectdetallado($desde, $hasta, 'Factura', 'Compra', '', '', '', 'Soles', $_SESSION['idsucursal']);
//        var_dump($detallado);
        require_once 'view/reportes/compra/listarcompra.php';
        }else {
            require_once 'view/sinpermiso.php';
        }
        require_once 'view/layout/footer.php';
    }

    function selectcompra() {

        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-vercomprobantecompra') || permisos::rol('rol-vercomprobantecomprasucur')){
            $month = date('m');
            $year = date('Y');
            $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
            $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
            $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));


            $sucursal = new sucursal();

            $sucursales = $sucursal->selectAll();

            $documentos = $this->documento->select($desde, $hasta, 'Factura', 'Compra', '', '', '', $_SESSION['idsucursal']);

            require_once 'view/documentocabecera/compra/listarcompras.php';
            
        }else {
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
    }

    function selectordencompra() {  // permisos en listarordencompra.php

        require_once 'view/layout/header.php';
       
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
        $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
        $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));


        $sucursal = new sucursal();

        $sucursales = $sucursal->selectAll();

        $documentos = $this->documento->select($desde, $hasta, 'orden_compra', 'orden_compra', '', '', '', $_SESSION['idsucursal']);

        require_once 'view/documentocabecera/ordencompra/listarordencompra.php';
        require_once 'view/layout/footer.php';
    }

    function selectcotizacion() { // permisos en listar_cotizacion.php
        require_once 'view/layout/header.php';
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
        $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
        $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));


        $sucursal = new sucursal();

        $sucursales = $sucursal->selectAll();

        $documentos = $this->documento->select($desde, $hasta, 'Cotizacion', 'Cotizacion', '', '', '', $_SESSION['idsucursal']);

        require_once 'view/documentocabecera/cotizacion/listar_cotizacion.php';
        require_once 'view/layout/footer.php';
    }

    function searchventa() {
//        var_dump($_POST);
        
        if (isset($_POST['dpdesde']) && isset($_POST['dphasta']) && isset($_POST['cbtipocomprobante']) && isset($_POST['txtbuscar']) && isset($_POST['txtserie']) && isset($_POST['txtnumero']) ) { // && isset($_POST['tipodoc'])
            
            if(isset($_POST['cbsucursal']) && permisos::rol('rol-vercomprobanteventaporsucur')){
                $sucursal = $_POST['cbsucursal'];
                
            }else if(permisos::rol('rol-vercomprobanteventa')){
                $sucursal = $_SESSION['idsucursal'];
                       
            }
            
            $desde = $_POST['dpdesde'];
            $dated = DateTime::createFromFormat('d/m/Y', $desde);
            $datedf = $dated->format('Y-m-d');

            $hasta = $_POST['dphasta'];
            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_POST['cbtipocomprobante'];
//            $tipodoc = $_POST['tipodoc'];

            $buscar = $_POST['txtbuscar'];
            $serie = $_POST['txtserie'];
            $numero = $_POST['txtnumero'];
            
            $op = '';
            
            switch ($tipocomp) {
                case 'Factura':
                    $op = 'Venta';

                    break;
                case 'Boleta':
                    $op = 'Venta';

                    break;
                case 'nota_venta':
                    $op = 'Venta';

                    break;
                case 'nota_credito':
                    $op = 'nota_credito';

                    break;
                case 'nota_debito':
                    $op = 'nota_debito';

                    break;

                default:
                    break;
            }
            

            $documentos = $this->documento->select($datedf, $datehf, $tipocomp, $op, $buscar, $serie, $numero, $sucursal);

            ////// tabla ///// 
            ?>
            <table class="table  table-hover table-bordered" id="tabladocumento">
                <thead>
           
                        <tr>

                            <th>Fecha</th>
                            <th>Tipo</th>
                            <th>Serie</th>
                            <th>Número</th>
                            <th>RUC/ DNI</th>
                            <th>Nombre / Rz. Social</th>
                            <th>Moneda</th>
                            <th>Total</th>
                            <th>Estado</th>
                            <!--<th>Est. Sunat</th>-->
                            <th>Imprimir</th>
                            <th>Acciones</th>
                        </tr>
       
                </thead>
            <!--                                <tfoot>
                    <tr>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Serie</th>
                        <th>Número</th>
                        <th>RUC/ DNI</th>
                        <th>Nombre / Rz. Social</th>
                        <th>Total</th>
                        <th>Est. Local</th>
                        <th>Est. Sunat</th>
                        <th>Imprimir</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>-->
                <tbody >
                    <?php
                    foreach ($documentos as $documento) {
                        $estados = '';
                        $estadol = '';
                      

                        if ($documento->getEstadolocal() == 'Registrado') {
//                            $estados = '<span class="label bg-green">' . $documento->getEstadosunat() . '</span>';
                            $estadol = '<a href="#" data-toggle="tooltip" data-placement="top" title="'.$documento->getEstadosunat().'"><span class="label bg-green">' . $documento->getEstadolocal() . '</span></a>';
                        } else {
                            $estadol = '<a href="#" data-toggle="tooltip" data-placement="top" title="'.$documento->getEstadosunat().'"><span class="label bg-red">' . $documento->getEstadolocal() . '</span></a>';
//                            $estados = '<span class="label bg-red">' . $documento->getEstadosunat() . '</span>';
                        }

                            echo '<tr>';
                            echo '<td>' . $documento->getFechaemision() . '</td>';
                            echo '<td>' . $documento->getTipo() . '</td>';
                            echo '<td>' . $documento->getSerie() . '</td>';
                            echo '<td>' . $documento->getNumero() . '</td>';
                            echo '<td>' . $documento->getRuc() . '</td>';
                            echo '<td>' . $documento->getRazonsocial() . '</td>';
                            echo '<td>' . $documento->getMoneda() . '</td>';
                            echo '<td>' . $documento->getTotal() . '</td>';
                            echo '<td>' . $estadol . '</td>';
//                            echo '<td>' . $estados . '</td>';

                            echo '<td><a  href="' . base_url . 'documento/imprimir&id=' . $documento->getId() . '" target="_blank" data-toggle="tooltip" data-placement="top" title="PDF" style="background: none;"> <i class="material-icons">picture_as_pdf</i></a><button type ="text" style="border:none;background: none;" data-toggle="tooltip" data-placement="top" title="TICKET" onclick ="VentanaCentrada(' . "'" . base_url . 'documento/printticket&id=' . $documento->getId() . "'" . ',' . "'" . 'Ticket' . "'" . ',' . "''" . ',' . "''" . ',' . "''" . ',' . "'false'" . ');">  <i class="material-icons">confirmation_number</i> </button> </td>';
                            echo '<td><a href="#" onclick="reenviar('.$documento->getId().')"  title="Enviar a '.$documento->getEmail().'" ><div class="demo-google-material-icon"> <i class="material-icons">mail_outline</i> </a>'
                            . '<a href="' . base_url . 'documento/loaddebit&id=' . $documento->getId() . '" data-toggle="tooltip" data-placement="top" title="NOTA DE DÉBITO"><i class="material-icons">control_point</i></a>'
                            . ' <a href="' . base_url . 'documento/loadcredit&id=' . $documento->getId() . '" data-toggle="tooltip" data-placement="top" title="NOTA DE CRÉDITO"><i class="material-icons">remove_circle_outline</i></a>';
                            if ($documento->getEstadolocal() != 'Anulado') {
                                echo '<a  href="#" data-toggle="modal" data-target=".modalanulardoc" data-id="' . $documento->getId() . '" data-serie="' . $documento->getSerie() .'" data-numero="' .$documento->getNumero(). '" data-fechaemision="' .$documento->getFechaemision(). '" data-placement="top" title="ANULAR"><i class="material-icons">block</i></a></div></td>';
                            }
                       

                        echo '</tr>';
                    }
                    ?>



                </tbody>

            </table>

            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 40
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });


                });


            //       $(function (){
            //        $('table tr:eq(0)').prepend('<th>ID</th>')
            //        var id=0;
            //        $('table tr:gt(0)').each(function (){
            //            id++
            //            $(this).prepend('<td>'+id+'</td>')
            //            
            //        })
            //       
            //       
            //       
            //       
            //       })


            </script>    


            <?php
        }
    }
    function searchguiatras() {
//        var_dump($_POST);
        
        if (isset($_POST['dpdesde']) && isset($_POST['dphasta']) && isset($_POST['cbtipocomprobante']) && isset($_POST['cbpuntopartida'])  && isset($_POST['cbdestinatario'])
                && isset($_POST['txtserie']) && isset($_POST['txtnumero']) ) { // && isset($_POST['tipodoc'])
            
        
            
            $desde = $_POST['dpdesde'];
            $dated = DateTime::createFromFormat('d/m/Y', $desde);
            $datedf = $dated->format('Y-m-d');

            $hasta = $_POST['dphasta'];
            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_POST['cbtipocomprobante'];
//            $tipodoc = $_POST['tipodoc'];

            
            $serie = $_POST['txtserie'];
            $numero = $_POST['txtnumero'];
            
            $puntopartida = $_POST['cbpuntopartida'];
            $destinatario = $_POST['cbdestinatario'];
            
            $op = '';
            
            
            $documentos = $this->documento->selecttraslado($datedf, $datehf, 'guia_remision', 'guia_remision', $serie, $numero,$puntopartida, $destinatario);

           

            ////// tabla ///// 
            ?>
             <table class="table  table-hover table-bordered" id="tabladocumento">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Tipo</th>
                                        <th>Serie</th>
                                        <th>Número</th>
                                      
                                        <th>Punto de partida</th>
                                        <th>Destinatario</th>
                                        <th>Estado</th>

                                        <th>Imprimir</th>
                                        
                                    </tr>
                                </thead>
<!--                                <tfoot>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Tipo</th>
                                        <th>Serie</th>
                                        <th>Número</th>
                                        <th>RUC/ DNI</th>
                                        <th>Nombre / Rz. Social</th>
                                        <th>Total</th>
                                        <th>Est. Local</th>
                                        <th>Est. Sunat</th>
                                        <th>Imprimir</th>
                                        <th>Acciones</th>
                                    </tr>
                                </tfoot>-->
                                <tbody >
                                    <?php foreach ($documentos as $documento){
                                        
                                        $estado = '';    
                                        if($documento->getEstadolocal() == 'sin_confirmar'){
                                            $estado = '<button onclick="confirmar('."'".$documento->getId()."'".','."'".$documento->getIdalmacendestino()."'".','."'".$documento->getIdalmacenemisor()."'".')" class="btn btn-danger" title="Confirme el traslado"> Sin confirmar</button>';
                                        }
                                        
                                       if($documento->getEstadolocal() == 'confirmado'){
                                            $estado = '<span class="label bg-green"> Confirmado </span>';
                                        }
                                        
                                            echo '<tr>';
                                            echo '<td>'.$documento->getFechaemision().'</td>';
                                            echo '<td>'.$documento->getTipo().'</td>';
                                            echo '<td>'.$documento->getSerie().'</td>';
                                            echo '<td>'.$documento->getNumero().'</td>';
                                        
                                            echo '<td>'.$documento->getRazonsocial().'</td>';
                                            echo '<td>'.$documento->getRazonsialdestinatario().'</td>';
                                            echo '<td>'.$estado.'</td>';
                                            echo '<td><a  href="'.base_url.'documento/imprimirguia&id='.$documento->getId().'" target="_blank" data-toggle="tooltip" data-placement="top" title="PDF" style="background: none;"> <i class="material-icons">picture_as_pdf</i></a><button type ="text" style="border:none;background: none;" data-toggle="tooltip" data-placement="top" title="TICKET" onclick ="VentanaCentrada('."'".base_url.'documento/printticketguia&id='.$documento->getId()."'".','."'".'Ticket'."'".','."''".','."''".','."''".','."'false'".');">  <i class="material-icons">confirmation_number</i> </button> </td>';

//                                            echo '<td><a  href="'.base_url.'documento/imprimir&id='.$documento->getId().'" target="_blank" data-toggle="tooltip" data-placement="top" title="PDF" style="background: none;"> '
//                                                    . '<i class="material-icons">picture_as_pdf</i></a><button type ="text" style="border:none;background: none;" data-toggle="tooltip" data-placement="top" title="TICKET" '
//                                                    . 'onclick ="VentanaCentrada('."'".base_url.'documento/printticket&id='.$documento->getId()."'".','."'".'Ticket'."'".','."''".','."''".','."''".','."'false'".');">  '
//                                                    . '<i class="material-icons">confirmation_number</i> </button> </td>';
                                            echo '</tr>';
                                        
                                    } ?>



                                </tbody>
                               
                            </table>

            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 40
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });


                });


            //       $(function (){
            //        $('table tr:eq(0)').prepend('<th>ID</th>')
            //        var id=0;
            //        $('table tr:gt(0)').each(function (){
            //            id++
            //            $(this).prepend('<td>'+id+'</td>')
            //            
            //        })
            //       
            //       
            //       
            //       
            //       })


            </script>    


            <?php
        }
    }
    function searchcotizacion() {
//        var_dump($_POST);
        
        if (isset($_POST['dpdesde']) && isset($_POST['dphasta']) && isset($_POST['cbtipocomprobante']) && isset($_POST['txtbuscar']) && isset($_POST['txtserie']) && isset($_POST['txtnumero']) ) { // && isset($_POST['tipodoc'])
            
            if(isset($_POST['cbsucursal']) && permisos::rol('rol-vercotizacionesventasucur')){
                $sucursal = $_POST['cbsucursal'];
                
            }else if(permisos::rol('rol-vercotizacionesventa')){
                $sucursal = $_SESSION['idsucursal'];
                       
            }

            $desde = $_POST['dpdesde'];
            $dated = DateTime::createFromFormat('d/m/Y', $desde);
            $datedf = $dated->format('Y-m-d');

            $hasta = $_POST['dphasta'];
            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_POST['cbtipocomprobante'];
//            $tipodoc = $_POST['tipodoc'];

            $buscar = $_POST['txtbuscar'];
            $serie = $_POST['txtserie'];
            $numero = $_POST['txtnumero'];
            

            $documentos = $this->documento->select($datedf, $datehf, $tipocomp, 'Cotizacion', $buscar, $serie, $numero, $sucursal);

            ////// tabla ///// 
            ?>
            <table class="table  table-hover table-bordered" id="tabladocumento">
                <thead>
            
                        <tr>
                            <th>Fecha</th>
                            <th>Tipo</th>

                            <th>Número</th>
                            <th>RUC/ DNI</th>
                            <th>Nombre / Rz. Social</th>
                            <th>Moneda</th>
                            <th>Total</th>

                            <th>Imprimir</th>
                            <th>Acciones</th>
                        </tr>

                </thead>

                <tbody >
                    <?php
                    foreach ($documentos as $documento) {
                        $estados = '';
                        $estadol = '';
                        if ($documento->getEstadosunat() == 'Aceptado') {
                            $estados = '<span class="label bg-green">' . $documento->getEstadosunat() . '</span>';
                        } else {
                            $estados = '<span class="label bg-red">' . $documento->getEstadosunat() . '</span>';
                        }

                        if ($documento->getEstadolocal() == 'Aceptado') {
                            $estadol = '<span class="label bg-green">' . $documento->getEstadolocal() . '</span>';
                        } else {
                            $estadol = '<span class="label bg-red">' . $documento->getEstadolocal() . '</span>';
                        }

                            echo '<tr>';
                            echo '<td>' . $documento->getFechaemision() . '</td>';
                            echo '<td>' . $documento->getTipo() . '</td>';

                            echo '<td>' . $documento->getNumero() . '</td>';
                            echo '<td>' . $documento->getRuc() . '</td>';
                            echo '<td>' . $documento->getRazonsocial() . '</td>';
                            echo '<td>' . $documento->getMoneda() . '</td>';
                            echo '<td>' . $documento->getTotal() . '</td>';


                            echo '<td><a  href="' . base_url . 'documento/imprimircotizacion&id=' . $documento->getId() . '" target="_blank" data-toggle="tooltip" data-placement="top" title="PDF" style="background: none;"> <i class="material-icons">picture_as_pdf</i></a></td>';
                            echo '<td>'
                            . '<a href="' . base_url . 'documento/loadcotizacion&id='.$documento->getId().'"  data-toggle="tooltip" data-placement="top" title="VENDER"><i class="material-icons">add_shopping_cart</i></a> </div></td>';
                     

                        echo '</tr>';
                    }
                    ?>



                </tbody>

            </table>

            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 40
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });


                });


            //       $(function (){
            //        $('table tr:eq(0)').prepend('<th>ID</th>')
            //        var id=0;
            //        $('table tr:gt(0)').each(function (){
            //            id++
            //            $(this).prepend('<td>'+id+'</td>')
            //            
            //        })
            //       
            //       
            //       
            //       
            //       })


            </script>    


            <?php
        }
    }
    function searchordencompra() {
//        var_dump($_POST);
        
        if (isset($_POST['dpdesde']) && isset($_POST['dphasta']) && isset($_POST['cbtipocomprobante']) && isset($_POST['txtbuscar']) && isset($_POST['txtserie']) && isset($_POST['txtnumero'])  ) { //&& isset($_POST['tipodoc'])
            
            if(isset($_POST['cbsucursal']) && permisos::rol('rol-vercomprobanteordencomprasucur')){
                $sucursal = $_POST['cbsucursal'];
                
            }else if(permisos::rol('rol-vercomprobanteordencompra')){
                $sucursal = $_SESSION['idsucursal'];
                       
            }

            $desde = $_POST['dpdesde'];
            $dated = DateTime::createFromFormat('d/m/Y', $desde);
            $datedf = $dated->format('Y-m-d');

            $hasta = $_POST['dphasta'];
            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_POST['cbtipocomprobante'];
//            $tipodoc = $_POST['tipodoc'];

            $buscar = $_POST['txtbuscar'];
            $serie = $_POST['txtserie'];
            $numero = $_POST['txtnumero'];
            

            $documentos = $this->documento->select($datedf, $datehf, $tipocomp, 'orden_compra', $buscar, $serie, $numero, $sucursal);

            ////// tabla ///// 
            ?>
            <table class="table  table-hover table-bordered" id="tabladocumento">
                <thead>
            
                        <tr>
                            <th>Fecha</th>
                            <th>Tipo</th>

                            <th>Número</th>
                            <th>RUC/ DNI</th>
                            <th>Nombre / Rz. Social</th>
                            <th>Moneda</th>
                            <th>Total</th>

                            <th>Imprimir</th>
                            <th>Acciones</th>
                        </tr>
 
                </thead>
      
                <tbody >
                    <?php
                    foreach ($documentos as $documento) {
                        $estados = '';
                        $estadol = '';
                        if ($documento->getEstadosunat() == 'Aceptado') {
                            $estados = '<span class="label bg-green">' . $documento->getEstadosunat() . '</span>';
                        } else {
                            $estados = '<span class="label bg-red">' . $documento->getEstadosunat() . '</span>';
                        }

                        if ($documento->getEstadolocal() == 'Aceptado') {
                            $estadol = '<span class="label bg-green">' . $documento->getEstadolocal() . '</span>';
                        } else {
                            $estadol = '<span class="label bg-red">' . $documento->getEstadolocal() . '</span>';
                        }


                            echo '<tr>';
                            echo '<td>' . $documento->getFechaemision() . '</td>';
                            echo '<td>' . $documento->getTipo() . '</td>';

                            echo '<td>' . $documento->getNumero() . '</td>';
                            echo '<td>' . $documento->getRuc() . '</td>';
                            echo '<td>' . $documento->getRazonsocial() . '</td>';
                            echo '<td>' . $documento->getMoneda() . '</td>';
                            echo '<td>' . $documento->getTotal() . '</td>';

                            echo '<td><a  href="' . base_url . 'documento/imprimir&id=' . $documento->getId() . '" target="_blank" data-toggle="tooltip" data-placement="top" title="PDF" style="background: none;"> <i class="material-icons">picture_as_pdf</i></a><button type ="text" style="border:none;background: none;" data-toggle="tooltip" data-placement="top" title="TICKET" onclick ="VentanaCentrada(' . "'" . base_url . 'documento/printticket&id=' . $documento->getId() . "'" . ',' . "'" . 'Ticket' . "'" . ',' . "''" . ',' . "''" . ',' . "''" . ',' . "'false'" . ');">  <i class="material-icons">confirmation_number</i> </button> </td>';

                            echo '<td><div class="demo-google-material-icon"> '
                            . '<a href="' . base_url . 'documento/loadcompra&id=' . $documento->getId() . '" data-toggle="tooltip" data-placement="top" title="COMPRAR"><i class="material-icons">shopping_basket</i></a>';

                            echo '<a  href="' . base_url . 'documento/loadorden&id=' . $documento->getId() . '" data-toggle="tooltip" data-placement="top"  title="EDITAR"><i class="material-icons">create</i></a></div></td>';
                        

                        echo '</tr>';
                    }
                    ?>



                </tbody>

            </table>

            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 40
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });


                });




            </script>    


            <?php
        }
    }
    function searchcompra() {
//        var_dump($_POST);

        if (isset($_POST['dpdesde']) && isset($_POST['dphasta']) && isset($_POST['cbtipocomprobante']) && isset($_POST['txtbuscar']) && isset($_POST['txtserie']) && isset($_POST['txtnumero']) ) { //&& isset($_POST['tipodoc'])

            if(permisos::rol('rol-vercomprobantecomprasucur')  && isset($_POST['cbsucursal'])){
                $sucursal = $_POST['cbsucursal'];
            }elseif(permisos::rol('rol-vercomprobantecompra')){
                $sucursal = $_SESSION['idsucursal'];
            }
            $desde = $_POST['dpdesde'];
            $dated = DateTime::createFromFormat('d/m/Y', $desde);
            $datedf = $dated->format('Y-m-d');

            $hasta = $_POST['dphasta'];
            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_POST['cbtipocomprobante'];
//            $tipodoc = $_POST['tipodoc'];

            $buscar = $_POST['txtbuscar'];
            $serie = $_POST['txtserie'];
            $numero = $_POST['txtnumero'];
            

            $documentos = $this->documento->select($datedf, $datehf, $tipocomp, 'Compra', $buscar, $serie, $numero, $sucursal);

            ////// tabla ///// 
            ?>
            <table class="table  table-hover table-bordered" id="tabladocumento">
                <thead>
       

                        <tr>
                            <th>Fecha</th>
                            <th>Tipo</th>
                            <th>Serie</th>
                            <th>Número</th>
                            <th>RUC/ DNI</th>
                            <th>Nombre / Rz. Social</th>
                            <th>Moneda</th>
                            <th>Total</th>

                            <th>Imprimir</th>
                            <th>Acciones</th>
                        </tr>
            
                </thead>
            <!--                                <tfoot>
                    <tr>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Serie</th>
                        <th>Número</th>
                        <th>RUC/ DNI</th>
                        <th>Nombre / Rz. Social</th>
                        <th>Total</th>
                        <th>Est. Local</th>
                        <th>Est. Sunat</th>
                        <th>Imprimir</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>-->
                <tbody >
                    <?php
                    foreach ($documentos as $documento) {
                        $estados = '';
                        $estadol = '';
                        if ($documento->getEstadosunat() == 'Aceptado') {
                            $estados = '<span class="label bg-green">' . $documento->getEstadosunat() . '</span>';
                        } else {
                            $estados = '<span class="label bg-red">' . $documento->getEstadosunat() . '</span>';
                        }

                        if ($documento->getEstadolocal() == 'Aceptado') {
                            $estadol = '<span class="label bg-green">' . $documento->getEstadolocal() . '</span>';
                        } else {
                            $estadol = '<span class="label bg-red">' . $documento->getEstadolocal() . '</span>';
                        }


                            echo '<tr>';
                            echo '<td>' . $documento->getFechaemision() . '</td>';
                            echo '<td>' . $documento->getTipo() . '</td>';
                            echo '<td>' . $documento->getSerie() . '</td>';
                            echo '<td>' . $documento->getNumero() . '</td>';
                            echo '<td>' . $documento->getRuc() . '</td>';
                            echo '<td>' . $documento->getRazonsocial() . '</td>';
                            echo '<td>' . $documento->getMoneda() . '</td>';
                            echo '<td>' . $documento->getTotal() . '</td>';



                            echo '<td><a  href="' . base_url . 'documento/imprimir&id=' . $documento->getId() . '" target="_blank" data-toggle="tooltip" data-placement="top" title="PDF" style="background: none;"> <i class="material-icons">picture_as_pdf</i></a><button type ="text" style="border:none;background: none;" data-toggle="tooltip" data-placement="top" title="TICKET" onclick ="VentanaCentrada(' . "'" . base_url . 'documento/printticket&id=' . $documento->getId() . "'" . ',' . "'" . 'Ticket' . "'" . ',' . "''" . ',' . "''" . ',' . "''" . ',' . "'false'" . ');">  <i class="material-icons">confirmation_number</i> </button> </td>';

                            echo '<td><div class="demo-google-material-icon"> ';

                            echo '<a  href="' . base_url . 'documento/loadcompraedit&id=' . $documento->getId() . '" data-toggle="tooltip" data-placement="top"  title="EDITAR"><i class="material-icons">create</i></a></div></td>';
                         

                        echo '</tr>';
                    }
                    ?>



                </tbody>

            </table>

            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 40
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });


                });


            //       $(function (){
            //        $('table tr:eq(0)').prepend('<th>ID</th>')
            //        var id=0;
            //        $('table tr:gt(0)').each(function (){
            //            id++
            //            $(this).prepend('<td>'+id+'</td>')
            //            
            //        })
            //       
            //       
            //       
            //       
            //       })


            </script>    


            <?php
        }
    }

    function searchdetallado() {
//        var_dump($_POST);

        if (isset($_POST['dpdesde']) && isset($_POST['dphasta']) && isset($_POST['cbtipocomprobante']) && isset($_POST['txtbuscar']) && isset($_POST['txtserie']) && isset($_POST['txtnumero'])  && isset($_POST['cbmoneda'])) {

            if(permisos::rol('rol-ventadetallereportesucur') && isset($_POST['cbsucursal'])){
                $sucursal = $_POST['cbsucursal'];
            }elseif(permisos::rol('rol-ventadetallereporte')){
                $sucursal = $_SESSION['idsucursal'];
            }
            $desde = $_POST['dpdesde'];
            $dated = DateTime::createFromFormat('d/m/Y', $desde);
            $datedf = $dated->format('Y-m-d');

            $hasta = $_POST['dphasta'];
            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_POST['cbtipocomprobante'];

            $moneda = $_POST['cbmoneda'];
            $buscar = $_POST['txtbuscar'];
            $serie = $_POST['txtserie'];
            $numero = $_POST['txtnumero'];
            

            $detallado = $this->documento->selectdetallado($datedf, $datehf, $tipocomp, 'Venta', $buscar, $serie, $numero, $moneda, $sucursal);

            ////// tabla ///// 
            ?>
            <table class="table  table-hover table-bordered" id="tabladocumento">
                <thead>
                    <tr>
                        <th>Documento</th>
                        <th>Serie / Num</th>
                        <th>Fecha</th>
                        <th>Nombre / Rz. Social</th>
                        <th>Vendedor</th>
                        <th>Tipo pago</th>
                        <th>Descripción</th>
                        <th>Total</th>
                        <th>Inc. Igv</th>
                        <th>Tipo Igv</th>
                        <th>Estado</th>
                    </tr>
                </thead>
            <!--                                <tfoot>
                    <tr>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Serie</th>
                        <th>Número</th>
                        <th>RUC/ DNI</th>
                        <th>Nombre / Rz. Social</th>
                        <th>Total</th>
                        <th>Est. Local</th>
                        <th>Est. Sunat</th>
                        <th>Imprimir</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>-->
                <tbody >
                    <?php
                    $total = 0;
                    $estado = '';
                    foreach ($detallado as $detalle) {
                        if($detalle['estadolocal']== 'Anulado'){
                             $estado= '<div class="label bg-red">'.$detalle['estadolocal'].'</div>';
                        }else {
                            $estado = '';
                            $total += $detalle['total'];
                        }

                        echo '<tr>';

                        echo '<td>' . $detalle['tipo'] . '</td>';
                        echo '<td>' . $detalle['serien'] . '</td>';
                        echo '<td>' . $detalle['fechaemision'] . '</td>';
                        echo '<td>' . $detalle['razonsocial'] . '</td>';
                        echo '<td>' . $detalle['vendedor'] . '</td>';
                        echo '<td>' . $detalle['tipo_pago'] . '</td>';
                        echo '<td>' . $detalle['descripcionprod'] . '</td>';
                        echo '<td>' . number_format($detalle['total'], 2) . '</td>';
                        echo '<td>' . $detalle['incigv'] . '</td>';
                        echo '<td>' . $detalle['impuesto'] . '</td>';
                        echo '<td>' . $estado . '</td>';

                        echo '</tr>';

                        
                    }


                    echo '<tr>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td colspan="2" class="h4"><strong>Total</strong></td>';
//                                            echo '<td></td>';
                    echo '<td>' . number_format($total, 2) . '</td>';
                    echo '</tr>';
                    ?>




                </tbody>

            </table>

            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 40
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });


                });




            </script>    


            <?php
        }
    }

    function searchdetalladocompra() {
//        var_dump($_POST);

        if (isset($_POST['dpdesde']) && isset($_POST['dphasta']) && isset($_POST['cbtipocomprobante']) && isset($_POST['txtbuscar']) && isset($_POST['txtserie']) && isset($_POST['txtnumero']) && isset($_POST['cbsucursal']) && isset($_POST['cbmoneda'])) {
            if(permisos::rol('rol-compradetallereportesucur') && isset($_POST['cbsucursal'])){
                $sucursal = $_POST['cbsucursal'];
            }elseif(permisos::rol('rol-compradetallereporte')) {
                $sucursal = $_SESSION['idsucursal'];
            }

            $desde = $_POST['dpdesde'];
            $dated = DateTime::createFromFormat('d/m/Y', $desde);
            $datedf = $dated->format('Y-m-d');

            $hasta = $_POST['dphasta'];
            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_POST['cbtipocomprobante'];

            $moneda = $_POST['cbmoneda'];
            $buscar = $_POST['txtbuscar'];
            $serie = $_POST['txtserie'];
            $numero = $_POST['txtnumero'];
            

            $detallado = $this->documento->selectdetallado($datedf, $datehf, $tipocomp, 'Compra', $buscar, $serie, $numero, $moneda, $sucursal);

            ////// tabla ///// 
            ?>
            <table class="table  table-hover table-bordered" id="tabladocumento">
                <thead>
                    <tr>
                        <th>Documento</th>
                        <th>Serie / Num</th>
                        <th>Fecha</th>
                        <th>Nombre / Rz. Social</th>

                        <th>Tipo pago</th>
                        <th>Descripción</th>
                        <th>Total</th>
                        <th>Inc. Igv</th>
                        <th>Tipo Igv</th>
                    </tr>
                </thead>
            <!--                                <tfoot>
                    <tr>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Serie</th>
                        <th>Número</th>
                        <th>RUC/ DNI</th>
                        <th>Nombre / Rz. Social</th>
                        <th>Total</th>
                        <th>Est. Local</th>
                        <th>Est. Sunat</th>
                        <th>Imprimir</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>-->
                <tbody >
                    <?php
                    $total = 0;
                    foreach ($detallado as $detalle) {

                        echo '<tr>';

                        echo '<td>' . $detalle['tipo'] . '</td>';
                        echo '<td>' . $detalle['serien'] . '</td>';
                        echo '<td>' . $detalle['fechaemision'] . '</td>';
                        echo '<td>' . $detalle['razonsocial'] . '</td>';

                        echo '<td>' . $detalle['tipo_pago'] . '</td>';
                        echo '<td>' . $detalle['descripcionprod'] . '</td>';
                        echo '<td>' . number_format($detalle['total'], 2) . '</td>';
                        echo '<td>' . $detalle['incigv'] . '</td>';
                        echo '<td>' . $detalle['impuesto'] . '</td>';

                        echo '</tr>';

                        $total += $detalle['total'];
                    }


                    echo '<tr>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';

                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td></td>';
                    echo '<td colspan="2" class="h4"><strong>Total</strong></td>';
//                                            echo '<td></td>';
                    echo '<td>' . number_format($total, 2) . '</td>';
                    echo '</tr>';
                    ?>




                </tbody>

            </table>

            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 20
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });


                });




            </script>    


            <?php
        }
    }

    function loaddebit() {
//        var_dump($_GET);
        require_once 'view/layout/header.php';
        if (isset($_GET['id']) && !empty($_GET['id'])) {


            $id = $_GET['id'];
            $idsucur = $_SESSION['idsucursal'];
            $detallesmod = new Detalle();
            $seriedet = new serieDetalle();
            $documentossuc = $this->docsucursal->selectAll($idsucur, 'nota_debito');
            $transactions = $this->sunattrans->selectAll();
            $usuarios = $this->usuario->selectAll();
            $impuestos = $this->impuesto->selectAll();
            $unidades = $this->unidad->selectAll();

            $nota = new tiponota();
            $notas = $nota->select('debito');
            
            $tipocambio = new tipocambio();
            $utimotc = $tipocambio->selectMax();
            $ventatc = $utimotc->getVenta();

            ///////////// carga el documento deseado (cabecera )///////////
            $documento = $this->documento->selectOne($id);
            // carga detalle del domento ///////
            $detalles = $detallesmod->selectOneDoc($id);
            $tipodocp = new personaTipoDocumento();
            $docs = $tipodocp->selectAll();
            $tipo = 'nota_debito';
            $titulo = "Emitir nota de débito electrónica";

            require_once 'view/documentocabecera/nota/form_documento_note.php';


            require_once 'view/layout/footer.php';
        } else {


            require_once 'view/error.php';


            require_once 'view/layout/footer.php';
        }
    }

    function loadcompra() {
//        var_dump($_GET);
        require_once 'view/layout/header.php';
        if (isset($_GET['id']) && !empty($_GET['id'])) {


            $id = $_GET['id'];

            $seriedet = new serieDetalle();
            $transactions = $this->sunattrans->selectAll();
            $usuarios = $this->usuario->selectAll();
            $impuestos = $this->impuesto->selectAll();
            $unidades = $this->unidad->selectAll();
            $tipod = new personaTipoDocumento();
            

            $tipocambio = new tipocambio();
            $cambio = $tipocambio->selectMax();
            $ventatc = $cambio->getCompra();
            
            
            $tipo = 'Compra';
            $detallem = new Detalle();
            $detalles = $detallem->selectOneDoc($id);

            $documento = $this->documento->selectOne($id);
            $docs = $tipod->selectAll();

            require_once 'view/documentocabecera/compra/form_documento_compra.php';


            require_once 'view/layout/footer.php';
        } else {


            require_once 'view/error.php';


            require_once 'view/layout/footer.php';
        }
    }

    function loadcompraedit() {
//        var_dump($_GET);
        require_once 'view/layout/header.php';
        if (isset($_GET['id']) && !empty($_GET['id'])) {


            $id = $_GET['id'];

            $tipodocp = new personaTipoDocumento();
            $seriedet = new serieDetalle();
            $transactions = $this->sunattrans->selectAll();
            $usuarios = $this->usuario->selectAll();
            $impuestos = $this->impuesto->selectAll();
            $unidades = $this->unidad->selectAll();
            
            $tipocambio = new tipocambio();
            $cambio = $tipocambio->selectMax();
            $ventatc = $cambio->getCompra();

            

            $detallem = new Detalle();
            $detalles = $detallem->selectOneDoc($id);

            $documento = $this->documento->selectOne($id);
            $docs = $tipodocp->selectAll();
            
            require_once 'view/documentocabecera/compra/form_documento_compra_editar.php';


            require_once 'view/layout/footer.php';
        } else {


            require_once 'view/error.php';


            require_once 'view/layout/footer.php';
        }
    }

    function loadcotizacion() {
//        var_dump($_GET);

        if (isset($_GET['id']) && !empty($_GET['id'])) {

            require_once 'view/layout/header.php';
            $id = $_GET['id'];
            
            
            if(permisos::rol('rol-boletafacturaventa')){
            $caja = new caja();

             if($caja->status(date('Y-m-d')) == false){
             require_once 'view/operaciones/apertura_caja.php';
             
            } else  {
                
            $sucursaldocm = new documentoSucursal();
            $tipodocventa = $sucursaldocm->selectTipoDocVenta();
            
            $tipocambio = new tipocambio();
            $utimotc = $tipocambio->selectMax();
            $ventatc = $utimotc->getVenta();
            
            $seriesdoc = array();
            $op = "";
            if(count($tipodocventa)>0){
                 $op = $tipodocventa[0]->getTipodoc();

            }
            $seriesdoc = $sucursaldocm->selectAll($_SESSION['idsucursal'], $op);
   //         var_dump($seriesdoc);

            $seriepredeterminado = "";
            if(count($seriesdoc) > 0){
               $seriepredeterminado = $seriesdoc[0]->getSerie(); 
            }
   //         var_dump($seriepredeterminado);
            ////////////// numero de documento predeterminado ////////////
            $documentomaxnumeromodel = $this->documento->selectMax('Venta', $op, $seriepredeterminado);
           $numeror = str_replace(" ", "", $documentomaxnumeromodel->getNumero());
           $numero = (int) $numeror + 1;
            /////////////////////////////////////////////////////////////

            $detallesmod = new Detalle();
            $seriedet = new serieDetalle();
            $transactions = $this->sunattrans->selectAll();
            $usuarios = $this->usuario->selectAll();
            $impuestos = $this->impuesto->selectAll();
            $unidades = $this->unidad->selectAll();
            $seriesm = new serieProducto();
//            $series = $seriesm->select(74);
            $sucurm = new sucursal();
            $sucursales = $sucurm->selectAll();
            $tipodoc = 'Venta';

            ///////////// carga el documento deseado (cabecera )///////////
            $documento = $this->documento->selectOne($id);
//            var_dump($documento);
            // carga detalle del domento ///////
            $detalles = $detallesmod->selectOneDoc($id);
            
            

//            $tipo = 'nota_debito';
//            $titulo = "Emitir nota de débito electrónica";
            $tipodocp = new personaTipoDocumento();
            $docs = $tipodocp->selectAll();
            require_once 'view/documentocabecera/form_documento.php';
            require_once 'view/documentosucursal/modalnewdocumentosucursal.php';
            }
            require_once 'view/layout/footer.php';
            
            } else {

                require_once 'view/sinpermiso.php';
            }
        }else {


            require_once 'view/error.php';


            require_once 'view/layout/footer.php';
        }
    }

    function loadcredit() {
//        var_dump($_GET);
        require_once 'view/layout/header.php';
        if (isset($_GET['id']) && !empty($_GET['id'])) {


            $id = $_GET['id'];
            $idsucur = $_SESSION['idsucursal'];
            $detallesmod = new Detalle();
            $seriedet = new serieDetalle();
            $documentossuc = $this->docsucursal->selectAll($idsucur, 'nota_credito');
            $transactions = $this->sunattrans->selectAll();
            $usuarios = $this->usuario->selectAll();
            $impuestos = $this->impuesto->selectAll();
            $unidades = $this->unidad->selectAll();

            $nota = new tiponota();
            $notas = $nota->select('credito');

            ///////////// carga el documento deseado (cabecera )///////////
            $documento = $this->documento->selectOne($id);
            // carga detalle del domento ///////
            $detalles = $detallesmod->selectOneDoc($id);
            $tipodocp = new personaTipoDocumento();
            $docs = $tipodocp->selectAll();
            
            $tipocambio = new tipocambio();
            $utimotc = $tipocambio->selectMax();
            $ventatc = $utimotc->getVenta();
            
            $tipo = 'nota_credito';
            $titulo = "Emitir nota de crédito electrónica";

            require_once 'view/documentocabecera/nota/form_documento_note.php';


            require_once 'view/layout/footer.php';
        } else {


            require_once 'view/error.php';


            require_once 'view/layout/footer.php';
        }
    }

    function loadorden() {
//        var_dump($_GET);
        require_once 'view/layout/header.php';
        if (isset($_GET['id']) && !empty($_GET['id'])) {


            $id = $_GET['id'];
//            $idsucur = $_SESSION['idsucursal'];
            $detallesmod = new Detalle();
            $seriedet = new serieDetalle();
            $transactions = $this->sunattrans->selectAll();
            $usuarios = $this->usuario->selectAll();
            $impuestos = $this->impuesto->selectAll();
            $unidades = $this->unidad->selectAll();


            ///////////// carga el documento deseado (cabecera )///////////
            $documento = $this->documento->selectOne($id);
            // carga detalle del domento ///////
            $detalles = $detallesmod->selectOneDoc($id);
            
            $tipodocp = new personaTipoDocumento();
            $docs = $tipodocp->selectAll();
            
             $tipocambio = new tipocambio();
            $cambio = $tipocambio->selectMax();
            $ventatc = $cambio->getCompra();

//            $tipo = 'nota_credito';
//            $titulo = "Emitir nota de crédito electrónica";

            require_once 'view/documentocabecera/ordencompra/form_documento_ordcompra_edit.php';


            require_once 'view/layout/footer.php';
        } else {


            require_once 'view/error.php';


            require_once 'view/layout/footer.php';
        }
    }

    function ordencompra() {
        require_once 'view/layout/header.php';
//        $documentossuc = $this->docsucursal->select();
        
        if(permisos::rol('rol-ordencompra')){
            $transactions = $this->sunattrans->selectAll();
            $usuarios = $this->usuario->selectAll();
            $impuestos = $this->impuesto->selectAll();
            $unidades = $this->unidad->selectAll();

//            $tipocambio = new tipocambio();
//            $cambio = $tipocambio->selectMax();
            
            $tipocambio = new tipocambio();
            $utimotc = $tipocambio->selectMax();
            $ventatc = $utimotc->getCompra();

            $detalles = array();
            $tipodoc = 'orden_compra';
            $tipodocp = new personaTipoDocumento();
            $docs = $tipodocp->selectAll();
            
            $nro = $this->documento->selectMax($tipodoc, $tipodoc, 'ORDEN_COMPRA')->getNumero() + 1;


            require_once 'view/documentocabecera/ordencompra/form_documento_ordcompra.php';
            
        }else {
            require_once 'view/sinpermisos.php';
        }
        


        require_once 'view/layout/footer.php';
    }

    function cotizacion() {
        require_once 'view/layout/header.php';
//        $documentossuc = $this->docsucursal->select();
        
        if(permisos::rol('rol-cotizacionventa')){
        $transactions = $this->sunattrans->selectAll();
        $usuarios = $this->usuario->selectAll();
        $impuestos = $this->impuesto->selectAll();
        $unidades = $this->unidad->selectAll();

        $tipocambio = new tipocambio();
        $cambio = $tipocambio->selectMax();
    
        $ventatc = $cambio->getVenta();

        $detalles = array();
        $tipodoc = 'cotizacion';
        $nro = $this->documento->selectMax($tipodoc, $tipodoc, 'COTIZACION')->getNumero() + 1;

        $tipodocp = new personaTipoDocumento();
        $docs = $tipodocp->selectAll();
        require_once 'view/documentocabecera/cotizacion/form_documento_cotizacion.php';
        }else {
            require_once 'view/sinpermiso.php';
            
        }

        require_once 'view/layout/footer.php';
    }

    function compra() {
        require_once 'view/layout/header.php';
//        $documentossuc = $this->docsucursal->selectAll();
        if(permisos::rol('rol-compra')){
            
            $caja = new caja();
       
         if($caja->status(date('Y-m-d')) == false){
             require_once 'view/operaciones/apertura_caja.php';
             
         }else {
            $personam = new persona();
            $tipod = new personaTipoDocumento();
            $transactions = $this->sunattrans->selectAll();
            $usuarios = $this->usuario->selectAll();
            $impuestos = $this->impuesto->selectAll();
            $unidades = $this->unidad->selectAll();

            $tipocambio = new tipocambio();
            $cambio = $tipocambio->selectMax();
            $ventatc = $cambio->getCompra();
            
            $tipo = 'Compra';
            $detalles = array();
            $documento = new documento();
            $personabydefault = $personam->bydefault(2);
            $docs = $tipod->selectAll();
            
            require_once 'view/documentocabecera/compra/form_documento_compra.php';
         }
            
        }else {
            require_once 'view/sinpermiso.php';
        }
        


        require_once 'view/layout/footer.php';
    }

    function insertcompra() {

//        var_dump($_POST);
        if(permisos::rol('rol-compra')){
        $id = 0;
        
        if (isset($_POST['tipodoc']) && isset($_POST['txtserie']) && isset($_POST['txtnro']) && isset($_POST['cbmoneda']) && isset($_POST['incigv']) &&
                isset($_POST['dpfechaemision']) && isset($_POST['dpfechavencimiento'])  && isset($_POST['txtrucbuscar']) &&
                isset($_POST['txtcliente']) && isset($_POST['txtdireccion']) && isset($_POST['txtemail']) && isset($_POST['txtordenc']) && isset($_POST['txtobservacion']) &&
                !empty($_POST['tipodoc']) && !empty($_POST['txtserie']) && !empty($_POST['txtnro']) && !empty($_POST['cbmoneda']) && !empty($_POST['dpfechaemision']) && 
                !empty($_POST['dpfechavencimiento'])  && !empty($_POST['txtrucbuscar']) && !empty($_POST['txtcliente']) && isset($_POST['id'])
                && isset($_POST['gratuita']) && isset($_POST['exonerado']) && isset($_POST['inafecto']) && isset($_POST['exportacion']) && isset($_POST['gravada']) 
                && isset($_POST['igv']) && isset($_POST['totaldoc'])  && isset($_POST['cbtipoventa']) 
                && !empty($_POST['cbtipoventa']) && isset($_POST['cbtipodocpersona']) && !empty($_POST['cbtipodocpersona']) ) { //&& !empty($_POST['totaldoc']) && !empty($_POST['gravada']) && !empty($_POST['igv']) && isset($_POST['txttipocambio'])&& !empty($_POST['txttipocambio'])
            $serier = str_replace(" ", "", $_POST['txtserie']);
            $numeror = str_replace(" ", "", $_POST['txtnro']);

            $serie = ltrim($serier, '0');
            $numero = (int) ($numeror);

            if ($this->documento->duplicado($serie, $numero, 'Compra') == 'valido') {
                $emision = trim($_POST['dpfechaemision']);

                $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
                $emisionf = $dateemis->format('Y-m-d');


                $vencimiento = trim($_POST['dpfechavencimiento']);
                $dateven = DateTime::createFromFormat('d/m/Y', $vencimiento);
                $vencimientof = $dateven->format('Y-m-d');


                $gratuita = 0;
                $exonerado = 0;
                $inafecto = 0;
                $exportacion = 0;
                
                $totaldoc = 0;
                $igvdoc = 0;
                $gravada= 0;
                if (!empty($_POST['totaldoc'])) {
                    $totaldoc = $_POST['totaldoc'];
                }
                if (!empty($_POST['gravada'])) {
                    $gravada = $_POST['gravada'];
                }
                if (!empty($_POST['igv'])) {
                    $igvdoc = $_POST['igv'];
                }


                if (!empty($_POST['gratuita'])) {
                    $gratuita = $_POST['gratuita'];
                }
                if (!empty($_POST['exonerado'])) {
                    $exonerado = $_POST['exonerado'];
                }
                if (!empty($_POST['inafecto'])) {
                    $inafecto = $_POST['inafecto'];
                }
                if (!empty($_POST['exportacion'])) {
                    $exportacion = $_POST['exportacion'];
                }

                if (isset($_POST['incigv'])) {
                    $incigv = 1;
                } else {
                    $incigv = 0;
                }

                if (!empty($_POST['cbsujetoa'])) {
                    $sujetoa = $_POST['cbsujetoa'];
                } else {
                    $sujetoa = 0;
                }


                $documento = new documento();

//                $gravada = $_POST['gravada'];
//                $totaldoc = $_POST['totaldoc'];
//                $igvdoc = $_POST['igv'];
//            $serie = $_POST['txtserie'];
//            $numero = $_POST['txtnro'];
                $moneda = $_POST['cbmoneda'];
                $fechaemision = $emisionf;
                $fechavencimiento = $vencimientof;
                $tipodoc = $_POST['tipodoc'];
                $tipocompra = $_POST['cbtipoventa'];

                $idpersona = $_POST['idcliente'];
                if (empty($idpersona)) {
                    $idpersona = 0;
                }

                $idusuario = $_SESSION['id'];
                
                list($personatipodoc,$idpersonatipodoc) = explode('-',$_POST['cbtipodocpersona']);
                
//                $personatipodoc = $_POST['cbtipodocpersona'];
                $ruc = $_POST['txtrucbuscar'];
                $razonsocial = $_POST['txtcliente'];
                $direccion = $_POST['txtdireccion'];
                $email = $_POST['txtemail'];
                $norden = $_POST['txtordenc'];
                $observacion = $_POST['txtobservacion'];
                $idsucursal = $_SESSION['idsucursal'];
                $tipocambio = 0.00; //$_POST['txttipocambio']
                
                
                $personam = new persona();
                $personam->setIdtipodocumento($idpersonatipodoc);
                $personam->setRuc($ruc);
                $personam->setNombre($razonsocial);
                $personam->setDireccion($direccion);
                $personam->setEmail($email);
                $personam->setAnivcumplenos('0000-00-00');
                $personam->setTipopersona(2);
                
                
                if($idpersona == 0){
                    
                    $personam->insert($personam);
                    
                }else {
                    $personam->setId($idpersona);
                    $personam->updatedoc($personam);
                    
                }
                 $caja = new caja();
                $cajault = $caja->ultimo();

                $documento->setSerie($serie);
                $documento->setNumero($numero);
                $documento->setMoneda($moneda);
                $documento->setFechaemision($fechaemision);
                $documento->setFechavencimiento($fechavencimiento);
                $documento->setTipodoc('Compra');
                $documento->setIdusuario($idusuario);
                $documento->setIdpersona($idpersona);
                $documento->setRuc($ruc);
                $documento->setRazonsocial($razonsocial);
                $documento->setDireccion($direccion);
                $documento->setEmail($email);
                $documento->setNorden($norden);
                $documento->setObservacion($observacion);
                $documento->setIdsucursal($idsucursal);
                $documento->setTipocambio($tipocambio);
                $documento->setTipo($tipodoc);
                $documento->setSujetoa($sujetoa);
                $documento->setIncigv($incigv);
                $documento->setGarantia(0);
                $documento->setValidezdias(0);
                $documento->setPlazoentregadias(0);
                $documento->setCondicionpagodias(0);
                $documento->setMontogratuita($gratuita);
                $documento->setMontoexonerado($exonerado);
                $documento->setMontoinafecto($inafecto);
                $documento->setMontoexportacion($exportacion);
                $documento->setMontogravada($gravada);
                $documento->setMontoigv($igvdoc);
                $documento->setTotal($totaldoc);
                $documento->setTipoventa($tipocompra);
                $documento->setPersonatipodoc($personatipodoc);
                $documento->setIdcaja($cajault->getId());




                $id = $documento->insert($documento);

                $idprod = $_POST['id'];
                $codigo = $_POST['codigo'];
                $descripcion = $_POST['descripcionprod'];
                $unidad = $_POST['unidad'];
                $tipoigv = $_POST['tipoigv'];
                $cantidad = $_POST['cantidad'];
                $precio = $_POST['precio'];
                $subtotal = $_POST['subtotal'];
                $total = $_POST['total'];

                $igvprod = $_POST['igvprod'];
                $valorunitref = $_POST['valorunitref'];
                $montobaseigv = $_POST['montobaseigv'];
                $montobaseexpo = $_POST['montobaseexpo'];
                $montobaseexonerado = $_POST['montobaseexonerado'];
                $montobaseinafecto = $_POST['montobaseinafecto'];
                $montobasegratuito = $_POST['montobasegratuito'];
                $montobaseivap = $_POST['montobaseivap'];
                $montobaseotrostributos = $_POST['montobaseotrostributos'];
                $tributoventagratuita = $_POST['tributoventagratuita'];
                $otrostributos = $_POST['otrostributos'];
                $porcentajeigv = $_POST['porcentajeigv'];
                $porcentajeotrostributos = $_POST['porcentajeotrostributos'];
                $porcentajetributoventagratuita = $_POST['porcentajetributoventagratuita'];
                $montooriginal = $_POST['montooriginal'];
                $monedaoriginal = $_POST['monedaoriginal'];
                $incluye = $_POST['incluye'];

                $detalles = array();
                $produpdate = array();  //////////// array de productos actualizar stock
                for ($i = 0; $i < count($codigo); $i++) {
                    $idpro = $idprod[$i];
                    $igvp = $igvprod[$i];
                    $valorunitr = $valorunitref[$i];
                    $montobaseig = $montobaseigv[$i];
                    $montobaseexp = $montobaseexpo[$i];
                    $montobaseexon = $montobaseexonerado[$i];
                    $montobaseinaf = $montobaseinafecto[$i];
                    $montobasegratu = $montobasegratuito[$i];
                    $montobaseiv = $montobaseivap[$i];
                    $montobaseotrostrib = $montobaseotrostributos[$i];
                    $tributoventagrat = $tributoventagratuita[$i];
                    $otrostrib = $otrostributos[$i];
                    $porceigv = $porcentajeigv[$i];
                    $porcotrostrib = $porcentajeotrostributos[$i];
                    $porcentajetribventgrat = $porcentajetributoventagratuita[$i];
                    $montoorig = $montooriginal[$i];
                    if (empty($idprod[$i])) {
                        $idpro = 0;
                    }
                    if (empty($igvprod[$i])) {
                        $igvp = 0;
                    }
                    if (empty($valorunitref[$i])) {
                        $valorunitr = 0;
                    }
                    if (empty($montobaseigv[$i])) {
                        $montobaseig = 0;
                    }
                    if (empty($montobaseexpo[$i])) {
                        $montobaseexp = 0;
                    }
                    if (empty($montobaseexonerado[$i])) {
                        $montobaseexon = 0;
                    }
                    if (empty($montobaseinafecto[$i])) {
                        $montobaseinaf = 0;
                    }
                    if (empty($montobasegratuito[$i])) {
                        $montobasegratu = 0;
                    }
                    if (empty($montobaseivap[$i])) {
                        $montobaseiv = 0;
                    }
                    if (empty($montobaseotrostributos[$i])) {
                        $montobaseotrostrib = 0;
                    }
                    if (empty($tributoventagratuita[$i])) {
                        $tributoventagrat = 0;
                    }
                    if (empty($otrostributos[$i])) {
                        $otrostrib = 0;
                    }
                    if (empty($porcentajeigv[$i])) {
                        $porceigv = 0;
                    }
                    if (empty($porcentajeotrostributos[$i])) {
                        $porcotrostrib = 0;
                    }
                    if (empty($porcentajetributoventagratuita[$i])) {
                        $porcentajetribventgrat = 0;
                    }
                    if (empty($montooriginal[$i])) {
                        $montoorig = 0;
                    }
                    $d = array(
                        $idpro,
                        $codigo[$i],
                        $descripcion[$i],
                        $unidad[$i],
                        $tipoigv[$i],
                        $cantidad[$i],
                        $precio[$i],
                        $subtotal[$i],
                        $total[$i],
                        $id,
                        $igvp,
                        $valorunitr,
                        $montobaseig,
                        $montobaseexp,
                        $montobaseexon,
                        $montobaseinaf,
                        $montobasegratu,
                        $montobaseiv,
                        $montobaseotrostrib,
                        $tributoventagrat,
                        $otrostrib,
                        $porceigv,
                        $porcotrostrib,
                        $porcentajetribventgrat,
                        $montoorig,
                        $monedaoriginal[$i],
                        $incluye[$i],0
                    );
                    array_push($detalles, $d);
//
//                    $produp = array(
//                        $cantidad[$i],
//                        $idpro
//                    );
//                    array_push($produpdate, $produp);
                }

                $detalle = new Detalle();
                $producto = new producto();
                
                $iddetalle = $detalle->insert($detalles);
//                $producto->updatestock($produpdate);
                
              $kardexm = new kardex();
               $kardexs = array();
               $idkardex = array();
               $idkardexantc = array();
               $cantini0 = 0;
                for ($j = 0; $j < count($codigo); $j++) {
                    $idp =null;
                    if(!empty($idprod[$j])){
                        $idp = $idprod[$j];
                    }
                    $kard = $kardexm->ultimomovimientoproducto($idp);

                    array_push($idkardexantc, $kard->getId());
                    $cadenaserieant = '';
                    $cadenaseriecompra = '';

                    if (isset($_POST['serieprod']) && isset($_POST['serieidprod'])) {
                        $idprodseriek = $_POST['serieidprod'];
                        $seriek = $_POST['serieprod'];
                        
                     //   echo 'seriescompra';
                       // var_dump($seriek);
                        $cant0 = $cantidad[$j]; /// recojo la cantida de series a incluir en el item
//                    echo 'cantidad '.$cant;
                        $c = 0;
                        if ($incluye[$j] == 'Si') {
                            for ($l = $cantini0; $l < $cant0 + $cantini0; $l++) { //////////// series de cada item q incluir = si                           //recorro las series 
                                if($c > 0){
                                    $cadenaseriecompra .= ' , ';

                                }

                                $cadenaseriecompra .= strtoupper($seriek[$l]);
                                $c ++;
                            }

                            $cantini0 += $cant0;


                                 $seriem  = new serieProducto();
                                $seriesk = $seriem->select($kard->getId());
                                $contsk = 0;
                                foreach($seriesk as $k){
                                    if($contsk > 0){
                                        $cadenaserieant .= ' , ';
        
                                    }
        
                                    $cadenaserieant .= $k->getSerie();
                                    $contsk ++;
                                }
                        }
                         //   if($incluye[$j] == 'Si'){
                           
        
        
        
                          //  }

                    }


                   
                    
                    
                    
                    $kardex = array(
                        $iddetalle[$j],
                        'Compra',
                        $cantidad[$j],
                        $precio[$j],
                        $idp,
                        ($kard->getStockactual()) + $cantidad[$j],
                        $kard->getStockactual(),
                        date('Y-m-d H:i:s'),
                        $_SESSION['idalmacen'],
                        $_SESSION['idempresa'],
                        $tipodoc." (".$serie."-".$numero.")",
                        $moneda,
                        $descripcion[$j],
                        $cadenaserieant,
                        $cadenaseriecompra
                        
                    );
                    array_push($kardexs, $kardex);
                }
                
              //  var_dump($kardexs);
                
                $idkardex =$kardexm->insert($kardexs);


                if (isset($_POST['serieprod']) && isset($_POST['serieidprod'])) { //&& isset($_POST['idserie'])
//            var_dump($_POST['idserieitem']);
//            $idserie = $_POST['idserieitem'];
                    $idprodserie = $_POST['serieidprod'];
                    $seriekp = $_POST['serieprod'];
                    $seriem = new serieProducto();
                    
                    
                    $seriesd = array();
                    $idupdate = array();
                    $cant = 0;
                    $cantini = 0;
                    $seriesinsert= array();
                    
                    for ($i = 0; $i < count($codigo); $i++) { //recorro cada unos de los item
                        
                     //   $ultimokardex = $kardexm->ultimomovimientoproducto($idkardexantc[$i]);
                        
                        $seriesk = $seriem->select($idkardexantc[$i]);
                        
                        //////////// series del kardex anterior ////////////////
                        echo 'series antiguas del kardex '. $idkardexantc[$i];
                        var_dump($seriesk);
                        foreach ($seriesk as $k){
                            $d = array (
                                $k->getSerie(),
                                $idkardex[$i],
                                $_SESSION['idempresa']
                        
                            );
                            array_push($seriesinsert, $d);
                        }
                        
                        if ($incluye[$i] == 'Si') { //// pregunto si incluye serie // 
                            echo 'cantidad '.$cantidad[$i].' del item '.$i;
                            $cant = $cantidad[$i]; /// recojo la cantida de series a incluir en el item
//                    echo 'cantidad '.$cant;
                            
                            for ($k = $cantini; $k < $cant + $cantini; $k++) { //////////// series de cada item q incluir = si                           //recorro las series 
                                $e = array(
                                strtoupper($seriekp[$k]),
                                    $idkardex[$i],
                                    $_SESSION['idempresa']
                                );
                               echo 'insert del item new'.$i ;
                               var_dump($e);
                                array_push($seriesinsert, $e);
                            }

                            $cantini += $cant;
                            echo 'cantini '.$cantini;

                        }

//                        ////////////////////////////////////////////////////
//                        $series = array();
//                        for ($i = 0; $i < count($serie); $i++) { ////////////// series de producto 
//                            $d = array(
//                                $serie[$i],
//                                $idprodserie[$i],
//                                1
//                            );
//                            array_push($series, $d);
//                        }
//                        $seriem = new serieProducto();
//                        $seriem->insert($series); ////////////// insert de las series segun array input 
//                    }
//                    $seried = new serieDetalle();
//                    $seried->insert($seriesd, $idupdate);
//            
                }
                    echo 'serieinsert';
                    var_dump($seriesinsert);
                    $seriem->insert($seriesinsert);
                }


                /////////////////////////////////
//            if (isset($_POST['serieidprod']) && isset($_POST['serieprod'])) {
////            var_dump($_POST['serieprod']);
//                $idprodserie = $_POST['serieidprod'];
//                $serie = $_POST['serieprod'];
//
//                $series = array();
//                for ($i = 0; $i < count($serie); $i++) {
//                    $d = array(
//                        $serie[$i],
//                        $idprodserie[$i],
//                        1
//                    );
//                    array_push($series, $d);
//                }
//                $seriem = new serieProducto();
//                $seriem->insert($series);
//            }
                ///////////////////////////
                if (isset($_POST['serieguia']) && isset($_POST['tipoguia'])) {
//            var_dump($_POST['serieguia']);
                    $serieguia = $_POST['serieguia'];
                    $tipoguia = $_POST['tipoguia'];

                    $guias = array();
                    for ($i = 0; $i < count($serieguia); $i++) {
                        $d = array(
                            $serieguia[$i],
                            $tipoguia[$i],
                            $id
                        );
                        array_push($guias, $d);
                    }
                    $docguias = new documentoGuia();
                    $docguias->insert($guias);
                }
                if (isset($_POST['nombreotros']) && isset($_POST['descripcionotros'])) {
//            var_dump($_POST['nombreotros']);
                    $nombre = $_POST['nombreotros'];
                    $descripcion = $_POST['descripcionotros'];

                    $otros = array();
                    for ($i = 0; $i < count($nombre); $i++) {
                        $d = array(
                            $nombre[$i],
                            $descripcion[$i],
                            $id
                        );
                        array_push($otros, $d);
                    }
                    $otrosm = new documentoOtros();
                    $otrosm->insert($otros);
                }
            } else {
                ?> 
                <script>

                    swal('No se realizo registro', 'El documento ya se encuentra emitido', 'error');
                </script>  <?php
            }

            if ($id > 0) {
                ?>

                <script>

                  swal("Éxitosamente!", "Operación realizada correctamente.", "success");

//                    $('#FormularioAjaxDocumento').trigger("reset");
//
//                    $('#tabla').empty();
//                    $('#lblgravada').html('<strong>GRAVADA: </strong>  S/ 0.00');
//                    $('#lbligv').html('<strong>IGV 18%: </strong>  S/ 0.00');
//                    $('#lbltotal').html('<strong>TOTAL: </strong>    S/ 0.00');
                    document.location.href= '<?= base_url.'documento/compra' ?>';
                </script>
                <?php
            } else {
                ?> 
                <script>

                    swal('No se realizarón cambios', 'Por favor recargue la página', 'error');
                </script>  <?php
            }
        } else {
            ?> 
            <script>

                swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'error');
            </script>  <?php
        }
        
        ////
        }else {
             ?> 
            <script>

                swal('Acceso denegado', 'Necesita permisos para realizar esta acción, 'error');
            </script>  <?php
            
        }
        
    }

    function updatecompra() {

//        var_dump($_POST);

        if (isset($_POST['tipodoc']) && isset($_POST['txtserie']) && isset($_POST['txtnro']) && isset($_POST['cbmoneda']) && isset($_POST['incigv']) &&
                isset($_POST['dpfechaemision']) && isset($_POST['dpfechavencimiento'])  && isset($_POST['txtrucbuscar']) &&
                isset($_POST['txtcliente']) && isset($_POST['txtdireccion']) && isset($_POST['txtemail']) && isset($_POST['txtordenc']) && isset($_POST['txtobservacion']) &&
                !empty($_POST['tipodoc']) && !empty($_POST['txtserie']) && !empty($_POST['txtnro']) && !empty($_POST['cbmoneda']) && !empty($_POST['dpfechaemision']) && 
                !empty($_POST['dpfechavencimiento'])  && !empty($_POST['txtrucbuscar']) && !empty($_POST['txtcliente']) && isset($_POST['id'])
                && isset($_POST['gratuita']) && isset($_POST['exonerado']) && isset($_POST['inafecto']) && isset($_POST['exportacion']) && isset($_POST['gravada']) 
                && isset($_POST['igv']) && isset($_POST['totaldoc'])  && isset($_POST['cbtipoventa']) 
                && !empty($_POST['cbtipoventa']) && isset($_POST['cbtipodocpersona']) && !empty($_POST['cbtipodocpersona']) && isset($_POST['iddoc']) && !empty($_POST['iddoc']) ) { //&& !empty($_POST['totaldoc']) && !empty($_POST['gravada'])&& !empty($_POST['igv']) && !empty($_POST['txttipocambio'])&& isset($_POST['txttipocambio'])
//            $serier = str_replace(PHP_EOL, ' ', $_POST['txtserie']);
//            $numeror = str_replace(PHP_EOL, ' ', $_POST['txtnro']);
//            
//            $serie = ltrim($serier,'0');
//            $numero = (int)($numeror);
            $id = $_POST['iddoc'];
//        if($this->documento->duplicado($serie, $numero,'Compra') == 'valido'){
            $emision = trim($_POST['dpfechaemision']);

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');


            $vencimiento = trim($_POST['dpfechavencimiento']);
            $dateven = DateTime::createFromFormat('d/m/Y', $vencimiento);
            $vencimientof = $dateven->format('Y-m-d');

            $gratuita = 0;
            $exonerado = 0;
            $inafecto = 0;
            $exportacion = 0;
            
            $totaldoc = 0;
            $igvdoc = 0;
            $gravada= 0;
            if (!empty($_POST['totaldoc'])) {
                $totaldoc = $_POST['totaldoc'];
            }
            if (!empty($_POST['gravada'])) {
                $gravada = $_POST['gravada'];
            }
            if (!empty($_POST['igv'])) {
                $igvdoc = $_POST['igv'];
            }


            if (!empty($_POST['gratuita'])) {
                $gratuita = $_POST['gratuita'];
            }
            if (!empty($_POST['exonerado'])) {
                $exonerado = $_POST['exonerado'];
            }
            if (!empty($_POST['inafecto'])) {
                $inafecto = $_POST['inafecto'];
            }
            if (!empty($_POST['exportacion'])) {
                $exportacion = $_POST['exportacion'];
            }

            if (isset($_POST['incigv'])) {
                $incigv = 1;
            } else {
                $incigv = 0;
            }

            if (!empty($_POST['cbsujetoa'])) {
                $sujetoa = $_POST['cbsujetoa'];
            } else {
                $sujetoa = 0;
            }


            $documento = new documento();
            $tipocompra = $_POST['cbtipoventa'];
//            $gravada = $_POST['gravada'];
//            $totaldoc = $_POST['totaldoc'];
//            $igvdoc = $_POST['igv'];

            $serie = $_POST['txtserie'];
            $numero = $_POST['txtnro'];
            $moneda = $_POST['cbmoneda'];
            $fechaemision = $emisionf;
            $fechavencimiento = $vencimientof;
            $tipodoc = $_POST['tipodoc'];


            $idpersona = $_POST['idcliente'];
            if (empty($idpersona)) {
                $idpersona = 0;
            }
            
            list($personatipodoc,$idpersonatipodoc) = explode('-',$_POST['cbtipodocpersona']);
            $idusuario = $_SESSION['id'];
//            $personatipodoc= $_POST['cbtipodocpersona'];
            $ruc = $_POST['txtrucbuscar'];
            $razonsocial = $_POST['txtcliente'];
            $direccion = $_POST['txtdireccion'];
            $email = $_POST['txtemail'];
            $norden = $_POST['txtordenc'];
            $observacion = $_POST['txtobservacion'];
            $idsucursal = $_SESSION['idsucursal'];
            $tipocambio = 0.00; //$_POST['txttipocambio']
            
            $personam = new persona();
            $personam->setIdtipodocumento($idpersonatipodoc);
            $personam->setRuc($ruc);
            $personam->setNombre($razonsocial);
            $personam->setDireccion($direccion);
            $personam->setEmail($email);

            if($idpersona == 0){
                $personam->setAnivcumplenos('0000-00-00');
                $personam->insert($personam);

            }else {
                $personam->setId($idpersona);
                $personam->updatedoc($personam);

            }

            $documento->setId($id);
            $documento->setSerie($serie);
            $documento->setNumero($numero);
            $documento->setMoneda($moneda);
            $documento->setFechaemision($fechaemision);
            $documento->setFechavencimiento($fechavencimiento);
//            $documento->setTipodoc('Compra');
            $documento->setIdusuario($idusuario);
            $documento->setIdpersona($idpersona);
            $documento->setRuc($ruc);
            $documento->setRazonsocial($razonsocial);
            $documento->setDireccion($direccion);
            $documento->setEmail($email);
            $documento->setNorden($norden);
            $documento->setObservacion($observacion);
            $documento->setIdsucursal($idsucursal);
            $documento->setTipocambio($tipocambio);
            $documento->setTipo($tipodoc);
            $documento->setSujetoa($sujetoa);
            $documento->setIncigv($incigv);
            $documento->setGarantia(0);
            $documento->setValidezdias(0);
            $documento->setPlazoentregadias(0);
            $documento->setCondicionpagodias(0);
            $documento->setMontogratuita($gratuita);
            $documento->setMontoexonerado($exonerado);
            $documento->setMontoinafecto($inafecto);
            $documento->setMontoexportacion($exportacion);
            $documento->setMontogravada($gravada);
            $documento->setMontoigv($igvdoc);
            $documento->setTotal($totaldoc);
            $documento->setTipoventa($tipocompra);
            $documento->setPersonatipodoc($personatipodoc);
            

            $documento->update($documento);

            $idprod = $_POST['id'];
            $codigo = $_POST['codigo'];
            $descripcion = $_POST['descripcionprod'];
            $unidad = $_POST['unidad'];
            $tipoigv = $_POST['tipoigv'];
            $cantidad = $_POST['cantidad'];
            $precio = $_POST['precio'];
            $subtotal = $_POST['subtotal'];
            $total = $_POST['total'];

            $igvprod = $_POST['igvprod'];
            $valorunitref = $_POST['valorunitref'];
            $montobaseigv = $_POST['montobaseigv'];
            $montobaseexpo = $_POST['montobaseexpo'];
            $montobaseexonerado = $_POST['montobaseexonerado'];
            $montobaseinafecto = $_POST['montobaseinafecto'];
            $montobasegratuito = $_POST['montobasegratuito'];
            $montobaseivap = $_POST['montobaseivap'];
            $montobaseotrostributos = $_POST['montobaseotrostributos'];
            $tributoventagratuita = $_POST['tributoventagratuita'];
            $otrostributos = $_POST['otrostributos'];
            $porcentajeigv = $_POST['porcentajeigv'];
            $porcentajeotrostributos = $_POST['porcentajeotrostributos'];
            $porcentajetributoventagratuita = $_POST['porcentajetributoventagratuita'];
            $montooriginal = $_POST['montooriginal'];
            $monedaoriginal = $_POST['monedaoriginal'];
            $incluye = $_POST['incluye'];

            $detalles = array();
            $produpdate = array();  //////////// array de productos actualizar stock
            for ($i = 0; $i < count($codigo); $i++) {
                $idpro = $idprod[$i];
                $igvp = $igvprod[$i];
                $valorunitr = $valorunitref[$i];
                $montobaseig = $montobaseigv[$i];
                $montobaseexp = $montobaseexpo[$i];
                $montobaseexon = $montobaseexonerado[$i];
                $montobaseinaf = $montobaseinafecto[$i];
                $montobasegratu = $montobasegratuito[$i];
                $montobaseiv = $montobaseivap[$i];
                $montobaseotrostrib = $montobaseotrostributos[$i];
                $tributoventagrat = $tributoventagratuita[$i];
                $otrostrib = $otrostributos[$i];
                $porceigv = $porcentajeigv[$i];
                $porcotrostrib = $porcentajeotrostributos[$i];
                $porcentajetribventgrat = $porcentajetributoventagratuita[$i];
                $montoorig = $montooriginal[$i];
                if (empty($idprod[$i])) {
                    $idpro = 0;
                }
                if (empty($igvprod[$i])) {
                    $igvp = 0;
                }
                if (empty($valorunitref[$i])) {
                    $valorunitr = 0;
                }
                if (empty($montobaseigv[$i])) {
                    $montobaseig = 0;
                }
                if (empty($montobaseexpo[$i])) {
                    $montobaseexp = 0;
                }
                if (empty($montobaseexonerado[$i])) {
                    $montobaseexon = 0;
                }
                if (empty($montobaseinafecto[$i])) {
                    $montobaseinaf = 0;
                }
                if (empty($montobasegratuito[$i])) {
                    $montobasegratu = 0;
                }
                if (empty($montobaseivap[$i])) {
                    $montobaseiv = 0;
                }
                if (empty($montobaseotrostributos[$i])) {
                    $montobaseotrostrib = 0;
                }
                if (empty($tributoventagratuita[$i])) {
                    $tributoventagrat = 0;
                }
                if (empty($otrostributos[$i])) {
                    $otrostrib = 0;
                }
                if (empty($porcentajeigv[$i])) {
                    $porceigv = 0;
                }
                if (empty($porcentajeotrostributos[$i])) {
                    $porcotrostrib = 0;
                }
                if (empty($porcentajetributoventagratuita[$i])) {
                    $porcentajetribventgrat = 0;
                }
                if (empty($montooriginal[$i])) {
                    $montoorig = 0;
                }
                $d = array(
                    $idpro,
                    $codigo[$i],
                    $descripcion[$i],
                    $unidad[$i],
                    $tipoigv[$i],
                    $cantidad[$i],
                    $precio[$i],
                    $subtotal[$i],
                    $total[$i],
                    $id,
                    $igvp,
                    $valorunitr,
                    $montobaseig,
                    $montobaseexp,
                    $montobaseexon,
                    $montobaseinaf,
                    $montobasegratu,
                    $montobaseiv,
                    $montobaseotrostrib,
                    $tributoventagrat,
                    $otrostrib,
                    $porceigv,
                    $porcotrostrib,
                    $porcentajetribventgrat,
                    $montoorig,
                    $monedaoriginal[$i],
                    $incluye[$i]
                );
                array_push($detalles, $d);

                $produp = array(
                    $cantidad[$i],
                    $idpro
                );
                array_push($produpdate, $produp);
            }
            $detalle = new Detalle();
            $producto = new producto();
            //////////////// update el stock del antiguo detalle////////////////////
            $produpdateant = array();
            foreach ($detalle->selectOneDoc($id) as $det) {
                $produpa = array(
                    ($det->getCantidad() * -1),
                    $det->getIdproducto()
                );
                array_push($produpdateant, $produpa);
            }
            $producto->updatestock($produpdateant);
            //////////////////////////Fin update stock///////////////////////////////////////////



            $detalle->delete($id);
            $iddetalle = $detalle->insert($detalles);
            $producto->updatestock($produpdate);


            if (isset($_POST['serieprod']) && isset($_POST['serieidprod'])) { //&& isset($_POST['idserie'])
//            var_dump($_POST['idserieitem']);
//            $idserie = $_POST['idserieitem'];
                $idprodserie = $_POST['serieidprod'];
                $serie = $_POST['serieprod'];

                $seriesd = array();
                $idupdate = array();
                $cant = 0;
                $cantini = 0;
                for ($i = 0; $i < count($codigo); $i++) { //recorro cada unos de los item
                    if ($incluye[$i] == 'Si') { //// pregunto si incluye serie // 
                        $cant = $cantidad[$i]; /// recojo la cantida de series a incluir en el item
//                    echo 'cantidad '.$cant;

                        for ($j = $cantini; $j < $cant + $cantini; $j++) { //////////// series de cada item q incluir = si                           //recorro las series 
                            $da = array(
                                $serie[$j],
//                            $idserie[$j],
                                $iddetalle[$i],
                                1
                            );


//                        $idup = array($idserie[$j]);
//                        array_push($idupdate, $idup);
                            array_push($seriesd, $da);
                        }




                        $cantini += $cant;
//                    echo 'cant '.$cantini;
//                    echo 'j '.$j;
                    }

                    ////////////////////////////////////////////////////
                    $series = array();
                    for ($i = 0; $i < count($serie); $i++) { ////////////// series de producto 
                        $d = array(
                            $serie[$i],
                            $idprodserie[$i],
                            1
                        );
                        array_push($series, $d);
                    }
                    $seriem = new serieProducto();
                    $seriem->insert($series); ////////////// insert de las series segun array input 
                }
                $seried = new serieDetalle();
                $seried->insert($seriesd, $idupdate);
//            
            }


            /////////////////////////////////
//            if (isset($_POST['serieidprod']) && isset($_POST['serieprod'])) {
////            var_dump($_POST['serieprod']);
//                $idprodserie = $_POST['serieidprod'];
//                $serie = $_POST['serieprod'];
//
//                $series = array();
//                for ($i = 0; $i < count($serie); $i++) {
//                    $d = array(
//                        $serie[$i],
//                        $idprodserie[$i],
//                        1
//                    );
//                    array_push($series, $d);
//                }
//                $seriem = new serieProducto();
//                $seriem->insert($series);
//            }
            ///////////////////////////
            if (isset($_POST['serieguia']) && isset($_POST['tipoguia'])) {
//            var_dump($_POST['serieguia']);
                $serieguia = $_POST['serieguia'];
                $tipoguia = $_POST['tipoguia'];

                $guias = array();
                for ($i = 0; $i < count($serieguia); $i++) {
                    $d = array(
                        $serieguia[$i],
                        $tipoguia[$i],
                        $id
                    );
                    array_push($guias, $d);
                }
                $docguias = new documentoGuia();
                $docguias->insert($guias);
            }
            if (isset($_POST['nombreotros']) && isset($_POST['descripcionotros'])) {
//            var_dump($_POST['nombreotros']);
                $nombre = $_POST['nombreotros'];
                $descripcion = $_POST['descripcionotros'];

                $otros = array();
                for ($i = 0; $i < count($nombre); $i++) {
                    $d = array(
                        $nombre[$i],
                        $descripcion[$i],
                        $id
                    );
                    array_push($otros, $d);
                }
                $otrosm = new documentoOtros();
                $otrosm->insert($otros);
            }

            if ($id > 0) {
                ?>

                <script>

                    swal("Éxitosamente!", "Operación realizada correctamente.", "success");

                </script>
                <?php
            } else {
                ?> 
                <script>

                    swal('No se realizarón cambios', 'Por favor recargue la página', 'error');
                </script>  <?php
            }
        } else {
            ?> 
            <script>

                swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'error');
            </script>  <?php
        die();
        }
    }

    function insertordencompra() {

//        var_dump($_POST);
        if(permisos::rol('rol-ordencompra')){
        if (isset($_POST['tipodoc']) && isset($_POST['txtnro']) && isset($_POST['cbmoneda']) && isset($_POST['incigv']) && isset($_POST['dpfechaemision']) && isset($_POST['dpfechavencimiento'])  && isset($_POST['txtrucbuscar']) && isset($_POST['txtcliente']) &&
                isset($_POST['txtdireccion']) && isset($_POST['txtemail']) && isset($_POST['txtgarantia']) && !empty($_POST['txtnro']) && !empty($_POST['cbmoneda']) && !empty($_POST['dpfechaemision']) && !empty($_POST['dpfechavencimiento'])  && !empty($_POST['txtgarantia']) && !empty($_POST['txtrucbuscar']) && !empty($_POST['txtcliente']) &&
                isset($_POST['id']) && isset($_POST['gratuita']) && isset($_POST['exonerado']) && isset($_POST['inafecto']) && isset($_POST['exportacion']) && isset($_POST['gravada']) && isset($_POST['igv']) && isset($_POST['totaldoc']) 
                && isset($_POST['cbtipodocpersona']) && !empty($_POST['cbtipodocpersona'])) { //!empty($_POST['totaldoc']) && !empty($_POST['gravada']) && !empty($_POST['igv']) && isset($_POST['txttipocambio']) && !empty($_POST['txttipocambio'])
//            $serier = str_replace(PHP_EOL, ' ', $_POST['txtserie']);
            $numeror = str_replace(PHP_EOL, ' ', $_POST['txtnro']);

            $serie = '';
            $numero = (int) ($numeror);

            if ($this->documento->duplicado($serie, $numero, 'orden_compra') == 'valido') {
                $emision = $_POST['dpfechaemision'];

                $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
                $emisionf = $dateemis->format('Y-m-d');


                $vencimiento = $_POST['dpfechavencimiento'];
                $dateven = DateTime::createFromFormat('d/m/Y', $vencimiento);
                $vencimientof = $dateven->format('Y-m-d');


                $gratuita = 0;
                $exonerado = 0;
                $inafecto = 0;
                $exportacion = 0;
                
                $totaldoc = 0;
                $igvdoc = 0;
                $gravada= 0;
                if (!empty($_POST['totaldoc'])) {
                    $totaldoc = $_POST['totaldoc'];
                }
                if (!empty($_POST['gravada'])) {
                    $gravada = $_POST['gravada'];
                }
                if (!empty($_POST['igv'])) {
                    $igvdoc = $_POST['igv'];
                }


                if (!empty($_POST['gratuita'])) {
                    $gratuita = $_POST['gratuita'];
                }
                if (!empty($_POST['exonerado'])) {
                    $exonerado = $_POST['exonerado'];
                }
                if (!empty($_POST['inafecto'])) {
                    $inafecto = $_POST['inafecto'];
                }
                if (!empty($_POST['exportacion'])) {
                    $exportacion = $_POST['exportacion'];
                }


                if (isset($_POST['incigv'])) {
                    $incigv = 1;
                } else {
                    $incigv = 0;
                }

                if (!empty($_POST['cbsujetoa'])) {
                    $sujetoa = $_POST['cbsujetoa'];
                } else {
                    $sujetoa = 0;
                }

                $documento = new documento();
//                $personatipodoc = $_POST['cbtipodocpersona'];
//                $gravada = $_POST['gravada'];
//                $totaldoc = $_POST['totaldoc'];
//                $igvdoc = $_POST['igv'];

                $serie = 'orden_compra';
//            $numero = $_POST['txtnro'];
                $moneda = $_POST['cbmoneda'];
                $fechaemision = $emisionf;
                $fechavencimiento = $vencimientof;
                $tipodoc = $_POST['tipodoc'];

                $idpersona = $_POST['idcliente'];

                if (empty($idpersona)) {
                    $idpersona = 0;
                }
                
                list($personatipodoc,$idpersonatipodoc) = explode('-',$_POST['cbtipodocpersona']);
                
                $idusuario = $_SESSION['id'];
                $ruc = $_POST['txtrucbuscar'];
                $razonsocial = $_POST['txtcliente'];
                $direccion = $_POST['txtdireccion'];
                $email = $_POST['txtemail'];
//        $norden=$_POST['txtordenc'];
//        $observacion=$_POST['txtobservacion'];
                $idsucursal = $_SESSION['idsucursal'];
                $tipocambio = 0.00; //$_POST['txttipocambio']
                $garantia = $_POST['txtgarantia'];
                
                $personam = new persona();
                $personam->setIdtipodocumento($idpersonatipodoc);
                $personam->setRuc($ruc);
                $personam->setNombre($razonsocial);
                $personam->setDireccion($direccion);
                $personam->setEmail($email);
                $personam->setTipopersona(2);
                
                
                if($idpersona == 0){
                    $personam->setAnivcumplenos('0000-00-00');
                    $personam->insert($personam);
                    
                }else {
                    $personam->setId($idpersona);
                    $personam->updatedoc($personam);
                    
                }
                
                if (empty($garantia)) {
                    $garantia = 0;
                }

                $documento->setSerie($serie);
                $documento->setNumero($numero);
                $documento->setMoneda($moneda);
                $documento->setFechaemision($fechaemision);
                $documento->setFechavencimiento($fechavencimiento);
                $documento->setTipodoc($tipodoc);
                $documento->setIdusuario($idusuario);
                $documento->setIdpersona($idpersona);
                $documento->setRuc($ruc);
                $documento->setRazonsocial($razonsocial);
                $documento->setDireccion($direccion);
                $documento->setEmail($email);
//            $documento->setNorden($norden);
//            $documento->setObservacion($observacion);
                $documento->setIdsucursal($idsucursal);
                $documento->setTipocambio($tipocambio);
                $documento->setTipo($tipodoc);
                $documento->setGarantia($garantia);

                $documento->setIncigv($incigv);
                $documento->setValidezdias(0);
                $documento->setPlazoentregadias(0);
                $documento->setCondicionpagodias(0);

                $documento->setMontogratuita($gratuita);
                $documento->setMontoexonerado($exonerado);
                $documento->setMontoinafecto($inafecto);
                $documento->setMontoexportacion($exportacion);
                $documento->setMontogravada($gravada);
                $documento->setMontoigv($igvdoc);
                $documento->setTotal($totaldoc);
                $documento->setPersonatipodoc($personatipodoc);
                
                $id = $documento->insert($documento);

                $idprod = $_POST['id'];
                $codigo = $_POST['codigo'];
                $descripcion = $_POST['descripcionprod'];
                $unidad = $_POST['unidad'];
                $tipoigv = $_POST['tipoigv'];
                $cantidad = $_POST['cantidad'];
                $precio = $_POST['precio'];
                $subtotal = $_POST['subtotal'];
                $total = $_POST['total'];

                $igvprod = $_POST['igvprod'];
                $valorunitref = $_POST['valorunitref'];
                $montobaseigv = $_POST['montobaseigv'];
                $montobaseexpo = $_POST['montobaseexpo'];
                $montobaseexonerado = $_POST['montobaseexonerado'];
                $montobaseinafecto = $_POST['montobaseinafecto'];
                $montobasegratuito = $_POST['montobasegratuito'];
                $montobaseivap = $_POST['montobaseivap'];
                $montobaseotrostributos = $_POST['montobaseotrostributos'];
                $tributoventagratuita = $_POST['tributoventagratuita'];
                $otrostributos = $_POST['otrostributos'];
                $porcentajeigv = $_POST['porcentajeigv'];
                $porcentajeotrostributos = $_POST['porcentajeotrostributos'];
                $porcentajetributoventagratuita = $_POST['porcentajetributoventagratuita'];
                $montooriginal = $_POST['montooriginal'];
                $monedaoriginal = $_POST['monedaoriginal'];
                $incluye = $_POST['incluye'];

                $detalles = array();
//         $produpdate = array();  //////////// array de productos actualizar stock
                for ($i = 0; $i < count($codigo); $i++) {

                    $idpro = $idprod[$i];
                    $igvp = $igvprod[$i];
                    $valorunitr = $valorunitref[$i];
                    $montobaseig = $montobaseigv[$i];
                    $montobaseexp = $montobaseexpo[$i];
                    $montobaseexon = $montobaseexonerado[$i];
                    $montobaseinaf = $montobaseinafecto[$i];
                    $montobasegratu = $montobasegratuito[$i];
                    $montobaseiv = $montobaseivap[$i];
                    $montobaseotrostrib = $montobaseotrostributos[$i];
                    $tributoventagrat = $tributoventagratuita[$i];
                    $otrostrib = $otrostributos[$i];
                    $porceigv = $porcentajeigv[$i];
                    $porcotrostrib = $porcentajeotrostributos[$i];
                    $porcentajetribventgrat = $porcentajetributoventagratuita[$i];
                    $montoorig = $montooriginal[$i];
                    if (empty($idprod[$i])) {
                        $idpro = 0;
                    }
                    if (empty($igvprod[$i])) {
                        $igvp = 0;
                    }
                    if (empty($valorunitref[$i])) {
                        $valorunitr = 0;
                    }
                    if (empty($montobaseigv[$i])) {
                        $montobaseig = 0;
                    }
                    if (empty($montobaseexpo[$i])) {
                        $montobaseexp = 0;
                    }
                    if (empty($montobaseexonerado[$i])) {
                        $montobaseexon = 0;
                    }
                    if (empty($montobaseinafecto[$i])) {
                        $montobaseinaf = 0;
                    }
                    if (empty($montobasegratuito[$i])) {
                        $montobasegratu = 0;
                    }
                    if (empty($montobaseivap[$i])) {
                        $montobaseiv = 0;
                    }
                    if (empty($montobaseotrostributos[$i])) {
                        $montobaseotrostrib = 0;
                    }
                    if (empty($tributoventagratuita[$i])) {
                        $tributoventagrat = 0;
                    }
                    if (empty($otrostributos[$i])) {
                        $otrostrib = 0;
                    }
                    if (empty($porcentajeigv[$i])) {
                        $porceigv = 0;
                    }
                    if (empty($porcentajeotrostributos[$i])) {
                        $porcotrostrib = 0;
                    }
                    if (empty($porcentajetributoventagratuita[$i])) {
                        $porcentajetribventgrat = 0;
                    }
                    if (empty($montooriginal[$i])) {
                        $montoorig = 0;
                    }
                    $d = array(
                        $idpro,
                        $codigo[$i],
                        $descripcion[$i],
                        $unidad[$i],
                        $tipoigv[$i],
                        $cantidad[$i],
                        $precio[$i],
                        $subtotal[$i],
                        $total[$i],
                        $id,
                        $igvp,
                        $valorunitr,
                        $montobaseig,
                        $montobaseexp,
                        $montobaseexon,
                        $montobaseinaf,
                        $montobasegratu,
                        $montobaseiv,
                        $montobaseotrostrib,
                        $tributoventagrat,
                        $otrostrib,
                        $porceigv,
                        $porcotrostrib,
                        $porcentajetribventgrat,
                        $montoorig,
                        $monedaoriginal[$i],
                        $incluye[$i],0
                    );
                    array_push($detalles, $d);

//            $produp = array(
//                $cantidad[$i],
//                $idprod[$i]
//                
//            );
//            array_push($produpdate, $produp);
                }

                $detalle = new Detalle();
//        $producto = new producto();
                $iddetalle = $detalle->insert($detalles);
//        $producto->updatestock($produpdate);

                if (isset($_POST['serieprod']) && isset($_POST['serieidprod'])) { //&& isset($_POST['idserie'])
//            var_dump($_POST['idserieitem']);
//            $idserie = $_POST['idserieitem'];
                    $idprodserie = $_POST['serieidprod'];
                    $serie = $_POST['serieprod'];

                    $seriesd = array();
                    $idupdate = array();
                    $cant = 0;
                    $cantini = 0;
                    for ($i = 0; $i < count($codigo); $i++) { //recorro cada unos de los item
                        if ($incluye[$i] == 'Si') { //// pregunto si incluye serie // 
                            $cant = $cantidad[$i]; /// recojo la cantida de series a incluir en el item
//                    echo 'cantidad '.$cant;

                            for ($j = $cantini; $j < $cant + $cantini; $j++) { //////////// series de cada item q incluir = si                           //recorro las series 
                                $da = array(
                                    $serie[$j],
//                            $idserie[$j],
                                    $iddetalle[$i],
                                    1,0
                                );


                                array_push($seriesd, $da);
                            }




                            $cantini += $cant;
//                    echo 'cant '.$cantini;
//                    echo 'j '.$j;
                        }
                    }
                    $seried = new serieDetalle();
                    $seried->insert($seriesd, $idupdate);
//            
                }





                if (isset($_POST['serieguia']) && isset($_POST['tipoguia'])) {
//            var_dump($_POST['serieguia']);
                    $serieguia = $_POST['serieguia'];
                    $tipoguia = $_POST['tipoguia'];

                    $guias = array();
                    for ($i = 0; $i < count($serieguia); $i++) {
                        $d = array(
                            $serieguia[$i],
                            $tipoguia[$i],
                            $id
                        );
                        array_push($guias, $d);
                    }
                    $docguias = new documentoGuia();
                    $docguias->insert($guias);
                }
                if (isset($_POST['nombreotros']) && isset($_POST['descripcionotros'])) {
//            var_dump($_POST['nombreotros']);
                    $nombre = $_POST['nombreotros'];
                    $descripcion = $_POST['descripcionotros'];

                    $otros = array();
                    for ($i = 0; $i < count($nombre); $i++) {
                        $d = array(
                            $nombre[$i],
                            $descripcion[$i],
                            $id
                        );
                        array_push($otros, $d);
                    }
                    $otrosm = new documentoOtros();
                    $otrosm->insert($otros);
                }
            } else {
                ?> 
                <script>

                    swal('No se realizo registro', 'El documento ya se encuentra emitido', 'error');
                </script>  <?php
            }
            if ($id > 0) {
                ?>

                <script>

                    swal("Éxitosamente!", "Operación realizada correctamente.", "success");
//                    var nro = $('#txtnro').val();
//                    $('#FormularioAjaxDocumento').trigger("reset");
//                    if (nro != '' || !isNaN(nro)) {
//                        $('#txtnro').val(parseInt(nro.trim()) + 1);
//                    }
//                    $('#tabla').empty();
//                    $('#lblgravada').html('<strong>GRAVADA: </strong>  S/ 0.00');
//                    $('#lbligv').html('<strong>IGV 18%: </strong>  S/ 0.00');
//                    $('#lbltotal').html('<strong>TOTAL: </strong>    S/ 0.00');
                document.location.href= '<?= base_url.'documento/ordencompra' ?>';
                
                </script>
                <?php
            } else {
                ?> 
                <script>

                    swal('No se realizarón cambios', 'Por favor recargue la página', 'error');
                </script>  <?php
            }
        } else {
            ?> 
            <script>

                swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'error');
            </script>  <?php
        }
       
    }else{
        ?> 
            <script>

                swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
    }
    }

    function insertcotizacion() {

//        var_dump($_POST);
        if(permisos::rol('rol-cotizacionventa')){
        $id = 0;
        if (isset($_POST['txtnro']) && isset($_POST['cbmoneda']) && isset($_POST['dpfechaemision']) && isset($_POST['txtvalidezdia']) && isset($_POST['txtplazoentrega']) && isset($_POST['cbvendedor']) && isset($_POST['txtatencion'])  && isset($_POST['txtrucbuscar']) && isset($_POST['txtcliente']) && isset($_POST['txtdireccion']) && isset($_POST['txtemail']) && isset($_POST['txtgarantia']) && isset($_POST['txtobservacion']) && !empty($_POST['txtnro']) && !empty($_POST['cbmoneda']) && !empty($_POST['dpfechaemision']) &&
                !empty($_POST['txtvalidezdia']) && !empty($_POST['txtplazoentrega'])  && !empty($_POST['txtgarantia']) 
                && !empty($_POST['cbvendedor']) && !empty($_POST['txtrucbuscar']) && !empty($_POST['txtcliente']) && isset($_POST['id']) && isset($_POST['gratuita']) 
                && isset($_POST['exonerado']) && isset($_POST['inafecto']) && isset($_POST['exportacion']) && isset($_POST['gravada']) && isset($_POST['igv']) 
                && isset($_POST['totaldoc']) &&  isset($_POST['cbtipodocpersona']) && !empty($_POST['cbtipodocpersona'])) { //!empty($_POST['totaldoc']) && !empty($_POST['gravada']) && !empty($_POST['igv']) && && !empty($_POST['txttipocambio']) && isset($_POST['txttipocambio'])

//            $serier = str_replace(PHP_EOL, ' ', $_POST['txtserie']);
            $numeror = str_replace(PHP_EOL, ' ', $_POST['txtnro']);

            $serie = 'COTIZACION';
            $numero = (int) ($numeror);

            if ($this->documento->duplicado($serie, $numero, 'Cotizacion') == 'valido') {

                $emision = $_POST['dpfechaemision'];

                $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
                $emisionf = $dateemis->format('Y-m-d');


//            $vencimiento = $_POST['dpfechavencimiento'];
//            $dateven = DateTime::createFromFormat('d/m/Y', $vencimiento);
                $vencimientof = '0000-00-00';
                $condicionpago = 'Contado';
                $validezdia = 0;
                $plazoentrega = 0;
//            $diacredito = 0;
                $garantia = 0;



                $gratuita = 0;
                $exonerado = 0;
                $inafecto = 0;
                $exportacion = 0;
                
                $totaldoc = 0;
                $igvdoc = 0;
                $gravada= 0;
                if (!empty($_POST['totaldoc'])) {
                    $totaldoc = $_POST['totaldoc'];
                }
                if (!empty($_POST['gravada'])) {
                    $gravada = $_POST['gravada'];
                }
                if (!empty($_POST['igv'])) {
                    $igvdoc = $_POST['igv'];
                }


                if (!empty($_POST['gratuita'])) {
                    $gratuita = $_POST['gratuita'];
                }
                if (!empty($_POST['exonerado'])) {
                    $exonerado = $_POST['exonerado'];
                }
                if (!empty($_POST['inafecto'])) {
                    $inafecto = $_POST['inafecto'];
                }
                if (!empty($_POST['exportacion'])) {
                    $exportacion = $_POST['exportacion'];
                }

                if (!empty($_POST['txtvalidezdia'])) {

                    $validezdia = $_POST['txtvalidezdia'];
                }
                if (!empty($_POST['txtplazoentrega'])) {

                    $plazoentrega = $_POST['txtplazoentrega'];
                }
//            if(!empty($_POST['txtdiacredito'])){
//                
//                $diacredito = $_POST['txtdiacredito'];
//            }
                if (!empty($_POST['txtgarantia'])) {

                    $garantia = $_POST['txtgarantia'];
                }

                if (!isset($_POST['ckcondicion'])) {
                    $condicionpago = 'Credito';
                }

                $condionpagodia = 0;
                if (isset($_POST['txtdiacredito'])) {
                    $condionpagodia = $_POST['txtdiacredito'];
                }

                if (isset($_POST['incigv'])) {
                    $incigv = 1;
                } else {
                    $incigv = 0;
                }

                if (!empty($_POST['cbsujetoa'])) {
                    $sujetoa = $_POST['cbsujetoa'];
                } else {
                    $sujetoa = 0;
                }

                $documento = new documento();

//                $gravada = $_POST['gravada'];
//                $totaldoc = $_POST['totaldoc'];
//                $igvdoc = $_POST['igv'];
//        $serie=$_POST['txtserie'];
//            $numero = $_POST['txtnro'];
                $moneda = $_POST['cbmoneda'];
                $fechaemision = $emisionf;
                $fechavencimiento = $vencimientof;
                $tipodoc = 'Cotizacion';

                $idpersona = $_POST['idcliente'];

                if (empty($idpersona)) {
                    $idpersona = 0;
                }

                list($personatipodoc,$idpersonatipodoc) = explode('-',$_POST['cbtipodocpersona']);
                
                $idusuario = $_POST['cbvendedor'];
//                $personatipodoc= $_POST['cbtipodocpersona'];
                $ruc = $_POST['txtrucbuscar'];
                $razonsocial = $_POST['txtcliente'];
                $direccion = $_POST['txtdireccion'];
                $email = $_POST['txtemail'];
//        $norden=$_POST['txtordenc'];
//        $observacion=$_POST['txtobservacion'];
                $personam = new persona();
                $personam->setIdtipodocumento($idpersonatipodoc);
                $personam->setRuc($ruc);
                $personam->setNombre($razonsocial);
                $personam->setDireccion($direccion);
                $personam->setEmail($email);
//                $personam->setBydefault(0);
                $personam->setAnivcumplenos('0000-00-00');
                $personam->setTipopersona(1);
                
                if($idpersona == 0){
                    $personam->insert($personam);
                    
                }else {
                    $personam->setId($idpersona);
                    $personam->updatedoc($personam);
                    
                }
                
                $idsucursal = $_SESSION['idsucursal'];
                $tipocambio = 0.00; //$_POST['txttipocambio']
//            $garantia = $garantia;
                $atencion = $_POST['txtatencion'];
                $observacion = $_POST['txtobservacion'];
                $documento->setSerie('COTIZACION');
                $documento->setNumero($numero);
                $documento->setMoneda($moneda);
                $documento->setFechaemision($fechaemision);
                $documento->setFechavencimiento($fechavencimiento);
                $documento->setTipodoc($tipodoc);
                $documento->setIdusuario($idusuario);
                $documento->setIdpersona($idpersona);
                $documento->setRuc($ruc);
                $documento->setRazonsocial($razonsocial);
                $documento->setDireccion($direccion);
                $documento->setEmail($email);
//            $documento->setNorden($norden);
//            $documento->setObservacion($observacion);
                $documento->setIdsucursal($idsucursal);
                $documento->setTipocambio($tipocambio);
                $documento->setTipo($tipodoc);
                $documento->setGarantia($garantia);
                $documento->setValidezdias($validezdia);
                $documento->setPlazoentregadias($plazoentrega);
                $documento->setCondicionpagodias($condionpagodia);
                $documento->setCondicionpago($condicionpago);
                $documento->setAtencion($atencion);
                $documento->setObservacion($observacion);
                $documento->setIncigv($incigv);
                $documento->setMontogratuita($gratuita);
                $documento->setMontoexonerado($exonerado);
                $documento->setMontoinafecto($inafecto);
                $documento->setMontoexportacion($exportacion);
                $documento->setMontogravada($gravada);
                $documento->setMontoigv($igvdoc);
                $documento->setTotal($totaldoc);
                $documento->setPersonatipodoc($personatipodoc);
                $id = $documento->insert($documento);

                $idprod = $_POST['id'];
                $codigo = $_POST['codigooriginal'];
                $descripcion = $_POST['descripcionprod'];
                $unidad = $_POST['unidad'];
                $tipoigv = $_POST['tipoigv'];
                $cantidad = $_POST['cantidad'];
                $precio = $_POST['precio'];
                $subtotal = $_POST['subtotal'];
                $total = $_POST['total'];

                $igvprod = $_POST['igvprod'];
                $valorunitref = $_POST['valorunitref'];
                $montobaseigv = $_POST['montobaseigv'];
                $montobaseexpo = $_POST['montobaseexpo'];
                $montobaseexonerado = $_POST['montobaseexonerado'];
                $montobaseinafecto = $_POST['montobaseinafecto'];
                $montobasegratuito = $_POST['montobasegratuito'];
                $montobaseivap = $_POST['montobaseivap'];
                $montobaseotrostributos = $_POST['montobaseotrostributos'];
                $tributoventagratuita = $_POST['tributoventagratuita'];
                $otrostributos = $_POST['otrostributos'];
                $porcentajeigv = $_POST['porcentajeigv'];
                $porcentajeotrostributos = $_POST['porcentajeotrostributos'];
                $porcentajetributoventagratuita = $_POST['porcentajetributoventagratuita'];
                $montooriginal = $_POST['montooriginal'];
                $monedaoriginal = $_POST['monedaoriginal'];
                $incluye = $_POST['incluye'];
                $porcentajedesc = $_POST['descuento'];



                $detalles = array();
//         $produpdate = array();  //////////// array de productos actualizar stock
                for ($i = 0; $i < count($codigo); $i++) {
                    $idpro = $idprod[$i];
                    $igvp = $igvprod[$i];
                    $valorunitr = $valorunitref[$i];
                    $montobaseig = $montobaseigv[$i];
                    $montobaseexp = $montobaseexpo[$i];
                    $montobaseexon = $montobaseexonerado[$i];
                    $montobaseinaf = $montobaseinafecto[$i];
                    $montobasegratu = $montobasegratuito[$i];
                    $montobaseiv = $montobaseivap[$i];
                    $montobaseotrostrib = $montobaseotrostributos[$i];
                    $tributoventagrat = $tributoventagratuita[$i];
                    $otrostrib = $otrostributos[$i];
                    $porceigv = $porcentajeigv[$i];
                    $porcotrostrib = $porcentajeotrostributos[$i];
                    $porcentajetribventgrat = $porcentajetributoventagratuita[$i];
                    $montoorig = $montooriginal[$i];
                    $descuento = $porcentajedesc[$i];
                    
                    if(empty($porcentajedesc[$i])){
                        $descuento = 0;
                    }
                    
                    if (empty($idprod[$i])) {
                        $idpro = 0;
                    }
                    if (empty($igvprod[$i])) {
                        $igvp = 0;
                    }
                    if (empty($valorunitref[$i])) {
                        $valorunitr = 0;
                    }
                    if (empty($montobaseigv[$i])) {
                        $montobaseig = 0;
                    }
                    if (empty($montobaseexpo[$i])) {
                        $montobaseexp = 0;
                    }
                    if (empty($montobaseexonerado[$i])) {
                        $montobaseexon = 0;
                    }
                    if (empty($montobaseinafecto[$i])) {
                        $montobaseinaf = 0;
                    }
                    if (empty($montobasegratuito[$i])) {
                        $montobasegratu = 0;
                    }
                    if (empty($montobaseivap[$i])) {
                        $montobaseiv = 0;
                    }
                    if (empty($montobaseotrostributos[$i])) {
                        $montobaseotrostrib = 0;
                    }
                    if (empty($tributoventagratuita[$i])) {
                        $tributoventagrat = 0;
                    }
                    if (empty($otrostributos[$i])) {
                        $otrostrib = 0;
                    }
                    if (empty($porcentajeigv[$i])) {
                        $porceigv = 0;
                    }
                    if (empty($porcentajeotrostributos[$i])) {
                        $porcotrostrib = 0;
                    }
                    if (empty($porcentajetributoventagratuita[$i])) {
                        $porcentajetribventgrat = 0;
                    }
                    if (empty($montooriginal[$i])) {
                        $montoorig = 0;
                    }
                    $d = array(
                        $idpro,
                        $codigo[$i],
                        $descripcion[$i],
                        $unidad[$i],
                        $tipoigv[$i],
                        $cantidad[$i],
                        $precio[$i],
                        $subtotal[$i],
                        $total[$i],
                        $id,
                        $igvp,
                        $valorunitr,
                        $montobaseig,
                        $montobaseexp,
                        $montobaseexon,
                        $montobaseinaf,
                        $montobasegratu,
                        $montobaseiv,
                        $montobaseotrostrib,
                        $tributoventagrat,
                        $otrostrib,
                        $porceigv,
                        $porcotrostrib,
                        $porcentajetribventgrat,
                        $montoorig,
                        $monedaoriginal[$i],
                        $incluye[$i],
                        $descuento
                    );
                    array_push($detalles, $d);

//            $produp = array(
//                $cantidad[$i],
//                $idprod[$i]
//                
//            );
//            array_push($produpdate, $produp);
                }

                $detalle = new Detalle();
//        $producto = new producto();
                $iddetalle = $detalle->insert($detalles);
//        $producto->updatestock($produpdate);

                if (isset($_POST['serieprod']) && isset($_POST['idserieitem'])) { //&& isset($_POST['idserie'])
//            var_dump($_POST['idserieitem']);
//            $idserie = $_POST['idserieitem'];
                    $idprodserie = $_POST['idserieitem'];
                    $serie = $_POST['serieprod'];

                    $seriesd = array();
                    $idupdate = array();
                    $cant = 0;
                    $cantini = 0;
                    for ($i = 0; $i < count($codigo); $i++) { //recorro cada unos de los item
                        if ($incluye[$i] == 'Si') { //// pregunto si incluye serie // 
                            $cant = $cantidad[$i]; /// recojo la cantida de series a incluir en el item
//                    echo 'cantidad '.$cant;

                            for ($j = $cantini; $j < $cant + $cantini; $j++) { //////////// series de cada item q incluir = si                           //recorro las series 
                                $da = array(
                                    $serie[$j],
//                            $idserie[$j],
                                    $iddetalle[$i],
                                    1,
                                    $idprodserie[$i]
                                );


//                        $idup = array($idserie[$j]);
//                        array_push($idupdate, $idup);
                                array_push($seriesd, $da);
                            }




                            $cantini += $cant;
//                    echo 'cant '.$cantini;
//                    echo 'j '.$j;
                        }

                        ////////////////////////////////////////////////////
//                    $series = array();
//                        for ($i = 0; $i < count($serie); $i++) { ////////////// series de producto 
//                            $d = array(
//                                $serie[$i],
//                                $idprodserie[$i],
//                                1
//                            );
//                            array_push($series, $d);
//                        }
//                        $seriem = new serieProducto();
//                        $seriem->insert($series); ////////////// insert de las series segun array input 
                    }
                    $seried = new serieDetalle();
                    $seried->insert($seriesd);
//            
                }





//            if (isset($_POST['serieidprod']) && isset($_POST['serieprod'])) {
////            var_dump($_POST['serieprod']);
//                $idprodserie = $_POST['serieidprod'];
//                $serie = $_POST['serieprod'];
//
//                $series = array();
//                for ($i = 0; $i < count($serie); $i++) {
//                    $d = array(
//                        $serie[$i],
//                        $idprodserie[$i],
//                        1
//                    );
//                    array_push($series, $d);
//                }
//                $seriem = new serieProducto();
//                $seriem->insert($series);
//            }




                if (isset($_POST['serieguia']) && isset($_POST['tipoguia'])) {
//            var_dump($_POST['serieguia']);
                    $serieguia = $_POST['serieguia'];
                    $tipoguia = $_POST['tipoguia'];

                    $guias = array();
                    for ($i = 0; $i < count($serieguia); $i++) {
                        $d = array(
                            $serieguia[$i],
                            $tipoguia[$i],
                            $id
                        );
                        array_push($guias, $d);
                    }
                    $docguias = new documentoGuia();
                    $docguias->insert($guias);
                }
                if (isset($_POST['nombreotros']) && isset($_POST['descripcionotros'])) {
//            var_dump($_POST['nombreotros']);
                    $nombre = $_POST['nombreotros'];
                    $descripcion = $_POST['descripcionotros'];

                    $otros = array();
                    for ($i = 0; $i < count($nombre); $i++) {
                        $d = array(
                            $nombre[$i],
                            $descripcion[$i],
                            $id
                        );
                        array_push($otros, $d);
                    }
                    $otrosm = new documentoOtros();
                    $otrosm->insert($otros);
                }
            } else {
                ?> 
                <script>

                    swal('No se realizo registro', 'El documento ya se encuentra emitido', 'error');
                </script>  <?php
            }


            if ($id > 0) {
                ?>

                <script>
                    VentanaCentrada('<?= base_url ?>documento/imprimircotizacion&id=<?= $id ?>', 'PDF', '', '', '', 'false');
//                        swal("Éxitosamente!", "Operación realizada correctamente.", "success");
//                        var nro = $('#txtnro').val();
//                        $('#FormularioAjaxDocumento').trigger("reset");
//                        if (nro != '' || !isNaN(nro)) {
//                            $('#txtnro').val(parseInt(nro.trim()) + 1);
//                        }
//                        $('#tabla').empty();
//                        $('#lblgravada').html('<strong>GRAVADA: </strong>  S/ 0.00');
//                        $('#lbligv').html('<strong>IGV 18%: </strong>  S/ 0.00');
//                        $('#lbltotal').html('<strong>TOTAL: </strong>    S/ 0.00');
                document.location.href= '<?= base_url.'documento/cotizacion' ?>';
                </script>
                <?php
            } else {
                ?> 
                <script>

                    swal('No se realizarón cambios', 'Por favor recargue la página', 'error');
                </script>  <?php
            }
        } else {
            ?> 
            <script>

                swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'error');
            </script>  <?php
        }
        
        //
    }else{
         ?> 
            <script>

                swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
    }
    }

    function insertsale() {

//    var_dump($_POST);
    
    
    
    
        if(permisos::rol('rol-boletafacturaventa')){
        $id = 0;

        if (isset($_POST['cbserie']) && isset($_POST['txtnro']) && isset($_POST['cbmoneda']) && isset($_POST['dpfechaemision']) && isset($_POST['dpfechavencimiento']) && isset($_POST['cbtipoop']) && isset($_POST['cbvendedor']) && isset($_POST['txtrucbuscar']) && isset($_POST['txtcliente']) && isset($_POST['txtdireccion']) && isset($_POST['txtemail']) && isset($_POST['txtordenc']) && isset($_POST['txtobservacion'])  &&
                 isset($_POST['cbtipoventa']) && isset($_POST['tipodoc']) && !empty($_POST['tipodoc']) && !empty($_POST['cbserie']) &&
                !empty($_POST['dpfechaemision']) && !empty($_POST['dpfechavencimiento']) && !empty($_POST['cbtipoop']) && !empty($_POST['cbvendedor']) &&
                !empty($_POST['cbtipoventa'])  && !empty($_POST['txtrucbuscar']) && !empty($_POST['txtcliente']) && isset($_POST['id']) 
                && isset($_POST['gratuita']) && isset($_POST['exonerado']) && isset($_POST['inafecto']) && isset($_POST['exportacion']) && isset($_POST['gravada']) 
                && isset($_POST['igv']) && isset($_POST['totaldoc']) 
                && isset($_POST['cbtipodocpersona']) && !empty($_POST['cbtipodocpersona'])) { //&& !empty($_POST['totaldoc']) && !empty($_POST['gravada']) && !empty($_POST['igv'])
            
            
            
            $serier = str_replace(PHP_EOL, ' ', $_POST['cbserie']);
            $numeror = str_replace(PHP_EOL, ' ', $_POST['txtnro']);

            $serie = ltrim($serier, '0');
            $numero = (int) ($numeror);
            $validapago = false;
            
            $emision = $_POST['dpfechaemision'];

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');

            $vencimiento = $_POST['dpfechavencimiento'];
            $dateven = DateTime::createFromFormat('d/m/Y', $vencimiento);
            $vencimientof = $dateven->format('Y-m-d');
            
            
            $emisionv = new DateTime($emisionf);
            $vencimientov = new DateTime($vencimientof);
            $diasp = $vencimientov->diff($emisionv);
            $msjpago = "Error en los metodos de pago";

            if($_POST['cbtipoventa']== "Contado" && isset($_POST['cbtipopago']) && !empty($_POST['cbtipopago']) && $_POST['cbtipopago'] == "Efectivo"){
                $validapago= true;
                $msjpago = "";
            }
            if($_POST['cbtipoventa']== "Contado" && isset($_POST['cbtipopago']) && !empty($_POST['cbtipopago']) && $_POST['cbtipopago'] == "Mixto"){
                $validapago= true;
                $msjpago = "";
            }
            if($_POST['cbtipoventa']== "Contado" && isset($_POST['cbtipopago']) && !empty($_POST['cbtipopago'])
                    && $_POST['cbtipopago'] == "Tarjeta" && isset ($_POST['cbtarjeta']) && isset ($_POST['txtnroop']) && !empty($_POST['txtnroop']) ) {
                $validapago= true;
                $msjpago = "";
            }elseif($_POST['cbtipoventa']== "Contado" && isset($_POST['cbtipopago']) && !empty($_POST['cbtipopago'])
                    && $_POST['cbtipopago'] == "Tarjeta" && isset ($_POST['cbtarjeta']) && isset ($_POST['txtnroop']) && empty($_POST['txtnroop'])) {
                $msjpago = "Para ventas al contado y con tarjeta debe ingresar el numero de operación";
            }
            
            if($_POST['cbtipoventa']== "Contado" && isset($_POST['cbtipopago']) && !empty($_POST['cbtipopago'])
                    && $_POST['cbtipopago'] == "Transferencia"  && isset ($_POST['txtnroop']) && !empty($_POST['txtnroop'])){
                $validapago= true;
                $msjpago = "";
            }elseif($_POST['cbtipoventa']== "Contado" && isset($_POST['cbtipopago']) && !empty($_POST['cbtipopago'])
                    && $_POST['cbtipopago'] == "Transferencia"  && isset ($_POST['txtnroop']) && empty($_POST['txtnroop'])) {
                $msjpago = "Para ventas al contado por transferencia debe ingresar el numero de operación";
            }
            
            if($_POST['cbtipoventa']== "Credito" && $diasp->days > 0){
                $validapago= true;
                 $msjpago = "";
            }elseif($_POST['cbtipoventa']== "Credito" && $diasp->days <= 0){
                $msjpago= "Debe ingresar una fecha de vencimiento mayor a la fecha de emisión para ventas al crédito";
            }
            
            if($validapago == true){
            if ($this->documento->duplicado($serie, $numero, 'Venta') == 'valido') {



              
                $gratuita = 0;
                $exonerado = 0;
                $inafecto = 0;
                $exportacion = 0;
                
                $totaldoc = 0;
                $igvdoc = 0;
                $gravada= 0;
                if (!empty($_POST['totaldoc'])) {
                    $totaldoc = $_POST['totaldoc'];
                }
                if (!empty($_POST['gravada'])) {
                    $gravada = $_POST['gravada'];
                }
                if (!empty($_POST['igv'])) {
                    $igvdoc = $_POST['igv'];
                }


                if (!empty($_POST['gratuita'])) {
                    $gratuita = $_POST['gratuita'];
                }
                if (!empty($_POST['exonerado'])) {
                    $exonerado = $_POST['exonerado'];
                }
                if (!empty($_POST['inafecto'])) {
                    $inafecto = $_POST['inafecto'];
                }
                if (!empty($_POST['exportacion'])) {
                    $exportacion = $_POST['exportacion'];
                }


                if (isset($_POST['incigv'])) {
                    $incigv = 1;
                } else {
                    $incigv = 0;
                }

                if (!empty($_POST['cbsujetoa'])) {
                    $sujetoa = $_POST['cbsujetoa'];
                } else {
                    $sujetoa = 0;
                }

                $documento = new documento();

//                $gravada = $_POST['gravada'];
//                $totaldoc = $_POST['totaldoc'];
//                $igvdoc = $_POST['igv'];


                $tipoventa = $_POST['cbtipoventa'];
                $moneda = $_POST['cbmoneda'];
                $fechaemision = $emisionf;
                $fechavencimiento = $vencimientof;
                $tipoventaop = $_POST['cbtipoop'];
                $idpersona = $_POST['idcliente'];

                $idpersona = $_POST['idcliente'];
                if (empty($idpersona)) {
                    $idpersona = 0;
                }
                $idusuario = $_POST['cbvendedor'];
                
                list($personatipodoc,$idpersonatipodoc) = explode('-',$_POST['cbtipodocpersona']);
                
//                $personatipodoc=$_POST['cbtipodocpersona'];
                $ruc = $_POST['txtrucbuscar'];
                $razonsocial = $_POST['txtcliente'];
                $direccion = $_POST['txtdireccion'];
                $email = $_POST['txtemail'];
                
                $norden = $_POST['txtordenc'];
                $observacion = $_POST['txtobservacion'];
                $idsucursal = $_SESSION['idsucursal'];
                $tipodoc = $_POST['tipodoc'];
                $telfijo = $_POST['txttelefono'];
                $personam = new persona();
                $personam->setIdtipodocumento($idpersonatipodoc);
                $personam->setRuc($ruc);
                $personam->setNombre($razonsocial);
                $personam->setDireccion($direccion);
                $personam->setEmail($email);
                $personam->setTelfijo($telfijo);
//                $personam->setBydefault(0);
                $personam->setAnivcumplenos('0000-00-00');
                $personam->setTipopersona(1);
                
                if($idpersona == 0){
                    $personam->insert($personam);
                    
                }else {
                    $personam->setId($idpersona);
                    $personam->updatedoc($personam);
                    
                }
                
                $tipopago = "";
                if(isset($_POST['cbtipopago'])){
                    $tipopago = $_POST['cbtipopago'];
                }
                
                $nrotipopago= "";
                if(isset($_POST['txtnroop'])){
                    $nrotipopago = $_POST['txtnroop'];
                }
                
                
                $tarjeta = "";
                if(isset($_POST['cbtarjeta'])){
                    $tarjeta = $_POST['cbtarjeta'];
                }
                $caja = new caja();
                        
                $cajault = $caja->ultimo();
                
                $documento->setSerie($serie);
                $documento->setNumero($numero);
                $documento->setMoneda($moneda);
                $documento->setFechaemision($fechaemision);
                $documento->setFechavencimiento($fechavencimiento);
                $documento->setTipoventaop($tipoventaop);
                $documento->setIdusuario($idusuario);
                $documento->setIdpersona($idpersona);
                $documento->setRuc($ruc);
                $documento->setRazonsocial($razonsocial);
                $documento->setDireccion($direccion);
                $documento->setEmail($email);
                $documento->setTelfijo($telfijo);
                $documento->setNorden($norden);
                $documento->setObservacion($observacion);
                $documento->setIdsucursal($idsucursal);
                $documento->setTipodoc('Venta');
                $documento->setTipo($tipodoc);

                $documento->setGarantia(0);
                $documento->setValidezdias(0);
                $documento->setPlazoentregadias(0);
                $documento->setCondicionpagodias(0);
                $documento->setTipopago($tipopago);
                $documento->setNrooptipopago($nrotipopago);
                $documento->setIncigv($incigv);
                $documento->setTipoventa($tipoventa);
                $documento->setMontogratuita($gratuita);
                $documento->setMontoexonerado($exonerado);
                $documento->setMontoinafecto($inafecto);
                $documento->setMontoexportacion($exportacion);
                $documento->setMontogravada($gravada);
                $documento->setMontoigv($igvdoc);
                $documento->setTotal($totaldoc);
                $documento->setPersonatipodoc($personatipodoc);
                $documento->setTarjeta($tarjeta);
                $documento->setIdcaja($cajault->getId());
                
                $id = $documento->insert($documento);

                $idprod = $_POST['id'];
                $codigo = $_POST['codigooriginal'];
                $descripcion = $_POST['descripcionprod'];
                $unidad = $_POST['unidad'];
                $tipoigv = $_POST['tipoigv'];
                $cantidad = $_POST['cantidad'];
                $precio = $_POST['precio'];
                $subtotal = $_POST['subtotal'];
                $total = $_POST['total'];
                $descuentoporc = $_POST['descuento'];


                $igvprod = $_POST['igvprod'];
                $valorunitref = $_POST['valorunitref'];
                $montobaseigv = $_POST['montobaseigv'];
                $montobaseexpo = $_POST['montobaseexpo'];
                $montobaseexonerado = $_POST['montobaseexonerado'];
                $montobaseinafecto = $_POST['montobaseinafecto'];
                $montobasegratuito = $_POST['montobasegratuito'];
                $montobaseivap = $_POST['montobaseivap'];
                $montobaseotrostributos = $_POST['montobaseotrostributos'];
                $tributoventagratuita = $_POST['tributoventagratuita'];
                $otrostributos = $_POST['otrostributos'];
                $porcentajeigv = $_POST['porcentajeigv'];
                $porcentajeotrostributos = $_POST['porcentajeotrostributos'];
                $porcentajetributoventagratuita = $_POST['porcentajetributoventagratuita'];
                $montooriginal = $_POST['montooriginal'];
                $monedaoriginal = $_POST['monedaoriginal'];
                $incluye = $_POST['incluye'];
                /////////////////////////////////////////////////////////////////////////////////////
                ////////////// Pagos mixtos ////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////////////////
                
                
                
                
                if(isset($_POST['pagomixto'])){
                    var_dump(json_decode($_POST['pagomixto'], true));
                    
                    $pagomarray =array();
                    $pagomixtom = new pagoMixto();
                            
                    $pagosmixtos = json_decode($_POST['pagomixto'], true);
                    foreach ( $pagosmixtos as  $pagomixto){
                        var_dump($pagomixto['tipodepago']);
                        $dp = array(
                            $pagomixto['tipodepago'],
                            $pagomixto['monto'],
                            $pagomixto['tarjeta'],
                            $pagomixto['nop'],
                            $id
                        );
                        array_push($pagomarray, $dp);
                    }
                    
                    $pagomixtom->insert($pagomarray);
                    
                }
                
                
                ///////////////////////////////////////////////////////
                // Detalles ///////////////////////////////////

                $detalles = array();
                $iddetalle = array();
                $kardexs =  array();  //////////// kardex
                for ($i = 0; $i < count($codigo); $i++) {
                    $idpro = $idprod[$i];
                    $igvp = $igvprod[$i];
                    $valorunitr = $valorunitref[$i];
                    $montobaseig = $montobaseigv[$i];
                    $montobaseexp = $montobaseexpo[$i];
                    $montobaseexon = $montobaseexonerado[$i];
                    $montobaseinaf = $montobaseinafecto[$i];
                    $montobasegratu = $montobasegratuito[$i];
                    $montobaseiv = $montobaseivap[$i];
                    $montobaseotrostrib = $montobaseotrostributos[$i];
                    $tributoventagrat = $tributoventagratuita[$i];
                    $otrostrib = $otrostributos[$i];
                    $porceigv = $porcentajeigv[$i];
                    $porcotrostrib = $porcentajeotrostributos[$i];
                    $porcentajetribventgrat = $porcentajetributoventagratuita[$i];
                    $montoorig = $montooriginal[$i];
                    $descuento = $descuentoporc[$i];
                    
                    if(empty($descuentoporc[$i])){
                        $descuento = 0;
                    }
                    if (empty($idprod[$i])) {
                        $idpro = 0;
                    }
                    if (empty($igvprod[$i])) {
                        $igvp = 0;
                    }
                    if (empty($valorunitref[$i])) {
                        $valorunitr = 0;
                    }
                    if (empty($montobaseigv[$i])) {
                        $montobaseig = 0;
                    }
                    if (empty($montobaseexpo[$i])) {
                        $montobaseexp = 0;
                    }
                    if (empty($montobaseexonerado[$i])) {
                        $montobaseexon = 0;
                    }
                    if (empty($montobaseinafecto[$i])) {
                        $montobaseinaf = 0;
                    }
                    if (empty($montobasegratuito[$i])) {
                        $montobasegratu = 0;
                    }
                    if (empty($montobaseivap[$i])) {
                        $montobaseiv = 0;
                    }
                    if (empty($montobaseotrostributos[$i])) {
                        $montobaseotrostrib = 0;
                    }
                    if (empty($tributoventagratuita[$i])) {
                        $tributoventagrat = 0;
                    }
                    if (empty($otrostributos[$i])) {
                        $otrostrib = 0;
                    }
                    if (empty($porcentajeigv[$i])) {
                        $porceigv = 0;
                    }
                    if (empty($porcentajeotrostributos[$i])) {
                        $porcotrostrib = 0;
                    }
                    if (empty($porcentajetributoventagratuita[$i])) {
                        $porcentajetribventgrat = 0;
                    }
                    if (empty($montooriginal[$i])) {
                        $montoorig = 0;
                    }
                    $d = array(
                        $idpro,
                        $codigo[$i],
                        $descripcion[$i],
                        $unidad[$i],
                        $tipoigv[$i],
                        $cantidad[$i],
                        $precio[$i],
                        $subtotal[$i],
                        $total[$i],
                        $id,
                        $igvp,
                        $valorunitr,
                        $montobaseig,
                        $montobaseexp,
                        $montobaseexon,
                        $montobaseinaf,
                        $montobasegratu,
                        $montobaseiv,
                        $montobaseotrostrib,
                        $tributoventagrat,
                        $otrostrib,
                        $porceigv,
                        $porcotrostrib,
                        $porcentajetribventgrat,
                        $montoorig,
                        $monedaoriginal[$i],
                        $incluye[$i],
                        $descuento
                    );
                    array_push($detalles, $d);

                   
                }

                $detalle = new Detalle();
                $producto = new producto();
                $iddetalle = $detalle->insert($detalles);
//                $producto->updatestock($produpdate);
                $kardexm = new kardex();
                $serieprodk = new serieProducto();
                $idkardex = array();
                $idkardexant = array();
                $contcant = 0;
                $contcant2 = 0;
                
                for ($j = 0; $j < count($codigo); $j++) {
                    $idp =null;
                    if(!empty($idprod[$j])){
                        $idp = $idprod[$j];
                    }
                    $cs= 0;
                    $kard = $kardexm->ultimomovimientoproducto($idp);
                    
                    array_push($idkardexant, $kard->getId());
                    $cadenaseriek = '';
                    $cadenaserie='';
                    if (isset($_POST['serieprod']) && isset($_POST['idserieitem'])) {
//                        $idserie = $_POST['idserieitem'];
                        $seriefr = $_POST['serieprod'];
                        
                        $seriesk = $serieprodk->select($kard->getId());
                        $contsk = 0;
                        
                        if ($incluye[$j] == 'Si') { //// series q se va na vender
                            $contcant = $cantidad[$j];
                            for ($o = $contcant2; $o < $contcant + $contcant2; $o++) {
                               
                                    if($cs > 0){
                                    $cadenaserie .= ' , ';
                                    }
                                    $cadenaserie .= $seriefr[$o];
                                    $cs++;
                            }
                            $contcant2 += $contcant;
                        }
//                        for ($m = 0; $m < count($seriefr); $m++){
//                            if($m > 0){
//                                $cadenaserie .= ' , ';
//                            }
//                            $cadenaserie .= $seriefr[$m];
//                        }
                        
                        
                        foreach ($seriesk as $sk){
//                            if($idserie != $sk->getId()){
                                if($contsk > 0){
                                    $cadenaseriek .= ' , ';
                                }
                                $cadenaseriek .= $sk->getSerie();
                                $contsk ++;
                                
//                            }
                            
                                
                        }
                    
                    }
                    $kardex = array(
                        $iddetalle[$j],
                        'Venta',
                        $cantidad[$j],
                        $precio[$j],
                        $idp,
                        ($kard->getStockactual()) - $cantidad[$j],
                        $kard->getStockactual(),
                        date('Y-m-d H:i:s'),
                        $_SESSION['idalmacen'],
                        $_SESSION['idempresa'],
                        $tipodoc." (".$serie."-".$numero.")",
                        $moneda,
                        $descripcion[$j],
                        $cadenaseriek,
                        $cadenaserie
                        
                    );
                    array_push($kardexs, $kardex);
                }
                
//                var_dump($kardexs);
                
                $idkardex =$kardexm->insert($kardexs);
                
                ////////////////////////////series ///
                if (isset($_POST['serieprod']) && isset($_POST['idserieitem'])) {
//                    echo 'entra al fin serie';
                    $idserie = $_POST['idserieitem'];
                    $seriek = $_POST['serieprod'];
                    
                    
                    
                    $serieinsert = array();
                    for ($k = 0; $k < count($codigo); $k++) {
                        $idp =null;
                        if(!empty($idprod[$k])){
                            $idp = $idprod[$k];
                        }
//                        echo 'idprod '.$idprod[$k];
                        
                        $kard = $kardexm->ultimomovimientoproducto($idp);
                        
//                       echo 'idkardex'.$kard->getId() ;
                        $serieskk = $serieprodk->select($idkardexant[$k]);
//                        echo 'antes de eliminar ';
//                    var_dump($serieskk);
//                        var_dump($serieskk);
                        
//                    echo 'incluye'.$incluye[$k];
                        if($incluye[$k] == 'Si'){
                            
                            for ($l = 0 ; $l < count($idserie); $l ++){
                                $contf = 0;
                                foreach ($serieskk as $sk){
                                    
                                    echo ('seriebd '.$sk->getId());
                                    echo ('seriefront'.$idserie[$l]);
                                    if($sk->getId() == $idserie[$l]){
                                        echo 'contf '.$contf;
                                        unset($serieskk[$contf]);
                                        $serieskk = array_values($serieskk);
                                        break;
                                    }
                                   $contf ++; 
                                }
//                                if($seriesk->getId() != $idserie[$l]){
//                                
//                                
//                                }
                            }
                            
//                             echo 'despuest de eliminar ';
//                            var_dump($serieskk);
                            foreach ($serieskk as $snew){
                                $a = array(
                                    $snew->getSerie(),
                                    $idkardex[$k],
                                    $_SESSION['idempresa']
                                    
                                );
                                array_push($serieinsert, $a);
                            }
                            
//                            $serieprodk->insert($serieinsert);
//                            echo 'insert serie dentro del si';
//                            var_dump($serieinsert);
                        }
                    }
//                     echo 'insert serie fuera del si';
                    $serieprodk->insert($serieinsert);
//                    $serieprodk->insert($serieinsert);
                    
                }
                


//                if (isset($_POST['serieprod']) && isset($_POST['idserie'])) {
////            var_dump($_POST['idserieitem']);
//                    $idserie = $_POST['idserieitem'];
//                    $serie = $_POST['serieprod'];
//
//                    $series = array();
//                    $idupdate = array();
//                    $cant = 0;
//                    $cantini = 0;
//                    for ($i = 0; $i < count($codigo); $i++) { //recorro cada unos de los item
//                        if ($incluye[$i] == 'Si') { //// pregunto si incluye serie // 
//                            $cant = $cantidad[$i]; /// recojo la cantida de series a incluir en el item
////                    echo 'cantidad '.$cant;
//
//                            for ($j = $cantini; $j < $cant + $cantini; $j++) {                            //recorro las series 
//                                $da = array(
//                                    $serie[$j],
////                            $idserie[$j],
//                                    $iddetalle[$i],
//                                    1,
//                                    $idserie[$j]
//                                );
//
//
//                                $idup = array($idserie[$j]);
//                                array_push($idupdate, $idup);
//                                array_push($series, $da);
//                            }
//
//                            $cantini += $cant;
////                    echo 'cant '.$cantini;
////                    echo 'j '.$j;
//                        }
//                    }
//                    $seried = new serieDetalle();
//                    $serieprod = new serieProducto();
//                    $seried->insert($series);
//                    $serieprod->updatecant($idupdate);
////            
//                }
                if (isset($_POST['serieguia']) && isset($_POST['tipoguia'])) {
//            var_dump($_POST['serieguia']);
                    $serieguia = $_POST['serieguia'];
                    $tipoguia = $_POST['tipoguia'];

                    $guias = array();
                    for ($i = 0; $i < count($serieguia); $i++) {
                        $d = array(
                            $serieguia[$i],
                            $tipoguia[$i],
                            $id
                        );
                        array_push($guias, $d);
                    }
                    $docguias = new documentoGuia();
                    $docguias->insert($guias);
                }
                if (isset($_POST['nombreotros']) && isset($_POST['descripcionotros'])) {
//            var_dump($_POST['nombreotros']);
                    $nombre = $_POST['nombreotros'];
                    $descripcion = $_POST['descripcionotros'];

                    $otros = array();
                    for ($i = 0; $i < count($nombre); $i++) {
                        $d = array(
                            $nombre[$i],
                            $descripcion[$i],
                            $id
                        );
                        array_push($otros, $d);
                    }
                    $otrosm = new documentoOtros();
                    $otrosm->insert($otros);
                }
                if ($id > 0) {
                    
                    if($tipodoc != 'nota_venta'){
                        $consumirfact = new consumirFacturacion($documento, $detalles);
                        $resp = $consumirfact->consumirapi();
                        $documento->respuestaFacturador($id, $resp);
                        $this->enviarmail($id);
                    }
                    
                    
                ?>

                <script>
                    VentanaCentrada('<?= base_url ?>documento/printticket&id=<?= $id ?>', 'Ticket', '', '', '', 'false');
//                        swal("Éxitosamente!", "Operación realizada correctamente.", "success");
//                        var nro = $('#txtnro').val();

                    document.location.href= '<?= base_url.'documento/sale' ?>';
//                        $('#FormularioAjaxDocumento').trigger("reset");
//                        if (nro != '' || !isNaN(nro)) {
//                            $('#txtnro').val(parseInt(nro.trim()) + 1);
//                        }
//                        $('#tabla').empty();
//                        $('#lblgravada').html('<strong>GRAVADA: </strong>  S/ 0.00');
//                        $('#lbligv').html('<strong>IGV 18%: </strong>  S/ 0.00');
//                        $('#lbltotal').html('<strong>TOTAL: </strong>    S/ 0.00');
                </script>
                <?php
            } else {
                ?> 
                <script>

                    swal('No se realizarón cambios', 'Por favor recargue la página', 'error');
                </script>  <?php
            }
                
                
            } else {
                ?> 
                <script>

                    swal('No se realizo registro', 'El documento ya se encuentra emitido', 'error');
                </script>  <?php
            }
            
            }else {
                ?> 
                <script>
                    var  msj = '<?= $msjpago ?>';
                    swal('No se realizo registro', msj, 'error');
                </script>  <?php
                
            }

      
        } else {
            ?> 
            <script>

                swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'error');
            </script>  <?php
        }
        
    }else{
        ?> 
            <script>

                swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
        
    }
    }

    function insertnota() {

//    var_dump($_POST);
        $id = 0;
        if (isset($_POST['cbserie']) && isset($_POST['txtnro']) && isset($_POST['cbmoneda']) && isset($_POST['dpfechaemision']) && isset($_POST['dpfechavencimiento']) && isset($_POST['cbtipoop']) && isset($_POST['txtrucbuscar']) && isset($_POST['txtcliente']) && isset($_POST['txtdireccion']) && isset($_POST['txtemail']) && isset($_POST['cbdocref']) && isset($_POST['txtserieref']) && isset($_POST['txtnumeroref']) && isset($_POST['cbidtiponota']) && isset($_POST['txtobservacion']) && !empty($_POST['cbserie']) && !empty($_POST['txtnro']) &&
                !empty($_POST['dpfechaemision']) && !empty($_POST['dpfechavencimiento']) && !empty($_POST['cbtipoop']) &&
                !empty($_POST['txtrucbuscar']) && !empty($_POST['txtcliente']) && isset($_POST['id']) && !empty($_POST['cbdocref']) && !empty($_POST['txtserieref']) 
                && !empty($_POST['txtnumeroref']) && !empty($_POST['cbidtiponota']) && !empty($_POST['txtobservacion']) && isset($_POST['gratuita']) && isset($_POST['exonerado']) 
                && isset($_POST['inafecto']) && isset($_POST['exportacion']) && isset($_POST['gravada']) && isset($_POST['igv']) && isset($_POST['totaldoc']) &&  isset($_POST['tipo']) && !empty($_POST['tipo']) && isset($_POST['cbtipodocpersona']) && !empty($_POST['cbtipodocpersona'])
                && isset($_POST['cbtipoventa']) &&  !empty($_POST['cbtipoventa'])) { //!empty($_POST['totaldoc'])&& !empty($_POST['gravada']) && !empty($_POST['igv']) &&

            $serier = str_replace(PHP_EOL, ' ', $_POST['cbserie']);
            $numeror = str_replace(PHP_EOL, ' ', $_POST['txtnro']);
            $tiponota = $_POST['tipo'];
            $serie = ltrim($serier, '0');
            $numero = (int) ($numeror);
            
            $emision = $_POST['dpfechaemision'];

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');


            $vencimiento = $_POST['dpfechavencimiento'];
            $dateven = DateTime::createFromFormat('d/m/Y', $vencimiento);
            $vencimientof = $dateven->format('Y-m-d');
            
            $emisionv = new DateTime($emisionf);
            $vencimientov = new DateTime($vencimientof);
            $diasp = $vencimientov->diff($emisionv);
            $msjpago = "";
            $validapago = false;
            
            if($_POST['cbtipoventa']== "Contado" && isset($_POST['cbtipopago']) && !empty($_POST['cbtipopago']) && $_POST['cbtipopago'] == "Efectivo"){
                $validapago= true;
                $msjpago = "";
            }
            if($_POST['cbtipoventa']== "Contado" && isset($_POST['cbtipopago']) && !empty($_POST['cbtipopago'])
                    && $_POST['cbtipopago'] == "Tarjeta" && isset ($_POST['cbtarjeta']) && isset ($_POST['txtnroop']) && !empty($_POST['txtnroop']) ) {
                $validapago= true;
                $msjpago = "";
            }elseif($_POST['cbtipoventa']== "Contado" && isset($_POST['cbtipopago']) && !empty($_POST['cbtipopago'])
                    && $_POST['cbtipopago'] == "Tarjeta" && isset ($_POST['cbtarjeta']) && isset ($_POST['txtnroop']) && empty($_POST['txtnroop'])) {
                $msjpago = "Para ventas al contado y con tarjeta debe ingresar el numero de operación";
            }
            
            if($_POST['cbtipoventa']== "Contado" && isset($_POST['cbtipopago']) && !empty($_POST['cbtipopago'])
                    && $_POST['cbtipopago'] == "Transferencia"  && isset ($_POST['txtnroop']) && !empty($_POST['txtnroop'])){
                $validapago= true;
                $msjpago = "";
            }elseif($_POST['cbtipoventa']== "Contado" && isset($_POST['cbtipopago']) && !empty($_POST['cbtipopago'])
                    && $_POST['cbtipopago'] == "Transferencia"  && isset ($_POST['txtnroop']) && empty($_POST['txtnroop'])) {
                $msjpago = "Para ventas al contado por transferencia debe ingresar el numero de operación";
            }
            
            if($_POST['cbtipoventa']== "Credito" && $diasp->days > 0){
                $validapago= true;
                 $msjpago = "";
            }elseif($_POST['cbtipoventa']== "Credito" && $diasp->days <= 0){
                $msjpago= "Debe ingresar una fecha de vencimiento mayor a la fecha de emisión para ventas al crédito";
            }
            
            if($validapago == true){

            if ($this->documento->duplicado($serie, $numero, $tiponota) == 'valido') {
                


                $gratuita = 0;
                $exonerado = 0;
                $inafecto = 0;
                $exportacion = 0;
                
                $totaldoc = 0;
                $igvdoc = 0;
                $gravada= 0;
                if (!empty($_POST['totaldoc'])) {
                    $totaldoc = $_POST['totaldoc'];
                }
                if (!empty($_POST['gravada'])) {
                    $gravada = $_POST['gravada'];
                }
                if (!empty($_POST['igv'])) {
                    $igvdoc = $_POST['igv'];
                }


                if (!empty($_POST['gratuita'])) {
                    $gratuita = $_POST['gratuita'];
                }
                if (!empty($_POST['exonerado'])) {
                    $exonerado = $_POST['exonerado'];
                }
                if (!empty($_POST['inafecto'])) {
                    $inafecto = $_POST['inafecto'];
                }
                if (!empty($_POST['exportacion'])) {
                    $exportacion = $_POST['exportacion'];
                }


                if (isset($_POST['incigv'])) {
                    $incigv = 1;
                } else {
                    $incigv = 0;
                }

                if (!empty($_POST['cbsujetoa'])) {
                    $sujetoa = $_POST['cbsujetoa'];
                } else {
                    $sujetoa = 0;
                }

                $documento = new documento();

//                $gravada = $_POST['gravada'];
//                $totaldoc = $_POST['totaldoc'];
//                $igvdoc = $_POST['igv'];

//            $serie = $_POST['cbserie'];
//            $numero = $_POST['txtnro'];
                $moneda = $_POST['cbmoneda'];
                $fechaemision = $emisionf;
                $fechavencimiento = $vencimientof;
                $tipoventaop = $_POST['cbtipoop'];


                $idpersona = $_POST['idcliente'];
                if (empty($idpersona)) {
                    $idpersona = 0;
                }

                $idusuario = $_SESSION['id'];
                list($personatipodoc,$idpersonatipodoc) = explode('-',$_POST['cbtipodocpersona']);
//                $personatipodoc = $_POST['cbtipodocpersona'];
                $ruc = $_POST['txtrucbuscar'];
                $razonsocial = $_POST['txtcliente'];
                $direccion = $_POST['txtdireccion'];
                $email = $_POST['txtemail'];
//        $norden=$_POST['txtordenc'];
                
                $personam = new persona();
                $personam->setIdtipodocumento($idpersonatipodoc);
                $personam->setRuc($ruc);
                $personam->setNombre($razonsocial);
                $personam->setDireccion($direccion);
                $personam->setEmail($email);
                $personam->setAnivcumplenos('0000-00-00');
                $personam->setTipopersona(1);

                if($idpersona == 0){
                    $personam->insert($personam);

                }else {
                    $personam->setId($idpersona);
                    $personam->updatedoc($personam);

                }
                
                
                
                $observacion = $_POST['txtobservacion'];
                $idsucursal = $_SESSION['idsucursal'];
                $tipodoc = $_POST['tipo'];
                $serieref = $_POST['txtserieref'];
                $numeroref = $_POST['txtnumeroref'];
                
                list($idtiponota,$tiponotaop) = explode('-',$_POST['cbidtiponota']);
                
                $tipoventa = $_POST["cbtipoventa"];
                
                 $tipopago = "";
                if(isset($_POST['cbtipopago'])){
                    $tipopago = $_POST['cbtipopago'];
                }
                
                $nrotipopago= "";
                if(isset($_POST['txtnroop'])){
                    $nrotipopago = $_POST['txtnroop'];
                }
                
                
                $tarjeta = "";
                if(isset($_POST['cbtarjeta'])){
                    $tarjeta = $_POST['cbtarjeta'];
                }
 
//                echo $id.'-'.$status;
                
                
//                $idtiponota = $_POST['cbidtiponota'];
                
                
                $observacion = $_POST['txtobservacion'];
                $docref = $_POST['cbdocref'];



                $documento->setSerie($serie);
                $documento->setNumero($numero);
                $documento->setMoneda($moneda);
                $documento->setFechaemision($fechaemision);
                $documento->setFechavencimiento($fechavencimiento);
                $documento->setTipoventaop($tipoventaop);
                $documento->setIdusuario($idusuario);
                $documento->setIdpersona($idpersona);
                $documento->setRuc($ruc);
                $documento->setRazonsocial($razonsocial);
                $documento->setDireccion($direccion);
                $documento->setEmail($email);
//        $documento->setNorden($norden);
                $documento->setObservacion($observacion);
                $documento->setIdsucursal($idsucursal);
                $documento->setTipodoc($tiponota);
                $documento->setTipo($tipodoc);
                $documento->setSerieref($serieref);
                $documento->setNumeroref($numeroref);
                $documento->setIdtiponota($idtiponota);
                $documento->setObservacion($observacion);
                $documento->setDocref($docref);
                $documento->setIncigv($incigv);

                $documento->setGarantia(0);
                $documento->setValidezdias(0);
                $documento->setPlazoentregadias(0);
                $documento->setCondicionpagodias(0);

                $documento->setMontogratuita($gratuita);
                $documento->setMontoexonerado($exonerado);
                $documento->setMontoinafecto($inafecto);
                $documento->setMontoexportacion($exportacion);
                $documento->setMontogravada($gravada);
                $documento->setMontoigv($igvdoc);
                $documento->setTotal($totaldoc);
                $documento->setPersonatipodoc($personatipodoc);
                $documento->setTiponotaop($tiponotaop);
                
                $documento->setTipoventa($tipoventa);
                $documento->setTipopago($tipopago);
                $documento->setNrooptipopago($nrotipopago);
                $documento->setTarjeta($tarjeta);
                
                $id = $documento->insert($documento);

                $idprod = $_POST['id'];

                $codigo = $_POST['codigooriginal'];
                $descripcion = $_POST['descripcionprod'];
                $unidad = $_POST['unidad'];
                $tipoigv = $_POST['tipoigv'];
                $cantidad = $_POST['cantidad'];
                $precio = $_POST['precio'];
                $subtotal = $_POST['subtotal'];
                $total = $_POST['total'];

                $igvprod = $_POST['igvprod'];
                $valorunitref = $_POST['valorunitref'];
                $montobaseigv = $_POST['montobaseigv'];
                $montobaseexpo = $_POST['montobaseexpo'];
                $montobaseexonerado = $_POST['montobaseexonerado'];
                $montobaseinafecto = $_POST['montobaseinafecto'];
                $montobasegratuito = $_POST['montobasegratuito'];
                $montobaseivap = $_POST['montobaseivap'];
                $montobaseotrostributos = $_POST['montobaseotrostributos'];
                $tributoventagratuita = $_POST['tributoventagratuita'];
                $otrostributos = $_POST['otrostributos'];
                $porcentajeigv = $_POST['porcentajeigv'];
                $porcentajeotrostributos = $_POST['porcentajeotrostributos'];
                $porcentajetributoventagratuita = $_POST['porcentajetributoventagratuita'];
                $montooriginal = $_POST['montooriginal'];
                $monedaoriginal = $_POST['monedaoriginal'];
                $incluye = $_POST['incluye'];



                $detalles = array();
                $iddetalle = array();
//                $produpdate = array();  //////////// array de productos actualizar stock
                for ($i = 0; $i < count($codigo); $i++) {
                    $idpro = $idprod[$i];
                    $igvp = $igvprod[$i];
                    $valorunitr = $valorunitref[$i];
                    $montobaseig = $montobaseigv[$i];
                    $montobaseexp = $montobaseexpo[$i];
                    $montobaseexon = $montobaseexonerado[$i];
                    $montobaseinaf = $montobaseinafecto[$i];
                    $montobasegratu = $montobasegratuito[$i];
                    $montobaseiv = $montobaseivap[$i];
                    $montobaseotrostrib = $montobaseotrostributos[$i];
                    $tributoventagrat = $tributoventagratuita[$i];
                    $otrostrib = $otrostributos[$i];
                    $porceigv = $porcentajeigv[$i];
                    $porcotrostrib = $porcentajeotrostributos[$i];
                    $porcentajetribventgrat = $porcentajetributoventagratuita[$i];
                    $montoorig = $montooriginal[$i];
                    if (empty($idprod[$i])) {
                        $idpro = 0;
                    }
                    if (empty($igvprod[$i])) {
                        $igvp = 0;
                    }
                    if (empty($valorunitref[$i])) {
                        $valorunitr = 0;
                    }
                    if (empty($montobaseigv[$i])) {
                        $montobaseig = 0;
                    }
                    if (empty($montobaseexpo[$i])) {
                        $montobaseexp = 0;
                    }
                    if (empty($montobaseexonerado[$i])) {
                        $montobaseexon = 0;
                    }
                    if (empty($montobaseinafecto[$i])) {
                        $montobaseinaf = 0;
                    }
                    if (empty($montobasegratuito[$i])) {
                        $montobasegratu = 0;
                    }
                    if (empty($montobaseivap[$i])) {
                        $montobaseiv = 0;
                    }
                    if (empty($montobaseotrostributos[$i])) {
                        $montobaseotrostrib = 0;
                    }
                    if (empty($tributoventagratuita[$i])) {
                        $tributoventagrat = 0;
                    }
                    if (empty($otrostributos[$i])) {
                        $otrostrib = 0;
                    }
                    if (empty($porcentajeigv[$i])) {
                        $porceigv = 0;
                    }
                    if (empty($porcentajeotrostributos[$i])) {
                        $porcotrostrib = 0;
                    }
                    if (empty($porcentajetributoventagratuita[$i])) {
                        $porcentajetribventgrat = 0;
                    }
                    if (empty($montooriginal[$i])) {
                        $montoorig = 0;
                    }
                    $d = array(
                        $idpro,
                        $codigo[$i],
                        $descripcion[$i],
                        $unidad[$i],
                        $tipoigv[$i],
                        $cantidad[$i],
                        $precio[$i],
                        $subtotal[$i],
                        $total[$i],
                        $id,
                        $igvp,
                        $valorunitr,
                        $montobaseig,
                        $montobaseexp,
                        $montobaseexon,
                        $montobaseinaf,
                        $montobasegratu,
                        $montobaseiv,
                        $montobaseotrostrib,
                        $tributoventagrat,
                        $otrostrib,
                        $porceigv,
                        $porcotrostrib,
                        $porcentajetribventgrat,
                        $montoorig,
                        $monedaoriginal[$i],
                        $incluye[$i],0
                    );
                    array_push($detalles, $d);

//                    if ($tiponota == 'nota_debito') {
//                        $cantidad = ($cantidad[$i]) * -1;
//                    } else {
//                        $cantidad = ($cantidad[$i]);
//                    }
//
//                    $produp = array(
//                        $cantidad,
//                        $idprod[$i]
//                    );
//                    array_push($produpdate, $produp);
                }

                $detalle = new Detalle();
                $producto = new producto();
                $iddetalle = $detalle->insert($detalles);
//                $producto->updatestock($produpdate);
                
                $kardexm = new kardex();
                $kardexs = array();
                for ($j = 0; $j < count($codigo); $j++) {
                    if ($tiponota == 'nota_debito') {
                        $cantidad = ($cantidad[$i]) * -1;
                    } else {
                        $cantidad = ($cantidad[$i]);
                    }
                     $idp =null;
                    if(!empty($idprod[$j])){
                        $idp = $idprod[$j];
                    }
                    
                    $kard = $kardexm->ultimomovimientoproducto($idp);
                    $kardex = array(
                        $iddetalle[$j],
                        $tiponota,
                        $cantidad[$j],
                        $precio[$j],
                        $idp,
                        ($kard->getStockactual()) + $cantidad,
                        $kard->getStockactual(),
                        date('Y-m-d H:i:s'),
                        $_SESSION['idalmacen'],
                        $_SESSION['idempresa'],
                        $tipodoc." (".$serie."-".$numero.")",
                        $moneda,
                        $descripcion[$j]
                        
                    );
                    array_push($kardexs, $kardex);
                }
                
                var_dump($kardexs);
                
                $kardexm->insert($kardexs);


                if (isset($_POST['serieprod']) && isset($_POST['idserie'])) {
//            var_dump($_POST['idserieitem']);
                    $idserie = $_POST['idserieitem'];
                    $serie = $_POST['serieprod'];

                    $series = array();
                    $idupdate = array();
                    $cant = 0;
                    $cantini = 0;
                    for ($i = 0; $i < count($codigo); $i++) { //recorro cada unos de los item
                        if ($incluye[$i] == 'Si') { //// pregunto si incluye serie // 
                            $cant = $cantidad[$i]; /// recojo la cantida de series a incluir en el item
//                    echo 'cantidad '.$cant;

                            for ($j = $cantini; $j < $cant + $cantini; $j++) {                            //recorro las series 
                                $da = array(
                                    $serie[$j],
//                            $idserie[$j],
                                    $iddetalle[$i],
                                    1,
                                    $idserie[$i]
                                );


                                $idup = array($idserie[$j]);
                                array_push($idupdate, $idup);
                                array_push($series, $da);
                            }

                            $cantini += $cant;
//                    echo 'cant '.$cantini;
//                    echo 'j '.$j;
                        }
                    }
                    $seried = new serieDetalle();
                    $seried->insert($series, $idupdate);
//            
                }
                if (isset($_POST['serieguia']) && isset($_POST['tipoguia'])) {
//            var_dump($_POST['serieguia']);
                    $serieguia = $_POST['serieguia'];
                    $tipoguia = $_POST['tipoguia'];

                    $guias = array();
                    for ($i = 0; $i < count($serieguia); $i++) {
                        $d = array(
                            $serieguia[$i],
                            $tipoguia[$i],
                            $id
                        );
                        array_push($guias, $d);
                    }
                    $docguias = new documentoGuia();
                    $docguias->insert($guias);
                }
                if (isset($_POST['nombreotros']) && isset($_POST['descripcionotros'])) {
//            var_dump($_POST['nombreotros']);
                    $nombre = $_POST['nombreotros'];
                    $descripcion = $_POST['descripcionotros'];

                    $otros = array();
                    for ($i = 0; $i < count($nombre); $i++) {
                        $d = array(
                            $nombre[$i],
                            $descripcion[$i],
                            $id
                        );
                        array_push($otros, $d);
                    }
                    $otrosm = new documentoOtros();
                    $otrosm->insert($otros);
                }

                if ($id > 0) {
                    $consumirfact = new consumirFacturacion($documento, $detalles);
                    $resp = $consumirfact->consumirapi();
                    $documento->respuestaFacturador($id, $resp);
                    ?>

                    <script>
                        VentanaCentrada('<?= base_url ?>documento/printticket&id=<?= $id ?>', 'Ticket', '', '', '', 'false');
                            swal("Éxitosamente!", "Operación realizada correctamente.", "success");
//                            var nro = $('#txtnro').val();
//                            $('#FormularioAjaxDocumento').trigger("reset");
//                            if (nro != '' || !isNaN(nro)) {
//                                $('#txtnro').val(parseInt(nro.trim()) + 1);
//                            }
//                            $('#tabla').empty();
//                            $('#lblgravada').html('<strong>GRAVADA: </strong>  S/ 0.00');
//                            $('#lbligv').html('<strong>IGV 18%: </strong>  S/ 0.00');
//                            $('#lbltotal').html('<strong>TOTAL: </strong>    S/ 0.00');
                        <?php if($tiponota == 'nota_credito') { ?>
                            document.location.href= '<?= base_url.'documento/notecredit' ?>';
                            
                        <?php } ?>
                        <?php if($tiponota == 'nota_debito') { ?>
                            document.location.href= '<?= base_url.'documento/notedebit' ?>';
                            
                        <?php } ?>
                    </script>
                    <?php
                } else {
                    ?> 
                    <script>

                        swal('No se realizarón cambios', 'Por favor recargue la página', 'error');
                    </script>  <?php
                }
            } else {
                ?> 
                <script>

                    swal('No se realizo registro', 'El documento ya se encuentra emitido', 'error');
                </script>  <?php
            }
            
            }else {
            ?> 
            <script>
                var  msj = '<?= $msjpago ?>';
                swal('No se realizo registro', msj, 'error');
            </script>  <?php
                
            }
            
        } else {
            ?> 
            <script>

                swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'error');
            </script>  <?php
        }
    }

    function updateordencompra() {

//        var_dump($_POST);

        if (isset($_POST['cbmoneda']) && isset($_POST['incigv']) && isset($_POST['dpfechaemision']) && isset($_POST['dpfechavencimiento'])  && isset($_POST['txtrucbuscar']) && isset($_POST['txtcliente']) &&
                isset($_POST['txtdireccion']) && isset($_POST['txtemail']) && isset($_POST['txtgarantia']) && !empty($_POST['cbmoneda']) && !empty($_POST['dpfechaemision']) && !empty($_POST['dpfechavencimiento'])  && !empty($_POST['txtgarantia']) && !empty($_POST['txtrucbuscar']) && !empty($_POST['txtcliente']) &&
                isset($_POST['id']) && isset($_POST['iddoc']) && !empty($_POST['iddoc']) && !is_nan($_POST['iddoc']) && isset($_POST['gratuita']) && isset($_POST['exonerado']) && isset($_POST['inafecto']) && isset($_POST['exportacion']) && isset($_POST['gravada']) && isset($_POST['igv']) && isset($_POST['totaldoc']) 
                && isset($_POST['cbtipodocpersona']) && !empty($_POST['cbtipodocpersona'])) { //&& !empty($_POST['totaldoc']) && !empty($_POST['gravada']) && !empty($_POST['igv']) && !empty($_POST['txttipocambio']) && isset($_POST['txttipocambio'])
//            $serier = str_replace(PHP_EOL, ' ', $_POST['txtserie']);
//            $numeror = str_replace(PHP_EOL, ' ', $_POST['txtnro']);
//            
//            $serie = '';
//            $numero = (int)($numeror);
            $id = $_POST['iddoc'];

//        if($this->documento->duplicado($serie, $numero,'orden_compra') == 'valido'){
            $emision = $_POST['dpfechaemision'];

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');


            $vencimiento = $_POST['dpfechavencimiento'];
            $dateven = DateTime::createFromFormat('d/m/Y', $vencimiento);
            $vencimientof = $dateven->format('Y-m-d');

            $gratuita = 0;
            $exonerado = 0;
            $inafecto = 0;
            $exportacion = 0;
            
            $totaldoc = 0;
            $igvdoc = 0;
            $gravada= 0;
            if (!empty($_POST['totaldoc'])) {
                $totaldoc = $_POST['totaldoc'];
            }
            if (!empty($_POST['gravada'])) {
                $gravada = $_POST['gravada'];
            }
            if (!empty($_POST['igv'])) {
                $igvdoc = $_POST['igv'];
            }


            if (!empty($_POST['gratuita'])) {
                $gratuita = $_POST['gratuita'];
            }
            if (!empty($_POST['exonerado'])) {
                $exonerado = $_POST['exonerado'];
            }
            if (!empty($_POST['inafecto'])) {
                $inafecto = $_POST['inafecto'];
            }
            if (!empty($_POST['exportacion'])) {
                $exportacion = $_POST['exportacion'];
            }



            if (isset($_POST['incigv'])) {
                $incigv = 1;
            } else {
                $incigv = 0;
            }

            if (!empty($_POST['cbsujetoa'])) {
                $sujetoa = $_POST['cbsujetoa'];
            } else {
                $sujetoa = 0;
            }

            $documento = new documento();
            
//            $gravada = $_POST['gravada'];
//            $totaldoc = $_POST['totaldoc'];
//            $igvdoc = $_POST['igv'];

//            $serie='orden_compra';
            $numero = $_POST['txtnro'];
            $moneda = $_POST['cbmoneda'];
            $fechaemision = $emisionf;
            $fechavencimiento = $vencimientof;
//            $tipodoc = $_POST['tipodoc'];

            $idpersona = $_POST['idcliente'];

            if (empty($idpersona)) {
                $idpersona = 0;
            }
            
            list($personatipodoc,$idpersonatipodoc) = explode('-',$_POST['cbtipodocpersona']);
            
            $idusuario = $_SESSION['id'];
//            $personatipodoc = $_POST['cbtipodocpersona'];
            $ruc = $_POST['txtrucbuscar'];
            $razonsocial = $_POST['txtcliente'];
            $direccion = $_POST['txtdireccion'];
            $email = $_POST['txtemail'];
//        $norden=$_POST['txtordenc'];
//        $observacion=$_POST['txtobservacion'];
            $idsucursal = $_SESSION['idsucursal'];
            $tipocambio = 0.00;// $_POST['txttipocambio']
            $garantia = $_POST['txtgarantia'];
            
            $personam = new persona();
            $personam->setIdtipodocumento($idpersonatipodoc);
            $personam->setRuc($ruc);
            $personam->setNombre($razonsocial);
            $personam->setDireccion($direccion);
            $personam->setEmail($email);
            $personam->setAnivcumplenos('0000-00-00');

            if($idpersona == 0){
                $personam->insert($personam);

            }else {
                $personam->setId($idpersona);
                $personam->updatedoc($personam);

            }
                
            if (empty($garantia)) {
                $garantia = 0;
            }
            $documento->setId($id);
//            $documento->setSerie($serie);
//            $documento->setNumero($numero);
            $documento->setMoneda($moneda);
            $documento->setFechaemision($fechaemision);
            $documento->setFechavencimiento($fechavencimiento);
//            $documento->setTipodoc($tipodoc);
            $documento->setIdusuario($idusuario);
            $documento->setIdpersona($idpersona);
            $documento->setRuc($ruc);
            $documento->setRazonsocial($razonsocial);
            $documento->setDireccion($direccion);
            $documento->setEmail($email);
//            $documento->setNorden($norden);
//            $documento->setObservacion($observacion);
            $documento->setIdsucursal($idsucursal);
            $documento->setTipocambio($tipocambio);
            $documento->setTipo('orden_compra');
            $documento->setGarantia($garantia);

            $documento->setIncigv($incigv);
            $documento->setValidezdias(0);
            $documento->setPlazoentregadias(0);
            $documento->setCondicionpagodias(0);
            $documento->setMontogratuita($gratuita);
            $documento->setMontoexonerado($exonerado);
            $documento->setMontoinafecto($inafecto);
            $documento->setMontoexportacion($exportacion);
            $documento->setMontogravada($gravada);
            $documento->setMontoigv($igvdoc);
            $documento->setTotal($totaldoc);
            $documento->setPersonatipodoc($personatipodoc);

            $documento->update($documento);

            $idprod = $_POST['id'];
            $codigo = $_POST['codigo'];
            $descripcion = $_POST['descripcionprod'];
            $unidad = $_POST['unidad'];
            $tipoigv = $_POST['tipoigv'];
            $cantidad = $_POST['cantidad'];
            $precio = $_POST['precio'];
            $subtotal = $_POST['subtotal'];
            $total = $_POST['total'];

            $igvprod = $_POST['igvprod'];
            $valorunitref = $_POST['valorunitref'];
            $montobaseigv = $_POST['montobaseigv'];
            $montobaseexpo = $_POST['montobaseexpo'];
            $montobaseexonerado = $_POST['montobaseexonerado'];
            $montobaseinafecto = $_POST['montobaseinafecto'];
            $montobasegratuito = $_POST['montobasegratuito'];
            $montobaseivap = $_POST['montobaseivap'];
            $montobaseotrostributos = $_POST['montobaseotrostributos'];
            $tributoventagratuita = $_POST['tributoventagratuita'];
            $otrostributos = $_POST['otrostributos'];
            $porcentajeigv = $_POST['porcentajeigv'];
            $porcentajeotrostributos = $_POST['porcentajeotrostributos'];
            $porcentajetributoventagratuita = $_POST['porcentajetributoventagratuita'];
            $montooriginal = $_POST['montooriginal'];
            $monedaoriginal = $_POST['monedaoriginal'];
            $incluye = $_POST['incluye'];

            $detalles = array();
//         $produpdate = array();  //////////// array de productos actualizar stock
            for ($i = 0; $i < count($codigo); $i++) {

                $idpro = $idprod[$i];
                $igvp = $igvprod[$i];
                $valorunitr = $valorunitref[$i];
                $montobaseig = $montobaseigv[$i];
                $montobaseexp = $montobaseexpo[$i];
                $montobaseexon = $montobaseexonerado[$i];
                $montobaseinaf = $montobaseinafecto[$i];
                $montobasegratu = $montobasegratuito[$i];
                $montobaseiv = $montobaseivap[$i];
                $montobaseotrostrib = $montobaseotrostributos[$i];
                $tributoventagrat = $tributoventagratuita[$i];
                $otrostrib = $otrostributos[$i];
                $porceigv = $porcentajeigv[$i];
                $porcotrostrib = $porcentajeotrostributos[$i];
                $porcentajetribventgrat = $porcentajetributoventagratuita[$i];
                $montoorig = $montooriginal[$i];
                if (empty($idprod[$i])) {
                    $idpro = 0;
                }
                if (empty($igvprod[$i])) {
                    $igvp = 0;
                }
                if (empty($valorunitref[$i])) {
                    $valorunitr = 0;
                }
                if (empty($montobaseigv[$i])) {
                    $montobaseig = 0;
                }
                if (empty($montobaseexpo[$i])) {
                    $montobaseexp = 0;
                }
                if (empty($montobaseexonerado[$i])) {
                    $montobaseexon = 0;
                }
                if (empty($montobaseinafecto[$i])) {
                    $montobaseinaf = 0;
                }
                if (empty($montobasegratuito[$i])) {
                    $montobasegratu = 0;
                }
                if (empty($montobaseivap[$i])) {
                    $montobaseiv = 0;
                }
                if (empty($montobaseotrostributos[$i])) {
                    $montobaseotrostrib = 0;
                }
                if (empty($tributoventagratuita[$i])) {
                    $tributoventagrat = 0;
                }
                if (empty($otrostributos[$i])) {
                    $otrostrib = 0;
                }
                if (empty($porcentajeigv[$i])) {
                    $porceigv = 0;
                }
                if (empty($porcentajeotrostributos[$i])) {
                    $porcotrostrib = 0;
                }
                if (empty($porcentajetributoventagratuita[$i])) {
                    $porcentajetribventgrat = 0;
                }
                if (empty($montooriginal[$i])) {
                    $montoorig = 0;
                }
                $d = array(
                    $idpro,
                    $codigo[$i],
                    $descripcion[$i],
                    $unidad[$i],
                    $tipoigv[$i],
                    $cantidad[$i],
                    $precio[$i],
                    $subtotal[$i],
                    $total[$i],
                    $id,
                    $igvp,
                    $valorunitr,
                    $montobaseig,
                    $montobaseexp,
                    $montobaseexon,
                    $montobaseinaf,
                    $montobasegratu,
                    $montobaseiv,
                    $montobaseotrostrib,
                    $tributoventagrat,
                    $otrostrib,
                    $porceigv,
                    $porcotrostrib,
                    $porcentajetribventgrat,
                    $montoorig,
                    $monedaoriginal[$i],
                    $incluye[$i]
                );
                array_push($detalles, $d);

//            $produp = array(
//                $cantidad[$i],
//                $idprod[$i]
//                
//            );
//            array_push($produpdate, $produp);
            }

            $detalle = new Detalle();

//        $producto = new producto();
            $detalle->delete($id);
            $iddetalle = $detalle->insert($detalles);
//        

            if (isset($_POST['serieprod']) && isset($_POST['serieidprod'])) { //&& isset($_POST['idserie'])
//            var_dump($_POST['idserieitem']);
//            $idserie = $_POST['idserieitem'];
                $idprodserie = $_POST['serieidprod'];
                $serie = $_POST['serieprod'];

                $seriesd = array();
                $idupdate = array();
                $cant = 0;
                $cantini = 0;
                for ($i = 0; $i < count($codigo); $i++) { //recorro cada unos de los item
                    if ($incluye[$i] == 'Si') { //// pregunto si incluye serie // 
                        $cant = $cantidad[$i]; /// recojo la cantida de series a incluir en el item
//                    echo 'cantidad '.$cant;

                        for ($j = $cantini; $j < $cant + $cantini; $j++) { //////////// series de cada item q incluir = si                           //recorro las series 
                            $da = array(
                                $serie[$j],
//                            $idserie[$j],
                                $iddetalle[$i],
                                1
                            );


                            array_push($seriesd, $da);
                        }




                        $cantini += $cant;
//                    echo 'cant '.$cantini;
//                    echo 'j '.$j;
                    }
                }
                $seried = new serieDetalle();
                $seried->insert($seriesd, $idupdate);
//            
            }





            if (isset($_POST['serieguia']) && isset($_POST['tipoguia'])) {
//            var_dump($_POST['serieguia']);
                $serieguia = $_POST['serieguia'];
                $tipoguia = $_POST['tipoguia'];

                $guias = array();
                for ($i = 0; $i < count($serieguia); $i++) {
                    $d = array(
                        $serieguia[$i],
                        $tipoguia[$i],
                        $id
                    );
                    array_push($guias, $d);
                }
                $docguias = new documentoGuia();
                $docguias->insert($guias);
            }
            if (isset($_POST['nombreotros']) && isset($_POST['descripcionotros'])) {
//            var_dump($_POST['nombreotros']);
                $nombre = $_POST['nombreotros'];
                $descripcion = $_POST['descripcionotros'];

                $otros = array();
                for ($i = 0; $i < count($nombre); $i++) {
                    $d = array(
                        $nombre[$i],
                        $descripcion[$i],
                        $id
                    );
                    array_push($otros, $d);
                }
                $otrosm = new documentoOtros();
                $otrosm->insert($otros);
            }

            if ($id > 0) {
                ?>

                <script>

                    swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                //                 var nro = $('#txtnro').val();
                ////                $('#FormularioAjaxDocumento').trigger("reset");
                //               if(nro != '' || !isNaN(nro)){
                //                   $('#txtnro').val(parseInt(nro.trim()) + 1);
                //               }
                //               $('#tabla').empty();
                //               $('#lblgravada').html('<strong>GRAVADA: </strong>  S/ 0.00');
                //               $('#lbligv').html('<strong>IGV 18%: </strong>  S/ 0.00');
                //               $('#lbltotal').html('<strong>TOTAL: </strong>    S/ 0.00');
                </script>
                <?php
            } else {
                ?> 
                <script>

                    swal('No se realizarón cambios', 'Por favor recargue la página', 'error');
                </script>  <?php
            }
        } else {
            ?> 
            <script>

                swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'error');
            </script>  <?php
        }
    }

    function selectmaxnro() {

//        var_dump($_POST);
        if (isset($_POST['tipo']) && isset($_POST['tipod']) && isset($_POST['serie'])) {
//            var_dump($_POST);
            $tipo = $_POST['tipo'];
            $tipod = $_POST['tipod'];
            $serie = $_POST['serie'];

            $documento = $this->documento->selectMax($tipod, $tipo, $serie);
            $numeror = str_replace(" ", "", $documento->getNumero());
            echo (int) $numeror + 1;
        }
    }

    function anular() {
        $fila = 0;
//        var_dump($_POST);
        if (isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['txtmotivo'])) {

            if (!empty($_POST['txtmotivo'])) {
                $id = $_POST['id'];
                $motivo = $_POST['txtmotivo'];
                
                $documento = new documento();
                $detdocm = new Detalle();
                $kardex = new kardex();
                $serieprodk = new serieProducto();
                $seriedetm = new serieDetalle();

                
                $docone = $documento->selectOne($id);


                if($docone->getEstadolocal() != 'Anulado'){
                        
                    $detallesdoc = $detdocm->selectOneDoc($id);


                    foreach($detallesdoc as $det){

                        $kardexinsetentrada = array();
                        $k = $kardex->ultimomovimientoproducto($det->getIdproducto());
                        $serieske = $serieprodk->select($k->getId());
                        
                        $contsp1 = 0;
                        $cadenasl1 = '';
                        foreach($serieske as $skp){
                            if($contsp1 >0 ){
                                $cadenasl1 .= ' , ';

                            }
                            $cadenasl1 .= $skp->getSerie();
                        }
                        $seriedetm->select($det->getId());
                        $contds1 =0;
                        $cadenasd1 = '';
                        foreach ($seriedetm as $sdt) {
                            if($contds1 > 0){
                                $cadenasd1 .= ' , ';
                            }
                            $cadenasd1 .= $sdt->getSerie();

                        }

                        $idinsetke=array();
                        $kard = array(
                            $det->getId(),
                            'Anulado',
                            $det->getCantidad(),
                            0,
                            $det->getIdproducto(),
                            $k->getStockactual() + $det->getCantidad() ,
                            $k->getStockactual(),
                            date('Y-m-d H:i:s'),
                            $k->getIdalmacen(),
                            $_SESSION['idempresa'],
                            $docone->getTipo()." (".$docone->getSerie()."-".$docone->getNumero().") - Motivo: ".$motivo ,
                            $det->getMonedaoriginal() ,
                            $det->getDescripcionprod(),
                            $cadenasl1,
                            $cadenasd1
                
                        );
                        
                        array_push($kardexinsetentrada, $kard );

                        $idinsetke = $kardex->insert($kardexinsetentrada);


                        $seriesdetke = $seriedetm->select($det->getId()); 

                        $serieinsertke =array();
                        foreach($seriesdetke as $d){

                                $ske = new serieProducto();
                                $ske->setSerie($d->getSerie());
                                $ske->setIdempresa($_SESSION['idempresa']);
                                $ske->setIdkardex($idinsetke[0]);

                                array_push($serieske,$ske);

            
                            }
                            $contf = 0;
                            foreach ($serieske as $sk){
                                    $dek =array(
                                        $sk->getSerie(),
                                        $idinsetke[0],
                                        $_SESSION['idempresa']



                                    );
                                    array_push( $serieinsertke,$dek);        
                                
                            }

                            $serieprodk->insert($serieinsertke);

                    }


                    if(strcmp($docone->getTipo(),'nota_venta') !== 0){
                        $anular = new anularFactura($motivo,$docone->getSerie(),$docone->getNumero(),$docone->getFechaemision(),$docone->getTipo());
                        $estado = $anular->consumirapi();
                        $fila = $this->documento->anular($id, $motivo,$estado["estadosunat"],$estado["estadolocal"]);
                    }else {
                        
                        $fila = $this->documento->anular($id, $motivo,null,'Anulado');
                    }
                    
                    
    //                $serie = $_POST['serie'];
    //                $numero = $_POST['numero'];
    //                $fecha = $_POST['fechaemision'];
    //                $tipo = $_POST['tipo'];
                    
                    
                    
                    

                    if ($fila > 0) {
                      echo 'Redireccionando ...';
                      echo "<META HTTP-EQUIV ='Refresh' CONTENT='0; URL=" . base_url . "documento/selectdocument'>";
                    } else {
                        echo '<div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Error!</strong> YA SE ENCUENTRA ANULADA
                        </div>';
                    }

                }else {
                    ?>
                    <script>
                        swal('El documento ya se encuentra anulado','Alert!',info);
                    </script>
                    <?php


                }
            } else {

                echo '<div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Error!</strong> INGRESE MOTIVO DE ANULACIÓN
                </div>';
            }
        }else {
            echo '<div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Error!</strong> ALGO SUCEDIO MAL :(
                </div>';
            
        }

    }

    function printticket() {

        if (isset($_GET['id'])) {

            $id = $_GET['id'];
            $sucursal = new sucursal();
            $usuario = new usuario();
            $detalle = new Detalle();
            $sucur = $sucursal->selectOne($_SESSION['idsucursal']);
            $document = $this->documento->selectOne($id);
            $detalles = $detalle->selectOneDoc($id);
            $user = $usuario->selectOne($document->getIdusuario());
            
//            var_dump($sucur);
            require_once 'view/CifrasEnLetras.php';
            require_once 'view/documentocabecera/ticket.php';
        }
    }
    function printticketguia() {

        if (isset($_GET['id'])) {

            $id = $_GET['id'];
            $sucursal = new sucursal();
            $usuario = new usuario();
            $detalle = new Detalle();
            $sucur = $sucursal->selectOne($_SESSION['idsucursal']);
            $document = $this->documento->selectOne($id);
            $detalles = $detalle->selectOneDoc($id);
            $user = $usuario->selectOne($document->getIdusuario());
            
//            var_dump($sucur);
            require_once 'view/CifrasEnLetras.php';
            require_once 'view/documentocabecera/guiatraslado/ticketguia.php';
        }
    }

    function imprimir() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $sucursal = new sucursal();
            $detalle = new Detalle();
             $usuario = new usuario();
             $document = $this->documento->selectOne($id);
            $sucur = $sucursal->selectOne($document->getIdsucursal());
            
            $detalles = $detalle->selectOneDoc($id);
//            require_once "vendor/autoload.php";
            $user = $usuario->selectOne($document->getIdusuario());
            require_once 'libs/phpqrcode/qrlib.php';
            require_once 'view/CifrasEnLetras.php';
//            require_once 'libs/dompdf';
            require_once 'view/documentocabecera/printA4.php';
        }
    }
    function imprimirguia() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $sucursal = new sucursal();
            $detalle = new Detalle();
             $usuario = new usuario();
            $sucur = $sucursal->selectOne($_SESSION['idsucursal']);
            $document = $this->documento->selectOne($id);
            $detalles = $detalle->selectOneDoc($id);
//            require_once "vendor/autoload.php";
            $user = $usuario->selectOne($document->getIdusuario());
            require_once 'libs/phpqrcode/qrlib.php';
            require_once 'view/CifrasEnLetras.php';
//            require_once 'libs/dompdf';
            require_once 'view/documentocabecera/guiatraslado/printguiaA4.php';
        }
    }

    function imprimircotizacion() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $sucursal = new sucursal();
            $detalle = new Detalle();
            $usuario = new usuario();
            $cuentam = new cuentaBancaria();
            $sucur = $sucursal->selectOne($_SESSION['idsucursal']);
            $document = $this->documento->selectOne($id);
            $detalles = $detalle->selectOneDoc($id);
            $cuentas = $cuentam->selectAll();
            $user = $usuario->selectOne($document->getIdusuario());
//            require_once "vendor/autoload.php";
            require_once 'libs/phpqrcode/qrlib.php';
            require_once 'view/CifrasEnLetras.php';
//            require_once 'libs/dompdf';
            require_once 'view/documentocabecera/printCotizacion.php';
        }
    }

    function imprimirexcel() {
//        var_dump($_GET);
        if (isset($_GET['dpdesde']) && isset($_GET['dphasta']) && isset($_GET['cbtipocomprobante']) && isset($_GET['txtbuscar']) && isset($_GET['txtserie']) && isset($_GET['txtnumero']) && isset($_GET['cbsucursal']) && isset($_GET['tipodoc'])) {


            $desde = $_GET['dpdesde'];
//            $dated = DateTime::createFromFormat('d/m/Y', $desde);
//            $datedf = $dated->format('Y-m-d');

            $hasta = $_GET['dphasta'];
//            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
//            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_GET['cbtipocomprobante'];
            $tipodoc = $_GET['tipodoc'];

            $buscar = $_GET['txtbuscar'];
            $serie = $_GET['txtserie'];
            $numero = $_GET['txtnumero'];
            $sucursal = $_GET['cbsucursal'];

            $documentos = $this->documento->select($desde, $hasta, $tipocomp, $tipodoc, $buscar, $serie, $numero, $sucursal);
//            var_dump($documentos);
            require_once 'view/reportes/functions/excel.php';

//            activeErrorReporting();
//            noCli();

            require_once 'plugins/PHPExcel/Classes/PHPExcel.php';
            require_once 'view/reportes/searchdocumentExcel.php';
        }
    }
    
        function imprimirexceltraslado() {
//        var_dump($_GET);
      if (isset($_GET['dpdesde']) && isset($_GET['dphasta']) && isset($_GET['cbtipocomprobante']) && isset($_GET['cbpuntopartida'])  && isset($_GET['cbdestinatario'])
                && isset($_GET['txtserie']) && isset($_GET['txtnumero']) ) { // && isset($_POST['tipodoc'])

            $desde = $_GET['dpdesde'];
//            $dated = DateTime::createFromFormat('d/m/Y', $desde);
//            $datedf = $dated->format('Y-m-d');

            $hasta = $_GET['dphasta'];
//            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
//            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_GET['cbtipocomprobante'];
           

           
            $serie = $_GET['txtserie'];
            $numero = $_GET['txtnumero'];
          
            
             $puntopartida = $_GET['cbpuntopartida'];
            $destinatario = $_GET['cbdestinatario'];

             $documentos = $this->documento->selecttraslado($desde, $hasta, 'guia_remision', 'guia_remision', $serie, $numero,$puntopartida, $destinatario);
//            var_dump($documentos);
            require_once 'view/reportes/functions/excel.php';

//            activeErrorReporting();
//            noCli();

            require_once 'plugins/PHPExcel/Classes/PHPExcel.php';
            require_once 'view/reportes/searchtrasladoExcel.php';
        }
    }

    function imprimirexcelventadetallado() {
//        var_dump($_GET);
        if (isset($_GET['dpdesde']) && isset($_GET['dphasta']) && isset($_GET['cbtipocomprobante']) && isset($_GET['txtbuscar']) && isset($_GET['txtserie']) && isset($_GET['txtnumero']) && isset($_GET['cbsucursal']) && isset($_GET['cbmoneda'])) {


            $desde = $_GET['dpdesde'];
//            $dated = DateTime::createFromFormat('d/m/Y', $desde);
//            $datedf = $dated->format('Y-m-d');

            $hasta = $_GET['dphasta'];
//            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
//            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_GET['cbtipocomprobante'];

            $moneda = $_GET['cbmoneda'];
            $buscar = $_GET['txtbuscar'];
            $serie = $_GET['txtserie'];
            $numero = $_GET['txtnumero'];
            $sucursal = $_GET['cbsucursal'];
//            
            $detallado = $this->documento->selectdetallado($desde, $hasta, $tipocomp, 'Venta', $buscar, $serie, $numero, $moneda, $sucursal);
//            var_dump($documentos);
            require_once 'view/reportes/functions/excel.php';
//            
//            activeErrorReporting();
//            noCli();

            require_once 'plugins/PHPExcel/Classes/PHPExcel.php';
            require_once 'view/reportes/venta/detalladoExcel.php';
        }
    }

    function imprimirexcelcompradetallado() {
        var_dump($_GET);
        if (isset($_GET['dpdesde']) && isset($_GET['dphasta']) && isset($_GET['cbtipocomprobante']) && isset($_GET['txtbuscar']) && isset($_GET['txtserie']) && isset($_GET['txtnumero']) && isset($_GET['cbsucursal']) && isset($_GET['cbmoneda'])) {


            $desde = $_GET['dpdesde'];
//            $dated = DateTime::createFromFormat('d/m/Y', $desde);
//            $datedf = $dated->format('Y-m-d');

            $hasta = $_GET['dphasta'];
//            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
//            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_GET['cbtipocomprobante'];

            $moneda = $_GET['cbmoneda'];
            $buscar = $_GET['txtbuscar'];
            $serie = $_GET['txtserie'];
            $numero = $_GET['txtnumero'];
            $sucursal = $_GET['cbsucursal'];
//            
            $detallado = $this->documento->selectdetallado($desde, $hasta, $tipocomp, 'Compra', $buscar, $serie, $numero, $moneda, $sucursal);
//            var_dump($documentos);
            require_once 'view/reportes/functions/excel.php';
//            
//            activeErrorReporting();
//            noCli();

            require_once 'plugins/PHPExcel/Classes/PHPExcel.php';
            require_once 'view/reportes/compra/detalladoCompraExcel.php';
        }
    }

    function imprimirexcelcuentas() {
        var_dump($_GET);
        if (isset($_GET['dpdesde']) && isset($_GET['dphasta']) && isset($_GET['txtbuscar']) && isset($_GET['cbsucursal']) && isset($_GET['cbtipo']) && !empty($_GET['dphasta']) && !empty($_GET['dpdesde']) && !empty($_GET['cbsucursal']) && !empty($_GET['cbtipo']) && isset($_GET['tipodoc']) && !empty($_GET['tipodoc'])) {


            $desde = $_GET['dpdesde'];
//            $dated = DateTime::createFromFormat('d/m/Y', $desde);
//            $datedf = $dated->format('Y-m-d');

            $hasta = $_GET['dphasta'];
//            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
//            $datehf = $dateh->format('Y-m-d');

            $buscar = $_GET['txtbuscar'];
            $idsucur = $_GET['cbsucursal'];
            $tipo = $_GET['cbtipo'];
            $tipodoc = $_GET['tipodoc'];

            $cobrar = $this->documento->selectcuentacobrar($desde, $hasta, $buscar, $idsucur, $tipo, $tipodoc);
//            var_dump($documentos);
            require_once 'view/reportes/functions/excel.php';
//            
//            activeErrorReporting();
//            noCli();

            require_once 'plugins/PHPExcel/Classes/PHPExcel.php';
            require_once 'view/reportes/cuentacobrar/cuentasExcel.php';
        }
    }

    function cuentacobrar() {
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-cuentascobrarreporte') && permisos::rol('rol-cuentascobrarreportesucur')){
            $sucursal = new sucursal();

            $sucursales = $sucursal->selectAll();
            $month = date('m');
            $year = date('Y');
            $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
            $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
            $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
            $titulo = 'Cuentas por cobrar';
            $tipodoc = 'Venta';
            $cobrar = $this->documento->selectcuentacobrar($desde, $hasta, '', $_SESSION['idsucursal'], 'Todo', 'Venta');
    //        $detallado = $this->documento->selectdetallado($desde, $hasta, 'Factura','Venta' ,'', '', '','Soles', $_SESSION['idsucursal']);
    //        var_dump($detallado);
            require_once 'view/reportes/cuentacobrar/listarcuentascobrar.php';
            
        }else {
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
    }

    function cuentapagar() {
        require_once 'view/layout/header.php';
        $sucursal = new sucursal();

        $sucursales = $sucursal->selectAll();
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
        $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
        $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
        $titulo = 'Cuentas por Pagar';
        $tipodoc = 'Compra';
        $cobrar = $this->documento->selectcuentacobrar($desde, $hasta, '', $_SESSION['idsucursal'], 'Todo', 'Compra');
//        $detallado = $this->documento->selectdetallado($desde, $hasta, 'Factura','Venta' ,'', '', '','Soles', $_SESSION['idsucursal']);
//        var_dump($detallado);
        require_once 'view/reportes/cuentacobrar/listarcuentascobrar.php';
        require_once 'view/layout/footer.php';
    }

    function searchcuenta() {
//        var_dump($_POST);

        if (isset($_POST['dpdesde']) && isset($_POST['dphasta']) && isset($_POST['txtbuscar'])  && isset($_POST['cbtipo']) && !empty($_POST['dphasta']) && !empty($_POST['dpdesde']) && !empty($_POST['cbsucursal']) && !empty($_POST['cbtipo']) && isset($_POST['tipodoc']) && !empty($_POST['tipodoc'])) {
            if(isset($_POST['cbsucursal']) && (permisos::rol('rol-cuentascobrarreportesucur') || permisos::rol('rol-cuentaspagarreportesucur'))){
                
                $idsucur = $_POST['cbsucursal'];
            }elseif(permisos::rol('rol-cuentaspagarreporte') || permisos::rol('rol-cuentascobrarreporte')){
                $idsucur = $_SESSION['idsucursal'];
            }
            
            
            $emision = $_POST['dpdesde'];

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');


            $vencimiento = $_POST['dphasta'];
            $dateven = DateTime::createFromFormat('d/m/Y', $vencimiento);
            $vencimientof = $dateven->format('Y-m-d');

            $buscar = $_POST['txtbuscar'];
            
            $tipo = $_POST['cbtipo'];
            $tipodoc = $_POST['tipodoc'];

            $cobrar = $this->documento->selectcuentacobrar($emisionf, $vencimientof, $buscar, $idsucur, $tipo, $tipodoc);

            ////// tabla ///// 
            ?>
            <table class="table  table-hover table-bordered" id="tabladocumento">
                <thead>
                    <tr>
                        <th>Serie / Numero</th>

                        <th>RUC / DNI</th>


                        <th>Nombre / Razón Social</th>
                        <th>Fecha Emisión</th>
                        <th>Fecha Vencimiento</th>
                        <th>Credito</th>
                        <th>Moneda</th>
                        <th>Total</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
  
                <tbody >
            <?php
//                         
            foreach ($cobrar as $detalle) {

                echo '<tr>';

                echo '<td>' . $detalle['documento'] . '</td>';
                echo '<td>' . $detalle['ruc'] . '</td>';
                echo '<td>' . $detalle['razonsocial'] . '</td>';
                echo '<td>' . $detalle['fechaemision'] . '</td>';

                echo '<td>' . $detalle['fechavencimiento'] . '</td>';
                echo '<td>' . $detalle['dias'] . '</td>';
                echo '<td>' . $detalle['moneda'] . '</td>';
                echo '<td>' . number_format($detalle['total'], 2) . '</td>';
                echo '<td>' . $detalle['tipoventa'] . '</td>';

                echo '<td><a href="' . base_url . 'documentopago/amortizar&id=' . $detalle['id'] . '" data-toggle="tooltip" data-placement="top" title="AMORTIZAR"><i class="material-icons" style="border:none;background: none;">attach_file</i></a></td>';

                echo '</tr>';
            }
            ?>





                </tbody>

            </table>

            <div class="pagination">
                <nav>
                    <ul class="pagination"></ul>

                </nav>

            </div>
            <script>

                var table = '#tabladocumento'
                $(document).ready(function () {

                    $('.pagination').html('')
                    var trnum = 0
                    var maxRows = 20
                    var totalRows = $(table + ' tbody tr').length
                    $(table + ' tr:gt(0)').each(function () {
                        trnum++
                        if (trnum > maxRows) {
                            $(this).hide()
                        }
                        if (trnum <= maxRows) {
                            $(this).show()

                        }



                    })
                    if (totalRows > maxRows) {
                        var pagenum = Math.ceil(totalRows / maxRows)
                        for (var i = 1; i <= pagenum; ) {
                            $('.pagination').append('<li data-page="' + i + '">\<span>' + i++ + '<span class="sr-only">(current)</span>\n\
                            </span>\</li>').show()

                        }

                    }
                    $('.pagination li:first-child').addClass('active')
                    $('.pagination li').on('click', function () {

                        var pageNum = $(this).attr('data-page')
                        var trIndex = 0;
                        $('.pagination li').removeClass('active')
                        $(this).addClass('active')
                        $(table + ' tr:gt(0)').each(function () {
                            trIndex++
                            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows)) {
                                $(this).hide()
                            } else {
                                $(this).show()
                            }

                        })

                    });


                });




            </script>    


            <?php
        }
    }
   

    function caja() {
        ?>
        <script>
        document.location.href= '<?= base_url.'caja/select' ?>';
        </script>
        <?php
//        require_once 'view/layout/header.php';
//        
//        if(permisos::rol('rol-movimientocajaoperaciones')){
//            $fecha = date('d/m/Y');
//            $fechat = date('Y-m-d');
//            $gastom = new gastos();
//
//            $resumen = $this->documento->resumen($_SESSION['idsucursal'], $fechat);
//            $gastos = $gastom->resumen($_SESSION['idsucursal'], $fechat);
//
//            require_once 'view/operaciones/form_caja.php';
//            
//        } else {
//            require_once 'view/sinpermiso.php';
//            
//        }
//        
//        require_once 'view/layout/footer.php';
    }

    function searchcaja() {


        if(permisos::rol('rol-movimientocajaoperaciones')){
        if (isset($_POST['dpfecha']) && !empty($_POST['dpfecha'])) {

            $emision = $_POST['dpfecha'];

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');
            $fechat = $emision;

            $gastom = new gastos();

            $resumen = $this->documento->resumen($_SESSION['idsucursal'], $emisionf);
            $gastos = $gastom->resumen($_SESSION['idsucursal'], $emisionf);
            ?>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">               

                    <div class="card">
                        <div class="header">

                            Ingresos
                        </div>

                        <div class="body">

                            <table class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                <th>Concepto</th>
                                <th>Monto</th>

                                </thead>

                                <tfoot>
                                <th colspan="2" class="text-center ">
                                    <h4 class="">[ Total Ingresos S/. <?= number_format($resumen['ventasoles'], 2) ?> ]
                                        - [ Total Ingresos $ <?= number_format($resumen['ventadolares'], 2) ?> ]</h4> </th>
                                </tfoot>    
                                <tbody>
                                    <tr>    
                                        <td>Ventas Soles</td>
                                        <td>S/. <?= number_format($resumen['ventasoles'], 2) ?></td>
                                    </tr>

                                    <tr>                                    
                                        <td>Ventas Dólares</td>
                                        <td>$ <?= number_format($resumen['ventadolares'], 2) ?></td>
                                    </tr>    

                                </tbody>

                            </table>

                        </div>


                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">               

                    <div class="card">
                        <div class="header">
                            Detalle

                        </div>

                        <div class="body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4 >Saldo en Soles</h4>
                                    S/. <?= number_format($resumen['ventasoles'] - ($resumen['comprasoles'] + $gastos['soles']), 2) ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Saldo en Dólares</h4>
                                    $ <?= number_format($resumen['ventadolares'] - ($resumen['compradolares'] + $gastos['dolares']), 2) ?>
                                </div>

                            </div>  




                        </div>





                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">               

                    <div class="card">
                        <div class="header">

                            Egresos
                        </div>

                        <div class="body">
                            <table class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                <th>Concepto</th>
                                <th>Monto</th>

                                </thead>


                                <tfoot>
                                <th colspan="2" class="text-center ">
                                    <h4 class="">[ Total Egresos S/. <?= number_format($resumen['comprasoles'] + $gastos['soles'], 2) ?> ]
                                        - [ Total Egresos $ <?= number_format($resumen['compradolares'] + $gastos['dolares'], 2) ?> ]</h4> </th>
                                </tfoot>    
                                <tbody>
                                    <tr>    
                                        <td>Compras Soles</td>
                                        <td>S/. <?= number_format($resumen['comprasoles'], 2) ?></td>
                                    </tr>

                                    <tr>                                    
                                        <td>Compras Dólares</td>
                                        <td>$ <?= number_format($resumen['compradolares'], 2) ?></td>
                                    </tr>  

                                    <tr>                                    
                                        <td>Gastos Soles</td>
                                        <td>S/. <?= number_format($gastos['soles'], 2) ?></td>
                                    </tr>    
                                    <tr>                                    
                                        <td>Gastos Dólares</td>
                                        <td>$ <?= number_format($gastos['dolares'], 2) ?></td>
                                    </tr>    

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">

                        <div class="body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Efectivo Soles</h4>
                                    S/. <?= number_format($resumen['efectivosoles'], 2) ?>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Efectivo Dolares</h4>
                                    $ <?= number_format($resumen['efectivodolares'], 2) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Tarjeta Soles</h4>
                                    S/. <?= number_format($resumen['tarjetasoles'], 2) ?>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Tarjeta Dolares</h4>
                                    $ <?= number_format($resumen['tarjetadollar'], 2) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Contra Entrega Soles</h4>
                                    S/. <?= number_format($resumen['contraentredolares'], 2) ?>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <h4>Contra Entrega Dolares</h4>
                                    $ <?= number_format($resumen['contraentresoles'], 2) ?>
                                </div>
                            </div>





                        </div>



                    </div>

                </div>

            </div>


            <?php
        }
        }else {
              ?> 
            <script>

                swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
            
            
        }
    }
     function crearguia(){
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-trasladooperaciones')){
        
        $almacenm = new Almacen();
        
            
            $sucursalm = new sucursal();
            $sucursales = $almacenm->selectAll();
            $sucurseriem = new documentoSucursal();
            $sucurseries= $sucurseriem->selectAll($_SESSION['idsucursal'], 'guia_remision');
            $origen = $almacenm->selectOne($_SESSION['idalmacen']);
            $detalles = array();
            $unidm = new unidmedida();
            $transm = new sunatTransaction();
            $inpum = new tipoImpuesto();
    //        $transactions = $transm->selectAll();
            $unidades = $unidm->selectAll();
            $impuestos = $inpum->selectAll();
            
            $tipocambio = new tipocambio();
            $utimotc = $tipocambio->selectMax();
            $ventatc = $utimotc->getVenta();
            require_once 'view/documentocabecera/guia/form_documento_guia.php'; 
            
        }else {
            require_once 'view/sinpermiso.php';
        }
       
        require_once 'view/layout/footer.php'; 
        
    }
    
    function insertguia(){
//    var_dump($_POST);
        
        if(permisos::rol('rol-trasladooperaciones')){
        if(isset($_POST['cbserie']) && isset($_POST['txtnro']) && isset($_POST['dpfechaentrega']) && isset($_POST['dpfechatraslado']) 
         && isset($_POST['txtmarcaplaca']) && isset($_POST['txtlicencia']) && isset($_POST['txtcertificado']) && isset($_POST['txtrazonsocialtransp']) 
                && isset($_POST['txtructransp']) && isset($_POST['txtflete']) && isset($_POST['txtmotivo'])  && isset($_POST['gratuita']) 
                         && isset($_POST['exonerado'])  && isset($_POST['inafecto'])  && isset($_POST['exportacion'])  && isset($_POST['gravada'])  && isset($_POST['igv'])
                 && isset($_POST['totaldoc']) && isset($_POST['id']) && 
                
                !empty($_POST['cbserie']) && !empty($_POST['txtnro']) && !empty($_POST['dpfechaentrega']) && !empty($_POST['dpfechatraslado']) 
                      && !empty($_POST['gravada'])  && !empty($_POST['igv'])
                 && !empty($_POST['totaldoc']) && isset($_POST['txtemisordireccion']) && !empty($_POST['txtemisordireccion'])) {
            
            $serier = str_replace(" ", "", $_POST['cbserie']);
            $numeror = str_replace(" ", "", $_POST['txtnro']);

            $serie = ltrim($serier, '0');
            $numero = (int) ($numeror);
            
            
            
            if(isset($_POST['cbdestinatario']) && isset($_POST['txtrazonsocialdestino'])  && isset($_POST['txtdirecciondestino']) && !empty($_POST['cbdestinatario'])  && !empty($_POST['txtrazonsocialdestino'])
                     && !empty($_POST['txtdirecciondestino'])){
                
                $idalmacendestino = $_POST['cbdestinatario'];
                $empresadestino = $_POST['txtrazonsocialdestino'];
                $direcciondestino = $_POST['txtdirecciondestino'];
//                $rucdestino = $_POST['txtrucdestino'];
//                list($idsucurdestino,$empresadestino,$direcciondestino,$rucdestino) = explode('-',$_POST['cbdestinatario']);
                
            }else {
                 ?> 
                <script>

                    swal('No se realizarón cambios', 'Por favor ingresa datos del destinatario', 'info');
                </script>  <?php
                
            }
            
            
 
//            echo $empresadestino.'-'.$direcciondestino.'-'.$rucdestino;
           
            if($this->documento->duplicado($serie, $numero,'guia_remision') == 'valido'){
//                $sucursalm = new sucursal();
                $almacenm = new Almacen();
                $origen = $almacenm->selectOne($_SESSION['idalmacen']);
                
                $emision = $_POST['dpfechaentrega'];

                $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
                $emisionf = $dateemis->format('Y-m-d');


                $vencimiento = $_POST['dpfechatraslado'];
                $dateven = DateTime::createFromFormat('d/m/Y', $vencimiento);
                $vencimientof = $dateven->format('Y-m-d');
                
                
                $fechaentrega = $emisionf;
                $fechatraslado= $vencimientof;
                
                $ructransp = $_POST['txtructransp'];
                $razonstransp= $_POST['txtrazonsocialtransp'];
                
                
                $direccionemisor= $_POST['txtemisordireccion'];
                $marcaplaca = $_POST['txtmarcaplaca'];
                $licencia = $_POST['txtlicencia'];
                $certificado = $_POST['txtcertificado'];
                $razonsocialtrans= $_POST['txtrazonsocialtransp'];
                $ructransp = $_POST['txtructransp'];
                $flete = $_POST['txtflete'];
                $motivo = $_POST['txtmotivo'];
                $gratuita = $_POST['gratuita'];
                $exonerado = $_POST['exonerado'];
                $inafecto = $_POST['inafecto'];
                $exportacion = $_POST['exportacion'];
                $gravada = $_POST['gravada'];
                $igv = $_POST['igv'];
                $total = $_POST['totaldoc'];
                if(empty($flete) && !is_numeric($flete)){
                    $flete = 0;
                }
                if(empty($gratuita)){
                    $gratuita = 0;
                }
                if(empty($exonerado)){
                    $exonerado = 0;
                }
                if(empty($inafecto)){
                    $inafecto = 0;
                }
                if(empty($exportacion)){
                    $exportacion = 0;
                }
              
                $guia = new documento();
                
                $guia->setSerie($serie);
                $guia->setNumero($numero);
                $guia->setFechaemision($fechatraslado);
                $guia->setFechavencimiento($fechaentrega);
              
               
                $guia->setIdsucursal($_SESSION['idsucursal']);
                $guia->setRazonsialdestinatario($empresadestino);
                $guia->setDirecciondestinatario($direcciondestino);
                $guia->setIdalmacendestino($idalmacendestino);
//                $guia->setRucdestinatario($rucdestino);
                
//                $guia->setRuc($origen->getRuc());
                $guia->setRazonsocial($origen->getNombre());
                $guia->setDireccion($direccionemisor);
                $guia->setIdalmacenemisor($_SESSION['idalmacen']);
                
                $guia->setRuctransportista($ructransp);
                $guia->setRazonsocialtransportista($razonstransp);
                $guia->setCertificdotransportista($certificado);
                
                $guia->setMarcaplaca($marcaplaca);
                $guia->setLicenciatransportista($licencia);
                
                
                $guia->setMotivoguia($motivo);
                
                $guia->setCostotransportista($flete);
                $guia->setMontogratuita($gratuita);
                $guia->setMontoexonerado($exonerado);
                $guia->setMontoinafecto($inafecto);
                $guia->setMontoexportacion($exportacion);
                $guia->setMontogravada($gravada);
                $guia->setMontoigv($igv);
                $guia->setTotal($total);
                
                
                $guia->setTipo('guia_remision');
                $guia->setTipodoc('guia_remision');
                $guia->setEstadolocal('sin_confirmar');
                
                $id = $guia->insert($guia);
                
                
                $idprod = $_POST['id'];
                $codigo = $_POST['codigooriginal'];
                $descripcion = $_POST['descripcionprod'];
                $unidad = $_POST['unidad'];
                $tipoigv = $_POST['tipoigv'];
                $cantidad = $_POST['cantidad'];
                $precio = $_POST['precio'];
                $subtotal = $_POST['subtotal'];
                $total = $_POST['total'];


                $igvprod = $_POST['igvprod'];
                $valorunitref = $_POST['valorunitref'];
                $montobaseigv = $_POST['montobaseigv'];
                $montobaseexpo = $_POST['montobaseexpo'];
                $montobaseexonerado = $_POST['montobaseexonerado'];
                $montobaseinafecto = $_POST['montobaseinafecto'];
                $montobasegratuito = $_POST['montobasegratuito'];
                $montobaseivap = $_POST['montobaseivap'];
                $montobaseotrostributos = $_POST['montobaseotrostributos'];
                $tributoventagratuita = $_POST['tributoventagratuita'];
                $otrostributos = $_POST['otrostributos'];
                $porcentajeigv = $_POST['porcentajeigv'];
                $porcentajeotrostributos = $_POST['porcentajeotrostributos'];
                $porcentajetributoventagratuita = $_POST['porcentajetributoventagratuita'];
                $montooriginal = $_POST['montooriginal'];
                $monedaoriginal = $_POST['monedaoriginal'];
                $incluye = $_POST['incluye'];
                
                $detalles = array();
                $iddetalle = array();
                $produpdate = array();  //////////// array de productos actualizar stock
                
                $updateproddesti = array(); //////////////// array de productos actualizar en la sucursal destino
                $insertproddesti = array(); /////////////// array e prouctos a insertar en la sucursal destino
                $producto = new producto();
                
                for ($i = 0; $i < count($codigo); $i++) {
                    $idpro = $idprod[$i];
                    $igvp = $igvprod[$i];
                    $valorunitr = $valorunitref[$i];
                    $montobaseig = $montobaseigv[$i];
                    $montobaseexp = $montobaseexpo[$i];
                    $montobaseexon = $montobaseexonerado[$i];
                    $montobaseinaf = $montobaseinafecto[$i];
                    $montobasegratu = $montobasegratuito[$i];
                    $montobaseiv = $montobaseivap[$i];
                    $montobaseotrostrib = $montobaseotrostributos[$i];
                    $tributoventagrat = $tributoventagratuita[$i];
                    $otrostrib = $otrostributos[$i];
                    $porceigv = $porcentajeigv[$i];
                    $porcotrostrib = $porcentajeotrostributos[$i];
                    $porcentajetribventgrat = $porcentajetributoventagratuita[$i];
                    $montoorig = $montooriginal[$i];
                    if (empty($idprod[$i])) {
                        $idpro = 0;
                    }
                    if (empty($igvprod[$i])) {
                        $igvp = 0;
                    }
                    if (empty($valorunitref[$i])) {
                        $valorunitr = 0;
                    }
                    if (empty($montobaseigv[$i])) {
                        $montobaseig = 0;
                    }
                    if (empty($montobaseexpo[$i])) {
                        $montobaseexp = 0;
                    }
                    if (empty($montobaseexonerado[$i])) {
                        $montobaseexon = 0;
                    }
                    if (empty($montobaseinafecto[$i])) {
                        $montobaseinaf = 0;
                    }
                    if (empty($montobasegratuito[$i])) {
                        $montobasegratu = 0;
                    }
                    if (empty($montobaseivap[$i])) {
                        $montobaseiv = 0;
                    }
                    if (empty($montobaseotrostributos[$i])) {
                        $montobaseotrostrib = 0;
                    }
                    if (empty($tributoventagratuita[$i])) {
                        $tributoventagrat = 0;
                    }
                    if (empty($otrostributos[$i])) {
                        $otrostrib = 0;
                    }
                    if (empty($porcentajeigv[$i])) {
                        $porceigv = 0;
                    }
                    if (empty($porcentajeotrostributos[$i])) {
                        $porcotrostrib = 0;
                    }
                    if (empty($porcentajetributoventagratuita[$i])) {
                        $porcentajetribventgrat = 0;
                    }
                    if (empty($montooriginal[$i])) {
                        $montoorig = 0;
                    }
                    
                    $d = array(
                        $idpro,
                        $codigo[$i],
                        $descripcion[$i],
                        $unidad[$i],
                        $tipoigv[$i],
                        $cantidad[$i],
                        $precio[$i],
                        $subtotal[$i],
                        $total[$i],
                        $id,
                        $igvp,
                        $valorunitr,
                        $montobaseig,
                        $montobaseexp,
                        $montobaseexon,
                        $montobaseinaf,
                        $montobasegratu,
                        $montobaseiv,
                        $montobaseotrostrib,
                        $tributoventagrat,
                        $otrostrib,
                        $porceigv,
                        $porcotrostrib,
                        $porcentajetribventgrat,
                        $montoorig,
                        $monedaoriginal[$i],
                        $incluye[$i],0
                    );
                    array_push($detalles, $d);

//                    $produp = array(
//                        ($cantidad[$i]* -1),
//                        trim($codigo[$i]),
//                        trim($descripcion[$i]),
//                        $_SESSION['idsucursal']
//                    );
//                    array_push($produpdate, $produp);
                    
//                     if($producto->duplicado($descripcion[$i],$codigo[$i],'producto',$idsucurdestino) == 0){ //// verifico si existe el producto en la sucursal de destino si es 0 insert else update
//                         $insertdest = array(
//                             null,
//                             $codigo[$i],
//                             null,
//                             null,
//                             $descripcion[$i],
//                             $unidad[$i],
//                             $monedaoriginal[$i],
//                             0,
//                             $precio[$i],
//                             $precio[$i],
//                             $cantidad[$i],
//                             0,
//                             $incluye[$i],
//                             null,
//                             null,
//                             $tipoigv[$i],
//                             'producto',
//                             0,
//                             0,
//                             0,
//                             0,
//                             1,
//                             (int)$idsucurdestino
//                             
//                         );
//                         array_push($insertproddesti, $insertdest);
//                         
//                    }else{
//                        $updatedest = array(
//                            $cantidad[$i],
//                            trim($codigo[$i]),
//                            trim($descripcion[$i]),
//                            $idsucurdestino
//                            
//                        );
//                        array_push($updateproddesti, $updatedest);
//                        
//                    }
                }
                
                $detalle = new Detalle();
                
                $iddetalle = $detalle->insert($detalles);               
                
                //////////////////////////////  UPDATE STOCK PRODUCTO //////////////
//                var_dump($produpdate);
//                $producto->updatestockporcoddescrip($produpdate);
////                var_dump($insertproddesti);
//                if(count($insertproddesti)>0){
//                    $producto->insertmultiple($insertproddesti);
//                }
//                if(count($updateproddesti)>0){
//                    $producto->updatestockporcoddescrip($updateproddesti);
//                }
                
                //////////////////////////////////////////////////////////////////
                
                
                if (isset($_POST['serieprod']) && isset($_POST['idserieitem'])) {
//            var_dump($_POST['idserieitem']);
                    $idserie = $_POST['idserieitem'];
                    $serie = $_POST['serieprod'];

                    $series = array();
                    $idupdate = array();
                    $cant = 0;
                    $cantini = 0;
                    for ($i = 0; $i < count($codigo); $i++) { //recorro cada unos de los item
                        if ($incluye[$i] == 'Si') { //// pregunto si incluye serie // 
                            $cant = $cantidad[$i]; /// recojo la cantida de series a incluir en el item
//                    echo 'cantidad '.$cant;

                            for ($j = $cantini; $j < $cant + $cantini; $j++) {                            //recorro las series 
                                $da = array(
                                    $serie[$j],
//                            $idserie[$j],
                                    $iddetalle[$i],
                                    1,
                                    $idserie[$j]
                                );


                                $idup = array($idserie[$j]);
                                array_push($idupdate, $idup);
                                array_push($series, $da);
                            }

                            $cantini += $cant;
//                    echo 'cant '.$cantini;
//                    echo 'j '.$j;
                        }
                    }
                    $seried = new serieDetalle();
//                    $serieprod = new serieProducto();
                    $seried->insert($series);
//                    $serieprod->updatecant($idupdate);
//            
                }
                
                    if ($id > 0) {
                ?>

                <script>
                       VentanaCentrada('<?= base_url ?>documento/printticketguia&id=<?= $id ?>', 'Ticket', '', '', '', 'false');

                        
                      document.location.href= '<?= base_url.'documento/crearguia' ?>';
                </script>
                <?php
            } else {
                ?> 
                <script>

                    swal('No se realizarón cambios', 'Por favor recargue la página', 'error');
                </script>  <?php
            }
               
        
            }else {
                ?> 
                <script>

                    swal('No se realizo registro', 'El documento ya se encuentra emitido', 'error');
                </script>  <?php
            }

        } else {
            ?> 
            <script>

                swal('No se realizarón cambios', 'Por favor ingrese campos obligatorios', 'error');
            </script>  <?php
        }
        
    }else {
         ?> 
            <script>

                swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
        
    }
        
        
        
        
        
        
        
    }
    
    function notificacion(){
//        $fecha = date('Y-m-d');
        $titulo = 'Cuentas por cobrar';
        $tipodoc = 'Venta';
        $cobrar = $this->documento->selectcuentacobrar('', '', '', $_SESSION['idsucursal'], 'Vencido', 'Venta');
        $pagar = $this->documento->selectcuentacobrar('', '', '', $_SESSION['idsucursal'], 'Vencido', 'Compra');
        
        ?>  
              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                <i class="material-icons">notifications</i>
                <span class="label-count" id="countnotificacion"><?= count($cobrar) + count($pagar)?></span>
            </a>
            
            <ul class="dropdown-menu">
            <li class="header">NOTIFICACIONES</li>
                <li class="body">
                    
                    <ul class="menu">
                        
                        <?php foreach ($cobrar as $c){ ?>
                         <li>
                            <a href="javascript:void(0);">
                                <div class="icon-circle bg-light-green">
                                    <i class="material-icons">call_received</i>
                                </div>
                                <div class="menu-info">
                                    <h4>[<?= $c['documento'].'] '.$c['razonsocial'] ?></h4>
                                    <p>
                                        <i class="material-icons">access_time</i> Vencimiento <?= $c['fechavencimiento'] ?> [Cobrar]
                                    </p>
                                </div>
                            </a>
                        </li>   
                            
                            
                       <?php } ?>
                        
                         <?php foreach ($pagar as $p){ ?>
                         <li>
                            <a href="javascript:void(0);">
                                <div class="icon-circle bg-red">
                                    <i class="material-icons">call_made</i>
                                </div>
                                <div class="menu-info">
                                    <h4>[<?= $p['documento'].'] '.$p['razonsocial'] ?></h4>
                                    <p>
                                        <i class="material-icons">access_time</i> Vencimiento <?= $p['fechavencimiento'] ?> [Pagar]
                                    </p>
                                </div>
                            </a>
                        </li>   
                            
                            
                       <?php } ?>
                        
                    </ul>
                </li>
            </ul>
                       
                
        <?php
       
        
        
    }
    
    public function ticketdetallado(){
        
         if (isset($_GET['dpdesde']) && isset($_GET['dphasta']) && isset($_GET['cbtipocomprobante']) && isset($_GET['txtbuscar']) && isset($_GET['txtserie']) && isset($_GET['txtnumero']) && isset($_GET['cbsucursal']) && isset($_GET['cbmoneda'])) {


            $desde = $_GET['dpdesde'];
//            $dated = DateTime::createFromFormat('d/m/Y', $desde);
//            $datedf = $dated->format('Y-m-d');

            $hasta = $_GET['dphasta'];
//            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
//            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_GET['cbtipocomprobante'];

            $moneda = $_GET['cbmoneda'];
            $buscar = $_GET['txtbuscar'];
            $serie = $_GET['txtserie'];
            $numero = $_GET['txtnumero'];
            $sucursal = $_GET['cbsucursal'];
//            
            $detallado = $this->documento->selectdetallado($desde, $hasta, $tipocomp, 'Venta', $buscar, $serie, $numero, $moneda, $sucursal);
//            var_dump($detallado);
            require_once 'view/reportes/venta/ticketdetallado.php';
//            
//            activeErrorReporting();
//            noCli();

        }
        
        
    }
    
        function printPDFdetallado() {
            
            
            if (isset($_GET['dpdesde']) && isset($_GET['dphasta']) && isset($_GET['cbtipocomprobante']) && isset($_GET['txtbuscar']) && isset($_GET['txtserie']) && isset($_GET['txtnumero']) && isset($_GET['cbsucursal']) && isset($_GET['cbmoneda'])) {


            $desde = $_GET['dpdesde'];
//            $dated = DateTime::createFromFormat('d/m/Y', $desde);
//            $datedf = $dated->format('Y-m-d');

            $hasta = $_GET['dphasta'];
//            $dateh = DateTime::createFromFormat('d/m/Y', $hasta);
//            $datehf = $dateh->format('Y-m-d');

            $tipocomp = $_GET['cbtipocomprobante'];

            $moneda = $_GET['cbmoneda'];
            $buscar = $_GET['txtbuscar'];
            $serie = $_GET['txtserie'];
            $numero = $_GET['txtnumero'];
            $sucursal = $_GET['cbsucursal'];
//            
            $detallado = $this->documento->selectdetallado($desde, $hasta, $tipocomp, 'Venta', $buscar, $serie, $numero, $moneda, $sucursal);
//            var_dump($detallado);

            require_once 'view/reportes/venta/printPDFdetallado.php';
       
    }
//    function consumir(){
//        
//        $consumir = new consumirFacturacion();
//        $consumir->consumirapi();
//        
//    }

}

  function confirmartraslado(){
//      var_dump($_POST);
  if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['iddestino']) && !empty($_POST['iddestino'])  && isset($_POST['idemisor']) && !empty($_POST['idemisor']) ){
          
          $id = $_POST['id'];
          $iddestino = $_POST['iddestino'];
          
          $detallem = new detalle();
          $producto = new producto();
          $seriedetm = new serieDetalle();
          
          $serieprodm = new serieProducto();
          
          $documentom = new documento();
          
          $documento = $documentom->selectOne($id);
          
//          $sucursalm = new sucursal();
//          $sucursallogin =  $sucursalm->selectOnebyruc($ruc);
//          $idsucurdestino = $sucursallogin->getId();
          $idsucuremisor = $_POST['idemisor'];
         
          
          $detalle = $detallem->selectOneDoc($id);
          
          
          
//          $insertproddesti = array();
//          $updateproddesti = array();
//          $produpdate = array();
//          $updateserieprod = array();
//          $insertserieprod = array();
//          $idupdateserie = array();
          
          $kardexs = array();
          
         
          
          
          if($_SESSION['idalmacen'] != $iddestino){
              
               ?> 
                <script>

                    swal('No le pertenece el traslado a este almacen', 'Alert!', 'info');
                </script>  <?php
                die();
          }
          
          $kardex = new kardex();
          $serieprodk = new serieProducto();
        //  $idkadexsalida = array();
          
          foreach ($detalle as $det){   //////////////// movimiento kardex salida ////
            $kardexinsert = array();
            $id=array();
              $k = $kardex->ultimomovimientoproductoalmacen($det->getIdproducto(),$idsucuremisor);
              $seriesk = $serieprodk->select($k->getId());
              $contd0 = 0;

              $serdet0 = $seriedetm->select($det->getId());
              $cadenaserieant0 = '';
              $cadenaseriesale = '';
              foreach($seriesk as $skk){
                    if($contd0 > 0){
                        $cadenaserieant0 .= ' , ';
                    }
                    $cadenaserieant0 .= $skk->getSerie();
              }
              $contsd = 0;
              foreach($serdet0 as $sdet){
                  if($contsd > 0){
                    $cadenaseriesale .= ' , ';
                  }
                  $cadenaseriesale .= $sdet->getSerie();
              }

              $kard = array(
                  $det->getId(),
                  'Traslado - salida',
                  $det->getCantidad(),
                  0,
                  $det->getIdproducto(),
                  $k->getStockactual() - $det->getCantidad() ,
                  $k->getStockactual(),
                  date('Y-m-d H:i:s'),
                  $idsucuremisor,
                  $_SESSION['idempresa'],
                  $documento->getTipo()." (".$documento->getSerie()."-".$documento->getNumero().")",
                  $det->getMonedaoriginal() ,
                  $det->getDescripcionprod(),
                  $cadenaserieant0,
                  $cadenaseriesale
              );
             array_push($kardexinsert, $kard );
              
            
            $id = $kardex->insert($kardexinsert);

            $seriesdet = $seriedetm->select($det->getId());

              $serieinsert =array();
              foreach($seriesdet as $d){


                $contf = 0;
                foreach ($seriesk as $sk){
                                    
                 echo ('seriebd '.$sk->getId());
                    echo ('seriefront'.$d->getIdserieprod());
                    if($sk->getId() == $d->getIdserieprod()){
                        echo 'contf '.$contf;
                        unset($seriesk[$contf]);
                        $seriesk = array_values($seriesk);
                         break;
                            }
                        $contf ++; 
                    }

                    echo 'dikardex '.$id[0];

                    foreach ($seriesk as $snew){
                        $a = array(
                            $snew->getSerie(),
                            $id[0],
                            $_SESSION['idempresa']
                            
                        );
                        array_push($serieinsert, $a);
                    }
                 


              }
              $serieprodk->insert($serieinsert); 

            }





          foreach ($detalle as $det){   //////////////// movimiento kardex entrada ////
            $kardexinsetentrada = array();
              $k = $kardex->ultimomovimientoproductoalmacen($det->getIdproducto(),$iddestino);
              $serieske = $serieprodk->select($k->getId());
              
              $contsp1 = 0;
              $cadenasl1 = '';
              foreach($serieske as $skp){
                if($contsp1 >0 ){
                    $cadenasl1 .= ' , ';

                }
                $cadenasl1 .= $skp->getSerie();
              }
              $seriedetm->select($det->getId());
              $contds1 =0;
              $cadenasd1 = '';
              foreach ($seriedetm as $sdt) {
                if($contds1 > 0){
                    $cadenasd1 .= ' , ';
                }
                $cadenasd1 .= $sdt->getSerie();

              }

              $idinsetke=array();
              $kard = array(
                  $det->getId(),
                  'Traslado - entrada',
                  $det->getCantidad(),
                  0,
                  $det->getIdproducto(),
                  $k->getStockactual() + $det->getCantidad() ,
                  $k->getStockactual(),
                  date('Y-m-d H:i:s'),
                  $iddestino,
                  $_SESSION['idempresa'],
                  $documento->getTipo()." (".$documento->getSerie()."-".$documento->getNumero().")",
                  $det->getMonedaoriginal() ,
                  $det->getDescripcionprod(),
                  $cadenasl1,
                  $cadenasd1
    
              );
              
               array_push($kardexinsetentrada, $kard );

               $idinsetke = $kardex->insert($kardexinsetentrada);


               $seriesdetke = $seriedetm->select($det->getId()); 

               $serieinsertke =array();
               foreach($seriesdetke as $d){

                    $ske = new serieProducto();
                    $ske->setSerie($d->getSerie());
                    $ske->setIdempresa($_SESSION['idempresa']);
                    $ske->setIdkardex($idinsetke[0]);

                    array_push($serieske,$ske);

 
                }
                 $contf = 0;
                 foreach ($serieske as $sk){
                        $dek =array(
                            $sk->getSerie(),
                            $idinsetke[0],
                            $_SESSION['idempresa']



                        );
                        array_push( $serieinsertke,$dek);        
                    
                  }

                  $serieprodk->insert($serieinsertke);

          }
          
       //   $respkardex = $kardex->insert($kardexs);
          
//          foreach ($detalle as $det){
//              
//              
//              if($det->getIncluyeserie() == 'Si'){  // si incluye serie -- update serie_producto set cantidad=0 where id = $det->getIdserieprod() 
//                   $series =  $seriedetm->select($det->getId());
//                     foreach ($series as $serie){
//                            $idup=array(
//                                $serie->getIdserieprod()
//                              );
//                         
//                     }
// 
//                  array_push($idupdateserie, $idup);
//                  
//              }else {
//                  $produp = array(
//                        ($det->getCantidad()* -1),
//                        trim($det->getCodigoprod()),
//                        
//                        $idsucuremisor
//                    );
//                    array_push($produpdate, $produp);  // si no incluye serie  stock -1 del producto
//                  
//              }
                
              
//              if($producto->duplicadobycodigo($det->getCodigoprod(),'producto',$idsucurdestino) == 0){ //// verifico si existe el producto en la sucursal de destino si es 0 insert else update
//                  
//                  $prod = new producto();
//                  $prod->setCodigo($det->getCodigoprod());
//                  $prod->setDescripcion($det->getDescripcionprod());
//                  $prod->setIdunidmedida($det->getIdunidad());
//                  $prod->setMoneda($det->getMonedaoriginal());
//                  $prod->setPreciov($det->getPrecio());
//                  $prod->setStock(0);
//                  $prod->setIncluir($det->getIncluyeserie());
//                  $prod->setIdtipoimpuesto($det->getIdimpuesto());
//                  $prod->setProdservi('producto');
//                  $prod->setSucursal((int)$idsucurdestino);
//                  $prod->setPreciovmin(null);
//                  $prod->setPrecioc(null);
//                  $prod->setIdmarca(null);
//                  $prod->setIdcategoria(null);
//                  $prod->setIdlinea(null);
//                  $prod->setObservacion("");
//                  $prod->setCodigoalternativo("");
//                  $prod->setDescripciondos("");
//                          
//                          
//                  
//                  $idprod = $prod->insert($prod);
//
//                 
//                 if($det->getIncluyeserie() == 'Si'){
//                    $series =  $seriedetm->select($det->getId());
//                     foreach ($series as $serie){
//                         $serieprodinsert = array(
//                             $serie->getSerie(),
//                             $idprod,
//                             1
//                                 
//                            );
//                         
//                     }
//                     array_push($insertserieprod, $serieprodinsert);
//                 }else{
//                     $produp = array(
//                        ($det->getCantidad()),
//                        trim($det->getCodigoprod()),
//                      
//                        $idsucurdestino
//                    );
//                    array_push($produpdate, $produp);  // si no incluye serie  stock +1 del producto
//                 }
//
//            }else{
//                
//                $idproddestino = $producto->selectcodsucursal($det->getCodigoprod(), $idsucurdestino);
//                if($det->getIncluyeserie() == 'Si'){
//                    $series =  $seriedetm->select($det->getId());
//                     foreach ($series as $serie){
//                         $serieprodinsert = array(
//                             $serie->getSerie(),
//                             $idproddestino,
//                             1
//                                 
//                            );
//                         
//                     }
//                     array_push($insertserieprod, $serieprodinsert);
//                 }else{
//                        $updatedest = array(
//                        $det->getCantidad(),
//                        trim($det->getCodigoprod()),
//                        
//                        $idsucurdestino
//
//                        );
//                        array_push($produpdate, $updatedest);
//                     
//                 }
//                
//                 
//
//
//            }
//          
//              
//              
//        }
//        
//        //////////////////////////////  UPDATE STOCK PRODUCTO //////////////
////                var_dump($produpdate);
//                if(count($insertserieprod)>0){
//                    
//                    $serieprodm->insert($insertserieprod);
//                }
//                
////                var_dump($idupdateserie);
//                if(count($idupdateserie)>0){
//                    $serieprodm->updatecant($idupdateserie);
//                    
//                }
//        
//        
//        
//        
//                if(count($produpdate) >0){
//                    $producto->updatestockbycod($produpdate);
//                }
                

               
                
       //////////////////////////////////////////////////////////////////
    //     if(count($respkardex) > 0){
      //       $this->documento->updatestadolocal($id, "confirmado");
             ?> 
            <script>

       
       //        swal("Éxitosamente!", "Operación realizada correctamente.", "success");
    //           document.location.href= '<?= base_url.'documento/selectguiatraslado' ?>';
            </script>  <?php
      //  }
        
    }   
  }

  function enviarmail($id){ //iddocumento

    $documentom = new documento();


    $doc = $documentom->selectOne($id);

    $sucursal = new sucursal();
    $detalle = new Detalle();
    $usuario = new usuario();
    $document = $this->documento->selectOne($id);
    $sucur = $sucursal->selectOne($document->getIdsucursal());
    
    $detalles = $detalle->selectOneDoc($id);
//            require_once "vendor/autoload.php";
    $user = $usuario->selectOne($document->getIdusuario());
    require_once 'libs/phpqrcode/qrlib.php';
    require_once 'view/CifrasEnLetras.php';
//           require_once 'libs/dompdf';
    require_once 'view/documentocabecera/PDFmail.php';
    return $msgresult;
  }

  function reenviarmail(){
    if(isset($_POST['id']) && !empty($_POST['id'])){

        $id = $_POST['id'];
        $msj = $this->enviarmail($id);
       
        ?> 
        <script>
            swal('<?= $msj ?>','','info');
        </script>
        <?php
        

    }else {
       
        ?> 
        <script>
            swal('No se encontro registro','alert!','error');
        </script>
        <?php

    }


  }


}
