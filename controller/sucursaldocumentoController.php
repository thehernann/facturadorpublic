<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sucursaldocumentoController
 *
 * @author HERNAN
 */
require_once 'model/documentoSucursal.php';
require_once 'model/sucursal.php';
class sucursaldocumentoController {
    //put your code here
    private $documento;
    private $sucursal;
    function __construct() {
        $this->documento= new documentoSucursal();
        $this->sucursal=new sucursal();
        
    }
    
    function select(){
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-documentolistar')){
            $sucursalesdoc =$this->documento->select();
        
            require_once 'view/documentosucursal/listar_sucursaldoc.php';  
            
        }else {
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
        
    }
    
    function cargar(){
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-documentoeditar')){
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $sucursaldoc =$this->documento->selectOne($id);
            $sucursales= $this->sucursal->selectAll();
//            var_dump($sucursales);
//            var_dump($sucursaldoc);
            
            require_once 'view/documentosucursal/form_sucursaldoc.php';
        }else {
            require_once 'view/error.php';
        }
            
   
        }else {
            require_once 'view/sinpermiso.php';
        }
        require_once 'view/layout/footer.php';
    }
    function crear(){
        
        require_once 'view/layout/header.php';
         if(permisos::rol('rol-documentonuevo')){
            $sucursaldoc =new documentoSucursal();
            $sucursales= $this->sucursal->selectAll();
//            var_dump($sucursales);
//            var_dump($sucursaldoc);
            
            require_once 'view/documentosucursal/form_sucursaldoc.php';
            
   
         }else{
             require_once 'view/sinpermiso.php';
         }
        require_once 'view/layout/footer.php';
    }
    function cargarserie(){
        if(isset($_POST['tipodoc'])){
            $idsucur = $_SESSION['idsucursal'];
            $tipodoc = $_POST['tipodoc'];
            $documentossuc =$this->documento->selectAll($idsucur,$tipodoc);
            
            echo '<select class="form-control show-tick" id="cbserie" name="cbserie" required onchange="ultimonumerodoc('."'".base_url.'documento/selectmaxnro'."'".');">';
                if(count($documentossuc) == 0){
                   echo '<option value="" class="text-danger">- Sin resultados  -</option>';
                }
//                
                
                foreach ($documentossuc as $docsucur) {

                    echo '<option value="' . $docsucur->getSerie() . '">' .$docsucur->getSerie() . '</option>';
                }
                
            echo '</select>'; 
        }
        
    }
    function insert(){
        
        if(permisos::rol('rol-documentonuevo')){
        if(isset($_POST['cbsucursal']) && isset($_POST['cbtipodoc']) && isset($_POST['txtserie'])  && isset($_POST['cbpredeterminado']) 
                && isset($_POST['cbprint']) && !empty($_POST['cbsucursal']) && !empty($_POST['cbtipodoc']) && !empty($_POST['txtserie'])   
                && !empty($_POST['cbprint'])){
            $doc = new documentoSucursal();
            $doc->setIdsucursal($_POST['cbsucursal']);
            $doc->setTipodoc($_POST['cbtipodoc']);
            $doc->setSerie($_POST['txtserie']);
            $predeterminado = 0;
            if(!empty($_POST['cbpredeterminado'])){
                $predeterminado = $_POST['cbpredeterminado'];
            }
            $doc->setPredeterminado($predeterminado);
            $doc->setModoimpresion($_POST['cbprint']);
            
            $fila= $this->documento->insert($doc);
            
           if($fila >0){
             ?> 
            <script>

                 swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                    $('#FormularioAjax').trigger("reset");
            </script>  <?php
                    
                
            }else {
                 ?> 
                <script>

                     swal('Error', 'No se realizarón cambios.', 'error');
                     
                </script>  <?php
            }
            
        }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php 
            
        }
    }else{
         ?> 
            <script>
               
                 swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php 
        
    }
        
    }
    
    function update(){
        
        if(permisos::rol('rol-documentoeditar')){
           if(isset($_POST['cbsucursal']) && isset($_POST['cbtipodoc']) && isset($_POST['txtserie'])   
                && isset($_POST['cbprint']) && isset($_POST['id']) && !empty($_POST['cbsucursal']) && !empty($_POST['cbtipodoc']) && !empty($_POST['txtserie'])   
                && !empty($_POST['cbprint']) && !empty($_POST['id']) && is_numeric($_POST['id'])){
               
            $id = (int)str_replace(" ", "", trim($_POST['id']));
            
            if(!isset($_POST['cbpredeterminado'])){
                $predeterminado = 0;
            }else {
                $predeterminado = 1;
            }
            
            $doc = new documentoSucursal();
            $doc->setIdsucursal($_POST['cbsucursal']);
            $doc->setTipodoc($_POST['cbtipodoc']);
            $doc->setSerie($_POST['txtserie']);
            $doc->setPredeterminado($predeterminado);
            $doc->setModoimpresion($_POST['cbprint']);
            $doc->setId($id);
            $fila = $this->documento->update($doc);
            
            if($fila >0){
             ?> 
            <script>

                 swal('Éxitosamente', 'Operación realizada correctamente.', 'success');

            </script>  <?php
                    
                
            }else {
                 ?> 
                <script>

                     swal('Error', 'No se realizarón cambios.', 'error');
                    
                </script>  <?php
            }
            
        }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php 
            
        }
        
    }else {
            ?> 
            <script>
               
                 swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php 
            
        }
        
    }
    
        function delete() {
        
            
        if(permisos::rol('rol-documentoeliminar')){
        if(isset($_POST['id']) && !empty($_POST['id']) && is_numeric($_POST['id'])){
//            var_dump($_POST);
            $id = $_POST['id'];
            $fila =$this->documento->delete($id);
            
            
//            echo 'fila '.$fila;
                    if($fila!=0 ){
                        echo '<script>swal("Éxitosamente!", "Operación realizada correctamente.", "success");</script>';
                        echo "<META HTTP-EQUIV='Refresh' CONTENT='0; URL=".base_url."sucursaldocumento/select'>";
//                        header("Location:".base_url."producto/selectserv");
   
                    }else {
                        
                        echo '<script>swal("No se realizarón cambios!", "Algo sucedio mal :(", "error");</script>';
                    }
            
            
        }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Algo sucedio mal :(', 'error');
            </script>  <?php 
            
        }
        
        }else{
             ?> 
            <script>
               
                 swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php 
        }
        
        
    }

}
