<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cajaController
 *
 * @author HERNAN
 */
require_once 'model/caja.php';
require_once 'model/gastos.php';
require_once 'model/sucursal.php';
require_once 'model/usuario.php';
require_once 'model/pagoMixto.php';
class cajaController {
    //put your code here
    
     function select() {
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-movimientocajaoperaciones')){
            $fecha = date('d/m/Y');
            $fechat = date('Y-m-d');
//            $gastom = new gastos();
            $caja = new caja;
            
            $responsables= array();
          
            $responsables = $caja->responsable($fechat);
            
            
            $responsable = reset($responsables);
            
            
            
//            $resumen = $caja->select($fechat,$responsable['id'],$_SESSION['idsucursal']);
//            
//            $gastos = $gastom->resumen($resumen['idcaja']);
            
//            var_dump($gastos);
            
            
            
           
            
//            var_dump($resumen);
//            var_dump($gastos);

            require_once 'view/operaciones/form_caja.php';
            require_once 'view/operaciones/modalingresoegreso.php';
            require_once 'view/operaciones/modalmostraringresoegreso.php';
            
        } else {
            require_once 'view/sinpermiso.php';
            
        }
        
        require_once 'view/layout/footer.php';
    }
    
    function  report(){
        require_once 'view/layout/header.php';
        
//        $month = date('m');
//        $year = date('Y');
//        $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
//        $desde = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
//        $hasta = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
        
//        $caja  = new caja();
        $sucursal = new sucursal();
        $usuario = new usuario();
        
        $usuarios = $usuario->selectAll();
        
        
        $sucursales = $sucursal->selectAll();
        
//        $items = $caja->selectreport($desde, $hasta, "", $_SESSION['idsucursal']);
//        var_dump($items);
        
        
        require_once 'view/operaciones/listar_caja.php';
        require_once 'view/operaciones/modaldetallecaja.php';
        
        require_once 'view/layout/footer.php';
    }
    
    function searchreport(){
        $result = array();
        
        
        if(isset($_POST['dpdesde']) && isset($_POST['dphasta']) && isset($_POST['cbsucursal']) && isset($_POST['cbusuario'])){
           
            $caja  = new caja();
            $desde = DateTime::createFromFormat('d/m/Y', $_POST['dpdesde']);
            $desdef = $desde->format('Y-m-d');
            
            $usuario = $_POST['cbusuario'];
            $sucursal = $_POST['cbsucursal'];

            $hasta = DateTime::createFromFormat('d/m/Y', $_POST['dphasta']);
            $hastaf = $hasta->format('Y-m-d');
            
            $items = $caja->selectreport($desdef, $hastaf, $usuario, $sucursal);
           
            
            foreach ($items as $item){
                
                    $estadosol='';
                    $estadodolar ='';
                                        
                    if($item['estado'] == 'CERRADA'){
                        if($item['montosoles'] == $item['cierre_soles']){

                           $estadosol = '<div class="alert alert-success" style="">
                                    <strong>Exacto</strong></div>';


                        }
                        if($item['montodolares'] == $item['cierre_dolares']){

                           $estadodolar = '<div class="alert alert-success">
                                    <strong>Exacto</strong></div>';

                        }

                        if($item['montodolares'] >= $item['cierre_dolares']){

                           $estadodolar = '<div class="alert alert-danger">
                                    <strong>Faltante</strong></div>';

                        }
                        if($item['montosoles'] >= $item['cierre_soles']){

                           $estadosol = '<div class="alert alert-danger" >
                                    <strong>Faltante</strong></div>';

                        }


                        if($item['montodolares'] <= $item['cierre_dolares']){

                           $estadodolar = '<div class="alert alert-danger">
                                    <strong>Sobrante</strong></div>';

                        }
                        if($item['montosoles'] <= $item['cierre_soles']){

                           $estadosol = '<div class="alert alert-danger">
                                    <strong>Sobrante</strong></div>';

                        }
                    }


                
                
                $r = [
                    "idcaja"=>$item['idcaja'],
                    "sucursal"=>$item['sucursal'],
                    "fecha_apertura"=>$item['fecha_apertura'],
                    "usuario"=>$item['usuario'],
                    "estado"=>$item['estado'],
                    "cierre_soles"=> number_format($item['cierre_soles'], 2),
                    "cierre_dolares"=> number_format($item['cierre_dolares'],2),
                    "montosoles"=> number_format(($item['montosoles'] + $item['ingresosoles'] + $item['apertura_soles']) - $item['egresosoles'],2),
                    "montodolares"=> number_format(($item['montodolares'] + $item['ingresodolares'] + $item['apertura_dolares']) - $item['egresodolares'],2),
                    "estadosol"=>$estadosol,
                    "estadodolar"=>$estadodolar,
                    "imprimir"=>'<a href="#" onclick="VentanaCentrada(' . "'" . base_url . 'caja/printticket&id=' . $item['idcaja'] . "'" . ',' . "'" . 'Ticket' . "'" . ',' . "''" . ',' . "''" . ',' . "''" . ',' . "'false'" . ');" data-toggle="tooltip"  data-placement="top" title="Imprimir"><i class="material-icons">confirmation_number</i></a>',
                    "acciones"=>'<a  href="'.base_url.'caja/printExcel&id='.$item['idcaja'].'" title="Exportar Excel"><i class="material-icons">file_download</i></a>'
                    . '<a  href="#" data-toggle="modal" data-target=".modaldetallecaja" data-id="'.$item['idcaja'].'" title="Ver resumen"><i class="material-icons">list</i></a>'
                    
                    
                    
                    
                ];
                
                array_push($result,$r );
                
            }
            
            
        }
        
        echo '{"data":'.json_encode($result).'}';
        
        
    }
            
    
     function cajabyid() {
//         var_dump($_POST);
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            
            $id = $_POST['id'];
         
            $caja = new caja;
            $resumen = $caja->selectbyid($id);
            
            $pmixtom = new pagoMixto();
            $pmixto =$pmixtom->selectbycaja($id);
                   
            
//            var_dump($resumen);
            $respuesta = array(
                "idcaja"=>$resumen['idcaja'],
                "estado"=>$resumen['estado'],
                "cierredolares"=>$resumen['cierre_dolares'],
                "cierresoles"=>$resumen['cierre_soles'],
                
                "efectivosoles" => $resumen['montosolesefectivo'] + $pmixto['mixtoefectivosoles'],
                "efectivodolar" => $resumen['montodolaresefectivo'] + $pmixto['mixtoefectivodolares'],
                
                "montosoles" => $resumen['montosoles'],
                "montodolar" => $resumen['montodolares'],
                 "montosolescompra" => $resumen["montosolescompra"],
                 "montodolarescompra" => $resumen["montodolarescompra"],
                 "montosolestarjeta" => $resumen["montosolestarjeta"]+ $pmixto['mixtotarjetasoles'],
                 "montodolarestarjeta" => $resumen["montodolarestarjeta"] + $pmixto['mixtotarjetadolares'],
                 "monrodolarestransferencia" => $resumen["monrodolarestransferencia"]+ $pmixto['mixtotransferenciadolares'],
                 "montosolestransferencia" => $resumen["montosolestransferencia"]+ $pmixto['mixtotransferenciasoles'],
                 //////////////////////////
                "ingresosoles" => $resumen["ingresosoles"],
                "ingresodolares" => $resumen["ingresodolares"],
                "egresosoles" => $resumen["egresosoles"],
                "egresodolares" => $resumen["egresodolares"],
                "apertura_soles" => $resumen["apertura_soles"],
                "apertura_dolares" => $resumen["apertura_dolares"],
                
                "cajasoles" => ($resumen["apertura_soles"] + $resumen['montosoles'] + $resumen["ingresosoles"] ) - $resumen["egresosoles"],
                "cajadolar" => ($resumen["apertura_dolares"] + $resumen['montodolares'] + $resumen["ingresodolares"]) - $resumen["egresodolares"]
                 );
            
            echo json_encode($respuesta);
            
            }else {
                echo array();

            }
       
    }
     function searchcaja() {

        
        if(permisos::rol('rol-movimientocajaoperaciones')){
        if (isset($_POST['dpfecha']) && !empty($_POST['dpfecha'])) {
            
            

            $emision = $_POST['dpfecha'];

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');
            $fechat = $emision;
            $responsable = '';
            if(isset($_POST['responsable'])){
                $responsable = $_POST['responsable'];
            }
            
            

            
            $caja = new caja;
            $resumen = $caja->select($emisionf,$responsable,$_SESSION['idsucursal'] );
            
            $pmixtom = new pagoMixto();
            
            $idcaja = 0;
            
            if(!empty($resumen['idcaja'])){
                $idcaja = $resumen['idcaja'];
            }
            $pmixto =$pmixtom->selectbycaja($idcaja);
           
          
//            var_dump($resumen);
//            var_dump($pmixto);
            
            $respuesta = array(
                "ingresosoles"=>$resumen['ingresosoles'],
                "ingresodolares"=>$resumen['ingresodolares'],
                "egresosoles"=>$resumen['egresosoles'],
                "egresodolares"=>$resumen['egresodolares'],
                
                "idcaja"=>$resumen['idcaja'],
                "ventasoles"=>$resumen['ventasoles'],
                "ventadolares"=>$resumen['ventadolares'],
                "comprasoles"=>$resumen['comprasoles'],
                "compradolares"=>$resumen['compradolares'],
                
                "efectivosoles"=>$resumen['efectivosoles']+ $pmixto['mixtoefectivosoles'],
                "efectivodolares"=>$resumen['efectivodolares']+ $pmixto['mixtoefectivodolares'],
                
                "transdollar"=>$resumen['transferenciadollar']+ $pmixto['mixtotransferenciadolares'],
                "transsoles"=>$resumen['transferenciasoles']+ $pmixto['mixtotransferenciasoles'],
                
                "tarjetadollar"=>$resumen['tarjetadollar']+ $pmixto['mixtotarjetadolares'],
                "tarjetasoles"=>$resumen['tarjetasoles']+ $pmixto['mixtotarjetasoles'],
               
                "estado"=>$resumen['estado'],
                "cierredolares"=>$resumen['cierredolares'],
                "cierresoles"=>$resumen['cierresoles'],
                "aperturadolares"=>$resumen['aperturadolares'],
                "aperturasoles"=>$resumen['aperturasoles'],
                
                "cajasoles" => ($resumen["aperturasoles"] + $resumen['ventasoles'] + $resumen["ingresosoles"] ) - $resumen["egresosoles"],
                "cajadolar" => ($resumen["aperturadolares"] + $resumen['ventadolares'] + $resumen["ingresodolares"]) - $resumen["egresodolares"]
                 );
            
            echo json_encode($respuesta);
            
            }else {
                  ?> 
                <script>

                    swal('Ingrese campos obligatorios (*)', 'Por favor recargue la página', 'error');
                </script>  <?php

            }
        }else {
              ?> 
            <script>

                swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
            
            
        }
    }
    
    
    function insertgasto(){
        

        if(isset($_POST['txtmonto']) && isset($_POST['txtdescripcion']) && isset($_POST['txtdetalle']) && isset($_POST['cbformapago']) && isset($_POST['txtmonto'])
           && !empty($_POST['txtmonto']) && !empty($_POST['txtdescripcion']) && !empty($_POST['txtdetalle']) && !empty($_POST['cbformapago']) && !empty($_POST['txtmonto'])
                && isset($_POST['idcaja'])){
            
            if(empty($_POST['idcaja'])){
                 ?>
                <script>
                swal('Seleccione responsable', 'Por favor seleccione responsable de caja', 'info');
                </script>
                <?php
                die();
            }
            
            if(!is_numeric($_POST['txtmonto'])){
                ?>
                    <script>
                    swal('Ingrese monto valido (*)', 'Por favor recargue la página', 'error');
                    </script>
                    <?php
                die();
            }
            $monto = $_POST['txtmonto'];
            $descripcion=$_POST['txtdescripcion'];
            $detalle=$_POST['txtdetalle'];
            $formapago=$_POST['cbformapago'];
            $moneda=$_POST['moneda'];
            $tipo = $_POST['op'];
            $nop = '';
            $tipopago='';
            $idcaja = $_POST['idcaja'];
            
            if(isset($_POST['txtnop']) && isset($_POST['cbtipopago'])){
                $nop = $_POST['txtnop'];
                $tipopago = $_POST['cbtipopago'];
                
                
            }
            
            $gasto = new gastos();
            
            $gasto->setMonto($monto);
            $gasto->setConcepto($descripcion);
            $gasto->setDetalle($detalle);
            $gasto->setFormapago($formapago);
            $gasto->setTipopago($tipopago);
            $gasto->setNop($nop);
            $gasto->setMoneda($moneda);
            $gasto->setTipo($tipo);
            $gasto->setIdcaja($idcaja);
            
            $fila = $gasto->insert($gasto);
            
            if($fila > 0 ){
             ?> 
                  <script>
                     $('#FormularioAjax').trigger("reset");
                     $('.modalingresoegreso').modal('hide');
                     swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                     $('#buscarcaja').submit(); 

                  </script>  <?php
             
                
            }else {
                
                ?>
            <script>
            swal('Algo sucedio mal', 'Por favor recargue la página', 'error');
            </script>
            <?php
            }
            
            
            
            
            
            
        }else{
            ?>
            <script>
            swal('Ingrese campos obligatorios (*)', 'Por favor recargue la página', 'error');
            </script>
            <?php
        }
        
        
        
        
    }
    
    function insert(){
        
        if(isset($_POST['montosoles']) &&  isset($_POST['montodolar']) ){
            if(!is_numeric($_POST['montodolar']) && !is_numeric($_POST['montosoles'])){
                ?>
                    <script>
                    swal('Ingrese montos validos (*)', 'Por favor recargue la página', 'error');
                    </script>
                    <?php
                die();
            
            }
            $aperturasoles = 0;
            $aperturadolares = 0;
            if(!empty($_POST['montosoles']) ){
              $aperturasoles= $_POST['montosoles'];  
                
            }
            if(!empty($_POST['montodolar'])){
                $aperturadolares = $_POST['montodolar']; 
                
            }
           
            
            
            $caja = new caja();
            
            $caja->setAperturadolares($aperturadolares);
            $caja->setAperturasoles($aperturasoles);
            
            $fila = $caja->apertura($caja);
            
            if($fila > 0){
                 ?>
                <script>
                swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                document.location.reload();
               
                </script>
                <?php
                
            }else{
                 ?>
                <script>
                swal('Algo sucedio mal', 'Por favor recargue la página', 'error');
                </script>
                <?php
                
                
            }
            
            
            
        }else{
            ?>
            <script>
            swal('Ingrese campos obligatorios (*)', 'Por favor recargue la página', 'error');
            </script>
            <?php
        }
       
        
        
    }
    function update(){
        if(isset($_POST['montosoles']) &&  isset($_POST['montodolar']) 
                && isset($_POST['idcaja'])){
            
            
            
             if(!is_numeric($_POST['montodolar']) && !is_numeric($_POST['montosoles'])){
                ?>
                    <script>
                    swal('Ingrese montos validos (*)', 'Por favor recargue la página', 'error');
                    </script>
                    <?php
                die();
            
            }
            
            if(empty($_POST['idcaja'])){
                 ?>
                <script>
                swal('Registro de caja', 'Por favor registre la caja del día en el modulo Ventas', 'info');
                </script>
                <?php
                die();
            }
            
            $cierresoles=0;
            $cierredolar = 0;
            if (!empty($_POST['montosoles']) ){
                $cierresoles = $_POST['montosoles'];
                
            }
            if (!empty($_POST['montodolar'])){
                $cierredolar= $_POST['montodolar'];
                
            }
            
            
            $id = $_POST['idcaja'];
                  
            $caja = new caja();
            $caja->setId($id);
            $caja->setCierredolares($cierredolar);
            $caja->setCierresoles($cierresoles);
            
            $fila = $caja->cerrar($caja);
            
            if($fila > 0){
                 ?>
                <script>
                swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                $('#buscarcaja').submit();
                </script>
                <?php
                
            }else{
                 ?>
                <script>
                swal('Algo sucedio mal', 'Por favor recargue la página', 'error');
                </script>
                <?php
                
                
            }
            
            
            
        }else{
            ?>
            <script>
            swal('Ingrese campos obligatorios (*)', 'Por favor recargue la página', 'error');
            </script>
            <?php
        }
        
        
    }
    
    function updateapertura(){
        
        
        if(isset($_POST['aperturasoles']) &&  isset($_POST['apertudadolar']) 
                && isset($_POST['id'])){
            
            
            
             if(!is_numeric($_POST['aperturasoles']) && !is_numeric($_POST['apertudadolar'])){
                ?>
                    <script>
                    swal('Ingrese montos validos (*)', 'Por favor recargue la página', 'error');
                    </script>
                    <?php
                die();
            
            }
            
            if(empty($_POST['id'])){
                 ?>
                <script>
                swal('Registro de caja', 'Por favor registre la caja del día en el modulo Ventas', 'info');
                </script>
                <?php
                die();
            }
            
            $aperturasoles=0;
            $aperturadolar = 0;
            if (!empty($_POST['aperturasoles']) ){
                $aperturasoles = $_POST['aperturasoles'];
                
            }
            if (!empty($_POST['apertudadolar'])){
                $aperturadolar= $_POST['apertudadolar'];
                
            }
            
            
            $id = $_POST['id'];
                  
            $caja = new caja();
            $caja->setId($id);
            $caja->setAperturasoles($aperturasoles);
            $caja->setAperturadolares($aperturadolar);
            
            $fila = $caja->updateapertura($caja);
            
            if($fila > 0){
                 ?>
                <script>
                swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                 $('#buscarcaja').submit();
                </script>
                <?php
                
            }else{
                 ?>
                <script>
                swal('No realizo cambios', 'No se detectaron cambios', 'info');
                </script>
                <?php
                
                
            }
            
            
            
        }else{
            ?>
            <script>
            swal('Ingrese campos obligatorios (*)', 'Por favor recargue la página', 'error');
            </script>
            <?php
        }
        
        
    }
    
    
    function responsable(){
       if(isset($_POST['dpfecha']) && !empty($_POST['dpfecha'])){
           
           $emision = $_POST['dpfecha'];

            $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
            $emisionf = $dateemis->format('Y-m-d');
            
            $caja = new caja();
            $responsables = $caja->responsable($emisionf);
      
            
            echo '<select class="form-control" id="responsable" name="responsable">';
            foreach ($responsables as $responsable){
             
               echo '<option value="'.$responsable['id'].'">'.$responsable['nombre'].' - '.$responsable['create'].'</option>';
               
                
            }
             echo '</select>';
           
       }
        
    }
    
    function printExcel(){
//        var_dump($_GET);
        if(isset($_GET['id']) && !empty($_GET['id'])){
            
            $id = $_GET['id'];
         
            $caja = new caja();
            $gastos = new gastos();
            $resumen = $caja->selectbyid($id);
            
            $egresosoles = $gastos->selectbycaja($id, "Soles", "Egreso");
            $egresodolares = $gastos->selectbycaja($id, "Dolares", "Egreso");
            $ingresosoles= $gastos->selectbycaja($id, "Soles", "Ingreso");
            $ingresodolares=$gastos->selectbycaja($id, "Dolares", "Ingreso");
            
//            var_dump($resumen);
            require_once 'view/reportes/functions/excel.php';

            require_once 'plugins/PHPExcel/Classes/PHPExcel.php';
            require_once 'view/operaciones/CajaExcel.php';
            
            
            
            
        }
    }
    
        function printticket() {

        if (isset($_GET['id'])) {

            $id = $_GET['id'];
           $caja = new caja();
            $gastos = new gastos();
            $resumen = $caja->selectbyid($id);
            
            $egresosoles = $gastos->selectbycaja($id, "Soles", "Egreso");
            $egresodolares = $gastos->selectbycaja($id, "Dolares", "Egreso");
            $ingresosoles= $gastos->selectbycaja($id, "Soles", "Ingreso");
            $ingresodolares=$gastos->selectbycaja($id, "Dolares", "Ingreso");
           
            require_once 'view/operaciones/ticketcaja.php';
        }
    }
    
    
}
