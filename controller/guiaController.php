<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of guiaController
 *
 * @author HERNAN
 */
require_once 'model/guia.php';
require_once 'model/sucursal.php';
require_once 'model/documentoSucursal.php';
require_once 'model/unidmedida.php';
require_once 'model/sunatTransaction.php';
require_once 'model/tipoImpuesto.php';
class guiaController {
    //put your code here
    private $guia;
    
    
    function __construct() {
        $this->guia = new guia();
    }
    
//    function crear(){
//        
//        require_once 'view/layout/header.php';
//        $sucursalm = new sucursal();
//        $sucursales = $sucursalm->selectAll();
//        $sucurseriem = new documentoSucursal();
//        $sucurseries= $sucurseriem->selectAll($_SESSION['idsucursal'], 'guia_remision');
//        $origen = $sucursalm->selectOne($_SESSION['idsucursal']);
//        $detalles = array();
//        $unidm = new unidmedida();
//        $transm = new sunatTransaction();
//        $inpum = new tipoImpuesto();
////        $transactions = $transm->selectAll();
//        $unidades = $unidm->selectAll();
//        $impuestos = $inpum->selectAll();
//        require_once 'view/guia/form_documento_guia.php';
//        require_once 'view/layout/footer.php'; 
//        
//    }
    
    function numero(){
        if(isset($_POST['cbserie']) and !empty($_POST['cbserie'])){
            $serie = trim($_POST['cbserie']);
            $guia = $this->guia->selectMax($serie, $_SESSION['idsucursal']);
            
            $num = (int)$guia->getNumero() + 1;
            
            echo $num;
            
        }else{
            
           echo $num= 0;
        }
        
        
    }
//    
//    function insert(){
////    var_dump($_POST);
//        
//        
//        if(isset($_POST['cbserie']) && isset($_POST['txtnro']) && isset($_POST['dpfechaentrega']) && isset($_POST['dpfechatraslado']) && isset($_POST['cbdestinatario'])
//         && isset($_POST['txtmarcaplaca']) && isset($_POST['txtlicencia']) && isset($_POST['txtcertificado']) && isset($_POST['txtrazonsocialtransp']) 
//                && isset($_POST['txtructransp']) && isset($_POST['txtflete']) && isset($_POST['txtmotivo'])  && isset($_POST['gratuita']) 
//                         && isset($_POST['exonerado'])  && isset($_POST['inafecto'])  && isset($_POST['exportacion'])  && isset($_POST['gravada'])  && isset($_POST['igv'])
//                 && isset($_POST['totaldoc']) && isset($_POST['id']) && 
//                
//                !empty($_POST['cbserie']) && !empty($_POST['txtnro']) && !empty($_POST['dpfechaentrega']) && !empty($_POST['dpfechatraslado']) && !empty($_POST['cbdestinatario'])
//                      && !empty($_POST['gravada'])  && !empty($_POST['igv'])
//                 && !empty($_POST['totaldoc']) && isset($_POST['txtemisordireccion']) && !empty($_POST['txtemisordireccion'])) {
//            
//            $serier = str_replace(" ", "", $_POST['cbserie']);
//            $numeror = str_replace(" ", "", $_POST['txtnro']);
//
//            $serie = ltrim($serier, '0');
//            $numero = (int) ($numeror);
//            
//            list($empresadestino,$direcciondestino,$rucdestino) = explode('-',$_POST['cbdestinatario']);
// 
//            echo $empresadestino.'-'.$direcciondestino.'-'.$rucdestino;
//           
//            if($this->guia->duplicado($serie, $numero) == 'valido'){
//                $sucursalm = new sucursal();
//                $origen = $sucursalm->selectOne($_SESSION['idsucursal']);
//                
//                $fechaentrega = $_POST['dpfechaentrega'];
//                $fechatraslado= $_POST['dpfechatraslado'];
//                
//                $ructransp = $_POST['txtructransp'];
//                $razonstransp= $_POST['txtrazonsocialtransp'];
//                
//                
//                $direccionemisor= $_POST['txtemisordireccion'];
//                $marcaplaca = $_POST['txtmarcaplaca'];
//                $licencia = $_POST['txtlicencia'];
//                $certificado = $_POST['txtcertificado'];
//                $razonsocialtrans= $_POST['txtrazonsocialtransp'];
//                $ructransp = $_POST['txtructransp'];
//                $flete = $_POST['txtflete'];
//                $motivo = $_POST['txtmotivo'];
//                $gratuita = $_POST['gratuita'];
//                $exonerado = $_POST['exonerado'];
//                $inafecto = $_POST['inafecto'];
//                $exportacion = $_POST['exportacion'];
//                $gravada = $_POST['gravada'];
//                $igv = $_POST['igv'];
//                $total = $_POST['totaldoc'];
//                if(empty($flete) && !is_numeric($flete)){
//                    $flete = 0;
//                }
//                if(empty($gratuita)){
//                    $gratuita = 0;
//                }
//                if(empty($exonerado)){
//                    $exonerado = 0;
//                }
//                if(empty($inafecto)){
//                    $inafecto = 0;
//                }
//                if(empty($exportacion)){
//                    $exportacion = 0;
//                }
//              
//                $guia = new guia();
//                
//                $guia->setSerie($serie);
//                $guia->setNumero($numero);
//                $guia->setFechaentrega($fechaentrega);
//                $guia->setFechatraslado($fechatraslado);
//              
//               
//                $guia->setIdsucursal($_SESSION['idsucursal']);
//                $guia->setRazonsialdestinatario($empresadestino);
//                $guia->setDirecciondestinatario($direcciondestino);
//                $guia->setRucdestinatario($rucdestino);
//                
//                $guia->setRucemisor($origen->getRuc());
//                $guia->setRazonsocialemisor($origen->getNombre());
//                $guia->setDireccionemisor($direccionemisor);
//                
//                $guia->setRuctransportista($ructransp);
//                $guia->setRazonsocialtransportista($razonstransp);
//                $guia->setCertificdotransportista($certificado);
//                
//                $guia->setMarcaplaca($marcaplaca);
//                $guia->setLicenciatransportista($licencia);
//                
//                
//                $guia->setMotivo($motivo);
//                
//                $guia->setFlete($flete);
//                $guia->setMontogratuita($gratuita);
//                $guia->setMontoexonerado($exonerado);
//                $guia->setMontoinafecto($inafecto);
//                $guia->setMontoexportacion($exportacion);
//                $guia->setMontogravada($gravada);
//                $guia->setMontoigv($igv);
//                $guia->setMontototal($total);
//                
//                $guia->setMarcaplaca($marcaplaca);
//                
//                echo $guia->insert($guia);
//                
//                
//                $idprod = $_POST['id'];
//                $codigo = $_POST['codigo'];
//                $descripcion = $_POST['descripcionprod'];
//                $unidad = $_POST['unidad'];
//                $tipoigv = $_POST['tipoigv'];
//                $cantidad = $_POST['cantidad'];
//                $precio = $_POST['precio'];
//                $subtotal = $_POST['subtotal'];
//                $total = $_POST['total'];
//
//
//                $igvprod = $_POST['igvprod'];
//                $valorunitref = $_POST['valorunitref'];
//                $montobaseigv = $_POST['montobaseigv'];
//                $montobaseexpo = $_POST['montobaseexpo'];
//                $montobaseexonerado = $_POST['montobaseexonerado'];
//                $montobaseinafecto = $_POST['montobaseinafecto'];
//                $montobasegratuito = $_POST['montobasegratuito'];
//                $montobaseivap = $_POST['montobaseivap'];
//                $montobaseotrostributos = $_POST['montobaseotrostributos'];
//                $tributoventagratuita = $_POST['tributoventagratuita'];
//                $otrostributos = $_POST['otrostributos'];
//                $porcentajeigv = $_POST['porcentajeigv'];
//                $porcentajeotrostributos = $_POST['porcentajeotrostributos'];
//                $porcentajetributoventagratuita = $_POST['porcentajetributoventagratuita'];
//                $montooriginal = $_POST['montooriginal'];
//                $monedaoriginal = $_POST['monedaoriginal'];
//                $incluye = $_POST['incluye'];
//                
//                $detalles = array();
//                $iddetalle = array();
//                $produpdate = array();  //////////// array de productos actualizar stock
//                
//                for ($i = 0; $i < count($codigo); $i++) {
//                    $idpro = $idprod[$i];
//                    $igvp = $igvprod[$i];
//                    $valorunitr = $valorunitref[$i];
//                    $montobaseig = $montobaseigv[$i];
//                    $montobaseexp = $montobaseexpo[$i];
//                    $montobaseexon = $montobaseexonerado[$i];
//                    $montobaseinaf = $montobaseinafecto[$i];
//                    $montobasegratu = $montobasegratuito[$i];
//                    $montobaseiv = $montobaseivap[$i];
//                    $montobaseotrostrib = $montobaseotrostributos[$i];
//                    $tributoventagrat = $tributoventagratuita[$i];
//                    $otrostrib = $otrostributos[$i];
//                    $porceigv = $porcentajeigv[$i];
//                    $porcotrostrib = $porcentajeotrostributos[$i];
//                    $porcentajetribventgrat = $porcentajetributoventagratuita[$i];
//                    $montoorig = $montooriginal[$i];
//                    if (empty($idprod[$i])) {
//                        $idpro = 0;
//                    }
//                    if (empty($igvprod[$i])) {
//                        $igvp = 0;
//                    }
//                    if (empty($valorunitref[$i])) {
//                        $valorunitr = 0;
//                    }
//                    if (empty($montobaseigv[$i])) {
//                        $montobaseig = 0;
//                    }
//                    if (empty($montobaseexpo[$i])) {
//                        $montobaseexp = 0;
//                    }
//                    if (empty($montobaseexonerado[$i])) {
//                        $montobaseexon = 0;
//                    }
//                    if (empty($montobaseinafecto[$i])) {
//                        $montobaseinaf = 0;
//                    }
//                    if (empty($montobasegratuito[$i])) {
//                        $montobasegratu = 0;
//                    }
//                    if (empty($montobaseivap[$i])) {
//                        $montobaseiv = 0;
//                    }
//                    if (empty($montobaseotrostributos[$i])) {
//                        $montobaseotrostrib = 0;
//                    }
//                    if (empty($tributoventagratuita[$i])) {
//                        $tributoventagrat = 0;
//                    }
//                    if (empty($otrostributos[$i])) {
//                        $otrostrib = 0;
//                    }
//                    if (empty($porcentajeigv[$i])) {
//                        $porceigv = 0;
//                    }
//                    if (empty($porcentajeotrostributos[$i])) {
//                        $porcotrostrib = 0;
//                    }
//                    if (empty($porcentajetributoventagratuita[$i])) {
//                        $porcentajetribventgrat = 0;
//                    }
//                    if (empty($montooriginal[$i])) {
//                        $montoorig = 0;
//                    }
//                    $d = array(
//                        $idpro,
//                        $codigo[$i],
//                        $descripcion[$i],
//                        $unidad[$i],
//                        $tipoigv[$i],
//                        $cantidad[$i],
//                        $precio[$i],
//                        $subtotal[$i],
//                        $total[$i],
//                        $id,
//                        $igvp,
//                        $valorunitr,
//                        $montobaseig,
//                        $montobaseexp,
//                        $montobaseexon,
//                        $montobaseinaf,
//                        $montobasegratu,
//                        $montobaseiv,
//                        $montobaseotrostrib,
//                        $tributoventagrat,
//                        $otrostrib,
//                        $porceigv,
//                        $porcotrostrib,
//                        $porcentajetribventgrat,
//                        $montoorig,
//                        $monedaoriginal[$i],
//                        $incluye[$i]
//                    );
//                    array_push($detalles, $d);
//
//                    $produp = array(
//                        ($cantidad[$i]) * -1,
//                        $idprod[$i]
//                    );
//                    array_push($produpdate, $produp);
//                }
//                
//              
//               
//        
//            }
//            
//            
//            
//            
//            
//            
//            
//        }
//        
//        
//        
//        
//        
//        
//        
//    }

}
