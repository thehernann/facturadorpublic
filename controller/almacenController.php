<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of almacenController
 *
 * @author HERNAN
 */
require_once 'model/almacen.php';
class almacenController {
    //put your code here
    
        function  selectbyid(){
//        var_dump($_POST);
        
        if(isset($_POST['id']) && !empty($_POST['id'])){
            
            $id = $_POST['id'];
            
            
            $almacenm = new Almacen();
            
            $almacen = $almacenm->selectOne($id);
            
//            var_dump($sucursal);
            
            $res = array(
                "razonsocial"=>$almacen->getNombre(),
//                "ruc"=>$sucursal->getRuc(),
                "direccion"=>$almacen->getDireccion()
                
                
            );
            
            echo json_encode($res);
            
            
            
            
            
        }
        
    }
    
    
    function select(){
        
        
        require_once 'view/layout/header.php';
               $almacenm = new Almacen();
            
//            if(permisos::rol('rol-bancolistar')){
                $almacenes = $almacenm->selectAll();
                require_once 'view/almacen/listar_almacenes.php';
                
//            }else {
//                
//                require_once 'view/sinpermiso.php';
//            }
 
        require_once 'view/layout/footer.php';
        
        
        
        
    }
    function cargar(){
        require_once 'view/layout/header.php';
        
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $id = $_GET['id'];
            $almacenm = new Almacen();
            
            $almacen = $almacenm->selectOne($id);
            
            require_once 'view/almacen/form_almacen.php';
            
            
        }else {
            
            require_once 'view/error.php';
        }
        
        require_once 'view/layout/footer.php';
        
        
        
    }
    
    
    function update(){
        
        if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['txtnombre']) && !empty($_POST['txtnombre'])
                && isset($_POST['txtdireccion']) && !empty($_POST['txtdireccion'])){
            
            $id = $_POST['id'];
            $nombre = $_POST['txtnombre'];
            $direccion = $_POST['txtdireccion'];
            
            $almacen = new Almacen();
            
            $almacen->setId($id);
            $almacen->setNombre($nombre);
            $almacen->setDireccion($direccion);
            
            $fila = $almacen->update($almacen);
            
            if($fila > 0){
                
                ?> 
                  <script>

                    swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                      
                  </script>  <?php
                
                
            }
            
        }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            
            
            
        }
        
        
        
    }
    
    
}
