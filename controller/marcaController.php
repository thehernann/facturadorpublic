<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of marcaController
 *
 * @author HERNAN
 */
require_once 'model/marcaprod.php';
class marcaController {
    private $marca;
    
    function __construct() {
      $this->marca = new marcaprod();   
    }
    
    function insert(){
        
        if(permisos::rol('rol-marcanuevo')){
        
        $fila= 0;       
        if(isset($_POST['txtdescripcionmarca'])  &&
                !empty($_POST['txtdescripcionmarca'])){ //&&  && !empty($_POST['txtcodmarca']) && isset($_POST['txtordenmarca']) && isset($_POST['txtcodmarca']) &&
            
                $cadena = trim($_POST['txtdescripcionmarca']);
                if($this->marca->duplicado($cadena) == 0){
                    $marca = new marcaprod();
//                    $marca->setCodigo($_POST['txtcodmarca']);
                    $marca->setDescripcion($_POST['txtdescripcionmarca']);
//                    $marca->setOrden($_POST['txtordenmarca']);
                    $fila= $this->marca->insert($marca);
                    
                }else {
                    ?> 
                    <script>

                         swal('Error', 'La marca ya se encuentra registrada.', 'error');
                    </script>  <?php
                    
                }
                
                if($fila > 0){
                     ?> 
                    <script>

                        swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                        $('#FormularioAjax').trigger("reset");
                        
                        if($('.modal').length && $('#divmarca').length){
                            $('.modal').modal('hide');
                            $('#divmarca').load(location.href + " #divmarca>*", ""); 
                            
                        }
                        
                    </script>  <?php
                    
                    
                }else {
                   
                    swal("Error!", "Algo sucedio mal :(.", "error");

                    
                    
                }
            
            
            
        }else {
             ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            
        }
    }else{
         ?> 
            <script>
               
                 swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
        
    }
        
    }
    
    function update(){
       
        if(permisos::rol('rol-marcaeditar')){
        $fila= 0;       
        if(isset($_POST['id']) && isset($_POST['txtdescripcionmarca'])  &&
                !empty($_POST['txtdescripcionmarca'])){ // && isset($_POST['txtcodmarca'])&& isset($_POST['txtordenmarca'])
            
              $cadena = trim($_POST['txtdescripcionmarca']);
              $id = str_replace(" ", "", $_POST['txtdescripcionmarca']);
             
                if($this->marca->duplicadoedit($cadena,$id) == 0){
                    $marca = new marcaprod();
                    $marca->setId($_POST['id']);
//                    $marca->setCodigo($_POST['txtcodmarca']);
                    $marca->setDescripcion($_POST['txtdescripcionmarca']);
//                    $marca->setOrden($_POST['txtordenmarca']);
                    $fila= $this->marca->update($marca);
                    
                }else {
                    ?> 
                    <script>

                         swal('Error', 'La marca ya se encuentra registrada.', 'error');
                    </script>  <?php
                    
                }
            
            
            
            if($fila > 0){
                 ?> 
                <script>

                     swal('Éxitosamente', 'Operación realizada correctamente.', 'success');
                </script>  <?php
                
                
            }
            
        }else {
            ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            
        }
    }else{
        ?> 
            <script>
               
                 swal('Acceso denegado', 'Necesita permisos para realizar esta acción', 'error');
            </script>  <?php
        
    }
       
    }
    
    
    function select(){
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-marcalistar')){
            $marcas = $this->marca->selectAll();
            require_once 'view/marca/listar_marca.php';   
            
        }else {
            $url='';
            require_once 'view/sinpermiso.php';
            
        }
        
      
        
        require_once 'view/layout/footer.php';
    }
    
    function crear(){
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-marcanuevo')){
            $marca= new marcaprod();

        require_once 'view/marca/form_marca.php'; 
            
        } else {
            $url='marca/select';
            require_once 'view/sinpermiso.php';
            
        }
       
      
        
        require_once 'view/layout/footer.php';
        
        
        
    }
    
    function cargar(){
        
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-marcaeditar')){
          if(isset($_GET['id']) && !empty($_GET['id'])){
            
            $marca = $this->marca->selectOne($_GET['id']);
            require_once 'view/marca/form_marca.php';
            }else{

                require_once 'view/error.php';
            }  
            
        }else {
            require_once 'view/sinpermiso.php';
            
        }

        require_once 'view/layout/footer.php';
        
        
        
    }



    //put your code here
}
