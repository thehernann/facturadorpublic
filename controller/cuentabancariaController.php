<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cuentabancariaController
 *
 * @author HERNAN
 */
require_once 'model/cuentaBancaria.php';
require_once 'model/bancos.php';
class cuentabancariaController {
    //put your code here
    private $cuenta; 
    private $banco;
    function __construct() {
        $this->cuenta = new cuentaBancaria();
        $this->banco = new bancos();
    }
    
    
    function select(){
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-cuentabancalistar')){
            $cuentas = $this->cuenta->selectAll();
            require_once 'view/cuentabancaria/listar_cuenta.php';  
            
        }else {
            
            require_once 'view/sinpermiso.php';
        } 
        
      
        
        require_once 'view/layout/footer.php';
        
        
        
    }
    function crear(){
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-cuentabancanuevo')){
            $cuenta = new cuentaBancaria();
            $bancos = $this->banco->selectAll();
            require_once 'view/cuentabancaria/form_cuenta.php';
        }else {
            $url = 'cuentabancaria/select';
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
        
        
        
    }
    function insert(){
        
      if(permisos::rol('rol-cuentabancanuevo')){   
        
        $fila = 0; 
    if(isset($_POST['cbbanco']) && isset($_POST['txtnumero']) && isset($_POST['txtcci']) && isset($_POST['txttitular'])
        && !empty($_POST['cbbanco']) && !empty($_POST['txtnumero']) && !empty($_POST['txtcci']) && !empty($_POST['txttitular']) ){
        $cuenta= new cuentaBancaria();
        
        $cuenta->setBanco($_POST['cbbanco']);
        $cuenta->setNumero($_POST['txtnumero']);
        $cuenta->setCci($_POST['txtcci']);
        $cuenta->setTitular($_POST['txttitular']);
        
        $fila = $this->cuenta->insert($cuenta);
        
        if($fila > 0){
                ?> 
                  <script>

                       swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                       $('#FormularioAjax').trigger("reset");

                  </script>  <?php

           }else {
               ?> 
                  <script>

                       swal("Error", "No se realizarón cambios", "error");
                     

                  </script>  <?php
           } 
        
    }else {
         ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
    }
    }else {
        ?> 
            <script>
               
                 swal('Acceso denegado', 'necesita permisos para realizar esta acción', 'error');
            </script>  <?php
        
    }
    
        
    }
    
    function update(){
        
        if(permisos::rol('rol-cuentabancaeditar')){   
        $fila =0;
        if(isset($_POST['id']) && isset($_POST['cbbanco']) && isset($_POST['txtnumero']) && isset($_POST['txtcci']) && isset($_POST['txttitular'])
                && !empty($_POST['id']) && !empty($_POST['cbbanco']) && !empty($_POST['txtnumero']) && !empty($_POST['txtcci']) && !empty($_POST['txttitular'])){
            $cuenta= new cuentaBancaria();
            $cuenta->setId($_POST['id']);
            $cuenta->setBanco($_POST['cbbanco']);
            $cuenta->setNumero($_POST['txtnumero']);
            $cuenta->setCci($_POST['txtcci']);
            $cuenta->setTitular($_POST['txttitular']);

            $fila = $this->cuenta->update($cuenta);
            
            if($fila > 0){
                ?> 
                  <script>
                       swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                  </script>  <?php
           }else {
            ?> 
               <script>

                    swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
               </script>  <?php
            }  
            
        }else {
         ?> 
            <script>
               
                 swal('No se realizo registro', 'Ingrese campos obligatorios', 'error');
            </script>  <?php
            }
    }else {
        ?> 
            <script>
               
                 swal('Acceso denegado', 'necesita permisos para realizar esta acción', 'error');
            </script>  <?php
        
    }
    
    
    }
    function cargar(){
        require_once 'view/layout/header.php';
        if(permisos::rol('rol-cuentabancaeditar')){   
        
        
        
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $cuenta = $this->cuenta->selectOne($_GET['id']);
            $bancos = $this->banco->selectAll();
            
            require_once 'view/cuentabancaria/form_cuenta.php';
            
            
            
        }else{
            require_once 'view/error.php';
            
        }
        
        }else {
            require_once 'view/sinpermiso.php';  
            
        }
      require_once 'view/layout/footer.php';  
        
        
    }
        
        


}
