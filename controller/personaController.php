<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of personaController
 *
 * @author HERNAN
 */
require_once 'model/persona.php';
require_once 'model/personaTipoDocumento.php';
class personaController {

    //put your code here
    private $persona;
    private $tipodoc;
    function __construct() {
        $this->persona = new persona();
        $this->tipodoc = new personaTipoDocumento();
    }

    
    function selectclient() {
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-clientelistar')){
            $tipo = 1;
            $personas= $this->persona->select($tipo); 
//            var_dump($personas);

            require_once 'view/persona/listar_persona.php';
            
        }else{
            $url = '';
            require_once 'view/sinpermiso.php';
        }
        
        require_once 'view/layout/footer.php';
        
    }
    function selectprovee() {
        require_once 'view/layout/header.php';
        
        if(permisos::rol('rol-proveedorlistar')){
        $tipo = 2;
        $personas= $this->persona->select($tipo);        
        
        require_once 'view/persona/listar_persona.php';
         }else{
             $url= '';
            require_once 'view/sinpermiso.php';
        }
        require_once 'view/layout/footer.php';
    }


    function cargarclient() {
        
         require_once 'view/layout/header.php';
         
         if(permisos::rol('rol-clienteeditar')){
        if( isset($_GET['id']) && !empty($_GET['id'])){
            $id=$_GET['id'];
         
            $titulo = 'EDITAR CLIENTE';
//        $persona = $this->persona->selectone($id);
        
            $docs = $this->tipodoc->selectAll();

            $persona = $this->persona->selectone($id);
            
            
            
//            $c = DateTime::createFromFormat('Y-m-d', $persona->getAnivcumplenos());
//            $cumple = $c->format('d/m/Y');
            
//            if($persona->getAnivcumplenos() != ""){
//                $cumple = date_format($persona->getAnivcumplenos(), 'd/m/Y') ;            
//                
//            }
            var_dump($persona->getAnivcumplenos());
            require_once 'view/persona/form_persona_client.php'; 
           
            }
            else {

                require_once 'view/error.php';

            }
         }else{
             $url='persona/selectclient';
            require_once 'view/sinpermiso.php';
        }
        require_once 'view/layout/footer.php';
    }
    
    
        function cargarprovee() {
        require_once 'view/layout/header.php';
         if(permisos::rol('rol-clienteeditar')){
        
        if(!empty($_GET['id']) && isset($_GET['id'])){
            $id=$_GET['id'];
         
          $titulo = 'EDITAR PROVEEDOR';
//        $persona = $this->persona->selectone($id);
        
            $docs = $this->tipodoc->selectAll();

            $persona = $this->persona->selectone($id);
            
            $c = DateTime::createFromFormat('Y-m-d', $persona->getAnivcumplenos());
            $cumple = $c->format('d/m/Y');
            
            require_once 'view/persona/form_persona_provee.php';
           
            }else {

                require_once 'view/error.php';

            }
         }else{
             $url='persona/selectprovee';
            require_once 'view/sinpermiso.php';
        }
        require_once 'view/layout/footer.php';
    }
    function update() {

         $fila = 0;
        
         
     if(isset($_POST['id']) && isset($_POST['txtcliente']) && isset($_POST['txtruc']) && isset($_POST['txtrepresentante']) 
              && 
              isset($_POST['txtdireccion']) && isset($_POST['txttelfijo'])
             && isset($_POST['txtemail']) && isset($_POST['txtweb'])
                    
               && isset($_POST['cbidtipodoc']) && isset($_POST['cbmodopago']) 
             && isset($_POST['cbrepresentante']) && isset($_POST['tipo']) && !empty($_POST['tipo'])){ 
// && isset($_POST['txtdni']) && isset($_POST['cbestado']) && isset($_POST['txtcel1']) && isset($_POST['txtcel2']) && isset($_POST['txtcel3'])  && isset($_POST['dpaniversario'])  && isset($_POST['txtrepresenta'])&& isset($_POST['txtpartida'])
         
        
        
         if(!empty($_POST['dpaniversario']) && isset($_POST['dpaniversario'])){
             $emision = trim($_POST['dpaniversario']);
             $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
             $emisionf = $dateemis->format('Y-m-d');
             
         } else {
             $emisionf= '0000-00-00';
         }
         
         $rol = '';
         if(isset($_POST['rol']) && !empty($_POST['rol'])){
             $rol = $_POST['rol'];
         }
         
         
         
         list($tipodoc,$idpersonatipodoc) = explode('-',$_POST['cbidtipodoc']);
          
         $tipo = $_POST['tipo']; 
         $id =  $_POST['id'];
         $cliente = $_POST['txtcliente'];
         $ruc = str_replace(" ", "",trim($_POST['txtruc']));
         $representante = $_POST['txtrepresentante'];
//         $dni = str_replace(" ", "",trim($_POST['txtdni']));
//         $dpto = $_POST['txtdpto'];
//         $provincia =  $_POST['txtprovincia'];
//         $distrito =  $_POST['txtdistrito'];
         $direccion =  $_POST['txtdireccion'];
         $telfijo = $_POST['txttelfijo'];
         
         $cel1 = "";
         $cel2 = "";
         $cel3 = "";
         if(isset($_POST['txtcel1'])){
             $cel1 = $_POST['txtcel1'];
         }
         if(isset($_POST['txtcel2'])){
             $cel2 = $_POST['txtcel2'];
         }
         if(isset($_POST['txtcel3'])){
             $cel3 = $_POST['txtcel3'];
         }
         
         $representa = "";
         
         if(isset($_POST['txtrepresenta'])){
             $representa = $_POST['txtrepresenta'];
         }
        $partida = "";
         if(isset($_POST['txtpartida'])){
             $partida = $_POST['txtpartida'];
         }
         
         $estado = "";
         if(isset($_POST['cbestado'])){
             $estado =$_POST['cbestado'];
         }
//         
//         $cel2 =  $_POST['txtcel2'];
//         $cel3 = $_POST['txtcel3'];
         $email = $_POST['txtemail'];
         $web = $_POST['txtweb'];
         $aniv = $emisionf;
//         $responsable = isset($_POST['txtresponsable']) ? $_POST['txtresponsable'] : false;
//         $estado = $_POST['cbestado'];
         
//         $partida = $_POST['txtpartida'];
//         $iddoc =  $_POST['cbidtipodoc'];
         
         $modopago = $_POST['cbmodopago'];
         $cbrepresentate = $_POST['cbrepresentante'];
         
//          echo 'id'.$id.'cliente'.$cliente.'ruc'.$ruc.'represrnt'.$representante.'dni'.$dni.'dpto'.$dpto.'prov'.$provincia.'distri'.$distrito.'direc'.$direccion.'tel'.$telfijo
//            .'cel1'.$cel1.'cel2'.$cel2.'cel3'.$cel3.'email'.$email.'web'.$web.'aniv'.$aniv.'estado'.$estado.'resenta'.$representa.'parti'.$partida.'iddoc'.$iddoc.'modpago'.$modopago.'cbrepres'.
//             $cbrepresentate;
//         
//         echo 'dentro del if';
         $fila = 0;
        
         if($this->persona->duplicadoedit($ruc, $tipo, $id) == 0){
         
         
         $persona = new persona();
         $persona->setId($id);
         $persona->setNombre($cliente);
         $persona->setRuc($ruc);
         $persona->setRepresentante($representante);
//         $persona->setDni($dni);
//         $persona->setDpto($dpto);
//         $persona->setProvincia($provincia);
//         $persona->setDistrito($distrito);
         $persona->setDireccion($direccion);
         $persona->setTelfijo($telfijo);
         $persona->setCel1($cel1);
         $persona->setCel2($cel2);
         $persona->setCel3($cel3);
         $persona->setEmail($email);
         $persona->setWeb($web);
         $persona->setAnivcumplenos($aniv);
         $persona->setResponsable($cbrepresentate);
         $persona->setEstado($estado);
         $persona->setRepresenta($representa);
         $persona->setPartida($partida);
         $persona->setIdtipodocumento($idpersonatipodoc);
         $persona->setModopago($modopago);
         $persona->setRol($rol);
         $fila = $persona->update($persona);

    }else {
        ?> 
            <script>
               
                 swal('No se realizo registro', 'Ya se encuentra registrado', 'error');
            </script>  <?php
    }
    
    if($fila > 0 ){
        ?> 
            <script>
               
                 swal("Éxitosamente!", "Operación realizada correctamente.", "success");
            </script>  <?php
        
        
    }
    
             
    
    }
     
              
     
   
    }

    function deleteprove() {
        
        if(permisos::rol('rol-proveedoreliminar')){
            if(isset($_POST['id']) && !empty($_POST['id'])){
//            var_dump($_POST);
            $id = $_POST['id'];
            $fila =$this->persona->delete($id);
            
            
            echo 'fila '.$fila;
                    if($fila!=0 ){
                        echo '<script>swal("Éxitosamente!", "Operación realizada correctamente.", "success");</script>';
                        echo "<META HTTP-EQUIV='Refresh' CONTENT='0; URL=".base_url."persona/selectprovee'>";
   
                    }else {
                        
                        echo '<script>swal("No se realizarón cambios!", "Algo sucedio mal :(", "error");</script>';
                    }
            
            
            }
            
        }else{
             echo '<script>swal("Acceso denegado", "Necesita permisos para realizar esta acción", "error");</script>';
        }
        
        
        
    }
    
       function deleteclient() {
        
           if(permisos::rol('rol-clienteeliminar')){
        if(isset($_POST['id']) && !empty($_POST['id'])){
//            var_dump($_POST);
            $id = $_POST['id'];
            $fila =$this->persona->delete($id);
            
            
            echo 'fila '.$fila;
                    if($fila!=0 ){
                        echo '<script>swal("Éxitosamente!", "Operación realizada correctamente.", "success");</script>';
                        echo "<META HTTP-EQUIV='Refresh' CONTENT='0; URL=".base_url."persona/selectclient'>";
   
                    }
            
            
        }
        
    }else{
             echo '<script>swal("Acceso denegado", "Necesita permisos para realizar esta acción", "error");</script>';
        }
    }

    function insert() {
         
         $fila = 0;
//         var_dump($_POST);
     if(isset($_POST['id']) && isset($_POST['txtcliente']) && isset($_POST['txtruc']) && isset($_POST['txtrepresentante']) 
             &&  isset($_POST['txtdireccion']) && isset($_POST['txttelfijo'])
             &&  isset($_POST['txtemail']) && isset($_POST['txtweb'])
                      
              && isset($_POST['cbidtipodoc']) && isset($_POST['cbmodopago']) && isset($_POST['cbrepresentante']) && 
             isset($_POST['tipo']) && !empty($_POST['tipo']) && isset($_POST['rol']) && !empty($_POST['rol'])){ //&& isset($_POST['txtdni']) //isset($_POST['txtcel1']) && isset($_POST['txtcel2']) && isset($_POST['txtcel3']) && && isset($_POST['dpaniversario'])&& isset($_POST['txtpartida'])  && isset($_POST['txtrepresenta']) && isset($_POST['cbestado'])
         
         $fila = 0;
          $emisionf= '0000-00-00';  
         if(isset($_POST['dpaniversario'])){
         $emision = trim($_POST['dpaniversario']);
         if(!empty($emision)){
             $dateemis = DateTime::createFromFormat('d/m/Y', $emision);
             $emisionf = $dateemis->format('Y-m-d');
             
         } 
         }
         list($tipodoc,$idpersonatipodoc) = explode('-',$_POST['cbidtipodoc']);
         
      
         
         $tipo= str_replace(" ", "",trim($_POST['tipo']));
         $id =  $_POST['id'];
         $cliente = $_POST['txtcliente'];
         $ruc = str_replace(" ", "",trim($_POST['txtruc']));
         $representante = $_POST['txtrepresentante'];
//         $dni = str_replace(" ", "",trim($_POST['txtdni']));
//         $dpto = $_POST['txtdpto'];
//         $provincia =  $_POST['txtprovincia'];
//         $distrito =  $_POST['txtdistrito'];
         $direccion =  $_POST['txtdireccion'];
         $telfijo = $_POST['txttelfijo'];
//         $cel1 = $_POST['txtcel1'];
//         $cel2 =  $_POST['txtcel2'];
//         $cel3 = $_POST['txtcel3'];
         $email = $_POST['txtemail'];
         $web = $_POST['txtweb'];
         $aniv = $emisionf;
//         $responsable = isset($_POST['txtresponsable']) ? $_POST['txtresponsable'] : false;
//         $estado = "";
//         if(isset($_POST['cbestado'])){
//              $estado = $_POST['cbestado'];
//         }
//         $representa="";
//         if(isset($_POST['txtrepresenta'])){
//             
//             $representa = $_POST['txtrepresenta'];
//         }
//         $partida = "";
//         if(isset($_POST['txtpartida'])){
//             $partida = $_POST['txtpartida'];
//         }
         
         
//         $iddoc =  $_POST['cbidtipodoc'];
         $modopago = $_POST['cbmodopago'];
         $cbrepresentate = $_POST['cbrepresentante'];
         
         $rol = $_POST['rol'];
         
//          echo 'id'.$id.'cliente'.$cliente.'ruc'.$ruc.'represrnt'.$representante.'dni'.$dni.'dpto'.$dpto.'prov'.$provincia.'distri'.$distrito.'direc'.$direccion.'tel'.$telfijo
//            .'cel1'.$cel1.'cel2'.$cel2.'cel3'.$cel3.'email'.$email.'web'.$web.'aniv'.$aniv.'estado'.$estado.'resenta'.$representa.'parti'.$partida.'iddoc'.$iddoc.'modpago'.$modopago.'cbrepres'.
//             $cbrepresentate;
//         
//         echo 'dentro del if';
         
         
         $cel1 = "";
         $cel2 = "";
         $cel3 = "";
         if(isset($_POST['txtcel1'])){
             $cel1 = $_POST['txtcel1'];
         }
         if(isset($_POST['txtcel2'])){
             $cel2 = $_POST['txtcel2'];
         }
         if(isset($_POST['txtcel3'])){
             $cel3 = $_POST['txtcel3'];
         }
         
         $representa = "";
         
         if(isset($_POST['txtrepresenta'])){
             $representa = $_POST['txtrepresenta'];
         }
        $partida = "";
         if(isset($_POST['txtpartida'])){
             $partida = $_POST['txtpartida'];
         }
         
         $estado = "";
         if(isset($_POST['cbestado'])){
             $estado =$_POST['cbestado'];
         }
         
         
         if($this->persona->duplicado($ruc, $tipo) == 0){
         $persona = new persona();
         $persona->setId($id);
         $persona->setNombre($cliente);
         $persona->setRuc($ruc);
         $persona->setRepresentante($representante);
//         $persona->setDni($dni);
//         $persona->setDpto($dpto);
//         $persona->setProvincia($provincia);
//         $persona->setDistrito($distrito);
         $persona->setDireccion($direccion);
         $persona->setTelfijo($telfijo);
         $persona->setCel1($cel1);
         $persona->setCel2($cel2);
         $persona->setCel3($cel3);
         $persona->setEmail($email);
         $persona->setWeb($web);
         $persona->setAnivcumplenos($aniv);
         $persona->setResponsable($cbrepresentate);
         $persona->setEstado($estado);
         $persona->setRepresenta($representa);
         $persona->setPartida($partida);
         $persona->setIdtipodocumento($idpersonatipodoc);
         $persona->setModopago($modopago);
         $persona->setTipopersona($tipo);
         $persona->setRol($rol);
//         $personam->setBydefault(0);
         
         $fila = $persona->insert($persona);

    }
    else {
        ?> 
            <script>
               
                 swal('No se realizo registro', 'Ya se encuentra registrado', 'error');
            </script>  <?php
        
    }
     if($fila > 0){
          ?> 
            <script>
               
                 swal("Éxitosamente!", "Operación realizada correctamente.", "success");
                 $('#FormularioAjax').trigger("reset");
                 
            </script>  <?php
            
         
     }       
        
    }else {
        ?> 
            <script>
               
                 swal("Error", "Ingrese campos obligatorios.", "error");
                
                 
            </script>  <?php
        
        
    }
    }
    function crearclient(){
        
        if(permisos::rol('rol-clientenuevo')){
            
            $titulo='NUEVO CLIENTE';
            $tipo = 1;
        
            $docs = $this->tipodoc->selectAll();

            $persona = new persona();
            require_once 'view/layout/header.php';
             require_once 'view/persona/form_persona_client.php';
            
        }else {
            $url='';
            require_once 'view/sinpermiso.php';
        }
             
             require_once 'view/layout/footer.php';

    }
        function crearprovee(){
            
             if(permisos::rol('rol-clientenuevo')){
            $tipo = 2;
        
            $docs = $this->tipodoc->selectAll();
            $titulo = 'NUEVO PROVEEDOR';
            $persona = new persona();
            require_once 'view/layout/header.php';
             require_once 'view/persona/form_persona_provee.php';
             }else {
            $url='';
            require_once 'view/sinpermiso.php';
            }
             require_once 'view/layout/footer.php';

    }
    
    
    function buscar(){
//        var_dump($_POST);
        $data = array();
        if (isset($_POST['ruc']) && !empty($_POST['ruc']) && isset($_POST['cbidtipodoc']) && !empty($_POST['cbidtipodoc'])) {

            $ruc = trim($_POST['ruc']);
//            $tipodoc = $_POST['cbidtipodoc'];
            
            list($tipodoc,$idpersonatipodoc) = explode('-',$_POST['cbidtipodoc']);
            
            $persona = $this->persona->search($ruc,$tipodoc);
//
//            var_dump($persona);
//
            if ($persona != null) {
                $array = array(
                    "id" => $persona->getId(),
                    "ruc" => $persona->getRuc(),
                    "razonSocial" => $persona->getNombre(),
                    "direccionCompleta" => $persona->getDireccion(),
                    "email" => $persona->getEmail(),
                    "telf" => $persona->getTelfijo()
                );
                
                $resp = json_encode($array);

            }elseif ($tipodoc=='1' || $tipodoc == 'RUC'){
      
        
        
        
    //        $name = 'Book name';
                //Server url
                $url = "https://api.cbetcloud.com/api/sunat-search-ruc?ruc=". $ruc;
                $apiKey = 'Bearer '.$_SESSION['token']; // should match with Server key
                $headers = array(
                     'Authorization: '.$apiKey
                );
                // Send request to Server
                $ch = curl_init($url);
                // To save response in a variable from server, set headers;
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                // Get response
                $response = curl_exec($ch);
                // Decode
    //            $result = json_decode($response);
                $resp = json_decode(json_encode($response));

    //            $datos = array(
    //                    0 => 0,
    //                    1 => $resp['ruc'],
    //                    2 => $resp['razonSocial'],
    //                    3 => $resp['direccionCompleta'],
    //                    4 => '-',
//                );
        }else {
            $url = "https://api.cbetcloud.com/api/reniec-search-dni?dni=". $ruc;
            $apiKey = 'Bearer '.$_SESSION['token']; // should match with Server key
            $headers = array(
                 'Authorization: '.$apiKey
            );
            // Send request to Server
            $ch = curl_init($url);
            // To save response in a variable from server, set headers;
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            // Get response
            $response = curl_exec($ch);
            // Decode
//            $result = json_decode($response);
            $resp = json_decode(json_encode($response));
            

            
            
            
        }
            echo $resp; 
//            var_dump($resp);
        
    }
    }
    
    function updatedefault(){
        if(isset($_POST['id']) and isset($_POST['tipo'])){
            $id= $_POST['id'];
            $tipo = $_POST['tipo'];
            echo $this->persona->updatedefault($id,$tipo);
            
        }
        
    }
    
    function listarcliente(){
        $persons = array();
        if(isset($_POST['tipo'])){
            $tipo = $_POST['tipo'];
            
            
        
      
        $personas= $this->persona->select($tipo);
        
        if($tipo == 1){
            $url = 'persona/crearclient';
            $urlup = 'persona/cargarclient';
            $urldel = base_url . 'persona/deleteclient'; 
            
        }
        
        if ($tipo == 2) {
            $url = 'persona/crearprovee';
            $urlup = 'persona/cargarprovee';
            $urldel = base_url . 'persona/deleteprove';
        }
         
        foreach ($personas as $persona){
            
            if($persona->getBydefault() == 1){
                $check = 'checked';
            }else{
                $check = '';
            }
            
            
            
            
            
            $bydefault = '<div class="switch" style="text-align: center;"><label><input type="checkbox" '.$check.' id="ckbydefaultcliente" name="ckbydefaultcliente" value="'.$persona->getId().'"><span class="lever"></span></label> </div>';
            $acciones = '<a href="' . base_url . $urlup . '&id=' . $persona->getId() . '" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>'
                        . ' <button  type="button" class="btn btn-danger" onclick=eliminar(' . $persona->getId() . ',"' . $urldel . '","delete")><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>';
            
            
            $array = [
            "tipodoc" => $persona->getTipodocumento(),
            "ndocumento" => $persona->getRuc(),
            "nombre" => $persona->getNombre(),
            "direccion" => $persona->getDireccion(),
            "email" => $persona->getEmail(),
            "telefono"=> "<a href= tel:".$persona->getTelfijo().">".$persona->getTelfijo()."</a>",    
            "bydefault" => $bydefault,
            "acciones" => $acciones
        ];
        array_push($persons, $array);
            
        }
        }
        echo '{"data":'.json_encode($persons).'}';  // send data as json format
        
        
    }
    
    
    
    }

