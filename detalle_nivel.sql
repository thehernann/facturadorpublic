/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : bdfacturacion

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-07-09 11:24:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for detalle_nivel
-- ----------------------------
DROP TABLE IF EXISTS `detalle_nivel`;
CREATE TABLE `detalle_nivel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `op` varchar(255) DEFAULT NULL,
  `id_nivel` bigint(20) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_nivel` (`id_nivel`),
  CONSTRAINT `fk_nivel` FOREIGN KEY (`id_nivel`) REFERENCES `nivel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2124 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detalle_nivel
-- ----------------------------
INSERT INTO `detalle_nivel` VALUES ('2037', 'rol-permisos', '9', 'Listar nivel');
INSERT INTO `detalle_nivel` VALUES ('2038', 'rol-bancoeditar', '9', 'Editar bancos');
INSERT INTO `detalle_nivel` VALUES ('2039', 'rol-bancoeliminar', '9', 'Eliminar bancos');
INSERT INTO `detalle_nivel` VALUES ('2040', 'rol-bancolistar', '9', 'Listar bancos');
INSERT INTO `detalle_nivel` VALUES ('2041', 'rol-banconuevo', '9', 'Nuevo banco');
INSERT INTO `detalle_nivel` VALUES ('2042', 'rol-cuentabancaeditar', '9', 'Cuenta bancaria editar');
INSERT INTO `detalle_nivel` VALUES ('2043', 'rol-cuentabancaeliminar', '9', 'Cuenta bancaria eliminar');
INSERT INTO `detalle_nivel` VALUES ('2044', 'rol-cuentabancalistar', '9', 'Cuenta bancaria listar');
INSERT INTO `detalle_nivel` VALUES ('2045', 'rol-cuentabancanuevo', '9', 'Cuenta bancaria nuevo');
INSERT INTO `detalle_nivel` VALUES ('2046', 'rol-clienteeliminar', '9', 'Cliente eliminar');
INSERT INTO `detalle_nivel` VALUES ('2047', 'rol-clientelistar', '9', 'Cliente listar');
INSERT INTO `detalle_nivel` VALUES ('2048', 'rol-clienteeditar', '9', 'Cliente editar');
INSERT INTO `detalle_nivel` VALUES ('2049', 'rol-clientenuevo', '9', 'Cliente nuevo');
INSERT INTO `detalle_nivel` VALUES ('2050', 'rol-clientedefecto', '9', 'Cliente por defecto');
INSERT INTO `detalle_nivel` VALUES ('2051', 'rol-proveedoreditar', '9', 'Proveedor editar');
INSERT INTO `detalle_nivel` VALUES ('2052', 'rol-proveedoreliminar', '9', 'Proveedor eliminar');
INSERT INTO `detalle_nivel` VALUES ('2053', 'rol-proveedorlistar', '9', 'Proveedor listar');
INSERT INTO `detalle_nivel` VALUES ('2054', 'rol-proveedornuevo', '9', 'Proveedor nuevo');
INSERT INTO `detalle_nivel` VALUES ('2055', 'rol-proveedordefecto', '9', 'Proveedor por defecto');
INSERT INTO `detalle_nivel` VALUES ('2056', 'rol-marcaeditar', '9', 'Marca editar');
INSERT INTO `detalle_nivel` VALUES ('2057', 'rol-marcaeliminar', '9', 'Marca eliminar');
INSERT INTO `detalle_nivel` VALUES ('2058', 'rol-marcalistar', '9', 'Marca listar');
INSERT INTO `detalle_nivel` VALUES ('2059', 'rol-marcanuevo', '9', 'Marca nuevo');
INSERT INTO `detalle_nivel` VALUES ('2060', 'rol-unideditar', '9', 'Unid. medida editar');
INSERT INTO `detalle_nivel` VALUES ('2061', 'rol-unideliminar', '9', 'Unid. medida eliminar');
INSERT INTO `detalle_nivel` VALUES ('2062', 'rol-unidlistar', '9', 'Unid. medida listar');
INSERT INTO `detalle_nivel` VALUES ('2063', 'rol-unidnuevo', '9', 'Unid. medida nuevo');
INSERT INTO `detalle_nivel` VALUES ('2064', 'rol-categoriaeditar', '9', 'Categoria editar');
INSERT INTO `detalle_nivel` VALUES ('2065', 'rol-categoriaeliminar', '9', 'Categoria eliminar');
INSERT INTO `detalle_nivel` VALUES ('2066', 'rol-categorialistar', '9', 'Categoria listar');
INSERT INTO `detalle_nivel` VALUES ('2067', 'rol-categorianuevo', '9', 'Categoria nuevo');
INSERT INTO `detalle_nivel` VALUES ('2068', 'rol-lineaeditar', '9', 'Linea editar');
INSERT INTO `detalle_nivel` VALUES ('2069', 'rol-lineaeliminar', '9', 'Linea eliminar');
INSERT INTO `detalle_nivel` VALUES ('2070', 'rol-linealistar', '9', 'Linea listar');
INSERT INTO `detalle_nivel` VALUES ('2071', 'rol-lineanuevo', '9', 'Linea nuevo');
INSERT INTO `detalle_nivel` VALUES ('2072', 'rol-articuloeditar', '9', 'Articulo editar');
INSERT INTO `detalle_nivel` VALUES ('2073', 'rol-articuloeliminar', '9', 'Articulo eliminar');
INSERT INTO `detalle_nivel` VALUES ('2074', 'rol-articulolistar', '9', 'Articulo listar');
INSERT INTO `detalle_nivel` VALUES ('2075', 'rol-articulonuevo', '9', 'Articulo nuevo');
INSERT INTO `detalle_nivel` VALUES ('2076', 'rol-servicioeditar', '9', 'Servicio editar');
INSERT INTO `detalle_nivel` VALUES ('2077', 'rol-servicioeliminar', '9', 'Servicio eliminar');
INSERT INTO `detalle_nivel` VALUES ('2078', 'rol-serviciolistar', '9', 'Servicio listar');
INSERT INTO `detalle_nivel` VALUES ('2079', 'rol-servicionuevo', '9', 'Servicio nuevo');
INSERT INTO `detalle_nivel` VALUES ('2080', 'rol-usuarioeditar', '9', 'Usuario editar');
INSERT INTO `detalle_nivel` VALUES ('2081', 'rol-usuarioeliminar', '9', 'Usuario eliminar');
INSERT INTO `detalle_nivel` VALUES ('2082', 'rol-usuariolistar', '9', 'Usuario listar');
INSERT INTO `detalle_nivel` VALUES ('2083', 'rol-usuarionuevo', '9', 'Usuario nuevo');
INSERT INTO `detalle_nivel` VALUES ('2084', 'rol-sucursalprincipalmostrar', '9', 'Sucursal principal mostrar');
INSERT INTO `detalle_nivel` VALUES ('2085', 'rol-sucursaleditar', '9', 'Sucursal editar');
INSERT INTO `detalle_nivel` VALUES ('2086', 'rol-sucursaleliminar', '9', 'Sucursal eliminar');
INSERT INTO `detalle_nivel` VALUES ('2087', 'rol-sucursallistar', '9', 'Sucursal listar');
INSERT INTO `detalle_nivel` VALUES ('2088', 'rol-sucursalnuevo', '9', 'Sucursal nuevo');
INSERT INTO `detalle_nivel` VALUES ('2089', 'rol-documentoeditar', '9', 'Documento editar');
INSERT INTO `detalle_nivel` VALUES ('2090', 'rol-documentoeliminar', '9', 'Documento eliminar');
INSERT INTO `detalle_nivel` VALUES ('2091', 'rol-documentolistar', '9', 'Documento listar');
INSERT INTO `detalle_nivel` VALUES ('2092', 'rol-documentonuevo', '9', 'Documento nuevo');
INSERT INTO `detalle_nivel` VALUES ('2093', 'rol-vercomprobanteventaporsucur', '9', 'Ver comprobantes por sucursal [ventas]');
INSERT INTO `detalle_nivel` VALUES ('2094', 'rol-boletafacturaventa', '9', 'Boleta / Factura [venta] ');
INSERT INTO `detalle_nivel` VALUES ('2095', 'rol-notacreditoventa', '9', 'Nota crédito');
INSERT INTO `detalle_nivel` VALUES ('2096', 'rol-notadebitoventa', '9', 'Nota débito');
INSERT INTO `detalle_nivel` VALUES ('2097', 'rol-cotizacionventa', '9', 'Cotización');
INSERT INTO `detalle_nivel` VALUES ('2098', 'rol-vercotizacionesventa', '9', 'Ver cotizaciones');
INSERT INTO `detalle_nivel` VALUES ('2099', 'rol-vercotizacionesventasucur', '9', 'Ver cotizaciones por sucursal');
INSERT INTO `detalle_nivel` VALUES ('2100', 'rol-vercomprobantecompra', '9', 'Ver comprobates compras');
INSERT INTO `detalle_nivel` VALUES ('2101', 'rol-vercomprobantecomprasucur', '9', 'Ver comprobantes por sucursal [compras]');
INSERT INTO `detalle_nivel` VALUES ('2102', 'rol-compra', '9', 'Compra');
INSERT INTO `detalle_nivel` VALUES ('2103', 'rol-ordencompra', '9', 'Orden de compra');
INSERT INTO `detalle_nivel` VALUES ('2104', 'rol-vercomprobanteordencompra', '9', 'Ver comprobantes orden de compra');
INSERT INTO `detalle_nivel` VALUES ('2105', 'rol-vercomprobanteordencomprasucur', '9', 'Ver orden de compra por sucursal');
INSERT INTO `detalle_nivel` VALUES ('2106', 'rol-ventadetallereporte', '9', 'Venta por detalle');
INSERT INTO `detalle_nivel` VALUES ('2107', 'rol-ventadetallereportesucur', '9', 'Venta detalle por sucursal');
INSERT INTO `detalle_nivel` VALUES ('2108', 'rol-compradetallereporte', '9', 'Compra por detalle');
INSERT INTO `detalle_nivel` VALUES ('2109', 'rol-compradetallereportesucur', '9', 'Compra detalle por sucursal');
INSERT INTO `detalle_nivel` VALUES ('2110', 'rol-generalreporte', '9', 'Stock general');
INSERT INTO `detalle_nivel` VALUES ('2111', 'rol-generalreportesucur', '9', 'Stock general por sucursal');
INSERT INTO `detalle_nivel` VALUES ('2112', 'rol-valorizadoreporte', '9', 'Stock valorizado');
INSERT INTO `detalle_nivel` VALUES ('2113', 'rol-valorizadoreportesucur', '9', 'Stock valorizado por sucursal');
INSERT INTO `detalle_nivel` VALUES ('2114', 'rol-compraventareporte', '9', 'Stock compra - venta ');
INSERT INTO `detalle_nivel` VALUES ('2115', 'rol-compraventareportesucur', '9', 'Stock compra - venta por sucursal');
INSERT INTO `detalle_nivel` VALUES ('2116', 'rol-kardexreporte', '9', 'Kardex');
INSERT INTO `detalle_nivel` VALUES ('2117', 'rol-kardexreportesucur', '9', 'Kardex por sucursal');
INSERT INTO `detalle_nivel` VALUES ('2118', 'rol-cuentascobrarreporte', '9', 'Cuentas por cobrar');
INSERT INTO `detalle_nivel` VALUES ('2119', 'rol-cuentascobrarreportesucur', '9', 'Cuenta por cobrar por sucursal');
INSERT INTO `detalle_nivel` VALUES ('2120', 'rol-cuentaspagarreporte', '9', 'Cuentas por pagar');
INSERT INTO `detalle_nivel` VALUES ('2121', 'rol-cuentaspagarreportesucur', '9', 'Cuentas por pagar sucursal');
INSERT INTO `detalle_nivel` VALUES ('2122', 'rol-movimientocajaoperaciones', '9', 'Movimiento de caja');
INSERT INTO `detalle_nivel` VALUES ('2123', 'rol-trasladooperaciones', '9', 'Traslado de articulos');
