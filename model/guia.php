<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of guia
 *
 * @author HERNAN
 */
class guia {
    //put your code here
    
    private $id;
    private $serie;
    private $numero;
    private $fechaentrega;
    private $fechatraslado;
    private $rucemisor;
    private $razonsocialemisor;
    private $direccionemisor;
    private $rucdestinatario;
    private $razonsialdestinatario;
    private $direcciondestinatario;
    private $motivo;
    private $ructransportista;
    private $razonsocialtransportista;
    private $costotransportista;
    private $marcaplaca;
    private $licenciatransportista;
    private $certificdotransportista;
    private $idsucursal;
    private $motivoanulacion;
    private $montogratuita;
    private $montoexonerado;
    private $montoinafecto;
    private $montoexportacion;
    private $montogravada;
    private $montoigv;
    private $montototal;
    private $flete;
    
    function __construct() {
        
    }
    
    function getFlete() {
        return $this->flete;
    }

    function setFlete($flete) {
        $this->flete = $flete;
    }

        function getRazonsocialtransportista() {
        return $this->razonsocialtransportista;
    }

    function setRazonsocialtransportista($razonsocialtransportista) {
        $this->razonsocialtransportista = $razonsocialtransportista;
    }

        function getId() {
        return $this->id;
    }

    function getSerie() {
        return $this->serie;
    }

    function getNumero() {
        return $this->numero;
    }

    function getFechaentrega() {
        return $this->fechaentrega;
    }

    function getFechatraslado() {
        return $this->fechatraslado;
    }

    function getRucemisor() {
        return $this->rucemisor;
    }

    function getRazonsocialemisor() {
        return $this->razonsocialemisor;
    }

    function getDireccionemisor() {
        return $this->direccionemisor;
    }

    function getRucdestinatario() {
        return $this->rucdestinatario;
    }

    function getRazonsialdestinatario() {
        return $this->razonsialdestinatario;
    }

    function getDirecciondestinatario() {
        return $this->direcciondestinatario;
    }

    function getMotivo() {
        return $this->motivo;
    }

    function getRuctransportista() {
        return $this->ructransportista;
    }

    function getCostotransportista() {
        return $this->costotransportista;
    }

    function getMarcaplaca() {
        return $this->marcaplaca;
    }

    function getLicenciatransportista() {
        return $this->licenciatransportista;
    }

    function getCertificdotransportista() {
        return $this->certificdotransportista;
    }

    function getIdsucursal() {
        return $this->idsucursal;
    }

    function getMotivoanulacion() {
        return $this->motivoanulacion;
    }

    function getMontogratuita() {
        return $this->montogratuita;
    }

    function getMontoexonerado() {
        return $this->montoexonerado;
    }

    function getMontoinafecto() {
        return $this->montoinafecto;
    }

    function getMontoexportacion() {
        return $this->montoexportacion;
    }

    function getMontogravada() {
        return $this->montogravada;
    }

    function getMontoigv() {
        return $this->montoigv;
    }

    function getMontototal() {
        return $this->montototal;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setSerie($serie) {
        $this->serie = $serie;
    }

    function setNumero($numero) {
        $this->numero = $numero;
    }

    function setFechaentrega($fechaentrega) {
        $this->fechaentrega = $fechaentrega;
    }

    function setFechatraslado($fechatraslado) {
        $this->fechatraslado = $fechatraslado;
    }

    function setRucemisor($rucemisor) {
        $this->rucemisor = $rucemisor;
    }

    function setRazonsocialemisor($razonsocialemisor) {
        $this->razonsocialemisor = $razonsocialemisor;
    }

    function setDireccionemisor($direccionemisor) {
        $this->direccionemisor = $direccionemisor;
    }

    function setRucdestinatario($rucdestinatario) {
        $this->rucdestinatario = $rucdestinatario;
    }

    function setRazonsialdestinatario($razonsialdestinatario) {
        $this->razonsialdestinatario = $razonsialdestinatario;
    }

    function setDirecciondestinatario($direcciondestinatario) {
        $this->direcciondestinatario = $direcciondestinatario;
    }

    function setMotivo($motivo) {
        $this->motivo = $motivo;
    }

    function setRuctransportista($ructransportista) {
        $this->ructransportista = $ructransportista;
    }

    function setCostotransportista($costotransportista) {
        $this->costotransportista = $costotransportista;
    }

    function setMarcaplaca($marcaplaca) {
        $this->marcaplaca = $marcaplaca;
    }

    function setLicenciatransportista($licenciatransportista) {
        $this->licenciatransportista = $licenciatransportista;
    }

    function setCertificdotransportista($certificdotransportista) {
        $this->certificdotransportista = $certificdotransportista;
    }

    function setIdsucursal($idsucursal) {
        $this->idsucursal = $idsucursal;
    }

    function setMotivoanulacion($motivoanulacion) {
        $this->motivoanulacion = $motivoanulacion;
    }

    function setMontogratuita($montogratuita) {
        $this->montogratuita = $montogratuita;
    }

    function setMontoexonerado($montoexonerado) {
        $this->montoexonerado = $montoexonerado;
    }

    function setMontoinafecto($montoinafecto) {
        $this->montoinafecto = $montoinafecto;
    }

    function setMontoexportacion($montoexportacion) {
        $this->montoexportacion = $montoexportacion;
    }

    function setMontogravada($montogravada) {
        $this->montogravada = $montogravada;
    }

    function setMontoigv($montoigv) {
        $this->montoigv = $montoigv;
    }

    function setMontototal($montototal) {
        $this->montototal = $montototal;
    }
    
    function selectAll() {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from guia  order by id desc;");


        $guias = array();
        foreach ($data_tabla as $clave => $valor) {
            $guia = new guia();
            $guia->setId($data_tabla[$clave]["id"]);
            $guia->setserie($data_tabla[$clave]["serie"]);
            $guia->setNumero($data_tabla[$clave]["numero"]);
            $guia->setFechaemision($data_tabla[$clave]["fechaentrega"]);
            $guia->setFechavencimiento($data_tabla[$clave]["fechatraslado"]);
            $guia->setNorden($data_tabla[$clave]["rucemisor"]);
            $guia->setMoneda($data_tabla[$clave]["razonsocialemisor"]);
            $guia->setIncigv($data_tabla[$clave]["direccionemisor"]);
            $guia->setIdsunattransaction($data_tabla[$clave]["rucdestinatario"]);
            $guia->setIdusuario($data_tabla[$clave]["razonsocialdestinatario"]);
            $guia->setTipoventa($data_tabla[$clave]["direcciondestinatario"]);
            $guia->setTipoventamov($data_tabla[$clave]["motivo"]);
            $guia->setTipoventaop($data_tabla[$clave]["ructransportista"]);
            $guia->setSujetoa($data_tabla[$clave]["razonsocialtransportista"]);
            $guia->setIdpersona($data_tabla[$clave]["costotransportista"]);
            $guia->setRuc($data_tabla[$clave]["marca_placa"]);
            $guia->setRazonsocial($data_tabla[$clave]["licenciatransportista"]);
            $guia->setDireccion($data_tabla[$clave]["certificadotransportista"]);
            $guia->setEmail($data_tabla[$clave]["id_sucursal"]);
            $guia->setEstadosunat($data_tabla[$clave]["motivoanulacion"]);
            $guia->setEstadolocal($data_tabla[$clave]["montogratuita"]);
            $guia->setEmail($data_tabla[$clave]["montoexonerado"]);
            $guia->setTipo($data_tabla[$clave]["montoinafecto"]);
            $guia->setTotal($data_tabla[$clave]["montoexportacion"]);
            $guia->setIdsucursal($data_tabla[$clave]["montogravada"]);
            $guia->setTipopago($data_tabla[$clave]["montoigv"]);
            $guia->setNrooptipopago($data_tabla[$clave]["montototal"]);

            array_push($guias, $guia);
        }
        return $guias;
    }
    function selectOne($id) {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from guia  where id=?;", array($id));


        $guia = new guia();
        foreach ($data_tabla as $clave => $valor) {
            
            $guia->setId($data_tabla[$clave]["id"]);
            $guia->setserie($data_tabla[$clave]["serie"]);
            $guia->setNumero($data_tabla[$clave]["numero"]);
            $guia->setFechaemision($data_tabla[$clave]["fechaentrega"]);
            $guia->setFechavencimiento($data_tabla[$clave]["fechatraslado"]);
            $guia->setNorden($data_tabla[$clave]["rucemisor"]);
            $guia->setMoneda($data_tabla[$clave]["razonsocialemisor"]);
            $guia->setIncigv($data_tabla[$clave]["direccionemisor"]);
            $guia->setIdsunattransaction($data_tabla[$clave]["rucdestinatario"]);
            $guia->setIdusuario($data_tabla[$clave]["razonsocialdestinatario"]);
            $guia->setTipoventa($data_tabla[$clave]["direcciondestinatario"]);
            $guia->setTipoventamov($data_tabla[$clave]["motivo"]);
            $guia->setTipoventaop($data_tabla[$clave]["ructransportista"]);
            $guia->setSujetoa($data_tabla[$clave]["razonsocialtransportista"]);
            $guia->setIdpersona($data_tabla[$clave]["costotransportista"]);
            $guia->setRuc($data_tabla[$clave]["marca_placa"]);
            $guia->setRazonsocial($data_tabla[$clave]["licenciatransportista"]);
            $guia->setDireccion($data_tabla[$clave]["certificadotransportista"]);
            $guia->setEmail($data_tabla[$clave]["id_sucursal"]);
            $guia->setEstadosunat($data_tabla[$clave]["motivoanulacion"]);
            $guia->setEstadolocal($data_tabla[$clave]["montogratuita"]);
            $guia->setEmail($data_tabla[$clave]["montoexonerado"]);
            $guia->setTipo($data_tabla[$clave]["montoinafecto"]);
            $guia->setTotal($data_tabla[$clave]["montoexportacion"]);
            $guia->setIdsucursal($data_tabla[$clave]["montogravada"]);
            $guia->setTipopago($data_tabla[$clave]["montoigv"]);
            $guia->setNrooptipopago($data_tabla[$clave]["montototal"]);

            
        }
        return $guia;
    }
    function selectMax($serie,$idsucur) {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from guia  order by id DESC LIMIT 1;"); 


        $guia = new guia();
        foreach ($data_tabla as $clave => $valor) {
            
            $guia->setId($data_tabla[$clave]["id"]);
            $guia->setserie($data_tabla[$clave]["serie"]);
            $guia->setNumero($data_tabla[$clave]["numero"]);
       

            
        }
        return $guia;
    }
   
        function insert(guia $guia) {

        $data_source = new DataSource();
        $id = 0;

        $data_source->ejecutarActualizacion("INSERT INTO guia(serie, numero, fechaentrega, fechatraslado, rucemisor, "
                . "razonsocialemisor, direccionemisor, rucdestinatario, razonsocialdestinatario, direcciondestinatario, "
                . "motivo, ructransportista, razonsocialtransportista, costotransportista, marca_placa, licenciatransportista,"
                . " certificadotransportista, id_sucursal, motivoanulacion, montogratuita, montoexonerado, montoinafecto, "
                . "montoexportacion, montogravada, montoigv, montototal) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", array(
            $guia->getSerie(),
            $guia->getNumero(),
            $guia->getFechaentrega(),
            $guia->getFechatraslado(),
            $guia->getRucemisor(),
            $guia->getRazonsocialemisor(),
            $guia->getDireccionemisor(),
            $guia->getRucdestinatario(),
            $guia->getRazonsialdestinatario(),
            $guia->getDirecciondestinatario(),
            $guia->getMotivo(),
            $guia->getRuctransportista(),
            $guia->getRazonsocialtransportista(),
            $guia->getCostotransportista(),
            $guia->getMarcaplaca(),
            $guia->getLicenciatransportista(),
            $guia->getCertificdotransportista(),
            $guia->getIdsucursal(),
            $guia->getMotivoanulacion(),
            $guia->getMontogratuita(),
            $guia->getMontoexonerado(),
            $guia->getMontoinafecto(),
            $guia->getMontoexportacion(),
            $guia->getMontogravada(),
            $guia->getMontoigv(),
            $guia->getMontototal()
           
            
        ));
        $id = $data_source->lastinsertid();
        return $id;
    }

    
     function update(guia $guia) {

        $data_source = new DataSource();
        $fila = 0;

        $fila = $data_source->ejecutarActualizacion("UPDATE guia set serie=?, numero=?, fechaentrega=?, fechatraslado=?, rucemisor=?, "
                . "razonsocialemisor=?, direccionemisor=?, rucdestinatario=?, razonsocialdestinatario=?, direcciondestinatario=?, "
                . "motivo=?, ructransportista=?, razonsocialtransportista=?, costotransportista=?, marca_placa=?, licenciatransportista=?,"
                . " certificadotransportista, id_sucursal, motivoanulacion, montogratuita, montoexonerado, montoinafecto, "
                . "montoexportacion=?, montogravada=?, montoigv=?, montototal=? where id = ?", array(
            $guia->getSerie(),
            $guia->getNumero(),
            $guia->getFechaentrega(),
            $guia->getFechatraslado(),
            $guia->getRucemisor(),
            $guia->getRazonsocialemisor(),
            $guia->getDireccionemisor(),
            $guia->getRucdestinatario(),
            $guia->getRazonsialdestinatario(),
            $guia->getDirecciondestinatario(),
            $guia->getMotivo(),
            $guia->getRuctransportista(),
            $guia->getRazonsocialtransportista(),
            $guia->getCostotransportista(),
            $guia->getMarcaplaca(),
            $guia->getLicenciatransportista(),
            $guia->getCertificdotransportista(),
            $guia->getIdsucursal(),
            $guia->getMotivoanulacion(),
            $guia->getMontogratuita(),
            $guia->getMontoexonerado(),
            $guia->getMontoinafecto(),
            $guia->getMontoexportacion(),
            $guia->getMontogravada(),
            $guia->getMontoigv(),
            $guia->getMontototal(),
            $guia->getId()
           
            
        ));
        
        return $fila;
    }
    
      function duplicado($serie, $numero) {
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select 1 from guia where serie = ? and numero = ?;", array($serie, $numero));
        $bol = 'valido';
        foreach ($data_tabla as $clave => $valor) {
            $bol = 'duplicado';
        }
        return $bol;
    }


    
    
}
