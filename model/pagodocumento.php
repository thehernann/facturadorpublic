<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pagodocumento
 *
 * @author HERNAN
 */
class pagodocumento {
    //put your code here
    private $id;
    private $tipopago;
    private $entidad;
    private $nop;
    private $monto;
    private $fecha;
    private $observacion;
    private $id_documento;
    
    function __construct() {
        
    }
    function getId() {
        return $this->id;
    }

    function getTipopago() {
        return $this->tipopago;
    }

    function getEntidad() {
        return $this->entidad;
    }

    function getNop() {
        return $this->nop;
    }

    function getMonto() {
        return $this->monto;
    }

    function getFecha() {
        return $this->fecha;
    }

    function getObservacion() {
        return $this->observacion;
    }

    function getId_documento() {
        return $this->id_documento;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTipopago($tipopago) {
        $this->tipopago = $tipopago;
    }

    function setEntidad($entidad) {
        $this->entidad = $entidad;
    }

    function setNop($nop) {
        $this->nop = $nop;
    }

    function setMonto($monto) {
        $this->monto = $monto;
    }

    function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    function setObservacion($observacion) {
        $this->observacion = $observacion;
    }

    function setId_documento($id_documento) {
        $this->id_documento = $id_documento;
    }
    
//        function selectOne($id) {
//
//        $data_source = new DataSource();
//
//        $data_tabla = $data_source->ejecutarconsulta("select * from documento_pagos where id_documento= ? order by id desc;",array($id));
//
//
//        $cabecera = array();
//        foreach ($data_tabla as $clave => $valor) {
//            $pago = new pagodocumento();
//            $pago->setId($data_tabla[$clave]["id"]);
//            $pago->setserie($data_tabla[$clave]["tipopago"]);
//            $pago->setNumero($data_tabla[$clave]["numero"]);
//            $pago->setFechaemision($data_tabla[$clave]["fechaemision"]);
//            $pago->setFechavencimiento($data_tabla[$clave]["fechavencimiento"]);
//            $pago->setNorden($data_tabla[$clave]["nroden"]);
//            $pago->setMoneda($data_tabla[$clave]["moneda"]);
//            $pago->setIncigv($data_tabla[$clave]["incigv"]);
//            $pago->setIdsunattransaction($data_tabla[$clave]["id_sunat_transaction"]);
//            $pago->setIdusuario($data_tabla[$clave]["id_usuario"]);
//            $pago->setTipoventa($data_tabla[$clave]["tipo_venta"]);
//            $pago->setTipoventamov($data_tabla[$clave]["tipo_venta_movimiento"]);
// 
//            array_push($documentos, $documento);
//        }
//        return $documentos;
//    }
        function selectOne($id) {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from documento_pagos where id_documento= ? order by fecha desc;",array($id));


        $cabecera = array();
        foreach ($data_tabla as $clave => $valor) {
            $pago = new pagodocumento();
            $pago->setId($data_tabla[$clave]["id"]);
            $pago->setTipopago($data_tabla[$clave]["tipopago"]);
            $pago->setEntidad($data_tabla[$clave]["entidad"]);
            $pago->setNop($data_tabla[$clave]["nop"]);
            $pago->setMonto($data_tabla[$clave]["monto"]);
            $pago->setFecha($data_tabla[$clave]["fecha"]);
            $pago->setObservacion($data_tabla[$clave]["observacion"]);
            $pago->setId_documento($data_tabla[$clave]["id_documento"]);

 
            array_push($cabecera, $pago);
        }
        return $cabecera;
    }
        function insert(pagodocumento $pago) {

        $data_source = new DataSource();

        $fila = $data_source->ejecutarActualizacion("insert into documento_pagos(tipopago,entidad,nop,monto,fecha,observacion"
                . ",id_documento) values(?,?,?,?,?,?,?);",array(
                    $pago->getTipopago(),
                    $pago->getEntidad(),
                    $pago->getNop(),
                    $pago->getMonto(),
                    $pago->getFecha(),
                    $pago->getObservacion(),
                    $pago->getId_documento()
                    
                    ));

        return $fila;
    }



    
            
}
