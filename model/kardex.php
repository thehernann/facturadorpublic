<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of kardex
 *
 * @author HERNAN
 */
class kardex {
    //put your code here
    private $id;
    private $iddetalle;
    private $concepto;
    private $cantidad;
    private $precio;
    private $idproducto;
    private $stockactual;
    private $stockanterior;
    private $idalmacen;
    private $idempresa; 
    private $createdat;
    private $observacion;
    private $moneda;
    
    
    private $descripcion;
    
    private $stockanteriorserie;
    private $tipodeajuste;
    private $series;
    
    function __construct() {
        
    }
    
    function getSeries() {
        return $this->series;
    }

    function setSeries($series) {
        $this->series = $series;
    }

        
    function getTipodeajuste() {
        return $this->tipodeajuste;
    }

    function setTipodeajuste($tipodeajuste) {
        $this->tipodeajuste = $tipodeajuste;
    }

        
    function getStockanteriorserie() {
        return $this->stockanteriorserie;
    }

    function setStockanteriorserie($stockanteriorserie) {
        $this->stockanteriorserie = $stockanteriorserie;
    }

        
    function getDescripcion() {
        return $this->descripcion;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

        
    function getMoneda() {
        return $this->moneda;
    }

    function setMoneda($moneda) {
        $this->moneda = $moneda;
    }

        
    function getObservacion() {
        return $this->observacion;
    }

    function setObservacion($observacion) {
        $this->observacion = $observacion;
    }

        
    function getIdempresa() {
        return $this->idempresa;
    }

    function getCreatedat() {
        return $this->createdat;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

    function setCreatedat($createdat) {
        $this->createdat = $createdat;
    }

        
    function getId() {
        return $this->id;
    }

    function getIddetalle() {
        return $this->iddetalle;
    }

    function getConcepto() {
        return $this->concepto;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function getPrecio() {
        return $this->precio;
    }

    function getIdproducto() {
        return $this->idproducto;
    }

    function getStockactual() {
        return $this->stockactual;
    }

    function getStockanterior() {
        return $this->stockanterior;
    }

    function getIdalmacen() {
        return $this->idalmacen;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIddetalle($iddetalle) {
        $this->iddetalle = $iddetalle;
    }

    function setConcepto($concepto) {
        $this->concepto = $concepto;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    function setPrecio($precio) {
        $this->precio = $precio;
    }

    function setIdproducto($idproducto) {
        $this->idproducto = $idproducto;
    }

    function setStockactual($stockactual) {
        $this->stockactual = $stockactual;
    }

    function setStockanterior($stockanterior) {
        $this->stockanterior = $stockanterior;
    }

    function setIdalmacen($idalmacen) {
        $this->idalmacen = $idalmacen;
    }
    
    function insert(array $detalles) {

        $data_source = new DataSource();
        $filas = array();

        $filas = $data_source->insertmultiple("insert into kardex (id_detalle,concepto ,cantidad ,precio,"
                . "id_producto,stock_actual,stock_anterior,created_at,id_almacen,id_empresa,observacion,moneda,descripcion,stock_anterior_serie,series) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", $detalles
        );
        return $filas;
    }
    function insertOne(kardex $kardex) {

        $data_source = new DataSource();
        

        $data_source->ejecutarActualizacion("insert into kardex (id_detalle,concepto ,cantidad ,precio,"
                . "id_producto,stock_actual,stock_anterior,id_almacen,id_empresa,observacion,moneda,descripcion,stock_anterior_serie,created_at,tipodeajuste) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                array( $kardex->getIddetalle(),
                       $kardex->getConcepto(),
                       $kardex->getCantidad(),
                       $kardex->getPrecio(),
                       $kardex->getIdproducto(),
                       $kardex->getStockactual(),
                       $kardex->getStockanterior(),
                       $kardex->getIdalmacen(),
                       $kardex->getIdempresa(),
                       $kardex->getObservacion(),
                       $kardex->getMoneda(),
                       $kardex->getDescripcion(),
                       $kardex->getStockanteriorserie(),
                       date('Y-m-d H:i:s'),
                       $kardex->getTipodeajuste()
                    
                )
        );
        return $data_source->lastinsertid();
    }
    
    function stockbyalmacen($idprod){
        
         $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("SELECT a.nombre,p.descripcion,k.stock_actual  from kardex as k INNER JOIN almacen as a on k.id_almacen = a.id 
        inner join producto as p on p.id=k.id_producto where k.id_producto=? and k.id_empresa=? ;", array($idprod,$_SESSION['idempresa']));
        
        $items = array();
        

        foreach ($data_tabla as $clave => $valor) {
            $item = array(
                "almacen"=>$data_tabla[$clave]["nombre"],
                "descripcion"=>$data_tabla[$clave]["descripcion"],
                "stock_actual"=>$data_tabla[$clave]["stock_actual"] 
            );

            array_push($items, $item);
        }
        return $items;
        
    }
    
    function ultimomovimientoproducto($idprod){
        
        
         $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("
        SELECT
	*
        FROM
                kardex
        WHERE
        id_producto = ?
        AND id_empresa = ?
        AND id_almacen = ?
        ORDER BY
                id DESC
        LIMIT 1;", array($idprod,$_SESSION['idempresa'],$_SESSION['idalmacen']));
        
        $kardex = new kardex();      

        foreach ($data_tabla as $clave => $valor) {
            
            $kardex->setId($data_tabla[$clave]["id"]);
            $kardex->setIddetalle($data_tabla[$clave]["id_detalle"]);
            $kardex->setConcepto($data_tabla[$clave]["concepto"]);
            $kardex->setCantidad($data_tabla[$clave]["cantidad"]);
            $kardex->setPrecio($data_tabla[$clave]["precio"]);
            $kardex->setIdproducto($data_tabla[$clave]["id_producto"]);
            $kardex->setStockactual($data_tabla[$clave]["stock_actual"]);
            $kardex->setStockanterior($data_tabla[$clave]["stock_anterior"]);
            $kardex->setCreatedat($data_tabla[$clave]["created_at"]);
            $kardex->setIdalmacen($data_tabla[$clave]["id_almacen"]);
            $kardex->setIdempresa($data_tabla[$clave]["id_empresa"]);
            $kardex->setObservacion($data_tabla[$clave]["observacion"]);
            $kardex->setMoneda($data_tabla[$clave]["moneda"]);
            $kardex->setDescripcion($data_tabla[$clave]["descripcion"]);

        }
        return $kardex; 
    }
    
    function ultimomovimientoproductoalmacen($idprod,$idalmacen){
        
        
         $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("
        SELECT
	*
        FROM
                kardex
        WHERE
        id_producto = ?
        AND id_empresa = ?
        AND id_almacen = ?
        ORDER BY
                id DESC
        LIMIT 1;", array($idprod,$_SESSION['idempresa'],$idalmacen));
        
        $kardex = new kardex();      

        foreach ($data_tabla as $clave => $valor) {
            
            $kardex->setId($data_tabla[$clave]["id"]);
            $kardex->setIddetalle($data_tabla[$clave]["id_detalle"]);
            $kardex->setConcepto($data_tabla[$clave]["concepto"]);
            $kardex->setCantidad($data_tabla[$clave]["cantidad"]);
            $kardex->setPrecio($data_tabla[$clave]["precio"]);
            $kardex->setIdproducto($data_tabla[$clave]["id_producto"]);
            $kardex->setStockactual($data_tabla[$clave]["stock_actual"]);
            $kardex->setStockanterior($data_tabla[$clave]["stock_anterior"]);
            $kardex->setCreatedat($data_tabla[$clave]["created_at"]);
            $kardex->setIdalmacen($data_tabla[$clave]["id_almacen"]);
            $kardex->setIdempresa($data_tabla[$clave]["id_empresa"]);
            $kardex->setObservacion($data_tabla[$clave]["observacion"]);
            $kardex->setMoneda($data_tabla[$clave]["moneda"]);
            $kardex->setDescripcion($data_tabla[$clave]["descripcion"]);

        }
        return $kardex; 
    }
    
    
    function primermovimientoproducto($idprod){
        
        
         $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("
        SELECT
	*
        FROM
                kardex
        WHERE
        id_producto = ?
        AND id_empresa = ?
        AND id_almacen = ?
        ORDER BY
                id ASC
        LIMIT 1;", array($idprod,$_SESSION['idempresa'],$_SESSION['idalmacen']));
        
        $kardex = new kardex();      

        foreach ($data_tabla as $clave => $valor) {
            
            $kardex->setId($data_tabla[$clave]["id"]);
            $kardex->setIddetalle($data_tabla[$clave]["id_detalle"]);
            $kardex->setConcepto($data_tabla[$clave]["concepto"]);
            $kardex->setCantidad($data_tabla[$clave]["cantidad"]);
            $kardex->setPrecio($data_tabla[$clave]["precio"]);
            $kardex->setIdproducto($data_tabla[$clave]["id_producto"]);
            $kardex->setStockactual($data_tabla[$clave]["stock_actual"]);
            $kardex->setStockanterior($data_tabla[$clave]["stock_anterior"]);
            $kardex->setCreatedat($data_tabla[$clave]["created_at"]);
            $kardex->setIdalmacen($data_tabla[$clave]["id_almacen"]);
            $kardex->setIdempresa($data_tabla[$clave]["id_empresa"]);
            $kardex->setObservacion($data_tabla[$clave]["observacion"]);
            $kardex->setMoneda($data_tabla[$clave]["moneda"]);
            $kardex->setDescripcion($data_tabla[$clave]["descripcion"]);

        }
        return $kardex; 
    }




//    function selectstocksucursal($idempresa) {
//
//        $data_source = new DataSource();
//
//        $data_tabla = $data_source->ejecutarconsulta("select CONCAT(s.nombre,' - ',s.direccion,' ') as sucursal,descripcion,case when p.incluir='Si' then SUM(sp.cantidad) else stock  end as stock
//        from producto as p inner join sucursal as s on s.id = p.id_sucursal left join serie_producto as sp 
//        on sp.id_producto = p.id WHERE p.codigo=?  GROUP BY p.id order by p.id desc;", array($codigo));
//        
//        $productos = array();
//        
//
//        foreach ($data_tabla as $clave => $valor) {
//            $producto = new producto();
//            
//            $producto->setDescripcion($data_tabla[$clave]["descripcion"]);
//            $producto->setStock($data_tabla[$clave]["stock"]);
//            $producto->setSucursal($data_tabla[$clave]["sucursal"]);
//            
//            array_push($productos, $producto);
//        }
//        return $productos;
//    }



    
    
}
