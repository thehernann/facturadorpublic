<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of producto
 *
 * @author HERNAN
 */
class producto {

    //put your code here
    private $id;
    private $tipo;
    private $codigo;
    private $codbarra;
    private $marca;
    private $linea;
    private $categoria;
    private $descripcion;
    private $unidmed;
    private $moneda;
    private $precioc;
    private $preciov;
    private $preciovmin;
    private $stock;
    private $peso;
    private $incluir;
    private $nrocuenta;
    private $observacion;
    private $idtipoimpuesto;
    private $prodservi;
    private $idcategoria;
    private $idlinea;
    private $idmarca;
    private $idunidmedida;
    
//    private $sucursal;
    private $caracteristica;
    
    private $serie;
    
    private $codigoalternativo;
    private $descripciondos;
    
    private $preciodos;
    private $preciotres;
    private $idempresa;
    
    private $observaciondos;
    
    

    function __construct() {
        
    }
    
    function getObservaciondos() {
        return $this->observaciondos;
    }

    function setObservaciondos($observaciondos) {
        $this->observaciondos = $observaciondos;
    }

        
    function getIdempresa() {
        return $this->idempresa;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

        
    function getPreciodos() {
        return $this->preciodos;
    }

    function getPreciotres() {
        return $this->preciotres;
    }

    function setPreciodos($preciodos) {
        $this->preciodos = $preciodos;
    }

    function setPreciotres($preciotres) {
        $this->preciotres = $preciotres;
    }

        function getCodigoalternativo() {
        return $this->codigoalternativo;
    }

    function getDescripciondos() {
        return $this->descripciondos;
    }

    function setCodigoalternativo($codigoalternativo) {
        $this->codigoalternativo = $codigoalternativo;
    }

    function setDescripciondos($descripciondos) {
        $this->descripciondos = $descripciondos;
    }

        function getCaracteristica() {
        return $this->caracteristica;
    }
    
    function getSerie() {
        return $this->serie;
    }

    function setSerie($serie) {
        $this->serie = $serie;
    }

    
    function setCaracteristica($caracteristica) {
        $this->caracteristica = $caracteristica;
    }

    
    function getId() {
        return $this->id;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getCodbarra() {
        return $this->codbarra;
    }

    function getMarca() {
        return $this->marca;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getUnidmed() {
        return $this->unidmed;
    }

    function getMoneda() {
        return $this->moneda;
    }

    function getPrecioc() {
        return $this->precioc;
    }

    function getPreciov() {
        return $this->preciov;
    }

    function getPreciovmin() {
        return $this->preciovmin;
    }

    function getStock() {
        return $this->stock;
    }

    function getPeso() {
        return $this->peso;
    }

    function getIncluir() {
        return $this->incluir;
    }

    function getNrocuenta() {
        return $this->nrocuenta;
    }

    function getObservacion() {
        return $this->observacion;
    }

    function getIdtipoimpuesto() {
        return $this->idtipoimpuesto;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setCodbarra($codbarra) {
        $this->codbarra = $codbarra;
    }

    function setMarca($marca) {
        $this->marca = $marca;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setUnidmed($unidmed) {
        $this->unidmed = $unidmed;
    }

    function setMoneda($moneda) {
        $this->moneda = $moneda;
    }

    function setPrecioc($precioc) {
        $this->precioc = $precioc;
    }

    function setPreciov($preciov) {
        $this->preciov = $preciov;
    }

    function setPreciovmin($preciovmin) {
        $this->preciovmin = $preciovmin;
    }

    function setStock($stock) {
        $this->stock = $stock;
    }

    function setPeso($peso) {
        $this->peso = $peso;
    }

    function setIncluir($incluir) {
        $this->incluir = $incluir;
    }

    function setNrocuenta($nrocuenta) {
        $this->nrocuenta = $nrocuenta;
    }

    function setObservacion($observacion) {
        $this->observacion = $observacion;
    }

    function setIdtipoimpuesto($idtipoimpuesto) {
        $this->idtipoimpuesto = $idtipoimpuesto;
    }

    function getProdservi() {
        return $this->prodservi;
    }

    function getIdcategoria() {
        return $this->idcategoria;
    }

    function getIdlinea() {
        return $this->idlinea;
    }

    function getIdmarca() {
        return $this->idmarca;
    }

    function getIdunidmedida() {
        return $this->idunidmedida;
    }

    function setProdservi($prodservi) {
        $this->prodservi = $prodservi;
    }

    function setIdcategoria($idcategoria) {
        $this->idcategoria = $idcategoria;
    }

    function setIdlinea($idlinea) {
        $this->idlinea = $idlinea;
    }

    function setIdmarca($idmarca) {
        $this->idmarca = $idmarca;
    }

    function setIdunidmedida($idunidmedida) {
        $this->idunidmedida = $idunidmedida;
    }
//    function getSucursal() {
//        return $this->sucursal;
//    }
//
//    function setSucursal($sucursal) {
//        $this->sucursal = $sucursal;
//    }
    
    function getLinea() {
        return $this->linea;
    }

    function getCategoria() {
        return $this->categoria;
    }

    function setLinea($linea) {
        $this->linea = $linea;
    }

    function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    
    
    function select($tipo) {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select p.id,tipo,p.codigo,p.codigobarra,p.marca,p.descripcion,unidmed,
    moneda,precioc,preciov,preciovmin,peso,incluir,nrocuenta,observacion,id_tipo_impuesto,prod_servi,id_categoria,
    id_linea,id_marca,id_unidmedida,codigoalternativo,descripciondos from producto as p 
    where prod_servi=? and activo=1 and p.id_empresa = ? GROUP BY p.id order by p.id desc;", array($tipo,$_SESSION['idempresa']));

        $producto = null;
        $productos = array();
        foreach ($data_tabla as $clave => $valor) {
            
//            if($data_tabla[$clave]["series"] != ''){
//                $series = '- Series: '.$data_tabla[$clave]["series"].'  ';
//            }else {
//                $series=$data_tabla[$clave]["series"];
//            }
            $producto = new producto();
            $producto->setId($data_tabla[$clave]["id"]);
            $producto->setTipo($data_tabla[$clave]["tipo"]);
            $producto->setCodigo($data_tabla[$clave]["codigo"]);
            $producto->setCodbarra($data_tabla[$clave]["codigobarra"]);
            $producto->setMarca($data_tabla[$clave]["marca"]);
            $producto->setDescripcion($data_tabla[$clave]["descripcion"]);
            $producto->setUnidmed($data_tabla[$clave]["unidmed"]);
            $producto->setMoneda($data_tabla[$clave]["moneda"]);
            $producto->setPrecioc($data_tabla[$clave]["precioc"]);
            $producto->setPreciov($data_tabla[$clave]["preciov"]);
            $producto->setPreciovmin($data_tabla[$clave]["preciovmin"]);
//            $producto->setStock($data_tabla[$clave]["stock"]);
            $producto->setPeso($data_tabla[$clave]["peso"]);
            $producto->setIncluir($data_tabla[$clave]["incluir"]);
            $producto->setNrocuenta($data_tabla[$clave]["nrocuenta"]);
            $producto->setObservacion($data_tabla[$clave]["observacion"]);
            $producto->setIdtipoimpuesto($data_tabla[$clave]["id_tipo_impuesto"]);
            $producto->setProdservi($data_tabla[$clave]["prod_servi"]);
            $producto->setIdcategoria($data_tabla[$clave]["id_categoria"]);
            $producto->setIdlinea($data_tabla[$clave]["id_linea"]);
            $producto->setIdmarca($data_tabla[$clave]["id_marca"]);
            $producto->setIdunidmedida($data_tabla[$clave]["id_unidmedida"]);
            
            $producto->setCodigoalternativo($data_tabla[$clave]["codigoalternativo"]);
            $producto->setDescripciondos($data_tabla[$clave]["descripciondos"]);
            array_push($productos, $producto);
        }
        return $productos;
    }
    function selectstocksucursal($idprod) {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("SELECT
	(
		SELECT
			stock_actual
		FROM
			kardex ka
		WHERE
			ka.id_almacen = a.id and p.id = ka.id_producto and ka.id_empresa = a.id_empresa
		ORDER BY
			ka.id DESC
		LIMIT 1
	) as stock,
	CONCAT(a.nombre, ' - ',a.direccion) as almacen,
	p.descripcion
        
        FROM
                almacen a
        INNER JOIN kardex k ON a.id = k.id_almacen INNER JOIN 
        producto as p on k.id_producto = p.id
        WHERE
                k.id_producto = ? and a.id_empresa = ?
        GROUP BY
	a.id", array($idprod,$_SESSION['idempresa']));

      
        $array = array();
        foreach ($data_tabla as $clave => $valor) {
            
            $item= array(
                "almacen" => $data_tabla[$clave]["almacen"],
                "stock" => $data_tabla[$clave]["stock"],
                "descripcion" => $data_tabla[$clave]["descripcion"]
            );
            
            array_push($array, $item);
          
        }
        return $array;
    }
    
    

    function selectone($id) {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from producto where id=? and id_empresa=?;", array($id,$_SESSION['idempresa']));

        $producto = new producto();

        foreach ($data_tabla as $clave => $valor) {

            $producto->setId($data_tabla[$clave]["id"]);
            $producto->setTipo($data_tabla[$clave]["tipo"]);
            $producto->setCodigo($data_tabla[$clave]["codigo"]);
            $producto->setCodbarra($data_tabla[$clave]["codigobarra"]);
            $producto->setMarca($data_tabla[$clave]["marca"]);
            $producto->setDescripcion($data_tabla[$clave]["descripcion"]);
            $producto->setUnidmed($data_tabla[$clave]["unidmed"]);
            $producto->setMoneda($data_tabla[$clave]["moneda"]);
            $producto->setPrecioc($data_tabla[$clave]["precioc"]);
            $producto->setPreciov($data_tabla[$clave]["preciov"]);
            $producto->setPreciovmin($data_tabla[$clave]["preciovmin"]);
//            $producto->setStock($data_tabla[$clave]["stock"]);
            $producto->setPeso($data_tabla[$clave]["peso"]);
            $producto->setIncluir($data_tabla[$clave]["incluir"]);
            $producto->setNrocuenta($data_tabla[$clave]["nrocuenta"]);
            $producto->setObservacion($data_tabla[$clave]["observacion"]);
            $producto->setIdtipoimpuesto($data_tabla[$clave]["id_tipo_impuesto"]);
            $producto->setProdservi($data_tabla[$clave]["prod_servi"]);
            $producto->setIdcategoria($data_tabla[$clave]["id_categoria"]);
            $producto->setIdlinea($data_tabla[$clave]["id_linea"]);
            $producto->setIdmarca($data_tabla[$clave]["id_marca"]);
            $producto->setIdunidmedida($data_tabla[$clave]["id_unidmedida"]);
            
            $producto->setCodigoalternativo($data_tabla[$clave]["codigoalternativo"]);
            $producto->setDescripciondos($data_tabla[$clave]["descripciondos"]);
            
            $producto->setPreciodos($data_tabla[$clave]["preciodos"]);
            $producto->setPreciotres($data_tabla[$clave]["preciotres"]);
            $producto->setObservaciondos($data_tabla[$clave]["observaciondos"]);
        }
        return $producto;
    }
    
    function selectcodsucursal($codigo){
        
        $data_source = new DataSource();

    $data_tabla = $data_source->ejecutarconsulta("select id from producto where codigo=? and id_empresa=?;", array($codigo,$_SESSION['idempresa']));

        
        $id = 0;
        foreach ($data_tabla as $clave => $valor) {

            $id = ($data_tabla[$clave]["id"]);
            
        
        }
        
        return $id;
    }         
    
    
    

    function insert(producto $producto) {

        $data_source = new DataSource();
        $id = 0;
//        $cliente = new cliente();
//        $cliente = $objeto;
//        var_dump($producto);
        $data_source->ejecutarActualizacion("insert into producto (tipo, codigo,"
                . "codigobarra,marca,descripcion,unidmed,"
                . "moneda, precioc,preciov,preciovmin,"
                . " peso,incluir,nrocuenta,observacion,"
                . "id_tipo_impuesto,prod_servi,id_categoria,id_linea,"
                . "id_marca,id_unidmedida,activo,codigoalternativo,descripciondos,preciodos,preciotres,observaciondos,id_empresa,created_at) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", array(
            $producto->getTipo(),
            $producto->getCodigo(),
            $producto->getCodbarra(),
            $producto->getMarca(),
            $producto->getDescripcion(),
            $producto->getUnidmed(),
            $producto->getMoneda(),
            $producto->getPrecioc(),
            $producto->getPreciov(),
            $producto->getPreciovmin(),
//            $producto->getStock(),
            $producto->getPeso(),
            $producto->getIncluir(),
            $producto->getNrocuenta(),
            $producto->getObservacion(),
            $producto->getIdtipoimpuesto(),
            $producto->getProdservi(),
            $producto->getIdcategoria(),
            $producto->getIdlinea(),
            $producto->getIdmarca(),
            $producto->getIdunidmedida(),
            1,
            
            $producto->getCodigoalternativo(),
            $producto->getDescripciondos(),
            $producto->getPreciodos(),
            $producto->getPreciotres(),
            $producto->getObservaciondos(),
                    
            $_SESSION['idempresa'],
            date('Y-m-d H:i:s')
              
        ));
        $id = $data_source->lastinsertid();

        return $id;
    }
    function insertmultiple(array $producto) {

        $data_source = new DataSource();
        $filas = 0;
//        $cliente = new cliente();
//        $cliente = $objeto;
//        var_dump($producto);
        $filas = $data_source->insertmultiple("insert into producto(tipo, codigo,"
                . "codigobarra,marca,descripcion,unidmed,"
                . "moneda, precioc,preciov,preciovmin,"
                . " peso,incluir,nrocuenta,observacion,"
                . "id_tipo_impuesto,prod_servi,id_categoria,id_linea,"
                . "id_marca,id_unidmedida,activo,id_empresa) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", $producto
        );

        return $filas;
    }

    function update(producto $producto) {

        $data_source = new DataSource();
        $filas = 0;
//        $cliente = new cliente();
//        $cliente = $objeto;
        $filas = $data_source->ejecutarActualizacion("update producto set tipo=?, codigo=?,"
                . "codigobarra=?,marca = ?,descripcion=?,unidmed=?,"
                . "moneda=?, precioc=?,preciov=?,preciovmin=?,"
                . " peso=?,nrocuenta=?,observacion=?,"
                . "id_tipo_impuesto=?,id_categoria=?,id_linea=?,"
                . "id_marca=?,id_unidmedida=?,codigoalternativo=?,descripciondos=?, preciodos=?,preciotres=?,updated_at=?,observaciondos=? where id= ? and id_empresa=?;", array(
            $producto->getTipo(),
            $producto->getCodigo(),
            $producto->getCodbarra(),
            $producto->getMarca(),
            $producto->getDescripcion(),
            $producto->getUnidmed(),
            $producto->getMoneda(),
            $producto->getPrecioc(),
            $producto->getPreciov(),
            $producto->getPreciovmin(),
//            $producto->getStock(),
            $producto->getPeso(),
//            $producto->getIncluir(),
            $producto->getNrocuenta(),
            $producto->getObservacion(),
            $producto->getIdtipoimpuesto(),
            $producto->getIdcategoria(),
            $producto->getIdlinea(),
            $producto->getIdmarca(),
            $producto->getIdunidmedida(),
            $producto->getCodigoalternativo(),
            $producto->getDescripciondos(),
            $producto->getPreciodos(),
            $producto->getPreciotres(),
            date('Y-m-d H:i:s'),
            $producto->getObservaciondos(),
            $producto->getId(),
            $_SESSION['idempresa']
        ));

        return $filas;
    }
    
    function updateincluir($idprod,$incluir) {

        $data_source = new DataSource();
        $filas = 0;
//        $cliente = new cliente();
//        $cliente = $objeto;
        $filas = $data_source->ejecutarActualizacion("update producto set "
                . "incluir=?, updated_at=? where id= ? and id_empresa=?;", array(
            $incluir,
            date('Y-m-d H:i:s'),
            $idprod,
            $_SESSION['idempresa']
        ));

        return $filas;
    }

//    function updatestock(array $producto) {
//
//        $data_source = new DataSource();
////        $filas = 0;
////        $cliente = new cliente();
////        $cliente = $objeto;
//        $filas = $data_source->insertmultiple("update producto set "
//                . "stock= stock + ? where id= ? and prod_servi = 'producto' and incluir != 'Si';", $producto);
//
//        return $filas;
//    }
    
//    function updatestockporcoddescrip(array $producto) {
//
//        $data_source = new DataSource();
////        $filas = 0;
////        $cliente = new cliente();
////        $cliente = $objeto;
//        $filas = $data_source->insertmultiple("update producto set "
//                . "stock= stock + ? where STRCMP(codigo, ?)=0 and STRCMP(descripcion,?)=0 and prod_servi = 'producto' and incluir != 'Si' and id_sucursal=? ;", $producto);
//
//        return $filas;
//    }
    
//    function updatestockbycod(array $producto) {
//
//        $data_source = new DataSource();
////        $filas = 0;
////        $cliente = new cliente();
////        $cliente = $objeto;
//        $filas = $data_source->insertmultiple("update producto set "
//                . "stock= stock + ? where STRCMP(codigo, ?)=0 and prod_servi = 'producto' and incluir != 'Si' and id_sucursal=? ;", $producto);
//
//        return $filas;
//    }

    function delete($id) {

        $data_source = new DataSource();
        $filas = 0;
//        $cliente = new cliente();
//        $cliente = $objeto;
        $filas = $data_source->ejecutarActualizacion("update producto set activo=0 where id= ?;", array(
            $id
        ));

        return $filas;
    }

    function search($cadena) {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from producto where descripcion like concat('%',?,'%') and id_empresa= ? and activo = 1 order by id desc;", array($cadena,$_SESSION['idempresa']));

        $producto = null;
        $productos = array();
        foreach ($data_tabla as $clave => $valor) {
            $producto = new producto();
            $producto->setId($data_tabla[$clave]["id"]);
            $producto->setTipo($data_tabla[$clave]["tipo"]);
            $producto->setCodigo($data_tabla[$clave]["codigo"]);
            $producto->setCodbarra($data_tabla[$clave]["codigobarra"]);
            $producto->setMarca($data_tabla[$clave]["marca"]);
            $producto->setDescripcion($data_tabla[$clave]["descripcion"]);
            $producto->setUnidmed($data_tabla[$clave]["unidmed"]);
            $producto->setMoneda($data_tabla[$clave]["moneda"]);
            $producto->setPrecioc($data_tabla[$clave]["precioc"]);
            $producto->setPreciov($data_tabla[$clave]["preciov"]);
            $producto->setPreciovmin($data_tabla[$clave]["preciovmin"]);
//            $producto->setStock($data_tabla[$clave]["stock"]);
            $producto->setPeso($data_tabla[$clave]["peso"]);
            $producto->setIncluir($data_tabla[$clave]["incluir"]);
            $producto->setNrocuenta($data_tabla[$clave]["nrocuenta"]);
            $producto->setObservacion($data_tabla[$clave]["observacion"]);
            $producto->setIdtipoimpuesto($data_tabla[$clave]["id_tipo_impuesto"]);
            $producto->setProdservi($data_tabla[$clave]["prod_servi"]);
            $producto->setIdcategoria($data_tabla[$clave]["id_categoria"]);
            $producto->setIdlinea($data_tabla[$clave]["id_linea"]);
            $producto->setIdmarca($data_tabla[$clave]["id_marca"]);
            $producto->setIdunidmedida($data_tabla[$clave]["id_unidmedida"]);
            
            $producto->setCodigoalternativo($data_tabla[$clave]["codigoalternativo"]);
            $producto->setDescripciondos($data_tabla[$clave]["descripciondos"]);
            
            array_push($productos, $producto);
        }
        return $productos;
    }
    function searchadvanced($cadena,$cadenaalt,$detalle,$detalledos,$categoria, $linea,$marca, $filtro) {
        
        if(!empty($categoria)){
            $categoria = ' and id_categoria = '.$categoria;
            
        }
        if(!empty($linea)){
            $linea = ' and id_linea = '.$linea;
            
            
        }
        if(!empty($marca)){
            $marca = ' and id_marca = '.$marca;
        }
        if(!empty($filtro)){
            
            $filtro = " HAVING  GROUP_CONCAT(DISTINCT c.caracteristica) like CONCAT('%','".$filtro."','%')";
        }

        $data_source = new DataSource();
        

        
//        echo "SELECT
//	p.id,
//	p.codigo,
//	p.descripcion,
//	GROUP_CONCAT(DISTINCT s.serie)as serie,
//	m.descripcion AS marca,
//	l.descripcion AS linea,
//	cat.descripcion AS categoria,
//	GROUP_CONCAT(DISTINCT c.caracteristica) caracteristica,
//	(SELECT stock_actual from kardex where id_producto = p.id  order by id DESC limit 1) as stock,
//         p.moneda,
//         p.precioc,
//         p.preciov,
//         p.observacion,
//         codigoalternativo,
//         descripciondos
//         FROM
//	 producto AS p 
//        LEFT JOIN caracteristicas AS c ON c.id_producto = p.id
//        LEFT JOIN marca AS m ON m.id = p.id_marca
//        LEFT JOIN linea AS l ON l.id = p.id_linea
//        LEFT JOIN categoria AS cat ON cat.id = p.id_categoria
//        LEFT JOIN (select * from serie_producto where cantidad > 0) AS s 
//        ON s.id_producto = p.id WHERE concat(p.descripcion,p.codigo) like concat('%',?,'%') and concat(p.codigoalternativo,p.descripciondos) like concat('%',?,'%') 
//            and observacion like concat('%',?,'%') and p.id_empresa= ? and p.activo = 1 and 
//            prod_servi ='producto' ".$categoria.$linea.$marca.
//            " GROUP BY p.id ".$filtro. "  order by p.id DESC ;";

        $data_tabla = $data_source->ejecutarconsulta("SELECT
	p.id,
	p.codigo,
	p.descripcion,
	
	m.descripcion AS marca,
	l.descripcion AS linea,
	cat.descripcion AS categoria,
	GROUP_CONCAT(DISTINCT c.caracteristica) caracteristica,
	coalesce((SELECT stock_actual from kardex where id_producto = p.id and id_almacen=".$_SESSION['idalmacen']." and id_empresa=".$_SESSION['idempresa']." order by id DESC limit 1),0) as stock,
         p.moneda,
         p.precioc,
         p.preciov,
         p.observacion,
         codigoalternativo,
         descripciondos,
         observaciondos
         FROM
	 producto AS p 
        LEFT JOIN caracteristicas AS c ON c.id_producto = p.id
        LEFT JOIN marca AS m ON m.id = p.id_marca
        LEFT JOIN linea AS l ON l.id = p.id_linea
        LEFT JOIN categoria AS cat ON cat.id = p.id_categoria
        
        WHERE concat(p.descripcion,p.codigo) like concat('%',?,'%') and concat(p.codigoalternativo,p.descripciondos) like concat('%',?,'%') 
            and observacion like concat('%',?,'%') and observaciondos like concat('%',?,'%') and p.id_empresa= ? and p.activo = 1 and 
            prod_servi ='producto' ".$categoria.$linea.$marca.
            " GROUP BY p.id ".$filtro. "  order by p.id DESC ;", array($cadena,$cadenaalt,$detalle,$detalledos,$_SESSION['idempresa']));

//        $producto = null;
        $productos = array();
        foreach ($data_tabla as $clave => $valor) {
            $producto = new producto();
            $producto->setId($data_tabla[$clave]["id"]);
            $producto->setDescripcion($data_tabla[$clave]["descripcion"]);
            $producto->setCodigo($data_tabla[$clave]["codigo"]);
//            $producto->setSerie($data_tabla[$clave]["serie"]);
         
            $producto->setMarca($data_tabla[$clave]["marca"]);
            $producto->setLinea($data_tabla[$clave]["linea"]);
            $producto->setCategoria($data_tabla[$clave]["categoria"]);
            $producto->setCaracteristica($data_tabla[$clave]["caracteristica"]);
           
           
            $producto->setMoneda($data_tabla[$clave]["moneda"]);
            $producto->setPrecioc($data_tabla[$clave]["precioc"]);
            $producto->setPreciov($data_tabla[$clave]["preciov"]);
            
            $producto->setStock($data_tabla[$clave]["stock"]);
           
            $producto->setObservacion($data_tabla[$clave]["observacion"]);
            
            $producto->setCodigoalternativo($data_tabla[$clave]["codigoalternativo"]);
            $producto->setDescripciondos($data_tabla[$clave]["descripciondos"]);
            $producto->setObservaciondos($data_tabla[$clave]["observaciondos"]);
            array_push($productos, $producto);
        }
        return $productos;
    }
    
    function searchcodserie($cadena) {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select p.id,tipo,p.codigo,p.codigobarra,p.marca,p.descripcion,unidmed,
        moneda,precioc,preciov,preciovmin,peso,incluir,nrocuenta,observacion, id_tipo_impuesto,prod_servi,id_categoria,
        id_linea,id_marca,id_unidmedida  from producto as p  
        where  p.codigobarra = ?  and activo=1 and p.id_empresa= ?  order by p.id desc limit 1;", 
                array($cadena,$_SESSION['idempresa']));

        $producto = new producto();
        foreach ($data_tabla as $clave => $valor) {
            
            $producto->setId($data_tabla[$clave]["id"]);
            $producto->setTipo($data_tabla[$clave]["tipo"]);
            $producto->setCodigo($data_tabla[$clave]["codigo"]);
            $producto->setCodbarra($data_tabla[$clave]["codigobarra"]);
            $producto->setMarca($data_tabla[$clave]["marca"]);
            $producto->setDescripcion($data_tabla[$clave]["descripcion"]);
            $producto->setUnidmed($data_tabla[$clave]["unidmed"]);
            $producto->setMoneda($data_tabla[$clave]["moneda"]);
            $producto->setPrecioc($data_tabla[$clave]["precioc"]);
            $producto->setPreciov($data_tabla[$clave]["preciov"]);
            $producto->setPreciovmin($data_tabla[$clave]["preciovmin"]);
//            $producto->setStock($data_tabla[$clave]["stock"]);
            $producto->setPeso($data_tabla[$clave]["peso"]);
            $producto->setIncluir($data_tabla[$clave]["incluir"]);
            $producto->setNrocuenta($data_tabla[$clave]["nrocuenta"]);
            $producto->setObservacion($data_tabla[$clave]["observacion"]);
            $producto->setIdtipoimpuesto($data_tabla[$clave]["id_tipo_impuesto"]);
            $producto->setProdservi($data_tabla[$clave]["prod_servi"]);
            $producto->setIdcategoria($data_tabla[$clave]["id_categoria"]);
            $producto->setIdlinea($data_tabla[$clave]["id_linea"]);
            $producto->setIdmarca($data_tabla[$clave]["id_marca"]);
            $producto->setIdunidmedida($data_tabla[$clave]["id_unidmedida"]);
           
        }
        return $producto;
    }

    function selectstockgeneral($idalmacen, $marca, $linea, $categoria, $buscar, $moneda) {



        if (!empty($idalmacen)) {
            $idalmacen = 'and id_almacen= ' . $idalmacen;
        }

        if (!empty($marca)) {
            $marca = 'id_marca= "' . $marca . '" and ';
        }
        if (!empty($linea)) {
            $linea = 'id_linea= "' . $linea . '" and ';
        }

        if (!empty($categoria)) {
            $categoria = 'id_categoria= "' . $categoria . '" and ';
        }
        if (!empty($buscar)) {
            $buscar = ' concat(prod.codigo,codigobarra,prod.descripcion) like concat("%","' . $buscar . '","%") and ';
        }

        if (!empty($moneda)) {
            $moneda = ' moneda= "' . $moneda . '" ';
        }
       
//        echo 'select prod.codigo,prod.codigobarra,cat.descripcion as categoria,li.descripcion as linea,
//        ma.descripcion as marca,prod.descripcion,GROUP_CONCAT(DISTINCT s.serie) as serie,prod.moneda,
//        preciov,(SELECT stock_actual from kardex where id_producto = prod.id  order by id DESC limit 1) as stock,preciovmin,id_tipo_impuesto,id_categoria,id_linea,
//        id_marca,id_unidmedida,codigoalternativo,descripciondos,preciodos,preciotres
//        from 	producto AS prod LEFT JOIN kardex as k on k.id_producto = prod.id
//        LEFT JOIN categoria AS cat ON cat.id = prod.id_categoria
//        LEFT JOIN linea AS li ON li.id = prod.id_linea
//        LEFT JOIN marca AS ma ON ma.id = prod.id_marca
//        LEFT JOIN serie_producto AS s ON s.id_producto = prod.id
//        where ' . $idalmacen . $marca . $linea . $categoria . $buscar . '  prod_servi = "producto" GROUP BY prod.id order by prod.descripcion desc;';


        
        
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta('select prod.codigo,prod.codigobarra,cat.descripcion as categoria,li.descripcion as linea,
        ma.descripcion as marca,prod.descripcion,prod.moneda,
        preciov,coalesce((SELECT stock_actual from kardex where id_producto = prod.id '.$idalmacen.'  order by id DESC limit 1),0) as stock,preciovmin,id_tipo_impuesto,id_categoria,id_linea,
        id_marca,id_unidmedida,codigoalternativo,descripciondos,preciodos,preciotres
        from 	producto AS prod LEFT JOIN kardex as k on k.id_producto = prod.id
        LEFT JOIN categoria AS cat ON cat.id = prod.id_categoria
        LEFT JOIN linea AS li ON li.id = prod.id_linea
        LEFT JOIN marca AS ma ON ma.id = prod.id_marca
        
        where ' . $marca . $linea . $categoria . $buscar . '  prod_servi = "producto" and prod.id_empresa= ? GROUP BY prod.id order by prod.descripcion desc;',array($_SESSION['idempresa']));


        $general = array();
        foreach ($data_tabla as $clave => $valor) {
            $serie = '';
            if(!empty($data_tabla[$clave]["serie"])){
                $serie = ' Series: '.$data_tabla[$clave]["serie"];
            }
            
            $item = array(
                "codigo" => $data_tabla[$clave]["codigo"],
                "codigobarra" => $data_tabla[$clave]["codigobarra"],
                "categoria" => $data_tabla[$clave]["categoria"],
                "linea" => $data_tabla[$clave]["linea"],
                "marca" => $data_tabla[$clave]["marca"],
                "descripcion" => $data_tabla[$clave]["descripcion"].$serie,
                "moneda" => $data_tabla[$clave]["moneda"],
//                "sucursal" => $data_tabla[$clave]["nombre"],
                "stock" => $data_tabla[$clave]["stock"],
                "preciov" => $data_tabla[$clave]["preciov"],
                "preciovmin" => $data_tabla[$clave]["preciovmin"],
                "id_tipo_impuesto" => $data_tabla[$clave]["id_tipo_impuesto"],
                "id_categoria" => $data_tabla[$clave]["id_categoria"],
                "id_linea" => $data_tabla[$clave]["id_linea"],
                "id_marca" => $data_tabla[$clave]["id_marca"],
                "id_unidmedida" => $data_tabla[$clave]["id_unidmedida"],
                "descripciondos" => $data_tabla[$clave]["descripciondos"],
                "preciodos" => $data_tabla[$clave]["preciodos"],
                "preciotres" => $data_tabla[$clave]["preciotres"],
                "codigoalternativo" => $data_tabla[$clave]["codigoalternativo"]);


            array_push($general, $item);
        }
        return $general;
    }

    function selectstockvalorizado($idalmacen, $marca, $linea, $categoria, $buscar, $moneda) {

        

        if (!empty($idalmacen)) {
            $idalmacen = ' and id_almacen= ' . $idalmacen;
        }

        if (!empty($marca)) {
            $marca = 'id_marca= "' . $marca . '" and ';
        }
        if (!empty($linea)) {
            $linea = 'id_linea= "' . $linea . '" and ';
        }

        if (!empty($categoria)) {
            $categoria = 'id_categoria= "' . $categoria . '" and ';
        }
        if (!empty($buscar)) {
            $buscar = 'concat(prod.codigo,codigobarra,prod.descripcion) like concat("%","' . $buscar . '","%") and ';
        }

        if (!empty($moneda)) {
            $moneda = 'moneda= "' . $moneda . '" ';
        }


//echo 'SELECT
//	prod.codigo,
//	prod.codigobarra,
//	cat.descripcion AS categoria,
//	li.descripcion AS linea,
//	ma.descripcion AS marca,
//	prod.descripcion,
//	prod.moneda,
//
//	prod.preciov,
//	coalesce(prod.precioc,0) as precioc,
//
//	COALESCE((
//		SELECT
//			stock_actual
//		FROM
//			kardex
//		WHERE
//			id_producto = prod.id '.$idalmacen.
//		'ORDER BY
//			id DESC
//		LIMIT 1
//	),0) AS stock,
//	COALESCE((
//		SELECT
//			stock_actual
//		FROM
//			kardex
//		WHERE
//			id_producto = prod.id '.$idalmacen.
//		'ORDER BY
//			id DESC
//		LIMIT 1
//	) * precioc ,0) as precioctotal,
//	COALESCE((
//		SELECT
//			stock_actual
//		FROM
//			kardex
//		WHERE
//			id_producto = prod.id '.$idalmacen.
//		'ORDER BY
//			id DESC
//		LIMIT 1
//	) * preciov ,0) as preciovtotal
//
//	
//	FROM
//	producto AS prod
//        LEFT JOIN categoria AS cat ON cat.id = prod.id_categoria
//        LEFT JOIN linea AS li ON li.id = prod.id_linea
//        LEFT JOIN marca AS ma ON ma.id = prod.id_marca
//        where ' . $marca . $linea . $categoria . $buscar . $moneda . ' and prod_servi = "producto" order by prod.descripcion desc;';


        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta('SELECT
	prod.codigo,
	prod.codigobarra,
	cat.descripcion AS categoria,
	li.descripcion AS linea,
	ma.descripcion AS marca,
	prod.descripcion,
	prod.moneda,

	prod.preciov,
	coalesce(prod.precioc,0) as precioc,

	COALESCE((
		SELECT
			stock_actual
		FROM
			kardex
		WHERE
			id_producto = prod.id '.$idalmacen.
		' ORDER BY
			id DESC
		LIMIT 1
	),0) AS stock,
	COALESCE((
		SELECT
			stock_actual
		FROM
			kardex
		WHERE
			id_producto = prod.id '.$idalmacen.
		' ORDER BY
			id DESC
		LIMIT 1
	) * precioc ,0) as precioctotal,
	COALESCE((
		SELECT
			stock_actual
		FROM
			kardex
		WHERE
			id_producto = prod.id '.$idalmacen.
		' ORDER BY
			id DESC
		LIMIT 1
	) * preciov ,0) as preciovtotal

	
	FROM
	producto AS prod
        LEFT JOIN categoria AS cat ON cat.id = prod.id_categoria
        LEFT JOIN linea AS li ON li.id = prod.id_linea
        LEFT JOIN marca AS ma ON ma.id = prod.id_marca
        where ' . $marca . $linea . $categoria . $buscar . $moneda . ' and prod_servi = "producto" order by prod.descripcion desc;');


        $valorizado = array();
        foreach ($data_tabla as $clave => $valor) {
            $item = array(
                "codigo" => $data_tabla[$clave]["codigo"],
                "codigobarra" => $data_tabla[$clave]["codigobarra"],
                "categoria" => $data_tabla[$clave]["categoria"],
                "linea" => $data_tabla[$clave]["linea"],
                "marca" => $data_tabla[$clave]["marca"],
                "descripcion" => $data_tabla[$clave]["descripcion"],
                "moneda" => $data_tabla[$clave]["moneda"],
//                "sucursal" => $data_tabla[$clave]["nombre"],
                "stock" => $data_tabla[$clave]["stock"],
                "preciov" => $data_tabla[$clave]["preciov"],
                "precioc" => $data_tabla[$clave]["precioc"],
                "precioctotal" => $data_tabla[$clave]["precioctotal"],
                "preciovtotal" => $data_tabla[$clave]["preciovtotal"],
                "utilidad" => $data_tabla[$clave]["preciovtotal"] - $data_tabla[$clave]["precioctotal"]);


            array_push($valorizado, $item);
        }
        return $valorizado;
    }

    function selectstockcompraventa($idalmacen, $marca, $linea, $categoria, $buscar) {



        if (!empty($idalmacen)) {
            $idalmacen = ' k.id_almacen= ' . $idalmacen . ' and ';
        }

        if (!empty($marca)) {
            $marca = 'id_marca= ' . $marca . ' and ';
        }
        if (!empty($linea)) {
            $linea = 'id_linea= ' . $linea . ' and ';
        }

        if (!empty($categoria)) {
            $categoria = 'id_categoria= ' . $categoria . ' and ';
        }
        if (!empty($buscar)) {
            $buscar = 'concat(prod.codigo,codigobarra,prod.descripcion) like concat("%","' . $buscar . '","%") and ';
        }




//
//    echo   'select det.id_producto,det.codigoprod,cat.descripcion as categoria,li.descripcion as linea,ma.descripcion
//        as marca,det.descripcionprod,tipo_doc,SUM(case doc.tipo_doc when "Compra" then cantidad
//       else 0 end) as compra, sum(case doc.tipo_doc when "Venta" then cantidad else 0 end ) as venta,sucur.nombre
//
//       from detalle
//        as det INNER JOIN documento as doc on det.id_documento = doc.id left join producto as prod on prod.id = det.id_producto 
//       left JOIN categoria as cat on cat.id = prod.id_categoria left JOIN linea as li on li.id= prod.id_linea 
//       LEFT JOIN marca as ma on ma.id = prod.id_marca INNER JOIN sucursal as sucur on sucur.id = prod.id_sucursal 
//        where ' . $idsucur . $marca . $linea . $categoria . $buscar . ' tipo_doc = "Compra" or tipo_doc = "Venta" and prod_servi="producto"  GROUP BY descripcionprod ORDER BY descripcionprod;';


        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta('SELECT
	k.id_producto,
	prod.codigo,
	cat.descripcion AS categoria,
	li.descripcion AS linea,
	ma.descripcion AS marca,
	prod.descripcion,sum(case when k.concepto = "Venta" then k.cantidad else 0 end) as venta,
        sum(case when k.concepto = "Compra" then k.cantidad else 0 end) as compra,
        (select stock_actual	 from kardex where id_producto = prod.id ORDER BY id DESC LIMIT 1) stock_actual



        FROM
                kardex AS k
        LEFT JOIN producto AS prod ON prod.id = k.id_producto
        LEFT JOIN categoria AS cat ON cat.id = prod.id_categoria
        LEFT JOIN linea AS li ON li.id = prod.id_linea
        LEFT JOIN marca AS ma ON ma.id = prod.id_marca 
        where ' . $idalmacen . $marca . $linea . $categoria . $buscar . '  prod_servi="producto"  GROUP BY prod.id
        ORDER BY prod.descripcion ASC;');


        $cabecera = array();
        foreach ($data_tabla as $clave => $valor) {
            $item = array(
                "idprod" => $data_tabla[$clave]["id_producto"],
                "codigo" => $data_tabla[$clave]["codigo"],
                "categoria" => $data_tabla[$clave]["categoria"],
                "linea" => $data_tabla[$clave]["linea"],
                "marca" => $data_tabla[$clave]["marca"],
                "descripcion" => $data_tabla[$clave]["descripcion"],
//                "tipodoc" => $data_tabla[$clave]["tipo_doc"],
                "compra" => $data_tabla[$clave]["compra"],
                "venta" => $data_tabla[$clave]["venta"],
                "stock_actual" => $data_tabla[$clave]["stock_actual"]);
//                "sucursal" => $data_tabla[$clave]["nombre"]);


            array_push($cabecera, $item);
        }
        return $cabecera;
    }
    
    
     function duplicado($cadena,$codigo,$tipo,$idsucur){
        $data_source = new DataSource();
        $fila = 0;
        $data_tabla  = $data_source->ejecutarconsulta("select 1 from producto where (descripcion = ?"
                . " or codigo = ?)  and prod_servi = ? and activo = 1 and id_empresa = ?;", array($cadena,$codigo,$tipo,$_SESSION['idempresa']));
        foreach ($data_tabla as $clave => $valor) {
            $fila ++;
             
         }
        return $fila;
    
    }
     function duplicadobycodigo($codigo,$tipo,$idsucur){
        $data_source = new DataSource();
        $fila = 0;
        $data_tabla  = $data_source->ejecutarconsulta("select 1 from producto where "
                . "  codigo = ?  and prod_servi = ? and activo = 1 and id_empresa = ?;", array($codigo,$tipo,$_SESSION['idempresa']));
        foreach ($data_tabla as $clave => $valor) {
            $fila ++;
             
         }
        return $fila;
    
    }
    
    function generarcod(){
        $data_source = new DataSource();
        $fila = 0;
        $data_tabla  = $data_source->ejecutarconsulta("select id from producto order by id desc LIMIT 1;");
        $num = 0;
        foreach ($data_tabla as $clave => $valor) {
           $num= $data_tabla[$clave]["id"];
             
         }
         $cod = 'P'.str_pad((int)$num+1, 6, "0", STR_PAD_LEFT);
        return $cod;
        
    }
            
    function duplicadoedit($cadena,$codigo,$tipo, $id,$idsucur){
        $data_source = new DataSource();
        $fila = 0;
        $data_tabla  = $data_source->ejecutarconsulta("select 1 from producto where (descripcion = ?"
            . " or codigo = ?) and id != ? and prod_servi = ? and activo = 1 and id_empresa=?;", array($cadena,$codigo,$id,$tipo,$_SESSION['idempresa']));
        foreach ($data_tabla as $clave => $valor) {
            $fila ++;
        }
        return $fila;
        
        
    }
    
    function kardexfisico($desde, $hasta,  $idalmacen) {


//        echo 'select * from documento  where '.$fecha.$tipocomp.$buscar.$serie.$numero.$idsucur.' order by id desc;';


        $data_source = new DataSource();
       

        $data_tabla = $data_source->ejecutarconsulta('SELECT
	k.id_producto,
	prod.codigo,
	cat.descripcion AS categoria,
	li.descripcion AS linea,
	ma.descripcion AS marca,
	k.descripcion,
	k.cantidad,
	coalesce(k.stock_anterior,0) as stock_anterior,
	k.stock_actual,
	k.concepto,
	k.observacion,
        k.precio,
        k.moneda,
        k.created_at
        FROM
                kardex AS k
        LEFT JOIN producto AS prod ON prod.id = k.id_producto
        LEFT JOIN categoria AS cat ON cat.id = prod.id_categoria
        LEFT JOIN linea AS li ON li.id = prod.id_linea
        LEFT JOIN marca AS ma ON ma.id = prod.id_marca
        where k.created_at between ? and ? and  k.id_empresa=? and id_almacen= ? order by k.id DESC;', array($desde,$hasta,$_SESSION['idempresa'],$idalmacen));


        $fisico = array();
        foreach ($data_tabla as $clave => $valor) {
            $item = array(
                "concepto"=>$data_tabla[$clave]["concepto"],
               
                "created_at"=>$data_tabla[$clave]["created_at"],
                "observacion"=>$data_tabla[$clave]["observacion"],
                "descripcion"=>$data_tabla[$clave]["descripcion"],
                "cantidad"=>$data_tabla[$clave]["cantidad"],
                "stock_anterior"=>$data_tabla[$clave]["stock_anterior"],
                "stock_actual"=>$data_tabla[$clave]["stock_actual"],
                "precio"=>$data_tabla[$clave]["precio"],
                "moneda"=>$data_tabla[$clave]["moneda"]);

                array_push($fisico, $item); 
        }
        return $fisico;
    }



}
