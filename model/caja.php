<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class caja{
    
    private $id;
    private $fechaapertura;
    private $fechacierre;
    private $aperturasoles;
    private $aperturadolares;
    
    private $cierresoles;
    private $cierredolares;

    private $estado;
    private $observacion;
    private $idsucursal;
    private $idusuario;
    private $idempresa;
    
    function getId() {
        return $this->id;
    }
    
    function getIdempresa() {
        return $this->idempresa;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

    
    function getFechaapertura() {
        return $this->fechaapertura;
    }

    function getFechacierre() {
        return $this->fechacierre;
    }

    function getAperturasoles() {
        return $this->aperturasoles;
    }

    function getAperturadolares() {
        return $this->aperturadolares;
    }

    function getCierresoles() {
        return $this->cierresoles;
    }

    function getCierredolares() {
        return $this->cierredolares;
    }

    function getEstado() {
        return $this->estado;
    }

    function getObservacion() {
        return $this->observacion;
    }

    function getIdsucursal() {
        return $this->idsucursal;
    }

    function getIdusuario() {
        return $this->idusuario;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setFechaapertura($fechaapertura) {
        $this->fechaapertura = $fechaapertura;
    }

    function setFechacierre($fechacierre) {
        $this->fechacierre = $fechacierre;
    }

    function setAperturasoles($aperturasoles) {
        $this->aperturasoles = $aperturasoles;
    }

    function setAperturadolares($aperturadolares) {
        $this->aperturadolares = $aperturadolares;
    }

    function setCierresoles($cierresoles) {
        $this->cierresoles = $cierresoles;
    }

    function setCierredolares($cierredolares) {
        $this->cierredolares = $cierredolares;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    function setObservacion($observacion) {
        $this->observacion = $observacion;
    }

    function setIdsucursal($idsucursal) {
        $this->idsucursal = $idsucursal;
    }

    function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;
    }

            

    
    
    function select($fecha,$usuario,$idsucursal){
           
        if(!empty($usuario)){
            
            $usuario = ' and c.id='.$usuario;
        }   
        
        $data_source = new DataSource();
        
        $data_tabla = $data_source->ejecutarconsulta("SELECT
	c.id,
	s.nombre AS sucursal,
	c.fecha_apertura,
	CONCAT(
		u.nombre,
		' ',
		u.apellidoP,
		' ',
		u.apellidoM
	) AS usuario,
	CASE
        WHEN c.estado = 1 THEN
                'APERTURADA'
        ELSE
                'CERRADA'
        END AS estado,
       coalesce(c.cierre_soles, 0) as cierre_soles,
       coalesce(c.cierre_dolares,0) as cierre_dolares,

       COALESCE(SUM(CASE
                WHEN  d.tipo_venta = 'Contado'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN 
                d.total
                ELSE
                        0
                END),0)
         AS montosoles,
         COALESCE(SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        ),0) AS montodolares
       
        ,
			COALESCE(SUM(CASE
                WHEN  d.tipo_venta = 'Contado'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Compra' THEN 
                d.total
                ELSE
                        0
                END),0)
         AS montosolescompra,
         COALESCE(SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Compra' THEN
                        d.total
                ELSE
                        0
                END
        ),0) AS montodolarescompra
       
        ,
				
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Tarjeta'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        )   AS montodolarestarjeta
       
        ,
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Efectivo'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        )  AS montodolaresefectivo
       
        ,
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Efectivo'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montosolesefectivo
       
        ,
				
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Tarjeta'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        )  AS montosolestarjeta
       
        ,

				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Transferencia'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        )  AS montodolarestransferencia
       
        ,
				
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Transferencia'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        )  AS montosolestransferencia
       
        ,

        coalesce(( SELECT  SUM(
                CASE
                WHEN tipo = 'ingreso'
                AND moneda = 'Soles' THEN
                        monto
                ELSE
                        0
                END
        ) from gastos where id_caja = c.id),0) as ingresosoles
        ,

         coalesce((select SUM(
                CASE
                WHEN tipo = 'ingreso'
                AND moneda = 'Dolares' THEN
                        monto
                ELSE
                        0
                END 
        ) from gastos where id_caja = c.id),0) as ingresodolares
         ,

        coalesce((SELECT SUM(
                CASE
                WHEN tipo = 'egreso'
                AND moneda = 'Soles' THEN
                        monto
                ELSE
                        0
                END
        )	from gastos where id_caja = c.id),0 )as egresosoles
        ,

         COALESCE((SELECT SUM(
                CASE
                WHEN tipo = 'egreso'
                AND moneda = 'Dolares' THEN
                        monto
                ELSE
                        0
                END
        ) from gastos where id_caja = c.id),0 ) as egresodolares, 

        COALESCE(c.apertura_soles,0) AS apertura_soles,
        COALESCE(c.apertura_dolares,0) AS apertura_dolares
				


        FROM
                caja as c

        INNER JOIN usuario  u ON u.id = c.id_usuario
        INNER JOIN sucursal  s ON s.id = c.id_sucursal
        LEFT JOIN (select * from documento where (tipo_doc= 'Venta' or tipo_doc='Compra') and	(estadolocal != 'Anulado' OR ISNULL(estadolocal)) ) as  d ON d.id_caja = c.id
        
			LEFT JOIN (select * from gastos GROUP BY id_caja) AS g ON g.id_caja = c.id
        WHERE c.fecha_apertura ='".$fecha."' and c.id_sucursal = ".$idsucursal." ".$usuario." and (d.estadolocal != 'Anulado' or ISNULL(d.estadolocal)) and c.id_empresa=".$_SESSION['idempresa']." ;");

        
        $item = array();
        foreach ($data_tabla as $clave => $valor) {
             $item = array(
                "idcaja" => $data_tabla[$clave]["id"],
                "ventasoles" => $data_tabla[$clave]["montosoles"],
                "ventadolares" => $data_tabla[$clave]["montodolares"],
                "comprasoles" => $data_tabla[$clave]["montosolescompra"],
                "compradolares" => $data_tabla[$clave]["montodolarescompra"],
                "efectivosoles" => $data_tabla[$clave]["montosolesefectivo"],
                "efectivodolares" => $data_tabla[$clave]["montodolaresefectivo"],
                "tarjetadollar" => $data_tabla[$clave]["montodolarestarjeta"],
                "tarjetasoles" => $data_tabla[$clave]["montosolestarjeta"],
                "transferenciadollar" => $data_tabla[$clave]["montodolarestransferencia"],
                "transferenciasoles" => $data_tabla[$clave]["montosolestransferencia"],
                "estado" => $data_tabla[$clave]["estado"],
                "cierredolares" => $data_tabla[$clave]["cierre_dolares"],
                "cierresoles" => $data_tabla[$clave]["cierre_soles"],
                "aperturadolares" => $data_tabla[$clave]["apertura_dolares"],
                "aperturasoles" =>   $data_tabla[$clave]["apertura_soles"],
                 
                "ingresosoles" =>   $data_tabla[$clave]["ingresosoles"],
                "ingresodolares" =>   $data_tabla[$clave]["ingresodolares"],
                "egresosoles" =>   $data_tabla[$clave]["egresosoles"],
                "egresodolares" =>   $data_tabla[$clave]["egresodolares"]
             
             

            );         
        }
        return $item;
        
    }
    function selectreport($desde,$hasta,$usuario,$idsucursal){
           
       
        $fecha = '';
        if (!empty($desde) && !empty($hasta)) {
            $fecha = ' and fecha_apertura between "' . $desde . '" and "' . $hasta . '"  ';
        }
        $user = '';
        if(!empty($usuario)){
            $user = ' and c.id_usuario ='.$usuario;
            
        }
        
        $data_source = new DataSource();
        
        $data_tabla = $data_source->ejecutarconsulta("SELECT
	c.id,
	s.nombre AS sucursal,
	c.fecha_apertura,
	CONCAT(
		u.nombre,
		' ',
		u.apellidoP,
		' ',
		u.apellidoM
	) AS usuario,
	CASE
        WHEN c.estado = 1 THEN
                'APERTURADA'
        ELSE
                'CERRADA'
        END AS estado,
       coalesce(c.cierre_soles, 0) as cierre_soles,
       coalesce(c.cierre_dolares,0) as cierre_dolares,

       SUM(CASE
                WHEN  d.tipo_venta = 'Contado'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN 
                d.total
                ELSE
                        0
                END)
         AS montosoles,
         SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montodolares
       
        ,
			SUM(CASE
                WHEN  d.tipo_venta = 'Contado'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Compra' THEN 
                d.total
                ELSE
                        0
                END)
         AS montosolescompra,
         SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Compra' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montodolarescompra
       
        ,
				
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Tarjeta'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montodolarestarjeta
       
        ,
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Efectivo'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montodolaresefectivo
       
        ,
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Efectivo'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montosolesefectivo
       
        ,
				
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Tarjeta'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montosolestarjeta
       
        ,

				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Transferencia'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montodolarestransferencia
       
        ,
				
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Transferencia'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montosolestransferencia
       
        ,

        coalesce(( SELECT  SUM(
                CASE
                WHEN tipo = 'ingreso'
                AND moneda = 'Soles' THEN
                        monto
                ELSE
                        0
                END
        ) from gastos where id_caja = c.id),0) as ingresosoles
        ,

         coalesce((select SUM(
                CASE
                WHEN tipo = 'ingreso'
                AND moneda = 'Dolares' THEN
                        monto
                ELSE
                        0
                END 
        ) from gastos where id_caja = c.id),0) as ingresodolares
         ,

        coalesce((SELECT SUM(
                CASE
                WHEN tipo = 'egreso'
                AND moneda = 'Soles' THEN
                        monto
                ELSE
                        0
                END
        )	from gastos where id_caja = c.id),0 )as egresosoles
        ,

         COALESCE((SELECT SUM(
                CASE
                WHEN tipo = 'egreso'
                AND moneda = 'Dolares' THEN
                        monto
                ELSE
                        0
                END
        ) from gastos where id_caja = c.id),0 ) as egresodolares, 

        c.apertura_soles,
        c.apertura_dolares


        FROM
                caja as c

        INNER JOIN usuario  u ON u.id = c.id_usuario
        INNER JOIN sucursal  s ON s.id = c.id_sucursal
        LEFT JOIN (select * from documento where (tipo_doc= 'Venta' or tipo_doc='Compra') and	(estadolocal != 'Anulado' OR ISNULL(estadolocal))) as  d ON d.id_caja = c.id
        LEFT JOIN (select * from gastos GROUP BY id_caja) AS g ON g.id_caja = c.id

        WHERE
          c.id_sucursal=".$idsucursal.$user.$fecha." and c.id_empresa=".$_SESSION['idempresa']." GROUP BY c.id ORDER BY c.created_at;");

        
        $items = array();
        foreach ($data_tabla as $clave => $valor) {
             $item = array(
                "idcaja" => $data_tabla[$clave]["id"],
                "sucursal" => $data_tabla[$clave]["sucursal"],
                "fecha_apertura" => $data_tabla[$clave]["fecha_apertura"],
                "usuario" => $data_tabla[$clave]["usuario"],
                "estado" => $data_tabla[$clave]["estado"],
                "cierre_soles" => $data_tabla[$clave]["cierre_soles"],
                "cierre_dolares" => $data_tabla[$clave]["cierre_dolares"],
                "montosoles" => $data_tabla[$clave]["montosoles"],
                "montodolares" => $data_tabla[$clave]["montodolares"],
                "ingresosoles" => $data_tabla[$clave]["ingresosoles"],
                "ingresodolares" => $data_tabla[$clave]["ingresodolares"],
                "egresosoles" => $data_tabla[$clave]["egresosoles"],
                "egresodolares" => $data_tabla[$clave]["egresodolares"],
                "apertura_soles" => $data_tabla[$clave]["apertura_soles"],
                "apertura_dolares" => $data_tabla[$clave]["apertura_dolares"],
                "montodolaresefectivo" => $data_tabla[$clave]["montodolaresefectivo"],
                "montosolesefectivo" => $data_tabla[$clave]["montosolesefectivo"],
            );
            array_push($items, $item);
        }
        return $items;
        
    }
    function selectbyid($id){
           
   
        $data_source = new DataSource();
        
        $data_tabla = $data_source->ejecutarconsulta(
                "SELECT
	c.id,
	s.nombre AS sucursal,
	c.fecha_apertura,
	CONCAT(
		u.nombre,
		' ',
		u.apellidoP,
		' ',
		u.apellidoM
	) AS usuario,
	CASE
        WHEN c.estado = 1 THEN
                'APERTURADA'
        ELSE
                'CERRADA'
        END AS estado,
       coalesce(c.cierre_soles, 0) as cierre_soles,
       coalesce(c.cierre_dolares,0) as cierre_dolares,

       SUM(CASE
                WHEN  d.tipo_venta = 'Contado'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN 
                d.total
                ELSE
                        0
                END)
         AS montosoles,
         SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montodolares
       
        ,
			SUM(CASE
                WHEN  d.tipo_venta = 'Contado'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Compra' THEN 
                d.total
                ELSE
                        0
                END)
         AS montosolescompra,
         SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Compra' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montodolarescompra
       
        ,
				
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Tarjeta'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        )   AS montodolarestarjeta
       
        ,
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Efectivo'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        )  AS montodolaresefectivo
       
        ,
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Efectivo'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        ) AS montosolesefectivo
       
        ,
				
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Tarjeta'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        )  AS montosolestarjeta
       
        ,

				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Transferencia'
                AND d.moneda = 'Dolares' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        )  AS montodolarestransferencia
       
        ,
				
				SUM(
                CASE
                WHEN 
                 d.tipo_venta = 'Contado' and d.tipo_pago = 'Transferencia'
                AND d.moneda = 'Soles' and d.tipo_doc= 'Venta' THEN
                        d.total
                ELSE
                        0
                END
        )  AS montosolestransferencia
       
        ,

        coalesce(( SELECT  SUM(
                CASE
                WHEN tipo = 'ingreso'
                AND moneda = 'Soles' THEN
                        monto
                ELSE
                        0
                END
        ) from gastos where id_caja = c.id),0) as ingresosoles
        ,

         coalesce((select SUM(
                CASE
                WHEN tipo = 'ingreso'
                AND moneda = 'Dolares' THEN
                        monto
                ELSE
                        0
                END 
        ) from gastos where id_caja = c.id),0) as ingresodolares
         ,

        coalesce((SELECT SUM(
                CASE
                WHEN tipo = 'egreso'
                AND moneda = 'Soles' THEN
                        monto
                ELSE
                        0
                END
        )	from gastos where id_caja = c.id),0 )as egresosoles
        ,

         COALESCE((SELECT SUM(
                CASE
                WHEN tipo = 'egreso'
                AND moneda = 'Dolares' THEN
                        monto
                ELSE
                        0
                END
        ) from gastos where id_caja = c.id),0 ) as egresodolares, 

        c.apertura_soles,
        c.apertura_dolares
				


        FROM
                caja as c

        INNER JOIN usuario  u ON u.id = c.id_usuario
        INNER JOIN sucursal  s ON s.id = c.id_sucursal
        LEFT JOIN (select * from documento where (tipo_doc= 'Venta' or tipo_doc='Compra') and	(estadolocal != 'Anulado' OR ISNULL(estadolocal)) ) as  d ON d.id_caja = c.id
        
			LEFT JOIN (select * from gastos GROUP BY id_caja) AS g ON g.id_caja = c.id
        WHERE c.id = ? and c.id_empresa=? GROUP BY c.id;", array($id,$_SESSION['idempresa']));

        $item = array();
      
        foreach ($data_tabla as $clave => $valor) {
             $item = array(
                "idcaja" => $data_tabla[$clave]["id"],
                "sucursal" => $data_tabla[$clave]["sucursal"],
                "fecha_apertura" => $data_tabla[$clave]["fecha_apertura"],
                "usuario" => $data_tabla[$clave]["usuario"],
                "estado" => $data_tabla[$clave]["estado"],
                "cierre_soles" => $data_tabla[$clave]["cierre_soles"],
                "cierre_dolares" => $data_tabla[$clave]["cierre_dolares"],
                "montosoles" => $data_tabla[$clave]["montosoles"],
                "montodolares" => $data_tabla[$clave]["montodolares"],
                 ////////////
                 "montosolescompra" => $data_tabla[$clave]["montosolescompra"],
                 "montodolarescompra" => $data_tabla[$clave]["montodolarescompra"],
                 "montosolestarjeta" => $data_tabla[$clave]["montosolestarjeta"],
                 "montodolarestarjeta" => $data_tabla[$clave]["montodolarestarjeta"],
                 "monrodolarestransferencia" => $data_tabla[$clave]["montodolarestransferencia"],
                 "montosolestransferencia" => $data_tabla[$clave]["montosolestransferencia"],
                 
                 
                 //////////////////////////
                "ingresosoles" => $data_tabla[$clave]["ingresosoles"],
                "ingresodolares" => $data_tabla[$clave]["ingresodolares"],
                "egresosoles" => $data_tabla[$clave]["egresosoles"],
                "egresodolares" => $data_tabla[$clave]["egresodolares"],
                "apertura_soles" => $data_tabla[$clave]["apertura_soles"],
                "apertura_dolares" => $data_tabla[$clave]["apertura_dolares"],
                 
                "montodolaresefectivo" => $data_tabla[$clave]["montodolaresefectivo"],
                "montosolesefectivo" => $data_tabla[$clave]["montosolesefectivo"],
            );
           
        }
        return $item;
        
    }
    
       function apertura(caja $caja) {

        $data_source = new DataSource();
       
        
        
        $filas = $data_source->ejecutarActualizacion("insert into caja (fecha_apertura,apertura_dolares,apertura_soles,"
                . "estado,id_sucursal,id_usuario,created_at,id_empresa) values(?,?,?,?,?,?,?,?)",
                array(date('Y-m-d'),
                    $caja->getAperturadolares(),
                    $caja->getAperturasoles(),
                    true,
                    $_SESSION['idsucursal'],
                    $_SESSION['id'],
                    date('Y-m-d h:m:s'),
                    $_SESSION['idempresa']
                    ));
       
        return $filas;
    }
       function cerrar(caja $caja) {

        $data_source = new DataSource();
      
        $filas = $data_source->ejecutarActualizacion("update caja set fecha_cierre=?,cierre_dolares=?,cierre_soles=?,"
                . "estado=0,updated_at=? where id=? and id_empresa=?",array(date('Y-m-d'),$caja->getCierredolares(),$caja->getCierresoles(), date('Y-m-d H:i:s'),
                    $caja->getId(), $_SESSION['idempresa']));
       
        return $filas;
    }
    
       function updateapertura(caja $caja) {

        $data_source = new DataSource();
      
        $filas = $data_source->ejecutarActualizacion("update caja set apertura_soles=?,apertura_dolares=?,"
                . "updated_at=? where id=? and id_empresa=?",array($caja->getAperturasoles(),$caja->getAperturadolares(), date('Y-m-d H:i'),
                    $caja->getId(), $_SESSION['idempresa']));
       
        return $filas;
    }
    
    function statusbyid($id){
        $data_source = new DataSource();
        $data_tabla = $data_source->ejecutarconsulta("SELECT estado from caja where id=? and id_empresa = ?;"
                ,array($id,$_SESSION['idempresa']));
        
       $estado = false;
        foreach ($data_tabla as $clave => $valor) {
            
            $estado = $data_tabla[$clave]["estado"];

        }
       
        return $estado;
    }
    
    function status($fecha){
        $data_source = new DataSource();
        $data_tabla = $data_source->ejecutarconsulta("SELECT estado from caja where fecha_apertura=? and id_sucursal=? and id_empresa=? ORDER BY id DESC  LIMIT 1;",array($fecha,$_SESSION['idsucursal'],$_SESSION['idempresa']));
       
        $estado = false;
        foreach ($data_tabla as $clave => $valor) {
            
            $estado = $data_tabla[$clave]["estado"];

        }
        return $estado;
    }
    
    
    function responsable($fecha){
        $data_source = new DataSource();
        $data_tabla = $data_source->ejecutarconsulta("select c.id ,LPAD(c.id,6,'0')as codigo,u.id as idusuario,u.nombre,c.created_at "
                . " from caja as c inner JOIN usuario as u on c.id_usuario=u.id where fecha_apertura = ? and c.id_sucursal=? and c.id_empresa=?;",array($fecha,$_SESSION['idsucursal'],$_SESSION['idempresa']));
       
        $responsables = array();
        foreach ($data_tabla as $clave => $valor) {
            
            
            $responsable = array(
              'nombre'=>  $data_tabla[$clave]["nombre"],
               'id'=> $data_tabla[$clave]["id"],
               'codigo'=> $data_tabla[$clave]["codigo"],
               'idusuario'=> $data_tabla[$clave]["idusuario"],
               'create'=> $data_tabla[$clave]["created_at"],
                );
            array_push($responsables, $responsable);

        }
        return $responsables;
        
        
    }
    
    function ultimo(){
         $data_source = new DataSource();
        $data_tabla = $data_source->ejecutarconsulta("SELECT * from caja where id_sucursal=? and id_empresa=? ORDER BY id DESC  LIMIT 1;", array($_SESSION['idsucursal'],$_SESSION['idempresa']));
       
        $caja = new caja();
        foreach ($data_tabla as $clave => $valor) {
            $caja->setId($data_tabla[$clave]["id"]);
            $caja->setFechaapertura($data_tabla[$clave]["fecha_apertura"]);
            $caja->setFechacierre($data_tabla[$clave]["fecha_cierre"]);
            $caja->setCierredolares($data_tabla[$clave]["cierre_dolares"]);
            $caja->setCierresoles($data_tabla[$clave]["cierre_soles"]);
            $caja->setAperturasoles($data_tabla[$clave]["apertura_soles"]);
            $caja->setAperturadolares($data_tabla[$clave]["apertura_dolares"]);
            $caja->setEstado($data_tabla[$clave]["estado"]);
            $caja->setObservacion($data_tabla[$clave]["observacion"]);
            $caja->setIdsucursal($data_tabla[$clave]["id_sucursal"]);
            $caja->setIdusuario($data_tabla[$clave]["id_usuario"]);
                      
        }
        return $caja;
        
        
    }
    
    
    
    


    
    
    
}

