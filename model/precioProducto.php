<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of precioProducto
 *
 * @author HERNAN
 */
class precioProducto {
    //put your code here
    private $id;
    private $idproducto;
    private $precio;
    private $cantidad;
    
    
    function __construct() {
        
    }
    
    function getId() {
        return $this->id;
    }

    function getIdproducto() {
        return $this->idproducto;
    }

    function getPrecio() {
        return $this->precio;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIdproducto($idproducto) {
        $this->idproducto = $idproducto;
    }

    function setPrecio($precio) {
        $this->precio = $precio;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }
    
    function insert(array $precios) {
          
//        var_dump($detalles);
        $data_source = new DataSource();
        $filas = array();

        $filas = $data_source->insertmultiple("insert into precio_producto (id_producto,precio ,cantidad) values(?,?,?)", $precios
        );
        
        
        return $filas;
    }
    
    function selectAll($id){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from precio_producto where id_producto = ? order by cantidad ASC;",array($id));

   
        $precios = array();
        foreach ($data_tabla as $clave => $valor) {
            $precio = new precioProducto();
            $precio->setId($data_tabla[$clave]["id"]);
            $precio->setIdproducto($data_tabla[$clave]["id_producto"]);
            $precio->setPrecio($data_tabla[$clave]["precio"]);
            $precio->setCantidad($data_tabla[$clave]["cantidad"]);
    
        
            array_push($precios, $precio);
        }
        return $precios;
        
    }
    function selectPrecio($id,$cantidad){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select precio from precio_producto where id_producto = ? and cantidad <= ?
        ORDER BY cantidad DESC LIMIT 1",array($id,$cantidad));

   
        $precio = null;
        foreach ($data_tabla as $clave => $valor) {
            
            $precio= $data_tabla[$clave]["precio"];

        }
        if($precio == null){
            
            $data_tabla = $data_source->ejecutarconsulta("select preciov from producto where id = ? ",array($id));
            foreach ($data_tabla as $clave => $valor) {
            
            $precio= $data_tabla[$clave]["preciov"];

            }
            
            
        }
        
        return $precio;
        
    }
    
    function delete($id){
        $data_source = new DataSource();

        $fila = $data_source->ejecutarActualizacion("delete from precio_producto where id_producto = ?;",array($id));

   
       
        return $fila;
        
    }
    



}
