<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of medida
 *
 * @author HERNAN
 */
class unidmedida {

    //put your code here
    private $id;
    private $codigo;
    private $descripcion;
    private $orden;
    private $idempresa;

    function __construct() {
        
    }
    function getIdempresa() {
        return $this->idempresa;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

    
    function getId() {
        return $this->id;
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getOrden() {
        return $this->orden;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setOrden($orden) {
        $this->orden = $orden;
    }

    function selectAll() {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from unidmedida where  id_empresa=? order by id desc;",array($_SESSION['idempresa']));

        $medida = null;
        $medidas = array();
        foreach ($data_tabla as $clave => $valor) {
            $medida = new unidmedida();
            $medida->setId($data_tabla[$clave]["id"]);
            $medida->setDescripcion($data_tabla[$clave]["descripcion"]);
            $medida->setCodigo($data_tabla[$clave]["codigo"]);
            $medida->setOrden($data_tabla[$clave]["orden"]);


            array_push($medidas, $medida);
        }
        return $medidas;
    }

    function selectOne($id) {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from unidmedida where id =? and id_empresa=?", array($id,$_SESSION['idempresa']));

        $medida = new unidmedida();
        foreach ($data_tabla as $clave => $valor) {

            $medida->setId($data_tabla[$clave]["id"]);
            $medida->setDescripcion($data_tabla[$clave]["descripcion"]);
            $medida->setCodigo($data_tabla[$clave]["codigo"]);
            $medida->setOrden($data_tabla[$clave]["orden"]);
        }
        return $medida;
    }

    function insert(unidmedida $unid) {
        $data_source = new DataSource();
        $filas = 0;

        $filas = $data_source->ejecutarActualizacion("insert into unidmedida( descripcion, codigo, orden,id_empresa) values(?,?,?,?)", array(
            $unid->getDescripcion(),
            $unid->getCodigo(),
            $unid->getOrden(),
            $_SESSION['idempresa']
        ));
        return $filas;
    }

    function update(unidmedida $unid) {
        $data_source = new DataSource();
        $filas = 0;

        $filas = $data_source->ejecutarActualizacion("update unidmedida set descripcion=?, codigo=?, orden=? where id=? and id_empresa=?", array(
            $unid->getDescripcion(),
            $unid->getCodigo(),
            $unid->getOrden(),
            $unid->getId(),
            $_SESSION['idempresa']
        ));
        return $filas;
    }
    
    function duplicado($cadena){
        $data_source = new DataSource();
        $fila = 0;
        $data_tabla  = $data_source->ejecutarconsulta("select 1 from unidmedida where descripcion = ? and id_empresa=?"
    . "  ;", array($cadena,$_SESSION['idempresa']));
        foreach ($data_tabla as $clave => $valor) {
            $fila ++;
             
         }
        return $fila;
        
        
    }
    function duplicadoedit($cadena, $id){
        $data_source = new DataSource();
        $fila = 0;
        $data_tabla  = $data_source->ejecutarconsulta("select 1 from unidmedida where descripcion = ?"
            . "  and id != ? and id_empresa=?;", array($cadena,$id,$_SESSION['idempresa']));
        foreach ($data_tabla as $clave => $valor) {
            $fila ++;
        }
        return $fila;
        
        
    }

}
