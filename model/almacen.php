<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Almacen
 *
 * @author HERNAN
 */
class Almacen {
    //put your code here
    private $id;
    private $direccion;
    private $cuidad;
    private $nombre;
    private $idempresa;
            
    
    function __construct() {
        
    }
    
    function getIdempresa() {
        return $this->idempresa;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

    

        
    function getId() {
        return $this->id;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getCuidad() {
        return $this->cuidad;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setCuidad($cuidad) {
        $this->cuidad = $cuidad;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
      function insert(Almacen $almacen) {

        $data_source = new DataSource();
        
        
        $data_source->ejecutarActualizacion("insert into almacen (direccion,ciudad,nombre,id_empresa,created_at,activo) values(?,?,?,?,?,?)",
                array($almacen->getDireccion(),
                    $almacen->getCuidad(),
                    $almacen->getNombre(),
                    $almacen->getIdempresa(),
                    date('Y-m-d H:i:s'),
                    TRUE));
        
        return $data_source->lastinsertid();
       
       
    }
      function update(Almacen $almacen) {

        $data_source = new DataSource();
        
        
        $fila = $data_source->ejecutarActualizacion("update almacen set nombre = ?, direccion=? where id = ? ",
                array(                 
                    $almacen->getNombre(),
                    $almacen->getDireccion(),    
                    $almacen->getId()));
        
        return $fila;
       
       
    }
    
    function selectAll(){
        
    $data_source = new DataSource();

    $data_tabla = $data_source->ejecutarconsulta("select * from almacen where activo=1 and id_empresa=?;" ,array($_SESSION['idempresa']));

        
        $almacenes= array();
        foreach ($data_tabla as $clave => $valor) {
            $almacen = new almacen();
            $almacen->setId($data_tabla[$clave]["id"]);
            $almacen->setDireccion($data_tabla[$clave]["direccion"]);
            $almacen->setCuidad($data_tabla[$clave]["ciudad"]);
            $almacen->setNombre($data_tabla[$clave]["nombre"]);
            $almacen->setIdempresa($data_tabla[$clave]["id_empresa"]);

            array_push($almacenes, $almacen);
        }
        return $almacenes;
        
    }
    function selectOne($id){
        
    $data_source = new DataSource();

    $data_tabla = $data_source->ejecutarconsulta("select * from almacen where activo=1 and id_empresa=? and id =? ;" ,array($_SESSION['idempresa'],$id));

        
        $almacen = new almacen();
        foreach ($data_tabla as $clave => $valor) {
            
            $almacen->setId($data_tabla[$clave]["id"]);
            $almacen->setDireccion($data_tabla[$clave]["direccion"]);
            $almacen->setCuidad($data_tabla[$clave]["ciudad"]);
            $almacen->setNombre($data_tabla[$clave]["nombre"]);
            $almacen->setIdempresa($data_tabla[$clave]["id_empresa"]);

           
        }
        return $almacen;
        
    }



    
}
