<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gastos
 *
 * @author HERNAN
 */
class gastos {
    //put your code here
    private $id;
    private $fecha;
    private $idusuario;
    private $entrega;
    private $monto;
    private $concepto;
    private $nrocontable;
    private $moneda;
    private $tipopago;
    private $entidad;
    private $nop;
    private $idsucursal;
    private $tipo;
    private $idcaja;
    private $detalle;
    private $formapago;
    private $idempresa;
   
    
    
    function __construct() {
        
    }
    
    function getIdempresa() {
        return $this->idempresa;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

        
    function getDetalle() {
        return $this->detalle;
    }

    function getFormapago() {
        return $this->formapago;
    }

    function setDetalle($detalle) {
        $this->detalle = $detalle;
    }

    function setFormapago($formapago) {
        $this->formapago = $formapago;
    }

        
    
    function getIdcaja() {
        return $this->idcaja;
    }

    function setIdcaja($idcaja) {
        $this->idcaja = $idcaja;
    }

        function getTipo() {
        return $this->tipo;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

        function getIdsucursal() {
        return $this->idsucursal;
    }

    function setIdsucursal($idsucursal) {
        $this->idsucursal = $idsucursal;
    }

        function getId() {
        return $this->id;
    }

    function getFecha() {
        return $this->fecha;
    }

    function getIdusuario() {
        return $this->idusuario;
    }

    function getEntrega() {
        return $this->entrega;
    }

    function getMonto() {
        return $this->monto;
    }

    function getConcepto() {
        return $this->concepto;
    }

    function getNrocontable() {
        return $this->nrocontable;
    }

    function getMoneda() {
        return $this->moneda;
    }

    function getTipopago() {
        return $this->tipopago;
    }

    function getEntidad() {
        return $this->entidad;
    }

    function getNop() {
        return $this->nop;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;
    }

    function setEntrega($entrega) {
        $this->entrega = $entrega;
    }

    function setMonto($monto) {
        $this->monto = $monto;
    }

    function setConcepto($concepto) {
        $this->concepto = $concepto;
    }

    function setNrocontable($nrocontable) {
        $this->nrocontable = $nrocontable;
    }

    function setMoneda($moneda) {
        $this->moneda = $moneda;
    }

    function setTipopago($tipopago) {
        $this->tipopago = $tipopago;
    }

    function setEntidad($entidad) {
        $this->entidad = $entidad;
    }

    function setNop($nop) {
        $this->nop = $nop;
    }
    
    function selectAll($id) {
        $data_source = new DataSource();
        
        $data_tabla = $data_source->ejecutarconsulta("select * from gastos where id_sucursal = ? and id_empresa= = ?", array($id,$_SESSION['idempresa']));
        $gastos = array();
        
        foreach ($data_tabla as $clave => $valor) {
            $gasto = new gastos();
            $gasto->setId($data_tabla[$clave]["id"]);
            $gasto->setFecha($data_tabla[$clave]["fecha"]);
            $gasto->setIdusuario($data_tabla[$clave]["id_usuario"]);
            $gasto->setEntrega($data_tabla[$clave]["entrega"]);
            $gasto->setMonto($data_tabla[$clave]["monto"]);
            $gasto->setConcepto($data_tabla[$clave]["concepto"]);
            $gasto->setNrocontable($data_tabla[$clave]["nrocontable"]);
            $gasto->setMoneda($data_tabla[$clave]["moneda"]);
            $gasto->setTipopago($data_tabla[$clave]["tipopago"]);
            $gasto->setEntidad($data_tabla[$clave]["entidad"]);
            $gasto->setNop($data_tabla[$clave]["nop"]);
            $gasto->setIdsucursal($data_tabla[$clave]["id_sucursal"]);
            $gasto->setTipo($data_tabla[$clave]["tipo"]);
            array_push($gastos, $gasto);
      
            
        }
        return $gastos;
    }
    function selectbycaja($id,$moneda,$tipo) {
        $data_source = new DataSource();
        
        $data_tabla = $data_source->ejecutarconsulta("select * from gastos where id_caja = ? and moneda=? and tipo = ? and id_empresa=?", array($id,$moneda,$tipo,$_SESSION['idempresa']));
        $gastos = array();
        
        foreach ($data_tabla as $clave => $valor) {
            $gasto = new gastos();
            $gasto->setId($data_tabla[$clave]["id"]);
            $gasto->setFecha($data_tabla[$clave]["fecha"]);
            $gasto->setIdusuario($data_tabla[$clave]["id_usuario"]);
            $gasto->setEntrega($data_tabla[$clave]["entrega"]);
            $gasto->setMonto($data_tabla[$clave]["monto"]);
            $gasto->setConcepto($data_tabla[$clave]["concepto"]);
            $gasto->setNrocontable($data_tabla[$clave]["nrocontable"]);
            $gasto->setMoneda($data_tabla[$clave]["moneda"]);
            $gasto->setTipopago($data_tabla[$clave]["tipopago"]);
            $gasto->setEntidad($data_tabla[$clave]["entidad"]);
            $gasto->setNop($data_tabla[$clave]["nop"]);
            $gasto->setIdsucursal($data_tabla[$clave]["id_sucursal"]);
            $gasto->setTipo($data_tabla[$clave]["tipo"]);
            $gasto->setDetalle($data_tabla[$clave]["detalle"]);
            $gasto->setFormapago($data_tabla[$clave]["formapago"]);
            array_push($gastos, $gasto);
      
            
        }
        return $gastos;
    }
    
    function selectOne($id) {
             
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from gastos where id = ? and id_empresa=?",array($id,$_SESSION['idempresa']));

       $gasto = new gastos();
        foreach ($data_tabla as $clave => $valor) {
            
            $gasto->setId($data_tabla[$clave]["id"]);
            $gasto->setFecha($data_tabla[$clave]["fecha"]);
            $gasto->setIdusuario($data_tabla[$clave]["id_usuario"]);
            $gasto->setEntrega($data_tabla[$clave]["entrega"]);
            $gasto->setMonto($data_tabla[$clave]["monto"]);
            $gasto->setConcepto($data_tabla[$clave]["concepto"]);
            $gasto->setNrocontable($data_tabla[$clave]["nrocontable"]);
            $gasto->setMoneda($data_tabla[$clave]["moneda"]);
            $gasto->setTipopago($data_tabla[$clave]["tipopago"]);
            $gasto->setEntidad($data_tabla[$clave]["entidad"]);
            $gasto->setNop($data_tabla[$clave]["nop"]);
            $gasto->setIdsucursal($data_tabla[$clave]["id_sucursal"]);
            $gasto->setTipo($data_tabla[$clave]["tipo"]);
      
            
        }
        return $gasto;
    }
    function eliminar($id) {
             
        $data_source = new DataSource();

        $fila = $data_tabla = $data_source->ejecutarActualizacion("delete from gastos where id = ?",array($id));


        return $fila;
    }
    
    
    
    
    function insert(gastos $gasto) {
        $data_source = new DataSource();
        $filas = 0;

        $filas = $data_source->ejecutarActualizacion("insert into gastos(fecha,  entrega,monto,concepto,nrocontable,"
                . "moneda,tipopago,entidad,nop,id_sucursal,tipo,id_caja,detalle,formapago,id_empresa,created_at) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", array(
            date('Y-m-d h:m:s'),
//            $gasto->getIdusuario(),
            $gasto->getEntrega(),
            $gasto->getMonto(),
            $gasto->getConcepto(),
            $gasto->getNrocontable(),
            $gasto->getMoneda(),
            $gasto->getTipopago(),
            $gasto->getEntidad(),
            $gasto->getNop(),
            $gasto->getIdsucursal(),
            $gasto->getTipo(),
            $gasto->getIdcaja(),
            $gasto->getDetalle(),
            $gasto->getFormapago(),
            $_SESSION['idempresa'],
            date('Y-m-d H:i:s')
            
        ));
        return $filas;
    }
    function update(gastos $gasto) {
        $data_source = new DataSource();
        $filas = 0;

        $filas = $data_source->ejecutarActualizacion("update gastos set  concepto=?,detalle=? , formapago=? ,tipopago=?,nop=?,monto=?, updated_at=?"
                . " where id = ? and id_empresa=?", array(
            $gasto->getConcepto(),
//            $gasto->getIdusuario(),
            $gasto->getDetalle(),
            $gasto->getFormapago(),
            $gasto->getTipopago(),
            $gasto->getNop(),
            $gasto->getMonto(),
           
            $gasto->getId(),
            $_SESSION['idempresa']
        ));
        return $filas;
    }
    
    function search($desde, $hasta, $buscar, $idsucur) {

        
        $fecha = '';
        if (!empty($desde) && !empty($hasta)) {
            $fecha = 'fecha between "' . $desde . '" and "' . $hasta . '" and ';
        }
 
        if (!empty($buscar)) {
            $buscar = 'concat(entrega,concepto) like concat("%","' . $buscar . '","%") and ';
        }
        
    
    
        if (!empty($idsucur)) {
            $idsucur = 'id_sucursal= ' . $idsucur;
        }


//        echo 'select * from documento  where '.$fecha.$tipocomp.$buscar.$serie.$numero.$idsucur.' order by id desc;';
//        echo 'SELECT doc.id,concat(serie," - ",numero) as documento,ruc,razonsocial,fechaemision,fechavencimiento,
//        concat(DATEDIFF(fechaemision , fechavencimiento)," Días") as dias,moneda,
//        SUM(case when incigv = 1 then (precio*cantidad)-(montobasegratuito+montobaseivap) ELSE
//        ((precio*cantidad)-(montobasegratuito+montobaseivap))+(((precio*cantidad) - (montobaseexpo+montobaseexonarado+montobaseinafecto)) - 
//        ((precio*cantidad) - (montobaseexpo+montobaseexonarado+montobaseinafecto))/1.18) END) as total,doc.tipo_venta
//                 from documento as doc INNER JOIN detalle as det on det.id_documento =
//    doc.id where tipo_doc = "Venta" and tipo_venta="Credito" ' . $fecha . $tipo . $buscar  . $idsucur . ' GROUP BY serie,numero order by fechavencimiento desc;';

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta('select * from gastos WHERE ' . $fecha . $buscar  . $idsucur . ' and id_empresa='.$_SESSION['idempresa'].'  order by fecha desc;');


        $pagos = array();
        foreach ($data_tabla as $clave => $valor) {
            $detalle = array(
                "id"=>$data_tabla[$clave]["id"],
                "fecha"=>$data_tabla[$clave]["fecha"],
                "idusuario"=>$data_tabla[$clave]["id_usuario"],
                "entrega"=>$data_tabla[$clave]["entrega"],
                "monto"=>$data_tabla[$clave]["monto"],
                "concepto"=>$data_tabla[$clave]["concepto"],
                "nrocontable"=>$data_tabla[$clave]["nrocontable"],
                "moneda"=>$data_tabla[$clave]["moneda"],
               
                "tipopago"=>$data_tabla[$clave]["tipopago"],
                "entidad"=>$data_tabla[$clave]["entidad"],
                "nop"=>$data_tabla[$clave]["nop"],
                "idsucursal"=>$data_tabla[$clave]["id_sucursal"]);
              
            
                array_push($pagos, $detalle); 
        }
        return $pagos;
    }
    
     function resumen($idcaja){
        
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select COALESCE(SUM(case when moneda= 'Soles' and tipo ='ingreso' then monto else 0 end),0) as ingresosoles, COALESCE(SUM(case when moneda= 'Dolares' and tipo ='ingreso' then monto else 0 end),0) as ingresodolares,
	COALESCE(SUM(case when moneda= 'Soles' and tipo ='egreso' then monto else 0 end),0) as egresosoles, COALESCE(SUM(case when moneda= 'Dolares' and tipo ='egreso' then monto else 0 end),0) as egresodolares
     from gastos where id_caja=? and id_empresa=?;",array($idcaja,$_SESSION['idempresa']));


        $item = array();
        foreach ($data_tabla as $clave => $valor) {
            
            $item = array(
                "ingresosoles" => $data_tabla[$clave]["ingresosoles"],
                "ingresodolares" => $data_tabla[$clave]["ingresodolares"],
                "egresosoles" => $data_tabla[$clave]["egresosoles"],
                "egresodolares" => $data_tabla[$clave]["egresodolares"]
            );            
                     
        }
        
        return $item;
        
    }
    
    
    



    
}
