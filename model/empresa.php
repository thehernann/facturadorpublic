<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Empresa
 *
 * @author HERNAN
 */
class Empresa {
    //put your code here
    
    private $id;
    private $nombrecomercial;
    private $razonsocial;
    private $direccion;
    private $gerente;
    private $duenio;
    private $telefono;
    private $activo;
    private $ruc;
    private $logo;
    private $ciudad;
    private  $email;
    private $estado;
    function __construct() {
        
    }
    
    function getEmail() {
        return $this->email;
    }

    function getEstado() {
        return $this->estado;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

        
    function getId() {
        return $this->id;
    }

    function getNombrecomercial() {
        return $this->nombrecomercial;
    }

    function getRazonsocial() {
        return $this->razonsocial;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getGerente() {
        return $this->gerente;
    }

    function getDuenio() {
        return $this->duenio;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getActivo() {
        return $this->activo;
    }

    function getRuc() {
        return $this->ruc;
    }

    function getLogo() {
        return $this->logo;
    }

    function getCiudad() {
        return $this->ciudad;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombrecomercial($nombrecomercial) {
        $this->nombrecomercial = $nombrecomercial;
    }

    function setRazonsocial($razonsocial) {
        $this->razonsocial = $razonsocial;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setGerente($gerente) {
        $this->gerente = $gerente;
    }

    function setDuenio($duenio) {
        $this->duenio = $duenio;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setActivo($activo) {
        $this->activo = $activo;
    }

    function setRuc($ruc) {
        $this->ruc = $ruc;
    }

    function setLogo($logo) {
        $this->logo = $logo;
    }

    function setCiudad($ciudad) {
        $this->ciudad = $ciudad;
    }
    
    function insert(Empresa $empresa) {

        $data_source = new DataSource();
        $filas = 0;
        
        $filas = $data_source->ejecutarActualizacion("insert into empresa "
                . "(nombre_comercial,razon_social,direccion,gerente,duenio,telefono,activo,ruc,logo,ciudad,created_at)"
                . " values(?,?,?,?,?,?,?,?,?,?,?)",
                array($empresa->getNombrecomercial(),
                    $empresa->getRazonsocial(),
                    $empresa->getDireccion(),
                    $empresa->getGerente(),
                    $empresa->getDuenio(),
                    $empresa->getTelefono(),
                    $empresa->getActivo(),
                    $empresa->getRuc(),
                    $empresa->getLogo(),
                    $empresa->getCiudad(),
                    date('Y-m-d H:i:s'))
                   );
       
        return $filas;
    }
    
          function update(Empresa $empresa) {

        $data_source = new DataSource();
        $filas = 0;
        
        $filas = $data_source->ejecutarActualizacion("update empresa "
                . "set nombre_comercial=?,razon_social=?,direccion=?,gerente=?,duenio=?,telefono=?,"
                . "activo=?,ruc=?,logo=?,ciudad=?,updated_at=?,email=? where id = ? ",
                array($empresa->getNombrecomercial(),
                    $empresa->getRazonsocial(),
                    $empresa->getDireccion(),
                    $empresa->getGerente(),
                    $empresa->getDuenio(),
                    $empresa->getTelefono(),
                    TRUE,
                    $empresa->getRuc(),
                    $empresa->getLogo(),
                    $empresa->getCiudad(),
                    date('Y-m-d H:i:s'),
                    $empresa->getEmail(),
                    $empresa->getId())
                    
                );
       
        return $filas;
    }
    
        function selectAll(){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from empresa order by id desc;");

   
        $empresas = array();
        foreach ($data_tabla as $clave => $valor) {
            $empresa = new Empresa();
            $empresa->setId($data_tabla[$clave]["id"]);
            $empresa->setNombrecomercial($data_tabla[$clave]["nombre_comercial"]);
            $empresa->setRazonsocial($data_tabla[$clave]["razon_social"]);
            $empresa->setDireccion($data_tabla[$clave]["direccion"]);
            $empresa->setGerente($data_tabla[$clave]["gerente"]);
            $empresa->setDuenio($data_tabla[$clave]["duenio"]);
            $empresa->setTelefono($data_tabla[$clave]["telefono"]);
            $empresa->setActivo($data_tabla[$clave]["activo"]);
            $empresa->setRuc($data_tabla[$clave]["ruc"]);
            $empresa->setLogo($data_tabla[$clave]["logo"]);
            $empresa->setCiudad($data_tabla[$clave]["ciudad"]);
            $empresa->setEmail($data_tabla[$clave]["email"]);
            $empresa->setEstado($data_tabla[$clave]["estado"]);
    
        
            array_push($empresas, $empresa);
        }
        return $empresas;
        
    }
    
      function selectOne($id){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from empresa where id= ? ",array($id));

   
        $empresa = new empresa();
        foreach ($data_tabla as $clave => $valor) {
            
            $empresa->setId($data_tabla[$clave]["id"]);
            $empresa->setNombrecomercial($data_tabla[$clave]["nombre_comercial"]);
            $empresa->setRazonsocial($data_tabla[$clave]["razon_social"]);
            $empresa->setDireccion($data_tabla[$clave]["direccion"]);
            $empresa->setGerente($data_tabla[$clave]["gerente"]);
            $empresa->setDuenio($data_tabla[$clave]["duenio"]);
            $empresa->setTelefono($data_tabla[$clave]["telefono"]);
            $empresa->setActivo($data_tabla[$clave]["activo"]);
            $empresa->setRuc($data_tabla[$clave]["ruc"]);
            $empresa->setLogo($data_tabla[$clave]["logo"]);
            $empresa->setCiudad($data_tabla[$clave]["ciudad"]);
            $empresa->setEmail($data_tabla[$clave]["email"]);
    
        
            
        }
        return $empresa;
        
    }
    
    



}
