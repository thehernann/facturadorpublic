<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of detalleNivel
 *
 * @author HERNAN
 */
class detalleNivel {
    //put your code her
    private $id;
    private $op;
    private $idnivel;
    private $descripcion;
    
    function __construct() {
       
        
    }
    function getId() {
        return $this->id;
    }

    function getOp() {
        return $this->op;
    }

    function getIdnivel() {
        return $this->idnivel;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setOp($op) {
        $this->op = $op;
    }

    function setIdnivel($idnivel) {
        $this->idnivel = $idnivel;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

 function selectAll($id){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("SELECT * from detalle_nivel where id_nivel = ?;",array($id));

        
        $niveles= array();
        foreach ($data_tabla as $clave => $valor) {
            $nivel = new detalleNivel();
            $nivel->setId($data_tabla[$clave]["id"]);
            $nivel->setDescripcion($data_tabla[$clave]["descripcion"]);
            $nivel->setOp($data_tabla[$clave]["op"]);
//            $nivel->setOp($data_tabla[$clave]["id_nivel"]);
      

            array_push($niveles, $nivel);
        }
        return $niveles;
        
    }
    
    function insert(array $det){
        
        $data_source = new DataSource();
        $ids = array();
        $ids = $data_source->insertmultiple("insert into detalle_nivel (id_nivel,descripcion,op) values(?,?,?)",
                $det);
        
        
        return $ids;
    }
    
   function delete($id){
        
        $data_source = new DataSource();
        
        $fila = $data_source->ejecutarActualizacion("delete from detalle_nivel where id_nivel = ?",
                array($id));
        
        
        return $fila;
    }

    
    
}
