<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of marcaprod
 *
 * @author HERNAN
 */
class marcaprod {

    //put your code here
    private $id;
    private $descripcion;
    private $codigo;
    private $orden;
    private $idempresa;
       

    function __construct() {
        
    }
    
    function getIdempresa() {
        return $this->idempresa;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

    
    function getId() {
        return $this->id;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getOrden() {
        return $this->orden;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setOrden($orden) {
        $this->orden = $orden;
    }

    function selectAll() {

        $data_source = new DataSource();

    $data_tabla = $data_source->ejecutarconsulta("select * from marca where id_empresa= ? order by id desc;",array($_SESSION['idempresa']));

        $marca = null;
        $marcas = array();
        foreach ($data_tabla as $clave => $valor) {
            $marca = new marcaprod();
            $marca->setId($data_tabla[$clave]["id"]);
            $marca->setDescripcion($data_tabla[$clave]["descripcion"]);
            $marca->setCodigo($data_tabla[$clave]["codigo"]);
            $marca->setOrden($data_tabla[$clave]["orden"]);


            array_push($marcas, $marca);
        }
        return $marcas;
    }
    
     function selectOne($id) {
         
         

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from marca where id_empresa=? and  id =?;",array($_SESSION['idempresa'],$id));
        $marca = new marcaprod();
        foreach ($data_tabla as $clave => $valor) {
            
            $marca->setId($data_tabla[$clave]["id"]);
            $marca->setDescripcion($data_tabla[$clave]["descripcion"]);
            $marca->setCodigo($data_tabla[$clave]["codigo"]);
            $marca->setOrden($data_tabla[$clave]["orden"]);


            
        }
        return $marca;
    }

    function insert(marcaprod $marca) {
        $data_source = new DataSource();
        $filas = 0;

        $filas = $data_source->ejecutarActualizacion("insert into marca( descripcion, codigo, orden,id_empresa,created_at) values(?,?,?,?,?)", array(
            $marca->getDescripcion(),
            $marca->getCodigo(),
            $marca->getOrden(),
            $_SESSION['idempresa'],
            date('Y-m-d H:i:s')
        ));
        return $filas;
    }
    
      function update(marcaprod $marca) {
        $data_source = new DataSource();
        $filas = 0;

        $filas = $data_source->ejecutarActualizacion("update marca set descripcion=?, codigo=?, orden=?, updated_at=?  where id = ? and id_empresa=?;", array(
            
            $marca->getDescripcion(),
            $marca->getCodigo(),
            $marca->getOrden(),
            date('Y-m-d H:i:s'),
            $marca->getId(),
            $_SESSION['idempresa']
        ));
        return $filas;
    }
    
     function duplicado($cadena){
        $data_source = new DataSource();
        $fila = 0;
        $data_tabla  = $data_source->ejecutarconsulta("select 1 from marca where descripcion = ? and id_empresa= ?"
                . "  ;", array($cadena, $_SESSION['idempresa']));
        foreach ($data_tabla as $clave => $valor) {
            $fila ++;
             
         }
        return $fila;
        
        
    }
    function duplicadoedit($cadena, $id){
        $data_source = new DataSource();
        $fila = 0;
        $data_tabla  = $data_source->ejecutarconsulta("select 1 from marca where descripcion = ?"
            . "  and id != ? and id_empresa=? ;", array($cadena,$id,$_SESSION['idempresa']));
        foreach ($data_tabla as $clave => $valor) {
            $fila ++;
        }
        return $fila;
        
        
    }

}
