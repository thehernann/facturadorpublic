<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pagoMixto
 *
 * @author HERNAN
 */
class pagoMixto {
    //put your code here
    private $id;
    private $tipopago;
    private $monto;
    private $tarjeta;
    private $numeroop;
    private $iddocumento;
    
    function __construct() {
        
    }
    
    function getId() {
        return $this->id;
    }

    function getTipopago() {
        return $this->tipopago;
    }

    function getMonto() {
        return $this->monto;
    }

    function getTarjeta() {
        return $this->tarjeta;
    }

    function getNumeroop() {
        return $this->numeroop;
    }

    function getIddocumento() {
        return $this->iddocumento;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTipopago($tipopago) {
        $this->tipopago = $tipopago;
    }

    function setMonto($monto) {
        $this->monto = $monto;
    }

    function setTarjeta($tarjeta) {
        $this->tarjeta = $tarjeta;
    }

    function setNumeroop($numeroop) {
        $this->numeroop = $numeroop;
    }

    function setIddocumento($iddocumento) {
        $this->iddocumento = $iddocumento;
    }
    
    
    function insert(array $det){
        
        $data_source = new DataSource();
        $ids = array();
        $ids = $data_source->insertmultiple("insert into pagomixto (tipopago,monto,tarjeta,numeroop,id_documento) values(?,?,?,?,?)",
                $det);
        
        
        return $ids;
    }
    
        function selectbycaja($id){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("SELECT
	coalesce(sum(
		CASE
		WHEN pm.tipopago = 'Transferencia'
		AND d.moneda = 'Soles' THEN
			monto
		ELSE
			0
		END
	),0) AS mixtotransferenciasoles,
	coalesce(sum(
		CASE
		WHEN pm.tipopago = 'Transferencia'
		AND d.moneda = 'Dolares' THEN
			monto
		ELSE
			0
		END
	),0) AS mixtotransferenciadolares,
	coalesce(sum(
		CASE
		WHEN pm.tipopago = 'Efectivo'
		AND d.moneda = 'Soles' THEN
			monto
		ELSE
			0
		END
	),0) AS mixtoefectivosoles,
	coalesce(sum(
		CASE
		WHEN pm.tipopago = 'Efectivo'
		AND d.moneda = 'Dolares' THEN
			monto
		ELSE
			0
		END
	),0) AS mixtoefectivodolares,
	coalesce(sum(
		CASE
		WHEN pm.tipopago = 'Tarjeta'
		AND d.moneda = 'Soles' THEN
			monto
		ELSE
			0
		END
	),0) AS mixtotarjetasoles,
	coalesce(sum(
		CASE
		WHEN pm.tipopago = 'Tarjeta'
		AND d.moneda = 'Dolares' THEN
			monto
		ELSE
			0
		END
	),0) AS mixtotarjetadolares
        FROM
                pagomixto AS pm
        INNER JOIN documento AS d ON d.id = pm.id_documento where d.id_caja = ?",
                array($id));

   
        $item = array();
        foreach ($data_tabla as $clave => $valor) {
            $item = array(
                "mixtotransferenciasoles" => $data_tabla[$clave]["mixtotransferenciasoles"],
                "mixtotransferenciadolares" => $data_tabla[$clave]["mixtotransferenciadolares"],
                "mixtoefectivosoles" => $data_tabla[$clave]["mixtoefectivosoles"],
                "mixtoefectivodolares" => $data_tabla[$clave]["mixtoefectivodolares"],
                "mixtotarjetasoles" => $data_tabla[$clave]["mixtotarjetasoles"],
                "mixtotarjetadolares" => $data_tabla[$clave]["mixtotarjetadolares"],
                
            );
        
            
        }
        return $item;
        
    }
    
    



    
}
