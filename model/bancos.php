<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of bancos
 *
 * @author HERNAN
 */
class bancos {
    //put your code here
    private $id;
    private $nombre;
    private $idempresa;
    
    
    function __construct() {
        
    }
    
    function getIdempresa() {
        return $this->idempresa;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

        
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
      function selectAll(){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from bancos where id_empresa = ? order by id desc;",
                array($_SESSION['idempresa']));

   
        $bancos = array();
        foreach ($data_tabla as $clave => $valor) {
            $banco = new bancos();
            $banco->setId($data_tabla[$clave]["id"]);
            $banco->setNombre($data_tabla[$clave]["nombre"]);
    
        
            array_push($bancos, $banco);
        }
        return $bancos;
        
    }
    
        function selectOne($id){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from bancos where id= ? and id_empresa=? ",array($id,$_SESSION['idempresa']));

   
        $banco = new bancos();
        foreach ($data_tabla as $clave => $valor) {
            
            $banco->setId($data_tabla[$clave]["id"]);
            $banco->setNombre($data_tabla[$clave]["nombre"]);
    
        
            
        }
        return $banco;
        
    }
    
    function insert(bancos $banco) {

        $data_source = new DataSource();
        $filas = 0;
        
        $filas = $data_source->ejecutarActualizacion("insert into bancos (nombre,id_empresa,created_at) values(?,?,?)",
                array($banco->getNombre(),$_SESSION['idempresa'],date('Y-m-d H:i:s')));
       
        return $filas;
    }
    
    
     function update(bancos $banco) {

        $data_source = new DataSource();
        $filas = 0;
        
        $filas = $data_source->ejecutarActualizacion("update bancos set nombre=?, updated_at=? where id= ? and id_empresa=?",
                array($banco->getNombre(),date('Y-m-d H:i:s'), $banco->getId(),$_SESSION['idempresa']));
       
        return $filas;
    }
    
     function duplicado($cadena){
        $data_source = new DataSource();
        $fila = 0;
        $data_tabla  = $data_source->ejecutarconsulta("select 1 from bancos where nombre = ?"
                . " and id_empresa = ? ;", array($cadena,$_SESSION['idempresa']));
        foreach ($data_tabla as $clave => $valor) {
            $fila ++;
             
         }
        return $fila;
        
        
    }
    function duplicadoedit($cadena, $id){
        $data_source = new DataSource();
        $fila = 0;
        $data_tabla  = $data_source->ejecutarconsulta("select 1 from bancos where nombre = ?"
                . "  and id != ? and id_empresa=?;", array($cadena,$id,$_SESSION['idempresa']));
        foreach ($data_tabla as $clave => $valor) {
            $fila ++;
        }
        return $fila;
        
        
    }



}
