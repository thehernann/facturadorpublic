<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of documento
 *
 * @author HERNAN
 */
class documento {

    //put your code here
    private $id;
    private $serie;
    private $numero;
    private $fechaemision;
    private $fechavencimiento;
    private $norden;
    private $moneda;
    private $incigv;
    private $idsunattransaction;
    private $idusuario;
    private $tipoventa;
    private $tipoventamov;
    private $tipoventaop;
    private $sujetoa;
    private $idpersona;
    private $personatipodoc;
    private $ruc;
    private $razonsocial;
    private $direccion;
    private $email;
    private $telfijo;
    private $estadosunat;
    private $estadolocal;
    private $total;
    private $tipo;
    private $idsucursal;
    private $observacion;
    private $tipodoc;
    private $tipocambio;
    private $docref;
    private $serieref;
    private $numeroref;
    private $idtiponota;
    private $tiponotaop;
    private $garantia;
    private $condicionpago;
    private $validezdias;
    private $plazoentregadias;
    private $condicionpagodias;
    private $atencion;
    private $tipopago;
    private $nrooptipopago;
    private $motivoanulacion;
    
    private $montogratuita;
    private $montoexonerado;
    private $montoinafecto;
    private $montoexportacion;
    private $montogravada;
    private $montoigv;
    
    private $rucdestinatario;
    private $razonsialdestinatario;
    private $direcciondestinatario;
    private $motivoguia;
    private $ructransportista;
    private $razonsocialtransportista;
    private $costotransportista;
    private $marcaplaca;
    private $licenciatransportista;
    private $certificdotransportista;
   
    private $tarjeta;
    
    private $idcaja;
    private $idempresa;
    
    private $idalmacendestino;
    private $idalmacenemisor;

    function __construct() {
        
    }
    
    function getIdalmacendestino() {
        return $this->idalmacendestino;
    }

    function getIdalmacenemisor() {
        return $this->idalmacenemisor;
    }

    function setIdalmacendestino($idalmacendestino) {
        $this->idalmacendestino = $idalmacendestino;
    }

    function setIdalmacenemisor($idalmacenemisor) {
        $this->idalmacenemisor = $idalmacenemisor;
    }

        
    
    function getIdempresa() {
        return $this->idempresa;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

        function getTelfijo() {
        return $this->telfijo;
    }

    function setTelfijo($telfijo) {
        $this->telfijo = $telfijo;
    }

        
    
    function getIdcaja() {
        return $this->idcaja;
    }

    function setIdcaja($idcaja) {
        $this->idcaja = $idcaja;
    }

        function getTarjeta() {
        return $this->tarjeta;
    }

    function setTarjeta($tarjeta) {
        $this->tarjeta = $tarjeta;
    }

        
    function getTiponotaop() {
        return $this->tiponotaop;
    }

    function setTiponotaop($tiponotaop) {
        $this->tiponotaop = $tiponotaop;
    }

        function getPersonatipodoc() {
        return $this->personatipodoc;
    }

    function setPersonatipodoc($personatipodoc) {
        $this->personatipodoc = $personatipodoc;
    }

    function getMotivoguia() {
        return $this->motivoguia;
    }

    function setMotivoguia($motivoguia) {
        $this->motivoguia = $motivoguia;
    }

        function getRucdestinatario() {
        return $this->rucdestinatario;
    }

    function getRazonsialdestinatario() {
        return $this->razonsialdestinatario;
    }

    function getDirecciondestinatario() {
        return $this->direcciondestinatario;
    }

   
    function getRuctransportista() {
        return $this->ructransportista;
    }

    function getRazonsocialtransportista() {
        return $this->razonsocialtransportista;
    }

    function getCostotransportista() {
        return $this->costotransportista;
    }

    function getMarcaplaca() {
        return $this->marcaplaca;
    }

    function getLicenciatransportista() {
        return $this->licenciatransportista;
    }

    function getCertificdotransportista() {
        return $this->certificdotransportista;
    }

    function setRucdestinatario($rucdestinatario) {
        $this->rucdestinatario = $rucdestinatario;
    }

    function setRazonsialdestinatario($razonsialdestinatario) {
        $this->razonsialdestinatario = $razonsialdestinatario;
    }

    function setDirecciondestinatario($direcciondestinatario) {
        $this->direcciondestinatario = $direcciondestinatario;
    }

   

    function setRuctransportista($ructransportista) {
        $this->ructransportista = $ructransportista;
    }

    function setRazonsocialtransportista($razonsocialtransportista) {
        $this->razonsocialtransportista = $razonsocialtransportista;
    }

    function setCostotransportista($costotransportista) {
        $this->costotransportista = $costotransportista;
    }

    function setMarcaplaca($marcaplaca) {
        $this->marcaplaca = $marcaplaca;
    }

    function setLicenciatransportista($licenciatransportista) {
        $this->licenciatransportista = $licenciatransportista;
    }

    function setCertificdotransportista($certificdotransportista) {
        $this->certificdotransportista = $certificdotransportista;
    }

        function getMontogratuita() {
        return $this->montogratuita;
    }

    function getMontoexonerado() {
        return $this->montoexonerado;
    }

    function getMontoinafecto() {
        return $this->montoinafecto;
    }

    function getMontoexportacion() {
        return $this->montoexportacion;
    }

    function getMontogravada() {
        return $this->montogravada;
    }

    function getMontoigv() {
        return $this->montoigv;
    }

    function setMontogratuita($montogratuita) {
        $this->montogratuita = $montogratuita;
    }

    function setMontoexonerado($montoexonerado) {
        $this->montoexonerado = $montoexonerado;
    }

    function setMontoinafecto($montoinafecto) {
        $this->montoinafecto = $montoinafecto;
    }

    function setMontoexportacion($montoexportacion) {
        $this->montoexportacion = $montoexportacion;
    }

    function setMontogravada($montogravada) {
        $this->montogravada = $montogravada;
    }

    function setMontoigv($montoigv) {
        $this->montoigv = $montoigv;
    }

    
    function getMotivoanulacion() {
        return $this->motivoanulacion;
    }

    function setMotivoanulacion($motivoanulacion) {
        $this->motivoanulacion = $motivoanulacion;
    }

        function getTipopago() {
        return $this->tipopago;
    }

    function getNrooptipopago() {
        return $this->nrooptipopago;
    }

    function setTipopago($tipopago) {
        $this->tipopago = $tipopago;
    }

    function setNrooptipopago($nrooptipopago) {
        $this->nrooptipopago = $nrooptipopago;
    }

    function getCondicionpago() {
        return $this->condicionpago;
    }

    function getValidezdias() {
        return $this->validezdias;
    }

    function getPlazoentregadias() {
        return $this->plazoentregadias;
    }

    function getCondicionpagodias() {
        return $this->condicionpagodias;
    }

    function getAtencion() {
        return $this->atencion;
    }

    function setCondicionpago($condicionpago) {
        $this->condicionpago = $condicionpago;
    }

    function setValidezdias($validezdias) {
        $this->validezdias = $validezdias;
    }

    function setPlazoentregadias($plazoentregadias) {
        $this->plazoentregadias = $plazoentregadias;
    }

    function setCondicionpagodias($condicionpagodias) {
        $this->condicionpagodias = $condicionpagodias;
    }

    function setAtencion($atencion) {
        $this->atencion = $atencion;
    }

    function getGarantia() {
        return $this->garantia;
    }

    function setGarantia($garantia) {
        $this->garantia = $garantia;
    }

    function getDocref() {
        return $this->docref;
    }

    function getSerieref() {
        return $this->serieref;
    }

    function getNumeroref() {
        return $this->numeroref;
    }

    function getIdtiponota() {
        return $this->idtiponota;
    }

    function setDocref($docref) {
        $this->docref = $docref;
    }

    function setSerieref($serieref) {
        $this->serieref = $serieref;
    }

    function setNumeroref($numeroref) {
        $this->numeroref = $numeroref;
    }

    function setIdtiponota($idtiponota) {
        $this->idtiponota = $idtiponota;
    }

    function getTipocambio() {
        return $this->tipocambio;
    }

    function setTipocambio($tipocambio) {
        $this->tipocambio = $tipocambio;
    }

    function getTipodoc() {
        return $this->tipodoc;
    }

    function setTipodoc($tipodoc) {
        $this->tipodoc = $tipodoc;
    }

    function getObservacion() {
        return $this->observacion;
    }

    function setObservacion($observacion) {
        $this->observacion = $observacion;
    }

    function getId() {
        return $this->id;
    }

    function getSerie() {
        return $this->serie;
    }

    function getNumero() {
        return $this->numero;
    }

    function getFechaemision() {
        return $this->fechaemision;
    }

    function getFechavencimiento() {
        return $this->fechavencimiento;
    }

    function getNorden() {
        return $this->norden;
    }

    function getMoneda() {
        return $this->moneda;
    }

    function getIncigv() {
        return $this->incigv;
    }

    function getIdsunattransaction() {
        return $this->idsunattransaction;
    }

    function getIdusuario() {
        return $this->idusuario;
    }

    function getTipoventa() {
        return $this->tipoventa;
    }

    function getTipoventamov() {
        return $this->tipoventamov;
    }

    function getTipoventaop() {
        return $this->tipoventaop;
    }

    function getSujetoa() {
        return $this->sujetoa;
    }

    function getIdpersona() {
        return $this->idpersona;
    }

    function getRuc() {
        return $this->ruc;
    }

    function getRazonsocial() {
        return $this->razonsocial;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getEmail() {
        return $this->email;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setSerie($serie) {
        $this->serie = $serie;
    }

    function setNumero($numero) {
        $this->numero = $numero;
    }

    function setFechaemision($fechaemision) {
        $this->fechaemision = $fechaemision;
    }

    function setFechavencimiento($fechavencimiento) {
        $this->fechavencimiento = $fechavencimiento;
    }

    function setNorden($norden) {
        $this->norden = $norden;
    }

    function setMoneda($moneda) {
        $this->moneda = $moneda;
    }

    function setIncigv($incigv) {
        $this->incigv = $incigv;
    }

    function setIdsunattransaction($idsunattransaction) {
        $this->idsunattransaction = $idsunattransaction;
    }

    function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;
    }

    function setTipoventa($tipoventa) {
        $this->tipoventa = $tipoventa;
    }

    function setTipoventamov($tipoventamov) {
        $this->tipoventamov = $tipoventamov;
    }

    function setTipoventaop($tipoventaop) {
        $this->tipoventaop = $tipoventaop;
    }

    function setSujetoa($sujetoa) {
        $this->sujetoa = $sujetoa;
    }

    function setIdpersona($idpersona) {
        $this->idpersona = $idpersona;
    }

    function setRuc($ruc) {
        $this->ruc = $ruc;
    }

    function setRazonsocial($razonsocial) {
        $this->razonsocial = $razonsocial;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function getEstadosunat() {
        return $this->estadosunat;
    }

    function getEstadolocal() {
        return $this->estadolocal;
    }

    function getTotal() {
        return $this->total;
    }

    function setEstadosunat($estadosunat) {
        $this->estadosunat = $estadosunat;
    }

    function setEstadolocal($estadolocal) {
        $this->estadolocal = $estadolocal;
    }

    function setTotal($total) {
        $this->total = $total;
    }

    function getTipo() {
        return $this->tipo;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function getIdsucursal() {
        return $this->idsucursal;
    }

    function setIdsucursal($idsucursal) {
        $this->idsucursal = $idsucursal;
    }

    function selectAll() {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from documento where id_empresa = ? order by id desc;", array($_SESSION['idempresa']));


        $documentos = array();
        foreach ($data_tabla as $clave => $valor) {
            $documento = new documento();
            $documento->setId($data_tabla[$clave]["id"]);
            $documento->setserie($data_tabla[$clave]["serie"]);
            $documento->setNumero($data_tabla[$clave]["numero"]);
            $documento->setFechaemision($data_tabla[$clave]["fechaemision"]);
            $documento->setFechavencimiento($data_tabla[$clave]["fechavencimiento"]);
            $documento->setNorden($data_tabla[$clave]["nroden"]);
            $documento->setMoneda($data_tabla[$clave]["moneda"]);
            $documento->setIncigv($data_tabla[$clave]["incigv"]);
            $documento->setIdsunattransaction($data_tabla[$clave]["id_sunat_transaction"]);
            $documento->setIdusuario($data_tabla[$clave]["id_usuario"]);
            $documento->setTipoventa($data_tabla[$clave]["tipo_venta"]);
            $documento->setTipoventamov($data_tabla[$clave]["tipo_venta_movimiento"]);
            $documento->setTipoventaop($data_tabla[$clave]["tipo_venta_nop"]);
            $documento->setSujetoa($data_tabla[$clave]["sujetoa"]);
            $documento->setIdpersona($data_tabla[$clave]["id_persona"]);
            $documento->setRuc($data_tabla[$clave]["ruc"]);
            $documento->setRazonsocial($data_tabla[$clave]["razonsocial"]);
            $documento->setDireccion($data_tabla[$clave]["direccion"]);
            $documento->setEmail($data_tabla[$clave]["email"]);
            $documento->setEstadosunat($data_tabla[$clave]["estadosunat"]);
            $documento->setEstadolocal($data_tabla[$clave]["estadolocal"]);
            $documento->setEmail($data_tabla[$clave]["email"]);
            $documento->setTipo($data_tabla[$clave]["tipo"]);
            $documento->setTotal($data_tabla[$clave]["total"]);
            $documento->setIdsucursal($data_tabla[$clave]["id_sucursal"]);
            $documento->setTipopago($data_tabla[$clave]["tipo_pago"]);
            $documento->setNrooptipopago($data_tabla[$clave]["nroop_tipopago"]);
            $documento->setMontogratuita($data_tabla[$clave]["montogratuita"]);
            $documento->setMontoexonerado($data_tabla[$clave]["montoexonerado"]);
            $documento->setMontoinafecto($data_tabla[$clave]["montoinafecto"]);
            $documento->setMontoexportacion($data_tabla[$clave]["montoexportacion"]);
            $documento->setMontogravada($data_tabla[$clave]["montogravada"]);
            $documento->setMontoigv($data_tabla[$clave]["montoigv"]);
            
            $documento->setRucdestinatario($data_tabla[$clave]["rucdestinatario"]);
            $documento->setRazonsialdestinatario($data_tabla[$clave]["razonsocialdestinatario"]);
            $documento->setDirecciondestinatario($data_tabla[$clave]["direcciondestinatario"]);
            $documento->setMotivoguia($data_tabla[$clave]["motivoguia"]);
            $documento->setRuctransportista($data_tabla[$clave]["ructransportista"]);
            $documento->setRazonsocialtransportista($data_tabla[$clave]["razonsocialtransportista"]);
            $documento->setCostotransportista($data_tabla[$clave]["costotransportista"]);
            $documento->setMarcaplaca($data_tabla[$clave]["marca_placa"]);
            $documento->setLicenciatransportista($data_tabla[$clave]["licenciatransportista"]);
            $documento->setCertificdotransportista($data_tabla[$clave]["certificadotransportista"]);
            
            $documento->setIdalmacendestino($data_tabla[$clave]["idalmacendestinatario"]);
            $documento->setIdalmacenemisor($data_tabla[$clave]["idalmacenemisor"]);
      





            array_push($documentos, $documento);
        }
        return $documentos;
    }

    function select($desde, $hasta, $tipocomp,$tipodoc, $buscar, $serie, $numero, $idsucur) {


        $fecha = '';
        if (!empty($desde) && !empty($hasta)) {
            $fecha = 'fechaemision between "' . $desde . '" and "' . $hasta . '" and ';
        }
        if (!empty($tipocomp)) {
            $tipocomp = 'tipo= "' . $tipocomp . '" and ';
        }
        if (!empty($tipodoc)) {
            $tipodoc = 'tipo_doc= "' . $tipodoc . '" and ';
        }
        if (!empty($buscar)) {
            $buscar = 'concat(ruc,razonsocial) like concat("%","' . $buscar . '","%") and ';
        }
        if (!empty($serie)) {
            $serie = 'serie= "' . $serie . '" and ';
        }
        if (!empty($numero)) {
            $numero = 'numero= "' . $numero . '" and ';
        }
        if (!empty($idsucur)) {
            $idsucur = 'id_sucursal= ' . $idsucur;
        }

//        echo 'SELECT doc.*,sum(det.total) as totaldoc FROM `documento` as doc inner join detalle as det on det.id_documento=doc.id
//         where ' . $fecha . $tipocomp . $tipodoc. $buscar . $serie . $numero . $idsucur . ' GROUP BY id_documento order by id_documento desc;';

//        echo 'SELECT doc.*,sum(det.total) as totaldoc FROM `documento` as doc inner join detalle as det on det.id_documento=doc.id
//  where ' . $fecha . $tipocomp . $tipodoc. $buscar . $serie . $numero . $idsucur . ' GROUP BY id_documento order by id_documento desc;';

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta('SELECT doc.*,sum(det.total) as totaldoc FROM `documento` as doc inner join detalle as det on det.id_documento=doc.id
  where ' . $fecha . $tipocomp . $tipodoc. $buscar . $serie . $numero . $idsucur . ' and doc.id_empresa='.$_SESSION['idempresa'].' GROUP BY id_documento order by id_documento desc;');


        $documentos = array();
        foreach ($data_tabla as $clave => $valor) {
            $documento = new documento();
            $documento->setId($data_tabla[$clave]["id"]);
            $documento->setserie($data_tabla[$clave]["serie"]);
            $documento->setNumero($data_tabla[$clave]["numero"]);
            $documento->setFechaemision($data_tabla[$clave]["fechaemision"]);
            $documento->setFechavencimiento($data_tabla[$clave]["fechavencimiento"]);
            $documento->setNorden($data_tabla[$clave]["nroden"]);
            $documento->setMoneda($data_tabla[$clave]["moneda"]);
            $documento->setIncigv($data_tabla[$clave]["incigv"]);
            $documento->setIdsunattransaction($data_tabla[$clave]["id_sunat_transaction"]);
            $documento->setIdusuario($data_tabla[$clave]["id_usuario"]);
            $documento->setTipoventa($data_tabla[$clave]["tipo_venta"]);
            $documento->setTipoventamov($data_tabla[$clave]["tipo_venta_movimiento"]);
            $documento->setTipoventaop($data_tabla[$clave]["tipo_venta_nop"]);
            $documento->setSujetoa($data_tabla[$clave]["sujetoa"]);
            $documento->setIdpersona($data_tabla[$clave]["id_persona"]);
            $documento->setRuc($data_tabla[$clave]["ruc"]);
            $documento->setRazonsocial($data_tabla[$clave]["razonsocial"]);
            $documento->setDireccion($data_tabla[$clave]["direccion"]);
            $documento->setEmail($data_tabla[$clave]["email"]);
            $documento->setEstadosunat($data_tabla[$clave]["estadosunat"]);
            $documento->setEstadolocal($data_tabla[$clave]["estadolocal"]);
            $documento->setEmail($data_tabla[$clave]["email"]);
            $documento->setTipo($data_tabla[$clave]["tipo"]);
            $documento->setTipodoc($data_tabla[$clave]["tipo_doc"]);
            $documento->setTotal($data_tabla[$clave]["totaldoc"]);
            $documento->setIdsucursal($data_tabla[$clave]["id_sucursal"]);
            $documento->setRucdestinatario($data_tabla[$clave]["rucdestinatario"]);
            $documento->setRazonsialdestinatario($data_tabla[$clave]["razonsocialdestinatario"]);
            $documento->setDirecciondestinatario($data_tabla[$clave]["direcciondestinatario"]);
            $documento->setMotivoguia($data_tabla[$clave]["motivoguia"]);
            $documento->setRuctransportista($data_tabla[$clave]["ructransportista"]);
            $documento->setRazonsocialtransportista($data_tabla[$clave]["razonsocialtransportista"]);
            $documento->setCostotransportista($data_tabla[$clave]["costotransportista"]);
            $documento->setMarcaplaca($data_tabla[$clave]["marca_placa"]);
            $documento->setLicenciatransportista($data_tabla[$clave]["licenciatransportista"]);
            $documento->setCertificdotransportista($data_tabla[$clave]["certificadotransportista"]);

            array_push($documentos, $documento);
        }
        return $documentos;
    }
    function selecttraslado($desde, $hasta, $tipocomp,$tipodoc,  $serie, $numero, $partida,$destino) {


        $fecha = '';
        if (!empty($desde) && !empty($hasta)) {
            $fecha = 'fechaemision between "' . $desde . '" and "' . $hasta . '" and ';
        }
        if (!empty($tipocomp)) {
            $tipocomp = 'tipo= "' . $tipocomp . '" and ';
        }
        if (!empty($tipodoc)) {
            $tipodoc = 'tipo_doc= "' . $tipodoc . '" and ';
        } 
        if (!empty($serie)) {
            $serie = 'serie= "' . $serie . '" and ';
        }
        if (!empty($numero)) {
            $numero = 'numero= "' . $numero . '" and ';
        }
       
        
        if (!empty($destino)) {
            $destino = ' idalmacendestinatario= "' . $destino.'" and';
        }
        
         if (!empty($partida)) {
            $partida = ' idalmacenemisor= "' . $partida.'"  ';
        }

//        echo 'SELECT doc.*,sum(det.total) as totaldoc FROM `documento` as doc inner join detalle as det on det.id_documento=doc.id
//  where ' . $fecha . $tipocomp . $tipodoc. $numero . $serie . $destino.$partida .  '  doc.id_empresa='.$_SESSION['idempresa'].' GROUP BY doc.id order by doc.id desc;';

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta('SELECT doc.*,sum(det.total) as totaldoc FROM `documento` as doc inner join detalle as det on det.id_documento=doc.id
  where ' . $fecha . $tipocomp . $tipodoc. $numero . $serie . $destino.$partida .  '  doc.id_empresa='.$_SESSION['idempresa'].' GROUP BY doc.id order by doc.id desc;');


        $documentos = array();
        foreach ($data_tabla as $clave => $valor) {
            $documento = new documento();
            $documento->setId($data_tabla[$clave]["id"]);
            $documento->setserie($data_tabla[$clave]["serie"]);
            $documento->setNumero($data_tabla[$clave]["numero"]);
            $documento->setFechaemision($data_tabla[$clave]["fechaemision"]);
            $documento->setFechavencimiento($data_tabla[$clave]["fechavencimiento"]);
            $documento->setNorden($data_tabla[$clave]["nroden"]);
            $documento->setMoneda($data_tabla[$clave]["moneda"]);
            $documento->setIncigv($data_tabla[$clave]["incigv"]);
            $documento->setIdsunattransaction($data_tabla[$clave]["id_sunat_transaction"]);
            $documento->setIdusuario($data_tabla[$clave]["id_usuario"]);
            $documento->setTipoventa($data_tabla[$clave]["tipo_venta"]);
            $documento->setTipoventamov($data_tabla[$clave]["tipo_venta_movimiento"]);
            $documento->setTipoventaop($data_tabla[$clave]["tipo_venta_nop"]);
            $documento->setSujetoa($data_tabla[$clave]["sujetoa"]);
            $documento->setIdpersona($data_tabla[$clave]["id_persona"]);
            $documento->setRuc($data_tabla[$clave]["ruc"]);
            $documento->setRazonsocial($data_tabla[$clave]["razonsocial"]);
            $documento->setDireccion($data_tabla[$clave]["direccion"]);
            $documento->setEmail($data_tabla[$clave]["email"]);
            $documento->setEstadosunat($data_tabla[$clave]["estadosunat"]);
            $documento->setEstadolocal($data_tabla[$clave]["estadolocal"]);
            $documento->setEmail($data_tabla[$clave]["email"]);
            $documento->setTipo($data_tabla[$clave]["tipo"]);
            $documento->setTipodoc($data_tabla[$clave]["tipo_doc"]);
            $documento->setTotal($data_tabla[$clave]["totaldoc"]);
            $documento->setIdsucursal($data_tabla[$clave]["id_sucursal"]);
            $documento->setRucdestinatario($data_tabla[$clave]["rucdestinatario"]);
            $documento->setRazonsialdestinatario($data_tabla[$clave]["razonsocialdestinatario"]);
            $documento->setDirecciondestinatario($data_tabla[$clave]["direcciondestinatario"]);
            $documento->setMotivoguia($data_tabla[$clave]["motivoguia"]);
            $documento->setRuctransportista($data_tabla[$clave]["ructransportista"]);
            $documento->setRazonsocialtransportista($data_tabla[$clave]["razonsocialtransportista"]);
            $documento->setCostotransportista($data_tabla[$clave]["costotransportista"]);
            $documento->setMarcaplaca($data_tabla[$clave]["marca_placa"]);
            $documento->setLicenciatransportista($data_tabla[$clave]["licenciatransportista"]);
            $documento->setCertificdotransportista($data_tabla[$clave]["certificadotransportista"]);
            
            $documento->setIdalmacendestino($data_tabla[$clave]["idalmacendestinatario"]);
            $documento->setIdalmacenemisor($data_tabla[$clave]["idalmacenemisor"]);

            array_push($documentos, $documento);
        }
        return $documentos;
    }
    function selectdetallado($desde, $hasta, $tipocomp,$tipodoc, $buscar, $serie, $numero, $moneda, $idsucur) {


        $fecha = '';
        if (!empty($desde) && !empty($hasta)) {
            $fecha = 'fechaemision between "' . $desde . '" and "' . $hasta . '" and ';
        }
        if (!empty($tipocomp)) {
            $tipocomp = 'tipo= "' . $tipocomp . '" and ';
        }
        if (!empty($moneda)) {
            $moneda = 'moneda= "' . $moneda . '" and ';
        }
        if (!empty($tipodoc)) {
            $tipodoc = 'tipo_doc= "' . $tipodoc . '" and ';
        }
        if (!empty($buscar)) {
            $buscar = 'concat(ruc,razonsocial) like concat("%","' . $buscar . '","%") and ';
        }
        if (!empty($serie)) {
            $serie = 'serie= "' . $serie . '" and ';
        }
        if (!empty($numero)) {
            $numero = 'numero= "' . $numero . '" and ';
        }
        if (!empty($idsucur)) {
            $idsucur = 'doc.id_sucursal= ' . $idsucur;
        }


//        echo 'select * from documento  where '.$fecha.$tipocomp.$buscar.$serie.$numero.$idsucur.' order by id desc;';

//        echo 'select doc.tipo,concat(doc.serie," - ",doc.numero) as serien,doc.fechaemision,doc.ruc,doc.razonsocial,u.nombre,doc.tipo_pago,
//        CONCAT(det.descripcionprod," (",det.cantidad," x ",det.precio,")") as descripcionprod,und.descripcion as unidad, precio,cantidad,doc.moneda, (det.cantidad*det.precio) - (det.montobasegratuito + det.montobaseivap) as total,
//        case WHEN doc.incigv = 1 THEN  (det.cantidad*det.precio) - (det.montobasegratuito + det.montobaseivap)  else  
//        (((det.cantidad*det.precio) - (det.montobasegratuito + det.montobaseivap))- 
//        (det.montobaseexpo+det.montobaseexonarado+det.montobaseinafecto))+((((det.cantidad*det.precio) - (det.montobasegratuito + det.montobaseivap))- 
//        (det.montobaseexpo+det.montobaseexonarado+det.montobaseinafecto)) - ((((det.cantidad*det.precio) - (det.montobasegratuito + det.montobaseivap))- 
//        (det.montobaseexpo+det.montobaseexonarado+det.montobaseinafecto))/ 1.18)) end as total2, case when doc.incigv = 1 THEN "Si" else "No" end as incluyeigv,
//        ti.descripcion, doc.estadolocal,doc.estadosunat
//         from documento as doc INNER JOIN detalle 
//        as det on doc.id = det.id_documento INNER JOIN tipo_impuesto as ti on ti.id= det.id_impuesto INNER JOIN usuario as u on u.id = doc.id_usuario
//        INNER JOIN unidmedida as und on und.id=det.id_unidad where ' . $fecha . $tipocomp . $tipodoc. $buscar . $serie . $numero .$moneda. $idsucur . ' order by id_documento desc;';
        
        
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta('select doc.tipo,concat(doc.serie," - ",doc.numero) as serien,doc.fechaemision,doc.ruc,doc.razonsocial,u.nombre,doc.tipo_pago,
        CONCAT(det.descripcionprod," (",det.cantidad," x ",det.precio,")") as descripcionprod,und.descripcion as unidad, precio,cantidad,doc.moneda, (det.cantidad*det.precio) - (det.montobasegratuito + det.montobaseivap) as total,
        case WHEN doc.incigv = 1 THEN  (det.cantidad*det.precio) - (det.montobasegratuito + det.montobaseivap)  else  
        (((det.cantidad*det.precio) - (det.montobasegratuito + det.montobaseivap))- 
        (det.montobaseexpo+det.montobaseexonarado+det.montobaseinafecto))+((((det.cantidad*det.precio) - (det.montobasegratuito + det.montobaseivap))- 
        (det.montobaseexpo+det.montobaseexonarado+det.montobaseinafecto)) - ((((det.cantidad*det.precio) - (det.montobasegratuito + det.montobaseivap))- 
        (det.montobaseexpo+det.montobaseexonarado+det.montobaseinafecto))/ 1.18)) end as total2, case when doc.incigv = 1 THEN "Si" else "No" end as incluyeigv,
        ti.descripcion, doc.estadolocal,doc.estadosunat
         from documento as doc INNER JOIN detalle 
        as det on doc.id = det.id_documento INNER JOIN tipo_impuesto as ti on ti.id= det.id_impuesto INNER JOIN usuario as u on u.id = doc.id_usuario
        INNER JOIN unidmedida as und on und.id=det.id_unidad where ' . $fecha . $tipocomp . $tipodoc. $buscar . $serie . $numero .$moneda. $idsucur . ' and doc.id_empresa='.$_SESSION['idempresa'].' order by id_documento desc;');


        $detallado = array();
        foreach ($data_tabla as $clave => $valor) {
            $documento = array(
                "tipo"=>$data_tabla[$clave]["tipo"],
                "serien"=>$data_tabla[$clave]["serien"],
                "fechaemision"=>$data_tabla[$clave]["fechaemision"],
                "ruc"=>$data_tabla[$clave]["ruc"],
                "razonsocial"=>$data_tabla[$clave]["razonsocial"],
                "vendedor"=>$data_tabla[$clave]["nombre"],
                "tipo_pago"=>$data_tabla[$clave]["tipo_pago"],
                "descripcionprod"=>$data_tabla[$clave]["descripcionprod"],
                "moneda"=>$data_tabla[$clave]["moneda"],
                "total"=>$data_tabla[$clave]["total2"],
                "incigv"=>$data_tabla[$clave]["incluyeigv"],
                "impuesto"=>$data_tabla[$clave]["descripcion"],
                "estadolocal"=>$data_tabla[$clave]["estadolocal"],
                "estadosunat"=>$data_tabla[$clave]["estadosunat"],
                "unidad"=>$data_tabla[$clave]["unidad"],
                "precio"=>$data_tabla[$clave]["precio"],
                "cantidad"=>$data_tabla[$clave]["cantidad"]);
            
                array_push($detallado, $documento); 
        }
        return $detallado;
    }
    function selectcuentacobrar($desde, $hasta, $buscar, $idsucur,$tipo,$tipodoc) {

        
        $fecha = '';
        if (!empty($desde) && !empty($hasta)) {
            $fecha = ' fechaemision between "' . $desde . '" and "' . $hasta . '" and ';
        }
        if ($tipo == 'Vencido') {
            $tipo = ' fechavencimiento <= CURDATE() and doc.total  >  (COALESCE((SELECT SUM(docp.monto) from documento_pagos as docp where docp.id_documento = doc.id),0)) and ';
        }
        if($tipo == 'Todo'){
            $tipo = ' doc.total  >  (COALESCE((SELECT SUM(docp.monto) from documento_pagos as docp where docp.id_documento = doc.id),0)) and ';
            
        }
        if($tipo == 'Cancelado'){
            $tipo = ' doc.total  <=  (COALESCE((SELECT SUM(docp.monto) from documento_pagos as docp where docp.id_documento = doc.id),0)) and ';
            
        }
       
        if (!empty($buscar)) {
            $buscar = 'concat(ruc,razonsocial) like concat("%","' . $buscar . '","%") and ';
        }
        
        if (!empty($idsucur)) {
            $idsucur = ' doc.id_sucursal= ' . $idsucur;
        }
//        echo 'SELECT doc.id,concat(serie," - ",numero) as documento,ruc,razonsocial,fechaemision,fechavencimiento,
//        concat(DATEDIFF(fechavencimiento ,fechaemision )," Días") as dias,moneda,
//       doc.total,doc.tipo_venta
//                 from documento as doc INNER JOIN detalle as det on det.id_documento =
//        doc.id WHERE tipo_doc = "'.$tipodoc.'" and tipo_venta="Credito" and ' . $fecha . $tipo . $buscar  . $idsucur . ' and doc.id_empresa='.$_SESSION['idempresa'].' order by fechavencimiento desc;';

// 

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta('SELECT doc.id,concat(serie," - ",numero) as documento,ruc,razonsocial,fechaemision,fechavencimiento,
        concat(DATEDIFF(fechavencimiento ,fechaemision )," Días") as dias,moneda,
       doc.total,doc.tipo_venta
                 from documento as doc INNER JOIN detalle as det on det.id_documento =
        doc.id WHERE tipo_doc = "'.$tipodoc.'" and tipo_venta="Credito" and ' . $fecha . $tipo . $buscar  . $idsucur . ' and doc.id_empresa='.$_SESSION['idempresa'].' order by fechavencimiento desc;');


        $cobrar = array();
        foreach ($data_tabla as $clave => $valor) {
            $detalle = array(
                "id"=>$data_tabla[$clave]["id"],
                "documento"=>$data_tabla[$clave]["documento"],
                "fechaemision"=>$data_tabla[$clave]["fechaemision"],
                "ruc"=>$data_tabla[$clave]["ruc"],
                "razonsocial"=>$data_tabla[$clave]["razonsocial"],
                "fechavencimiento"=>$data_tabla[$clave]["fechavencimiento"],
                "dias"=>$data_tabla[$clave]["dias"],
                "moneda"=>$data_tabla[$clave]["moneda"],
               
                "total"=>$data_tabla[$clave]["total"],
                "tipoventa"=>$data_tabla[$clave]["tipo_venta"]);
              
            
                array_push($cobrar, $detalle); 
        }
        return $cobrar;
    }

    function insert(documento $documento) {
        
        if(!empty($documento->getIdcaja())){
            $idcaja = $documento->getIdcaja();
        }else {
            $idcaja = 0;
        }
        
        

        $data_source = new DataSource();
        $id = 0;

        $data_source->ejecutarActualizacion("insert into documento(serie, numero, fechaemision,fechavencimiento,nroden,"
                . "moneda,incigv,id_sunat_transaction,id_usuario,tipo_venta,tipo_venta_movimiento,tipo_venta_nop,"
                . "sujetoa,id_persona,ruc,razonsocial,direccion,email,estadosunat,estadolocal,total,tipo,id_sucursal,tipo_doc,tipo_cambio,documento_ref,serie_ref,numero_ref,id_tiponota,observacion,garantia,condicionpago,"
                . "validezdias,plazoentregadias,condicionpagodias,atencion,tipo_pago,nroop_tipopago,montogratuita,montoexonerado,"
                . "montoinafecto,montoexportacion,montogravada,montoigv,rucdestinatario,razonsocialdestinatario,"
                . "direcciondestinatario,motivoguia,ructransportista,razonsocialtransportista,costotransportista,marca_placa,"
                . "licenciatransportista,certificadotransportista,persona_tipo_doc,tarjeta,id_caja,telfijo,id_empresa,created_at,idalmacendestinatario,idalmacenemisor) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", array(
            $documento->getSerie(),
            $documento->getNumero(),
            $documento->getFechaemision(),
            $documento->getFechavencimiento(),
            $documento->getNorden(),
            $documento->getMoneda(),
            $documento->getIncigv(),
            $documento->getIdsunattransaction(),
            $documento->getIdusuario(),
            $documento->getTipoventa(),
            $documento->getTipoventamov(),
            $documento->getTipoventaop(),
            $documento->getSujetoa(),
            $documento->getIdpersona(),
            $documento->getRuc(),
            $documento->getRazonsocial(),
            $documento->getDireccion(),
            $documento->getEmail(),
            $documento->getEstadosunat(),
            $documento->getEstadolocal(),
            $documento->getTotal(),
            $documento->getTipo(),
            $documento->getIdsucursal(),
            $documento->getTipodoc(),
            $documento->getTipocambio(),
            $documento->getDocref(),
            $documento->getSerieref(),
            $documento->getNumeroref(),
            $documento->getIdtiponota(),
            $documento->getObservacion(),
            $documento->getGarantia(),
            $documento->getCondicionpago(),
            $documento->getValidezdias(),
            $documento->getPlazoentregadias(),
            $documento->getCondicionpagodias(),
            $documento->getAtencion(),
            $documento->getTipopago(),
            $documento->getNrooptipopago(),
            $documento->getMontogratuita(),
            $documento->getMontoexonerado(),
            $documento->getMontoinafecto(),
            $documento->getMontoexportacion(),
            $documento->getMontogravada(),
            $documento->getMontoigv(),
                
            $documento->getRucdestinatario(),
            $documento->getRazonsialdestinatario(),
            $documento->getDirecciondestinatario(),
            $documento->getMotivoguia(),
            $documento->getRuctransportista(),
            $documento->getRazonsocialtransportista(),
            $documento->getCostotransportista(),
            $documento->getMarcaplaca(),
            $documento->getLicenciatransportista(),
            $documento->getCertificdotransportista(),
            $documento->getPersonatipodoc(),
            $documento->getTarjeta(),
            $idcaja,
            $documento->getTelfijo(),
            $_SESSION['idempresa'],
            date('Y-m-d H:i:s'),
            
            $documento->getIdalmacendestino(),
            $documento->getIdalmacenemisor(),
                    
                
               
                    
            
        ));
        $id = $data_source->lastinsertid();
        return $id;
    }
    function update(documento $documento) {

        $data_source = new DataSource();
      

          $fila = $data_source->ejecutarActualizacion("update documento set  fechaemision=?,fechavencimiento=?,nroden=?,"
                . "moneda=?,incigv=?,id_sunat_transaction=?,id_usuario=?,tipo_venta=?,tipo_venta_movimiento=?,tipo_venta_nop=?,"
                . "sujetoa=?,id_persona=?,ruc=?,razonsocial=?,direccion=?,email=?,estadosunat=?,estadolocal=?,total=?,id_sucursal=?,tipo_cambio=?,documento_ref=?,serie_ref=?,numero_ref=?,id_tiponota=?,observacion=?,garantia=?,condicionpago=?,"
                . "validezdias=?,plazoentregadias=?,condicionpagodias=?,atencion=?,tipo_pago=?,nroop_tipopago=?,"
                  . "montogratuita=?,montoexonerado=?,montoinafecto=?,montoexportacion=?,montogravada=?,montoigv=?,"
                  . "rucdestinatario=?,razonsocialdestinatario=?,direcciondestinatario=?,motivoguia=?,ructransportista=?,razonsocialtransportista=?,costotransportista=?,marca_placa=?,"
                . "licenciatransportista=?,certificadotransportista=?,persona_tipo_doc=?, updated_at=? where id=? and id_empresa=?;", array( //serie=?, numero=?,tipo=?,
//            $documento->getSerie(),
//            $documento->getNumero(),
            $documento->getFechaemision(),
            $documento->getFechavencimiento(),
            $documento->getNorden(),
            $documento->getMoneda(),
            $documento->getIncigv(),
            $documento->getIdsunattransaction(),
            $documento->getIdusuario(),
            $documento->getTipoventa(),
            $documento->getTipoventamov(),
            $documento->getTipoventaop(),
            $documento->getSujetoa(),
            $documento->getIdpersona(),
            $documento->getRuc(),
            $documento->getRazonsocial(),
            $documento->getDireccion(),
            $documento->getEmail(),
            $documento->getEstadosunat(),
            $documento->getEstadolocal(),
            $documento->getTotal(),
//            $documento->getTipo(),
            $documento->getIdsucursal(),
//            $documento->getTipodoc(),
            $documento->getTipocambio(),
            $documento->getDocref(),
            $documento->getSerieref(),
            $documento->getNumeroref(),
            $documento->getIdtiponota(),
            $documento->getObservacion(),
            $documento->getGarantia(),
            $documento->getCondicionpago(),
            $documento->getValidezdias(),
            $documento->getPlazoentregadias(),
            $documento->getCondicionpagodias(),
            $documento->getAtencion(),
            $documento->getTipopago(),
            $documento->getNrooptipopago(),
            $documento->getMontogratuita(),
            $documento->getMontoexonerado(),
            $documento->getMontoinafecto(),
            $documento->getMontoexportacion(),
            $documento->getMontogravada(),
            $documento->getMontoigv(),
            
            $documento->getRucdestinatario(),
            $documento->getRazonsialdestinatario(),
            $documento->getDirecciondestinatario(),
            $documento->getMotivoguia(),
            $documento->getRuctransportista(),
            $documento->getRazonsocialtransportista(),
            $documento->getCostotransportista(),
            $documento->getMarcaplaca(),
            $documento->getLicenciatransportista(),
            $documento->getCertificdotransportista(),          
            $documento->getPersonatipodoc(),   
            date('Y-m-d H:i:s'),
                      
            $documento->getId(),
            $_SESSION['idempresa']
                    
        ));
       
        return $fila;
    }

    function selectOne($id) {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("SELECT * FROM documento 
        where id=? and id_empresa=?;", array($id,$_SESSION['idempresa']));


        $documento = new documento();
        foreach ($data_tabla as $clave => $valor) {

            $documento->setId($data_tabla[$clave]["id"]);
            $documento->setserie($data_tabla[$clave]["serie"]);
            $documento->setNumero($data_tabla[$clave]["numero"]);
            $documento->setFechaemision($data_tabla[$clave]["fechaemision"]);
            $documento->setFechavencimiento($data_tabla[$clave]["fechavencimiento"]);
            $documento->setNorden($data_tabla[$clave]["nroden"]);
            $documento->setMoneda($data_tabla[$clave]["moneda"]);
            $documento->setIncigv($data_tabla[$clave]["incigv"]);
            $documento->setIdsunattransaction($data_tabla[$clave]["id_sunat_transaction"]);
            $documento->setIdusuario($data_tabla[$clave]["id_usuario"]);
            $documento->setTipoventa($data_tabla[$clave]["tipo_venta"]);
            $documento->setTipoventamov($data_tabla[$clave]["tipo_venta_movimiento"]);
            $documento->setTipoventaop($data_tabla[$clave]["tipo_venta_nop"]);
            $documento->setSujetoa($data_tabla[$clave]["sujetoa"]);
            $documento->setIdpersona($data_tabla[$clave]["id_persona"]);
            $documento->setRuc($data_tabla[$clave]["ruc"]);
            $documento->setRazonsocial($data_tabla[$clave]["razonsocial"]);
            $documento->setDireccion($data_tabla[$clave]["direccion"]);
            $documento->setEmail($data_tabla[$clave]["email"]);
            $documento->setEstadosunat($data_tabla[$clave]["estadosunat"]);
            $documento->setEstadolocal($data_tabla[$clave]["estadolocal"]);
            $documento->setEmail($data_tabla[$clave]["email"]);
            $documento->setTipo($data_tabla[$clave]["tipo"]);
            $documento->setTipodoc($data_tabla[$clave]["tipo_doc"]);
//            $documento->setTotal($data_tabla[$clave]["total"]);
            $documento->setIdsucursal($data_tabla[$clave]["id_sucursal"]);
            $documento->setTipopago($data_tabla[$clave]["tipo_pago"]);
            $documento->setNrooptipopago($data_tabla[$clave]["nroop_tipopago"]);
            $documento->setObservacion($data_tabla[$clave]["observacion"]);
            $documento->setGarantia($data_tabla[$clave]["garantia"]);
            $documento->setCondicionpago($data_tabla[$clave]["condicionpago"]);
            $documento->setValidezdias($data_tabla[$clave]["validezdias"]);
            $documento->setPlazoentregadias($data_tabla[$clave]["plazoentregadias"]);
            $documento->setCondicionpagodias($data_tabla[$clave]["condicionpagodias"]);
            $documento->setAtencion($data_tabla[$clave]["atencion"]);
            $documento->setCondicionpagodias($data_tabla[$clave]["condicionpagodias"]);
            $documento->setTipopago($data_tabla[$clave]["tipo_pago"]);
            $documento->setNrooptipopago($data_tabla[$clave]["nroop_tipopago"]);
            $documento->setMotivoanulacion($data_tabla[$clave]["motivoanulacion"]);
            $documento->setTipocambio($data_tabla[$clave]["tipo_cambio"]);
            $documento->setTotal($data_tabla[$clave]["total"]);
            $documento->setMontogratuita($data_tabla[$clave]["montogratuita"]);
            $documento->setMontoexonerado($data_tabla[$clave]["montoexonerado"]);
            $documento->setMontoinafecto($data_tabla[$clave]["montoinafecto"]);
            $documento->setMontoexportacion($data_tabla[$clave]["montoexportacion"]);
            $documento->setMontogravada($data_tabla[$clave]["montogravada"]);
            $documento->setMontoigv($data_tabla[$clave]["montoigv"]);
            
            $documento->setRucdestinatario($data_tabla[$clave]["rucdestinatario"]);
            $documento->setRazonsialdestinatario($data_tabla[$clave]["razonsocialdestinatario"]);
            $documento->setDirecciondestinatario($data_tabla[$clave]["direcciondestinatario"]);
            $documento->setMotivoguia($data_tabla[$clave]["motivoguia"]);
            $documento->setRuctransportista($data_tabla[$clave]["ructransportista"]);
            $documento->setRazonsocialtransportista($data_tabla[$clave]["razonsocialtransportista"]);
            $documento->setCostotransportista($data_tabla[$clave]["costotransportista"]);
            $documento->setMarcaplaca($data_tabla[$clave]["marca_placa"]);
            $documento->setLicenciatransportista($data_tabla[$clave]["licenciatransportista"]);
            $documento->setCertificdotransportista($data_tabla[$clave]["certificadotransportista"]);
            $documento->setPersonatipodoc($data_tabla[$clave]["persona_tipo_doc"]);
        }
        return $documento;
    }

    function selectMax($tipodoc, $tipo, $serie) {

        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from documento where tipo_doc = ? and tipo = ? and serie = ? and id_empresa=? ORDER BY id DESC LIMIT 1;", array($tipodoc, $tipo, $serie,$_SESSION['idempresa']));


        $documento = new documento();
        foreach ($data_tabla as $clave => $valor) {

            $documento->setId($data_tabla[$clave]["id"]);
            $documento->setserie($data_tabla[$clave]["serie"]);
            $documento->setNumero($data_tabla[$clave]["numero"]);
            $documento->setFechaemision($data_tabla[$clave]["fechaemision"]);
            $documento->setFechavencimiento($data_tabla[$clave]["fechavencimiento"]);
            $documento->setNorden($data_tabla[$clave]["nroden"]);
            $documento->setMoneda($data_tabla[$clave]["moneda"]);
            $documento->setIncigv($data_tabla[$clave]["incigv"]);
            $documento->setIdsunattransaction($data_tabla[$clave]["id_sunat_transaction"]);
            $documento->setIdusuario($data_tabla[$clave]["id_usuario"]);
            $documento->setTipoventa($data_tabla[$clave]["tipo_venta"]);
            $documento->setTipoventamov($data_tabla[$clave]["tipo_venta_movimiento"]);
            $documento->setTipoventaop($data_tabla[$clave]["tipo_venta_nop"]);
            $documento->setSujetoa($data_tabla[$clave]["sujetoa"]);
            $documento->setIdpersona($data_tabla[$clave]["id_persona"]);
            $documento->setRuc($data_tabla[$clave]["ruc"]);
            $documento->setRazonsocial($data_tabla[$clave]["razonsocial"]);
            $documento->setDireccion($data_tabla[$clave]["direccion"]);
            $documento->setEmail($data_tabla[$clave]["email"]);
            $documento->setEstadosunat($data_tabla[$clave]["estadosunat"]);
            $documento->setEstadolocal($data_tabla[$clave]["estadolocal"]);
            $documento->setEmail($data_tabla[$clave]["email"]);
            $documento->setTipo($data_tabla[$clave]["tipo"]);
            $documento->setTotal($data_tabla[$clave]["total"]);
            $documento->setIdsucursal($data_tabla[$clave]["id_sucursal"]);
            $documento->setTipopago($data_tabla[$clave]["tipo_pago"]);
            $documento->setNrooptipopago($data_tabla[$clave]["nroop_tipopago"]);
        }
        return $documento;
    }

    function duplicado($serie, $numero, $tipodoc) {
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select 1 from documento where serie = ? and numero = ? and tipo_doc = ? and id_sucursal=? and id_empresa=?;", array($serie, $numero, $tipodoc,$_SESSION['idsucursal'] ,$_SESSION['idempresa']));
        $bol = 'valido';
        foreach ($data_tabla as $clave => $valor) {
            $bol = 'duplicado';
        }
        return $bol;
    }

    function anular($id, $motivo,$estadosunat,$estadolocal) {
        $data_source = new DataSource();

        return $data_source->ejecutarActualizacion("update documento set estadolocal=? , estadosunat = concat(estadosunat,' - ',?) , motivoanulacion=? , updated_at =? where id = ?", array($estadolocal,$estadosunat,$motivo,date('Y-m-d H:i:s'), $id));
    }
    
    function resumen($idsursal, $fecha){
        
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("SELECT total,SUM(case when tipo_doc='Venta' and moneda='Soles' then total else 0 end ) as ventasoles,
        SUM(case when tipo_doc='Venta' and moneda='Dolares' then total else 0 end ) as ventadolares,
        SUM(case when tipo_doc='Compra' and moneda='Soles' then total else 0 end ) as comprasoles,
        SUM(case when tipo_doc= 'Compra' and moneda='Dolares' then total else 0 END) as compradolares,

        SUM(case when tipo_pago= 'Efectivo' and moneda='Soles' then total else 0 END) as efectivosoles,
        SUM(case when tipo_pago= 'Efectivo' and moneda='Dolares' then total else 0 END) as efectivodolares,

        SUM(case when tipo_pago= 'MasterCard' or tipo_pago= 'Visa' or tipo_pago= 'American Express'  and moneda='Dolares' then total else 0 END) as tarjetadollar,
        SUM(case when tipo_pago= 'MasterCard' or tipo_pago= 'Visa' or tipo_pago= 'American Express'  and moneda='Soles' then total else 0 END) as tarjetasoles,
        SUM(case when tipo_pago= 'C.Entrega' and moneda='Dolares' then total else 0 END) as contraentredolares,
        SUM(case when tipo_pago= 'C.Entrega' and moneda='Soles' then total else 0 END) as contraentresoles 


        from documento where fechaemision = ? and id_sucursal = ? and id_empresa;",array($fecha,$idsursal,$_SESSION['idempresa']));

        $item = array();
        
        foreach ($data_tabla as $clave => $valor) {
            
            $item = array(
                "ventasoles" => $data_tabla[$clave]["ventasoles"],
                "ventadolares" => $data_tabla[$clave]["ventadolares"],
                "comprasoles" => $data_tabla[$clave]["comprasoles"],
                "compradolares" => $data_tabla[$clave]["compradolares"],
                "efectivosoles" => $data_tabla[$clave]["efectivosoles"],
                "efectivodolares" => $data_tabla[$clave]["efectivodolares"],
                "tarjetadollar" => $data_tabla[$clave]["tarjetadollar"],
                "tarjetasoles" => $data_tabla[$clave]["tarjetasoles"],
                "contraentredolares" => $data_tabla[$clave]["contraentredolares"],
                "contraentresoles" => $data_tabla[$clave]["contraentresoles"]

            );            
         
            
        }
        
        return $item;
        
    }
    
    function respuestaFacturador($id,array $resp){
        $data_source = new DataSource();
      

          $fila = $data_source->ejecutarActualizacion("update documento set estadolocal=?, estadosunat=? where id = ?",
                  array($resp["estadolocal"],$resp["estadosunat"] ,$id));
          return $fila;
        
        
    }
    
    function updatestadolocal($id, $resp){
        $data_source = new DataSource();
      

          $fila = $data_source->ejecutarActualizacion("update documento set estadolocal=? where id = ?",
                  array($resp ,$id));
          return $fila;
        
        
    }
    
    

    
   

}
