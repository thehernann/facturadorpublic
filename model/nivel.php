<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of nivel
 *
 * @author HERNAN
 */
class nivel {
    //put your code here
    private $id;
    private $descripcion;
    private $abreviatura;
    private $idempresa;
   
    
    
    
    function __construct() {
        
    }
    
    function getIdempresa() {
        return $this->idempresa;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

        
    function getId() {
        return $this->id;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getAbreviatura() {
        return $this->abreviatura;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setAbreviatura($abreviatura) {
        $this->abreviatura = $abreviatura;
    }
    
    function selectpermiso(){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("SELECT n.id,n.descripcion, group_concat(distinct det.descripcion) as permiso from nivel as n INNER JOIN detalle_nivel as det on det.id_nivel = n.id
        where activo = 1 and id_empresa=? GROUP BY n.descripcion;",array($_SESSION['idempresa']));
        
        $niveles= array();
        foreach ($data_tabla as $clave => $valor) {
            $nivel = new nivel();
            $nivel->setId($data_tabla[$clave]["id"]);
            $nivel->setDescripcion($data_tabla[$clave]["descripcion"]);
            $nivel->setAbreviatura($data_tabla[$clave]["permiso"]);
      

            array_push($niveles, $nivel);
        }
        return $niveles;
        
    }

    
    

    function selectAll(){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("SELECT * from nivel where activo = 1 and id_empresa=?;",array($_SESSION['idempresa']));

        
        $niveles= array();
        foreach ($data_tabla as $clave => $valor) {
            $nivel = new nivel();
            $nivel->setId($data_tabla[$clave]["id"]);
            $nivel->setDescripcion($data_tabla[$clave]["descripcion"]);
//            $nivel->setAbreviatura($data_tabla[$clave]["abreviatura"]);
      

            array_push($niveles, $nivel);
        }
        return $niveles;
        
    }
     function selectpermisoporempresa($id){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("SELECT n.id,n.descripcion, group_concat(distinct det.descripcion) as permiso from nivel as n INNER JOIN detalle_nivel as det on det.id_nivel = n.id
        where activo = 1 and id_empresa=? GROUP BY n.descripcion;",array($id));
        
        $niveles= array();
        foreach ($data_tabla as $clave => $valor) {
            $nivel = new nivel();
            $nivel->setId($data_tabla[$clave]["id"]);
            $nivel->setDescripcion($data_tabla[$clave]["descripcion"]);
            $nivel->setAbreviatura($data_tabla[$clave]["permiso"]);
      

            array_push($niveles, $nivel);
        }
        return $niveles;
        
    }
    function selectone($id){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("SELECT * from nivel where id=? and id_empresa=?;",array($id,$_SESSION['idempresa']));
         $nivel = new nivel();
        foreach ($data_tabla as $clave => $valor) {
            
            $nivel->setId($data_tabla[$clave]["id"]);
            $nivel->setDescripcion($data_tabla[$clave]["descripcion"]);
//            $nivel->setAbreviatura($data_tabla[$clave]["abreviatura"]);
      

           
        }
        return $nivel;
        
    }
    
    function insert(nivel $nivel){
        
        $data_source = new DataSource();

        $data_source->ejecutarActualizacion("insert into nivel(descripcion,activo,created_at,id_empresa) values(?,?,?,?);",
                array(
                    $nivel->getDescripcion(),
                    true,
                    date('Y-m-d H:i:s'),
                    $_SESSION['idempresa']));
        
        $id = $data_source->lastinsertid();
        return $id;
    }
    
    function update(nivel $nivel){
        
        $data_source = new DataSource();

        $fila =$data_source->ejecutarActualizacion("update nivel set descripcion=?,updated_at=? where id=? and id_empresa=?;",
                array(
                    $nivel->getDescripcion(),
                    date('Y-m-d H:i:s'),
                    $nivel->getId(),
                    $_SESSION['idempresa']));
        
        
        return $fila;
    }
    function delete($id){
        
        $data_source = new DataSource();

        $fila =$data_source->ejecutarActualizacion("update nivel set activo=0,updated_at= ? where id=? and id_empresa=?;", array(date('Y-m-d H:i:s'),$id,$_SESSION['idempresa']));
        
        
        return $fila;
    }

}
