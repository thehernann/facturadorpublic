<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class serieProducto {

    private $id;
    private $serie;
   
    private $cantidad;
    private $idkardex;

    private $idempresa;
    function __construct() {
        
    }
    function getIdempresa() {
        return $this->idempresa;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

        
    function getIdkardex() {
        return $this->idkardex;
    }

    function setIdkardex($idkardex) {
        $this->idkardex = $idkardex;
    }

    
    function getId() {
        return $this->id;
    }

    function getSerie() {
        return $this->serie;
    }

   
    function setId($id) {
        $this->id = $id;
    }

    function setSerie($serie) {
        $this->serie = $serie;
    }

    
    function getCantidad() {
        return $this->cantidad;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    function select($idkardex) {
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("SELECT * from serie_producto where id_kardex=? and id_empresa=? ;", array($idkardex,$_SESSION['idempresa']));


        $series = array();
        foreach ($data_tabla as $clave => $valor) {
            $serie = new serieProducto();
            $serie->setId($data_tabla[$clave]["id"]);
            $serie->setSerie($data_tabla[$clave]["serie"]);
            $serie->setIdkardex($data_tabla[$clave]["id_kardex"]);
           


            array_push($series, $serie);
        }
        return $series;
    }

    function insert(array $series) {

        
        $data_source = new DataSource();
      

        $filas = $data_source->insertmultiple("insert into serie_producto(serie,id_kardex ,id_empresa) values(?,?,?)", $series
        );
        return $filas;
    }
    
 
    
//    function updatecant(array $id) {
//
//        
//        $data_source = new DataSource();
//        $filas = 0;
//
//        $filas = $data_source->insertmultiple("update serie_producto set cantidad=0 where id= ?;", $id
//        );
//        return $filas;
//    }
    
    

}
