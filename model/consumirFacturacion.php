<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of consumir
 *
 * @author HERNAN
 */


class consumirFacturacion {
    //put your code here
    private $cabecera;
    private $detalles;
   
    
    function __construct(documento $cabecera, array $detalles) {  //documento $cabecera, Detalle $detalles
        $this->cabecera = $cabecera;
        $this->detalles = $detalles;

    }
    function getCabecera() {
        return $this->cabecera;
    }

    function getDetalle() {
        return $this->detalles;
    }

    function setCabecera($cabecera) {
        $this->cabecera = $cabecera;
    }

    function setDetalle($detalles) {
        $this->detalles = $detalles;
    }
    
    function consumirapi(){
        
//        $valida = true;
        // RUTA para enviar documentos
        $url = "https://api.cbetcloud.com/api/integration/tickets";

        //TOKEN para enviar documentos
        $apiKey = "Bearer ".$_SESSION['token'];

        
//        $this->cabecera = new documento();
        
        //////// construir array para el detalle 
        $tipocomprobante = "";
         switch ($this->cabecera->getTipo()){
                case "Factura":  
                $tipocomprobante= "01";
                break;
                case "Boleta":  
                $tipocomprobante= "03";
                break;
                case "nota_credito":  
                $tipocomprobante= "07";
                break;
                case "nota_debito":  
                $tipocomprobante= "08";
                break;
                
             
         }
         $tipodocumentoreceptor= "";
          switch ($this->cabecera->getPersonatipodoc()){
                case "RUC":  
                $tipodocumentoreceptor= "6";
                break;
                case "DNI":  
                $tipodocumentoreceptor= "1";
                break;
                case "VARIOS":  
                $tipodocumentoreceptor= "-";
                break;
                case "CARNET EXT.":  
                $tipodocumentoreceptor= "4";
                break;
                case "PASAPORTE":  
                $tipodocumentoreceptor= "7";
                break;
                case "CEDULA DIPLOMATICA DE IDENTIDAD":  
                $tipodocumentoreceptor= "A";
                break;
                case "NO DOMICILIADO, SIN RUC (EXPORTACION)":  
                $tipodocumentoreceptor= "0";
                break;
                
             
         }
      
       
                
     
          $arraydets = array();
          foreach ($this->detalles as $detalle){
                $montobaseexpor = "";
                $montobaseexonerado = null;
                $montobaseinfecto = null;
                $montobasegratuito = null;
                $montobaseisc = "";
                $montobaseotrostributos ="";
                $tributoVentaGratuita=null;
                $otrosTributos="";
                $porcentajeOtrosTributos="";
                $porcentajeTributoVentaGratuita = null;
       
                if($detalle[13] != 0){
                    $montobaseexpor = $detalle[13];
                }
                if($detalle[14] != 0){
                    $montobaseexonerado = $detalle[14];
                }
                if($detalle[15] != 0){
                    $montobaseinfecto = $detalle[15];
                }

                if($detalle[16] != 0){
                    $montobasegratuito = $detalle[16];
                }

                if($detalle[17] != 0){
                    $montobaseisc = $detalle[17];
                }
                if($detalle[18] != 0){
                    $montobaseotrostributos = $detalle[18];
                }
                if($detalle[19] != 0){
                    $tributoVentaGratuita =$detalle[19];
                }
                if($detalle[20] != 0){
                    $otrosTributos =$detalle[20];
                }
                if($detalle[22] != 0){
                    $porcentajeOtrosTributos =$detalle[22];
                }
                if($detalle[23] != 0){
                    $porcentajeTributoVentaGratuita =$detalle[23];
                }

              $arraydet =  array(
                "cantidad"=> $detalle[5],
		"codigoUnidadMedida"=> "NIU",
		"codigoTipoAfectacionIgv"=> $detalle[4],
		"descripcion"=> $detalle[2],
		"codigoProducto"=> $detalle[1],
		"valorUnitario"=> $detalle[7],
		"precioVentaUnitario"=> $detalle[6],
		"valorReferencialUnitario"=> null,
		"igv"=> $detalle[10],
		"montoBaseIgv"=> $detalle[12],
		"montoBaseExportacion"=> $montobaseexpor,
		"montoBaseExonerado"=> $montobaseexonerado,
		"montoBaseInafecto"=> $montobaseinfecto,
		"montoBaseGratuito"=> $montobasegratuito,
		"montoBaseIsc"=> $montobaseisc,
		"montoBaseOtrosTributos"=> $montobaseotrostributos,
		"tributoVentaGratuita"=> $tributoVentaGratuita,
		"otrosTributos"=> $otrosTributos,
		"porcentajeIgv"=> $detalle[21],
		"porcentajeOtrosTributos"=> $porcentajeOtrosTributos,
		"porcentajeTributoVentaGratuita"=> $porcentajeTributoVentaGratuita,
		"valorVenta"=> $detalle[8]/ (($detalle[10]/100)+1),
		"precioTotal"=> $detalle[8],
		"descuento"=> null
                );
                array_push($arraydets,$arraydet);
                
            }
//            var_dump($arraydets);
            
        $totalValorVentaExonerada= null;
        $totalValorVentaExportacion = null;
        $totalValorVentaGravada = null;
        $totalValorVentaInafecta = null;
        $totalValorVentaGratuita = null;
        $totalIgv =null;
        $totalVenta =null;
        $totalTributosOperacionGratuita =null;
        if($this->cabecera->getMontoexonerado() != 0){
            $totalValorVentaExonerada = $this->cabecera->getMontoexonerado();
        }
        
        if($this->cabecera->getMontoexportacion() != 0){
            $totalValorVentaExportacion = $this->cabecera->getMontoexportacion();
        }
        
        if($this->cabecera->getMontogravada() != 0){
            $totalValorVentaGravada = $this->cabecera->getMontogravada();
        }
        
        if($this->cabecera->getMontoinafecto() != 0){
            $totalValorVentaInafecta = $this->cabecera->getMontoinafecto();
        }
        
        if($this->cabecera->getMontogratuita() != 0){
            $totalValorVentaGratuita = $this->cabecera->getMontogratuita();
            $totalTributosOperacionGratuita=$this->cabecera->getMontogratuita();
        }
        
        if($this->cabecera->getMontoigv() != 0){
            $totalIgv = $this->cabecera->getMontoigv();
        }
        
      
        if($this->cabecera->getTotal() != 0){
            $totalVenta = $this->cabecera->getTotal();
        }
  
        $emision = new DateTime($this->cabecera->getFechaemision());
        $vencimiento = new DateTime($this->cabecera->getFechavencimiento());
        
        $diasp = $vencimiento->diff($emision);
        // will output 2 days
        if($diasp->days > 0){
            $diasprestamo = $diasp->days;
            
        }else {
            $diasprestamo = null;
        }
        $data = array(
        "items" => $arraydets,
        "guiasRelacionadas"=> [],
	"camposAdicionales"=> [],
//	"puntoVenta"=> array(
//		"id"=> 1,
//		"nombreCorto"=> "Central",
//		"nombre"=> "Principal",
//		"direccion"=> "Lima"
//	),
	"totalValorVentaExonerada"=> $totalValorVentaExonerada,
	"totalValorVentaExportacion"=> $totalValorVentaExportacion,
	"totalValorVentaGravada"=> $totalValorVentaGravada,
	"totalValorVentaInafecta"=> $totalValorVentaInafecta,
	"totalValorVentaGratuita"=> $totalValorVentaGratuita,
	"totalIgv"=> $totalIgv,
	"totalVenta"=> $totalVenta,
	"descuentoGlobal"=> null,
	"totalDescuento"=> null,
	"totalTributosOperacionGratuita"=> $totalTributosOperacionGratuita,
	"serie"=> $this->cabecera->getSerie(),
	"numero"=> $this->cabecera->getNumero(),
	"tipoComprobante"=> $tipocomprobante,
	"fechaEmision"=> $this->cabecera->getFechaemision(),
	"codigoMoneda"=> ($this->cabecera->getMoneda()== 'Soles')? "PEN" : "USD" ,
	"fechaVencimiento"=> $this->cabecera->getFechavencimiento(),
	"codigoTipoOperacion"=> "0101",
	"tipoDocumentoReceptor"=> $tipodocumentoreceptor,  ///////////////////////////////////
	"numeroDocumentoReceptor"=> $this->cabecera->getRuc(),
	"denominacionReceptor"=> $this->cabecera->getRazonsocial(),
        "diasCredito" => $diasprestamo,
	"direccionReceptor"=> $this->cabecera->getDireccion(),
	"emailReceptor"=> $this->cabecera->getEmail(),
	"telefonoReceptor"=> "",
	"tipoVenta"=> strtoupper($this->cabecera->getTipoventa()),
	"formaPago"=> array(
		"formaPago"=> ($this->cabecera->getTipoventa() == 'Credito') ? "EFECTIVO" :strtoupper($this->cabecera->getTipopago()),  //strtoupper($this->cabecera->getTipopago())
		"tipoTarjeta"=> ($this->cabecera->getTarjeta() == '') ? null : strtoupper($this->cabecera->getTarjeta()),  //($this->cabecera->getTipopago() != 'Efectivo' && $this->cabecera->getTipopago() != 'C.Entrega') ? strtoupper($this->cabecera->getTipopago()) : null
		"numeroOperacion"=> $this->cabecera->getNrooptipopago(), //$this->cabecera->getNrooptipopago()
		"pagosMixtos"=> []
	),
	"ordenCompra"=> $this->cabecera->getNorden(),
	"observacion"=> $this->cabecera->getObservacion(),
	"detraccion"=> ($this->cabecera->getSujetoa()== '') ? "N" : "Y" ,
	"porcentajeDetraccion"=> ($this->cabecera->getSujetoa()!= '') ? "12" : "",
	"tipoComprobanteAfectado"=> ($this->cabecera->getDocref() != '') ? $this->cabecera->getDocref() : "",
	"serieAfectado"=> $this->cabecera->getSerieref(),
	"numeroAfectado"=> $this->cabecera->getNumeroref(),
	"motivoNota"=> ($tipocomprobante == "07" || $tipocomprobante == "08") ? strtoupper($this->cabecera->getObservacion()) : "",
	"codigoTipoNotaCredito"=> ($this->cabecera->getTiponotaop() != '' && $tipocomprobante=='07')? $this->cabecera->getTiponotaop(): "",
	"codigoTipoNotaDebito"=> ($this->cabecera->getTiponotaop() != '' && $tipocomprobante=='08')? $this->cabecera->getTiponotaop(): "",
	"horaEmision"=> date("H:i:s")
        );
                                

        $data_json = json_encode($data);
//        var_dump($data_json);

        $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt(
                $ch, CURLOPT_HTTPHEADER, array(
                'Authorization: '.$apiKey,
                'Content-Type: application/json',
                )
        );

        // To save response in a variable from server, set headers;
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Get response
        $response = curl_exec($ch);
        // Decode
//            $result = json_decode($response);
        $resp = (json_decode($response,true));
        
//            var_dump($resp);
      
        if ((isset($resp['estado']) && $resp['estado'] == 'INTERNAL_SERVER_ERROR') || (isset($resp['status']) && $resp['status'] == '401')) {
                //Mostramos los errores si los hay
            $msj = "";
            
            if(isset($resp['mensaje'])){
                $msj =$resp['mensaje'];
            }
            if(isset($resp['message'])){
                $msj =$resp['message'];
            }
           
            $estados = array(
                "estadolocal" =>"Error",
                "estadosunat" =>$msj
            );
            
            
        } else {
            
            $estados = array(
                "estadolocal" =>"Registrado",
                "estadosunat" =>$resp['mensaje']
            );
    
        }
//        var_dump($estados);
//        var_dump($resp);
        return $estados;
//        $leer_respuesta = json_decode($respuesta, true);
//        if (isset($leer_respuesta['errors'])) {
//                //Mostramos los errores si los hay
//            echo $leer_respuesta['errors'];
//          
//            $valida=false;
//            
//        } else {
//                //Mostramos la respuesta
// 
//                var_dump($leer_respuesta);
////            $this->cabecera->setSerie($leer_respuesta['serie']);
////            $this->cabecera->setNumero($leer_respuesta['numero']);
////            $this->cabecera->setEnlace($leer_respuesta['enlace']);
////            $this->cabecera->setAceptadasunat($leer_respuesta['aceptada_por_sunat']);
////            $this->cabecera->setSunatdescrip($leer_respuesta['sunat_description']);
////            $this->cabecera->setSunatnote($leer_respuesta['sunat_note']);
////            $this->cabecera->setSunatresponse($leer_respuesta['sunat_responsecode']);
////            $this->cabecera->setSunaterror($leer_respuesta['sunat_soap_error']);
////            $this->cabecera->setPdf($leer_respuesta['pdf_zip_base64']);
////            $this->cabecera->setXml($leer_respuesta['xml_zip_base64']);
////            $this->cabecera->setQr($leer_respuesta['cadena_para_codigo_qr']);
////            $this->cabecera->setCodigohash($leer_respuesta['codigo_hash']);
//            
////            $this->documentocontrol->update($this->cabecera);
//            
//                    
//        }
//        
//        
//        
////        return $this->cabecera;
    }



    

    
    
   
}
