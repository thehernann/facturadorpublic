<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of carateristicas
 *
 * @author HERNAN
 */
class carateristicas {
    //put your code here
    private $id;
    private $caracteristica;
    private $idproducto;
    private $idempresa;
    
    function getId() {
        return $this->id;
    }
    
    function getIdempresa() {
        return $this->idempresa;
    }

    function setIdempresa($idempresa) {
        $this->idempresa = $idempresa;
    }

        
    
    function getIdproducto() {
        return $this->idproducto;
    }

    function setIdproducto($idproducto) {
        $this->idproducto = $idproducto;
    }

    
    function getCaracteristica() {
        return $this->caracteristica;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCaracteristica($caracteristica) {
        $this->caracteristica = $caracteristica;
    }
    
    function search($cadena){
        
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from caracteristicas where caracteristica like concat('%',?,'%') and activo = 1 and id_empresa=? order by id desc;",
                array($cadena,$_SESSION['idempresa']));

        
        $caracts = array();
        foreach ($data_tabla as $clave => $valor) {
            $carat = new carateristicas();
            $carat->setId($data_tabla[$clave]["id"]);
            $carat->setCaracteristica($data_tabla[$clave]["caracteristica"]);
            $carat->setIdproducto($data_tabla[$clave]["id_producto"]);

            array_push($caracts, $carat);
        }
        return $caracts;
        
    }
    
      function selectAll($idprod){
        $data_source = new DataSource();

        $data_tabla = $data_source->ejecutarconsulta("select * from caracteristicas where id_producto= ? and id_empresa=? order by id desc;",
                array($idprod,$_SESSION['idempresa']));

   
        $caracteristicas = array();
        foreach ($data_tabla as $clave => $valor) {
            $caract = new carateristicas();
            $caract->setId($data_tabla[$clave]["id"]);
            $caract->setCaracteristica($data_tabla[$clave]["caracteristica"]);
    
        
            array_push($caracteristicas, $caract);
        }
        return $caracteristicas;
        
    }
    
      function insert(array $caracts) {
          
//        var_dump($detalles);
        $data_source = new DataSource();
        $filas = array();

        $filas = $data_source->insertmultiple("insert into caracteristicas (id_producto,caracteristica,activo,id_empresa) values(?,?,?,?)", $caracts
        );
        
        
        return $filas;
    }
    
    function delete($id){
        $data_source = new DataSource();

    $fila = $data_source->ejecutarActualizacion("delete from caracteristicas where id_producto = ? and id_empresa=?;",array($id,$_SESSION['idempresa']));

   
       
        return $fila;
        
    }
    
    
    


    
    
}
